////
////  DemoViewController.m
////  iHC-MIRF
////
////  Created by Vratislav Zima on 10/11/13.
////  Copyright (c) 2013 Vratislav Zima. All rights reserved.
////

#import "DemoViewController.h"
//#import "CoreDataManager.h"
#import "SYCoreDataManager.h"
#import "AppDelegate.h"
#import "Camera+Setup.h"
#import "Constants.h"
#import "SYDataLoader.h"



#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )


@interface DemoViewController ()

@property (nonatomic, strong) NSFetchedResultsController* fetchedResultsController;


- (void)initFetchedResultsController;
@end



@implementation DemoViewController

@synthesize tableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.camera == nil){
        self.camera = [[SYCoreDataManager sharedInstance] createEntityCamera];
    }
    self.camera.address = @CAMERA_URL;
    self.camera.username = @CAMERA_USERNAME;
    self.camera.password = @CAMERA_PASS;
    self.camera.type=@"Axis";
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.tableView;
    
    self.operationQueue = [[NSOperationQueue alloc]init];
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.5; //seconds
    lpgr.delegate = self;
    [self.tableView addGestureRecognizer:lpgr];
}

-(void)viewWillAppear:(BOOL)animated{

    [self initFetchedResultsController];
    

    if (_tableViewDelegate == nil) {
        _tableViewDelegate = [[DevicesTableViewDelegate alloc] init];
        [_tableViewDelegate setView:self.view];
        [self.tableView setDelegate:_tableViewDelegate];
        [self.tableView setDataSource:_tableViewDelegate];
        [_tableViewDelegate setTableView:self.tableView];
        [_tableViewDelegate setRoomID: @"promo"];
        [_tableViewDelegate.tableView reloadData];
    }
    
    
    NSLog(@"%@", [[SYCoreDataManager sharedInstance]getCurrentElan]);
 
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncComplete:)
                                                 name:@"dataSyncComplete"
                                               object:nil];
    
    
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(loadingStarted:)
    //                                                 name:@"loadingStarted"
    //                                               object:nil];
    

    
    [[SYWebSocket sharedInstance] reconnect];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncComplete:)
                                                 name:@"dataSyncComplete"
                                               object:nil];
}



- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)viewDidAppear:(BOOL)animated{
    [self.player setUrl:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/mjpg/video.mjpg", self.camera.address] ]] ;

    [self.player playWith:self.camera.username And:self.camera.password And:self.camera.type];
    
    UITapGestureRecognizer * answerDoubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap:)];
    answerDoubleTapGesture.numberOfTapsRequired=1;
    [self.player addGestureRecognizer:answerDoubleTapGesture];
    [[SYDataLoader sharedInstance] reloadIfNeeded];
//    if (app.myWS.connecting==NO && app.myWS.connected==NO && [Util getServerAddress]!=nil){
//        [app createWebSocket];
//        [app.myWS startMyWebSocket];
//    }



}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)initFetchedResultsController {
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Room"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"elan.mac == %@", [[[SYCoreDataManager sharedInstance] getCurrentElan] mac]]];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetchRequest
                                 managedObjectContext:context
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    _fetchedResultsController.delegate = self;

    NSError *error;
    
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}



//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//    // Return the number of sections.
//    return 1;
//}
//
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    // Return the number of rows in the section.
//    // If you're serving data from an array, return the length of the array:
//    return [self.devices count];
//}
//
//
//RGBTrimmerViewController* nonSystemsController;
//// Customize the appearance of table view cells.
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MyCell"];
//
//    if(cell == nil)
//    {
//        cell = [[[NSBundle mainBundle] loadNibNamed:@"MyCell" owner:self options:nil] lastObject];
//
//    }
//
//
//    Device* selectedDevice = [self.devices objectAtIndex:indexPath.row];
//   // NSLog(@"    SELECTED:   Name : %@, beightness value:%d, type:%@", selectedDevice.name, selectedDevice.brightness.intValue, selectedDevice.type);
//   // NSLog(@"cell width %f and height %f", cell.frame.size.width, cell.frame.size.height);
//
//    UILabel * deviceName = (UILabel*)[cell viewWithTag:2];
//    UIImageView * deviceIcon = (UIImageView*)[cell viewWithTag:1];
//    UIImageView * deviceControl = (UIImageView*)[cell viewWithTag:3];
//
//    [deviceName setTextAlignment:NSTextAlignmentLeft];
//    [deviceName setTextColor:[UIColor colorWithRed:1.0 green:1 blue:1 alpha:1]];
//    [deviceName setText:[deviceName.text stringByAppendingString:@" °C"]];
//    UIFont* boldFont = [UIFont systemFontOfSize:17];
//    while ([cell viewWithTag:201]!=nil){
//        [[cell viewWithTag:201] removeFromSuperview];
//    }
//    [deviceName setFont:boldFont];
//
//    deviceName.text = ((Device*)[self.devices objectAtIndex:indexPath.row]).label;
//
//    deviceControl.hidden=NO;
//    [[cell viewWithTag:99] removeFromSuperview];
//
//    //    NSLog(selectedDevice.type);
//    // Setting different views for different type of devices
//    if ([selectedDevice.type isEqualToString:@"light"]){
//        if (selectedDevice.deviceState.on != nil){
//
//            if (selectedDevice.deviceState.on.intValue == 1){
//                [deviceIcon setImage:[UIImage imageNamed:@"zarovka_on.png"]];
//                [deviceControl setImage:[UIImage imageNamed:@"room_stav_on.png"]];
//            }else{
//                [deviceIcon setImage:[UIImage imageNamed:@"zarovka_off.png"]];
//                [deviceControl setImage:[UIImage imageNamed:@"room_stav_off.png"]];
//            }
//        }else{
//            if (selectedDevice.deviceState.brightness.intValue==0){
//                [deviceIcon setImage:[UIImage imageNamed:@"lampa_off.png"]];
//            }else{
//                [deviceIcon setImage:[UIImage imageNamed:@"lampa_on.png"]];
//            }
//            TrimmerViewController* nonSystemsController = [[TrimmerViewController alloc] initWithNibName:@"TrimmerView" bundle:nil value:selectedDevice.deviceState.brightness.intValue maxValue:selectedDevice.brightnessSettings.max.intValue];
//
//            nonSystemsController.view.tag=99;
//
//            [cell addSubview:nonSystemsController.view];
//            nonSystemsController.view.center = CGPointMake(258+23, 13+22);
//
//            [cell bringSubviewToFront:nonSystemsController.view];
//            deviceControl.hidden=YES;
//
//        }
//
//    }else if ([selectedDevice.type isEqualToString:@"dimmed light"]){
//
//
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        NSNumber *intensity = (NSNumber*)[defaults objectForKey:selectedDevice.name];
//        if (intensity==nil){
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            NSNumber * intensityNumber = [[NSNumber alloc] initWithInt:selectedDevice.deviceState.brightness.intValue];
//            [defaults setObject:intensityNumber forKey:selectedDevice.name];
//            [defaults synchronize];
//        }
//
//
//
//        if (selectedDevice.deviceState.brightness.intValue==0){
//            [deviceIcon setImage:[UIImage imageNamed:@"lampa_off.png"]];
//        }else{
//            [deviceIcon setImage:[UIImage imageNamed:@"lampa_on.png"]];
//        }
//        TrimmerViewController* nonSystemsController = [[TrimmerViewController alloc] initWithNibName:@"TrimmerView" bundle:nil value:selectedDevice.deviceState.brightness.intValue maxValue:selectedDevice.brightnessSettings.max.intValue];
//
//        nonSystemsController.view.tag=99;
//
//        [cell addSubview:nonSystemsController.view];
//
//        //[nonSystemsController didMoveToParentViewController:self];
//        //[self addChildViewController:nonSystemsController];
//
//
//        nonSystemsController.view.center = CGPointMake(258+23, 13+22);
//
//        [cell bringSubviewToFront:nonSystemsController.view];
//        deviceControl.hidden=YES;
//    }else if ([selectedDevice.type isEqualToString:@"rgb light"]){
//
//        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//        NSNumber *intensity = (NSNumber*)[defaults objectForKey:selectedDevice.name];
//        if (intensity==nil){
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            NSNumber * intensityNumber = [[NSNumber alloc] initWithInt:selectedDevice.deviceState.brightness.intValue];
//            [defaults setObject:intensityNumber forKey:selectedDevice.name];
//            [defaults synchronize];
//        }
//
//        if (selectedDevice.deviceState.brightness.intValue==0){
//            [deviceIcon setImage:[UIImage imageNamed:@"lampa_off.png"]];
//        }else{
//            [deviceIcon setImage:[UIImage imageNamed:@"lampa_on.png"]];
//        }
//
//
//        UIColor * colorUI = [Util convertToUIColorRed:(CGFloat)selectedDevice.deviceState.red.floatValue green:(CGFloat)selectedDevice.deviceState.green.floatValue blue:(CGFloat)selectedDevice.deviceState.blue.floatValue];
//
//
//
//        nonSystemsController = [[RGBTrimmerViewController alloc] initWithNibName:@"RGBTrimmerView" bundle:nil value:selectedDevice.deviceState.brightness.intValue maxValue:selectedDevice.brightnessSettings.max.intValue color:colorUI];
//
//        nonSystemsController.view.tag=99;
//
//        [cell addSubview:nonSystemsController.view];
//
//        //[nonSystemsController didMoveToParentViewController:self];
//        //[self addChildViewController:nonSystemsController];
//
//
//        nonSystemsController.view.center = CGPointMake(258+23, 13+22);
//
//        [cell bringSubviewToFront:nonSystemsController.view];
//        if (self.rgbControllers==nil){
//            self.rgbControllers = [[NSMutableArray alloc] init];
//        }
//        [self.rgbControllers addObject:nonSystemsController];
//        deviceControl.hidden=YES;
//    }else if ([selectedDevice.type isEqualToString:@"heating"]){
//        [deviceIcon setImage:[UIImage imageNamed:@"teplomer_off.png"]];
//        [deviceControl setImage:[UIImage imageNamed:@"room_item_slunko_bcg.png"]];
//
//        if (selectedDevice.deviceState.on){
//            UIImageView * thermoON = [[UIImageView alloc] init];
//            UIImage* img = [UIImage imageNamed:@"room_item_slunko-01.png"];
//            [thermoON setImage:img];
//            thermoON.frame = CGRectMake(thermoON.frame.origin.x, thermoON.frame.origin.y,
//                                        30, 30);
//            thermoON.tag=201;
//            [cell addSubview:thermoON];
//            thermoON.center =CGPointMake(256+23, 14+22);
//
//            [cell bringSubviewToFront:thermoON];
//        }
//
//        if (selectedDevice.temperatureIN==nil){
//            [deviceName setText:selectedDevice.label];
//        }else{
//            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//            formatter.roundingIncrement = [NSNumber numberWithDouble:0.1];
//            formatter.numberStyle = NSNumberFormatterDecimalStyle;
//            [formatter setDecimalSeparator:@","];
//            NSLog(@"heatin value: %f", selectedDevice.temperatureIN.doubleValue);
//            deviceName.text = [formatter stringFromNumber:selectedDevice.temperatureIN];
//            [deviceName setTextAlignment:NSTextAlignmentCenter];
//            [deviceName setTextColor:[UIColor colorWithRed:0.56 green:0.78 blue:0.05 alpha:1]];
//            [deviceName setText:[deviceName.text stringByAppendingString:@" °C"]];
//            UIFont* boldFont = [UIFont boldSystemFontOfSize:18];
//
//            [deviceName setFont:boldFont];
//        }
//    }else if ([selectedDevice.type isEqualToString:@"blinds"]){
//        if ([selectedDevice.deviceState.up boolValue]){
//            [deviceControl setImage:[UIImage imageNamed:@"blinds_up-01.png.png"]];
//            [deviceIcon setImage:[UIImage imageNamed:@"zaluzie_off.png"]];
//        }else{
//            [deviceControl setImage:[UIImage imageNamed:@"blinds_down-01.png.png"]];
//            [deviceIcon setImage:[UIImage imageNamed:@"zaluzie_on_off.png"]];
//        }
//
//
//    }else if ([selectedDevice.type isEqualToString:@"thermometer"]){
//        [deviceIcon setImage:[UIImage imageNamed:@"teplomer_off.png"]];
//        [deviceControl setImage:[UIImage imageNamed:@"room_item_slunko_bcg.png"]];
//
//        UIImageView * thermoON = [[UIImageView alloc] init];
//        UIImage* img = [UIImage imageNamed:@"room_item_slunko-01.png"];
//        [thermoON setImage:img];
//        thermoON.frame = CGRectMake(thermoON.frame.origin.x, thermoON.frame.origin.y,
//                                    30, 30);
//
//        [cell addSubview:thermoON];
//        thermoON.center =CGPointMake(256+23, 14+22);
//        thermoON.tag=201;
//        [cell bringSubviewToFront:thermoON];
//
//
//        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
//        formatter.roundingIncrement = [NSNumber numberWithDouble:0.1];
//        formatter.numberStyle = NSNumberFormatterDecimalStyle;
//        [formatter setDecimalSeparator:@","];
//        deviceName.text = [formatter stringFromNumber:selectedDevice.temperatureIN];
//        [deviceName setTextAlignment:NSTextAlignmentCenter];
//        [deviceName setTextColor:[UIColor colorWithRed:0.56 green:0.78 blue:0.05 alpha:1]];
//        [deviceName setText:[deviceName.text stringByAppendingString:@" °C"]];
//        UIFont* boldFont = [UIFont boldSystemFontOfSize:18];
//
//        [deviceName setFont:boldFont];
//
//    }else {
//        [deviceIcon setImage:[UIImage imageNamed:@"All_on_off_off.png"]];
//        [deviceControl setImage:[UIImage imageNamed:@"room_stav_off.png"]];
//        if (selectedDevice.deviceState.on != nil){
//
//            if (selectedDevice.deviceState.on.intValue == 1){
//                [deviceControl setImage:[UIImage imageNamed:@"room_stav_on.png"]];
//            }else{
//                [deviceControl setImage:[UIImage imageNamed:@"room_stav_off.png"]];
//            }
//        }
//
//
//    }
//    return cell;
//}
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    for (RGBTrimmerViewController * controller in self.rgbControllers){
//        [nonSystemsController.circleView setBackgroundColor:controller.color];
//    }
//}
//
//
//-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//
//    Device* selectedDevice = (Device*)[self.devices objectAtIndex:indexPath.row];
//    if (selectedDevice==nil) return;
//    URLConnector * connector = [[URLConnector alloc] initWithAddressAndLoader:[@"devices/" stringByAppendingString:selectedDevice.name] activityInd:nil];
//    if ([selectedDevice.type isEqualToString:@"light"]){
//        NSString * postDataString = @"{\"on\":";
//        if (selectedDevice.deviceState.on.intValue==1)
//            postDataString = [postDataString stringByAppendingString:@"false"];
//        else
//            postDataString = [postDataString stringByAppendingString:@"true"];
//
//        postDataString = [postDataString stringByAppendingString:@"}"];
//        NSData* data = [postDataString dataUsingEncoding:NSUTF8StringEncoding];
//        NSLog(@"post data to %@ with value %@",[@"device/" stringByAppendingString:selectedDevice.name], postDataString );
//        [connector setConnectionListener:self];
//        [connector ConnectAndPostData:data];
//
//
//    }else if ([selectedDevice.type isEqualToString:@"blinds"]){
//        NSString * postDataString;
//        if (selectedDevice.deviceState.up.intValue==1)
//            postDataString = @"{\"roll down\":null";
//        else
//            postDataString =@"{\"roll up\":null";
//
//        postDataString = [postDataString stringByAppendingString:@"}"];
//        NSData* data = [postDataString dataUsingEncoding:NSUTF8StringEncoding];
//        NSLog(@"post data to %@ with value %@",[@"device/" stringByAppendingString:selectedDevice.name], postDataString );
//        [connector setConnectionListener:self];
//        [connector ConnectAndPostData:data];
//
//
//    }else if ([selectedDevice.type isEqualToString:@"rgb light"]){
//
//        UIColor * colorUI = [Util convertToUIColorRed:(CGFloat)selectedDevice.deviceState.red.floatValue green:(CGFloat)selectedDevice.deviceState.green.floatValue blue:(CGFloat)selectedDevice.deviceState.blue.floatValue];
//        [nonSystemsController.circleView setBackgroundColor:colorUI];
//        for (RGBTrimmerViewController * controller in self.rgbControllers){
//            [controller.circleView setBackgroundColor:colorUI];
//        }
//
//        if (selectedDevice.deviceState.brightness.intValue==0){
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            NSNumber *intensity = (NSNumber*)[defaults objectForKey:selectedDevice.name];
//            if (intensity!=nil){
//                selectedDevice.deviceState.brightness = intensity;
//                //[defaults removeObjectForKey:selectedDevice.name];
//            }
//        }else{
//
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            NSNumber * intensityNumber = [[NSNumber alloc] initWithInt:selectedDevice.deviceState.brightness.intValue];
//            [defaults setObject:intensityNumber forKey:selectedDevice.name];
//            [defaults synchronize];
//            selectedDevice.deviceState.brightness = [[NSNumber alloc] initWithInt:0];
//        }
//
//        NSString * postDataString = @"{\"brightness\":";
//        NSString * outputString = @"{\"red\":";
//        outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"%d", selectedDevice.deviceState.red.intValue]];
//        outputString = [outputString stringByAppendingString:@", \"green\":"];
//        outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"%d", selectedDevice.deviceState.green.intValue]];
//        outputString = [outputString stringByAppendingString:@", \"blue\":"];
//        outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"%d", selectedDevice.deviceState.blue.intValue]];
//        outputString = [outputString stringByAppendingString:@", \"brightness\":"];
//        outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"%d", selectedDevice.deviceState.brightness.intValue]];
//        outputString = [outputString stringByAppendingString:@"}"];
//
//        postDataString = [postDataString stringByAppendingString:[NSString stringWithFormat:@"%d", selectedDevice.deviceState.brightness.intValue]];
//        postDataString = [postDataString stringByAppendingString:@"}"];
//        postDataString = outputString;
//        NSData* data = [postDataString dataUsingEncoding:NSUTF8StringEncoding];
//        NSLog(@"post data to %@ with value %@",[@"device/" stringByAppendingString:selectedDevice.name], postDataString );
//        [connector setConnectionListener:self];
//        [connector ConnectAndPostData:data];
//        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
//
//    }else if ([selectedDevice.type isEqualToString:@"dimmed light"]){
//
//
//        if (selectedDevice.deviceState.brightness.intValue==0){
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            NSNumber *intensity = (NSNumber*)[defaults objectForKey:selectedDevice.name];
//            if (intensity!=nil){
//                selectedDevice.deviceState.brightness = intensity;
//                //[defaults removeObjectForKey:selectedDevice.name];
//            }
//        }else{
//
//            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//            NSNumber * intensityNumber = [[NSNumber alloc] initWithInt:selectedDevice.deviceState.brightness.intValue];
//            [defaults setObject:intensityNumber forKey:selectedDevice.name];
//            [defaults synchronize];
//            selectedDevice.deviceState.brightness = [[NSNumber alloc] initWithInt:0];
//        }
//
//        NSString * postDataString = @"{\"brightness\":";
//
//        postDataString = [postDataString stringByAppendingString:[NSString stringWithFormat:@"%d", selectedDevice.deviceState.brightness.intValue]];
//        postDataString = [postDataString stringByAppendingString:@"}"];
//        NSData* data = [postDataString dataUsingEncoding:NSUTF8StringEncoding];
//        NSLog(@"post data to %@ with value %@",[@"device/" stringByAppendingString:selectedDevice.name], postDataString );
//        [connector setConnectionListener:self];
//        [connector ConnectAndPostData:data];
//        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
//
//    }else{
//        NSString * postDataString = @"{\"on\":";
//        if (selectedDevice.deviceState.on.intValue==1)
//            postDataString = [postDataString stringByAppendingString:@"false"];
//        else
//            postDataString = [postDataString stringByAppendingString:@"true"];
//
//        postDataString = [postDataString stringByAppendingString:@"}"];
//        NSData* data = [postDataString dataUsingEncoding:NSUTF8StringEncoding];
//        NSLog(@"post data to %@ with value %@",[@"device/" stringByAppendingString:selectedDevice.name], postDataString );
//        [connector setConnectionListener:self];
//        [connector ConnectAndPostData:data];
//        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
//    }
//}
//
//
//
//
//
-(void)syncComplete:(NSNotification *)note{

    //self.devices = [[NSMutableArray alloc] initWithArray:[SYCoreDataManager ] ]
    NSLog(@"%@", _devices);
    [_tableViewDelegate setDevices:_devices ];
    [_tableViewDelegate.tableView reloadData];

}
//
//-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
//{
//    CGPoint p = [gestureRecognizer locationInView:self.tableView];
//
//    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
//    Device * device = [self.devices objectAtIndex:indexPath.row];
//
//
//    if ([device.type isEqualToString:@"rgb light"] ){
//        if ([self.view viewWithTag:60]==nil){
//            _rgb = [[RGBViewController alloc] initWithNibName:@"RGBViewController" bundle:nil device:device];
//            _rgb.view.tag = 60;
//
//            [self.view addSubview:_rgb.view];
//        }
//    }else{
//        if ([self.view viewWithTag:50]==nil){
//            if (device.secondaryActions!=nil && [device.secondaryActions count]>0){
//                _actions = [[DeviceActionsViewController alloc] initWithNibName:@"DeviceActionsViewController" bundle:nil ];
//                [_actions setDevice:device];
//                _actions.view.tag = 50;
//                [self.view addSubview:_actions.view];
//            }
//        }
//    }
//}
//
//
//
-(void)onTap:(UITapGestureRecognizer *)recognizer{

}


- (void)connectionResponseFailed:(NSInteger)code {
    
}

- (void)connectionResponseOK:(NSObject *)returnedObject response:(id <Response>)responseType {
    
    
}

//
//
//- (BOOL)prefersStatusBarHidden
//{
//    return YES;
//}
//
@end

