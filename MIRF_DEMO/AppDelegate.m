//
//  AppDelegate.m
//  MIRF_DEMO
//
//  Created by Vratislav Zima on 10/11/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "AppDelegate.h"
#import "Constants.h"
#import "SYApiManager.h"
#import "Constants.h"

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;



- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    [Util setServerAddress:[NSString stringWithFormat:@"http://%s", SERVER_URL]];
    self.operationQueue = [[NSOperationQueue alloc]init];
    [self.operationQueue setMaxConcurrentOperationCount:1];
    
    NSLog(@"%s", SERVER_URL);
    
    [[SYAPIManager sharedInstance] getAPIRootForURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [Util getServerAddress]]] success:^(AFHTTPRequestOperation* request, id object){
        NSLog(@"connection success!");
        NSDictionary * objectDict = (NSDictionary*)object;
        NSDictionary * info = [objectDict objectForKey:@"info"];
        _elan = [[SYCoreDataManager sharedInstance] createEntityElan];
        _elan.mac = [info valueForKey:@"MAC address"];
        //[_elan parseTypeAndVersion:info];
        _elan.baseAddress = @SERVER_URL;
        [[SYCoreDataManager sharedInstance]setCurrentElan:_elan];
        
        [[SYCoreDataManager sharedInstance] saveContext];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"nefunguje");
    }];
    
    
   // NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadDataAsychn:) object:nil];
   // [self.operationQueue addOperation:invocationOperation];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSString stringWithFormat:@"ws://%s/api/ws", SERVER_URL] forKey:@"notification"];
    [defaults synchronize];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncComplete)
                                                 name:@"dataSyncComplete"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStarted)
                                                 name:@"loadingStarted"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStateChanged:)
                                                 name:LOADING_DEVICES
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStateChanged:)
                                                 name:LOADING_DEVICES_ENDED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStateChanged:)
                                                 name:LOADING_ROOMS
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStateChanged:)
                                                 name:LOADING_ROOMS_ENDED
                                               object:nil];

    
    return YES;
}

- (void)loadingStateChanged:(NSNotification*) notification{
    if ([notification.name isEqualToString:LOADING_DEVICES]){
        _loadingDevices = true;
    } else if ([notification.name isEqualToString:LOADING_DEVICES_ENDED]){
        _loadingDevices = false;
    } else if ([notification.name isEqualToString:LOADING_ROOMS]){
        _loadingRooms = true;
    } else if ([notification.name isEqualToString:LOADING_ROOMS_ENDED]){
        _loadingRooms = false;
    } else if ([notification.name isEqualToString:LOADING_SCENES]){
        _loadingScenes = true;
    } else if ([notification.name isEqualToString:LOADING_SCENES_ENDED]){
        _loadingScenes = false;
    }
    
    if (!_loadingRooms && !_loadingDevices){
        [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_DATA_ENDED object:self];
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{

    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    self.inBackground=YES;
    [[SYWebSocket sharedInstance] disconnect];


}
UIBackgroundTaskIdentifier bgTask;
- (void)applicationDidEnterBackground:(UIApplication *)application
{

    self.inBackground=YES;
    [[SYWebSocket sharedInstance] disconnect];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"app enters foreground");
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        NSLog(@"app becomes active");
    
    self.operationQueue = [[NSOperationQueue alloc]init];
    [self.operationQueue setMaxConcurrentOperationCount:1];
    
    NSLog(@"%@", [Util getServerAddress]);
    
    [[SYAPIManager sharedInstance] getAPIRootForURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [Util getServerAddress]]] success:^(AFHTTPRequestOperation* request, id object){
        NSLog(@"connection success!");
        NSDictionary * objectDict = (NSDictionary*)object;
        NSDictionary * info = [objectDict objectForKey:@"info"];
        
      //  [_elan parseTypeAndVersion:info];
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"nefunguje");
    }];
    
    
    if ( [Util getSocketAddress]!=nil){
        self.inBackground=NO;
        [[SYWebSocket sharedInstance] connect];
    }

}




- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }

    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}
/*
 - (NSManagedObjectModel *)managedObjectModel {
 if (_managedObjectModel != nil) {
 return _managedObjectModel;
 }
 _managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil] ;
 return _managedObjectModel;
 }
 // Returns the managed object model for the application.
 // If the model doesn't already exist, it is created from the application's model.
 */

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CoreDataSourceProject" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }

    if (![NSThread currentThread].isMainThread) {
        dispatch_sync(dispatch_get_main_queue(), ^{
            (void)[self persistentStoreCoordinator];
        });
        return _persistentStoreCoordinator;
    }
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"CoreDataSourceProject.sqlite"];

    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.

         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.


         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.

         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]

         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}

         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.

         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }

    return _persistentStoreCoordinator;
}


- (void)loadingStarted{
    _loadingDevices = true;
}

- (void)syncComplete{
    _loadingDevices = false;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[SYWebSocket sharedInstance] disconnect];
    [self saveContext];
}


#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}



-(void)loadDataAsychn:(NSMutableDictionary*)dict
{
    self.loader = [[DeviceLoaderSync alloc] init];
    [self.loader LoadDevices:self];

    //     [self performSelectorOnMainThread:@selector(showImage:) withObject:nil waitUntilDone:NO];
}

@end
