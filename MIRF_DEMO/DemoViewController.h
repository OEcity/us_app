//
//  DemoViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 10/11/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RGBTrimmerViewController.h"
#import "Device.h"
#import "TrimmerViewController.h"
#import "Util.h"
#import "URLConnector.h"
#import "IntensityConfigurationViewController.h"
#import "RGBViewController.h"
#import "ThermostatViewController.h"
#import "MotionJpegImageView.h"
#import "Camera.h"
#import "DeviceActionsViewController.h"
#import "DevicesTableViewDelegate.h"

@interface DemoViewController : UIViewController <URLConnectionListener, UIGestureRecognizerDelegate,NSFetchedResultsControllerDelegate>


@property (nonatomic, retain) NSMutableArray * devices;
@property (nonatomic, retain) NSArray * mdevices;
@property (nonatomic, retain) NSMutableArray * rgbControllers;
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (retain) IntensityConfigurationViewController * intensity;
@property (retain) RGBViewController * rgb;
@property (retain) ThermostatViewController * thermView;
@property (nonatomic, retain) IBOutlet MotionJpegImageView * player;
@property (nonatomic, retain) Camera * camera;
@property (nonatomic, retain) NSTimer * hideTimer;
@property (nonatomic, retain) NSOperationQueue * operationQueue;
@property (retain) DeviceActionsViewController * actions;
@property (nonatomic, retain) DevicesTableViewDelegate * tableViewDelegate;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (strong, nonatomic) Elan* elan;



-(void)syncComplete:(NSNotification *)note;
-(void)onTap:(UITapGestureRecognizer *)recognizer;

@end
