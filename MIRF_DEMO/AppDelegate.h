//
//  AppDelegate.h
//  MIRF_DEMO
//
//  Created by Vratislav Zima on 10/11/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYWebSocket.h"
#import "DevicesLoader.h"
//#import "ViewController.h"
#import "DeviceLoaderSync.h"
#import "Util.h"
#import "SYCoreDataManager.h"
#import "Elan.h"

#define SERVER_URL "217.197.144.56:9090"
#define CAMERA_URL "217.197.144.56:5539"
#define CAMERA_USERNAME "root"
#define CAMERA_PASS "demokx"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, retain) DeviceLoaderSync * loader;
@property (nonatomic, retain) NSOperationQueue *operationQueue;
@property BOOL inBackground;
@property BOOL loadingDevices;
@property BOOL loadingScenes;
@property (nonatomic, retain) NSDictionary* limitsSettings;

@property BOOL loadingRooms;
@property (strong, nonatomic) Elan* elan;


- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;


@end
