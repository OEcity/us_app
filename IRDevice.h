//
//  IRDevice.h
//  
//
//  Created by Tom Odler on 12.09.16.
//
//

#import <Foundation/Foundation.h>
#import "Device.h"

@class IRAction;

NS_ASSUME_NONNULL_BEGIN

@interface IRDevice : Device

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "IRDevice+CoreDataProperties.h"
