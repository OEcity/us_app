2.1.21
- ELANRF-196

2.1.20
- ELANRF-188

2.1.18
- ELANRF-137

2.1.17
- ELANRF-140 Lze se nyní dostat do nastavení přes dlouhý stisk ze všech obrazovek.
- ELANRF-141 Opraveno načítání seznamu oblíbených.
- ELANRF-64 Oblíbené nyní zobrazují stejné rozložení prvků jako v seznamu zařízení pro místnost.

2.1.16
- Upraveno rozlišní zapnutého HCA prvku.

2.1.15
- Modifikace načítání objektů z databáze.

2.1.14
- Upraveno předávání url pro websocket.

2.1.13
- Oprava padání aplikace při odebírání položek
- Oprava připojení WebSocket.

2.1.12
- Optimalizace ukládání informací o zařízeních.

2.1.11
- ELANRF-121, ELANRF-112, ELANRF-118, ELANRF-118

2.1.11
- ELANRF-92 - stabilizována synchronizace s databází cache v zařízení

2.1.10
- issuess ELANRF-92,ELANRF-108


2.1.9
- issuess ELANRF-92 - not deleting devices on state error, Elan selections screen bug fixed

2.1.8
- issuess ELANRF-92
- some elans in list could not be selected - fixed


2.1.7
- issuess ELANRF-18, ELANRF-99, ELANRF-19, ELANRF-21, ELANRF-28, ELANRF-12


2.1.6
- issuess ELANRF-61


2.1.5
- issues 2024, 2023, 2025 (Crashlytics implemented for now)
- translations added


2.1.4
- after display locked hca accepts state change events


2.1.3
- issues 2012,2016, 2017, 2014, 2006



2.1.2
- translations added - issue 1849
- loading fix

2.1.1
- changed way of downloading element states and device data
- pickerview empty value crash fixed


2.1.0
- incrased version number to fit latest itunes version

1.2.011
- devices displayed when only one room
- devices loaded again on room detail


1.2.010
- device pairing clearing bug fixed


1.2.009
- several bug fixes
- hca and central source adding condition included


1.2.008
- several bug fixes
- HCA battery and settings button visibility issues


1.2.007
- HA heaters fixed

1.2.006
- HA heaters bug fixed
- Central source not being saved - fixed


1.2.005
- temperature sources saving fixed
- HA temp sensor bug fixes

1.2.004
- only few devices loading bug fixed


1.2.003
- hca bug fixes
- time schedule bug fixes


1.2.002
- Time schedule bug fixes

1.2.001
- naming conventions changed
- schedule intervals bug fixed
- screen layout offset fixed
- minor bug fixes

1.183
- added copy to function

1.182
- added work with intervals

1.181
- added elan search
- fixed IDs: 1853, 1852, 1851, 1848, 1846, 1844, 1842

1.16
- pridan dalsi arabsky preklad ID 1233

1.15
- moznost editace a mazani IP, ID 1209, 1168, 1195

1.14
- oprava chybneho zobrazeni brightness

1.13
- oprava z CDS

1.12
- oprava chybneho zobrazeni pro iPhone 5s

1.11
- zmena verze na publikacni

1.0.4
- oprava navigace v nastaveni (misto Help -> Mistnosti)
- dodan anglicky manual

1.0.2-1.0.3
- verze pro interni testovani
- dodany preklady

1.0.1
- publikacni verze

0.26
- oprava nacitani prvniho stavu u zarizeni
- pridan defaultni port 80

0.25
- oprava scen

0.24
- oprava pridani RGB prvku do sceny

0.23
- dalsi publikacni verze

0.22
- oprava chyby RGB sceny

0.21
- oprava zaseknuti pri tvorbe zarizeni
- sjednocen dialog pro nacitani
- oprava typu zarizeni

0.20
- oprava padani sceny pro neexistujici zarizeni

0.19
- oprava stavu -1
- oprava nahravani obrazku
- oprava obcasneho mizeni nazvu mistnosti
- oprava detailu prazdne mistnosti

0.18
- oprava grafiky urizlych sipek pro iOS6 u zaluzii

0.17
- opravena chyba pro grafiku v iOS6

0.16
- pridany vsechny akce pro sceny
- pridano d&d pro parovani mistnosti
- pridan dialog pro zobrazeni secondary actions
- opraveny chyby z testovani promo

0.15
- pokud prijde zmena zarizeni, jiz je spravne zpracovana (ID 1007)
- v parovani se zobrazuje typ mistnosti (ID 970)
- nova mistnost se jiz korektne zobrazuje nehlede na stav floorplanu (ID 964)

0.14
- opraveno pridavani mistnosti (ID 1008)

0.13
- pridany ikonky typu mistnoti a zarizeni (ID 942)
- floorplan lze ulozit bez nazvu (ID 991)
- opraveno prohazovani nazvu kamer (ID 987)
- upraveno razeni oblibenych (ID 989)


0.12
- Vyresene ID: 969, 968, 967, 966, 964, 963, 962, 961
- pridan manual
- mensi opravy

0.11
- Jina grafika ve spodnim menu na hlavni obrazovce a jinych obrazovkach. Prosim zapracovat jako na hlavni obrazovce
- V detailu mistnosti v konfiguraci, pokud si vyberu floorplan tak loadovaci kolecko je rozhozene.
- upraveno nacitani pro states
- Vsechny pickery museji jit schovat tapnutim kamkoliv jinam. Ted jdou zrusit jen vyberem prvku.
- Nezobrazuje se typ mistnostni v detailu
- Nenacita se floorplan pokud je u mistnosti nastaven.
- Ve spodnim menu v nastaveni a hlavni obrazovce jsou jinak fonty a rozmisteni obrazoku
- Nelze editovat scenu.
- Prehozene poradi sceny a oblibenych v headru na hlavni obrazovce
- Jina grafika scen oproti MARF (zbytecne puntiky, scena nema stav)
- Pokud si zobrazim stmivaci dialog tak slider se me nenastavi na aktualni hodnotu
- Stala se me divna vec - v mistnosti All jsem mel zobrazeny jen tri prvky (ikdyz jich tam je 6). Jakmile jsem ty prvky sepnul na jinem telefonu tak se me najednou zobrazily.
- V floorplanech se nenacita spravny obrazek
- Ulozi mistnost jako     { "id": "02638","devices":{},"room info":{"type":"workroom","label":"test"},"floorplan":{}}​ Misto jako { "id": "02638","devices":{},"room info":{"type":"workroom","label":"test"},"floorplan":null}
- Chybí dvouslovné typy místností, Pravděpodobně nevyčítá stavy z API
​- Aplikace náhodně padá nebo se zasekává. Stalo se mi ze po spusteni samovolne cca po 10sec. spadla někdy po minutě někdy vubec a nebo se sekla a po vyjetí z aplikace a pokus o znovunajeti do aplikace zase spadla....
- Při přidávání prvků do oblíbených myzí při rolování zelené potvrzovací faje a přesto se prvky přidají
-​ Špatně fungující notifikace - Elan notifikaci posílá ale aplikace na změny reaguje až po několik a sekundách a někdy vůbec
- Špatné RGB ovládání - ​Nelze z něj někdy odejít křížkem a při pokusu o odejítí z RGB ovládání se zasekne. Špatně reaguje na změny barvy
- Špatné ovládání scén - ​Po vložení padá nezobrazuje upozornění na vložení a vloží třeba několik stejných, nezobrazují se správně akce, nejdou ani správne nasavit, u stmívaného svetla nejde nastavit jas a u spínacího aktoru nejde nastavit stav sepnuto nebo vypnuto - víc nemá smysls zkoušet když nefunguje základní funkce
- ​Při konfiguraci aktorů se nezobrazí adresa a typ aktoru

0.10
- upraveno nacitani pro states