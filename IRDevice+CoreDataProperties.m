//
//  IRDevice+CoreDataProperties.m
//  
//
//  Created by Tom Odler on 12.09.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "IRDevice+CoreDataProperties.h"

@implementation IRDevice (CoreDataProperties)

@dynamic led;
@dynamic irAction;

@end
