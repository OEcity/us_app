//
//  MIRF_DEMOTests.m
//  MIRF_DEMOTests
//
//  Created by Vratislav Zima on 10/11/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface MIRF_DEMOTests : XCTestCase

@end

@implementation MIRF_DEMOTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);
}

@end
