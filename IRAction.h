//
//  IRAction.h
//  
//
//  Created by Tom Odler on 12.09.16.
//
//

#import <Foundation/Foundation.h>
#import "Action.h"

@class IRDevice;

NS_ASSUME_NONNULL_BEGIN

@interface IRAction : Action

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "IRAction+CoreDataProperties.h"
