//
//  IconWithLabelTableViewCell.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/8/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IconWithLabelTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ivIcon;
@property (weak, nonatomic) IBOutlet UILabel *lLabel;
@property (weak, nonatomic) IBOutlet UILabel *lSubLabel;

@end
