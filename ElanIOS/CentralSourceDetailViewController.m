//
//  CentralSourceDetailViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 25.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "CentralSourceDetailViewController.h"
#import "Constants.h"

@interface CentralSourceDetailViewController ()

@end

@implementation CentralSourceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UITableView*tbv = [[self.view subviews] firstObject];
    tbv.layer.borderWidth = 1.0f;
    tbv.layer.borderColor = USBlueColor.CGColor;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){
            UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell"];
        
        UILabel*label = [[cell contentView] viewWithTag:2];
        UIImageView*sipka = [[cell contentView] viewWithTag:1];
        UIView*line = [[cell contentView] viewWithTag:3];
        
        label.text = @"Central source";
        sipka.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
        line.hidden = YES;
        return cell;
    }
    
    UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:@"addContact"];
    UIButton*leftButton = [[cell contentView] viewWithTag:5];
    UIButton*rightButton = [[cell contentView] viewWithTag:1005];
    
    UILabel*leftLabel = [[cell contentView] viewWithTag:10];
    UILabel*rightLabel = [[cell contentView] viewWithTag:11];
    
    UIView*line = [[cell contentView] viewWithTag:3];
    
    leftLabel.hidden = rightLabel.hidden = YES;
    leftButton.hidden = rightButton.hidden = NO;
    line.hidden = YES;
    
    if(indexPath.row == 1){
        leftButton.hidden = rightButton.hidden = YES;
        leftLabel.hidden = rightLabel.hidden = NO;
        line.hidden = NO;
    }
    
    leftButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    rightButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    
    leftButton.tag = rightButton.tag = indexPath.row-1;
    
    [leftButton addTarget:nil action:@selector(leftButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    [rightButton addTarget:nil action:@selector(rightButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

-(void)leftButtonTapped:(id)sender{
    UIButton*button = sender;
    NSLog(@"leftbtnTag: %ld", (long)button.tag);
}

-(void)rightButtonTapped:(id)sender{
    UIButton*button = sender;
    NSLog(@"rightBtnTag: %ld", (long)button.tag);
}

@end
