//
//  SYPairDevicesViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/4/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYContainerViewController.h"
#import "Device.h"
#import "Room.h"
#import "PickerViewController.h"
@interface SYPairDevicesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, NSFetchedResultsControllerDelegate, PickerViewDelegate>


@property (nonatomic, retain) SYContainerViewController * containerViewController;
@property (nonatomic, retain) NSMutableArray * devices;
@property (nonatomic, retain) NSArray * rooms;
@property (nonatomic, retain) NSMutableArray * added;


@property (nonatomic, retain) NSString * roomName;
@property (nonatomic, retain) Room * room;
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) IBOutlet UIPickerView * roomsPicker;
@property (nonatomic, retain) IBOutlet UITextField * roomLabel;
@property (nonatomic, retain) IBOutlet UIImageView * roomIcon;
@property (weak, nonatomic) IBOutlet UILabel *iconLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *leftBottomButton;
@property (weak, nonatomic) IBOutlet UIButton *rightBottomButton;
@property (nonatomic, retain) PickerViewController * pickerViewController;



@end
