//
//  DeviceSchedule.h
//  iHC-MIRF
//
//  Created by Tom Odler on 16.03.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TempScheduleMode.h"
#import "DeviceScheduleDay.h"

@interface DeviceSchedule : NSObject

@property (nonatomic, retain) NSMutableArray * modes;
@property (nonatomic, retain) NSMutableArray * days;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, retain) NSString* type;
@property (nonatomic, retain) NSNumber* hysteresis;
@property (nonatomic, copy) NSString* serverId;
- (DeviceScheduleDay*) getDayByPosition:(int) position;
-(id)initWithType:(NSString*)type;

- (void) setDayOnPosition:(DeviceScheduleDay*)day position:(int)position;

@end
