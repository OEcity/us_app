//
//  HeatSettingsViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 24.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "HeatSettingsViewController.h"
#import "SYCoreDataManager.h"
#import "ScheduleDetailViewController.h"
#import "HeatCoolAreaDetailViewController.h"
#import "CentralSourceDetailViewController.h"
#import "Constants.h"

@interface HeatSettingsViewController (){
    BOOL schedulesSelected;
    BOOL hcaSelected;
    BOOL centralPowerSelected;
    
    UITableView *schedulesTableView;
    UITableView *hcaTableview;
    UITableView *centralPowerTableView;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation HeatSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    schedulesSelected = hcaSelected = centralPowerSelected = NO;
    _addNewButton.hidden = YES;
    
    [self initFetchedResultsController];
    _tableView.scrollEnabled = YES;
    
    _tableViewHeight.constant = 3*55;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"schedule"]){
        ScheduleDetailViewController*controller = segue.destinationViewController;
        controller.schedule = sender;
    } else if([segue.identifier isEqualToString:@"heatCoolArea"]){
        HeatCoolAreaDetailViewController * controller = segue.destinationViewController;
        controller.heatCoolArea = sender;
    } else if ([segue.identifier isEqualToString:@"centralSource"]){
        
    }
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return NO;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == schedulesTableView && schedulesSelected)return [[_schedules fetchedObjects] count]+1;
    if(tableView == hcaTableview && hcaSelected)return [[_HCAs fetchedObjects] count]+1;
    if(tableView == centralPowerTableView && centralPowerSelected)return 1;
    if(tableView.tag == 1)return 3;
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _tableView && schedulesSelected && indexPath.row == 0) return [[_schedules fetchedObjects] count]*55 + 55;
    if(tableView == _tableView && hcaSelected && indexPath.row == 1) return [[_HCAs fetchedObjects] count]*55 + 55;
    if(tableView == _tableView && centralPowerSelected && indexPath.row == 0) return 55;
    return 55;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView.tag == 1){
        UITableViewCell * cell = nil;
        
        switch (indexPath.row) {
            case 0:{
                
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                
                schedulesTableView = tbv;
                
                UIView *bgColorView = [[UIView alloc] init];
                bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
                [cell setSelectedBackgroundView:bgColorView];

                
                tbv.tag = 2;
                
                tbv.delegate = self;
                tbv.dataSource = self;

                return cell;
            }
                break;
            case 1:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                hcaTableview = tbv;
                
                
                UIView *bgColorView = [[UIView alloc] init];
                bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
                [cell setSelectedBackgroundView:bgColorView];
                
                tbv.tag = 3;
                
                tbv.delegate = self;
                tbv.dataSource = self;

                return cell;
            }
                break;
            case 2: {
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                centralPowerTableView = tbv;
                
                UIView *bgColorView = [[UIView alloc] init];
                bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
                [cell setSelectedBackgroundView:bgColorView];
            
                tbv.tag = 4;
                
                tbv.delegate = self;
                tbv.dataSource = self;
                
                return cell;
            }
                break;
            default:
                break;
        }
        
        return cell;
        
        
    } else if(tableView == schedulesTableView){
    
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        [cell setSelectedBackgroundView:bgColorView];
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        
        label.text = NSLocalizedString(@"time_schedule_settings_title", @"");
        
        if(schedulesSelected){
            img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            line.hidden = YES;
            
            if(indexPath.row >0){
                img.hidden = YES;
                HeatTimeSchedule*schedule = [_schedules objectAtIndexPath: [NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section]];
                label.text = schedule.label;
            } else {
                label.text = NSLocalizedString(@"time_schedule_settings_title", @"");
                img.hidden = NO;
            }
            
        } else {
            img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
            img.hidden = NO;
            line.hidden = NO;
            
        }
        return cell;
        
    } else if(tableView == hcaTableview){
        

        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        [cell setSelectedBackgroundView:bgColorView];
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        
        label.text = NSLocalizedString(@"heating_area_settings_title", @"");
        
        if(hcaSelected){
            img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            line.hidden = YES;
            
            if(indexPath.row >0){
                HeatCoolArea*hca = [_HCAs objectAtIndexPath: [NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section]];
                label.text = hca.label;
                img.hidden = YES;
            } else {
                label.text = NSLocalizedString(@"heating_area_settings_title", @"");
                img.hidden = NO;
            }
        } else {
            img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
            img.hidden = NO;
            line.hidden = NO;
            
        }
        return cell;

    } else if(tableView == centralPowerTableView){
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        [cell setSelectedBackgroundView:bgColorView];
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        
        label.text = NSLocalizedString(@"heating_source_settings_title", @"");
        
        if(centralPowerSelected){
            img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            line.hidden = YES;
            
            if(indexPath.row >0){

            } else {
            
            }
            
        } else {
            img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
            img.hidden = NO;
            line.hidden = NO;
            
        }
        return cell;
    } else {
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    return cell;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"tableview tag :%ld", (long)tableView.tag);
    if(tableView == schedulesTableView && indexPath.row == 0 && !schedulesSelected){
        schedulesSelected = YES;
        hcaSelected = NO;
        centralPowerSelected = NO;
        
//        [_tableView beginUpdates];
//        [_tableView endUpdates];
        
        
    } else if (tableView == schedulesTableView && indexPath.row == 0 && schedulesSelected){
        schedulesSelected = NO;
        hcaSelected = NO;
        centralPowerSelected = NO;
        
//        [_tableView beginUpdates];
//        [_tableView endUpdates];
        
        
    } else if (tableView == hcaTableview && indexPath.row == 0 && !hcaSelected){
        hcaSelected = YES;
        schedulesSelected = NO;
        centralPowerSelected = NO;
        
//        [_tableView beginUpdates];
//        [_tableView endUpdates];
        
    } else if (tableView == hcaTableview && indexPath.row == 0 && hcaSelected){
        schedulesSelected = NO;
        hcaSelected = NO;
        centralPowerSelected = NO;
        
//        [_tableView beginUpdates];
//        [_tableView endUpdates];
        
    } else if (tableView == centralPowerTableView && indexPath.row == 0 && !centralPowerSelected){
        schedulesSelected = NO;
        hcaSelected = NO;
        centralPowerSelected = YES;
        
//        [_tableView beginUpdates];
//        [_tableView endUpdates];
        
    } else if (tableView  == centralPowerTableView && indexPath.row == 0 && centralPowerSelected) {
        schedulesSelected = NO;
        hcaSelected = NO;
        centralPowerSelected = NO;
//        
//        [_tableView beginUpdates];
//        [_tableView endUpdates];
        
    } else if (tableView == schedulesTableView && indexPath.row > 0){
        HeatTimeSchedule*schedule = [_schedules objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section]];
        [self performSegueWithIdentifier:@"schedule" sender:schedule];
    } else if (tableView == hcaTableview && indexPath.row> 0){
        HeatCoolArea *hca = [_HCAs objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section]];
        [self performSegueWithIdentifier:@"heatCoolArea" sender:hca];
    } else if (tableView == centralPowerTableView && indexPath.row > 0){

        [self performSegueWithIdentifier:@"centralSource" sender:nil];
    }
    
    if(indexPath.row == 0)
        [tableView reloadData];
    
    [self setTableViewBorder];
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == hcaTableview && indexPath.row > 0){
        return YES;
    }
    return NO;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete && _HCAs) {
        if ([_HCAs.fetchedObjects count]<= indexPath.row-1) return;
        Device * device = [_HCAs objectAtIndexPath: [NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section]];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_heatingArea", nil), device.label] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
        alert.tag = indexPath.row-1;
        [alert show];
    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
        
        Device * device = [_HCAs objectAtIndexPath:indexPath];
        [[SYAPIManager sharedInstance] deleteDevice:device success:^(AFHTTPRequestOperation * request, id object){
            
        } failure:^(AFHTTPRequestOperation * request, NSError * error){
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                              message:NSLocalizedString(@"heat_cool_area_failed_delete", nil)
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];
            
        }];
    }
}


-(void)setTableViewBorder{
    if(schedulesSelected){
        _tableViewHeight.constant = ([[_schedules fetchedObjects] count] * 55) + (3*55);
    } else if(hcaSelected){
        _tableViewHeight.constant = ([[_HCAs fetchedObjects] count] * 55) + (3*55);
    } else if (centralPowerSelected){
        _tableViewHeight.constant = (3*55);
    } else {
        _tableViewHeight.constant = (3*55);
    }
    
    if(schedulesSelected){
        schedulesTableView.layer.borderColor =USBlueColor.CGColor;
        schedulesTableView.layer.borderWidth = 1.0f;
        
    } else {
        schedulesTableView.layer.borderColor = nil;
        schedulesTableView.layer.borderWidth = 0;
    }
    
    if (hcaSelected){
        hcaTableview.layer.borderColor = USBlueColor.CGColor;
        hcaTableview.layer.borderWidth = 1.0f;
    } else {
        hcaTableview.layer.borderColor = nil;
        hcaTableview.layer.borderWidth = 0;
    }
    
    if(centralPowerSelected){
        centralPowerTableView.layer.borderColor =  USBlueColor.CGColor;
        centralPowerTableView.layer.borderWidth = 1.0f;
    } else {
        centralPowerTableView.layer.borderColor = nil;
        centralPowerTableView.layer.borderWidth = 0;
    }
    
    if(schedulesSelected || hcaSelected || centralPowerSelected){
        _addNewButton.hidden = NO;
    } else {
        _addNewButton.hidden = YES;
    }
    
    [_tableView reloadData];
    
    [hcaTableview reloadData];
    [schedulesTableView reloadData];
    [centralPowerTableView reloadData];
}

- (IBAction)addNewTapped:(id)sender {
    if(schedulesSelected)
        [self performSegueWithIdentifier:@"schedule" sender:nil];
    else if(hcaSelected)
        [self performSegueWithIdentifier:@"heatCoolArea" sender:nil];
    else
        [self performSegueWithIdentifier:@"centralSource" sender:nil];
}
#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"HeatTimeSchedule"];
    NSFetchRequest *fetchRequest2 = [[NSFetchRequest alloc] initWithEntityName:@"HeatCoolArea"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    [fetchRequest2 setSortDescriptors:sortDescriptors];
    
    _HCAs  = [[NSFetchedResultsController alloc]
                        initWithFetchRequest:fetchRequest2
                        managedObjectContext:context
                        sectionNameKeyPath:nil
                        cacheName:nil];
    _HCAs.delegate = self;
    
    _schedules = [[NSFetchedResultsController alloc]
                 initWithFetchRequest:fetchRequest
                 managedObjectContext:context
                 sectionNameKeyPath:nil
                 cacheName:nil];
    _schedules.delegate = self;
    NSError *error;
    
    if (![_schedules performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
    
    if (![_HCAs performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    if(controller == _HCAs){
        [hcaTableview beginUpdates];
    }
    if(controller == _schedules){
        [schedulesTableView beginUpdates];
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = nil;
    newIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row+1 inSection:newIndexPath.section];
    indexPath = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
    
    if(controller == _HCAs){
       tableView = hcaTableview;
    }
    if(controller == _schedules){
        tableView = schedulesTableView;
    }
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            //            self.emptyLabel.hidden = [_contacts.fetchedObjects count] > 0;
            
            break;
            
        case NSFetchedResultsChangeUpdate:{
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller{
    if(controller == _HCAs){
        [hcaTableview endUpdates];
    }
    if(controller == _schedules){
        [schedulesTableView endUpdates];
    }
    [_tableView beginUpdates];
    [_tableView endUpdates];
}



@end
