//
//  SY_State_RGBView.m
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 18/06/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "SY_State_RGBView.h"

@implementation SY_State_RGBView


#define DEGREES_TO_RADIANS(angle) (angle/180.0*M_PI)

- (void)rotateView:(UIView *)view
            inView:(UIView *)superVeiw
           degrees:(CGFloat)degrees {
    CGRect imageBounds = view.bounds;
    [view.layer setAnchorPoint:CGPointMake(((superVeiw.center.x-CGRectGetMinX(superVeiw.frame))-CGRectGetMinX(view.frame))/ imageBounds.size.width, ((superVeiw.center.y-CGRectGetMinY(superVeiw.frame))-CGRectGetMinY(view.frame)) / imageBounds.size.height)];
    [view setCenter:CGPointMake(superVeiw.center.x-superVeiw.frame.origin.x, superVeiw.center.y-superVeiw.frame.origin.y)];
    view.transform = CGAffineTransformRotate(view.transform, DEGREES_TO_RADIANS(degrees));
}

-(void) drawColorView{
    if (_color == nil){
        [_vColorView setHidden:YES];
        [_lBrightness setHidden:NO];
        float onValue = (float)_value/_maxValue;
        int onValuePercent = onValue * 100;
        [_lBrightness setText:[NSString stringWithFormat:@"%d", onValuePercent]];
    }else{
        [_vColorView setHidden:NO];
        [_lBrightness setHidden:YES];
        _vColorView.alpha = 1.0;
        _vColorView.layer.cornerRadius = _vColorView.frame.size.width/2;
        _vColorView.backgroundColor = _color;
        
        _vColorView.layer.borderColor = [UIColor whiteColor].CGColor;
        _vColorView.layer.borderWidth = 1.0f;
    }
    
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    for(UIView *view in [_vCircleView subviews]){
        [view removeFromSuperview];
    }
    
    int max = 50;
    float onValue = (float)_value/_maxValue;
    UIImageView * myImage;
    for (int i=0; i < max; i++){
        if (((float)i/max)>=onValue){
            myImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stmivani_off.png"]];
        }else{
            myImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stmivani_on1.png"]];
        }
        myImage.frame = CGRectMake(_vCircleView.bounds.size.width/2 - myImage.frame.size.width/2 , 0, myImage.frame.size.width, myImage.frame.size.height);
        [_vCircleView addSubview:myImage];
        float consMove =(360.0/max);
        [self rotateView:myImage inView:_vCircleView degrees:i*consMove - (360/4)];
    }
    [self drawColorView];
}


@end
