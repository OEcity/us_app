//
//  SYTimeScheduleDetailViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 12/3/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYHeatContainerViewController.h"
#import "TemperatureOffsetViewController.h"
@interface SYTimeScheduleDetailViewController : UIViewController <TemperatureSetterDelegate>
@property (nonatomic, retain) SYHeatContainerViewController * container;
@property (nonatomic, retain) NSMutableDictionary * schedule;
@property (weak, nonatomic) IBOutlet UITextField *tfName;
@property (weak, nonatomic) IBOutlet UILabel *lHysteresisInput;
@property (nonatomic, retain) TemperatureOffsetViewController * temperatureOffset;
@property (weak, nonatomic) IBOutlet UIButton *btnMinimum;
@property (weak, nonatomic) IBOutlet UIButton *btnAttenuation;
@property (weak, nonatomic) IBOutlet UIButton *btnNormal;
@property (weak, nonatomic) IBOutlet UIButton *btnComfort;

@property (weak, nonatomic) IBOutlet UILabel *lNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *lHysteresisTitle;
@property (weak, nonatomic) IBOutlet UILabel *lTempModeSettingsTitle;
@property (weak, nonatomic) IBOutlet UILabel *lMinimumTitle;
@property (weak, nonatomic) IBOutlet UILabel *lAttenuationTitle;
@property (weak, nonatomic) IBOutlet UILabel *lNormalTitle;
@property (weak, nonatomic) IBOutlet UILabel *lComfortTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@end
