//
//  NSManagedObject+NeccessaryValue.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 15/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (CheckedValue)
-(void) setCheckedValue:(id)value forKey:(NSString*)key;
@end
