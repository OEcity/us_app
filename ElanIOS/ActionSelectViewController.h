//
//  ActionSelectViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 18/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
#import "Scene.h"
#import "SYColidingAction.h"

@interface ActionSelectViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property ( nonatomic, retain ) NSSet *actionsInfo;
@property ( nonatomic, retain ) NSMutableSet *selectedActions;
@property ( nonatomic, retain ) NSMutableArray *cells;
@property ( nonatomic, retain ) Device *device;
@property ( nonatomic, retain ) Scene *scene;
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@property (nonatomic, strong) SYColidingAction  *syColidingActions;
@property (nonatomic, strong) NSArray  *colidingActions;

- (IBAction)okPressed:(id)sender;
- (IBAction)cancelPressed:(id)sender;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andArray:(NSSet*)actionsInfoArray selected:(NSArray*)selected withDevice:(Device*)aDevice withScene:(Scene*)aScene;

@end
