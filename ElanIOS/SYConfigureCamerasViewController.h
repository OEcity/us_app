//
//  SYConfigureCamerasViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/14/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Camera.h"
#import "SYCoreDataManager.h"

@interface SYConfigureCamerasViewController : UIViewController <UITableViewDataSource, UITableViewDataSource, NSFetchedResultsControllerDelegate>


@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) Camera * selectedCamera;
@property (weak, nonatomic) IBOutlet UILabel *camerasLabel;
@property (weak, nonatomic) IBOutlet UIButton *bottomRightButton;
@property (weak, nonatomic) IBOutlet UIButton *bottomLeftButton;
@property (weak, nonatomic) IBOutlet UILabel *emtyLabel;

@end
