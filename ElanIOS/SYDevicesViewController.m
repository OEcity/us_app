//
//  DevicesViewController.m
//  ElanIOS
//
//  Created by Vratislav Zima on 5/29/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYDevicesViewController.h"
#import "URLConnector.h"
#import "DevicesResponse.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "Device.h"
#import "TrimmerViewController.h"
#import "RGBTrimmerViewController.h"
#import "DeviceInRoom.h"
#import "SYCoreDataManager.h"
#import "IntensityConfigurationViewController.h"
#import "Util.h"
#import "NSObject+SBJson.h"
#import "HUDWrapper.h"
#import "SYDataLoader.h"
#import "CoreDataObjects.h"
#import "Constants.h"
#import "SYSettingsViewController.h"
#import "DevicesCollectionViewDelegate.h"
#import "BlindsViewController.h"
#import "RGBViewController.h"
#import "DimmingViewController.h"
#import "HCAViewController.h"
#import "ChromaticityViewController.h"
#import "UIViewController+RevealViewControllerAddon.h"
#import "ReleViewController.h"
#import "IRControllerViewController.h"
#import "IRKlimaViewController.h"

@interface SYDevicesViewController (){
    BOOL isLoading;
}
@property (nonatomic, strong) NSFetchedResultsController* fetchedResultsController;
@property (strong, nonatomic) UIBarButtonItem *settingsButton;
@end

@implementation SYDevicesViewController

@synthesize activityInd;
@synthesize devices;
@synthesize tableView;
@synthesize header;
@synthesize headerName;
@synthesize headerLabel;
@synthesize imageController;
@synthesize intensity;
@synthesize rgb;
@synthesize collectionView;

#pragma mark - View Management

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _pageControl.numberOfPages = [[[SYCoreDataManager sharedInstance] getAllRooms] count];
    _pageControl.currentPage = _currentRoomID;
    
    header.text = self.room.label.uppercaseString;
    //AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.5; //seconds
    lpgr.delegate = self;
    [self.collectionView addGestureRecognizer:lpgr];
    [self.tableView addGestureRecognizer:lpgr];
    UISwipeGestureRecognizer* swipeUpGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveLeft:)];
    swipeUpGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    
    UISwipeGestureRecognizer* swipeUpGestureRecognizer2 = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveRight:)];
    swipeUpGestureRecognizer2.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [self.headerView addGestureRecognizer:swipeUpGestureRecognizer];
    [self.headerView addGestureRecognizer:swipeUpGestureRecognizer2];
    
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    
    // NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadRoomsAsychn:) object:nil];
    // [app.operationQueue addOperation:invocationOperation];
    
    
    
    UITableViewController *tableViewController = [[UITableViewController alloc] init];
    tableViewController.tableView = self.tableView;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor colorWithRed:(76.0/255.0) green:(183.0/255.0) blue:(237.0/255.0) alpha:1];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.refreshControl addTarget:self
                            action:@selector(refresh)
                  forControlEvents:UIControlEventValueChanged];
    tableViewController.refreshControl = self.refreshControl;
    
    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    self.navigationItem.backBarButtonItem  = [[UIBarButtonItem alloc] initWithTitle:@"Menu"
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:nil
                                                                             action:nil];

}

// Musi byt kvuli tomu kdyz se aplikace sleepne a pak vzbudi (zapina odchytavani notifikaci)
- (void)appplicationIsActive:(NSNotification *)notification {
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (app.loadingDevices==YES){
        [_loaderDialog showWithLabel:NSLocalizedString(@"loading_element_state", nil)];
    }
    
    
    
}

-(void)refresh {
    if (self.refreshControl) {
      //  [_tableViewDelegate.fetchedResultsController performFetch:nil];
      //  [_tableViewDelegate.fetchedResultsController2 performFetch:nil];
        [self.refreshControl endRefreshing];
        
        
        [[SYDataLoader sharedInstance] reloadDevices];
        
        /*
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Loading data" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles: nil];
        
        UIActivityIndicatorView *indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        [indicator startAnimating];
        
        [alertView setValue:indicator forKey:@"accessoryView"];
        [alertView show];
         */
    }
}
/*
 - (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
 CGRect rect = CGRectMake(0, 0, 120, 80);
 UILabel *label = [[UILabel alloc]initWithFrame:rect];
 CGAffineTransform rotate = CGAffineTransformMakeRotation(3.14/2);
 rotate = CGAffineTransformScale(rotate, 0.25, 2.0);
 
 [label setTransform:rotate];
 label.text = [self.rooms objectAtIndex:row];
 label.font = [UIFont systemFontOfSize:22.0];
 label.textAlignment = NSTextAlignmentCenter;
 label.numberOfLines = 2;
 label.lineBreakMode = NSLineBreakByWordWrapping;
 label.backgroundColor = [UIColor clearColor];
 label.clipsToBounds = YES;
 return label ;
 
 }
 */
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    [[SYDataLoader sharedInstance] updateDeviceStates];
    UITapGestureRecognizer *rightTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveRight:)];
    rightTap.numberOfTapsRequired = 1;
    rightTap.numberOfTouchesRequired = 1;
    [self.moveRight addGestureRecognizer:rightTap];
    UITapGestureRecognizer *leftTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(moveLeft:)];
    leftTap.numberOfTapsRequired = 1;
    leftTap.numberOfTouchesRequired = 1;
    [self.moveLeft addGestureRecognizer:leftTap];
    // [[LoaderHelper sharedLoaderHelper] performSelectorInBackground:@selector(reloadDevicesAndRoomsOnCounterChange) withObject:nil];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (app.showRooms){
        app.showRooms=false;
        [self dismissViewControllerAnimated:NO completion:nil];
        return;
    }
    _collectionViewDelegate = [[DevicesCollectionViewDelegate alloc]init];
    [_collectionViewDelegate setView:self.view];
    [collectionView setDelegate:_collectionViewDelegate];
    [collectionView setDataSource:_collectionViewDelegate];
    [_collectionViewDelegate setCollectionView:collectionView];
    
    UINib *dimmingNib = [UINib nibWithNibName:@"DimmingCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:dimmingNib forCellWithReuseIdentifier:@"dimmingCell"];
    
    
    //NSSortDescriptor* sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:nil ascending:NO selector:@selector(roomSort:)];
    //self.rooms = [app.viewController.rooms sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [_collectionViewDelegate setRoomID:_room.roomID];
    [_collectionViewDelegate.collectionView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncComplete:)
                                                 name:@"dataSyncComplete"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dataSyncError:)
                                                 name:@"dataSyncError"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedNotification:)
                                                 name:LOADING_DEVICES
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedNotification:)
                                                 name:LOADING_DEVICES_ENDED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedNotification:)
                                                 name:LOADING_STATES
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedNotification:)
                                                 name:LOADING_STATES_ENDED
                                               object:nil];
    
    
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(loadingStarted:)
    //                                                 name:@"loadingStarted"
    //                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appplicationIsActive:)
                                                 name:UIApplicationDidBecomeActiveNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appplicationIsActive:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    [[SYWebSocket sharedInstance] reconnect];
    
    if (app.loadingDevices==YES){
        [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    }
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Setup view

- (void)initLocalizableStrings {
    [_leftBottomButton setTitle:NSLocalizedString(@"rooms", nil) forState:UIControlStateNormal];
    [_rightBottomButton setTitle:NSLocalizedString(@"menu", nil) forState:UIControlStateNormal];
}

-(void)connectionResponseFailed:(NSInteger)code{
    NSLog(@"Room detail connection failed");
}


-(IBAction)BackButtonPressed:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(IBAction)SettingsButtonPressed:(id)sender{
    [self performSegueWithIdentifier:@"settings" sender:self];
    

}

-(void)syncComplete:(NSNotification *)note{
    [_loaderDialog hide];
    isLoading=NO;
}
-(void)dataSyncError:(NSNotification *)note{
    [_loaderDialog hide];
    UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                      message:NSLocalizedString(@"deviceCommFailed", nil)
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];
    
}
/*
 -(void)syncCompleteLoader:(NSNotification *)note{
 if (note.userInfo !=nil){
 if ([[note.userInfo objectForKey:@"reloadAll"] boolValue]){
 [_loaderDialog hide];
 _rooms = [CoreDataManager readRooms];
 if (!_rooms || [_rooms count]==0 || _currentRoomID >= [_rooms count] ) {
 [self dismissViewControllerAnimated:YES completion:nil];
 return;
 }
 if (_currentRoomID == [_rooms count]-1){
 _moveRight.hidden=YES;
 }
 Room* room = (Room*)[_rooms objectAtIndex:_currentRoomID];
 headerName = room.roomID;
 headerLabel = room.label;
 
 [header setText:headerLabel];
 devices = [[NSMutableArray alloc] initWithArray:[CoreDataManager getDevicesInRoom:headerName]];
 [_tableViewDelegate setDevices:devices ];
 [_tableViewDelegate.tableView reloadData];
 
 isLoading=NO;
 
 }
 }else{
 Device * dev = [[note userInfo] objectForKey:@"device"];
 //devices = [CoreDataManager getDevicesInRoom:headerName];
 for (int i=0; i < [devices count]; i++){
 Device* mydev = [devices objectAtIndex:i];
 
 if ([mydev.deviceID isEqualToString:dev.deviceID]){
 [devices replaceObjectAtIndex:i withObject:dev];
 }
 }
 [_tableViewDelegate setDevices:devices ];
 [_tableViewDelegate.tableView reloadData];
 
 isLoading=NO;
 }
 
 
 
 }
 */


-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    
    [self.collectionViewDelegate handleLongPress:gestureRecognizer];
   
//    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
//     
//     CGPoint p = [gestureRecognizer locationInView:self.collectionView];
//     
//     NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
//     Device * deviceInRoom = (Device*)[self.collectionViewDelegate.fetchedResultsController objectAtIndexPath:indexPath];
//     
//     
//     NSLog(@"long press fired for %@", deviceInRoom.label);
//
//     }
}


-(void) moveRight:(UIGestureRecognizer *)gestureRecognizer{
    if (self.currentRoomID+1 < [self.rooms count]){
        [self changeCurrentRoom:self.currentRoomID+1];
        _pageControl.currentPage +=1;
    }
    
}

-(void)moveLeft:(UIGestureRecognizer *)gestureRecognizer{
    if (self.currentRoomID > 0){
        [self changeCurrentRoom:self.currentRoomID-1];
        _pageControl.currentPage -=1;
    }
    
}

- (void)changeCurrentRoom:(long)newRoomIndex {
    if ([self.rooms count]<=0){
        return;
    }
    
    Room *nextRoom = [self.rooms objectAtIndex:newRoomIndex];
    self.moveLeft.hidden=NO;
    if (newRoomIndex+1 >= [self.rooms count]) {
        self.moveRight.hidden=YES;
    }else {
        self.moveRight.hidden=NO;
    }
    
    if (newRoomIndex < 1) {
        self.moveLeft.hidden=YES;
    }else {
        self.moveLeft.hidden=NO;
    }
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    headerLabel =  nextRoom.label.uppercaseString;
    headerName = nextRoom.roomID;
    app.viewController.currentRoomID = newRoomIndex;
    
    [header setText:headerLabel];
    
    [_collectionViewDelegate setRoomID:nextRoom.roomID];
    [_collectionViewDelegate.collectionView reloadData];
    
    self.currentRoomID = newRoomIndex;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}


-(void)loadingStarted:(NSNotification *)note{
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    isLoading=YES;
}


-(void)loadRoomsAsychn:(NSDictionary*)dict{
    [self performSelectorOnMainThread:@selector(startLoading:) withObject:nil waitUntilDone:NO];
    
    DeviceLoaderSync *devLoader = [[DeviceLoaderSync alloc] init];
    [devLoader LoadDevices:self];
    [self performSelectorOnMainThread:@selector(complete:) withObject:nil waitUntilDone:NO];
}

-(void)complete:(NSMutableDictionary *)Dict{
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self userInfo:Dict];
    
}
-(void)startLoading:(NSDictionary*)object{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingStarted" object:self userInfo:object];
    
}

-(void)recievedNotification:(NSNotification*)notification{
    if ([notification.name isEqualToString:LOADING_DEVICES_ENDED]){
        [_loaderDialog hide];
    } else if ([notification.name  isEqual:LOADING_DEVICES]){
        [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    } else if ([notification.name isEqualToString:LOADING_STATES_ENDED]){
        [_loaderDialog hide];
    } else if ([notification.name  isEqual:LOADING_STATES]){
        [_loaderDialog showWithLabel:NSLocalizedString(@"downloadingNewStates", nil)];
    }
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    
    if(touch.view == _moveLeft){
        [_moveLeft setHighlighted: true];
    }
    
    if(touch.view == _moveRight){
        [_moveRight setHighlighted:true];
    }
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    
    if(touch.view == _moveLeft){
        [_moveLeft setHighlighted: false];
    }
    
    if(touch.view == _moveRight){
        [_moveRight setHighlighted:false];
    }
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    
    if(touch.view == _moveLeft){
        [_moveLeft setHighlighted: false];
    }
    
    if(touch.view == _moveRight){
        [_moveRight setHighlighted:false];
    }
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.identifier isEqualToString:@"blindsSegue"]){
        BlindsViewController *controller = (BlindsViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
        
    } else if([segue.identifier isEqualToString:@"RGBSegue"]){
        RGBViewController *controller = (RGBViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
        
    } else if([segue.identifier isEqualToString:@"dimmingSegue"]){
        DimmingViewController *controller = (DimmingViewController*) [segue destinationViewController];
        controller.brightness = [[_segueDevice getStateValueForStateName:@"brightness"] intValue];
        controller.device = _segueDevice;
        
    } else if([segue.identifier isEqualToString:@"HCASegue"]){
        HCAViewController *controller = (HCAViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
        
    } else if ([segue.identifier isEqualToString:@"LightChtomaticitySegue"]){
        ChromaticityViewController *controller = (ChromaticityViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
    } else if ([segue.identifier isEqualToString:@"releSegue"]){
        ReleViewController *controller = (ReleViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
    } else if ([segue.identifier isEqualToString:@"IRControllerSegue"]){
        IRControllerViewController *controller = (IRControllerViewController*) [segue destinationViewController];
        controller.device = (IRDevice*)_segueDevice;
        controller.inSettings = false;
    } else if ([segue.identifier isEqualToString:@"IR_klima"]){
        IRKlimaViewController *controller = (IRKlimaViewController*) [segue destinationViewController];
        controller.device = (IRDevice*)_segueDevice;
        controller.inSettings = false;
    }

}
@end
