//
//  SelectedDevicesTableViewCell.h
//  iHC-MIRF
//
//  Created by Tom Odler on 30.03.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"

@interface SelectedDevicesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *deviceName;
@property (weak, nonatomic) IBOutlet UIImageView *checker;
@property (nonatomic, retain) IBOutlet Device * device;
@property (weak, nonatomic) IBOutlet UIView *vOverlay;
@property (weak, nonatomic) IBOutlet UIImageView *deviceImage;
@end
