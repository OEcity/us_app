#import "SYContainerViewController.h"
#import "SYConfigureDevicesViewController.h"
#import "SYAddDeviceConfigurationViewController.h"
#import "SYConfigureRoomsViewController.h"
#import "SYAddRoomConfigurationViewController.h"
#import "SYFloorPlanViewController.h"
#import "SYAddFloorplanViewController.h"
#import "SYPairDevicesViewController.h"
#define SegueIdentifierFirst @"embedFirst"
#define SegueIdentifierSecond @"embedSecond"

@interface SYContainerViewController (){
    BOOL changing;
}
@property (strong, nonatomic) NSString *currentSegueIdentifier;
@end

@implementation SYContainerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.currentSegueIdentifier = @"floorPlan";
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
    }

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (changing) return;
    if ([segue.identifier isEqualToString:SegueIdentifierFirst])
    {
        ((SYConfigureDevicesViewController*)segue.destinationViewController).containerViewController=self;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }else if ([segue.identifier isEqualToString:@"embedFirstDetail"]){
        ((SYAddDeviceConfigurationViewController*)segue.destinationViewController).containerViewController=self;
        ((SYAddDeviceConfigurationViewController*)segue.destinationViewController).device=self.device;

        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }
    else if ([segue.identifier isEqualToString:SegueIdentifierSecond])
    {
        ((SYConfigureRoomsViewController*)segue.destinationViewController).containerViewController=self;

        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }else if ([segue.identifier isEqualToString:@"embedSecondDetail"]){
        ((SYAddRoomConfigurationViewController*)segue.destinationViewController).containerViewController=self;
        ((SYAddRoomConfigurationViewController*)segue.destinationViewController).devicesForRoom=self.devicesForRoom;
        ((SYAddRoomConfigurationViewController*)segue.destinationViewController).room=self.room;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }else if ([segue.identifier isEqualToString:@"floorPlan"]){

        ((SYFloorPlanViewController*)segue.destinationViewController).containerViewController=self;
        [self initSubviews:segue];
    }else if ([segue.identifier isEqualToString:@"addFloorplan"]){
        ((SYAddFloorplanViewController*)segue.destinationViewController).containerViewController=self;
                ((SYAddFloorplanViewController*)segue.destinationViewController).floorplan=self.floorplan;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];

    }else if ([segue.identifier isEqualToString:@"sparovatDevices"]){
        ((SYPairDevicesViewController *)segue.destinationViewController).containerViewController=self;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }




}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);

    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    changing = YES;
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:0.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
        changing=NO;
    }];
}

- (void)setViewController:(NSString*)identifier
{
    if ([_currentSegueIdentifier isEqualToString:identifier]) return;
    self.currentSegueIdentifier=identifier;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}

- (NSUInteger)supportedInterfaceOrientations
{

    return UIInterfaceOrientationMaskPortrait;
}


-(void)initSubviews:(UIStoryboardSegue *)segue{
    if (self.childViewControllers.count > 0) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }
    else {
        [self addChildViewController:segue.destinationViewController];
        ((UIViewController *)segue.destinationViewController).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:((UIViewController *)segue.destinationViewController).view];
        [segue.destinationViewController didMoveToParentViewController:self];
    }
}
@end