//
//  UniNavViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 21.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "UniNavViewController.h"

@interface UniNavViewController ()

@end

@implementation UniNavViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setNavigationBarHidden:NO];
    [[self navigationBar] setBackgroundImage:[UIImage new] forBarMetrics: UIBarMetricsDefault];
    [[self navigationBar] setShadowImage: [UIImage new]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)shouldAutorotate{
    return NO;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

@end
