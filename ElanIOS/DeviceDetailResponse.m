//
//  DeviceDetailResponse.m
//  ElanIOS
//
//  Created by Vratislav Zima on 5/30/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "DeviceDetailResponse.h"
#import "SBJson.h"
#import "Device.h"
#import "Action.h"
#import "AppDelegate.h"
//#import "Brightness.h"
#import "Device+Images.h"

@implementation DeviceDetailResponse
@synthesize name;
@synthesize url;
/*
-(NSObject*) parseResponse:(id )inputData identifier:(NSString *)identifier{
    //NSMutableArray * itemsArray = [[NSMutableArray alloc] init];
    
    SBJsonParser *parser2 = [[SBJsonParser alloc] init];
    NSDictionary* jsonObjects;
    if ([inputData isKindOfClass:[NSData class]]){
        NSString *inputString = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];
        
        jsonObjects = [parser2 objectWithString:inputString];
    }else{
        jsonObjects = (NSDictionary*)inputData;
    }
    
    AppDelegate* app = [[UIApplication sharedApplication] delegate];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Device" inManagedObjectContext:app.managedObjectContext];
    
    Device* device = (Device*)[[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    
    
    //parse device info from response

    NSDictionary* deviceInfo = [jsonObjects objectForKey:@"device info"];
    if (deviceInfo==nil) return nil;
    NSString* type = [deviceInfo valueForKey:@"type"];
    NSString* label = [deviceInfo valueForKey:@"label"];
    NSNumber* address = [deviceInfo valueForKey:@"address"];
    NSString* product_type = [deviceInfo valueForKey:@"product type"];
    // parse state
    NSDictionary* state = [jsonObjects objectForKey:@"state"];
    
    NSDictionary* actionsDict = [jsonObjects objectForKey:@"actions info"];
    
    device.heatingDevices =[jsonObjects objectForKey:@"heating devices"];
    device.coolingDevices =[jsonObjects objectForKey:@"cooling devices"];
    
    if([actionsDict valueForKey:@"brightness"] && ![[actionsDict valueForKey:@"brightness"] isKindOfClass:[NSNull class]]){
        NSDictionary* bright = [actionsDict objectForKey:@"brightness"];
        
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Brightness" inManagedObjectContext:app.managedObjectContext];
        
        Brightness * resultBright = (Brightness*)[[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
        resultBright.min = [bright valueForKey:@"min"];
        resultBright.max = [bright valueForKey:@"max"];
        resultBright.step = [bright valueForKey:@"step"];
        resultBright.type = [bright valueForKey:@"type"];
        device.brightnessSettings = resultBright;
    }

    if ([type isEqualToString:@"light"]){
        device.on = [state objectForKey:@"on"];
        if (device.on == nil){
            device.brightness = [state objectForKey:@"brightness"];
            device.iconPrefix =@"zarovka";
        }
        
    }else if ([type isEqualToString:@"blinds"] ){
        device.up = [state valueForKey:@"roll up"];
        device.iconPrefix =@"zaluzie";


    }else if ([type isEqualToString:@"dimmed light"]){
        NSNumber*number = [state valueForKey:@"brightness"];
        device.brightness =number;
        device.iconPrefix =@"zarovka";

    }else if ([type isEqualToString:@"heating"] ||
              [type isEqualToString:@"thermometer"] ||
              [type isEqualToString:@"temperature regulation area"]){
        device.on = [state valueForKey:@"on"];
        NSNumber *temperatureIN = [state valueForKey:@"temperature IN"];
        device.temperatureIN=temperatureIN;
        NSNumber *temperatureOUT = [state valueForKey:@"temperature OUT"];
        device.temperatureOUT=temperatureOUT;
        if (temperatureIN==nil && temperatureOUT==nil){
            NSNumber *temperature = [state valueForKey:@"temperature"];
            device.temperatureIN=temperature;
            
        }
        if ([device.heatingDevices count] > 0) {
            device.iconPrefix = @"teplomer_heating";
        } else if ([device.coolingDevices count] > 0){
            device.iconPrefix = @"teplomer_cooling";
        } else {
            device.iconPrefix =@"teplomer";
        }

    }else if ([type isEqualToString:@"rgb light"]){

        device.brightness = [state objectForKey:@"brightness"];
        RGBColor * color = [[RGBColor alloc] init];
        color.red  = [state objectForKey:@"red"];
        color.green = [state objectForKey:@"green"];
        color.blue = [state objectForKey:@"blue"];
        color.demo = [state valueForKey:@"demo"];
        device.rgbColor = color;
        device.iconPrefix =@"rgb";



    }else if ([type isEqualToString:@"appliance"]){
        device.iconPrefix =@"zasuvka";
        device.on = [state valueForKey:@"on"];
    }else if ([type isEqualToString:@"ventilation"]){
        device.iconPrefix =@"ventilator";
        device.on = [state valueForKey:@"on"];

    }else if ([type isEqualToString:@"gate"]){
        device.up = [state valueForKey:@"on"];
        device.iconPrefix =@"gate";
    }else{
        device.iconPrefix =@"all_on_off";
        device.on = [state valueForKey:@"on"];
    }


    [device setImageOff:[UIImage imageNamed:[device.iconPrefix stringByAppendingString:@"_off"]]];
    [device setImageOn:[UIImage imageNamed:[device.iconPrefix stringByAppendingString:@"_on"]]];

    name = [jsonObjects objectForKey:@"id"];

    device.actionsInfo =  [jsonObjects objectForKey:@"actions info"];
    device.secondaryActions =  [jsonObjects objectForKey:@"secondary actions"];

    
    device.name = name;
    device.type = type;
    device.label = label;
   
    device.product_type = product_type;
    device.device_address = address;
    device.schedule = [jsonObjects objectForKey:@"schedule"];
    device.temperatureOffset = [jsonObjects objectForKey:@"temperature offset"];

    
    NSDictionary* temperatureSensor = [jsonObjects objectForKey:@"temperature sensor"];
    
    if (temperatureSensor && [[temperatureSensor allKeys] count]>0){
        device.temperatureSensorId = [[temperatureSensor allKeys] objectAtIndex:0];
        device.temperatureSensorType = [temperatureSensor valueForKey:device.temperatureSensorId];
    }

    
    
    return device;
    
}
*/
@end
