//
//  SYSettingsViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 6/26/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//


@interface SYSettingsViewController : UIViewController

@property (nonatomic, retain) IBOutlet UIView * bottomMenu;
-(IBAction)backToMainMenu:(id)sender;
-(IBAction)IPConfigureButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *bottomButtonRight;
@property (weak, nonatomic) IBOutlet UIButton *bottomButtonLeft;
@property (weak, nonatomic) IBOutlet UIButton *configurebutton;
@property (weak, nonatomic) IBOutlet UIButton *heatingButton;
@property (weak, nonatomic) IBOutlet UIButton *ipSettingsButton;
@property (weak, nonatomic) IBOutlet UIButton *favouritesButton;
@property (weak, nonatomic) IBOutlet UIButton *scenesButton;
@property (weak, nonatomic) IBOutlet UIButton *camerasButton;
@property (weak, nonatomic) IBOutlet UIImageView *updateBadge;
@property (weak, nonatomic) IBOutlet UIButton *othersButton;
@end
