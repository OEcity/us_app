//
//  SceneReleViewController.h
//  Click Smart
//
//  Created by Tom Odler on 10.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
#import "SYAddSceneViewController.h"
#import "GuideScenesAddViewController.h"

@interface SceneReleViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>
@property (nonatomic)Device *device;
@property (nonatomic) SYAddSceneViewController *controller;
@property (nonatomic) GuideScenesAddViewController *guideController;

@end
