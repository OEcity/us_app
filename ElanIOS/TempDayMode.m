//
//  TempDayMode.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 1/21/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "TempDayMode.h"

@implementation TempDayMode

@synthesize duration = _duration;
@synthesize mode = _mode;
@synthesize startTime;
@synthesize originalStartTime;
@synthesize originalEndTime;
@synthesize endTime;
@synthesize type = _type;

-(id)initWithDayMode:(NSNumber*)mode duration:(NSNumber*)duration type:(NSString *)type{
    self = [super init];
    if (self){
        _mode = mode;
        _duration = duration;
        _type = type;
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    TempDayMode *another = [[TempDayMode allocWithZone: zone] init];
    [another setDuration: [_duration copyWithZone:zone]];
    [another setMode:[_mode copyWithZone:zone]];
    [another setStartTime:[startTime copyWithZone:zone]];
    [another setEndTime:[endTime copyWithZone:zone]];
    [another setType:[_type copyWithZone:zone]];
    [another setOriginalEndTime:[originalEndTime copyWithZone:zone]];
    [another setOriginalStartTime:[originalStartTime copyWithZone:zone]];
    
    
    return another;
}


@end
