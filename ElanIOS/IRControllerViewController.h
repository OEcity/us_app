//
//  IRControllerViewController.h
//  Click Smart
//
//  Created by Tom Odler on 16.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IRDevice.h"
#import "OBShapedButton.h"
#import "HUDWrapper.h"

@interface IRControllerViewController : UIViewController<UIScrollViewDelegate, UIActionSheetDelegate, UIGestureRecognizerDelegate>
@property (nonatomic, retain) IRDevice* device;

//First view buttons
@property (weak, nonatomic) IBOutlet UIButton *upButton;//1
@property (weak, nonatomic) IBOutlet UIButton *leftButton;//2
@property (weak, nonatomic) IBOutlet UIButton *rightButton;//3
@property (weak, nonatomic) IBOutlet UIButton *downButton;//4
@property (weak, nonatomic) IBOutlet UIButton *okButton;//5

@property (weak, nonatomic) IBOutlet UIButton *volUpButton;//6
@property (weak, nonatomic) IBOutlet UIButton *volDownButton;//7
@property (weak, nonatomic) IBOutlet UIButton *chUpButton;//8
@property (weak, nonatomic) IBOutlet UIButton *chDownButton;//9

@property (weak, nonatomic) IBOutlet UIButton *menuButton;//10
@property (weak, nonatomic) IBOutlet UIButton *sourceButton;//11
@property (weak, nonatomic) IBOutlet UIButton *helpButton;//12
@property (weak, nonatomic) IBOutlet UIButton *optionsButton;//13

@property (weak, nonatomic) IBOutlet UIButton *smartButton;//14

@property (nonatomic, retain) HUDWrapper *loaderDialog;

@property (nonatomic) BOOL inSettings;

@end
