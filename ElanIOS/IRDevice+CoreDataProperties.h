//
//  IRDevice+CoreDataProperties.h
//  
//
//  Created by Tom Odler on 12.09.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "IRDevice.h"

NS_ASSUME_NONNULL_BEGIN

@interface IRDevice (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *led;
@property (nullable, nonatomic, retain) IRAction *inIRAction;

@end

NS_ASSUME_NONNULL_END
