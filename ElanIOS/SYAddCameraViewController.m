//
//  SYAddCameraViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/14/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYAddCameraViewController.h"
#import "SYCoreDataManager.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "UIViewController+RevealViewControllerAddon.h"

@interface SYAddCameraViewController (){
    NSString *myCameraName;
    NSString *myCameraAddress;
    NSString *myCameraType;
    NSString *myCameraUsername;
    NSString *myCameraPassword;
    BOOL selected;
    NSArray *typesArray;
    NSInteger activeRow;
    CGRect keyboardBounds;
    CGFloat heightToRefresh;
}

@property (nonatomic, retain) IBOutlet UITextField *actifText;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *centerTbvConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbvHeight;

@end

@implementation SYAddCameraViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    typesArray = @[@"iNELS cam", @"Axis", @"Custom"];
    // Register notification when the keyboard will be show
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    if(_camera != nil){
        myCameraName = _camera.label;
        myCameraAddress = _camera.address;
        myCameraType = _camera.type;
        myCameraUsername = _camera.username;
        myCameraPassword = _camera.password;
    }
    
//    [[self navigationController] navigationBar].hidden = NO;
    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark textField Delegate
- (IBAction)textFieldDidBeginEditing:(UITextField *)textField
{/*
    self.actifText = textField;
    activeRow = textField.tag-1;
    NSLog(@"active row: %ld", (long)activeRow);
    [self setpsswfVisible];*/
    
    if(textField.tag == 5){
        _centerTbvConstraint.constant = -60;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    //    myRoomName = textField.text;
    switch (textField.tag) {
        case 1:
            myCameraName = textField.text;
            break;
        case 2:
            myCameraAddress = textField.text;
            break;
        case 4:
            myCameraUsername = textField.text;
            break;
        case 5:
            myCameraPassword = textField.text;
            _centerTbvConstraint.constant = 0;
            break;
            
        default:
            break;
    }
//    [self setPSSWDUnvisible];
    
    self.actifText = nil;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if(textField.tag == 5|| textField.tag == 2){
        [textField resignFirstResponder];
    } else {
        NSInteger nextTag = textField.tag;
        
        if(nextTag == 2){
            nextTag = 3;
        }
        // Try to find next responder
        activeRow = nextTag;
        NSIndexPath *path = [NSIndexPath indexPathForRow:nextTag inSection:0];
        UITableViewCell *cell = [_tableView cellForRowAtIndexPath:path];
        UITextField *tf = (UITextField*)[[[cell contentView] subviews] firstObject];
        UIResponder* nextResponder = tf;
        
        if (nextResponder) {
            // Found next responder, so set it.
            [_tableView scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
            [nextResponder becomeFirstResponder];
        } else {
            // Not found, so remove keyboard.
            [textField resignFirstResponder];
        }
    }
    return NO;
}

-(void)setpsswfVisible{
    if(activeRow == 4){
        // Detect orientation
        UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
        CGRect frame = self.tableView.frame;
        
        // Start animation
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [UIView setAnimationDuration:0.3f];
        
        // Reduce size of the Table view
        if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown){
            frame.size.height -= keyboardBounds.size.height;
            heightToRefresh = keyboardBounds.size.height;
        }
        else
            frame.size.height -= keyboardBounds.size.width;
        
        // Apply new size of table view
        self.tableView.frame = frame;
        
        // Scroll the table view to see the TextField just above the keyboard
        if (self.actifText)
        {
            CGRect textFieldRect = [self.tableView convertRect:self.actifText.bounds fromView:self.actifText];
            [self.tableView scrollRectToVisible:textFieldRect animated:NO];
        }
        
        [UIView commitAnimations];
    }
}

-(void)setPSSWDUnvisible{
    // Detect orientation
    if(activeRow != 4) return;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    CGRect frame = self.tableView.frame;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.3f];
    
    // Increase size of the Table view
    if (orientation == UIInterfaceOrientationPortrait || orientation == UIInterfaceOrientationPortraitUpsideDown)
        frame.size.height += heightToRefresh;
    else
        frame.size.height += keyboardBounds.size.width;
    
    // Apply new size of table view
    self.tableView.frame = frame;
    
    [UIView commitAnimations];
}

#pragma mark - UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView.tag != 1 && selected)
        return typesArray.count +1;
    if (tableView.tag != 1)
        return 1;
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1 && indexPath.row == 2 && selected){
        return 4*50;
    }
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        //UITableViewCell * cell = nil;
        
        switch (indexPath.row) {
            case 0:{
                NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"enterName", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField *tf = (UITextField*)[[[cell contentView] subviews] firstObject];
                tf.delegate = self;
                tf.attributedPlaceholder = namePlaceHolder;
                tf.tag = 1;
                tf.secureTextEntry = NO;
                tf.text = @"";
                if(myCameraName != nil){
                    tf.text = myCameraName;
                }
                return cell;
            }
                break;
            case 1:{
                NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:@"address" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField *tf = (UITextField*)[[[cell contentView] subviews] firstObject];
                tf.delegate = self;
                tf.attributedPlaceholder = namePlaceHolder;
                tf.tag = 2;
                tf.secureTextEntry = NO;
                tf.text = @"";
                if(myCameraAddress != nil){
                    tf.text = myCameraAddress;
                }
                return cell;
            }
                break;
            case 2:{
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.delegate = self;
                tbv.dataSource = self;
                return cell;
            }
                break;
            case 3:
            {
                NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:@"username" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField *tf = (UITextField*)[[[cell contentView] subviews] firstObject];
                tf.delegate = self;
                tf.attributedPlaceholder = namePlaceHolder;
                tf.tag = 4;
                tf.secureTextEntry = NO;
                tf.text = @"";
                if(myCameraUsername != nil){
                    tf.text = myCameraUsername;
                }
                return cell;
            }
                break;
            case 4:
            {
                NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:@"password" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField *tf = (UITextField*)[[[cell contentView] subviews] firstObject];
                tf.delegate = self;
                tf.attributedPlaceholder = namePlaceHolder;
                tf.secureTextEntry = YES;
                tf.tag = 5;
                tf.text = @"";
                if(myCameraPassword != nil){
                    tf.text = myCameraPassword;
                }
                return cell;
            }
                break;
            default:
                break;
        }
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        
        return cell;
        
        
    } else {
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        if(selected){
            line.hidden = YES;
        } else {
            line.hidden = NO;
        }
        
        if(selected && indexPath.row>0){
            
            label.text = [typesArray objectAtIndex:indexPath.row-1];
            if([label.text isEqualToString:myCameraType]){
                [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            img.hidden = YES;
        } else {
            label.text = NSLocalizedString(@"cameraType", nil);
            
            if(myCameraType && !selected){
                label.text = myCameraType;
            }
        }
        
        if(indexPath.row == 0){
            img.hidden = NO;
            
            if(selected){
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            } else {
                img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
            }
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag != 1 && indexPath.row == 0 && !selected){
        selected = YES;
        
        tableView.layer.borderColor = USBlueColor.CGColor;
        tableView.layer.borderWidth = 1.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
        
    } else if (tableView.tag != 1 && indexPath.row == 0 && selected){
        selected = NO;
        
        tableView.layer.borderColor = nil;
        tableView.layer.borderWidth = 0.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
    } else if(tableView.tag != 1 && indexPath.row >0 && selected){
        myCameraType = [typesArray objectAtIndex:indexPath.row-1];
    }
    
    if(indexPath.row == 0)
        [tableView reloadData];
    
    if(selected){
        _tbvHeight.constant = 9*50;
    } else {
        _tbvHeight.constant = 6*50;
    }
    
}


-(void) keyboardWillShow:(NSNotification *)note
{
    // Get the keyboard size
    
    [[note.userInfo valueForKey:UIKeyboardFrameBeginUserInfoKey] getValue: &keyboardBounds];

}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(IBAction)saveCamera:(id)sender{
    if ([myCameraName isEqualToString:@""]  || myCameraName == nil){
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"nameError", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    
    if([myCameraAddress isEqualToString:@""]|| myCameraAddress == nil){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"cameraIP", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    
    if([myCameraType isEqualToString:@""] || myCameraType == nil){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"pleaseAddCameraType", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    NSString * address  = self.camera.address;
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"address = %@", address];
    NSArray *previous = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Camera" withPredicate:predicate sortDescriptor:nil inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
    
    Camera * camera = [previous firstObject];
    if (camera == nil){
        camera = [[SYCoreDataManager sharedInstance] createEntityCamera];
        //        [camera setElan:[[SYCoreDataManager sharedInstance] getCurrentElanInContext:[[SYCoreDataManager sharedInstance] privateObjectContext]]];
    }
    
    camera.label= myCameraName;
    camera.address = myCameraAddress;
    camera.type = myCameraType;
    camera.username = myCameraUsername;
    camera.password = myCameraPassword;
    [[SYCoreDataManager sharedInstance] saveContext];
    [[self navigationController] popViewControllerAnimated:YES];
}



@end
