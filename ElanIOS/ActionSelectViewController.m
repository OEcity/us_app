//
//  ActionSelectViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 18/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "ActionSelectViewController.h"
#import "CheckedActionCell.h"
#import "Util.h"
#import "SceneAction.h"
#import "Action.h"
#import "SYCoreDataManager.h"
@interface ActionSelectViewController (){
    NSArray * _selected;
    NSDictionary* translateDict;
}

@end

@implementation ActionSelectViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andArray:(NSSet*)actionsInfoArray selected:(NSArray*)selected withDevice:(Device*)aDevice withScene:(Scene*)aScene {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _actionsInfo = actionsInfoArray;
        _selected = selected;
        _device = aDevice;
        _scene = aScene;
        translateDict = [[Util generateNames] copy];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectedActions = [[NSMutableSet alloc] init];
    _cells = [[NSMutableArray alloc] init];
    NSString * pred = [NSString new];
    BOOL first = YES;
    BOOL hasRGB = NO;
    for (SceneAction* object in [_selected copy]){
        if (first){
            pred = [pred stringByAppendingString:[NSString stringWithFormat:@"name != '%@' ", object.action.name]];
            first = NO;
        } else {
            pred = [pred stringByAppendingString:[NSString stringWithFormat:@"AND name != '%@' ", object.action.name]];
        }
        if ([object.action.name isEqualToString:@"red"] || [object.action.name isEqualToString:@"green"] ||[object.action.name isEqualToString:@"blue"]){
            hasRGB = YES;
        }
        [_selectedActions addObject:object.action];
    }
    pred = [pred stringByAppendingString:[first?@"":@"AND " stringByAppendingString:@"name != 'red' AND name != 'green' AND name != 'blue'"]];
    if ([pred length]>0){
        [_cells addObjectsFromArray:[[_actionsInfo filteredSetUsingPredicate:[NSPredicate predicateWithFormat:pred]] allObjects]];
        if ([[_actionsInfo filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"name == 'red' OR name == 'green' OR name == 'blue'"]] count]>=3 && !hasRGB){
            [_cells addObject:[self createRGBCell:@"RGB"]];
        }
    } else{
        _cells =[[_actionsInfo allObjects] mutableCopy];
    }
    [_topLabel setText:NSLocalizedString(@"selectProductType", nil)];
    [_okButton setTitle:NSLocalizedString(@"Ok", nil) forState:UIControlStateNormal];
    [_cancelButton setTitle:NSLocalizedString(@"confirmation.cancel", nil) forState:UIControlStateNormal];
    
    _syColidingActions = [[SYColidingAction alloc] init];
    _colidingActions = [_syColidingActions filterColidingActionsForAtions:_actionsInfo andSelectedActions:_selectedActions];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)okPressed:(id)sender {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    NSMutableArray * res = [[NSMutableArray alloc] init];
    
    if (_selected !=nil){
        [res addObjectsFromArray:_selected];
    }
    if (_selectedActions ){
        
        for (Action * action in _selectedActions){
            SceneAction *sceneAction = [[SYCoreDataManager sharedInstance] getSceneActionWithSceneID:_scene.sceneID withDeviceID:_device.deviceID withActionName:action.name inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
            if (sceneAction == nil){
                sceneAction = [[SYCoreDataManager sharedInstance] createEntitySceneActionInContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
                [sceneAction setAction:[[SYCoreDataManager sharedInstance] getActionWithName:action.name withDeviceID:_device.deviceID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]]];
                [sceneAction setDevice:[[SYCoreDataManager sharedInstance] getDeviceWithID:_device.deviceID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]]];
                [sceneAction setScene:[[SYCoreDataManager sharedInstance] getSceneWithID:_scene.sceneID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]]];
                [[SYCoreDataManager sharedInstance] saveContext];
            }
            [res addObject:sceneAction];
        }
    }
    NSDictionary * result = [[NSDictionary alloc] initWithObjectsAndKeys:res,@"selected", nil];
    [center postNotificationName:@"actionsSelected" object:self userInfo:result];
    [self.view removeFromSuperview];
}

- (IBAction)cancelPressed:(id)sender {
    [self.view removeFromSuperview];
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_cells count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CheckedActionCell* cell;
    Action * action = [_cells objectAtIndex:indexPath.row];
    cell = [self createCustomCell:action];
    if ([_selectedActions containsObject:action] || ([cell.name  isEqual: @"RGB"] && [[_selectedActions filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"name == 'red' OR name == 'green' OR name == 'blue'"]] count]>0)){
        [cell.checker setHidden:NO];
    } else {
        [cell.checker setHidden:YES];
    }
    if ([_colidingActions containsObject:action] || ([cell.name  isEqual: @"RGB"] && [[_colidingActions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"name == 'red' OR name == 'green' OR name == 'blue'"]] count]>0)){
        [cell setUserInteractionEnabled:NO];
//        [cell setBackgroundColor:[UIColor redColor]];
        [[cell vOverlay] setHidden:NO];
    } else {
        [cell setUserInteractionEnabled:YES];
//        [cell setBackgroundColor:[UIColor clearColor]];
        [[cell vOverlay] setHidden:YES];
    }
    return cell;
}

-(CheckedActionCell *)createCustomCell:(Action*)action
{
    NSString * label = action.name;
    
    
    CheckedActionCell* cell = (CheckedActionCell *)[[[NSBundle mainBundle] loadNibNamed:@"CheckedActionCell" owner:self options:nil] objectAtIndex:0];
    cell.action = action;
    NSString* value = [translateDict objectForKey:label];
    
    if(value)
        [cell.nameLabel setText: NSLocalizedString(value, @"")];
    else
        [cell.nameLabel setText:label];
    
    cell.name =label;
    [cell.nameLabel setTextColor:[UIColor blackColor]];
    return cell;
}

-(CheckedActionCell *)createRGBCell:(NSString*)label
{
    CheckedActionCell* cell = (CheckedActionCell *)[[[NSBundle mainBundle] loadNibNamed:@"CheckedActionCell" owner:self options:nil] objectAtIndex:0];
    NSString* value = [translateDict objectForKey:label];
    
    if(value)
        [cell.nameLabel setText: NSLocalizedString(value, @"")];
    else
        [cell.nameLabel setText:label];
    
    cell.name =label;
    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        [self changeViewTableSize];
    }
}


-(void)changeViewTableSize{
    CGFloat tableHeight = 0.0f;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenHeight = screenRect.size.height;
    for (int i = 0; i < [_cells count]; i ++) {
        tableHeight += [self tableView:self.tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if (tableHeight + 89 > screenHeight) tableHeight-=[self tableView:self.tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    
    
    _contentView.frame = CGRectMake(_contentView.frame.origin.x, screenHeight/2 - (tableHeight+89)/2, _contentView.frame.size.width, tableHeight+89);
    _tableView.frame = CGRectMake(self.tableView.frame.origin.x, 39, self.tableView.frame.size.width, tableHeight);
    _okButton.frame = CGRectMake(_okButton.frame.origin.x, _contentView.frame.size.height -50, _okButton.frame.size.width, _okButton.frame.size.height);
    _cancelButton.frame = CGRectMake(_cancelButton.frame.origin.x, _contentView.frame.size.height -50, _cancelButton.frame.size.width, _cancelButton.frame.size.height);
    
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 72.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    CheckedActionCell * cell = (CheckedActionCell*)[tableView cellForRowAtIndexPath:indexPath];
    if ([cell.name isEqualToString:@"RGB"]){
        NSSet* objects = [_actionsInfo filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"name == 'red' OR name == 'green' OR name == 'blue'"]];
        if (cell.checker.hidden==NO){
            for (id object in objects){
                [_selectedActions removeObject:object];
            }
            [cell.checker setHidden:YES];
        }else{
            for (id object in objects){
                [_selectedActions addObject:object];
            }
            [cell.checker setHidden:NO];
        }
        
    } else {
        Action* action1 = cell.action;
        if (cell.checker.hidden==NO){
            [_selectedActions removeObject:action1];
            [cell.checker setHidden:YES];
        }else{
            [_selectedActions addObject:action1];
            [cell.checker setHidden:NO];
        }
    }
    _colidingActions = [_syColidingActions filterColidingActionsForAtions:_actionsInfo andSelectedActions:_selectedActions];
    [self.tableView reloadData];
}





@end
