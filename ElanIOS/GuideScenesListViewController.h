//
//  GuideScenesListViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 05.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Scene.h"

@interface GuideScenesListViewController : UIViewController<NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController* scenes;

@end
