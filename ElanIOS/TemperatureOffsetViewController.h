//
//  TemperatureOffsetViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/17/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol TemperatureSetterDelegate
- (void)temperatureSet:(float)temperature data:(NSDictionary*)data;
@end

@interface TemperatureOffsetViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lDigits;
@property (weak, nonatomic) IBOutlet UILabel *lNumbers;
@property float realTemperature;
@property (nonatomic, retain) id <TemperatureSetterDelegate> delegate;
@property (nonatomic, retain) NSDictionary*data;
@property (weak, nonatomic) IBOutlet UIButton *bCancel;
@property (weak, nonatomic) IBOutlet UIButton *bDone;

@property (weak, nonatomic) IBOutlet UIButton *bDigitsDown;
@property (weak, nonatomic) IBOutlet UIButton *bDigitsUp;
@property (weak, nonatomic) IBOutlet UIButton *bNumbersUp;
@property (weak, nonatomic) IBOutlet UIButton *bNumbersDown;
-(id)initWithDelegate:(id<TemperatureSetterDelegate>)delegate min:(float)min max:(float)max data:(NSDictionary*)data;
-(void)setBoundsWithMax:(float)max min:(float)min;
-(void)displayNumber;

@end
