//
//  SY_State_View.m
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 18/06/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "SY_State_View.h"

@implementation SY_State_View

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

+(id)initStateViewForNibName:(NSString*)name{
    NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:name
                                                      owner:self
                                                    options:nil];
    return [nibViews firstObject];
}

+(void)insertView:(UIView*)view ToSuperView:(UIView*)superVeiw{
    [view setCenter:CGPointMake(superVeiw.frame.size.width/2,                                                                                                                superVeiw.frame.size.height/2)];
    [view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [superVeiw addSubview:view];
    NSArray * verticalConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":view}];
    NSArray * horizontalConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":view}];
    [superVeiw addConstraints:verticalConstraint];
    [superVeiw addConstraints:horizontalConstraint];
}


-(void)insertViewToSuperView:(UIView*)superVeiw{
    [self setTranslatesAutoresizingMaskIntoConstraints:NO];
    [superVeiw addSubview:self];
    NSArray * verticalConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":self}];
    NSArray * horizontalConstraint = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[view]-0-|" options:0 metrics:nil views:@{@"view":self}];
    [superVeiw addConstraints:verticalConstraint];
    [superVeiw addConstraints:horizontalConstraint];
}

-(void)displayView{
    [self setNeedsDisplay];
    [self updateConstraints];
}

@end
