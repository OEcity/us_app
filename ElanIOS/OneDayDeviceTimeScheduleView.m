//
//  OneDayDeviceTimeScheduleView.m
//  iHC-MIRF
//
//  Created by admin on 10.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "OneDayDeviceTimeScheduleView.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation OneDayDeviceTimeScheduleView{
    
    float WIDTH_TO_TIME;
    UIColor * paint;
    
    
    NSMutableArray *modesStartY;
    NSMutableArray *modesEndY;
    
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
-(void)repaintDay:(DeviceScheduleDay *)day{
    _mDay = day;
    [self repaintContent];
    
}

-(void)repaintContent{
    NSArray *viewsToRemove = [self subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    [self drawModes];
    
    
}
-(void)drawModes{
    WIDTH_TO_TIME = self.frame.size.height / 1440;
    
    if ([_mDay.modes count] == 0)
        return;
    modesStartY = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
    modesEndY = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
    
    int i=0;
    float startX = 0;
    for (TempDayMode *mode in _mDay.modes) {
        modesStartY[i] = [[NSNumber alloc ] initWithFloat:startX + [mode.startTime intValue] * WIDTH_TO_TIME];
        modesEndY[i] = [[NSNumber alloc ] initWithFloat: ([mode.endTime intValue] - [mode.startTime intValue]) * WIDTH_TO_TIME];
        
        
        paint = [self.dataSource getColorForPosintion:[mode.mode intValue]];
        
        
        UIView * areaRect = [[UIView alloc] initWithFrame:CGRectMake(0, [modesStartY[i] floatValue], self.frame.size.width,[modesEndY[i] floatValue])];
        [areaRect setBackgroundColor:paint];
        areaRect.tag = i+10;
        [self addSubview:areaRect];
        
        if ([self.dataSource shouldShowImage]){
            UIImageView *upDownImage = nil;
            switch([mode.mode intValue]){
                case 1:
                    upDownImage = [[UIImageView alloc] initWithFrame:CGRectMake(areaRect.bounds.origin.x + (areaRect.bounds.size.width/10), areaRect.bounds.origin.y, (areaRect.bounds.size.width/10)*8, areaRect.bounds.size.height)];
                    [upDownImage setImage:[UIImage imageNamed:@"zaluzieNahoru"]];
                    break;
                case 2:
                    upDownImage = [[UIImageView alloc] initWithFrame:CGRectMake(areaRect.bounds.origin.x + (areaRect.bounds.size.width/10), areaRect.bounds.origin.y, (areaRect.bounds.size.width/10)*8, areaRect.bounds.size.height)];
                    [upDownImage setImage:[UIImage imageNamed:@"zaluziedolu"]];
                    break;
                default:
                    break;
            }
            if (upDownImage){
                [upDownImage setContentMode:UIViewContentModeScaleAspectFit];
                
                [areaRect addSubview:upDownImage];
            }
        }

        
        //NSString* time =[NSString stringWithFormat:@"%@-%@",  [self getTimeFromMinute:[mode.startTime intValue]], [self getTimeFromMinute:[mode.endTime intValue]]];
    }
        
        
        
    
}

/*
-(NSString*) getTimeFromMinute:(int) minutes {
    int hours = minutes / 60;
    return [NSString stringWithFormat:@"%02d:%02d", hours,minutes - hours * 60];
}
*/

@end
