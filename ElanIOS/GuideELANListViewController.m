//
//  GuideELANListViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 27.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "GuideELANListViewController.h"
#import "SYCoreDataManager.h"
#import "SYAPIManager.h"
#import "SYWebSocket.h"
#import "SYDataLoader.h"
#import "IPTableViewCell.h"
#import "AppDelegate.h"

@interface GuideELANListViewController (){
    NSInteger selectedRow;
    BOOL changed;
    NSIndexPath* indexOfElan;

}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController* elans;



@end

@implementation GuideELANListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    // Do any additional setup after loading the view.
    [self initFetchedResultsController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    //    [self.tableView endUpdates];
    [self.tableView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Elan"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _elans = [[NSFetchedResultsController alloc]
              initWithFetchRequest:fetchRequest
              managedObjectContext:context
              sectionNameKeyPath:nil
              cacheName:nil];
    _elans.delegate = self;
    NSError *error;
    
    if (![_elans performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}

#pragma mark - UITableView datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    // If you're serving data from an array, return the length of the array:
    NSLog(@"%lu",[_elans.fetchedObjects count] );
    return [_elans.fetchedObjects count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IPTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"serverCell"];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)tableView:(UITableView *)_tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject* object = [_elans objectAtIndexPath:indexPath];
        
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_elan", nil),[object valueForKey:@"label"]] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"confirmation.yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [controller dismissViewControllerAnimated:YES completion:nil];
             [self deleteElanWithMac:[[_elans objectAtIndexPath:indexPath] mac]];
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"confirmation.no", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [controller dismissViewControllerAnimated:YES completion:nil];
        }];

        [controller addAction:okAction];
        [controller addAction:cancelAction];
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    
    
}

-(void)loadingRooms{
    [_loaderDialog hide];
    //    [[SYWebSocket sharedInstance] initialize];
    [_tableView reloadData];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pairingRooms" object:nil];
}

- (void) deleteElanWithMac:(NSString*)macAddress{
    [[SYAPIManager sharedInstance] cancelAllOperations];
    
    Elan * elan = [[SYCoreDataManager sharedInstance] getELANwithMAC:macAddress inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
    [[SYCoreDataManager sharedInstance] deleteManagedObject:elan inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
    [[SYCoreDataManager sharedInstance] saveContext];
    
    for(Elan*myElan in [[SYCoreDataManager sharedInstance] getCurrentElans]){
        if ([[myElan mac] isEqualToString:macAddress]){
            [[SYWebSocket sharedInstance] disconnect];
            //[[SYCoreDataManager sharedInstance] setCurrentElan:nil];
            [[SYCoreDataManager sharedInstance] saveContext];
            [[SYAPIManager sharedInstance] reinit];
        }
    }
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedRow = indexPath.row;
    UITableViewCell * cell = [_tableView cellForRowAtIndexPath:indexPath];
    cell.selected=NO;
    [self performSegueWithIdentifier:@"input" sender:self];
}

-(void) switchTapped:(UIButton*) sender{
    _currentElan = [_elans objectAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    if(_currentElan.selected.boolValue){
        
        [[SYCoreDataManager sharedInstance] setUnselectedElan:_currentElan];
        [[SYCoreDataManager sharedInstance]deleteObjectsForElan:_currentElan];
        [_tableView reloadData];
        
        
        AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        MainMenuPVC*menu = app.mainMenuPVC;
        
        [menu refreshPVC:@"ip config view controller - switch tapped"];
        return;
    }
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"downloading_data", nil) ] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"confirmation.yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [controller dismissViewControllerAnimated:YES completion:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(loadingRooms)
                                                     name:@"pairingRooms"
                                                   object:nil];
        
        [[SYAPIManager sharedInstance] cancelAllOperations];
        [SYWebSocket sharedInstance].connected=YES;
        [[SYWebSocket sharedInstance] disconnect];
        
        [[SYCoreDataManager sharedInstance] setSelectedElan:_currentElan];
        [self setupElan];
        
        [_tableView reloadData];
        changed=YES;
            }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"confirmation.no", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [controller addAction:okAction];
    [controller addAction:cancelAction];
    [self presentViewController:controller animated:YES completion:nil];
}

- (void)configureCell:(IPTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    UILabel * nameLabel = (UILabel*)[cell viewWithTag:1];
    UILabel * addressLabel = (UILabel*)[cell viewWithTag:2];
    UIImageView * isActive = (UIImageView*)[cell viewWithTag:3];
    UIImageView * iVCircle = (UIImageView*)[cell viewWithTag:5];
    Elan* elan =(Elan*)[_elans objectAtIndexPath:indexPath];
    nameLabel.text = elan.label;
    addressLabel.text = elan.baseAddress;
    //[isActive setHighlighted:!elan.selected.boolValue];
    if(elan.selected.boolValue == true){
        [isActive setImage:[UIImage imageNamed:@"vyber_modre_kolecko.png"]];
    }else{
        [isActive setImage:[UIImage imageNamed:@"vyber_sede_kolecko.png"]];
    }
    [isActive setUserInteractionEnabled:NO];
    if (elan.selected.boolValue){
        iVCircle.image = [UIImage imageNamed:@"elan_on.png"];
        iVCircle.highlightedImage = [UIImage imageNamed:@"elan_bila.png"];
    }else{
        [iVCircle setImage:[UIImage imageNamed:@"elan_off.png"]];
        iVCircle.image= [UIImage imageNamed:@"elan_off.png"];
        iVCircle.highlightedImage = [UIImage imageNamed:@"elan_bila.png"];
    }
    cell.btn.userInteractionEnabled = YES;
    cell.btn.tag = indexPath.row;
    [cell.btn addTarget:self action:@selector(switchTapped:) forControlEvents:UIControlEventTouchUpInside];
    
}


-(void)setupElan{
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    [[SYAPIManager sharedInstance] getAPIRootWithSuccessForElan:_currentElan success:^(AFHTTPRequestOperation *request, NSDictionary* response){
        NSString*inf = [response valueForKey:@"info"];
        NSString*myType = nil;
        
        for (NSString* elanType in ELAN_TYPES) {
            NSString* version = [inf valueForKey:[elanType stringByAppendingString:@" version"]];
            if(version != nil){
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:elanType forKey:@"elanType"];
                [defaults synchronize];
                myType = elanType;
                NSLog(@"Elan type: %@", elanType);
            }
        }
        
        NSString* ws = [response valueForKey:@"notifications"];
        
        [[SYCoreDataManager sharedInstance] setElanTypeAndWSAddress:_currentElan type:myType wsAddress:ws];
        
        [[SYDataLoader sharedInstance] loadDataFromElan:_currentElan];
        
        //        [[SYWebSocket sharedInstance] initialize];
        
        
    }failure:^(AFHTTPRequestOperation * request, NSError * error){
        [_loaderDialog hide];
        UIAlertController*alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"deviceCommFailed", nil) preferredStyle:UIAlertControllerStyleAlert];

        UIAlertAction*ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}


- (IBAction)saveTapped:(id)sender {
    if([[SYCoreDataManager sharedInstance] getCurrentElans].count > 0){
        [self performSegueWithIdentifier:@"rooms" sender:nil];
    } else {
        UIAlertController*alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"selectElan", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction*ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

@end
