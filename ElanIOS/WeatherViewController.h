//
//  WeatherViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 05.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lVlhkost;
@property (weak, nonatomic) IBOutlet UIImageView *imgVitr;
@property (weak, nonatomic) IBOutlet UILabel *lVitr;
@property (weak, nonatomic) IBOutlet UILabel *lTlak;
@property (weak, nonatomic) IBOutlet UILabel *lTeplota;
@property (weak, nonatomic) IBOutlet UILabel *lPocitovaTeplota;

@end
