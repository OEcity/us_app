//
//  SYFavouritesViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 7/28/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYFavouritesViewController.h"
#import "AppDelegate.h"


@interface SYFavouritesViewController ()
@property (nonatomic, strong) NSFetchedResultsController* favourites;

@end

@implementation SYFavouritesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initFetchedResultsController];
    [_tableView reloadData];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.tableView setEditing:NO];
}

- (void)initLocalizableStrings {
    [_favouritesLabel setText:NSLocalizedString(@"favourite", nil)];
    [_bottomRightMenu setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
    [_bottomLeftMenu setTitle:NSLocalizedString(@"menu", nil) forState:UIControlStateNormal];

}

#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Device"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"favourite ==1"];
    [fetchRequest setPredicate:predicate];
    
    _favourites = [[NSFetchedResultsController alloc]
              initWithFetchRequest:fetchRequest
              managedObjectContext:context
              sectionNameKeyPath:nil
              cacheName:nil];
    _favourites.delegate = self;
    NSError *error;
    
    if (![_favourites performFetch:&error]) {
        NSLog(@"error fetching Devices: %@",[error description]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            //            self.emptyLabel.hidden = [_fetchedResultsController.fetchedObjects count] > 0;
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[_tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Device *selectedDevice = [self.favourites objectAtIndexPath:indexPath];
    UILabel * deviceName = (UILabel*)[cell viewWithTag:2];
    UIImageView * deviceIcon = (UIImageView*)[cell viewWithTag:1];
    
    [deviceName setTextAlignment:NSTextAlignmentLeft];
    //[deviceName setTextColor:[UIColor colorWithRed:1.0 green:1 blue:1 alpha:1]];
    UIFont* boldFont = [UIFont systemFontOfSize:17];
    while ([cell viewWithTag:201]!=nil){
        [[cell viewWithTag:201] removeFromSuperview];
    }
    [deviceName setFont:boldFont];
    
    
    deviceName.text = selectedDevice.label;
    [[cell viewWithTag:99] removeFromSuperview];
    
    // Setting different views for different type of devices
    deviceIcon.image = [selectedDevice imageOff];
    deviceIcon.highlightedImage = [selectedDevice imageOn];
    


}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {


    return [self.favourites.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"MyCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];

    return cell;

}

-(void)tableView:(UITableView*)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    UIView * view1 = [cell viewWithTag:2];

    [UIView animateWithDuration:0.2f animations:^{
        CGRect theFrame = view1.frame;
        theFrame.size.width -= 50;
        view1.frame = theFrame;
        [view1 layoutIfNeeded];
    }];
    [UIView commitAnimations];

}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Device *selectedDevice = [_favourites objectAtIndexPath:indexPath];

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_favourite", nil), selectedDevice.label] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
        alert.tag = indexPath.row;
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        Device *selectedDevice = [self.favourites objectAtIndexPath:[NSIndexPath indexPathForRow:alertView.tag inSection:0]];
        Device *databaseDevice = [[SYCoreDataManager sharedInstance] getDeviceWithID:selectedDevice.deviceID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];

        databaseDevice.favourite = [[NSNumber alloc] initWithBool:NO];
        [[SYCoreDataManager sharedInstance] saveContext];

    }
}

-(IBAction)nextStep:(id)sender{
    [self performSegueWithIdentifier:@"setFavourite" sender:self];

}
-(IBAction)back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (NSUInteger)supportedInterfaceOrientations
{

    return UIInterfaceOrientationMaskPortrait;
}

@end
