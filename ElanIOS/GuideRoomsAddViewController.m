//
//  GuideRoomsAddViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 28.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "GuideRoomsAddViewController.h"
#import "ResourceTiles.h"
#import "Elan.h"
#import "SYCoreDataManager.h"
#import "Constants.h"
#import "SYAPIManager.h"
#import "HUDWrapper.h"

@interface GuideRoomsAddViewController (){
    NSArray * roomTypes;
    NSString * myRoomtype;
    NSString* myRoomName;
    BOOL selected;
    Elan*selectedElan;
}
@property (nonatomic, retain) HUDWrapper *loaderDialog;
@property (nonatomic, strong) NSFetchedResultsController* elans;
@end

@implementation GuideRoomsAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"typesList" ofType:@"plist"]];
    roomTypes = [dict objectForKey:@"roomTypes"];
    [self initFetchedResultsController];
    
    if(_room != nil){
        myRoomtype = _room.type;
        myRoomName = _room.label;
        selectedElan = _room.elan;
        [_tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    
    selected = NO;
    
    [self.view layoutIfNeeded];
}

#pragma mark textField Delegate
-(void)textFieldDidEndEditing:(UITextField *)textField{
    myRoomName = textField.text;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return NO;
}

#pragma mark - UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView.tag != 1 && selected)
        return [_elans.fetchedObjects count]+1;
    if (tableView.tag != 1)
        return 1;
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1 && indexPath.row == 1 && selected){
        return [_elans.fetchedObjects count]*50+50;
    }
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        UITableViewCell * cell = nil;

        switch (indexPath.row) {
            case 0:{
                NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"enterName", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField *tf = (UITextField*)[[[cell contentView] subviews] firstObject];
                tf.delegate = self;
                tf.attributedPlaceholder = namePlaceHolder;
                if(myRoomName != nil){
                    tf.text = myRoomName;
                }
            }
                break;
            case 1:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.delegate = self;
                tbv.dataSource = self;
            }
                break;
            case 2:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"roomsCell"forIndexPath:indexPath];
                
                UICollectionView *collection = nil;
                for(UIView *view in [[cell contentView] subviews]){
                    if ([view isKindOfClass:[UICollectionView class]]){
                        collection = (UICollectionView*)view;
                    }
                }
                if(collection){
                    collection.delegate = self;
                    collection.dataSource = self;
                
                }
            }
                break;
            default:
                break;
        }
        
        return cell;
        
        
    } else {
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }

        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        if(selected){
            line.hidden = YES;
        } else {
            line.hidden = NO;
        }
        
        if(selected && indexPath.row>0){
            Elan*elan = [_elans.fetchedObjects objectAtIndex:indexPath.row-1];
            label.text = elan.label;
            if([elan.mac isEqualToString:selectedElan.mac]){
                [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            img.hidden = YES;
        } else {
            label.text = @"eLAN";
            if(selectedElan && !selected){
                label.text = selectedElan.label;
            }
        }
        
        if(indexPath.row == 0){
            img.hidden = NO;
            
            if(selected){
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            } else {
                img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
            }
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag != 1 && indexPath.row == 0 && !selected){
        selected = YES;
        
        tableView.layer.borderColor = USBlueColor.CGColor;
        tableView.layer.borderWidth = 1.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
        
    } else if (tableView.tag != 1 && indexPath.row == 0 && selected){
        selected = NO;
        
        tableView.layer.borderColor = nil;
        tableView.layer.borderWidth = 0.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
    } else if(tableView.tag != 1 && indexPath.row>0){
        selectedElan = [_elans.fetchedObjects objectAtIndex:indexPath.row-1];
    }
    
    if(indexPath.row == 0)
    [tableView reloadData];
    
}

#pragma mark - UICollectionViewDelegate
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   return [roomTypes count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"roomCell" forIndexPath:indexPath];
    
    
    NSString * roomType = [roomTypes objectAtIndex:indexPath.item];
    NSString * recourceName = [[ResourceTiles getDict] valueForKey:roomType];
    
    UIImage * iconImage = [UIImage imageNamed:[recourceName stringByAppendingString:@"_off"]];
    UIImage *onIconImage = [UIImage imageNamed:[recourceName stringByAppendingString:@"_on"]];
    if (iconImage==nil){
        iconImage =[UIImage imageNamed:@"jine_off"];
    }
    
    if (onIconImage==nil){
        onIconImage =[UIImage imageNamed:@"jine_on"];

    }
   
    UIImageView *img = [[[cell contentView] subviews]firstObject];
    
    if([roomType isEqualToString:myRoomtype]){
        [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        img.image = onIconImage;
    } else {
        img.image = iconImage;
    }
    
    return cell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * roomType = [roomTypes objectAtIndex:indexPath.item];
    NSLog(@"room type: %@", roomType);
    myRoomtype = roomType;
    
    [collectionView reloadData];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(IBAction)storeRoom:(id)sender{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell *cell = [_tableView cellForRowAtIndexPath:indexPath];
    UILabel *label = [[[cell contentView]subviews]firstObject];
    myRoomName = label.text;
    
    if ([myRoomName isEqualToString:@""]  ){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"nameError", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if ([myRoomtype isEqualToString:@""] || myRoomtype == nil ){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"deviceType", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    for(Room*room in [[SYCoreDataManager sharedInstance]getAllRooms]){
        NSLog(@"Room: '%@', myString:'%@'", room.label, myRoomName);
        if([[room.label stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:[myRoomName stringByReplacingOccurrencesOfString:@" " withString:@""]]){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"duplicateNameRoom",nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
    }
    
    if(selectedElan == nil){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"selectElan", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;

    }
    
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", @"")];
    
    
    NSDictionary * elanJson = [self createJSON];
    [[SYAPIManager sharedInstance] updateRoomWithDictionary:elanJson toElan:selectedElan success:^(AFHTTPRequestOperation *operation, id response) {
        [_loaderDialog hide];
        [self performSegueWithIdentifier:@"roomSaved" sender:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [_loaderDialog hide];
        NSLog(@"Error: %@", [error localizedDescription]);
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"cannotAddRoom", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }];
    
    
}

-(NSDictionary*)createJSON{
    NSDictionary *RoomInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                              myRoomName,@"label",
                              myRoomtype,@"type",
                              nil];
    

    NSDictionary *result = [NSDictionary dictionaryWithObjectsAndKeys:
                            RoomInfo , @"room info",
                            [[NSDictionary alloc] init], @"devices",
                            nil];
    
    if (self.room!=nil){
        
        NSArray* devicesArray = [_room.devicesInRoom allObjects];// [CoreDataManager getDevicesInRoom:self.room.name];
        NSMutableDictionary * devices = [[NSMutableDictionary alloc] init];
        for (DeviceInRoom * dev in devicesArray){
            [devices setValue:[NSDictionary dictionaryWithObjectsAndKeys:[[NSNull alloc] init] , @"coordinates",nil ] forKey:dev.device.deviceID];
        }
        result = [NSDictionary dictionaryWithObjectsAndKeys:
                  self.room.roomID, @"id",
                  RoomInfo , @"room info",
                  //                      floorplan,@"floorplan",
                  devices, @"devices",
                  nil];
        
    }


    return result;
}

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Elan"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    
    _elans = [[NSFetchedResultsController alloc]
              initWithFetchRequest:fetchRequest
              managedObjectContext:context
              sectionNameKeyPath:nil
              cacheName:nil];
    _elans.delegate = self;
    NSError *error;
    
    if (![_elans performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}


@end
