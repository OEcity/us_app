//
//  OneWeekDeviceScheduleViewController.m
//  iHC-MIRF
//
//  Created by admin on 10.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "OneWeekDeviceScheduleViewController.h"
#import "DeviceSchedule.h"
#import "DeviceScheduleDay.h"
//#import "DeviceTimeScheduleView.h"

#import "OneDayDeviceTimeScheduleView.h"
#import "OneDayDeviceTimeAdjustView.h"

#import "HUDWrapper.h"
#import "SYAPIManager.h"

#import "AddDeviceModeViewController.h"



@interface OneWeekDeviceScheduleViewController()

@property (nonatomic, retain) HUDWrapper * loaderDialog;

//Frames for showing temperature maps
@property (weak, nonatomic) IBOutlet OneDayDeviceTimeScheduleView *mondayView;
@property (weak, nonatomic) IBOutlet OneDayDeviceTimeScheduleView *tuesdayView;
@property (weak, nonatomic) IBOutlet OneDayDeviceTimeScheduleView *wednesdayView;
@property (weak, nonatomic) IBOutlet OneDayDeviceTimeScheduleView *thursdayView;
@property (weak, nonatomic) IBOutlet OneDayDeviceTimeScheduleView *fridayView;
@property (weak, nonatomic) IBOutlet OneDayDeviceTimeScheduleView *saturdayView;
@property (weak, nonatomic) IBOutlet OneDayDeviceTimeScheduleView *sundayView;

//Place for editable temperature map and copyTo buttons
@property (weak, nonatomic) IBOutlet UIView *setterView;
//Editable temperature map view

//Edit
@property (weak, nonatomic) IBOutlet OneDayDeviceTimeAdjustView *setterScheduleView;
@property (weak, nonatomic) IBOutlet UILabel *setterViewDayLabel;
@property (weak, nonatomic) IBOutlet UIView *saveButtonView;


@property (weak, nonatomic) IBOutlet UIButton *mondayCopyTo;
@property (weak, nonatomic) IBOutlet UIButton *tuesdayCopyTO;
@property (weak, nonatomic) IBOutlet UIButton *wednesDayCopyTo;
@property (weak, nonatomic) IBOutlet UIButton *thursdayCopyTO;
@property (weak, nonatomic) IBOutlet UIButton *fridayCopyTo;
@property (weak, nonatomic) IBOutlet UIButton *saturdayCopyTo;
@property (weak, nonatomic) IBOutlet UIButton *sundayCopyTo;

//local variables
@property (strong, nonatomic) NSArray * arrayDaysView;
@property (strong, nonatomic) NSArray * buttonsCopyToArray;

@property (strong, nonatomic) NSArray * colorArray;
@property (strong, nonatomic) NSArray * brightnessArray;



//model variables
@property (nonatomic, retain) DeviceSchedule * deviceSchedule; //=tempSchedule
@property (nonatomic, retain) DeviceScheduleDay * mDay;
//@property (nonatomic, retain) DeviceTimeScheduleView * timeScheduleView;

@property (strong, nonatomic) NSString *type;

@property BOOL shouldShowImageUpDown;
@property BOOL shouldShowPercentage;
@property BOOL shouldShowZero;


@property NSInteger testModePos;
@property NSInteger testdayInWeek;
@property (strong, nonatomic) TempDayMode *testTempMode;



@end


@implementation OneWeekDeviceScheduleViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    _deviceSchedule = [[DeviceSchedule alloc] initWithType:[_schedule objectForKey:@"type"]];
    if ([_schedule objectForKey:@"schedule"]!=nil)
        _deviceSchedule.days =  [self getDays:_schedule];
    if ([_schedule objectForKey:@"modes"]!=nil)
        _deviceSchedule.modes = [self getModes:_schedule];
    [_deviceSchedule setServerId:[_schedule objectForKey:@"id"]];
    NSLog(@"ID: %@", [_schedule objectForKey:@"id"]);
    NSLog(@"SCHEDULE: %@", _schedule);
    [_deviceSchedule setHysteresis:[_schedule objectForKey:@"hysteresis"]];
    [_deviceSchedule setName:[_schedule objectForKey:@"label"]];
    
    _type = _deviceSchedule.type;
    
    
    _arrayDaysView = [NSArray arrayWithObjects:_mondayView, _tuesdayView, _wednesdayView, _thursdayView, _fridayView, _saturdayView, _sundayView, nil];
    
    _buttonsCopyToArray = [NSArray arrayWithObjects:_mondayCopyTo, _tuesdayCopyTO, _wednesDayCopyTo, _thursdayCopyTO, _fridayCopyTo,_saturdayCopyTo,_sundayCopyTo, nil];
    
    
    _shouldShowImageUpDown = NO;
    _shouldShowPercentage = NO;
    
    _setterScheduleView.dataSource = self;
    _setterScheduleView.delegate = self;
    [self createColorArray];
    
    [self initDaysView];
    
}

-(void)initDaysView{
    int i = 0;
    
    //Až bude view
    for (OneDayDeviceTimeScheduleView *view in _arrayDaysView){
        _mDay = [_deviceSchedule getDayByPosition:i];
        view.dataSource = self;
        [view repaintDay:_mDay];
        
        i++;
    }
    
    [_setterView setHidden:YES];
    [_saveButtonView setHidden:YES];
}

-(void)createColorArray{
    
    UIColor *color0 = nil;
    UIColor *color1 = nil;
    UIColor *color2 = nil;
    UIColor *color3 = nil;
    UIColor *color4 = nil;
    UIColor *color5 = nil;
    
    
    if ([[_schedule objectForKey:@"type"] isEqualToString:@"rgb"]){
        float modeR = [[[[_schedule objectForKey:@"modes"] objectForKey:@"1"] objectForKey:@"R"] floatValue];
        float modeG = [[[[_schedule objectForKey:@"modes"] objectForKey:@"1"] objectForKey:@"G"] floatValue];
        float modeB = [[[[_schedule objectForKey:@"modes"] objectForKey:@"1"] objectForKey:@"B"] floatValue];
        //ta je tu na tvrdo - ta má být šedá
        color1 = [UIColor colorWithRed:109.0f/255.0f green:110.0f/255.0f blue:113.0f/255.0f alpha:1.0f];
        
        modeR = [[[[_schedule objectForKey:@"modes"] objectForKey:@"2"] objectForKey:@"R"] floatValue];
        modeG = [[[[_schedule objectForKey:@"modes"] objectForKey:@"2"] objectForKey:@"G"] floatValue];
        modeB = [[[[_schedule objectForKey:@"modes"] objectForKey:@"2"] objectForKey:@"B"] floatValue];
        
        color2 = [UIColor colorWithRed:modeR/255.0f green:modeG/255.0f blue:modeB/255.0f alpha:1.0f];
        
        modeR = [[[[_schedule objectForKey:@"modes"] objectForKey:@"3"] objectForKey:@"R"] floatValue];
        modeG = [[[[_schedule objectForKey:@"modes"] objectForKey:@"3"] objectForKey:@"G"] floatValue];
        modeB = [[[[_schedule objectForKey:@"modes"] objectForKey:@"3"] objectForKey:@"B"] floatValue];
        
        color3 = [UIColor colorWithRed:modeR/255.0f green:modeG/255.0f blue:modeB/255.0f alpha:1.0f];
        
        modeR = [[[[_schedule objectForKey:@"modes"] objectForKey:@"4"] objectForKey:@"R"] floatValue];
        modeG = [[[[_schedule objectForKey:@"modes"] objectForKey:@"4"] objectForKey:@"G"] floatValue];
        modeB = [[[[_schedule objectForKey:@"modes"] objectForKey:@"4"] objectForKey:@"B"] floatValue];
        
        color4 = [UIColor colorWithRed:modeR/255.0f green:modeG/255.0f blue:modeB/255.0f alpha:1.0f];
        
        modeR = [[[[_schedule objectForKey:@"modes"] objectForKey:@"5"] objectForKey:@"R"] floatValue];
        modeG = [[[[_schedule objectForKey:@"modes"] objectForKey:@"5"] objectForKey:@"G"] floatValue];
        modeB = [[[[_schedule objectForKey:@"modes"] objectForKey:@"5"] objectForKey:@"B"] floatValue];
        
        color5 = [UIColor colorWithRed:modeR/255.0f green:modeG/255.0f blue:modeB/255.0f alpha:1.0f];
        
        _brightnessArray = [NSArray arrayWithObjects:
                            @"",
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"1"] objectForKey:@"brightness"] stringValue] stringByAppendingString:@" %"],
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"2"] objectForKey:@"brightness"] stringValue] stringByAppendingString:@" %"],
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"3"] objectForKey:@"brightness"] stringValue] stringByAppendingString:@" %"],
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"4"] objectForKey:@"brightness"] stringValue] stringByAppendingString:@" %"],
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"5"] objectForKey:@"brightness"] stringValue] stringByAppendingString:@" %"],
                            nil];
        
        
        _shouldShowPercentage = YES;
        _shouldShowZero = NO;
        
    }
    else if ([[_schedule objectForKey:@"type"] isEqualToString:@"white"]){
        
        /*color1 = [UIColor colorWithRed:109.0f/255.0f green:110.0f/255.0f blue:113.0f/255.0f alpha:1.0f];
        color2 = [UIColor colorWithRed:109.0f/255.0f green:110.0f/255.0f blue:113.0f/255.0f alpha:1.0f];
        color3 = [UIColor colorWithRed:109.0f/255.0f green:110.0f/255.0f blue:113.0f/255.0f alpha:1.0f];
        color4 = [UIColor colorWithRed:109.0f/255.0f green:110.0f/255.0f blue:113.0f/255.0f alpha:1.0f];
        color5 = [UIColor colorWithRed:109.0f/255.0f green:110.0f/255.0f blue:113.0f/255.0f alpha:1.0f];*/
        
        float modeR = [[[[_schedule objectForKey:@"modes"] objectForKey:@"1"] objectForKey:@"R"] floatValue];
        float modeG = [[[[_schedule objectForKey:@"modes"] objectForKey:@"1"] objectForKey:@"G"] floatValue];
        float modeB = [[[[_schedule objectForKey:@"modes"] objectForKey:@"1"] objectForKey:@"B"] floatValue];
        //ta je tu na tvrdo - ta má být šedá
        color1 = [UIColor colorWithRed:109.0f/255.0f green:110.0f/255.0f blue:113.0f/255.0f alpha:1.0f];
        
        modeR = [[[[_schedule objectForKey:@"modes"] objectForKey:@"2"] objectForKey:@"R"] floatValue];
        modeG = [[[[_schedule objectForKey:@"modes"] objectForKey:@"2"] objectForKey:@"G"] floatValue];
        modeB = [[[[_schedule objectForKey:@"modes"] objectForKey:@"2"] objectForKey:@"B"] floatValue];
        
        color2 = [UIColor colorWithRed:modeR/255.0f green:modeG/255.0f blue:modeB/255.0f alpha:1.0f];
        
        modeR = [[[[_schedule objectForKey:@"modes"] objectForKey:@"3"] objectForKey:@"R"] floatValue];
        modeG = [[[[_schedule objectForKey:@"modes"] objectForKey:@"3"] objectForKey:@"G"] floatValue];
        modeB = [[[[_schedule objectForKey:@"modes"] objectForKey:@"3"] objectForKey:@"B"] floatValue];
        
        color3 = [UIColor colorWithRed:modeR/255.0f green:modeG/255.0f blue:modeB/255.0f alpha:1.0f];
        
        modeR = [[[[_schedule objectForKey:@"modes"] objectForKey:@"4"] objectForKey:@"R"] floatValue];
        modeG = [[[[_schedule objectForKey:@"modes"] objectForKey:@"4"] objectForKey:@"G"] floatValue];
        modeB = [[[[_schedule objectForKey:@"modes"] objectForKey:@"4"] objectForKey:@"B"] floatValue];
        
        color4 = [UIColor colorWithRed:modeR/255.0f green:modeG/255.0f blue:modeB/255.0f alpha:1.0f];
        
        modeR = [[[[_schedule objectForKey:@"modes"] objectForKey:@"5"] objectForKey:@"R"] floatValue];
        modeG = [[[[_schedule objectForKey:@"modes"] objectForKey:@"5"] objectForKey:@"G"] floatValue];
        modeB = [[[[_schedule objectForKey:@"modes"] objectForKey:@"5"] objectForKey:@"B"] floatValue];
        
        color5 = [UIColor colorWithRed:modeR/255.0f green:modeG/255.0f blue:modeB/255.0f alpha:1.0f];
        
        
        
        
        _brightnessArray = [NSArray arrayWithObjects:
                            @"",
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"1"] objectForKey:@"brightness"] stringValue] stringByAppendingString:@" %"],
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"2"] objectForKey:@"brightness"] stringValue] stringByAppendingString:@" %"],
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"3"] objectForKey:@"brightness"] stringValue] stringByAppendingString:@" %"],
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"4"] objectForKey:@"brightness"] stringValue] stringByAppendingString:@" %"],
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"5"] objectForKey:@"brightness"] stringValue] stringByAppendingString:@" %"],
                            nil];
        
        
        _shouldShowPercentage = YES;
        _shouldShowZero = NO;
        
        
    }
    else if ([[_schedule objectForKey:@"type"] isEqualToString:@"switch"]){
        color1 = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        
        _brightnessArray = [NSArray arrayWithObjects:@"OFF", @"ON", nil];
        _shouldShowZero = YES;
        
    }
    else if ([[_schedule objectForKey:@"type"] isEqualToString:@"diming"]){
        //šedá alternativa
        /*
        float value = [[[[_schedule objectForKey:@"modes"] objectForKey:@"1"] objectForKey:@"value"] floatValue]/100.0f;
        
        
        color1 = [UIColor colorWithRed:((146.0f * value)+109)/255.0f green:((145.0f * value)+110)/255.0f blue:((142.0f * value)+113)/255.0f alpha:1.0f];
        
        value = [[[[_schedule objectForKey:@"modes"] objectForKey:@"2"] objectForKey:@"value"] floatValue]/100.0f;
        
        color2 = [UIColor colorWithRed:((146.0f * value)+109)/255.0f green:((145.0f * value)+110)/255.0f blue:((142.0f * value)+113)/255.0f alpha:1.0f];
        
        value = [[[[_schedule objectForKey:@"modes"] objectForKey:@"3"] objectForKey:@"value"] floatValue]/100.0f;
        
        color3 = [UIColor colorWithRed:((146.0f * value)+109)/255.0f green:((145.0f * value)+110)/255.0f blue:((142.0f * value)+113)/255.0f alpha:1.0f];
        
        value = [[[[_schedule objectForKey:@"modes"] objectForKey:@"4"] objectForKey:@"value"] floatValue]/100.0f;
        
        color4 = [UIColor colorWithRed:((146.0f * value)+109)/255.0f green:((145.0f * value)+110)/255.0f blue:((142.0f * value)+113)/255.0f alpha:1.0f];
        
        value = [[[[_schedule objectForKey:@"modes"] objectForKey:@"5"] objectForKey:@"value"] floatValue]/100.0f;
        
        color5 = [UIColor colorWithRed:((146.0f * value)+109)/255.0f green:((145.0f * value)+110)/255.0f blue:((142.0f * value)+113)/255.0f alpha:1.0f];
        */
        //modrá alternativa
        
        _brightnessArray = [NSArray arrayWithObjects:
                            @"",
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"1"] objectForKey:@"value"] stringValue] stringByAppendingString:@" %"],
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"2"] objectForKey:@"value"] stringValue] stringByAppendingString:@" %"],
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"3"] objectForKey:@"value"] stringValue] stringByAppendingString:@" %"],
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"4"] objectForKey:@"value"] stringValue] stringByAppendingString:@" %"],
                            [[[[[_schedule objectForKey:@"modes"] objectForKey:@"5"] objectForKey:@"value"] stringValue] stringByAppendingString:@" %"],
                            nil];

        float value = [[[[_schedule objectForKey:@"modes"] objectForKey:@"1"] objectForKey:@"value"] floatValue]/100.0f;
        
        
        color1 = [UIColor colorWithRed:(255.0f * value)/255.0f green:((63.0f * value)+192)/255.0f blue:((12.0f * value)+243)/255.0f alpha:1.0f];
        
        value = [[[[_schedule objectForKey:@"modes"] objectForKey:@"2"] objectForKey:@"value"] floatValue]/100.0f;
        
        color2 = [UIColor colorWithRed:(255.0f * value)/255.0f green:((63.0f * value)+192)/255.0f blue:((12.0f * value)+243)/255.0f alpha:1.0f];
        
        value = [[[[_schedule objectForKey:@"modes"] objectForKey:@"3"] objectForKey:@"value"] floatValue]/100.0f;
        
        color3 = [UIColor colorWithRed:(255.0f * value)/255.0f green:((63.0f * value)+192)/255.0f blue:((12.0f * value)+243)/255.0f alpha:1.0f];
        
        value = [[[[_schedule objectForKey:@"modes"] objectForKey:@"4"] objectForKey:@"value"] floatValue]/100.0f;
        
        color4 = [UIColor colorWithRed:(255.0f * value)/255.0f green:((63.0f * value)+192)/255.0f blue:((12.0f * value)+243)/255.0f alpha:1.0f];
        
        value = [[[[_schedule objectForKey:@"modes"] objectForKey:@"5"] objectForKey:@"value"] floatValue]/100.0f;
        
        color5 = [UIColor colorWithRed:(255.0f * value)/255.0f green:((63.0f * value)+192)/255.0f blue:((12.0f * value)+243)/255.0f alpha:1.0f];
        _shouldShowPercentage = YES;
        _shouldShowZero = NO;
        
    }
    else if ([[_schedule objectForKey:@"type"] isEqualToString:@"shutters"]){
        color1 = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        color2 = [UIColor colorWithRed:109.0f/255.0f green:110.0f/255.0f blue:113.0f/255.0f alpha:1.0f];
        
        _brightnessArray = [NSArray arrayWithObjects:@"",@"UP", @"DOWN", nil];
        _shouldShowImageUpDown = YES;
        _shouldShowZero = NO;
    }
    //color0 je opravdu použita pouze dim
    color0 = [UIColor colorWithRed:109.0f/255.0f green:110.0f/255.0f blue:113.0f/255.0f alpha:1.0f];
    _colorArray = [NSArray arrayWithObjects: color0, color1, color2, color3, color4, color5, nil];
    
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"addDeviceMode"]){
        if ([[segue.destinationViewController class]hash] ==  [[AddDeviceModeViewController class] hash]){
            
            AddDeviceModeViewController *vc =  segue.destinationViewController;
            vc.modePos = -1;
            vc.delegate = self;
            vc.temperatures = [_brightnessArray copy];
            vc.shouldShowZero = _shouldShowZero;
            if (([_type isEqualToString:@"rgb"]) || ([_type isEqualToString:@"white"])){
                vc.shouldShowColor = YES;
                vc.colors = [_colorArray copy];
            }
            else{
                vc.shouldShowColor = NO;
                vc.colors = nil;
            }
            
           
        }
    }
    else if ([segue.identifier  isEqual: @"editDeviceMode"]){
        
     
        EditDeviceModeViewController *vc =  segue.destinationViewController;
        vc.day = _testdayInWeek;
        vc.modePos = _testModePos;
        vc.tempDayMode = _testTempMode;
        vc.delegate = self;
        vc.shouldShowZero = _shouldShowZero;
        
        vc.temperatures = [_brightnessArray copy];
        if (([_type isEqualToString:@"rgb"]) || ([_type isEqualToString:@"white"])){
            vc.shouldShowColor = YES;
            vc.colors = [_colorArray copy];
        }
        else{
            vc.shouldShowColor = NO;
            vc.colors = nil;
        }

       
    }

    
}


- (IBAction)adjustButtonPressed:(id)sender {
    
    [self writeData];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


- (IBAction)saveButtonPressed:(id)sender {
    [_setterView setHidden:YES];
    [_saveButtonView setHidden:YES];
    
    int i = 0;
    for(UIButton *copyTo in _buttonsCopyToArray){
        if(copyTo.state == UIControlStateDisabled){
            
            [_deviceSchedule setDayOnPosition: _setterScheduleView.mDay position:i];
        }
        else if(copyTo.state == UIControlStateSelected){
            DeviceScheduleDay *anotherDay = [_setterScheduleView.mDay copy];
            switch (i) {
                case 0:
                    anotherDay.id = @"monday";
                    break;
                case 1:
                    anotherDay.id = @"tuesday";
                    break;
                case 2:
                    anotherDay.id = @"wednesday";
                    break;
                case 3:
                    anotherDay.id = @"thursday";
                    break;
                case 4:
                    anotherDay.id = @"friday";
                    break;
                case 5:
                    anotherDay.id = @"saturday";
                    break;
                case 6:
                    anotherDay.id = @"sunday";
                    break;
                default: break;
            }
            [_deviceSchedule setDayOnPosition:anotherDay position:i];
        }
        i++;
    }
    
    
    
    i = 0;
    
    
    for (OneDayDeviceTimeScheduleView *view in _arrayDaysView){
        _mDay = [_deviceSchedule getDayByPosition:i];
        [view repaintDay:_mDay];
        i++;
    }
    
    
    [self enableAllCopyToButtons];
    [self setUnselectedAllCopyToButton];
    
    
}

-(void) writeData{
    
    NSMutableDictionary * root = [[NSMutableDictionary alloc] init];
    
    if (_deviceSchedule.serverId != nil){
        [root setObject:_deviceSchedule.serverId forKey:@"id"];
    }
    
    [root setObject:_deviceSchedule.type forKey:@"type"];
    [root setObject:_deviceSchedule.name forKey:@"label"];
    
    
    NSMutableDictionary * modes = [[NSMutableDictionary alloc] init];
    modes = [_schedule objectForKey:@"modes"];
    [root setObject:modes forKey:@"modes"];
    
    NSMutableDictionary * devices = [[NSMutableDictionary alloc] init];
    devices = [_schedule objectForKey:@"devices"];
    [root setObject:devices forKey:@"devices"];
    
    NSMutableArray* modesA;
    NSMutableDictionary* temp = [[NSMutableDictionary alloc] init];
    for (DeviceScheduleDay* day in _deviceSchedule.days) {
        modesA = [[NSMutableArray alloc] init];
        
        for (TempDayMode *mode in day.modes) {
            NSDictionary * modeObj = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithInt:[mode.endTime intValue]-[mode.startTime intValue]], @"duration",
                                      mode.mode , @"mode", nil];
            [modesA addObject:modeObj];
            
        }
        [temp setObject:modesA forKey:day.id];
        
    }
    [root setObject:temp forKey:@"schedule"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:root
                                                       options:(NSJSONWritingOptions)  (NSJSONWritingPrettyPrinted)
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        
    } else {
        NSLog(@"%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    }
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    [_loaderDialog showWithLabel:[NSString stringWithFormat:@"%@\n%@", @"ukládání časového plánu", NSLocalizedString(@"time_schedule_dialog_wait_tile", nil) ]];
    
    
    [[SYAPIManager sharedInstance] postDeviceScheduleWithDictionary:root success:^(AFHTTPRequestOperation * operation , id response){
        [_loaderDialog hide];
        NSLog(@"updated succesfully");
       
        
        
        [self.navigationController popToViewController:[[self.navigationController viewControllers] objectAtIndex:[[self.navigationController viewControllers]count]-2] animated:YES];
    }
                                                            failure:^(AFHTTPRequestOperation * operation , NSError * error){
                                                                [_loaderDialog hide];
                                                                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                                                                                  message:NSLocalizedString(@"temp_schedule_failed_save", nil)
                                                                                                                 delegate:nil
                                                                                                        cancelButtonTitle:@"OK"
                                                                                                        otherButtonTitles:nil];
                                                                [message show];
                                                                
                                                                NSLog(@"update err");
                                                            }];
    
}

-(NSMutableArray*)getModes:(NSDictionary*)dictionary{
    
    TempScheduleMode* mode;
    NSMutableArray * modes = [[NSMutableArray alloc] init];
    NSDictionary * modesDictionary = [dictionary objectForKey:@"modes"];
    for (NSString* fieldName in modesDictionary) {
        
        NSDictionary * object = [modesDictionary objectForKey:fieldName];
        mode = [[TempScheduleMode alloc] init];
        mode.max = [[NSNumber alloc] initWithInt:35];
        
        //mode.min =  [object valueForKey:@"min"];
        
        mode.id = [[NSNumber alloc] initWithInt:[fieldName intValue]] ;
        [modes addObject:mode];
    }
    return modes;
}


-(NSMutableArray*)getDays:(NSDictionary*)dictionary{
    DeviceScheduleDay * day;
    TempDayMode * dayMode;
    NSMutableArray * days = [[NSMutableArray alloc] init];
    for (NSString * dayName in [dictionary objectForKey:@"schedule"]) {
        
        day = [[DeviceScheduleDay alloc] init];
        day.id = dayName;
        
        day.modes = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dayObject in [[dictionary objectForKey:@"schedule"] objectForKey:dayName]) {
            
            dayMode = [[TempDayMode alloc] init];
            dayMode.duration = [dayObject valueForKey:@"duration"];
            dayMode.mode = [dayObject valueForKey:@"mode"];
            dayMode.type = [dictionary objectForKey:@"type"];
            
            [day.modes addObject:dayMode];
        }
        
        [day setupModes];
        [days addObject:day];
    }
    return days;
    
}

- (IBAction)mondayRiseUp:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_deviceSchedule getDayByPosition:0];
        DeviceScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:0];
        [_setterViewDayLabel setText:@"MON"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:0];
        //_setterScheduleView.mModePosition = 0;
        
    }
}
- (IBAction)tuesdayRiseUP:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_deviceSchedule getDayByPosition:1];
        DeviceScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:1];
        [_setterViewDayLabel setText:@"TUE"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:1];
        //_setterScheduleView.mModePosition = 0;
    }
    
}
- (IBAction)wednesdayRiseUp:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_deviceSchedule getDayByPosition:2];
        DeviceScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:2];
        [_setterViewDayLabel setText:@"WED"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:2];
        //_setterScheduleView.mModePosition = 0;
    }
    
}
- (IBAction)thursdayRiseUp:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_deviceSchedule getDayByPosition:3];
        DeviceScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:3];
        [_setterViewDayLabel setText:@"THU"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:3];
        //_setterScheduleView.mModePosition = 0;
    }
    
}
- (IBAction)fridayRiseUp:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_deviceSchedule getDayByPosition:4];
        DeviceScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:4];
        [_setterViewDayLabel setText:@"FRI"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:4];
        //_setterScheduleView.mModePosition = 0;
    }
    
}
- (IBAction)saturdayRiseUp:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_deviceSchedule getDayByPosition:5];
        DeviceScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:5];
        [_setterViewDayLabel setText:@"SAT"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:5];
        //_setterScheduleView.mModePosition = 0;
    }
    
}
- (IBAction)sundayRiseUp:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_deviceSchedule getDayByPosition:6];
        DeviceScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:6];
        [_setterViewDayLabel setText:@"SUN"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:6];
        //_setterScheduleView.mModePosition = 0;
    }
    
}
- (IBAction)dismissSetterView:(id)sender {
    [_setterView setHidden:YES];
    [_saveButtonView setHidden:YES];
    
    int i = 0;
    for (OneDayDeviceTimeScheduleView *view in _arrayDaysView){
        _mDay = [_deviceSchedule getDayByPosition:i];
        [view repaintDay:_mDay];
        i++;
    }
    [self enableAllCopyToButtons];
    [self setUnselectedAllCopyToButton];
    
    //NOW ALL ENABLED, and ZERO STATE and ARRAY ZERO state
}

-(void)enableAllCopyToButtons{
    for (UIButton *button in _buttonsCopyToArray){
        [button setEnabled:YES];
    }
}
-(void)disableOneCopyToButton:(NSUInteger)index{
    UIButton *button = [_buttonsCopyToArray objectAtIndex:index];
    //[button setSelected:YES];
    [button setEnabled:NO];
    
    
}
-(void)setUnselectedAllCopyToButton{
    for (UIButton *button in _buttonsCopyToArray){
        [button setSelected:NO];
    }
}



- (IBAction)copyTo:(UIButton *)sender {
    sender.selected = ![sender isSelected];
}


#pragma mark OneDayDeviceTimeScheduleViewDataSource methods

-(NSString *)getPercentOfColor:(NSInteger)index{
    return [_brightnessArray objectAtIndex:index];
}

-(UIColor *)getColorForPosintion:(NSInteger)position{
    return [_colorArray objectAtIndex:position];
}


-(BOOL)shouldShowImage{
   
    return _shouldShowImageUpDown;
}

-(BOOL)shouldShowPercents{
    return _shouldShowPercentage;
}


#pragma mark AddDeviceModeDelegate methods

-(void)addDeviceModeToday:(NSInteger)day comesFromLTouch:(BOOL)edit tempDayMode:(TempDayMode *)tempdayMode position:(NSInteger)position erase:(BOOL)erase{
    
    
    if(edit){
        if (!erase){
            [_setterScheduleView.mDay editMode:tempdayMode modePosition:(int)position];
            
        }
        else{
            [_setterScheduleView.mDay deleteMode:position type:_type];
        }
        [_setterScheduleView repaintContent];
        
    }
    else{
        if (!erase){
            if(day>=0 && day<7){
                DeviceScheduleDay* deviceScheduleDay = [_deviceSchedule getDayByPosition:(int)day];
                [deviceScheduleDay editMode:tempdayMode modePosition:(int)position];
            }
        }
        else{
            if(day>=0 && day<7){
                DeviceScheduleDay* deviceScheduleDay = [_deviceSchedule getDayByPosition:(int)day];
                [deviceScheduleDay deleteMode:position type:_type];
                
            }
            
        }
        int i = 0;
        for (OneDayDeviceTimeScheduleView *view in _arrayDaysView){
            _mDay = [_deviceSchedule getDayByPosition:i];
            [view repaintDay:_mDay];
            i++;
        }
    }
    
}

#pragma mark OneDayDeviceTimeAdjustViewDelegate methods

-(void)putEditingOnScreen:(TempDayMode*)mode modePos:(int)modePos dayInWeek:(int)day{
    
    if (self.isViewLoaded && self.view.window) {
        
        _testModePos = modePos;
        _testdayInWeek = day;
        _testTempMode = mode;
        [self performSegueWithIdentifier:@"editDeviceMode" sender:nil];
        


    }
}

@end
