//
//  HeatSettingsViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 24.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeatTimeSchedule.h"
#import "HeatCoolArea.h"
@interface HeatSettingsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) NSFetchedResultsController* schedules;
@property (nonatomic, strong) NSFetchedResultsController* HCAs;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *addNewButton;

@end
