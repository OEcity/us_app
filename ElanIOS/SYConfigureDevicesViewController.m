//
//  ConfigureDevicesViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/18/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYConfigureDevicesViewController.h"
#import "Device.h"
#import "SYCoreDataManager.h"
#import "AppDelegate.h"
#import "SYAPIManager.h"
#import "Constants.h"
#import "AddDeviceViewController.h"
#import "ElementsTableViewCell.h"
#import "DeviceSchedulesTableViewCell.h"
#import "SYDataLoader.h"

@interface SYConfigureDevicesViewController (){
    BOOL elementsSelected;
    BOOL schedulesSelected;
    
    UITableView*elementsTBV;
    UITableView*schedulesTBV;
}

@property (nonatomic, strong) NSFetchedResultsController* devices;
@property (nonatomic, strong) NSFetchedResultsController* schedules;

@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbvHeight;

@end

@implementation SYConfigureDevicesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.emptyLabel.text = NSLocalizedString(@"noDevices", nil);
    elementsSelected = schedulesSelected = NO;
    [self initFetchedResultsController];
    
    _tbvHeight.constant = 2*55;
    _addButton.hidden = YES;
 }


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    if(indexPath.row == 0){
        return NO;
    } else {
        return YES;
    }
    
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        if(tableView == elementsTBV){
            NSString* name = ((Device*)[_devices objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section]]).label;
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_device", nil), name] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
            alert.tag = indexPath.row-1;
            [alert show];
        } else {
            NSString* name = ((DeviceTimeScheduleClass*)[_schedules objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row-1 inSection:0]]).label;
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_device", nil), name] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
            alert.tag = 200 +indexPath.row-1;
            [alert show];
        }


    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        if(alertView.tag < 199){
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
            Device* device = ((Device*)[_devices objectAtIndexPath:indexPath]);
            [[SYAPIManager sharedInstance] deleteDevice:device success:^(AFHTTPRequestOperation * operation, id resultObject)
             {
                 
                 Device * dev = [_devices objectAtIndexPath:indexPath];
                 dev = [[SYCoreDataManager sharedInstance] getDeviceWithID:dev.deviceID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
                 [[SYCoreDataManager sharedInstance] deleteManagedObject:dev];
                 
             } failure:^(AFHTTPRequestOperation * operation, NSError * error)
             {
                 NSLog(@"cannot delete %ld", (long)error.code);
                 UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"cannotDeleteDevice"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];
                 
             }];
        } else {
            NSIndexPath * indexPath = [NSIndexPath indexPathForRow:alertView.tag-200 inSection:0];
            DeviceTimeScheduleClass* schedule = ((DeviceTimeScheduleClass*)[_schedules objectAtIndexPath:indexPath]);
            
            
            [[SYAPIManager sharedInstance] deleteDeviceScheduleWithId:schedule.devicetimescheduleclassID fromElan:schedule.elan success:^(AFHTTPRequestOperation *operation, id response) {
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                
            }];
            
        }

    }
}

#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Device"];
    NSFetchRequest *schedulesFetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"DeviceTimeScheduleClass"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    [fetchRequest setIncludesSubentities:NO];
    [schedulesFetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.address != nil AND self.productType != 'IR'"];
    [fetchRequest setPredicate:predicate];
    
    _schedules = [[NSFetchedResultsController alloc]
                  initWithFetchRequest:schedulesFetchRequest
                  managedObjectContext:context
                  sectionNameKeyPath:nil
                  cacheName:nil];
    _schedules.delegate = self;
    
    _devices = [[NSFetchedResultsController alloc]
                               initWithFetchRequest:fetchRequest
                               managedObjectContext:context
                               sectionNameKeyPath:nil
                               cacheName:nil];
    _devices.delegate = self;
    NSError *error;
    
    if (![_devices performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
    
    if (![_schedules performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
    }

#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    if(controller == _schedules){
        [schedulesTBV beginUpdates];
    }
    
    if(controller == _devices){
        [elementsTBV beginUpdates];
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    newIndexPath = [NSIndexPath indexPathForRow:newIndexPath.row+1 inSection:newIndexPath.section];
    indexPath = [NSIndexPath indexPathForRow:indexPath.row+1 inSection:indexPath.section];
    
    UITableView *tableView = nil;
    if(controller == _schedules){
        tableView = schedulesTBV;
    }

    if(controller == _devices){
        tableView = elementsTBV;
    }
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if(controller == _schedules){
        [schedulesTBV endUpdates];
    }
    
    if(controller == _devices){
        [elementsTBV endUpdates];
    }
    
    [_tableView beginUpdates];
    [_tableView endUpdates];
}


#pragma mark UITableView DataSource methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == elementsTBV && elementsSelected){
        return [[_devices fetchedObjects]count]+1;
    }
    if(tableView == schedulesTBV && schedulesSelected){
        return [[_schedules fetchedObjects]count]+1;
    }
    if(tableView.tag == 1){
        return 2;
    }
    return 1;

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1 && indexPath.row == 0 && elementsSelected){
        return[[_devices fetchedObjects]count]*55+55;
    }
    if(tableView.tag == 1 && indexPath.row == 1 && schedulesSelected){
        return[[_schedules fetchedObjects]count]*55+55;
    }
    return 55;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        UITableViewCell * cell = nil;
        
        switch (indexPath.row) {
            case 0:{
                
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                
                elementsTBV = tbv;
                
                UIView *bgColorView = [[UIView alloc] init];
                bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
                [cell setSelectedBackgroundView:bgColorView];
                
                
                tbv.tag = 2;
                
                tbv.delegate = self;
                tbv.dataSource = self;
                
                return cell;
            }
                break;
            case 1:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                schedulesTBV = tbv;
                
                
                UIView *bgColorView = [[UIView alloc] init];
                bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
                [cell setSelectedBackgroundView:bgColorView];
                
                tbv.tag = 3;
                
                tbv.delegate = self;
                tbv.dataSource = self;
                
                return cell;
            }
                break;
            default:
                break;
        }
        
        return cell;
        
        
    } else if(tableView == elementsTBV){
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        [cell setSelectedBackgroundView:bgColorView];
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        
        label.text = NSLocalizedString(@"devices", @"");
        
        if(elementsSelected){
            img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            line.hidden = YES;
            
            if(indexPath.row >0){
                img.hidden = YES;
                Device*Device = [_devices objectAtIndexPath: [NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section]];
                label.text = Device.label;
            } else {
                label.text = NSLocalizedString(@"devices", @"");
                img.hidden = NO;
            }
            
        } else {
            img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
            img.hidden = NO;
            line.hidden = NO;
            
        }
        return cell;
        
    } else if(tableView == schedulesTBV){
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        [cell setSelectedBackgroundView:bgColorView];
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        
        label.text = NSLocalizedString(@"time_schedule_settings_title", @"");
        
        if(schedulesSelected){
            img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            line.hidden = YES;
            
            if(indexPath.row >0){
                DeviceTimeScheduleClass*schedule = [_schedules objectAtIndexPath: [NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section]];
                label.text = schedule.label;
                img.hidden = YES;
            } else {
                label.text = NSLocalizedString(@"time_schedule_settings_title", @"");
                img.hidden = NO;
            }
        } else {
            img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
            img.hidden = NO;
            line.hidden = NO;
            
        }
        return cell;
    }
    UITableViewCell*cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"row: %ld, section: %ld", (long)indexPath.row, (long)indexPath.section);

    
    if(indexPath.row == 0){
        if(tableView == elementsTBV){
            if(elementsSelected){
                elementsSelected = NO;
                schedulesSelected = NO;
            } else {
                elementsSelected = YES;
                schedulesSelected = NO;
            }
        } else {
            if(schedulesSelected){
                elementsSelected = NO;
                schedulesSelected = NO;
            }else {
                elementsSelected = NO;
                schedulesSelected = YES;
            }
        }
    } else {
        indexPath = [NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section];
        if(tableView == elementsTBV){
            Device*device = [_devices objectAtIndexPath:indexPath];
            [self performSegueWithIdentifier:@"addDeviceSegue" sender:device];
        } else {
            DeviceTimeScheduleClass*schedule = [_schedules objectAtIndexPath:indexPath];
            [self performSegueWithIdentifier:@"scheduleDetail" sender:schedule];
        }
    }
    
    if(schedulesSelected || elementsSelected){
        _addButton.hidden = NO;
    } else {
        _addButton.hidden = YES;
    }
    
    [self setTableViewBorder];
}

-(void)setTableViewBorder{
/*    if(schedulesSelected){
        _tableViewHeight.constant = ([[_schedules fetchedObjects] count] * 55) + (3*55);
    } else if(hcaSelected){
        _tableViewHeight.constant = ([[_HCAs fetchedObjects] count] * 55) + (3*55);
    } else if (centralPowerSelected){
        _tableViewHeight.constant = (3*55);
    } else {
        _tableViewHeight.constant = (3*55);
    }*/
    
    if(elementsSelected){
        elementsTBV.layer.borderColor =USBlueColor.CGColor;
        elementsTBV.layer.borderWidth = 1.0f;
        
        _tbvHeight.constant = (2*55) + [_devices.fetchedObjects count] * 55;
    } else {
        elementsTBV.layer.borderColor = nil;
        elementsTBV.layer.borderWidth = 0;
    }
    
    if(schedulesSelected){
        schedulesTBV.layer.borderColor =USBlueColor.CGColor;
        schedulesTBV.layer.borderWidth = 1.0f;
        
        _tbvHeight.constant = (2*55) + [_schedules.fetchedObjects count] *55;
    } else {
        schedulesTBV.layer.borderColor = nil;
        schedulesTBV.layer.borderWidth = 0;
    }
    
    if(!schedulesSelected && !elementsSelected){
        _tbvHeight.constant = 2*55;
    }
    
    [_tableView reloadData];
    
    [schedulesTBV reloadData];
    [elementsTBV reloadData];
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)addTapped:(id)sender {
    if(schedulesSelected){
        [self performSegueWithIdentifier:@"scheduleDetail" sender:nil];
    } else {
        [self performSegueWithIdentifier:@"addDeviceSegue" sender:nil];
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
        if([segue.identifier isEqualToString:@"scheduleDetail"]){
           DeviceDetailScheduleViewController*controller = (DeviceDetailScheduleViewController*) [segue destinationViewController];
            controller.mySchedule = sender;
        } else if([segue.identifier isEqualToString:@"addDeviceSegue"]){
            AddDeviceViewController *myController = (AddDeviceViewController*) segue.destinationViewController;
            myController.device = sender;
        }
        
}




@end
