//
//  DevicesCollectionViewDelegate.m
//  US App
//
//  Created by Tom Odler on 01.02.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "DevicesCollectionViewDelegate.h"
#import "RGBTrimmerViewController.h"
#import "Device.h"
#import "TrimmerViewController.h"
#import "Util.h"
#import "URLConnector.h"
#import "NSObject+SBJson.h"
#import "SYCoreDataManager.h"
#import "AppDelegate.h"
#import "DeviceInRoom.h"
#import "Device+State.h"
#import "Device+Action.h"
#import "SYAPIManager.h"
#import "DevicesCollectionViewCell.h"
#import "SY_State_HCAView.h"
#import "SY_State_TemperatureView.h"
#import "SY_State_RGBView.h"
#import "SY_State_TempTrimmerView.h"
#import "SYDevicesViewController.h"
#import "DimmingCollectionViewCell.h"
#import "SWRevealViewControllerAddon.h"
#import "EFCircularSlider.h"
#import "Constants.h"

@interface DevicesCollectionViewDelegate ()


- (void)initFetchedResultsController;

@end

@implementation DevicesCollectionViewDelegate

- (id)initWithRoomID:(NSString *)roomID {
    id res = [super init];
    if (res != nil) {
        _roomID = roomID;
        [self initFetchedResultsController];
        [_collectionView reloadData];
    }
    return res;
}

- (id)init {
    self = [super init];
    
    if (self) {
        _deviceCollectionMode = DeviceCollectionModeRoom;
        
    }
    
    return self;
}

#pragma mark - Properties

- (void)setRoomID:(NSString *)roomID {
    _roomID = roomID;
    [self initFetchedResultsController];
}

- (void)setDeviceListMode:(DeviceCollectionMode)deviceListMode {
    _deviceCollectionMode = deviceListMode;
    
    [self initFetchedResultsController];
}

#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:_deviceCollectionMode == DeviceCollectionModeRoom ? @"DeviceInRoom" : @"Device"];
    // [[NSFetchRequest alloc] init];
    
    //  NSEntityDescription *entity = [NSEntityDescription entityForName: _deviceListMode==DeviceListModeRoom?@"DeviceInRoom":@"Device"  inManagedObjectContext:context];
    
    NSPredicate *searchFilter;
    if (_deviceCollectionMode == DeviceCollectionModeRoom) {
        searchFilter = [NSPredicate predicateWithFormat:@"room.roomID == %@", _roomID];
    } else if (_deviceCollectionMode == DeviceCollectionModeFavourite) {
        searchFilter = [NSPredicate predicateWithFormat:@"favourite == YES"];
        //        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.favourite == 1"];
        //        _favourites = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Device" withPredicate:predicate];
    }
    
    //    [fetchRequest setRelationshipKeyPathsForPrefetching:[NSArray arrayWithObjects:@"device.state", nil]];
    [fetchRequest setIncludesSubentities:YES];
    //[fetchRequest setEntity:entity];
    [fetchRequest setPredicate:searchFilter];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:_deviceCollectionMode == DeviceCollectionModeRoom ? @"device.label" : @"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetchRequest
                                 managedObjectContext:context
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    _fetchedResultsController.delegate = self;
    NSError *error;
    
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"Error fetching devices: %@", error.description);
    }
    
    NSFetchRequest *fetchRequest2 = [[NSFetchRequest alloc] initWithEntityName:@"State"];
    NSPredicate *searchFilter2;
    if (_deviceCollectionMode == DeviceCollectionModeRoom) {
        searchFilter2 = [NSPredicate predicateWithFormat:@"(SUBQUERY(self.device.inRooms, $r, $r.room.roomID == %@).@count > 0 OR \
                         SUBQUERY(self.device.deviceCooling.inRooms, $r1, $r1.room.roomID == %@).@count > 0 OR \
                         SUBQUERY(self.device.deviceHeating.inRooms, $r1, $r1.room.roomID == %@).@count > 0)", _roomID, _roomID, _roomID];
    } else {
        searchFilter2 = [NSPredicate predicateWithFormat:@"self.device.favourite == YES"];
    }
    [fetchRequest2 setPredicate:searchFilter2];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"device.label" ascending:YES];
    [fetchRequest2 setSortDescriptors:[[NSArray alloc] initWithObjects:sortDescriptor2, nil]];
    
    [fetchRequest2 setReturnsObjectsAsFaults:NO];
    
    _fetchedResultsController2 = [[NSFetchedResultsController alloc]
                                  initWithFetchRequest:fetchRequest2
                                  managedObjectContext:context
                                  sectionNameKeyPath:nil
                                  cacheName:nil];
    _fetchedResultsController2.delegate = self;
    if (![_fetchedResultsController2 performFetch:&error]) {
        NSLog(@"Error fetching devices: %@", error.description);
    }
    
}

#pragma mark - NSfetchRequestResultsDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.collectionView reloadData];
    NSLog(@"Reloading collectionView data");
    //    if (_deviceListMode == DeviceListModeFavourite) {
    //        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.favourite == '1'"];
    //        _favourites = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Device" withPredicate:predicate];
    //    }
}

/*- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
 NSLog(@"updating row %ld",(long)indexPath.row);
 switch (type) {
 case NSFetchedResultsChangeInsert:
 [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
 break;
 case  NSFetchedResultsChangeDelete:
 [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
 break;
 case  NSFetchedResultsChangeMove:
 [self.tableView moveRowAtIndexPath:indexPath toIndexPath:newIndexPath];
 break;
 case  NSFetchedResultsChangeUpdate:
 [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
 break;
 
 default:
 break;
 }
 }*/

#pragma mark - TableView handling
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ([[_fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}


RGBTrimmerViewController *nonSystemsController;
// Customize the appearance of table view cells.

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UINib *myNib = [UINib nibWithNibName:@"DevicesCollectionViewCell" bundle:nil];
    [self.collectionView registerNib:myNib forCellWithReuseIdentifier:@"deviceNIBCell"];
    
    Device *selectedDevice;
    
    if (_deviceCollectionMode == DeviceCollectionModeRoom) {
        DeviceInRoom *deviceInRoom = (DeviceInRoom *) [_fetchedResultsController objectAtIndexPath:indexPath];
        selectedDevice = deviceInRoom.device;
    } else {
        selectedDevice = (Device *) [_fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    DimmingCollectionViewCell *myCell = (DimmingCollectionViewCell*)[self.collectionView dequeueReusableCellWithReuseIdentifier:@"dimmingCell" forIndexPath:indexPath];
    
    myCell.deviceLabel.text = selectedDevice.label;
    myCell.deviceLabel.textColor = USBlueColor;
    myCell.heatingView.hidden = YES;
    myCell.warning.hidden = YES;
    
    myCell.automatLabel.hidden = YES;
    if([selectedDevice hasStateName:@"automat"]){
        myCell.automatLabel.hidden = ![[selectedDevice getStateValueForStateName:@"automat"] boolValue];
    }
    
    myCell.delayed.hidden = YES;
    if([selectedDevice hasStateName:@"delay"] && ![[selectedDevice getStateValueForStateName:@"locked"]boolValue]){
        myCell.delayed.hidden = ![[selectedDevice getStateValueForStateName:@"delay"] boolValue];
    }
    
    [myCell.deviceImage setImage:[Util determineImage:selectedDevice]];
    [myCell.deviceImage setContentMode:UIViewContentModeScaleAspectFit];
    
    CGRect sliderFrame = CGRectMake(0, -10, myCell.circleView.frame.size.width + 20, myCell.circleView.frame.size.height + 20);
    EFCircularSlider* _circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    
    CGFloat dash[]={1,15};
    
    [_circularSlider setHandleRadius:5];
    [_circularSlider setUnfilledLineDash:dash andCount:2];
    [_circularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_circularSlider setUnfilledColor:[UIColor whiteColor]];
    [_circularSlider setFilledColor:[UIColor whiteColor]];
    [_circularSlider setHandleColor:[UIColor whiteColor]];
    [_circularSlider setLineWidth:1];
    [_circularSlider setArcStartAngle:140];
    [_circularSlider setArcAngleLength:260];
    [_circularSlider setDeviceForHandler:selectedDevice];
    
    [_circularSlider addTarget:self action:@selector(touchCancel:) forControlEvents:UIControlEventEditingDidEnd];
    _circularSlider.tag = 100;
    
    [_circularSlider setUserInteractionEnabled:YES];
    [myCell setUserInteractionEnabled:YES];
    /*
 *Reloading collectionView caused stacking of subviews - lot of circularSliders
 */
    for(UIView *myView in myCell.circleView.subviews){
        [myView removeFromSuperview];
    }
    
    if([myCell.circleView viewWithTag:100] == nil)
        [myCell.circleView addSubview:_circularSlider];
    
    if ([selectedDevice hasStateName:@"temperature"] && [selectedDevice hasStateName:@"mode"] && [selectedDevice hasStateName:@"correction"]){
        //GET DEVICE BRIGHTNESS
        [_circularSlider setCurrentArcValue:75 forStartAnglePadding:10 endAnglePadding:10];
        [_circularSlider setUserInteractionEnabled:NO];
        
        myCell.deviceImage.hidden = YES;
        myCell.heatingView.hidden = NO;
        myCell.warning.hidden = YES;
        
        if ([selectedDevice getStateValueForStateName:@"temperature"] == nil || [[selectedDevice getStateValueForStateName:@"temperature"]intValue] > 60 || [[selectedDevice getStateValueForStateName:@"temperature"] intValue] < -60) {
            myCell.heatingView.hidden = YES;
            myCell.warning.hidden = NO;
        } else {
            UILabel*tempLabel = [myCell.heatingView viewWithTag:10];
            double roundedVal = [[selectedDevice getStateValueForStateName:@"temperature"] doubleValue];
            roundedVal = lround(roundedVal);

            tempLabel.text = [NSString stringWithFormat:@"%d", (int)roundedVal];
        }
        
        UIStackView*stack = [myCell.heatingView viewWithTag:1];
        if([[selectedDevice getStateValueForStateName:@"heating"] boolValue]){
            stack.hidden = NO;
        } else {
            stack.hidden = YES;
        }
        
                switch ([[selectedDevice getStateValueForStateName:@"mode"] intValue]) {
                    case 1: {
                        [_circularSlider setHandleColor: USBlueColor];
                        break;
                    }
                        
                    case 2: {
                        [_circularSlider setHandleColor: USUtlumColor];
                        break;
                    }
                    case 3: {
                        [_circularSlider setHandleColor: USNormalColor];
                        break;
                        
                    }
                    case 4: {
                        [_circularSlider setHandleColor: USKomfrotColor];
                        break;
                        
                    }
                    default:
                        [_circularSlider setHandleColor: [UIColor blackColor]];
                        
                        break;
                }
            
        
        return myCell;
    } else if ([selectedDevice hasStateName:@"brightness"] && ([selectedDevice hasStateName:@"white balance"])){
        float brightness = [[selectedDevice getStateValueForStateName:@"brightness"] floatValue]/2.55;
        [_circularSlider setCurrentArcValue:brightness forStartAnglePadding:10 endAnglePadding:10];
        
        if([selectedDevice getStateValueForStateName:@"brightness"] == nil){
            myCell.deviceImage.image = [Util determineImageGrey:selectedDevice];
            [myCell setUserInteractionEnabled:NO];
            [_circularSlider setUserInteractionEnabled:NO];
            myCell.deviceLabel.textColor = [UIColor grayColor];
        }
        return myCell;
    } else if([selectedDevice hasStateName:@"red"] && [selectedDevice hasStateName:@"green"] && [selectedDevice hasStateName:@"blue"] && [selectedDevice hasStateName:@"demo"] && [selectedDevice hasStateName:@"brightness"]) {
        int _red = [[selectedDevice getStateValueForStateName:@"red"] intValue];
        int _green = [[selectedDevice getStateValueForStateName:@"green"] intValue];
        int _blue = [[selectedDevice getStateValueForStateName:@"blue"] intValue];
        
        float brightness = [[selectedDevice getStateValueForStateName:@"brightness"] floatValue]/2.55;
        
        [_circularSlider setHandleColor:[UIColor colorWithRed:(CGFloat)_red/255.0 green:(CGFloat)_green/255.0 blue:(CGFloat)_blue/255.0 alpha:1.0]];
        [_circularSlider setCurrentArcValue:brightness forStartAnglePadding:10 endAnglePadding:10];
        [_circularSlider setUserInteractionEnabled:YES];
        
        if([selectedDevice getStateValueForStateName:@"brightness"] == nil){
            myCell.deviceImage.image = [Util determineImageGrey:selectedDevice];
            [myCell setUserInteractionEnabled:NO];
            [_circularSlider setUserInteractionEnabled:NO];
            myCell.deviceLabel.textColor = [UIColor grayColor];
        }
        return myCell;
    } else if([selectedDevice hasActionWithName:@"brightness"]){
        float brightness = [[selectedDevice getStateValueForStateName:@"brightness"] floatValue];
        [_circularSlider setCurrentArcValue:brightness forStartAnglePadding:10 endAnglePadding:10];
        
        if([selectedDevice getStateValueForStateName:@"brightness"] == nil){
            myCell.deviceImage.image = [Util determineImageGrey:selectedDevice];
            [myCell setUserInteractionEnabled:NO];
            [_circularSlider setUserInteractionEnabled:NO];
            myCell.deviceLabel.textColor = [UIColor grayColor];
        }
        return myCell;
    }
    
    DevicesCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:@"deviceNIBCell"forIndexPath:indexPath];
    
    if([selectedDevice.elan.type containsString:@"IR"]){
        UILabel *stateLabel = (UILabel *) [cell viewWithTag:3];
        UILabel *stupne = (UILabel *) [cell viewWithTag:5];
        UIView *doubleTempView = (UIView *) [cell viewWithTag:10];
        UILabel *deviceName = (UILabel *) [cell viewWithTag:4];
        UIImageView *deviceIcon = (UIImageView *) [cell viewWithTag:2];
        
        doubleTempView.hidden = YES;
        stupne.hidden = YES;
        deviceIcon.hidden = NO;
        stateLabel.hidden = YES;
        
        [deviceName setText:selectedDevice.label];
        [deviceIcon setImage:[Util determineImage:selectedDevice]];
        [deviceIcon setContentMode:UIViewContentModeScaleAspectFit];
        
        return cell;
    }
    
    if (selectedDevice == nil) return cell;
    UIImageView *kolecko = (UIImageView*) [cell viewWithTag:1];
    UILabel *deviceName = (UILabel *) [cell viewWithTag:4];
    UIImageView *deviceIcon = (UIImageView *) [cell viewWithTag:2];
    UILabel *stateLabel = (UILabel *) [cell viewWithTag:3];
    UILabel *stupne = (UILabel *) [cell viewWithTag:5];
    UIView *doubleTempView = (UIView *) [cell viewWithTag:10];
    
    UILabel *tempIN = (UILabel *) [cell viewWithTag:6];
    UILabel *tempOUT = (UILabel *) [cell viewWithTag:7];
    UILabel *automatLabel = [cell viewWithTag:11];
    
    UILabel*termoHlaviceSt = [cell viewWithTag:20];
    UILabel*termoHlaviceCis = [cell viewWithTag:21];
    UIStackView*stack = [cell viewWithTag:12];
    
    UIView*warning = [cell viewWithTag:22];
    UIView*delayed = [cell viewWithTag:23];
    
    doubleTempView.hidden = YES;
    stupne.hidden = YES;
    deviceIcon.hidden = YES;
    stateLabel.hidden = YES;
    automatLabel.hidden = YES;
    termoHlaviceSt.hidden = termoHlaviceCis.hidden = stack.hidden = warning.hidden = delayed.hidden = YES;
    
    
    [kolecko setImage: [UIImage imageNamed:@"kolecko_prava.png"]];
    
    cell.userInteractionEnabled = true;
    deviceName.textColor = [UIColor colorWithRed:91/255.0f green:197/255.0f blue:242/255.0f alpha:1];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.roundingIncrement = [NSNumber numberWithDouble:0.1];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.decimalSeparator = @",";

    deviceName.text = selectedDevice.label;
    
    bool unknown = false;
    
    if([selectedDevice hasStateName:@"automat"]){
        automatLabel.hidden = ![[selectedDevice getStateValueForStateName:@"automat"] boolValue];
    }
    
    if([selectedDevice hasStateName:@"delay"] && ![[selectedDevice getStateValueForStateName:@"locked"]boolValue]){
        if([[selectedDevice getStateValueForStateName:@"delay"] boolValue]){
            delayed.hidden = NO;
        }
    }
    
    if ([selectedDevice getStateValueForStateName:@"roll up"]) {
              
              deviceIcon.hidden = NO;
        
        if ([selectedDevice getStateValueForStateName:@"roll up"] == nil) {
            [cell setUserInteractionEnabled:NO];
            
        } else {
            [cell setUserInteractionEnabled:YES];
        }
    } else if ([selectedDevice hasStateName:@"temperature IN"] && [selectedDevice hasStateName:@"temperature OUT"]) {
        if ([selectedDevice getStateValueForStateName:@"temperature IN"] == nil && [selectedDevice getStateValueForStateName:@"temperature OUT"] == nil) {
            
            unknown = true;
        } else {
            
            doubleTempView.hidden = NO;
            UIImageView*warningUP = [doubleTempView viewWithTag:26];
            UIImageView*warningDWN = [doubleTempView viewWithTag:25];
            
            warningUP.hidden = warningDWN.hidden = YES;
            if([selectedDevice getStateValueForStateName:@"temperature IN"] == nil || [[selectedDevice getStateValueForStateName:@"temperature IN"]intValue] >60 || [[selectedDevice getStateValueForStateName:@"temperature IN"] intValue] < -60){
                tempIN.hidden = YES;
                warningUP.hidden = NO;
            } else {
                [tempIN setText:[NSString stringWithFormat:@"%@", [formatter stringFromNumber:[selectedDevice getStateValueForStateName:@"temperature IN"]]]]; }
            
            if([selectedDevice getStateValueForStateName:@"temperature OUT"] == nil || [[selectedDevice getStateValueForStateName:@"temperature OUT"]intValue] >60 || [[selectedDevice getStateValueForStateName:@"temperature OUT"] intValue] < -60){
                tempOUT.hidden = YES;
                warningDWN.hidden = NO;
            } else{
                [tempOUT setText:[NSString stringWithFormat:@"%@", [formatter stringFromNumber:[selectedDevice getStateValueForStateName:@"temperature OUT"]]]];}
           
            if ([selectedDevice getActionForActionName:@"on"] == nil) {
                [cell setUserInteractionEnabled:NO];
            }
        }
        // HCA prvek
    } else if ([selectedDevice hasStateName:@"temperature"] && [selectedDevice hasStateName:@"mode"] && [selectedDevice hasStateName:@"correction"]) {
        if ([selectedDevice getStateValueForStateName:@"temperature"] == nil || [selectedDevice getStateValueForStateName:@"mode"] == nil) {

            
        }
    } else if ([selectedDevice hasStateName:@"temperature"] && [selectedDevice hasStateName:@"open window"] && [selectedDevice hasStateName:@"open valve"] && [selectedDevice hasStateName:@"battery"] && [selectedDevice hasStateName:@"requested temperature"] && [selectedDevice hasStateName:@"open window sensitivity"] && [selectedDevice hasStateName:@"open window off time"]) {
        
        if([selectedDevice getStateValueForStateName:@"open valve"] != nil){
            double valveNumber = [[selectedDevice getStateValueForStateName:@"open valve"] doubleValue];
            for(UIImageView*subView in [stack subviews]){
                double myNumber = (100/6)*subView.tag;
                if(myNumber<valveNumber){
                    subView.image = [UIImage imageNamed:@"chladi1.png"];
                } else {
                    subView.image = [UIImage imageNamed:@"chladi_topi_OFF.png"];
                }
            }
        }
        
         if ([selectedDevice getStateValueForStateName:@"temperature"] == nil && [selectedDevice getStateValueForStateName:@"open window"] == nil && [selectedDevice getStateValueForStateName:@"open valve"] == nil && [selectedDevice getStateValueForStateName:@"battery"] == nil && [selectedDevice getStateValueForStateName:@"requested temperature"] == nil && [selectedDevice getStateValueForStateName:@"open window sensitivity"] == nil && [selectedDevice getStateValueForStateName:@"open window off time"] == nil){
            unknown = true;
             warning.hidden = NO;
         } else if([selectedDevice getStateValueForStateName:@"temperature"] == nil || [[selectedDevice getStateValueForStateName:@"temperature"] intValue]>60 || [[selectedDevice getStateValueForStateName:@"temperature"]intValue] < -60){
             warning.hidden = NO;
         } else {
            stack.hidden = NO;
            termoHlaviceCis.hidden = NO;
            termoHlaviceSt.hidden = NO;
            double roundedVal = [[selectedDevice getStateValueForStateName:@"temperature"] doubleValue];
            roundedVal = lround(roundedVal);
            termoHlaviceCis.text = [NSString stringWithFormat:@"%d", (int)roundedVal];
        }
    } else if ([selectedDevice hasStateName:@"temperature"]) {
        if ([selectedDevice getStateValueForStateName:@"temperature"] == nil || [[selectedDevice getStateValueForStateName:@"temperature"]intValue] > 60 || [[selectedDevice getStateValueForStateName:@"temperature"]intValue] < -60) {
            
            warning.hidden = NO;
        } else {
            stateLabel.hidden = NO;
            stupne.hidden = NO;
            stateLabel.text = [NSString stringWithFormat:@"%@", [formatter stringFromNumber:[selectedDevice getStateValueForStateName:@"temperature"]]];
        }
    }  else if ([selectedDevice hasStateName:@"on"]) {
        
        deviceIcon.hidden = NO;
        
        if ([selectedDevice getStateValueForStateName:@"on"] == nil) {
            [cell setUserInteractionEnabled:NO];
            unknown = true;
        } else {
            [cell setUserInteractionEnabled:YES];
            unknown = false;
        }
    }
    
    BOOL locked = false;
    if ([selectedDevice hasStateName:@"locked"]) {
        if ([[selectedDevice getStateValueForStateName:@"locked"] boolValue] == YES) {
            locked = true;
        }
    }
    
    if(unknown){
        [deviceIcon setImage: [Util determineImageGrey:selectedDevice]];
        deviceName.textColor = [UIColor grayColor];
        cell.userInteractionEnabled = false;
        [kolecko setImage: [UIImage imageNamed:@"kolecko_prava_seda.png"]];
    } else if(locked){
        cell.userInteractionEnabled = false;
        [kolecko setImage: [UIImage imageNamed:@"kolecko_prava_seda.png"]];
        [deviceIcon setImage: [Util determineImage:selectedDevice]];
    } else {
        cell.userInteractionEnabled = true;
        [kolecko setImage: [UIImage imageNamed:@"kolecko_prava.png"]];
        [deviceIcon setImage: [Util determineImage:selectedDevice]];
    }


[deviceIcon setContentMode:UIViewContentModeScaleAspectFit];
return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    Device *selectedDevice;
    
        DeviceInRoom *deviceInRoom = (DeviceInRoom *) [_fetchedResultsController objectAtIndexPath:indexPath];
        selectedDevice = (Device *) deviceInRoom.device;
        deviceInRoom.coordX = [(NSManagedObject *) deviceInRoom valueForKey:@"coordX"];
        deviceInRoom.coordY = [(NSManagedObject *) deviceInRoom valueForKey:@"coordY"];
        

    
    
    if (selectedDevice == nil) return;
    if (selectedDevice.states == nil)return;
    if([selectedDevice hasStateName:@"automat"]){
        if([[selectedDevice getStateValueForStateName:@"automat"] boolValue] == YES){
            return;
        }
    }
    
    
    if ([selectedDevice getStateValueForStateName:@"locked"] != nil) if ([[selectedDevice getStateValueForStateName:@"locked"] boolValue] == YES) return;
    
    if([selectedDevice getStateValueForStateName:@"motor"] != nil) {
        NSDictionary *dict = nil;
        dict = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithBool:true], @"motor", nil];
        [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:dict device:selectedDevice success:^(AFHTTPRequestOperation *operation, id object) {
        }                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        }];
        
    } else
        
        if([selectedDevice getStateValueForStateName:@"brightness"] != nil && [selectedDevice getStateValueForStateName:@"white balance"]!= nil){
            
            Action *brigtnessAction = [selectedDevice getActionForActionName:@"brightness"];
            id brigtnessValue = [selectedDevice getStateValueForStateName:@"brightness"];
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            if ([brigtnessValue isEqualToNumber:brigtnessAction.min]) {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSNumber *intensity = (NSNumber *) [defaults objectForKey:selectedDevice.deviceID];
                if (intensity != nil && ![intensity isEqualToNumber:brigtnessAction.min]) {
                    //                    [selectedDevice setStateValue:intensity forName:@"brightness"];
                    [dict setObject:intensity forKey:@"brightness"];
                } else {
                    //                    [selectedDevice setStateValue:brigtnessAction.max forName:@"brightness"];
                    [dict setObject:brigtnessAction.max forKey:@"brightness"];
                }
            } else {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSNumber *intensityNumber = [[NSNumber alloc] initWithInt:[brigtnessValue intValue]];
                [defaults setObject:intensityNumber forKey:selectedDevice.deviceID];
                [defaults synchronize];
                //                [selectedDevice setStateValue:brigtnessAction.min forName:@"brightness"];
                [dict setObject:brigtnessAction.min forKey:@"brightness"];
            }
            
            [dict setObject:[selectedDevice getStateValueForStateName:@"white balance"] forKey:@"white balance"];
            
            [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:dict device:selectedDevice success:^(AFHTTPRequestOperation *operation, id object) {
            }                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                                  message:NSLocalizedString(@"elan_not_reacheable", nil)
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                
                [message show];
                NSLog(@"Elan did not respond fot action!");
            }];
        } else
            if ([selectedDevice getStateValueForStateName:@"roll up"]) {
                NSDictionary *dict = nil;
                if ([[selectedDevice getStateValueForStateName:@"roll up"] boolValue] == YES)
                    dict = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNull alloc] init], @"roll down", nil];
                else
                    dict = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNull alloc] init], @"roll up", nil];
                
                //  NSLog(@"post data to %@ with value %@",[@"device/" stringByAppendingString:selectedDevice.name], postDataString );
                [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:dict device:selectedDevice success:^(AFHTTPRequestOperation *operation, id object) {
                }                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                }];
            } else if ([selectedDevice getStateValueForStateName:@"red"] != nil && [selectedDevice getStateValueForStateName:@"green"] != nil && [selectedDevice getStateValueForStateName:@"blue"] != nil) {
                
                //UIColor * colorUI = [Util convertToUIColorRed:(CGFloat)[[selectedDevice getStateValueForStateName:@"red"] floatValue] green:(CGFloat)[[selectedDevice getStateValueForStateName:@"green"] floatValue] blue:(CGFloat)[[selectedDevice getStateValueForStateName:@"blue"] floatValue]];
                //[nonSystemsController.circleView setBackgroundColor:colorUI];
                /*for (RGBTrimmerViewController * controller in self.rgbControllers){
                 [controller.circleView setBackgroundColor:colorUI];
                 }*/
                NSMutableDictionary *dict = [NSMutableDictionary dictionary];

                if ([[selectedDevice getStateValueForStateName:@"brightness"] boolValue] == NO) {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSNumber *intensity = (NSNumber *) [defaults objectForKey:selectedDevice.deviceID];
                    if (intensity != nil && intensity.intValue != 0) {
                        //                [selectedDevice setStateValue:intensity forName:@"brightness"];
                        [dict setObject:intensity forKey:@"brightness"];
                    } else {
                        //                [selectedDevice setStateValue:[[selectedDevice getActionForActionName:@"brightness"] max] forName:@"brightness"];
                        [dict setObject:[[selectedDevice getActionForActionName:@"brightness"] max] forKey:@"brightness"];
                    }
                } else {
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    NSNumber *intensityNumber = [[NSNumber alloc] initWithInt:[[selectedDevice getStateValueForStateName:@"brightness"] intValue]];
                    [defaults setObject:intensityNumber forKey:selectedDevice.deviceID];
                    [defaults synchronize];
                    //            [selectedDevice setStateValue:[[NSNumber alloc] initWithInt:0] forName:@"brightness"];
                    [dict setObject:[[selectedDevice getActionForActionName:@"brightness"] min] forKey:@"brightness"];
                }
                [dict setObject:[selectedDevice getStateValueForStateName:@"green"] forKey:@"green"];
                [dict setObject:[selectedDevice getStateValueForStateName:@"red"] forKey:@"red"];
                [dict setObject:[selectedDevice getStateValueForStateName:@"blue"] forKey:@"blue"];
                [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:dict device:selectedDevice success:^(AFHTTPRequestOperation *operation, id object) {
                }                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                }];
                
            } else {
                if ([selectedDevice getStateValueForStateName:@"on"] != nil) {
                    NSDictionary *stateDictionary = nil;
                    if ([[selectedDevice getStateValueForStateName:@"on"] intValue] == 1)
                        stateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithBool:NO], @"on", nil];
                    else
                        stateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithBool:YES], @"on", nil];
                    
                    NSLog(@"deviceID: %@, devicename: %@", selectedDevice.deviceID, selectedDevice.label);
                    [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:stateDictionary device:selectedDevice success:^(AFHTTPRequestOperation *operation, id object) {
                        NSLog(@"sucessfully updated");
                    }                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        NSLog(@"update err");
                    }];
                    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
                }
                if ([selectedDevice getActionForActionName:@"brightness"] != nil) {
                    //            NSNumber * newValue = nil;
                    //            if ([[selectedDevice getStateValueForStateName:@"brightness"] intValue]==0){
                    //                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    //                NSNumber *intensity = (NSNumber*)[defaults objectForKey:selectedDevice.deviceID];
                    //                if (intensity.intValue!=0){
                    //                    newValue = intensity;
                    //                    //[defaults removeObjectForKey:selectedDevice.name];
                    //                }else{
                    //                    newValue = [[selectedDevice getActionForActionName:@"brightness"] max];
                    //                }
                    //            }else{
                    //
                    //                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    //                NSNumber * intensityNumber = [[NSNumber alloc] initWithInt:[[selectedDevice getStateValueForStateName:@"brightness"] intValue]];
                    //                [defaults setObject:intensityNumber forKey:selectedDevice.deviceID];
                    //                [defaults synchronize];
                    //                newValue = [[NSNumber alloc] initWithInt:0];
                    //            }
                    //            NSString * postDataString = @"{\"brightness\":";
                    //
                    //            postDataString = [postDataString stringByAppendingString:[NSString stringWithFormat:@"%d", [[selectedDevice getStateValueForStateName:@"brightness"] intValue]]];
                    //            postDataString = [postDataString stringByAppendingString:@"}"];
                    //            NSData* data = [postDataString dataUsingEncoding:NSUTF8StringEncoding];
                    //            //            NSLog(@"post data to %@ with value %@",[@"device/" stringByAppendingString:selectedDevice.name], postDataString );
                    //            NSDictionary * stateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:newValue, @"brightness", nil];
                    //
                    //            [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:stateDictionary device:selectedDevice success:^(AFHTTPRequestOperation * operation, id object){} failure:^(AFHTTPRequestOperation * operation, NSError * error){}];
                    
                    Action *brigtnessAction = [selectedDevice getActionForActionName:@"brightness"];
                    id brigtnessValue = [selectedDevice getStateValueForStateName:@"brightness"];
                    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
                    if ([brigtnessValue isEqualToNumber:brigtnessAction.min]) {
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        NSNumber *intensity = (NSNumber *) [defaults objectForKey:selectedDevice.deviceID];
                        if (intensity != nil && ![intensity isEqualToNumber:brigtnessAction.min]) {
                            //                    [selectedDevice setStateValue:intensity forName:@"brightness"];
                            [dict setObject:intensity forKey:@"brightness"];
                        } else {
                            //                    [selectedDevice setStateValue:brigtnessAction.max forName:@"brightness"];
                            [dict setObject:brigtnessAction.max forKey:@"brightness"];
                        }
                    } else {
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        NSNumber *intensityNumber = [[NSNumber alloc] initWithInt:[brigtnessValue intValue]];
                        [defaults setObject:intensityNumber forKey:selectedDevice.deviceID];
                        [defaults synchronize];
                        //                [selectedDevice setStateValue:brigtnessAction.min forName:@"brightness"];
                        [dict setObject:brigtnessAction.min forKey:@"brightness"];
                    }
                    [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:dict device:selectedDevice success:^(AFHTTPRequestOperation *operation, id object) {
                        
                    }                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                                          message:NSLocalizedString(@"elan_not_reacheable", nil)
                                                                         delegate:nil
                                                                cancelButtonTitle:@"OK"
                                                                otherButtonTitles:nil];
                        
                        [message show];
                    }];
                    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
                    
                }
                
            }
    
    [self.collectionView deselectItemAtIndexPath:indexPath animated:YES];
}


- (void) showUknownDeviceStateForCell:(DevicesCollectionViewCell*)cell deviceControl:(UIImageView*)deviceControl{

    [deviceControl setImage:[UIImage imageNamed:@"unknown_device_state"]];
    [deviceControl setHidden:NO];
    [cell setUserInteractionEnabled:NO];
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        CGPoint p = [gestureRecognizer locationInView:self.collectionView];
    
        
        NSIndexPath *indexPath = [self.collectionView indexPathForItemAtPoint:p];
        Device *device;
        
        UINavigationController *navController = (UINavigationController*) self.view.window.rootViewController;
        SWRevealViewControllerAddon *myController2 = (SWRevealViewControllerAddon*) navController.viewControllers.lastObject;
        
        UINavigationController *navController2 = (UINavigationController *)myController2.frontViewController;
        SYDevicesViewController *myController = (SYDevicesViewController*) navController2.viewControllers.lastObject ;
        
        SYRoomsViewController*roomsController = nil;
        if([myController isKindOfClass: [MainMenuPVC class]]){
            
            MainMenuPVC *pvc = (MainMenuPVC*)myController;
            roomsController = (SYRoomsViewController*)[pvc getCurrentViewController];
        }
        
        if (_deviceCollectionMode == DeviceCollectionModeRoom) {
            DeviceInRoom *deviceInRoom = (DeviceInRoom *) [_fetchedResultsController objectAtIndexPath:indexPath];
            device = deviceInRoom.device;
        } else {
            device = (Device *) [_fetchedResultsController objectAtIndexPath:indexPath];
            
        }
        
        NSSet *actionsInfo = device.actions;
        
        if ([device getStateValueForStateName:@"locked"] != nil) if ([[device getStateValueForStateName:@"locked"] boolValue] == YES) return;
        if([device.elan.type containsString:@"IR"]){
            if([device.type containsString:@"clima"]){
                if(roomsController != nil){
                    roomsController.segueDevice = device;
                    [roomsController performSegueWithIdentifier:@"IR_klima" sender:device];
                } else {
                    myController.segueDevice = device;
                    [myController performSegueWithIdentifier:@"IR_klima" sender:nil];
                }
            } else {
            if(roomsController != nil){
                roomsController.segueDevice = device;
                [roomsController performSegueWithIdentifier:@"IRControllerROOMSegue" sender:device];
            } else {
                    myController.segueDevice = device;
                    [myController performSegueWithIdentifier:@"IRControllerSegue" sender:nil];
                }
            }
        } else
        if( [device getActionForActionName:@"roll up"] != nil){
            if(roomsController != nil){
                roomsController.segueDevice = device;
                [roomsController performSegueWithIdentifier:@"blindsROOMSegue" sender:nil];
            } else {
                myController.segueDevice = device;
                [myController performSegueWithIdentifier:@"blindsSegue" sender:nil];
            }
        } else
            if ([device hasStateName:@"red"] && [device hasStateName:@"green"] && [device hasStateName:@"blue"] && [device hasStateName:@"demo"] && [device hasStateName:@"brightness"]) {
                if(roomsController != nil){
                    roomsController.segueDevice = device;
                    [roomsController performSegueWithIdentifier:@"RGBROOMSegue" sender:nil];
                } else {
                    myController.segueDevice = device;
                    [myController performSegueWithIdentifier:@"RGBSegue" sender:nil];
                }
                
            } else if( [device getStateValueForStateName:@"white balance" ] != nil){
                if(roomsController != nil){
                    roomsController.segueDevice = device;
                    [roomsController performSegueWithIdentifier:@"LightChtomaticityROOMSegue" sender:nil];
                } else {
                    myController.segueDevice = device;
                    [myController performSegueWithIdentifier:@"LightChtomaticitySegue" sender:nil];
                }
                
            }else if([device getActionForActionName:@"brightness"] != nil ){
                if(roomsController != nil){
                    roomsController.segueDevice = device;
                    [roomsController performSegueWithIdentifier:@"dimmingROOMSegue" sender:nil];
                } else {
                    myController.segueDevice = device;
                    [myController performSegueWithIdentifier:@"dimmingSegue" sender:nil];
                }
                
            } else if([[actionsInfo filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"name == 'delayed on' OR name == 'delayed off'"]]count] > 1){
                if(roomsController != nil){
                    roomsController.segueDevice = device;
                    [roomsController performSegueWithIdentifier:@"releROOMSegue" sender:nil];
                } else{
                    myController.segueDevice = device;
                    [myController performSegueWithIdentifier:@"releSegue" sender:nil];
                }
            } else {
                if ([self.view viewWithTag:50] == nil) {
                    if (![device getActionForActionName:@"requested temperature"] && ![device getActionForActionName:@"correction"]) {
                        
                        if ([device.secondaryActions count] <= 0) {
                            return;
                        }
                        _actions = [[DeviceActionsViewController alloc] initWithNibName:@"DeviceActionsViewController" bundle:nil];
                        [_actions setDevice:device];
                    } else if ([device getStateValueForStateName:@"temperature"] != nil && [device getStateValueForStateName:@"open window"] != nil && [device getStateValueForStateName:@"open valve"] != nil && [device getStateValueForStateName:@"battery"] != nil && [device getStateValueForStateName:@"requested temperature"] != nil && [device getStateValueForStateName:@"open window sensitivity"] != nil && [device getStateValueForStateName:@"open window off time"] != nil) {
                        if ([self.view viewWithTag:40] == nil) {
                            //                        _thermView = [[ThermostatViewController alloc] initWithNibName:@"ThermostatViewController" bundle:nil device:device];
                        }
                    } else if ([device getStateValueForStateName:@"correction"] != nil && [device getStateValueForStateName:@"mode"] != nil && [device getStateValueForStateName:@"temperature"] != nil) {
                        myController.segueDevice = device;
                        [myController performSegueWithIdentifier:@"HCASegue" sender:nil];
                        //                    if ([self.view viewWithTag:40] == nil) {
                        //                        _heatView = [[HeatCoolAreaViewController alloc] initWithNibName:@"HeatCoolAreaViewController" bundle:nil device:(HeatCoolArea*) device];
                        //                        
                        //                        _heatView.view.tag = 40;
                        //                        [self.view addSubview:_heatView.view];
                        //                    }
                        
                    }
                }
            }
    }

    
    /*CGPoint p = [gestureRecognizer locationInView:self.tableView];
     
     NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
     
     Device * device;
     
     if (_deviceListMode == DeviceListModeRoom) {
     DeviceInRoom* deviceInRoom = (DeviceInRoom*)[_fetchRequestController objectAtIndexPath:indexPath];
     device = (Device* )[CoreDataManager readDevice:deviceInRoom.deviceName];
     } else {
     Favourites* favourite = (Favourites*)[_fetchRequestController objectAtIndexPath:indexPath];
     device = (Device* )[CoreDataManager readDevice:favourite.deviceName];
     }
     
     
     if ([device.type isEqualToString:@"rgb light"] ){
     if ([self.view viewWithTag:60]==nil){
     _rgb = [[RGBViewController alloc] initWithNibName:@"RGBViewController" bundle:nil device:device];
     _rgb.view.tag = 60;
     
     [self.view addSubview:_rgb.view];
     }
     }else{
     if ([self.view viewWithTag:50]==nil){
     if (device.secondaryActions!=nil && [device.secondaryActions count]>0){
     _actions = [[DeviceActionsViewController alloc] initWithNibName:@"DeviceActionsViewController" bundle:nil ];
     [_actions setDevice:device];
     _actions.view.tag = 50;
     [self.view addSubview:_actions.view];
     }
     }
     }*/
}

- (void)connectionResponseOK:(NSObject *)returnedObject response:(id <Response>)responseType {
    
    
}

- (void)connectionResponseFailed:(NSInteger)code {
    
}

- (void)drawTrimmerWithValue:(int)value maxValue:(int)maxValue view:(UIView *)view {
    //    [number setText:[NSString stringWithFormat:@"%d", value]];
    int max = 50;
    float onValue = (50.00 / maxValue);
    onValue *= value;
    UIImageView *myImage;
    
    for (int i = 0; i < max; i++) {
        
        if (i >= onValue) {
            myImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stmivani_off.png"]];
        } else {
            myImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stmivani_on1.png"]];
        }
        myImage.frame = CGRectMake(320 - 36 - myImage.frame.size.width / 2, 5, myImage.frame.size.width, myImage.frame.size.height);
        myImage.tag = 100;
        [view addSubview:myImage];
        float consMove = (360.0 / max);
        [self rotateView:myImage aroundPoint:CGPointMake(320 - 36, 35) degrees:i * consMove - (360 / 4)];
    }
    
}


#define DEGREES_TO_RADIANS(angle) (angle/180.0*M_PI)

- (void)rotateView:(UIView *)view
       aroundPoint:(CGPoint)rotationPoint
           degrees:(CGFloat)degrees {
    
    CGPoint anchorPoint = CGPointMake((rotationPoint.x - CGRectGetMinX(view.frame)) / CGRectGetWidth(view.bounds),
                                      (rotationPoint.y - CGRectGetMinY(view.frame)) / CGRectGetHeight(view.bounds));
    
    [[view layer] setAnchorPoint:anchorPoint];
    [[view layer] setPosition:rotationPoint]; // change the position here to keep the frame
    CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(degrees));
    view.transform = rotationTransform;
    
}

-(void) touchCancel:(EFCircularSlider*)circularSlider
{
    NSLog(@"device: %@", circularSlider.deviceForHandler.label);
    if(circularSlider.deviceForHandler == nil)
        return;
    Device *myDevice = circularSlider.deviceForHandler;
    
    
//RF WHITE
    if([myDevice hasStateName:@"brightness"] && [myDevice hasStateName:@"white balance"])
    {
        int brightness = [circularSlider getCurrentArcValueForStartAnglePadding:10 endAnglePadding:10] * 2.55; //RF white has 0-255 brightness
        int chroma = [[myDevice getStateValueForStateName:@"white balance"] intValue];
        NSDictionary* dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithInt:brightness],@"brightness",[[NSNumber alloc] initWithFloat:chroma], @"white balance",nil];
        
        [self sendDimmAction:dict device:myDevice];
        
//RGB
    } else if ([myDevice hasStateName:@"red"] && [myDevice hasStateName:@"green"] && [myDevice hasStateName:@"blue"] && [myDevice hasStateName:@"demo"] && [myDevice hasStateName:@"brightness"]){
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        int brightness = [circularSlider getCurrentArcValueForStartAnglePadding:10 endAnglePadding:10] * 2.55;
        
        [dict setObject: [NSNumber numberWithInt:brightness] forKey:@"brightness"];
        [dict setObject:[myDevice getStateValueForStateName:@"green"] forKey:@"green"];
        [dict setObject:[myDevice getStateValueForStateName:@"red"] forKey:@"red"];
        [dict setObject:[myDevice getStateValueForStateName:@"blue"] forKey:@"blue"];
        [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:dict device:myDevice success:^(AFHTTPRequestOperation *operation, id object) {
        }                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        }];
        
        
        
//DIMMER
    } else if ( [myDevice hasStateName:@"brightness"]) {
        int brightness = [circularSlider getCurrentArcValueForStartAnglePadding:10 endAnglePadding:10]; // Simple dimmers have 0-100 only, step = 10
        brightness = [self nearestNumber:brightness step:10];
        NSDictionary* dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithInt:brightness],@"brightness",nil];
        
        [self sendDimmAction:dict device:myDevice];
    }
}

-(void)sendDimmAction: (NSDictionary*)dict device:(Device*)device{
    [[SYAPIManager sharedInstance] putDeviceAction:dict device:[[SYCoreDataManager sharedInstance] getDeviceWithID:device.deviceID]
                                           success:^(AFHTTPRequestOperation* operation, id response){
                                               NSLog(@"update complete: %@",response);
                                           }
                                           failure:^(AFHTTPRequestOperation* operation, NSError *error) {
                                               NSLog(@"update error: %@",[error description]);
                                           }];
}


-(int) nearestNumber:(int)num step:(int)step{
    NSLog(@"input number : %d step is %d", num, step);
    if (num % step == 0)
        NSLog(@"OK");
    else if (num % step < ((float)step/2.0))
        num = num - num % step;
    else
        num = num + (step - num % step);
    return num;
}

@end
