//
//  SWRevealViewControllerAddon.m
//  SAM_11_NewDesign
//
//  Created by admin on 06.07.16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "SWRevealViewControllerAddon.h"

@implementation SWRevealViewControllerAddon

//REQUIRED: USE IN STORYBOARD INSTEAD OF SWRevealViewController CLASS
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.navigationController) {
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.view.window setRootViewController:self.navigationController];
}
@end
