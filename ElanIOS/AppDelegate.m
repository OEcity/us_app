//
//  AppDelegate.m
//  ElanIOS
//
//  Created by Vratislav Zima on 5/24/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "AppDelegate.h"
#import "Device.h"
#import "DevicesLoader.h"
#import "Util.h"
#import "SYCoreDataManager.h"
#import "SYAPIManager.h"
#import "LoaderHelper.h"
#import "Constants.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "LinphoneManager.h"
#import "CurrentCallViewController.h"
#import "IncomingCallViewController.h"
#import "SYDataLoader.h"

@implementation AppDelegate
@synthesize loader;
@synthesize viewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{

    //  [[DeployGateSDK sharedInstance] launchApplicationWithAuthor:@"Maleandr" key:@"ccc68a89e41b52c0fe9813a9982c26d6e00e3c3c"];
   // [NSThread sleepForTimeInterval:2];
    self.operationQueue = [[NSOperationQueue alloc]init];
    [[SYAPIManager sharedInstance] getLimitsConfig:^(AFHTTPRequestOperation * request, id object){
        _limitsSettings = object;
    }
                                           failure:^(AFHTTPRequestOperation * request, NSError * error){
                                               // create default values for object
                                           }];
    [Fabric with:@[[Crashlytics class]]];
    _isAfterLaunch = true;
    _showRooms = false;
    
    _hasOPWeather = _hasGIOMWeather = _intercomShown = NO;
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:INTERCOM_ENABLED] != nil){
        _itercomEnabled = [[defaults objectForKey:INTERCOM_ENABLED] boolValue];
    } else {
        [defaults setObject:[NSNumber numberWithBool:_itercomEnabled] forKey:INTERCOM_ENABLED];
    }
    
    if(_itercomEnabled)
    [[LinphoneManager instance] startLibLinphone];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    self.inBackground=YES;
    [[SYWebSocket sharedInstance] disconnect];
    
    if(_itercomEnabled)
    [[LinphoneManager instance] resignActive];
    
}
UIBackgroundTaskIdentifier bgTask;
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    self.inBackground=YES;
    [[SYWebSocket sharedInstance] disconnect];
    
    if(![LinphoneManager isLinphoneCoreReady]) return;
    
    if(_itercomEnabled)
    [[LinphoneManager instance] enterBackgroundMode];
}



- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"foreground");
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [[SYWebSocket sharedInstance] connect];
    
    for(Elan *elan in [[SYCoreDataManager sharedInstance] getAllRFElans]){
        if(elan.selected){
            [[SYDataLoader sharedInstance]loadWeatherFromElan:elan];
        }
        
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncComplete)
                                                 name:@"dataSyncComplete"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStarted)
                                                 name:@"loadingStarted"
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStateChanged:)
                                                 name:LOADING_DEVICES
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStateChanged:)
                                                 name:LOADING_DEVICES_ENDED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStateChanged:)
                                                 name:LOADING_ROOMS
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStateChanged:)
                                                 name:LOADING_ROOMS_ENDED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStateChanged:)
                                                 name:LOADING_SCENES
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStateChanged:)
                                                 name:LOADING_SCENES_ENDED
                                               object:nil];
    
    if(_itercomEnabled)
    [[LinphoneManager instance] becomeActive];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(callUpdate:)
                                                 name:kLinphoneCallUpdate
                                               object:nil];
}

- (void)callUpdate:(NSNotification *)notif
{
    LinphoneCall *call = [[notif.userInfo objectForKey: @"call"] pointerValue];
    LinphoneCallState state = [[notif.userInfo objectForKey: @"state"] intValue];
    NSString *message = [notif.userInfo objectForKey: @"message"];
    
    NSLog(@"Message: %@, %u", message, state);
    //
    if (state == LinphoneCallReleased) {
        //[self dismissViewControllerAnimated:YES completion:^{}];
    }
    
    if (incomingCallExists == TRUE && activeCallExists == TRUE && state != LinphoneCallError && state != LinphoneCallEnd && state != LinphoneCallReleased) {
        return;
    }
    
    //
    
    if (incomingCallExists != TRUE && state == LinphoneCallIncomingReceived) { // novy prichozi hovor
        
        IncomingCallViewController * myViewController = [[IncomingCallViewController alloc] init];

            [self.window.rootViewController presentViewController:myViewController animated:NO completion:^{}];
        _intercomShown = YES;
        
        [myViewController setCall:call];
        
        incomingCallExists = TRUE;
    }
    
    else if (activeCallExists != TRUE && (state == LinphoneCallOutgoingInit || state == LinphoneCallConnected)) {
        
        if (incomingCallExists) {
            if(_intercomShown){
            [self.window.rootViewController dismissViewControllerAnimated:NO completion:^{}];
            _intercomShown = NO;
            }
        }
        
        NSLog(@"INCCALL EXIST %d",incomingCallExists);
        
        CurrentCallViewController * myViewController = [[CurrentCallViewController alloc] init];

            [self.window.rootViewController presentViewController:myViewController animated:NO completion:^{}];
        _intercomShown = YES;
            

        [myViewController setCall:call];
        
        incomingCallExists = FALSE;
        activeCallExists = TRUE;
    }
    
    else if (state == LinphoneCallEnd || state == LinphoneCallError) {
        activeCallExists = incomingCallExists = FALSE;
//        intercomActive = NO;
        
        if(_intercomShown){
        [self.window.rootViewController dismissViewControllerAnimated:YES completion:^{
            if (state == LinphoneCallError) {
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"VOIP_Message", nil) message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
                
                [alert show];
            }
        }];
        _intercomShown = NO;
        }
    }
    else if (state == LinphoneCallReleased) {
        //activeCallExists = incomingCallExists = FALSE;
        
        //[self dismissViewControllerAnimated:YES completion:^{}];
    }
}


- (void)loadingStateChanged:(NSNotification*) notification{
    if ([notification.name isEqualToString:LOADING_DEVICES]){
        _loadingDevices = true;
    } else if ([notification.name isEqualToString:LOADING_DEVICES_ENDED]){
        _loadingDevices = false;
    } else if ([notification.name isEqualToString:LOADING_ROOMS]){
        _loadingRooms = true;
    } else if ([notification.name isEqualToString:LOADING_ROOMS_ENDED]){
        _loadingRooms = false;
    } else if ([notification.name isEqualToString:LOADING_SCENES]){
        _loadingScenes = true;
    } else if ([notification.name isEqualToString:LOADING_SCENES_ENDED]){
        _loadingScenes = false;
    }
    
    if (!_loadingScenes && !_loadingRooms && !_loadingDevices){
        [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_DATA_ENDED object:self];
    }
}

- (BOOL)isLoadingData{
    if (!_loadingScenes && !_loadingRooms && _loadingDevices){
        return NO;
    }
    return YES;
}


- (void)loadingStarted{
    _loadingDevices = true;
}

- (void)syncComplete{
    _loadingDevices = false;
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[SYWebSocket sharedInstance] disconnect];
}




#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

@end
