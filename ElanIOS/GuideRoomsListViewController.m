//
//  GuideRoomsListViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 29.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "GuideRoomsListViewController.h"
#import "SYCoreDataManager.h"
#import "SYAPIManager.h"
#import "GuideRoomsAddViewController.h"

@interface GuideRoomsListViewController ()
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;
@property (nonatomic, retain)Room *selectedRoom;

@end

@implementation GuideRoomsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initFetchedResultsController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"editRoom"]){
        GuideRoomsAddViewController *controller = segue.destinationViewController;
        controller.room = _selectedRoom;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
  return [[_rooms fetchedObjects] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"roomLabel" forIndexPath:indexPath];
    
    UILabel *name = [[cell contentView]viewWithTag:1];
    Room *room = [_rooms objectAtIndexPath:indexPath];
    name.text = room.label;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectedRoom = [_rooms objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"editRoom" sender:nil];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSString* name = ((Room*)[_rooms objectAtIndexPath:indexPath]).label;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_room", nil) ,name] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
        alert.tag = indexPath.row;
        [alert show];
    }
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
        NSString* roomID = ((Room*)[_rooms objectAtIndexPath:indexPath]).roomID;
        Room*room =[_rooms objectAtIndexPath:indexPath];
        if (roomID!=nil){
            [[SYAPIManager sharedInstance] deleteRoom:room success:^(AFHTTPRequestOperation * operation, id object)
             {
                 [[SYCoreDataManager sharedInstance] deleteManagedObject:[[SYCoreDataManager sharedInstance] getRoomWithID:roomID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]]];
                 
             }failure:^(AFHTTPRequestOperation * operation, NSError * error){
                 UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"cannotDeleteRoom"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                 [alert show];
                 
             }];
        }
    }
}


#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Room"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _rooms = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetchRequest
                                 managedObjectContext:context
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    _rooms.delegate = self;
    NSError *error;
    
    if (![_rooms performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            self.emptyLabel.hidden = [_rooms.fetchedObjects count] > 0;
            
            break;
            
        case NSFetchedResultsChangeUpdate:{
            NSMutableArray *indexPaths = [NSMutableArray new];
            [indexPaths addObject:indexPath];
            [tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        }
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {

    [self.tableView endUpdates];
    self.emptyLabel.hidden = [_rooms.fetchedObjects count] > 0;
}


@end
