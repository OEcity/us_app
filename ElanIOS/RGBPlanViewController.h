//
//  RGBPlanViewController.h
//  iHC-MIRF
//
//  Created by Tom Odler on 07.04.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TouchDownGestureRecognizer.h"
@protocol rgbSetterDelegate
-(void)setRGB:(int)cRed green:(int)cGreen blue:(int)cBlue mode:(NSInteger)mode intensity:(int)intensity;
@end

@interface RGBPlanViewController : UIViewController
@property (nonatomic) NSInteger mode;
@property (nonatomic, retain) id <rgbSetterDelegate> myDelegate;
-(id)initWithDelegate:(id<rgbSetterDelegate>)delegate red:(int)cRed green:(int)cGreen blue:(int)cBlue mode:(NSInteger)mode intensity:(int)intensity;
@end
