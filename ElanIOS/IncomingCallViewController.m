//
//  IncomingCallViewController.m
//  iHC
//
//  Created by Pavel Gajdoš on 06.09.13.
//  Copyright (c) 2013 Pavel Gajdoš. All rights reserved.
//

#import "IncomingCallViewController.h"
#import "SYCoreDataManager.h"

@interface IncomingCallViewController ()

@end

@implementation IncomingCallViewController

@synthesize call;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification{
    NSLog(@"Crash: %@", [notification.userInfo objectForKey:@"callId"]);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"sdsdsds: %@", [userInfo objectForKey:@"callId"]);
}

-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
    NSLog(@"sdsdsds: %@", url);
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    [incomingCallLabel setText:NSLocalizedString(@"incomingCall", nil)];

    [unlockButton setTitle:NSLocalizedString(@"unlock", nil) forState:UIControlStateNormal];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    NSString*soundName = @"ring";
    NSString *soundFilePath = [NSString stringWithFormat:@"%@/%@.wav",
                               [[NSBundle mainBundle] resourcePath], soundName];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL
                                                     error:nil];
    _player.numberOfLoops = 0;
    
    [_player play];
}



- (void)viewWillDisappear:(BOOL)animated
{
    [_player stop];
//    [cameraView stop];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setCall:(LinphoneCall *)acall
{
    call = acall;
    
    [self update];
}


- (void)update
{
    const LinphoneAddress *addr = linphone_call_get_remote_address(call);
    
    NSString* username = nil;
    
    if (addr != NULL) {
        
        const char* username_char = linphone_address_get_username(addr);
        
        username = [NSString stringWithCString:username_char encoding:NSUTF8StringEncoding];
    }
    else {
        username = @"Unknown";
    }
    
    //
    
    NSString * name;
    
    IntercomContact * contact = [[SYCoreDataManager sharedInstance] getIntercomContact:username];
    
    if (contact) {
        name = contact.label;
        
        
        switchCode = contact.switchCode;
        [unlockButton setHidden:![LinphoneManager checkUnlockCode:switchCode]];
        [unlockButton setHidden:YES];
        

//        [cameraView setUrl:[NSURL URLWithString:[ self streamAddressUsingContact:contact]]];
        
//        [cameraView setHidden:NO];
//        [cameraView playWith:contact.username And:contact.password And:@""];
    }
    else {
        name = username;
        [unlockButton setHidden:YES];
    }
    [callerNameLabel setText:name];
}

-(NSString*)streamAddressUsingContact:(IntercomContact*)contact{
   NSString*path = [NSString stringWithFormat:@"http://%@%@",contact.ipAddress,@"/api/camera/snapshot?width=640&height=480&source=internal"];
    return path;
}

#pragma mark - Button event handlers

- (void)acceptCallButtonPressed:(id)sender
{
    if ([self call] != nil) {
        [[LinphoneManager instance] acceptCall:[self call]];
    }
        
    //[[self presentingViewController] dismissViewControllerAnimated:NO completion:^{}];
}

- (void)declineCallButtonPressed:(id)sender
{
    if ([self call] != nil) {
        [[LinphoneManager instance] declineCall:[self call]];
    }
    
//    [[self presentingViewController] dismissViewControllerAnimated:YES completion:^{}];
}

- (void)unlockButtonPressed:(id)sender
{    
    [[LinphoneManager instance] sendStringUsingDTMF:switchCode];
    
    [self declineCallButtonPressed:nil];
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
