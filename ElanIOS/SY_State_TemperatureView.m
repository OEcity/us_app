//
//  SY_State_TemperatureView.m
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 18/06/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "SY_State_TemperatureView.h"
@implementation SY_State_TemperatureView

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(void)setModeTemp{
    [_lTemp setHidden:NO];
    [_lTempIn setHidden:YES];
    [_lTempOut setHidden:YES];
}

-(void)setModeTempINnOUT{
    [_lTemp setHidden:YES];
    [_lTempIn setHidden:NO];
    [_lTempOut setHidden:NO];
}

@end
