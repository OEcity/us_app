//
//  GuideScenesAddViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 04.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "GuideScenesAddViewController.h"
#import "SYCoreDataManager.h"
#import "Constants.h"
#import "SceneRGBViewController.h"
#import "SceneDimmingViewController.h"
#import "SceneReleViewController.h"
#import "SceneBlindsViewController.h"
#import "SceneChromacityViewController.h"
#import "SYAPIManager.h"


@interface GuideScenesAddViewController (){
    NSString *mySceneName;
    
    BOOL elanSelected;
    BOOL devicesSelected;
    
    Elan*selectedElan;
}
@property (nonatomic) Device *selectedDevice;
@property (nonatomic) NSInteger selectedIndex;
@property (nonatomic) UITableView *myDevTbv;
@end

@implementation GuideScenesAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.selectedDevices = [[NSMutableDictionary alloc] init];
//    self.deviceActions =[[NSMutableDictionary alloc] init];
    self.actionsArray =[[NSMutableArray alloc] init];
    
    [self initFetchedResultsController];
    
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    
    self.selectedDevices = [[NSMutableDictionary alloc] init];
    
    if(_scene != nil){
        mySceneName = self.scene.label;
        selectedElan = _scene.elan;
        
        NSMutableArray *arrayForParse = [[NSMutableArray alloc]init];
        NSMutableDictionary *dictToArray = [[NSMutableDictionary alloc]init];
        
        for (SceneAction * action in self.scene.sceneAction) {
            if (action.device == nil) continue;
            
            [dictToArray setObject:action.action.name forKey:action.action.device.deviceID];
            [arrayForParse addObject:dictToArray];
            [dictToArray removeAllObjects];
            
            NSString * deviceName = [action.device.deviceID copy];
            Device * d = [[SYCoreDataManager sharedInstance] getDeviceWithID:deviceName];
            if(d) {
                [self.selectedDevices setObject:d forKey:deviceName];
            }
        }
        [self.tableView reloadData];
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    [_tableView reloadData];
    
//    elanSelected = devicesSelected = NO;
    NSLog(@"%@", self.actionsArray);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark textField Delegate
-(void)textFieldDidEndEditing:(UITextField *)textField{
    mySceneName = textField.text;
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return NO;
}

#pragma mark - UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView.tag == 3 && devicesSelected)return [[_fetchedResultsController fetchedObjects] count]+1;
    if(tableView.tag == 2 && elanSelected)return [[SYCoreDataManager sharedInstance]getAllElans].count+1;
    if(tableView.tag == 1)return 3;
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1 && indexPath.row == 1 && elanSelected) return [[SYCoreDataManager sharedInstance]getAllElans].count*50+50;
    if(tableView.tag == 1 && indexPath.row == 2 && devicesSelected) return [[_fetchedResultsController fetchedObjects] count]*50+50;
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        UITableViewCell * cell = nil;
        
        switch (indexPath.row) {
            case 0:{
                NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"enterName", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField *tf = (UITextField*)[[[cell contentView] subviews] firstObject];
                tf.delegate = self;
                tf.attributedPlaceholder = namePlaceHolder;
                if(mySceneName != nil){
                    tf.text = mySceneName;
                }
            }
                break;
            case 1:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 2;
                tbv.delegate = self;
                tbv.dataSource = self;
            }
                break;
            case 2:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 3;
                tbv.delegate = self;
                tbv.dataSource = self;
                _myDevTbv = tbv;
                }
            }
        return cell;
    } else if (tableView.tag == 2){
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        
        if(elanSelected){
            line.hidden = YES;
            
            if(indexPath.row>0){
                img.hidden = YES;
                label.text = ((Elan*)[[[SYCoreDataManager sharedInstance]getAllElans] objectAtIndex:indexPath.row-1]).label;
                if([label.text isEqualToString:selectedElan.label]){
                    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            } else {
                label.text = @"eLAN";
                if(!elanSelected && selectedElan){
                    label.text = selectedElan.label;
                }
                
                img.hidden = NO;
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            }
        } else {
            label.text = @"eLAN";
            line.hidden = NO;
            img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
        }
        return cell;
    } else {
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        UIButton *button = [[cell contentView] viewWithTag:5];
        NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section];
        
        if(devicesSelected){
            line.hidden = YES;
            
            if(indexPath.row>0){
                img.hidden = NO;
                Device *selctedDev = [_fetchedResultsController objectAtIndexPath:index];
                if ([self.selectedDevices objectForKey:selctedDev.deviceID] != nil){
                    
                    [img setImage:[UIImage imageNamed:@"fajfka_vyber_prvku.png"]];
                }else{
                    [img setImage:[UIImage imageNamed:@"krizek_vyber_prvku.png"]];
                }
                [button addTarget:nil action:@selector(removeDeviceFromArray:) forControlEvents:UIControlEventTouchUpInside];

                

                label.text = ((Device*)[_fetchedResultsController objectAtIndexPath:index]).label;
            } else {
                label.text = @"Devices";
                img.hidden = NO;
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            }
        } else {
            label.text = @"Devices";
            line.hidden = NO;
            img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
        }
        return cell;
    }
}

-(void)removeDeviceFromArray:(UIButton*)sender{
    
    
    UIButton *button = (UIButton *)sender;
    CGRect buttonFrame = [button convertRect:button.bounds toView:_myDevTbv];
    NSIndexPath *indexPath = [_myDevTbv indexPathForRowAtPoint:buttonFrame.origin];
    NSLog(@"indexpathrow: %ld", (long)indexPath.row);
    
    NSIndexPath *myIndex = [NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section];
    Device *selectedDevice = ((Device*)[_fetchedResultsController objectAtIndexPath:myIndex]);
    [self.selectedDevices removeObjectForKey:selectedDevice.deviceID];
    NSUInteger index = 0;
    NSMutableIndexSet *itemsToRemove = [[NSMutableIndexSet alloc] init];
    
    for (NSDictionary*dict in _actionsArray){
        if([[[dict allKeys] firstObject] isEqualToString:selectedDevice.deviceID]){
            [itemsToRemove addIndex:index];
        }
        index++;
    }
    
    [_actionsArray removeObjectsAtIndexes:itemsToRemove];
    NSLog(@"%@", _actionsArray);
    
    [_myDevTbv reloadData];
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 2 && indexPath.row == 0 && !elanSelected){
        elanSelected = YES;
        
        tableView.layer.borderColor = USBlueColor.CGColor;
        tableView.layer.borderWidth = 1.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        //        NSIndexPath *mIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
    } else if (tableView.tag == 2 && indexPath.row == 0 && elanSelected){
        elanSelected = NO;
        
        tableView.layer.borderColor = nil;
        tableView.layer.borderWidth = 0.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        //        NSIndexPath *mIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
    }    if(tableView.tag == 3 && indexPath.row == 0 && !devicesSelected){
        devicesSelected = YES;
        
        tableView.layer.borderColor = USBlueColor.CGColor;
        tableView.layer.borderWidth = 1.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        //        NSIndexPath *mIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
    } else if (tableView.tag == 3 && indexPath.row == 0 && devicesSelected){
        devicesSelected = NO;
        
        tableView.layer.borderColor = nil;
        tableView.layer.borderWidth = 0.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        //        NSIndexPath *mIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
    } else if (tableView.tag == 3 && indexPath.row>0){
        NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section];
        _selectedDevice = [_fetchedResultsController objectAtIndexPath:index];
        NSString* name  = _selectedDevice.deviceID;
        [self.selectedDevices setObject:_selectedDevice forKey:name];
        [self determineSegueWithDevice:_selectedDevice];
        self.selectedIndex = index.row;
        
        [tableView reloadData];
    } else if(tableView.tag == 2 && indexPath.row > 0){
        selectedElan = [[[SYCoreDataManager sharedInstance] getAllElans] objectAtIndex:indexPath.row-1];
    }
    
    if(indexPath.row == 0)
       [tableView reloadData];
}
-(void)determineSegueWithDevice:(Device*)device{
    NSLog(@"Device type: %@", device.productType);
    NSSet *actionsInfo = device.actions;
    
    if ([device hasStateName:@"brightness"] && ([device hasStateName:@"white balance"])){
        
        [self performSegueWithIdentifier:@"chromacity" sender:nil];
        
    }else if([device hasStateName:@"red"] && [device hasStateName:@"green"] && [device hasStateName:@"blue"] && [device hasStateName:@"demo"] && [device hasStateName:@"brightness"]) {
        
        [self performSegueWithIdentifier:@"rgb" sender:nil];
        
    } else if ([device hasStateName:@"brightness"]){
        
        [self performSegueWithIdentifier:@"dimming" sender:nil];
        
    } else if ([device getStateValueForStateName:@"roll up"]) {
        
        [self performSegueWithIdentifier:@"blinds" sender:nil];
        
    } else if([[actionsInfo filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"name == 'delayed on' OR name == 'delayed off'"]]count] > 1){
        
        [self performSegueWithIdentifier:@"rele" sender:nil];
        
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"rgb"]) {
        SceneRGBViewController *controller = (SceneRGBViewController*) [segue destinationViewController];
        controller.device = _selectedDevice;
        controller.guideController = self;
    } else if ([segue.identifier isEqualToString:@"dimming"]){
        SceneDimmingViewController *controller = (SceneDimmingViewController*) [segue destinationViewController];
        controller.device = _selectedDevice;
        controller.guideController = self;
    } else if ([segue.identifier isEqualToString:@"blinds"]){
        SceneBlindsViewController *controller = (SceneBlindsViewController*) [segue destinationViewController];
        controller.device = _selectedDevice;
        controller.guideController = self;
    } else if ([segue.identifier isEqualToString:@"chromacity"]){
        SceneChromacityViewController *controller = (SceneChromacityViewController*) [segue destinationViewController];
        controller.device = _selectedDevice;
        controller.guideController = self;
    } else if([segue.identifier isEqualToString:@"rele"]){
        SceneReleViewController *controller = (SceneReleViewController*) [segue destinationViewController];
        controller.device = _selectedDevice;
        controller.guideController = self;
    }
}

#pragma mark - Init

- (void)initFetchedResultsController {
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Device"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    //    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.actions[COUNT] > 0 AND SUBQUERY(self.states,$state,$state.name == 'locked' AND $state.value != 0).@count == 0"];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.actions.@count > 0 AND self.address != nil AND self.states.@count > 0 AND SUBQUERY(self.states,$s,($s.name MATCHES 'locked' AND $s.value != 0) OR $s.name MATCHES 'temperature' OR $s.name MATCHES 'temperature IN' OR $s.name MATCHES 'temperature OUT').@count == 0"]; //
    [fetchRequest setPredicate:predicate];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetchRequest
                                 managedObjectContext:context
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    _fetchedResultsController.delegate = self;
    NSError *error;
    
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
    [self.tableView reloadData];
}

#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.myDevTbv beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.myDevTbv;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            //            self.emptyLabel.hidden = [_fetchedResultsController.fetchedObjects count] > 0;
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.myDevTbv endUpdates];
}

-(NSDictionary*) storeToJSON{
    NSMutableDictionary* data = [NSMutableDictionary new];
    NSMutableArray* actions = [NSMutableArray new];
    

    [data setObject:mySceneName forKey:@"label"];
    
    [data setObject:_actionsArray forKey:@"actions"];
    
    if (self.scene == nil){
        return data;
    }
    [data setValue:_scene.sceneID forKey:@"id"];
    return data;
}
- (IBAction)saveScene:(id)sender {
    if(mySceneName == nil || [mySceneName isEqualToString:@""]){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"nameError", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if(_actionsArray.count == 0){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"selectDevices", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if(selectedElan == nil){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"selectElan", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    NSDictionary* json = [self storeToJSON];
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    [[SYAPIManager sharedInstance] postSceneWithID:_scene.sceneID withDict:json success:^(AFHTTPRequestOperation *operation, id response) {
        [_loaderDialog hide];
        [self performSegueWithIdentifier:@"sceneList" sender:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [_loaderDialog hide];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"cannotAddScene", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }];
}
@end
