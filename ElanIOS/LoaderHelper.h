//
//  LoaderHelper.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 2/14/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LoaderHelper : NSObject

+(LoaderHelper*) sharedLoaderHelper;
- (void)reloadDevicesAndRoomsOnCounterChange;
- (void)reloadDevicesAndRooms;
- (void)reloadDevices;
@property BOOL errorPopupped;

@property (nonatomic, retain) NSMutableArray * devices;
@end
