//
//  SceneDimmingViewController.h
//  Click Smart
//
//  Created by Tom Odler on 29.07.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
#import "SYAddSceneViewController.h"
#import "GuideScenesAddViewController.h"

@interface SceneDimmingViewController : UIViewController
@property Device *device;
@property (nonatomic) SYAddSceneViewController *controller;
@property (nonatomic) GuideScenesAddViewController *guideController;

@end
