//
//  DeviceTypeResponse.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/1/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "DeviceTypeResponse.h"
#import "SBJson.h"
#import "Device.h"
@implementation DeviceTypeResponse


-(NSObject*)parseResponse:(NSData *)inputData identifier:(NSString *)identifier{
    NSMutableArray * itemsArray = [[NSMutableArray alloc] init];

    SBJsonParser *parser2 = [[SBJsonParser alloc] init];
    NSString *inputString = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];

    NSDictionary* jsonObjects = [parser2 objectWithString:inputString];


    for(NSString* dict in jsonObjects)
    {

        Device *device = [[Device alloc] init];
        @try {
            device.type = dict;
            NSDictionary* newDict = [jsonObjects objectForKey:dict];
            NSDictionary* icons =  [newDict objectForKey:@"icons"];
            NSString* defaultIcon = [icons valueForKey:@"default"];
//            device.iconPrefix = defaultIcon;

        }
        @catch (NSException *exception) {
            //handle this exception !!

        }@finally {
            [itemsArray addObject:device];
        }
        
    }
    return itemsArray;
    
}

@end
