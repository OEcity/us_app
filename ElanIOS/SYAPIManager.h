//
//  SYAPIManager.h
//  IHC Mirf
//
//  Created by Vratislav Zima on 7/14/14.
//  Copyright (c) 2014 Quiche. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>
#import "CoreDataObjects.h"

@interface SYAPIManager : NSObject

@property(nonatomic, retain) AFHTTPRequestOperationManager *manager;
+(SYAPIManager*) sharedInstance;

-(void)reinit;

-(void)cancelAllOperations;

-(void)postWeatherCoordinates:(NSDictionary*)data toElan:(Elan*)elan
                      success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                      failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getWeatherFromElan:(Elan*)elan
                  success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)deleteWeatherFromElan:(Elan*)elan
                     success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getRoomsWithSuccessFromElan:(Elan*)elan
                           success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getRoomWithID:(NSString *) ID fromElan:(Elan*)elan
             success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)deleteRoom:(Room*)room
                success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)createRoomWithDictionary:(NSDictionary*)roomDictionary toElan:(Elan*)elan
                        success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)updateRoomWithDictionary:(NSDictionary*)roomDictionary toElan:(Elan*)elan
                        success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getDevicesWithSuccessForElan:(Elan*)elan
                            success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                            failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)deleteDevice:(Device*)device 
                success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)createDeviceWithLabel:(NSString *)label deviceType:(NSString*)deviceType productType:(NSString*)productType address:(NSNumber*)address toElan:(Elan*)elan
                     success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)updateDeviceWithLabel:(NSString *) label deviceType:(NSString*)deviceType productType:(NSString*)productType address:(NSNumber*)address id:(NSString*)deviceID toElan:(Elan*)elan
                     success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getDeviceState:(NSString*) deviceID fromElan:(Elan*)elan
              success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getDeviceWithID:(NSString *) ID fromElan:(Elan*)elan
               success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)updateDeviceStateWithDictionary:(NSDictionary *) state device:(Device*)device
                               success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

- (void)putDeviceAction:(NSDictionary*)action device:(Device*)device
                success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)putIRDeviceAction:(NSDictionary*)action deviceID:(NSString*)deviceID toElan:(Elan*)elan
                 success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                 failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void) saveIRActionWithSuccessToElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void) saveIRAction:(NSString*)id
        success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void) getScenesWithSuccessFromElan:(Elan*)elan
                             success:(void (^)(AFHTTPRequestOperation *, id))success
                             failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure;

-(void)getSceneWithID:(NSString *) ID fromElan:(Elan*)elan
              success:(void (^)(AFHTTPRequestOperation *, id))sucess
              failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure;

-(void)deleteSceneWithID:(NSString*)id fromElan:(Elan*)elan
            success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)performScene:(Scene*)scene
            success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
            failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)postTemperatureForDevice:(Device*)device temperature:(NSNumber*) temperature
                        success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)postWindowSensitivityForDevice:(Device*)device sensitivity:(NSNumber*)sensitivity
                              success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;


-(void)postWindowOffTimeForDevice:(Device*)device offtime:(NSNumber*)offtime
                          success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)postCorrectionForDevice:(Device*)device correction:(NSNumber*)correction
                       success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                       failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)postModeForDevice:(Device*)device
                success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                 failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)postControllForDevice:(Device*)device
                     success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getTemperatureSchedulesWithSuccessFromElan:(Elan*)elan
                       success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                       failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getScheduleWithID:(NSString *) ID fromElan:(Elan*)elan
                 success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getDeviceSchedulesWithSuccessFromElan:(Elan*)elan
                            success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getDeviceScheduleDetailsWithName:(NSString*)name fromElan:(Elan*)elan
                                success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getCentralSourceWithSuccess:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)postDataWithHeatCoolAreaToElan:(Elan*)elan dict:(NSDictionary*)HeatCoolAreaSettings
                        success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)deleteTemperatureScheduleWithId:(NSString*)id
        success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)postDataWithTemperatureSources:(NSDictionary*)TemperatureSourceSettings
                              success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)deleteTemperatureSourceWithId:(NSString*)id
    success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getLimitsConfig:(void (^)(AFHTTPRequestOperation *operation, id  response))success
               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)postDeviceActionWithDictionary:(NSDictionary*)deviceAction device:(Device*)device
                              success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getAPIRootWithSuccessForElan:(Elan*)elan
                    success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getAPIRootForURL:(NSURL*)url
                success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getFWFromUpdateServer:(NSURL*)url
                     success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;


-(void)postScheduleWithDictionary:(NSDictionary*)scheduleDictionary
                          success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)postDeviceScheduleWithDictionary:(NSDictionary*)scheduleDictionary
                                success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)postCentralSourceWithDictionary:(NSDictionary*)sourceDictionary
                               success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getTemperatureSourceDetailsWithName:(NSString*)name
                                   success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)deleteCentralSourceWithId:(NSString*)sourceName
                         success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                         failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)deleteDeviceScheduleWithId:(NSString*)id fromElan:(Elan*)elan
        success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;


-(void)getConfigForElan:(Elan*)elan
                success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getDataFromURL:(NSString*)url
              success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)getDataFromURL:(NSString*)url user: (NSString*) user password: (NSString*) password
              success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void) postSceneWithID:(NSString*)id withDict:(NSDictionary*)data
        success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void) sendIRDevice:(NSString *) label deviceType:(NSString*)deviceType productType:(NSString*)productType
             success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void) deleteIRDeviceWithID:(Device*)dev
                     success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;

-(void)updateIRDeviceWithDict:(NSDictionary*)dict toElan:(Elan*)elan
                      success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure;
@end
