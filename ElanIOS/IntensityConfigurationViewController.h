//
//  IntensityConfigurationViewController.h
//  ElanIOS
//
//  Created by Vratislav Zima on 6/13/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol intensitySetterDelegate
-(void)setBrightnessValue:(int)value mode:(NSInteger)mode;
@end

@interface IntensityConfigurationViewController : UIViewController

@property float value;
@property int resultValue;
@property (nonatomic) NSInteger mode;
@property (nonatomic, retain) id <intensitySetterDelegate> myDelegate;
-(id)initWithDelegate:(id<intensitySetterDelegate>)delegate value:(float)value mode:(NSInteger)mode;




@end
