//
//  SparovatDevicesViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/4/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYPairDevicesViewController.h"
#import "SYCoreDataManager.h"
#import "SBJson.h"
#import "ResourceTiles.h"
#import "SYPairFloorplanViewController.h"
#import "SYCoreDataManager.h"
#import "DeviceInRoom.h"
@interface SYPairDevicesViewController ()

@property (nonatomic, strong) NSFetchedResultsController* fetchedDevicesResultsController;
@property (nonatomic, strong) NSFetchedResultsController* fetchedRoomsResultsController;
@end

@implementation SYPairDevicesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.added = [[NSMutableArray alloc] init];
    _pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
    _pickerViewController.delegate = self;
    [_roomNameLabel setText:NSLocalizedString(@"roomName", nil)];
    [_roomLabel setText:NSLocalizedString(@"chooseRoomTitle", nil)];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    //self.devices = [[NSMutableArray alloc] initWithArray:[CoreDataManager readDevices]];
    self.rooms = [[SYCoreDataManager sharedInstance] getAllRooms];
    if (self.rooms!=nil)
    [self.roomsPicker reloadAllComponents];
    [self.tableView reloadData];

}
-(void)viewWillAppear:(BOOL)animated{
    [_leftBottomButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_rightBottomButton setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
    [_iconLabel setText:NSLocalizedString(@"icon", nil)];
    [self initFetchedResultsController];
//    [_roomNameLabel setText:NSLocalizedString(@"roomName", nil)];
//    [_roomLabel setText:NSLocalizedString(@"chooseRoomTitle", nil)];
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    _fetchedDevicesResultsController = nil;
    _fetchedRoomsResultsController = nil;
}
#pragma mark - Init

-(void)initFetchedResultsController {
    [self initDevicesFetchedResultsController];
    [self initRoomsFetchedResultsController];
}

- (void)initDevicesFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Device"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _fetchedDevicesResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetchRequest
                                 managedObjectContext:context
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    _fetchedDevicesResultsController.delegate = self;
    NSError *error;
    
    if (![_fetchedDevicesResultsController performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}

- (void)initRoomsFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Room"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _fetchedRoomsResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetchRequest
                                 managedObjectContext:context
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    _fetchedRoomsResultsController.delegate = self;
    NSError *error;
    
    if (![_fetchedRoomsResultsController performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}
#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            //self.emptyLabel.hidden = [_fetchedDevicesResultsController.fetchedObjects count] > 0;
            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {

    if (indexPath == nil || indexPath.length == 0){
        return;
    }

    Device *selectedDevice = [_fetchedDevicesResultsController objectAtIndexPath:indexPath];
    UILabel * deviceName = (UILabel*)[cell viewWithTag:2];
    UIImageView * deviceIcon = (UIImageView*)[cell viewWithTag:1];
    UIImageView * deviceFavourite = (UIImageView*)[cell viewWithTag:3];
    
    [deviceName setTextAlignment:NSTextAlignmentLeft];
    //[deviceName setTextColor:[UIColor colorWithRed:1.0 green:1 blue:1 alpha:1]];
    UIFont* boldFont = [UIFont systemFontOfSize:17];
    while ([cell viewWithTag:201]!=nil){
        [[cell viewWithTag:201] removeFromSuperview];
    }
    [deviceName setFont:boldFont];
    
    
    deviceName.text = selectedDevice.label;
    if (deviceName.text==nil){
        deviceName.text = selectedDevice.label;
    }
    [[cell viewWithTag:99] removeFromSuperview];
 
    deviceIcon.image = [selectedDevice imageOff];
    deviceIcon.highlightedImage = [selectedDevice imageOn];
    if (self.roomName ==nil) return;
    
    bool found = NO;
    for(NSString* deviceID in self.added){
        if ([deviceID isEqualToString:selectedDevice.deviceID]){
            found=YES;
        }
    }
    
    if (found==NO){
        if ([selectedDevice.inRooms containsObject:_room]){
            [_added addObject:[selectedDevice.deviceID copy]];
            [deviceFavourite setImage:[UIImage imageNamed:@"fajfka.png"]];
        }else{
            [deviceFavourite setImage:[UIImage imageNamed:@"krizek.png"]];
        }
    }else{
        
        [deviceFavourite setImage:[UIImage imageNamed:@"fajfka.png"]];
    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {


    return [_fetchedDevicesResultsController.fetchedObjects count] ;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"MyCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    Device *selectedDevice = [_fetchedDevicesResultsController objectAtIndexPath:indexPath];
    NSString* name  = selectedDevice.deviceID;
    UIImageView* check = (UIImageView *)[cell viewWithTag:3];

    if([self.added containsObject:selectedDevice.deviceID]){
    [check setImage:[UIImage imageNamed:@"krizek.png"]];
        NSArray * myArr = [self.added copy];
        for (NSString * dev in myArr){
            if ([name isEqualToString:dev]){
                [self.added removeObject:dev];
            }
        }
    }else{
        [check setImage:[UIImage imageNamed:@"fajfka.png"]];
        [self.added addObject:selectedDevice.deviceID];
    }
    
}

-(IBAction)nextStep:(id)sender{
    if (self.roomName == nil){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"selectRoom", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    [self performSegueWithIdentifier:@"pairFloorplan" sender:self];


}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if  ([segue.destinationViewController isKindOfClass:[SYPairFloorplanViewController class]]){
        SYPairFloorplanViewController*dest = (SYPairFloorplanViewController*)segue.destinationViewController;
        dest.selectedDevices = [[NSMutableArray alloc] init];
        for (NSString * deviceID in _added){
            Device *selected = [[SYCoreDataManager sharedInstance] getDeviceWithID:deviceID];
            if (selected!=nil)
                [dest.selectedDevices addObject:selected];
        }
        dest.selectedRoom = self.room;
        dest.parent = self;
    }

}
-(IBAction)back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];

}

-(IBAction)openRooms:(id)sender{
    [[[[self.view superview] superview] superview] addSubview:_pickerViewController.view ];

}

#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
   if (!_fetchedRoomsResultsController || !_fetchedRoomsResultsController.fetchedObjects.count) return 0;
    return [_fetchedRoomsResultsController.fetchedObjects count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    if (!_fetchedRoomsResultsController || !_fetchedRoomsResultsController.fetchedObjects.count) return nil;
    Room * room = [_fetchedRoomsResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:row inSection:component]];
    return room.label;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    _pickerViewController.row = row;
    _pickerViewController.component= component;

}
-(void)DoneButtonTappedForRow:(NSInteger)row inComponent:(NSInteger)component{
    if (!_fetchedRoomsResultsController || !_fetchedRoomsResultsController.fetchedObjects.count) return;
    _room = [_fetchedRoomsResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:row inSection:component]];
    self.roomName = _room.roomID;
    UIImage *tileOn = [[ResourceTiles generateTileForName:_room.type] tile_on];
    [self.roomIcon setImage:tileOn];

    
    self.added = [[NSMutableArray alloc] init];
    
    for (DeviceInRoom * deviceInRoom in _room.devicesInRoom){
        [self.added addObject:[deviceInRoom.device.deviceID copy]];
    }
    [self.tableView reloadData];
    [self.roomLabel setText:_room.label];
}

- (NSUInteger)supportedInterfaceOrientations
{

    return UIInterfaceOrientationMaskPortrait;
}

@end
