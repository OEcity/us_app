//
//  CheckedActionCell.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 18/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "CheckedActionCell.h"

@implementation CheckedActionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

}

@end
