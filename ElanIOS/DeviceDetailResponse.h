//
//  DeviceDetailResponse.h
//  ElanIOS
//
//  Created by Vratislav Zima on 5/30/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"


@interface DeviceDetailResponse : NSObject <Response>

@property (copy) NSString * name;
@property (copy) NSString * url;

@end
