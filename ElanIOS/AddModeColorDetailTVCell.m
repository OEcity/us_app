//
//  AddModeColorDetailTVCell.m
//  iHC-MIRF
//
//  Created by admin on 15.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "AddModeColorDetailTVCell.h"

@implementation AddModeColorDetailTVCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    UIColor *color = self.colorView.backgroundColor;
    UIColor *color1 = self.whiteView.backgroundColor;
    [super setSelected:selected animated:animated];
    
    if (selected){
        self.colorView.backgroundColor = color;
        self.whiteView.backgroundColor = color1;
    }
}

-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    UIColor *color = self.colorView.backgroundColor;
    UIColor *color1 = self.whiteView.backgroundColor;
    [super setHighlighted:highlighted animated:animated];
    
    if (highlighted){
        self.colorView.backgroundColor = color;
        self.whiteView.backgroundColor = color1;
    }
}



@end
