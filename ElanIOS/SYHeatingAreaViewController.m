////
//  SYHeatingAreaViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/4/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "SYHeatingAreaViewController.h"

@interface SYHeatingAreaViewController ()

@end

@implementation SYHeatingAreaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [_lSectionName setText:NSLocalizedString(@"time_schedule_settings_title", nil) ];
    [_lCentralSourceTitle setText:NSLocalizedString(@"heating_source_settings_title", nil) ];
    [_lHeatingAreaTitle setText:NSLocalizedString(@"heating_area_settings_title", nil) ];
    [_lTimeScheduleTitle setText:NSLocalizedString(@"time_schedule_settings_title", nil) ];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"embedContainer"]) {
        self.containerViewController = segue.destinationViewController;
    }
}


- (IBAction)sectionButtonClicked:(id)sender {
    [_bTimeSchedule setBackgroundImage:[UIImage imageNamed:@"dlazdice_off"] forState:UIControlStateNormal];
    [_bHeatingArea setBackgroundImage:[UIImage imageNamed:@"dlazdice_off"] forState:UIControlStateNormal];
    [_bCentralSource setBackgroundImage:[UIImage imageNamed:@"dlazdice_off"] forState:UIControlStateNormal];
    UIButton* button = (UIButton*)sender;
    [button setBackgroundImage:[UIImage imageNamed:@"dlazdice_on"] forState:UIControlStateNormal];
    switch (((UIView*)sender).tag) {
        case 1:
            [_iVTimeSchedule setImage:[UIImage imageNamed:@"calendar_ico_on"]];
            [_iVHeatingArea setImage:[UIImage imageNamed:@"heat_area_off"]];
            [_iVCentralSource setImage:[UIImage imageNamed:@"central_source_off"]];
            [_lSectionName setText:NSLocalizedString(@"time_schedule_settings_title", nil) ];
            [self.containerViewController setViewController:@"timeSchedule"];

            break;
        case 2:
            [_iVTimeSchedule setImage:[UIImage imageNamed:@"caledar_ico_off"]];
            [_iVHeatingArea setImage:[UIImage imageNamed:@"heat_area_on_oprava"]];
            [_iVCentralSource setImage:[UIImage imageNamed:@"central_source_off"]];
            [_lSectionName setText:NSLocalizedString(@"heating_area_settings_title", nil) ];
            [self.containerViewController setViewController:@"heatingArea"];

            break;
        case 3:
            [_iVTimeSchedule setImage:[UIImage imageNamed:@"caledar_ico_off"]];
            [_iVHeatingArea setImage:[UIImage imageNamed:@"heat_area_off"]];
            [_iVCentralSource setImage:[UIImage imageNamed:@"central_source_on"]];
            [_lSectionName setText:NSLocalizedString(@"heating_source_settings_title", nil) ];
            [self.containerViewController setViewController:@"centralSource"];

            break;
            
    }

}

- (NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}



@end
