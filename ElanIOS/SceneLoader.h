//
//  SceneLoader.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 7/31/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "URLConnectionListener.h"
#import "URLConnector.h"
#import "Scene.h"
#import "Action.h"
#import "SYCoreDataManager.h"
@interface SceneLoader : NSObject <URLConnectionListener>

@property (nonatomic, retain) NSMutableArray * scenes;
@property BOOL isLoading;

-(void) LoadScenes:(id)sender;

@end
