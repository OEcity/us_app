//
//  IPTableViewCell.h
//  iHC-MIRF
//
//  Created by Vlastimil Venclik on 26.04.14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IPTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UIImageView *btnImage;

@end
