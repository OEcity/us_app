//
//  ReleViewController.h
//  Click Smart
//
//  Created by Tom Odler on 09.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device+Action.h"

@interface ReleViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate>
@property (nonatomic, strong) NSFetchedResultsController* devices;
@property (nonatomic)Device *device;

@end
