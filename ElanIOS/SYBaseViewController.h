//
//  SYBaseViewController.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 14.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SYBaseViewController : UIViewController

- (void)initLocalizableStrings;

@end
