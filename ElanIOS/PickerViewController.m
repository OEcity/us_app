//
//  PickerViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 12/15/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "PickerViewController.h"

@interface PickerViewController ()

@end

@implementation PickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _pickerView.delegate = self;
    [_bDone setTitle:NSLocalizedString(@"confirmation.done", nil) forState:UIControlStateNormal];
    _pickerView.dataSource = (id<UIPickerViewDataSource>)_delegate;
    _pickerView.delegate = (id<UIPickerViewDelegate>)_delegate;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    _row=0;
    _component=0;
    [_pickerView reloadAllComponents];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)cancelView:(id)sender {
    [self.view removeFromSuperview];
}


- (IBAction)SelectButtonDetected:(id)sender {

    [self.view removeFromSuperview];
    if ([_pickerView numberOfRowsInComponent:_component]>0)
    [_delegate DoneButtonTappedForRow:_row inComponent:_component];


}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _row = row;
    _component= component;
}

-(void)setDelegate:(id<PickerViewDelegate>)delegate{
    _delegate = delegate;
}

@end
