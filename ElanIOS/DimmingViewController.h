//
//  DimmingViewController.h
//  US App
//
//  Created by Tom Odler on 23.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device+State.h"

@interface DimmingViewController : UIViewController<NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) NSFetchedResultsController* devices;
@property Device *device;
@property int brightness;

@end
