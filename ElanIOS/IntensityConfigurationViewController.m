//
//  IntensityConfigurationViewController.m
//  ElanIOS
//
//  Created by Vratislav Zima on 6/13/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "IntensityConfigurationViewController.h"
#import "EFCircularSlider.h"

@interface IntensityConfigurationViewController ()
@property EFCircularSlider* circularSlider;
@property (weak, nonatomic) IBOutlet UIView *topSliderView;
@property (weak, nonatomic) IBOutlet UILabel *topSlideViewValueIndicatorLabel;
@end

@implementation IntensityConfigurationViewController


-(id)initWithDelegate:(id<intensitySetterDelegate>)delegate value:(float)value mode:(NSInteger)mode{
    self = [[IntensityConfigurationViewController alloc] initWithNibName:@"IntensityConfigurationViewController" bundle:nil];
    
    if(self != nil){
        _myDelegate = delegate;
        _value = value;
        _mode = mode;
        
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
   // [self.name setText: device.label];
    CGRect sliderFrame = CGRectMake(0, 0, _topSliderView.frame.size.width, _topSliderView.frame.size.height);
    _circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    [_circularSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [_topSliderView addSubview:_circularSlider];
    [_circularSlider pauseAutoredrawing];
    
    //GET DEVICE BRIGHTNESS
    
    CGFloat dash[]={1,15};
    
    [_circularSlider setHandleRadius:10];
    [_circularSlider setUnfilledLineDash:dash andCount:2];
    [_circularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_circularSlider setUnfilledColor:[UIColor whiteColor]];
    [_circularSlider setFilledColor:[UIColor whiteColor]];
    [_circularSlider setHandleColor:[UIColor whiteColor]];
    [_circularSlider setLineWidth:1];
    [_circularSlider setArcStartAngle:150];
    [_circularSlider setArcAngleLength:240];
    
    [_circularSlider setCurrentArcValue:_value forStartAnglePadding:10 endAnglePadding:10];

}

-(void)valueChanged:(EFCircularSlider*)circularSlider {
    _value =  [self nearestNumber:(int)[circularSlider getCurrentArcValueForStartAnglePadding:10 endAnglePadding:10] step:10];
    _topSlideViewValueIndicatorLabel.text = [NSString stringWithFormat:@"%d", (int)_value];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(int) nearestNumber:(int)num step:(int)step{
    NSLog(@"input number : %d step is %d", num, step);
        if (num % step == 0)
            NSLog(@"OK");
        else if (num % step < ((float)step/2.0))
            num = num - num % step;
        else
            num = num + (step - num % step);
        return num;
}
- (IBAction)closeTap:(id)sender {
    [_myDelegate setBrightnessValue:_value mode:_mode];
    [self.view removeFromSuperview];
}

@end
