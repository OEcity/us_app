//
//  SceneReleViewController.m
//  Click Smart
//
//  Created by Tom Odler on 10.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "SceneReleViewController.h"
#import "Constants.h"


const NSString* delayedOnSetTime = @"delayed on: set time";
const NSString* delayedOffSetTime = @"delayed off: set time";
const NSString* delayedOn = @"delayed on";
const NSString* delayedOff = @"delayed off";
const NSString* automat = @"automat";

@interface SceneReleViewController ()
@property (weak, nonatomic) IBOutlet UILabel *headerLabel2;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel1;

@property (weak, nonatomic) IBOutlet UIView *timePickerView;

@property (weak, nonatomic) IBOutlet UIView *topPickerView;

@property (weak, nonatomic) IBOutlet UIPickerView *powerOnPicker;

@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *startButton;

@property (weak, nonatomic) IBOutlet UIButton *delayedOnButton;
@property (weak, nonatomic) IBOutlet UIButton *delayedOffButton;
@property (weak, nonatomic) IBOutlet UIButton *automatButton;

@property (strong, nonatomic) NSNumberFormatter *formatter;

@property (nonatomic) int seconds1;
@property (nonatomic) int seconds2;
@property (nonatomic) int minutes1;
@property (nonatomic) int minutes2;
@property (nonatomic) int resultTime1;
@property (nonatomic) int resultTime2;

@property (nonatomic) BOOL delayedONOpened;
@property (nonatomic) BOOL delayedON;
@property (nonatomic) BOOL delayedOFF;
@property (nonatomic) BOOL save;
@property (nonatomic) BOOL start;
@property (nonatomic) BOOL automat;

@end

@implementation SceneReleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _delayedONOpened = _delayedON = _delayedOFF = _save = _start = NO;
    
    // Do any additional setup after loading the view.
    _powerOnPicker.delegate = self;
    _powerOnPicker.dataSource = self;
    
    _formatter = [[NSNumberFormatter alloc] init];
    [_formatter setNumberStyle:NSNumberFormatterNoStyle];
    [_formatter setMaximumIntegerDigits:2];
    [_formatter setMinimumIntegerDigits:2];
    
    _cancelButton.layer.borderWidth = 1.0f;
    _cancelButton.layer.borderColor = USBlueColor.CGColor;
    
    _startButton.layer.borderWidth = 1.0f;
    _startButton.layer.borderColor = USBlueColor.CGColor;
    
    _saveButton.layer.borderWidth = 1.0f;
    _saveButton.layer.borderColor = USBlueColor.CGColor;
        
    _topPickerView.layer.borderWidth = 1.0f;
    _topPickerView.layer.borderColor = USBlueColor.CGColor;

    _headerLabel1.text = NSLocalizedString(@"actionTimedOff", @"");
    _headerLabel2.text = NSLocalizedString(@"actionTimedOn", @"");
    
    [_saveButton setBackgroundImage:[self imageWithColor:USBlueColor] forState:UIControlStateHighlighted];
    [_cancelButton setBackgroundImage:[self imageWithColor:USBlueColor] forState:UIControlStateHighlighted];
    [_startButton setBackgroundImage:[self imageWithColor:USBlueColor] forState:UIControlStateHighlighted];
    
    [_saveButton setBackgroundImage:[self imageWithColor:USBlueColor] forState:UIControlStateSelected];
    [_cancelButton setBackgroundImage:[self imageWithColor:USBlueColor] forState:UIControlStateSelected];
    [_startButton setBackgroundImage:[self imageWithColor:USBlueColor] forState:UIControlStateSelected];
    
    _delayedON = _delayedOFF = _delayedONOpened = NO;
    
    
    [self loadParametersFromArray];
//    [self getMinutesAndSeconds];
}

-(void)loadParametersFromArray{
    NSArray *mySceneArray = nil;
    
    if(_controller != nil){
        mySceneArray = _controller.actionsArray;
    } else {
        mySceneArray = _guideController.actionsArray;
    }
    
    NSInteger index = 0;
    NSMutableIndexSet *indexForDelete = [[NSMutableIndexSet alloc]init];
    for(NSDictionary*dict in mySceneArray){
        NSString *deviceID =[[dict allKeys]firstObject];
        if([deviceID isEqualToString:_device.deviceID]){
            NSDictionary *myActionsDict = [dict objectForKey:deviceID];
            
            _delayedON = [myActionsDict objectForKey:delayedOn]?YES:NO;
            _delayedOFF = [myActionsDict objectForKey:delayedOff]?YES:NO;
            
            if([myActionsDict objectForKey:delayedOn] || [myActionsDict objectForKey:delayedOff]){
                _start = YES;
            }
            
            if([myActionsDict objectForKey:delayedOnSetTime] || [myActionsDict objectForKey:delayedOffSetTime]){
                _save = YES;
                
                if([myActionsDict objectForKey:delayedOnSetTime]){
                    _resultTime1 = [[myActionsDict objectForKey:delayedOnSetTime] intValue];
                    _minutes1 = _resultTime1/60;
                    _seconds1 = _resultTime1%60;
                } else {
                    _resultTime2 = [[myActionsDict objectForKey:delayedOffSetTime]intValue];
                    _minutes2 = _resultTime2/60;
                    _seconds2 = _resultTime2%60;
                }
            }

            [self selectButtons];
            
            [indexForDelete addIndex:index];
        }
        index++;
    }
    
    if ([indexForDelete count] > 0){
        if(_guideController)
            [_guideController.actionsArray removeObjectsAtIndexes:indexForDelete];
        else
            [_controller.actionsArray removeObjectsAtIndexes:indexForDelete];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _timePickerView.hidden = YES;
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 60;
    
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width/2-25, 44)];
    label.backgroundColor = [UIColor blackColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Roboto-Light" size:18];
    if (component == 0){
        label.textAlignment = NSTextAlignmentRight;
        label.text = [NSString stringWithFormat:@"%li",(long)(row)];
        
    }
    else {
        label.textAlignment = NSTextAlignmentLeft;
        label.text = [_formatter stringFromNumber:[NSNumber numberWithInteger:row]];
    }
    return label;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(_delayedONOpened){
        
        if(component == 0){
            _minutes1 = (int)row;
        } else {
            _seconds1 = (int)row;
        }
        _resultTime1 = (_minutes1*60) + _seconds1;
        
    } else {
        
        if(component == 0){
            _minutes2 = (int)row;
        } else {
            _seconds2 = (int)row;
        }
        _resultTime2 = (_minutes2*60) + _seconds2;
        
    }
    NSLog(@"time1: %d, time2: %d", _resultTime1, _resultTime2);
}

- (IBAction)delayedOnButtonTapped:(id)sender {
    if(_delayedOFF){
        _start = NO;
        _save = NO;
        _delayedOFF = false;
    
        _minutes2 = 0;
        _seconds2 = 0;
    }
     _timePickerView.hidden = NO;
    
    [_powerOnPicker selectRow:_minutes1 inComponent:0 animated:NO];
    [_powerOnPicker selectRow:_seconds1 inComponent:1 animated:NO];

    _delayedON = true;
    _delayedONOpened = true;
    [self selectButtons];
}

- (IBAction)delayedOffButtonTapped:(id)sender {
    if(_delayedON){
        _start = NO;
        _save = NO;
        _delayedON = NO;
        
        _minutes1 = 0;
        _seconds1 = 0;
    }
     _timePickerView.hidden = NO;
    
    [_powerOnPicker selectRow:_minutes2 inComponent:0 animated:NO];
    [_powerOnPicker selectRow:_seconds2 inComponent:1 animated:NO];

    _delayedOFF = true;
    _delayedONOpened = false;
    [self selectButtons];
}

- (IBAction)startButtonTapped:(id)sender {
    if(_start){
        _start = NO;
    } else {
        _start = YES;
    }
    [self selectButtons];
}

- (IBAction)saveButtonTapped:(id)sender {
    if(_save){
        _save = NO;
    } else {
    _save = YES;
    }
    [self selectButtons];
}

- (IBAction)cancelTapped:(id)sender {
    [_timePickerView setHidden:YES];
}

-(void)selectButtons{
    if(_save){
        [_saveButton setSelected:YES];
    } else {
        [_saveButton setSelected:NO];
    }
    
    if(_start){
        [_startButton setSelected:YES];
    } else {
        [_startButton setSelected:NO];
    }
    
    if(_delayedON){
        [_delayedOnButton setSelected:YES];
    } else {
        [_delayedOnButton setSelected:NO];
    }
    
    if(_delayedOFF){
        [_delayedOffButton setSelected:YES];
    } else {
        [_delayedOffButton setSelected:NO];
    }
    
    if(_automat){
        [_automatButton setSelected:YES];
    } else {
        [_automatButton setSelected:NO];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    if(_delayedON){
        if(_save){
            [dict setObject:[NSNumber numberWithInt:_resultTime1] forKey:delayedOnSetTime];
        }
        
        if(_start){
            [dict setObject:[[NSNull alloc]init] forKey:delayedOn];
        }
    } else if (_delayedOFF){
        if(_save){
            [dict setObject:[NSNumber numberWithInt:_resultTime2] forKey:delayedOffSetTime];
        }
        
        if(_start){
            [dict setObject:[[NSNull alloc]init] forKey:delayedOff];
        }
    } else if(_automat){
        [dict setObject:[NSNumber numberWithBool:YES] forKey:automat];
    }
    
    if([dict count]>0){
        NSMutableDictionary *dictToSend = [[NSMutableDictionary alloc]init];
        [dictToSend setObject:dict forKey:_device.deviceID];
        
        if(_controller){
            [_controller.actionsArray addObject:dictToSend];
            [[self.controller selectedDevices] setObject:_device forKey:_device.deviceID];
        } else {
            [_guideController.actionsArray addObject:dictToSend];
            [[self.guideController selectedDevices] setObject:_device forKey:_device.deviceID];
        }        
    }
}
@end
