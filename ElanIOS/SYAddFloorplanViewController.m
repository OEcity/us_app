//
//  AddFloorplanViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/1/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYAddFloorplanViewController.h"
#import "SBJson.h"
#import "Util.h"
#import "AppDelegate.h"
#import "SYAPIManager.h"
#import "SYCoreDataManager.h"
@interface SYAddFloorplanViewController ()

@end

@implementation SYAddFloorplanViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.operationQueue = [[NSOperationQueue alloc]init];
    _pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
    _pickerViewController.delegate = self;
    [_save setTitle:NSLocalizedString(@"newFloorplan", nil) forState:UIControlStateNormal];

	// Do any additional setup after loading the view.
}

-(void) viewDidAppear:(BOOL)animated{
    if (self.floorplan!=nil){
        [self.name setText:self.floorplan.name];
        if (self.floorplan.image != nil)
            [self.sourceImage setImage:self.floorplan.image];
        else{
            NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadImageFromElan:) object:nil];
            [self.operationQueue addOperation:invocationOperation];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [_leftBottomButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_nameLabel setText:NSLocalizedString(@"floorplanAddName", nil)];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];

}
-(IBAction)closeKeyboard:(id)sender{
    [self.name resignFirstResponder];

}
-(IBAction)back:(id)sender{
    [self.containerViewController setViewController:@"floorPlan"];
}
-(IBAction)storeDevice:(id)sender{
    
    NSDictionary *argumentDictionary = [NSDictionary dictionaryWithObjectsAndKeys:/*UIImagePNGRepresentation(self.imageData)*/ UIImageJPEGRepresentation(self.imageData, 0.8), @"data",[@"floorplans/" stringByAppendingString:[self processName]] , @"url", nil];
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadDataAsychn:) object:argumentDictionary];
    [app.operationQueue addOperation:invocationOperation];

    /*[[SYAPIManager sharedInstance] postFloorplanWithPath:[self processName] andImage:self.imageData success:^(AFHTTPRequestOperation* operation, id result){
        NSLog(@"Saved floorplan");
        [self showImage:nil];
    } failure:^(AFHTTPRequestOperation*operation, NSError * error){
        NSLog(@"Failed Save floorplan");
        [self failedUpload:nil];
    }];
     */
    [self.actInd startAnimating];

}

-(NSString*)processName{
    if ([self.name.text hasSuffix:@".jpeg"]) return self.name.text;
   return [[self.name.text stringByReplacingOccurrencesOfString:@" " withString:@"_"] stringByAppendingString:@".jpeg"];

}

-(IBAction)submit:(id)sender{
    if (self.imageData==nil){
        [self showPicker:self];
    }else{
        if ([self.name.text isEqualToString:@""]){
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"floorplanAddName", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            return;
        }
        [self storeDevice:self];
    }


}



-(IBAction)takePhoto:(id)sender{

    if ([UIImagePickerController isSourceTypeAvailable:
         UIImagePickerControllerSourceTypeCamera] == NO)
        return;


    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
    if (self.fromCamera==NO){
        cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    }
    // Displays a control that allows the user to choose picture or
    // movie capture, if both are available:
    cameraUI.mediaTypes =
    [UIImagePickerController availableMediaTypesForSourceType:
     UIImagePickerControllerSourceTypeCamera];

    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    cameraUI.allowsEditing = NO;

    cameraUI.delegate = self;

    [self presentViewController:cameraUI animated:YES completion:nil];
    return ;

}

- (void) imagePickerController: (UIImagePickerController *) picker
 didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    UIImage* image = [info objectForKey:UIImagePickerControllerOriginalImage];
    CGSize size;

    if (image.size.width > image.size.height){
        size = CGSizeMake(640, 480);
    }else{
        size = CGSizeMake(480, 640);
    }

    UIGraphicsBeginImageContext(size);
    [image drawInRect:CGRectMake(0,0,size.width,size.height)];
    self.imageData = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    
    [self.sourceImage setImage:self.imageData];
    [self.save setTitle:@"Uložit" forState:UIControlStateNormal];
    [picker dismissViewControllerAnimated:NO completion:nil];
}

-(void)loadImageFromElan:(NSMutableDictionary*)dict
{
    NSString * imageURl = self.floorplan.url;
    UIActivityIndicatorView * actInd = [dict objectForKey:@"ind"];
    
    NSData *imageData=[[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:imageURl]];
    UIImage *image=[[UIImage alloc]initWithData:imageData];
    NSMutableDictionary* imgDict = [[NSMutableDictionary alloc] init];
    [imgDict setValue:image forKey:@"image"];
    [imgDict setValue:actInd forKey:@"ind"];
    [imgDict setValue:imageURl forKey:@"imageNo"];
    [self performSelectorOnMainThread:@selector(showImageFromElan:) withObject:imgDict waitUntilDone:NO];
}

-(void)showImageFromElan:(NSMutableDictionary *)Dict
{
    UIImage * image = [Dict objectForKey:@"image"];
    [self.sourceImage setImage:image];
}


-(void)loadDataAsychn:(NSDictionary*)dict
{
    NSString* url =[dict valueForKey:@"url"];
    NSString *baseURL = [NSString stringWithFormat:@"http://%@/api/",[[[SYCoreDataManager sharedInstance] getCurrentElan] baseAddress]];

    url = [baseURL stringByAppendingString:url];
    NSData* imageData =[dict valueForKey:@"data"];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[imageData length]];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:1000] ;

    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"image/jpeg" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:imageData];
    NSURLResponse* response;
    NSError* error = nil;
    
    //ASIFormDataRequest * requestASI =[ASIFormDataRequest requestWithURL:[NSURL URLWithString:url]];
//    [requestASI setData:imageData withFileName:@"myphoto.png" andContentType:@"image/png" forKey:@"photo"];

    //Capturing server response
    [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
  //  [requestASI startSynchronous];
   // error= [requestASI error];
    NSMutableDictionary * imgDict = [[NSMutableDictionary alloc] init];
    [imgDict setValue:url forKey:@"imageNo"];
    if (error==nil)
        [self performSelectorOnMainThread:@selector(showImage:) withObject:imgDict waitUntilDone:NO];
    else{
        [self performSelectorOnMainThread:@selector(failedUpload:) withObject:imgDict waitUntilDone:NO];
    }
}
-(void)showImage:(NSMutableDictionary *)Dict
{
    [self.actInd stopAnimating];
    [self back:self];

}

-(void)failedUpload:(NSMutableDictionary *)Dict{
    [self.actInd stopAnimating];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString( @"Floorplan was not saved. Please try again later",nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
- (NSUInteger)supportedInterfaceOrientations
{

    return UIInterfaceOrientationMaskPortrait;
}


-(IBAction)showPicker:(id)sender{
    [[[[self.view superview] superview] superview] addSubview:_pickerViewController.view ];

}

#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
       return 2;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    switch (row) {
        case 0:{
            return NSLocalizedString(@"photo", nil);
            break;}
        case 1:{
            return NSLocalizedString(@"gallery", nil);
            break;}
        default:
            break;
    }
    return nil;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    _pickerViewController.row = row;
    _pickerViewController.component= component;

    
}

-(void)DoneButtonTappedForRow:(NSInteger)row inComponent:(NSInteger)component{
    switch (row) {
        case 0:
            self.fromCamera = YES;
            break;
        case 1:
            self.fromCamera = NO;
            break;
        default:
            break;
    }
    [self takePhoto:self];
    

}

-(BOOL)textFieldShouldReturn:(UITextField*)textField{

    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.

    
    
}

- (IBAction)editBegin:(id)sender {
    UITextField * tf = sender;
    [tf setBackground:[UIImage imageNamed:@"edit_text_bcg_on.png"]];
    [tf setTextColor:[UIColor whiteColor]];
    
}
- (IBAction)editEnd:(id)sender {
    UITextField * tf = sender;
    [tf setBackground:[UIImage imageNamed:@"edit_text_bcg.png"]];
    [tf setTextColor:[UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1]];
}


@end
