//
//  DeviceSchedule+CoreDataProperties.h
//  
//
//  Created by Tom Odler on 24.08.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DeviceSchedule.h"

NS_ASSUME_NONNULL_BEGIN

@interface DeviceSchedule (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *devicescheduleID;
@property (nullable, nonatomic, retain) NSString *label;
@property (nullable, nonatomic, retain) Device *devices;

@end

NS_ASSUME_NONNULL_END
