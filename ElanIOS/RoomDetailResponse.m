//
//  RoomDetailResponse.m
//  ElanIOS
//
//  Created by Vratislav Zima on 6/2/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "RoomDetailResponse.h"
#import "SBJson.h"
#import "Room.h"
@implementation RoomDetailResponse

/*
-(NSObject*) parseResponse:(NSData *)inputData identifier:(NSString *)identifier{

    Room* room = [[Room alloc] init];
    
    NSMutableArray * devices = [[NSMutableArray alloc] init];
    
    SBJsonParser *parser2 = [[SBJsonParser alloc] init];
    NSString *inputString = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];
    
    NSDictionary* jsonObjects = [parser2 objectWithString:inputString];
    //NSString* name = [jsonObjects objectForKey:@"id"];
    NSDictionary* floorplanDict = [jsonObjects objectForKey:@"floorplan"];
    NSString * floorplan;
    if ([floorplanDict isKindOfClass: [NSNull class]]==NO ){
        floorplan = [floorplanDict objectForKey:@"image"];
    }
    NSDictionary* roomInfo = [jsonObjects objectForKey:@"room info"];
    NSString * label  =[roomInfo valueForKey:@"label"];
    NSString * type  =[roomInfo valueForKey:@"type"];

    room.name = identifier;
    room.label = label;
    room.type = type;

    [room setFloorplan:floorplan];
    [devices addObject:room];

    //parse device info from response
    NSDictionary* deviceInfo = [jsonObjects objectForKey:@"devices"];
    
    for(NSString* device in deviceInfo)
    {
        NSDictionary * value = [deviceInfo objectForKey:device];
        NSArray * position = [value objectForKey:@"coordinates"];
        [devices addObject:[[NSDictionary alloc] initWithObjectsAndKeys:position,device, nil]];
    }
    return devices;
    
    
}
*/

@end
