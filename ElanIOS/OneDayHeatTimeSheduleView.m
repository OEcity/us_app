//
//  OneDayHeatTimeSheduleView.m
//  iHC-MIRF
//
//  Created by admin on 24.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "OneDayHeatTimeSheduleView.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]



@implementation OneDayHeatTimeSheduleView{
    
    float WIDTH_TO_TIME;
    UIColor * paint;
    
    
    NSMutableArray *modesStartY;
    NSMutableArray *modesEndY;
    
}


-(void)repaintDay:(TempScheduleDay *)day{
    _mDay = day;
    [self repaintContent];
   
}

-(void)repaintContent{
    NSArray *viewsToRemove = [self subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    [self drawModes];
    

}
-(void)drawModes{
    WIDTH_TO_TIME = self.frame.size.height / 1440;
    
    if ([_mDay.modes count] == 0)
        return;
    modesStartY = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
    modesEndY = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
    
    int i=0;
    float startX = 0;
    for (TempDayMode *mode in _mDay.modes) {
        modesStartY[i] = [[NSNumber alloc ] initWithFloat:startX + [mode.startTime intValue] * WIDTH_TO_TIME] ;
        modesEndY[i] = [[NSNumber alloc ] initWithFloat: ([mode.endTime intValue] - [mode.startTime intValue]) * WIDTH_TO_TIME];
        
        switch ([mode.mode intValue]) {
            case 5:
                paint = nil;
                break;
            case 4:
                paint = UIColorFromRGB(0xF82D26);
                break;
                
            case 3:
                paint = UIColorFromRGB(0xF5863C);
                break;
                
            case 2:
                paint = UIColorFromRGB(0x93A26C);
                break;
                
            case 1:
                paint = UIColorFromRGB(0x00C0F3);
                break;
        }
        
        
        UIView * areaRect = [[UIView alloc] initWithFrame:CGRectMake(0, [modesStartY[i] floatValue], self.frame.size.width,[modesEndY[i] floatValue])];
        [areaRect setBackgroundColor:paint];
        areaRect.tag = i+10;
        [self addSubview:areaRect];
        
        //NSString* time =[NSString stringWithFormat:@"%@-%@",  [self getTimeFromMinute:[mode.startTime intValue]], [self getTimeFromMinute:[mode.endTime intValue]]];
    
    
    }
}

/*
-(NSString*) getTimeFromMinute:(int) minutes {
    int hours = minutes / 60;
    return [NSString stringWithFormat:@"%02d:%02d", hours,minutes - hours * 60];
}
*/

@end
