//
//  SceneRGBViewController.h
//  Click Smart
//
//  Created by Tom Odler on 29.07.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device+State.h"
#import "SYAddSceneViewController.h"
#import "GuideScenesAddViewController.h"

@interface SceneRGBViewController : UIViewController
@property Device *device;
@property (nonatomic) SYAddSceneViewController *controller;
@property (nonatomic) GuideScenesAddViewController *guideController;

@end
