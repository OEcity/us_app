//
//  ContactsViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 11.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IntercomContact+CoreDataClass.h"

@interface ContactsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate, NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController* contacts;

@end
