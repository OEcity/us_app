//
//  UrlResponse.h
//  iHC-MIRF
//
//  Created by Vlastimil Venclik on 05.09.13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"
@interface UrlResponse : NSObject <Response>

@end
