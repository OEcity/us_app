//
//  ContactAddViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 11.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "ContactAddViewController.h"
#import "Constants.h"


@interface ContactAddViewController (){
@private UITextField *name;
@private UITextField *sipName;

    @private UITextField *ipAddress;
    @private UITextField *userName;
    @private UITextField *passWord;
    @private UITextField *switchCode;
    
    BOOL selected;
    BOOL iHCSelected;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property NSString *sName;
@property NSString *sSipName;
@property NSString *sServerAddress;
@property NSString *sIpAddress;
@property NSString *sUserName;
@property NSString *sPassWord;
@property NSString *sSwitchCode;




@end

@implementation ContactAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if(_contact != nil){
        _sName = _contact.label;
        _sSipName = _contact.sipName;
        iHCSelected = _contact.ihcDevice.boolValue;
        
        if(!iHCSelected){
            _sIpAddress = _contact.ipAddress;
            _sUserName = _contact.username;
            _sPassWord = _contact.password;
            _sSwitchCode = _contact.switchCode;
        }
    }
    
    iHCSelected = selected = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView.tag !=1 && selected) return 3;
    if(tableView.tag == 1 && !iHCSelected) return 7;
    return 3;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%ld, %ld", (long)tableView.tag, (long)indexPath.row);
    if(tableView.tag == 1 && indexPath.row == 2 && selected)return 2*50+50;
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    
    if(tableView.tag == 1){
             UITableViewCell * cell = nil;

        UITextField *tf = nil;
    NSAttributedString *placeholder = nil;
        
        [cell setSelectionStyle: UITableViewCellSelectionStyleNone];
    
    switch(indexPath.row){
        case 0:
            cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
            placeholder = [[NSAttributedString alloc] initWithString:
                           NSLocalizedString(@"name", @"") attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
            tf = [[[cell contentView] subviews] firstObject];
            name = tf;
            if(_sName != nil){
                tf.text = _sName;
            }
            break;
        case 1:
            cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
            placeholder = [[NSAttributedString alloc] initWithString: NSLocalizedString(@"sipName", @"") attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
            tf = [[[cell contentView] subviews] firstObject];
            sipName = tf;
            if(_sSipName != nil){
                tf.text = _sSipName;
            }
            break;
        case 2:{
            cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell" forIndexPath:indexPath];
            UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
            tbv.delegate = self;
            tbv.dataSource = self;
            return cell;
        }
            break;
        case 3:
            cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
            placeholder = [[NSAttributedString alloc] initWithString: NSLocalizedString(@"ipAddress", @"") attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
            tf = [[[cell contentView] subviews] firstObject];
            ipAddress = tf;
            if(_sIpAddress != nil){
                tf.text = _sIpAddress;
            }
            break;
        case 4:
            cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
            placeholder = [[NSAttributedString alloc] initWithString: NSLocalizedString(@"username", @"") attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
            tf = [[[cell contentView] subviews] firstObject];
            userName = tf;
            if(_sUserName != nil){
                tf.text = _sUserName;
            }
            break;
        case 5:
            cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
            placeholder = [[NSAttributedString alloc] initWithString: NSLocalizedString(@"password", @"") attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
            tf = [[[cell contentView] subviews] firstObject];
            passWord = tf;
            if(_sPassWord != nil){
                tf.text = _sPassWord;
            }
            break;
        case 6:
            cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
            placeholder = [[NSAttributedString alloc] initWithString: @"Switch Code"attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
            tf = [[[cell contentView] subviews] firstObject];
            switchCode = tf;
            if(_sSwitchCode != nil){
                tf.text = _sSwitchCode;
            }
            break;
    }
        tf.attributedPlaceholder = placeholder;
        tf.delegate = self;
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        return cell;
    } else {
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        if(selected){
            line.hidden = YES;
        } else {
            line.hidden = NO;
        }
        
        if(selected && indexPath.row>0){
            switch (indexPath.row) {
                case 1:
                    if(iHCSelected)
                       [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                     label.text = @"iHC";
                    break;
                default:
                    if(!iHCSelected)
                        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                    label.text = @"2N Contact";
                    break;
            }
           
            img.hidden = YES;
        } else {
            label.text = NSLocalizedString(@"contactType", @"");
        }
        
        if(indexPath.row == 0){
            img.hidden = NO;
            
            if(selected){
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            } else {
                img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
            }
        }
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag != 1 && indexPath.row == 0 && !selected){
        selected = YES;
        
        tableView.layer.borderColor = USBlueColor.CGColor;
        tableView.layer.borderWidth = 1.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        NSIndexPath*index = [NSIndexPath indexPathForRow:2 inSection:0];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
        
    } else if (tableView.tag != 1 && indexPath.row == 0 && selected){
        selected = NO;
        
        tableView.layer.borderColor = nil;
        tableView.layer.borderWidth = 0.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        NSIndexPath*index = [NSIndexPath indexPathForRow:2 inSection:0];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
    } else if (tableView.tag != 1 && indexPath.row > 0){
        switch (indexPath.row) {
            case 1:
                iHCSelected = YES;
                break;
                
            default:
                iHCSelected = NO;
                break;
        }
        [_tableView reloadData];
    }
    
    if(indexPath.row == 0)
        [tableView reloadData];
    
}


-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField == name){
        _sName = textField.text;
    } else if(textField == sipName){
        _sSipName = textField.text;
    } else if(textField == ipAddress){
        _sIpAddress = textField.text;
    } else if(textField == userName){
        _sUserName = textField.text;
    } else if(textField == passWord){
        _sPassWord = textField.text;
    } else if(textField == switchCode){
        _sSwitchCode = textField.text;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}
- (IBAction)saveContact:(id)sender {
    for(IntercomContact*contact in  [[SYCoreDataManager sharedInstance] getAllIntercomContacts]){
        if([contact.label isEqualToString:_sName] || [contact.sipName isEqualToString:_sSipName] ){
            return;
        }
    }
    
    if(_sName == nil || [_sName isEqualToString:@""] || _sSipName == nil || [_sSipName isEqualToString:@""]){
        return;
    }
    
    if(!iHCSelected){
        if(_sIpAddress == nil || [_sIpAddress isEqualToString:@""]){
            return;
        }
    }
    
    IntercomContact*myContact = [[SYCoreDataManager sharedInstance] createEntityIntercomContact];
    myContact.label = _sName;
    myContact.sipName = _sSipName;
    myContact.ihcDevice = [NSNumber numberWithBool:iHCSelected];
    
    if(!iHCSelected){
        myContact.ipAddress = _sIpAddress;
        myContact.username = _sUserName;
        myContact.password = _sPassWord;
        myContact.switchCode = _sSwitchCode;
    }
    [[SYCoreDataManager sharedInstance]saveContext];
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
