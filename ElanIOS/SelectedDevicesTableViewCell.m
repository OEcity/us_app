//
//  SelectedDevicesTableViewCell.m
//  iHC-MIRF
//
//  Created by Tom Odler on 30.03.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "SelectedDevicesTableViewCell.h"

@implementation SelectedDevicesTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
