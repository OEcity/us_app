//
//  DeterminableActionCell.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 19/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYSceneActionsViewController.h"

typedef enum {
    kPlain,
    kBrightness,
    kTime,
    kRgb,
    kOn
} Type;

#define kTypeArray @"PlainActionCell", @"BrightnessActionCell", @"TimerActionCell", @"RgbActionCell",@"OnActionCell", nil


#define kColorArray [7]{ 0xFF00A2, 0x9500FF, 0x0132FE, 0x01FAFF,0x00FF00,0xFFFF00,0xFE3600};

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
@interface DeterminableActionCell : UITableViewCell

@property Type type;
@property (nonatomic, retain) IBOutlet UILabel* name;
@property (nonatomic, strong) NSString* itemName;
@property BOOL expanded;
@property CGFloat height;
@property CGFloat expandedHeight;
@property (weak, nonatomic) IBOutlet UIImageView *secondaryArrow;
@property (weak, nonatomic) IBOutlet UIView *view;

@property (nonatomic, retain) SYSceneActionsViewController * sceneActionsViewController;

// Brigtness Cell
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property(nonatomic, weak) IBOutlet UILabel *lSliderValue;
@property CGFloat stepValue;

// RGB Cell
@property (retain, nonatomic) IBOutlet UIImageView *colorPicker;
@property (retain, nonatomic) IBOutlet UIImageView *colorView;
@property (nonatomic, retain) UIColor* color;
@property (nonatomic) unsigned char* data;
@property size_t w;
@property size_t h;


// On cell
@property (weak, nonatomic) IBOutlet UIButton *offButton;
@property (weak, nonatomic) IBOutlet UIButton *onButton;
@property (nonatomic, retain) NSNumber* on;
@property (weak, nonatomic) IBOutlet UILabel *offLabel;
@property (weak, nonatomic) IBOutlet UILabel *onLabel;


//Timer cell
@property (retain, nonatomic) IBOutlet UILabel *secondsLabel;
@property (retain, nonatomic) IBOutlet UILabel *minutesLabel;
@property (retain, nonatomic) IBOutlet UILabel *hoursLabel;
@property NSInteger timerStep;
@property NSInteger timerMin;
@property NSInteger timerMax;
@property NSInteger time;
-(id) initRGB:(NSArray*)sceneActions WithOwner:(id) owner;


-(id) initWithAction:(SceneAction*)sceneAction owner:(id)owner;
- (IBAction)OnButtonPressed:(id)sender;
- (IBAction)OffButtonPressed:(id)sender;
-(NSDictionary*)jsonRepresentation;
-(void)makeTimeVisible;
-(void)makeTimeInvisibleWithAlert:(BOOL)show;
- (BOOL)isEqual:(id)other;
- (IBAction)setTimer:(id)sender;

@end
