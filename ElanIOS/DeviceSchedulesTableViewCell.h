//
//  DeviceSchedulesTableViewCell.h
//  Click Smart
//
//  Created by Tom Odler on 25.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYConfigureDevicesViewController.h"
#import "HUDWrapper.h"

@interface DeviceSchedulesTableViewCell : UITableViewCell<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) NSMutableArray*dataArray;
@property (nonatomic) UITableView *tableView;
@property (nonatomic, retain) NSMutableArray * added;
@property (nonatomic) SYConfigureDevicesViewController*parentController;
@property (nonatomic, retain) HUDWrapper* loaderDialog;

@end
