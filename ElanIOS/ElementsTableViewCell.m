////
////  ElementsTableViewCell.m
////  Click Smart
////
////  Created by Tom Odler on 24.08.16.
////  Copyright © 2016 Vratislav Zima. All rights reserved.
////
//
//#import "ElementsTableViewCell.h"
//#import "Constants.h"
//#import "SYCoreDataManager.h"
//#import "SYAPIManager.h"
//
//
//@implementation ElementsTableViewCell
//
//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        // Initialization code
//        self.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
//        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain]; //create tableview a
//        _tableView.scrollEnabled = false;
//        _tableView.tag = 100;
//        _tableView.delegate = self;
//        _tableView.dataSource = self;
//        _tableView.separatorColor = [UIColor clearColor];
//        [self initFetchedResultsController];
//        [self addSubview:_tableView]; // add it cell
//        
//    }
//    return self;
//}
//
//
//- (void)awakeFromNib {
//    [super awakeFromNib];
//    // Initialization code
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//
//    // Configure the view for the selected state
//}
//
//-(void)layoutSubviews
//{
//    [super layoutSubviews];
//    //    UITableView *subMenuTableView =(UITableView *) [self viewWithTag:100];
//    _tableView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
//    _tableView.backgroundColor = [UIColor blackColor];
//    _tableView.layer.borderColor = USBlueColor.CGColor;
//    _tableView.layer.borderWidth = 1.0f;
//    //set the frames for tableview
//    
//}
//
////manage datasource and  delegate for submenu tableview
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//
//    return 1;
//}
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return [[_fetchedResultsController fetchedObjects] count];
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
//    if(cell == nil)
//    {
//        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
//    }
//    cell.backgroundColor = [UIColor blackColor];
//
//    UIView *bgColorView = [[UIView alloc] init];
//    bgColorView.backgroundColor = USBlueColor;
//    [cell setSelectedBackgroundView:bgColorView];
//    
//    cell.backgroundColor = [UIColor blackColor];
//    
//    Device *myDevice = [_fetchedResultsController objectAtIndexPath:indexPath];
//    
//        cell.textLabel.text = myDevice.label;
//        cell.textLabel.textColor = [UIColor whiteColor];
//
//    return cell;
//}
//
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//
//        Device *myDevice = [_fetchedResultsController objectAtIndexPath:indexPath];
//        _parentController.selectedDevice = myDevice;
//        [_parentController performSegueWithIdentifier:@"addDeviceSegue" sender:nil];
//
//}
//
//-(void)elementsTapped{
//    [_parentController reloadTableView];
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,300,60)];
//    
//    // create the label object
//    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//    headerLabel.frame = CGRectMake(0,0,_tableView.frame.size.width,60);
//    headerLabel.backgroundColor = [UIColor blackColor];
//    headerLabel.font = [UIFont fontWithName:@"Roboto-Thin" size:18];
//    headerLabel.textAlignment = NSTextAlignmentCenter;
//    
//    UITapGestureRecognizer *singleFingerTap =
//    [[UITapGestureRecognizer alloc] initWithTarget:self
//                                            action:@selector(elementsTapped)];
//    [customView addGestureRecognizer:singleFingerTap];
//    
//    UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectZero];
//    arrow.frame = CGRectMake(_tableView.frame.size.width - 30,20,20,20);
//    arrow.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
//    
//    headerLabel.text = @"Elements";
//
//    headerLabel.textColor = [UIColor whiteColor];
//    
//    [customView addSubview:headerLabel];
//    [customView addSubview:arrow];
//    
//    return customView;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 50;
//}
//
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    return 50;
//}
//
//-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
//    return YES;
//}
//
//// Override to support editing the table view.
//- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
//    
//    if (editingStyle == UITableViewCellEditingStyleDelete) {
//        NSString* name = ((Device*)[_fetchedResultsController objectAtIndexPath:indexPath]).label;
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_device", nil) ,name] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
//        alert.tag = indexPath.row;
//        [alert show];
//    }
//}
//
//
//- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
//    if (buttonIndex == 1) {
//        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
////        self.deletedIndex = indexPath;
//        NSString* deviceID = ((Device*)[_fetchedResultsController objectAtIndexPath:indexPath]).deviceID;
//        Device*device=[_fetchedResultsController objectAtIndexPath:indexPath];
//        if (deviceID!=nil){
//            [[SYAPIManager sharedInstance] deleteDevice:device success:^(AFHTTPRequestOperation *operation, id response) {
//                [[SYCoreDataManager sharedInstance] deleteManagedObject:[[SYCoreDataManager sharedInstance] getDeviceWithID:deviceID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]]];
//            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"cannotDeleteRoom"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//                [alert show];
//            }];
//        }
//    }
//}
//
//#pragma mark - Init
//
//- (void)initFetchedResultsController {
//    
//    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
//    
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Device"];
//    // Configure the request's entity, and optionally its predicate.
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
//    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
//    [fetchRequest setSortDescriptors:sortDescriptors];
//    [fetchRequest setIncludesSubentities:NO];
//    
//    
//    _fetchedResultsController = [[NSFetchedResultsController alloc]
//                                 initWithFetchRequest:fetchRequest
//                                 managedObjectContext:context
//                                 sectionNameKeyPath:nil
//                                 cacheName:nil];
//    _fetchedResultsController.delegate = self;
//    NSError *error;
//    
//    if (![_fetchedResultsController performFetch:&error]) {
//        NSLog(@"error fetching Rooms: %@",[error description]);
//    }
//}
//
//#pragma mark - NSFetchedResultsControllerDelegate methods
//- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
//    [self.tableView beginUpdates];
//}
//
//
//- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
//       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
//      newIndexPath:(NSIndexPath *)newIndexPath {
//    
//    UITableView *tableView = self.tableView;
//    
//    switch(type) {
//            
//        case NSFetchedResultsChangeInsert:
//            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
//                             withRowAnimation:UITableViewRowAnimationFade];
//            break;
//            
//        case NSFetchedResultsChangeDelete:
//            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
//                             withRowAnimation:UITableViewRowAnimationFade];
//           // self.emptyLabel.hidden = [_fetchedResultsController.fetchedObjects count] > 0;
//            break;
//            
//        case NSFetchedResultsChangeUpdate:
//            break;
//            
//        case NSFetchedResultsChangeMove:
//            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
//                             withRowAnimation:UITableViewRowAnimationFade];
//            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
//                             withRowAnimation:UITableViewRowAnimationFade];
//            break;
//    }
//}
//- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
//    [self.tableView endUpdates];
////    self.emptyLabel.hidden = [_fetchedResultsController.fetchedObjects count] > 0;
//}
//
//
//
//
//
//@end
