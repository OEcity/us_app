//
//  PairFloorplanViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 05/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "SYPairFloorplanViewController.h"
#import "DragableView.h"
#import "AppDelegate.h"
#import "Util.h"
#import "SBJson.h"
#import "SYCoreDataManager.h"
#import "SYAPIManager.h"
#import "DeviceInRoom.h"
#import "Room+Dictionary.h"


@interface SYPairFloorplanViewController ()

@end

@implementation SYPairFloorplanViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewDidAppear:(BOOL)animated{
    [self returnViews];
    
    if (_selectedRoom.floorPlan==nil){
        
        [_alertLabel setHidden:NO];
        
    }else{
        UITapGestureRecognizer *oneFingerTwoTaps =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(canvasTap:)];
        
        // Set required taps and number of touches
        [oneFingerTwoTaps setNumberOfTapsRequired:1];
        [oneFingerTwoTaps setNumberOfTouchesRequired:1];
        
        // Add the gesture to the view
        [_canvas addGestureRecognizer:oneFingerTwoTaps];
        
        if (_landscape==NO){
            AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            NSString* address= [Util getAddressWith:[NSString stringWithFormat:@"floorplans/%@",_selectedRoom.floorPlan]];
            NSMutableDictionary * dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:address, @"url",  nil];
            NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadDataAsychn:) object:dict];
            [app.operationQueue addOperation:invocationOperation];
            [_alertLabel setText:NSLocalizedString(@"Floorplan loading", nil)];
            [_alertLabel setHidden:NO];
        }else{
            UIImageView * image =[[UIImageView alloc] initWithFrame:_canvas.frame];
            [image setImage:_loadedImage];
            image.contentMode = UIViewContentModeScaleToFill;
            [image setFrame:CGRectMake(0, 0, _canvas.frame.size.width, _canvas.frame.size.height)];
            [_canvas addSubview:image];
            [_canvas sendSubviewToBack:image];
        }
    }
}

-(void)loadDataAsync:(NSMutableDictionary*)dict
{
    NSString * imageURl = _selectedRoom;
    UIActivityIndicatorView * actInd = [dict objectForKey:@"ind"];
    
    NSData *imageData=[[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:imageURl]];
    UIImage *image=[[UIImage alloc]initWithData:imageData];
    NSMutableDictionary* imgDict = [[NSMutableDictionary alloc] init];
    [imgDict setValue:image forKey:@"image"];
    [imgDict setValue:actInd forKey:@"ind"];
    [imgDict setValue:imageURl forKey:@"imageNo"];
    [self performSelectorOnMainThread:@selector(showImage:) withObject:imgDict waitUntilDone:NO];
}
//-(void)showImage:(NSMutableDictionary *)Dict
//{
//    UIImage * loadedImage = [Dict objectForKey:@"image"];
//    UIImageView * image =[[UIImageView alloc] initWithFrame:_canvas.frame];
//    [image setImage:loadedImage];
//    image.contentMode = UIViewContentModeScaleToFill;
//    [image setFrame:CGRectMake(0, 0, _canvas.frame.size.width, _canvas.frame.size.height)];
//    [_canvas addSubview:image];
//    [_canvas sendSubviewToBack:image];
//}

-(void)viewWillAppear:(BOOL)animated{
    [_leftBottomButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_rightBottomButton setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
    
    
}
-(void) returnViews{
    UIView *tile;
    CGFloat spaceSize = 10;
    CGFloat onespace;
    CGFloat startPos;
    if (_landscape){
        onespace = self.deviceBackgroundTemplate.frame.size.height + spaceSize;
        startPos = self.view.frame.size.height/2-44 - (([self.selectedDevices count]-1)* (self.deviceBackgroundTemplate.frame.size.height+spaceSize))/2;
    }else{
        onespace = self.deviceBackgroundTemplate.frame.size.width + spaceSize;
        startPos = self.view.frame.size.width/2 - (([self.selectedDevices count]-1)* (self.deviceBackgroundTemplate.frame.size.width+spaceSize))/2;
    }
    self.deviceViews = [[NSMutableArray alloc] init];
    self.viewsInCanvas = [[NSMutableArray alloc] init];
    
    int inkr=0;
    
    for (Device * device in _selectedDevices){
        tile = [[UIView alloc] initWithFrame:self.deviceBackgroundTemplate.frame];
        [tile setBackgroundColor:[UIColor clearColor]];
        UIImageView *backImage = [[UIImageView alloc] initWithFrame:self.deviceBackgroundTemplate.frame];
        backImage.contentMode = UIViewContentModeScaleToFill;
        [backImage setBackgroundColor:[UIColor clearColor]];
        [backImage setCenter:CGPointMake(backImage.frame.size.width/2, backImage.frame.size.height/2)];
        if (self.selectedDevice==device)
            [backImage setImage:[UIImage imageNamed:@"dlazdice_on.png"]];
        else
            [backImage setImage:[UIImage imageNamed:@"dlazdice_off.png"]];
        backImage.tag=1;
        UILabel * nameLabel = [[UILabel alloc] initWithFrame:self.deviceLabelTemplate.frame];
        [nameLabel setFont:[self.deviceLabelTemplate font]];
        [nameLabel setTextColor:[self.deviceLabelTemplate textColor]];
        [nameLabel setTextAlignment:NSTextAlignmentCenter];
        [nameLabel setAdjustsFontSizeToFitWidth:YES];
        [nameLabel setBackgroundColor:[UIColor clearColor]];
        [nameLabel setText:device.label];
        UIImageView * icon = [[UIImageView alloc] initWithFrame:self.deviceIconTemplate.frame];
        if (self.selectedDevice==device) {
            [icon setImage:[device imageOn]];
        }else{
            [icon setImage:[device imageOff]];
        }
        
        icon.contentMode = UIViewContentModeScaleAspectFit;
        [icon setBackgroundColor:[UIColor clearColor]];
        icon.tag=2;
        if (_landscape){
            [tile setCenter:CGPointMake( self.deviceBackgroundTemplate.center.x, startPos+inkr*onespace)];
        }else{
            [tile setCenter:CGPointMake(startPos+inkr*onespace, self.deviceBackgroundTemplate.center.y)];
        }
        [tile addSubview:backImage];
        [tile addSubview:icon];
        [tile addSubview:nameLabel];
        tile.tag = 100+inkr;
        [[self.view viewWithTag:1] addSubview:tile];
        UITapGestureRecognizer *oneFingerTwoTaps =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognize:)];
        
        // Set required taps and number of touches
        [oneFingerTwoTaps setNumberOfTapsRequired:1];
        [oneFingerTwoTaps setNumberOfTouchesRequired:1];
        
        // Add the gesture to the view
        [tile addGestureRecognizer:oneFingerTwoTaps];
        [self.deviceViews addObject:tile];
        if (_selectedRoom.floorPlan!=nil){
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"room.roomID == %@", _selectedRoom.roomID];
            [device.inRooms filteredSetUsingPredicate:predicate];
            NSSet * devicesInRoom = [device.inRooms filteredSetUsingPredicate:predicate];
            CGRect position =CGRectMake(0, 0, 45, 45);
            if ([devicesInRoom count] == 0){
                position =CGRectMake(0, 0, 45, 45);
            }else{
                DeviceInRoom * deviceInRoom = [[devicesInRoom allObjects] objectAtIndex:0];
                position =CGRectMake([deviceInRoom.coordX floatValue]*_canvas.frame.size.width - 23, [deviceInRoom.coordY floatValue]*_canvas.frame.size.height - 23, 45, 45);
                if (position.origin.x<0 && position.origin.y<0) position =CGRectMake(0, 0, 45, 45);
                
            }
            DragableView *back = [[DragableView alloc] initWithFrame:position];
            [back setImage:[UIImage imageNamed: @"room_dlazdice_off"]];
            back.contentMode = UIViewContentModeScaleAspectFill;
            UIImageView *item = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
            [item setBackgroundColor:[UIColor clearColor]];
            [item setTag:1];
            [item setImage:[device imageOff]];
            item.contentMode = UIViewContentModeScaleAspectFit;
            [back addSubview:item];
            back.tag = 100+inkr;
            [back setUserInteractionEnabled:YES];
            UITapGestureRecognizer *oneTap =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemTapRecognize:)];
            
            // Set required taps and number of touches
            [oneTap setNumberOfTapsRequired:1];
            [oneTap setNumberOfTouchesRequired:1];
            
            // Add the gesture to the view
            [back addGestureRecognizer:oneTap];
            [back setSelectedDevices:self.selectedDevices];
            [_canvas addSubview:back];
            [self.viewsInCanvas addObject:back];
        }
        inkr++;
        
        
    }
    
    for (DragableView * view in _viewsInCanvas){
        [view setDeviceViews:_deviceViews];
        [view setLandscape:_landscape];
    }
    
}


-(void) moveViews:(CGPoint) newLocation{
    
    for (UIView *v in self.deviceViews){
        if (_landscape==NO){
            [v setCenter:CGPointMake(v.center.x +  newLocation.x - self.startPos.x, v.center.y)];
        }else{
            [v setCenter:CGPointMake(v.center.x , v.center.y +  newLocation.y - self.startPos.y)];
        }
        
    }
    self.startPos = newLocation;
}

-(void) moveViewsRelative:(CGPoint) newLocation{
    
    for (UIView *v in self.deviceViews){
        if (_landscape==YES){
            [v setCenter:CGPointMake(v.center.x  , v.center.y +  newLocation.y)];
        }else{
            [v setCenter:CGPointMake(v.center.x +  newLocation.x , v.center.y )];
        }
        
    }
}

#pragma mark TouchGestureRecognizer - swipe recognizer methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    self.startPos = [touch locationInView:[self.view viewWithTag:1]];
    
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    
    if (self.startPos.y < [self.view viewWithTag:1].frame.size.height && _landscape==NO){
        [self moveViews:[touch locationInView:[self.view viewWithTag:1]]];
    }
    
    if (self.startPos.x < [self.view viewWithTag:1].frame.size.height && _landscape==YES){
        [self moveViews:[touch locationInView:[self.view viewWithTag:1]]];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    [self moveViews:[touch locationInView:[self.view viewWithTag:1]]];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch* touch = [touches anyObject];
    [self moveViews:[touch locationInView:[self.view viewWithTag:1]]];
    
}


#pragma tap gesture recognizer methods

-(void) tapRecognize:(UITapGestureRecognizer*) recognizer{
    Device * device = [self.selectedDevices objectAtIndex:recognizer.view.tag-100];
    
    
    //clear all
    for (int i=100; i < 100 + [self.selectedDevices count]; i++){
        UIImageView * iV1 = (UIImageView*)[[[self.view viewWithTag:1] viewWithTag:i] viewWithTag:1];
        [iV1 setImage:[UIImage imageNamed:@"dlazdice_off.png"]];
        UIImageView * iV2 = (UIImageView*)[[[self.view viewWithTag:1] viewWithTag:i] viewWithTag:2];
        [iV2 setImage:[((Device*)[self.selectedDevices objectAtIndex:i-100]) imageOff]];
        
        UIImageView * iV3 = (UIImageView*)[self.canvas viewWithTag:i];
        [iV3 setImage:[UIImage imageNamed:@"room_dlazdice_off.png"]];
        UIImageView * iV4 = (UIImageView*)[[self.canvas viewWithTag:i] viewWithTag:1];
        [iV4 setImage:[((Device*)[self.selectedDevices objectAtIndex:i-100]) imageOff]];
    
    }
    if ([self.selectedDevice isEqual:device]){
        self.selectedDevice = nil;
        return;
    }
    else{
        UIImageView * iV1 = (UIImageView*)[[[self.view viewWithTag:1] viewWithTag:recognizer.view.tag] viewWithTag:1];
        [iV1 setImage:[UIImage imageNamed:@"dlazdice_on.png"]];
        UIImageView * iV2 = (UIImageView*)[[[self.view viewWithTag:1] viewWithTag:recognizer.view.tag] viewWithTag:2];
        [iV2 setImage:[device imageOn]];
        
        UIImageView *iv1 = (UIImageView*)[self.canvas viewWithTag:recognizer.view.tag] ;
        [iv1 setImage:[UIImage imageNamed:@"room_dlazdice_on.png"]];
        UIImageView *iv2 = (UIImageView*)[[self.canvas viewWithTag:recognizer.view.tag] viewWithTag:1] ;
        [iv2 setImage:[device imageOn]];
        self.selectedDevice = device;
    }
}
-(void) itemTapRecognize:(UITapGestureRecognizer*) recognizer{
    [self tapRecognize:recognizer];
    UIView * iV1 = (UIView*)[[self.view viewWithTag:1] viewWithTag:recognizer.view.tag] ;
    if (_landscape==NO)
        [self moveViewsRelative:CGPointMake(150 - iV1.center.x, 0)];
    else
        [self moveViewsRelative:CGPointMake(0 , 130- iV1.center.y)];
}

-(void) canvasTap:(UITapGestureRecognizer*) recognizer{
    for (int i=100; i < 100 + [self.selectedDevices count]; i++){
        if ([self.selectedDevices objectAtIndex:i-100] == self.selectedDevice){
            [[_canvas viewWithTag:i] setCenter:[recognizer locationInView:_canvas]];
        }
    }
    
}


- (NSUInteger)supportedInterfaceOrientations
{
    if (_landscape)
        return UIInterfaceOrientationMaskLandscape;
    else{
        return UIInterfaceOrientationMaskPortrait;
    }
}

-(IBAction)back{
    [self dismissViewControllerAnimated:NO completion:nil];
}


-(void)loadDataAsychn:(NSMutableDictionary*)dict
{
    NSString * imageURl = [dict objectForKey:@"url"];
    
    NSData *imageData=[[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:imageURl]];
    UIImage *image=[[UIImage alloc]initWithData:imageData];
    NSMutableDictionary* imgDict = [[NSMutableDictionary alloc] init];
    [imgDict setValue:image forKey:@"image"];
    [self performSelectorOnMainThread:@selector(showImage:) withObject:imgDict waitUntilDone:NO];
}
-(void)showImage:(NSMutableDictionary *)Dict
{
    UIImage * original =[Dict objectForKey:@"image"];
    CGSize frame = original.size;
    if (frame.width>frame.height){
        _loadedImage = original;
        [self performSegueWithIdentifier:@"pairFloorplanLandscape" sender:self];
    }
    UIImageView * image =[[UIImageView alloc] initWithFrame:_canvas.frame];
    [image setImage:original];
    image.contentMode = UIViewContentModeScaleToFill;
    [image setFrame:CGRectMake(0, 0, _canvas.frame.size.width, _canvas.frame.size.height)];
    [_canvas addSubview:image];
    [_canvas sendSubviewToBack:image];
    [_alertLabel setHidden:YES];
    
}

/*

-(NSString*) createJSON{
    //    NSString * jsonString= [[NSString alloc] initWithString:@"{"];
    
    SBJsonWriter *writer = [[SBJsonWriter alloc] init];
    NSDictionary *RoomInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                              _selectedRoom.label,@"label",
                              _selectedRoom.type,@"type",
                              nil];
    NSMutableDictionary * devices = [[NSMutableDictionary alloc] init];
    for (Device * dev in self.selectedDevices){
        if (_selectedRoom.floorPlan==nil){
            [devices setValue:[NSDictionary dictionaryWithObjectsAndKeys:                            [[NSNull alloc] init] , @"coordinates",nil ] forKey:dev.name];
        }else{
            NSMutableArray * coordString = [self getCoordsFor:dev];
            [devices setValue:[NSDictionary dictionaryWithObjectsAndKeys:                            coordString , @"coordinates",nil ] forKey:dev.name];
            
            
        }
        
    }
    NSDictionary * floorplan = [NSDictionary dictionaryWithObjectsAndKeys:
                                _selectedRoom.floorplan , @"image",
                                nil];
    NSDictionary *result = [NSDictionary dictionaryWithObjectsAndKeys:
                            _selectedRoom.name,@"id",
                            RoomInfo , @"room info",
                            floorplan,@"floorplan",
                            devices, @"devices",
                            nil];
    NSString *jsonCommand = [writer stringWithObject:result];
    
    return jsonCommand;
    
}

*/

/*     NSString *predicate = [NSString stringWithFormat:@"room.roomID == %@",_selectedRoom.roomID];
 NSArray * devicesInRoom = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"DeviceInRoom"
 withPredicate:[NSPredicate predicateWithFormat:predicate]
 sortDescriptor:nil
 inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
 for (Device * device in _selectedDevices){
 predicate = [predicate stringByAppendingString:@" AND device.deviceID!= '%@'", devices.deviceID];
 
 
 deviceInRoom.room = _selectedRoom;
 deviceInRoom.device = device;
 deviceInRoom.coordX = [[NSNumber alloc] initWithDouble:[self getCoordsFor:device].coordX];
 deviceInRoom.coordY = [[NSNumber alloc] initWithDouble:[self getCoordsFor:device].coordY];
 [devicesInRoom addObject:deviceInRoom];
 }
 _selectedRoom.devicesInRoom = devicesInRoom;
 */

-(IBAction)save:(id)sender{
//    NSMutableSet *  devicesInRoom = [NSMutableSet new];
    [[SYCoreDataManager sharedInstance] unpairAllDevicesForRoom:_selectedRoom];
    
    for (Device * device in _selectedDevices){
        [[SYCoreDataManager sharedInstance] pairDevice:device andRoom:_selectedRoom withCoordinates:[self getCoordsFor:device]];
//        DeviceInRoom * deviceInRoom = [[SYCoreDataManager sharedInstance] createEntityDeviceInRoomInContext:[[SYCoreDataManager sharedInstance] managedObjectContext]];
//        deviceInRoom.room = _selectedRoom;
//        deviceInRoom.device = device;
//        deviceInRoom.coordX = [[NSNumber alloc] initWithDouble:[self getCoordsFor:device].coordX];
//        deviceInRoom.coordY = [[NSNumber alloc] initWithDouble:[self getCoordsFor:device].coordY];
//        [devicesInRoom addObject:deviceInRoom];
    }
//    _selectedRoom.devicesInRoom = devicesInRoom;
    
    NSDictionary * dict = [[[SYCoreDataManager sharedInstance] getRoomWithID:_selectedRoom.roomID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]] serializeToDictionary];
    
    
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    [[SYAPIManager sharedInstance] updateRoomWithDictionary:dict success:^(AFHTTPRequestOperation * operation, id object)
     {
        [_loaderDialog hide];
         if (_landscape==NO){
             [self dismissViewControllerAnimated:YES completion:nil];
         }else{
             [self backFromLandscape:self];
         }
         
     }failure:^(AFHTTPRequestOperation * operation, NSError * error)
     {
         [_loaderDialog hide];
         UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Could not pair devices. Please retry again later", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
    
    
}



-(struct SYDevicePosition)getCoordsFor:(Device*)device{
    struct SYDevicePosition position;
    NSMutableArray * result = [[NSMutableArray alloc] init];
    for (int i=100; i < 100 + [self.selectedDevices count]; i++){
        if ([((Device*)[self.selectedDevices objectAtIndex:i-100]).deviceID isEqualToString:device.deviceID]){
            UIView * view = [_canvas viewWithTag:i];
            CGPoint point = CGPointMake(view.center.x, view.center.y) ;
            position.coordX = point.x / _canvas.frame.size.width;
            NSNumber* val1 = [NSNumber numberWithFloat:point.x / _canvas.frame.size.width] ;
            [result addObject:val1];
            NSNumber* val2 =[NSNumber numberWithFloat:point.y / _canvas.frame.size.height] ;
            position.coordY = point.y / _canvas.frame.size.height;
            [result addObject:val2];
            
        }
        
    }
    return position;
}



-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if  ([segue.destinationViewController isKindOfClass:[SYPairFloorplanViewController class]]){
        SYPairFloorplanViewController*dest = (SYPairFloorplanViewController*)segue.destinationViewController;
        dest.selectedDevices = self.selectedDevices;
        dest.selectedRoom = self.selectedRoom;
        dest.landscape=YES;
        dest.loadedImage = _loadedImage;
        dest.parent = self.parent;
    }
    
    
}


-(IBAction)backFromLandscape:(id)sender{
    [self.parent dismissViewControllerAnimated:NO completion:nil];
    
}
@end
