//
//  HCAViewController.m
//  US App
//
//  Created by Tom Odler on 24.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "HCAViewController.h"
#import "UIViewController+RevealViewControllerAddon.h"
#import "EFCircularSlider.h"
#import "SYAPIManager.h"
#import "SYCoreDataManager.h"
#import "Constants.h"
#import "HeatCoolArea.h"
#import "HeatTimeSchedule.h"

@interface HCAViewController (){
    double actSetTemp;
    double tempWithoutCorrection;
}
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *modeButtonCollection;
@property (weak, nonatomic) IBOutlet UILabel *lActTemp;
@property (weak, nonatomic) IBOutlet UILabel *lSetTemperature;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *checkObdelniky;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *controllButtons;

@property (weak, nonatomic) IBOutlet UIButton *onButton;
@property (weak, nonatomic) IBOutlet UIImageView *onObdelnik;

@property (nonatomic, strong) NSFetchedResultsController* devices;

@property (strong, nonatomic) EFCircularSlider* circularSlider;

@property (weak, nonatomic) IBOutlet UIView *topSliderView;

@property (nonatomic) HeatCoolArea* myHCA;
@property (nonatomic) HeatTimeSchedule* schedule;
@property (nonatomic) HeatTimeSchedule* holidaySchedule;

@property (nonatomic) double correction;
@property (nonatomic) BOOL afterLoad;

@end

@implementation HCAViewController
#define MINIMUM 1
#define ATTENUATION 2
#define NORMAL 3
#define COMFORT 4

- (void)viewDidLoad {
    [super viewDidLoad];
    _afterLoad = YES;
    // Do any additional setup after loading the view.
    
    //Configure navigation bar apperence
    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
    
    _myHCA = (HeatCoolArea*)_device;
    
    _schedule = [[SYCoreDataManager sharedInstance] getScheduleWithID:_myHCA.schedule];
    _holidaySchedule = [[SYCoreDataManager sharedInstance] getScheduleWithID:_myHCA.holidaySchedule];
    
    CGRect sliderFrame = CGRectMake(0, 0, _topSliderView.frame.size.width, _topSliderView.frame.size.height);
    _circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    [_circularSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [_circularSlider addTarget:self action:@selector(touchCancel:) forControlEvents:UIControlEventEditingDidEnd];

 //   [_circularSlider addTarget:self action:@selector(valuc) forControlEvents:UIControlEventEditingDidEnd];
    [_topSliderView addSubview:_circularSlider];
    [_circularSlider pauseAutoredrawing];
    
    CGFloat dash[]={1,15};
    [_circularSlider setHandleRadius:10];
    [_circularSlider setUnfilledLineDash:dash andCount:2];
    [_circularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_circularSlider setUnfilledColor:[UIColor whiteColor]];
    [_circularSlider setFilledColor:[UIColor whiteColor]];
    [_circularSlider setLineWidth:1];
    [_circularSlider setArcStartAngle:150];
    [_circularSlider setArcAngleLength:240];
    
    [_circularSlider setCurrentArcValue:(CGFloat)50 forStartAnglePadding:2 endAnglePadding:2];
    
    [self initGUI];
    
    if(_device){
        _typeLabel.text = _device.productType;
        _nameLabel.text = _device.label;
        _correction = [[_myHCA getStateValueForStateName:@"correction"]doubleValue];
        [self initFetchedResultsControllerWithHCAID:_device.deviceID];
    }
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _afterLoad = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)initGUI{
    if ([_device getStateValueForStateName:@"temperature"]!=nil)
        [_lActTemp setText:[NSString stringWithFormat:@"%@", [_device getStateValueForStateName:@"temperature"]]];
  
        double currentRequestedTemp = [[_device getStateValueForStateName:@"requested temperature"] doubleValue];
    NSLog(@"Device: %@", _device.label);
    NSLog(@"req temp :%f", currentRequestedTemp);
        [_lSetTemperature setText:[NSString stringWithFormat:@"%.1f", currentRequestedTemp]];
    actSetTemp = currentRequestedTemp;
    
    _correction = [[_device getStateValueForStateName:@"correction"]doubleValue];
    tempWithoutCorrection = actSetTemp - _correction;
    double valueForSlider = (_correction + 5) * 10;
    
    [_circularSlider setCurrentArcValue:valueForSlider forStartAnglePadding:2 endAnglePadding:2];
    
    // **ON/OFF BUTTON**
    NSString*powerString = nil;
    if([_device hasStateName:@"power"]){
        powerString = @"power";
    } else {
        powerString = @"on";
    }
    if ([_device getStateValueForStateName:powerString]!=nil){
        if ([[_device getStateValueForStateName:powerString] boolValue]==YES){
            _onButton.selected = true;
            _onObdelnik.hidden = false;
        } else {
            _onButton.selected = false;
            _onObdelnik.hidden = true;
        }
    }
    
    // **HEAT MODES**
    switch ([[_device getStateValueForStateName:@"mode"] intValue]) {
        case MINIMUM:
            for (UIButton *myButton in _modeButtonCollection){
                if (myButton.tag == MINIMUM){
                    [myButton setSelected: NO];
                } else {
                    [myButton setSelected: YES];
                }
            }
            
            for (UIImageView *heatImageView in _modesOnCollection){
                if((heatImageView.tag - 10) == MINIMUM){
                    heatImageView.hidden = NO;
                } else {
                    heatImageView.hidden = YES;
                }
            }
            
            [_circularSlider setHandleColor:USBlueColor];
            [_circularSlider redraw];
            break;
        case ATTENUATION:
            for (UIButton *myButton in _modeButtonCollection){
                if (myButton.tag == ATTENUATION){
                    [myButton setSelected: NO];
                } else {
                    [myButton setSelected: YES];
                }
            }
            
            
            for (UIImageView *heatImageView in _modesOnCollection){
                if((heatImageView.tag - 10) == ATTENUATION){
                    heatImageView.hidden = NO;
                } else {
                    heatImageView.hidden = YES;
                }
            }
            
            [_circularSlider setHandleColor:USUtlumColor];
            [_circularSlider redraw];
            break;
        case NORMAL:
            for (UIButton *myButton in _modeButtonCollection){
                if (myButton.tag == NORMAL){
                    [myButton setSelected: NO];
                } else {
                    [myButton setSelected: YES];
                }
            }
            
            
            for (UIImageView *heatImageView in _modesOnCollection){
                if((heatImageView.tag - 10) == NORMAL){
                    heatImageView.hidden = NO;
                } else {
                    heatImageView.hidden = YES;
                }
            }
            
            [_circularSlider setHandleColor:USNormalColor];
            [_circularSlider redraw];
            break;
        case COMFORT:
            for (UIButton *myButton in _modeButtonCollection){
                if (myButton.tag == COMFORT){
                    [myButton setSelected: NO];
                } else {
                    [myButton setSelected: YES];
                }
            }
            
            for (UIImageView *heatImageView in _modesOnCollection){
                if((heatImageView.tag - 10) == COMFORT){
                    heatImageView.hidden = NO;
                } else {
                    heatImageView.hidden = YES;
                }
            }
            
            [_circularSlider setHandleColor:USKomfrotColor];
            [_circularSlider redraw];
            break;
    }
    
    // **CONTROLL**
    switch ([[_device getStateValueForStateName:@"controll"]intValue]) {
        case 1:
            for (UIButton* button in _controllButtons){
                if (button.tag - 110 == 1){
                    button.selected = true;
                } else {
                    button.selected = false;
                }
            }
            
            for (UIImageView* button in _checkObdelniky){
                if (button.tag - 100 == 1){
                    button.hidden = false;
                } else {
                    button.hidden = true;
                }
            }
            break;
        case 2:
            for (UIButton* button in _controllButtons){
                if (button.tag - 110 == 2){
                    button.selected = true;
                } else {
                    button.selected = false;
                }
            }
            
            for (UIImageView* button in _checkObdelniky){
                if (button.tag - 100 == 2){
                    button.hidden = false;
                } else {
                    button.hidden = true;
                }
            }
            break;
        case 3:
            for (UIButton* button in _controllButtons){
                if (button.tag - 110 == 3){
                    button.selected = true;
                } else {
                    button.selected = false;
                }
            }
            
            for (UIImageView* button in _checkObdelniky){
                if (button.tag - 100 == 3){
                    button.hidden = false;
                } else {
                    button.hidden = true;
                }
            }
            break;
        case 4:
            for (UIButton* button in _controllButtons){
                if (button.tag - 110 == 4){
                    button.selected = true;
                } else {
                    button.selected = false;
                }
            }
            
            for (UIImageView* button in _checkObdelniky){
                if (button.tag - 100 == 4){
                    button.hidden = false;
                } else {
                    button.hidden = true;
                }
            }
            break;
    }
    
    // **MODE BUTTONS TEMPERATURE SET**
    if([[_device getStateValueForStateName:@"controll"]intValue] == 2){
        for(UIButton* myButton in _modeButtonCollection){
            switch (myButton.tag) {
                case MINIMUM:
                    [myButton setTitle:[NSString stringWithFormat:@"%@", _holidaySchedule.temp1 ] forState:UIControlStateNormal];
                    break;
                case ATTENUATION:
                    [myButton setTitle:[NSString stringWithFormat:@"%@", _holidaySchedule.temp2 ] forState:UIControlStateNormal];
                    break;
                case NORMAL:
                    [myButton setTitle:[NSString stringWithFormat:@"%@", _holidaySchedule.temp3 ] forState:UIControlStateNormal];
                    break;
                case COMFORT:
                    [myButton setTitle:[NSString stringWithFormat:@"%@", _holidaySchedule.temp4 ] forState:UIControlStateNormal];
                    break;
            }
        }
    } else {
        for(UIButton* myButton in _modeButtonCollection){
            switch (myButton.tag) {
                case MINIMUM:
                    [myButton setTitle:[NSString stringWithFormat:@"%@", _schedule.temp1 ] forState:UIControlStateNormal];
                    break;
                case ATTENUATION:
                    [myButton setTitle:[NSString stringWithFormat:@"%@", _schedule.temp2 ] forState:UIControlStateNormal];
                    break;
                case NORMAL:
                    [myButton setTitle:[NSString stringWithFormat:@"%@", _schedule.temp3 ] forState:UIControlStateNormal];
                    break;
                case COMFORT:
                    [myButton setTitle:[NSString stringWithFormat:@"%@", _schedule.temp4 ] forState:UIControlStateNormal];
                    break;
            }
        }
    }
}

- (IBAction)sendControll:(id)sender {
    UIButton *myButton = (UIButton*)sender;
    switch (myButton.tag) {
        case  111:
            [_device setStateValue:[[NSNumber alloc] initWithInt:1] forName:@"controll"];
            [[SYAPIManager sharedInstance] postControllForDevice:_device
             
                                                         success:^(AFHTTPRequestOperation * operation, id response){
                                                             NSLog(@"Temperature changed OK!");
                                                         }
                                                         failure:^(AFHTTPRequestOperation * request, NSError * error){
                                                             NSLog(@"Temperature change Failure!");
                                                         }];
            break;
        case 112:
            [_device setStateValue:[[NSNumber alloc] initWithInt:2] forName:@"controll"];
            [[SYAPIManager sharedInstance] postControllForDevice:_device
             
                                                         success:^(AFHTTPRequestOperation * operation, id response){
                                                             NSLog(@"Temperature changed OK!");
                                                         }
                                                         failure:^(AFHTTPRequestOperation * request, NSError * error){
                                                             NSLog(@"Temperature change Failure!");
                                                         }];
            break;
        case 113:
            [_device setStateValue:[[NSNumber alloc] initWithInt:3] forName:@"controll"];
            [[SYAPIManager sharedInstance] postControllForDevice:_device
             
                                                         success:^(AFHTTPRequestOperation * operation, id response){
                                                             NSLog(@"Temperature changed OK!");
                                                         }
                                                         failure:^(AFHTTPRequestOperation * request, NSError * error){
                                                             NSLog(@"Temperature change Failure!");
                                                         }];
            break;
        case 114:
            [_device setStateValue:[[NSNumber alloc] initWithInt:4] forName:@"controll"];
            [[SYAPIManager sharedInstance] postControllForDevice:_device
             
                                                         success:^(AFHTTPRequestOperation * operation, id response){
                                                             NSLog(@"Temperature changed OK!");
                                                         }
                                                         failure:^(AFHTTPRequestOperation * request, NSError * error){
                                                             NSLog(@"Temperature change Failure!");
                                                         }];
            break;
    }

}
- (IBAction)changeHeatMode:(id)sender {
    if([[_device getStateValueForStateName:@"controll"] intValue] == 2){
        [self showHolidayWarning];
        return;
    }
    
    UIButton * button =(UIButton*)sender;
        switch (button.tag) {
            case MINIMUM:
                [_device setStateValue:[[NSNumber alloc] initWithInt:MINIMUM] forName:@"mode"];
                [[SYAPIManager sharedInstance] postModeForDevice:_device
                 
                                                         success:^(AFHTTPRequestOperation * operation, id response){
                                                             NSLog(@"Temperature changed OK!");
                                                         }
                                                         failure:^(AFHTTPRequestOperation * request, NSError * error){
                                                             NSLog(@"Temperature change Failure!");
                                                         }];
                break;
            case ATTENUATION:
                [_device setStateValue:[[NSNumber alloc] initWithInt:ATTENUATION] forName:@"mode"];
                [[SYAPIManager sharedInstance] postModeForDevice:_device
                 
                                                         success:^(AFHTTPRequestOperation * operation, id response){
                                                             NSLog(@"Temperature changed OK!");
                                                         }
                                                         failure:^(AFHTTPRequestOperation * request, NSError * error){
                                                             NSLog(@"Temperature change Failure!");
                                                         }];
                
                break;
            case NORMAL:
                [_device setStateValue:[[NSNumber alloc] initWithInt:NORMAL] forName:@"mode"];
                [[SYAPIManager sharedInstance] postModeForDevice:_device
                 
                                                         success:^(AFHTTPRequestOperation * operation, id response){
                                                             NSLog(@"Temperature changed OK!");
                                                         }
                                                         failure:^(AFHTTPRequestOperation * request, NSError * error){
                                                             NSLog(@"Temperature change Failure!");
                                                         }];
                
                break;
            case COMFORT:
                [_device setStateValue:[[NSNumber alloc] initWithInt:COMFORT] forName:@"mode"];
                [[SYAPIManager sharedInstance] postModeForDevice:_device
                 
                                                         success:^(AFHTTPRequestOperation * operation, id response){
                                                             NSLog(@"Temperature changed OK!");
                                                         }
                                                         failure:^(AFHTTPRequestOperation * request, NSError * error){
                                                             NSLog(@"Temperature change Failure!");
                                                         }];
                
                break;
        }
}

- (IBAction)onOffTap:(id)sender {
    if([[_device getStateValueForStateName:@"controll"] intValue] == 2){
        [self showHolidayWarning];
        return;
    }
    
    NSString*powerString = nil;
    if([_device hasStateName:@"power"]){
        powerString = @"power";
    } else {
        powerString = @"on";
    }
    
    NSDictionary * data = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithBool:!_onButton.isSelected],powerString, nil];
    
    NSLog(@"Data: %@", data);
    
    [[SYAPIManager sharedInstance] postDeviceActionWithDictionary:data device:_device success:^(AFHTTPRequestOperation * operation, id response){
        NSLog(@"Batch devices changed OK!");
    }
                                                          failure:^(AFHTTPRequestOperation * request, NSError * error){
                                                              NSLog(@"Batch devices failed!");
                                                          }];
}

- (void)initFetchedResultsControllerWithHCAID:(NSString*)hcaID {
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"State"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"device.label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.device.deviceID == %@", hcaID];
    [fetchRequest setPredicate:predicate];
    _devices = [[NSFetchedResultsController alloc]
                initWithFetchRequest:fetchRequest
                managedObjectContext:context
                sectionNameKeyPath:nil
                cacheName:nil];
    _devices.delegate = self;
    NSError *error;
    
    if (![_devices performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }else if ([[_devices fetchedObjects] count]>0){
        _device = (HeatCoolArea*)((State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if ([_devices.fetchedObjects count]>0){
        _device = (HeatCoolArea*)((State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
        
        [self initGUI];
    }
}

-(void)showHolidayWarning{

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"warning", nil)
                                                        message:NSLocalizedString(@"heat_cool_area_holiday_alert", nil)
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
}

-(double) nearestNumber:(double)num step:(double)step{
    NSLog(@"input number : %f step is %f", num, step);
    
    if (fmod(num, step) == 0)
        NSLog(@"OK");
    else if (fmod(num, step) < ((float)step/2.0))
        num = num -fmod(num, step);
    else
        num = num + (step - fmod(num, step));
    return num;
}

-(void)valueChanged:(EFCircularSlider*)circularSlider {
   //if(!_afterLoad)
    //[_circularSlider setCurrentArcValue:(CGFloat)50 forStartAnglePadding:2 endAnglePadding:2];
    
    _correction = [circularSlider getCurrentArcValueForStartAnglePadding:2 endAnglePadding:2]/10 - 5;
    _correction = [self nearestNumber:_correction step:0.5];
    _lSetTemperature.text = [NSString stringWithFormat:@"%.1f", tempWithoutCorrection + _correction];
    NSLog(@"correction :%f", _correction);
}

-(void)touchCancel:(EFCircularSlider*)circularSlider{
    NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithDouble:_correction],@"correction", nil];
    
    _correction = [[_device getStateValueForStateName:@"correction"]doubleValue];
    double valueForSlider = (_correction + 5)*10;
    
    [_circularSlider setCurrentArcValue:valueForSlider forStartAnglePadding:2 endAnglePadding:2];
    
    [[SYAPIManager sharedInstance] putDeviceAction:dict device:_device success:^(AFHTTPRequestOperation *operation, id response) {
        NSLog(@"correction sent!", nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"failed to send correction error: %@", [error localizedDescription]);
    }];
}

@end
