//
//  GuideELANManualViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 27.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "GuideELANManualViewController.h"
#import "SYCoreDataManager.h"
#import "SYAPIManager.h"
#import "Elan+Dictionary.h"

@interface GuideELANManualViewController ()
@property (weak, nonatomic) IBOutlet UITextField *name;
@property (weak, nonatomic) IBOutlet UITextField *address;
@property (weak, nonatomic) IBOutlet UITextField *port;

@end

@implementation GuideELANManualViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"enterName", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
    NSAttributedString *addressPlaceholder = [[NSAttributedString alloc] initWithString:@"Address" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
    
    _name.attributedPlaceholder = namePlaceHolder;
    _address.attributedPlaceholder = addressPlaceholder;
    
    _name.delegate = _address.delegate = _port.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [textField resignFirstResponder];
    }
    return NO; // We do not want UITextField to insert line-breaks.
}
- (IBAction)saveELAN:(id)sender {
    if ([_name.text isEqualToString:@""]){
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"elan_name_dialog_title", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        return;
    } if([_address.text isEqualToString:@""]) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"ipError", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        return;
    }if ([_port.text isEqualToString:@""] ){
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"portError", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        return;
        
        
    }
    
    
    NSString* elanLabel = _name.text;
    NSString* elanBaseAddress = [[_address.text stringByAppendingString:@":"] stringByAppendingString:_port.text];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"baseAddress == %@", elanBaseAddress];
    
    NSArray * foundElans = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Elan" withPredicate:predicate];
    if (foundElans.count!=0){
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"elan_already_saved", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        return;
    }
    
    
    //Obtain additionally mac address and fw version
    NSURL * baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/", elanBaseAddress]];
    
    [[SYAPIManager sharedInstance] getAPIRootForURL:baseURL success:^(AFHTTPRequestOperation* request, id object){
        NSDictionary * objectDict = (NSDictionary*)object;
        NSDictionary * info = [objectDict objectForKey:@"info"];
        
        
        Elan*_elan = [[SYCoreDataManager sharedInstance] createEntityElan];
        _elan.mac = [info valueForKey:@"MAC address"];
        [_elan parseTypeAndVersion:info];
        _elan.baseAddress = elanBaseAddress;
        _elan.label = elanLabel;
        
        [[SYCoreDataManager sharedInstance] saveContext];
        
        [self performSegueWithIdentifier:@"elanList" sender:self];
        
    }failure:^(AFHTTPRequestOperation * operation, NSError * error){
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"elan_not_reacheable", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        
    }];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
