//
//  SYHeatContainerViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/4/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
@interface SYHeatContainerViewController : UIViewController
- (void)setViewController:(NSString*)identifier;
@property (nonatomic, retain) Device* heatingDetailDevice;
@property (nonatomic, retain) NSDictionary* timeScheduleConfig;
@property (nonatomic, retain) NSDictionary* centralSourceItem;

@end
