//
//  DevicesLoader.m
//  ElanIOS
//
//  Created by Vratislav Zima on 5/30/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "DevicesLoader.h"
#import "Device.h"
#import "AppDelegate.h"
#import "DevicesResponse.h"
#import "DeviceDetailResponse.h"
#import "Room.h"
#import "ItemsResponse.h"
#import "RoomDetailResponse.h"
#import "SYCoreDataManager.h"
#import "Util.h"
@implementation DevicesLoader

@synthesize isLoading;
@synthesize rooms;
@synthesize devices;

- (id)init {
    self = [super init];
        
    
    return self;
}
/*
id _sender;
NSInteger roomsSize=0;
int roomsCouter=0;
int devicesCounter=0;
-(void) LoadDevices:(id)sender{
    isLoading = YES;
    roomsSize=0;
    roomsCouter=0;
    devicesCounter=0;

    //[app.myWS closeWS];

    [CoreDataManager deleteAllObjects:@"Device"];
    [CoreDataManager deleteAllObjects:@"Room"];
    [CoreDataManager deleteAllObjects:@"DeviceInRoom"];
    _sender = sender;
    URLConnector * url = [[URLConnector alloc] initWithAddressAndLoader:[Util getAddressWith:@"rooms"] activityInd:nil];
    ItemsResponse * roomsResponse = [[ItemsResponse alloc] init];
    [url setParser:roomsResponse];
    [url setConnectionListener:self];
    [url ConnectAndGetData];
    
}
-(void) LoadOnlyDevices:(id)sender{
    isLoading = YES;
    roomsSize=0;
    roomsCouter=0;
    devicesCounter=0;
    
    //[app.myWS closeWS];
    
    [CoreDataManager deleteAllObjects:@"Device"];
    _sender = sender;
    URLConnector * url = [[URLConnector alloc] initWithAddressAndLoader:[Util getAddressWith:@"devices"] activityInd:nil];
    DevicesResponse * roomsResponse = [[DevicesResponse alloc] init];
    [url setParser:roomsResponse];
    [url setConnectionListener:self];
    [url ConnectAndGetData];
    
}


NSInteger devicesSize;
- (void)requestDeviceInfo:(NSArray*)devicesArray sender:(id)sender{
    DeviceDetailResponse * deviceResponse;
    devicesSize = [devicesArray count];
    for (Device * device in devicesArray){
        URLConnector * url = [[URLConnector alloc] initWithAddressAndLoader:[NSString stringWithFormat:@"%@/%@", [Util getAddressWith:@"devices"],device.name] activityInd:nil];
        deviceResponse = [[DeviceDetailResponse alloc] init];
        [url setParser:deviceResponse];
        [url setConnectionListener:sender];
        [url ConnectAndGetData];
        
        
    }
    
}





-(void)connectionResponseOK:(NSObject *)returnedObject response:(id<Response>)responseType{
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext* objectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [objectContext setPersistentStoreCoordinator:appDelegate.persistentStoreCoordinator];
    NSError* error;
    
    if ([responseType isKindOfClass:[ItemsResponse class]] ){
        rooms = [[NSMutableArray alloc] initWithArray:(NSArray*) returnedObject];
        //        [self saveRooms:rooms];
        //       [collections reloadData];
        roomsSize=[rooms count];
        roomsCouter=0;
        for (Room*room in rooms){
            URLConnector * url = [[URLConnector alloc] initWithAddressAndLoader:[NSString stringWithFormat:@"%@/%@", [Util getAddressWith:@"rooms"],room.name] activityInd:nil];
            
            RoomDetailResponse * itemsResponse = [[RoomDetailResponse alloc] init];
            
            [url setParser:itemsResponse];
            [url setConnectionListener:self];
            url.name = room.name;
            [url ConnectAndGetData];
        }
    }else if ([responseType isKindOfClass:[RoomDetailResponse class]] ){
        NSArray * returnedArray = (NSArray*) returnedObject;
        Room* room = (Room*)[returnedArray objectAtIndex:0];
        [CoreDataManager saveRoom:room inContext:objectContext];
        roomsCouter++;
        for (int i=1; i < [returnedArray count]; i++){
            [CoreDataManager storeRoomDeviceConnection:room.name deviceId:(NSString*)[returnedArray objectAtIndex:i] inContext:objectContext];
        }
        if (roomsCouter>=roomsSize){
        
            URLConnector * url = [[URLConnector alloc] initWithAddressAndLoader:[Util getAddressWith:@"devices"] activityInd:nil];
        
            DevicesResponse * itemsResponse = [[DevicesResponse alloc] init];
        
            [url setParser:itemsResponse];
            [url setConnectionListener:self];
        
            [url ConnectAndGetData];
        }
        
        [objectContext save:&error];
    }else if ([responseType isKindOfClass:[DevicesResponse class]]){
        [self requestDeviceInfo:(NSArray*)returnedObject sender:self];
    }else if ([responseType isKindOfClass:[DeviceDetailResponse class]]){
        [CoreDataManager saveDevice:(Device*)returnedObject inContext:objectContext];
        [objectContext save:&error];
        devicesCounter++;
        if (devicesCounter>=devicesSize){
            dispatch_async(dispatch_get_main_queue(), ^{
                [appDelegate.viewController syncComplete:nil];
               // [app.myWS reconnect];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self];
            });
        }
    }
}




-(void) connectionResponseFailed:(NSInteger)code{
    
    
    
}
 */
@end
