//
//  SYViewScenesViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/7/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYViewScenesViewController.h"
#import "SceneLoader.h"
#import "AppDelegate.h"
#import "SYAPIManager.h"
@interface SYViewScenesViewController ()

@property (nonatomic, strong) NSFetchedResultsController* scenes;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation SYViewScenesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initFetchedResultsController];
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self initFetchedResultsController];
}
- (void)initLocalizableStrings {

}

/*

}
-(void)viewWillAppear:(BOOL)animated{
    [_leftBottomButton setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    [_rightBottomButton setTitle:NSLocalizedString(@"menu", nil) forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loaderComplete:)
                                                 name:@"sceneLoaderComplete"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncComplete:)
                                                 name:@"dataSyncComplete"
                                               object:nil];
#warning Reconnecting websocket
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [app.myWS reconnect];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
*/
#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Scene"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _scenes = [[NSFetchedResultsController alloc]
                     initWithFetchRequest:fetchRequest
                     managedObjectContext:context
                     sectionNameKeyPath:nil
                     cacheName:nil];
    _scenes.delegate = self;
    NSError *error;
    
    if (![_scenes performFetch:&error]) {
        NSLog(@"error fetching Scenes: %@",[error description]);
    }
}



- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.collectionView reloadData];
}



-(void)connectionResponseFailed:(NSInteger)code{
    [self.tableView reloadData];

}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
/*
-(void)expired{
    if (isLoaded==NO){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Scenes could not be loaded. Please try again later"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

        [_loaderDialog hide];
    }

}
*/
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    return [self.scenes.fetchedObjects count];
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    
    
    Scene* scene =[self.scenes objectAtIndexPath:indexPath];
    
    UILabel * cellName  = (UILabel*)[cell viewWithTag:2];
    cellName.text = scene.label;
  
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    [[SYAPIManager sharedInstance] performScene:((Scene*)[_scenes objectAtIndexPath:indexPath]) success:^(AFHTTPRequestOperation* operation, id response) {
        [_collectionView reloadData];
    }failure:^(AFHTTPRequestOperation * operation, NSError * error){
        [_collectionView reloadData];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"elan_not_reacheable", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        NSLog(@"Elan did not respond fot action!");
        
    }];

}
@end
