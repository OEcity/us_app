//
//  SY_State_RGBView.m
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 18/06/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "SY_State_TempTrimmerView.h"

@implementation SY_State_TempTrimmerView


- (instancetype)init {
    self = [super init];
    _maxValue = -1;
    return self;
}

#define DEGREES_TO_RADIANS(angle) (angle/180.0*M_PI)

- (void)rotateView:(UIView *)view
            inView:(UIView *)superVeiw
           degrees:(CGFloat)degrees {
    CGRect imageBounds = view.bounds;
    [view.layer setAnchorPoint:CGPointMake(((superVeiw.center.x-CGRectGetMinX(superVeiw.frame))-CGRectGetMinX(view.frame))/ imageBounds.size.width, ((superVeiw.center.y-CGRectGetMinY(superVeiw.frame))-CGRectGetMinY(view.frame)) / imageBounds.size.height)];
    [view setCenter:CGPointMake(superVeiw.center.x-superVeiw.frame.origin.x, superVeiw.center.y-superVeiw.frame.origin.y)];
    view.transform = CGAffineTransformRotate(view.transform, DEGREES_TO_RADIANS(degrees));
}

-(void) displayTemp{
    [_lTemp setHidden:NO];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.roundingIncrement = [NSNumber numberWithDouble:0.1];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.decimalSeparator = @",";
    [_lTemp setText:[NSString stringWithFormat:@"%@\n ºC", [formatter stringFromNumber:_temp]]];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    for(UIView *view in [_vCircleView subviews]){
        [view removeFromSuperview];
    }
    if (!_showTrimmer){
        [_vImageCircle setHidden:NO];
        [_vCircleView setHidden:YES];
        if (_on){
            [_vImageCircle setImage:[UIImage imageNamed:@"kolecko_barevne"]];
        } else{
            [_vImageCircle setImage:[UIImage imageNamed:@"kolecko_sede"]];
        }
    } else {
        [_vImageCircle setHidden:YES];
        [_vCircleView setHidden:NO];
        int max = 50;
        float onValue = (float)_value/_maxValue;
        UIImageView * myImage;
        for (int i=0; i < max; i++){
            if (((float)i/max)>=onValue){
                myImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stmivani_off.png"]];
            }else{
                myImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stmivani_on1.png"]];
            }
            myImage.frame = CGRectMake(_vCircleView.bounds.size.width/2 - myImage.frame.size.width/2 , 0, myImage.frame.size.width, myImage.frame.size.height);
            [_vCircleView addSubview:myImage];
            float consMove =(360.0/max);
            [self rotateView:myImage inView:_vCircleView degrees:i*consMove - (360/4)];
        }
    }
    [self displayTemp];
}


@end
