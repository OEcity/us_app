//
//  OneDayHeatTimeAdjustView.h
//  iHC-MIRF
//
//  Created by admin on 27.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TempScheduleDay.h"
#import "TempDayMode.h"

@protocol OneDayHeatTimeModesTemperatuDataSource <NSObject>
@optional
-(NSNumber *)days:(NSUInteger)index;
@end

@protocol OneDayHeatTimeModesEditDelegate <NSObject>
@optional
-(void)putEditingOnScreen:(TempDayMode*)mode modePos:(int)modePos dayInWeek:(int)day;
@end

@interface OneDayHeatTimeAdjustView : UIView<UIGestureRecognizerDelegate>

@property (nonatomic, retain) TempScheduleDay * mDay;
@property NSInteger dayInWeek;
@property (nonatomic, weak) id <OneDayHeatTimeModesTemperatuDataSource> dataSource;
@property (nonatomic, weak) id <OneDayHeatTimeModesEditDelegate> delegate;
@property int mModePosition;

-(void) repaintDay:(TempScheduleDay*) day;
-(void)paintDateFirstTime:(TempScheduleDay *)day dayInWeek:(NSInteger)dayInWeek;
-(void)repaintContent;


@end
