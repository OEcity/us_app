//
//  WeatherOptionsViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 04.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "WeatherOptionsViewController.h"
#import "SYAPIManager.h"
#import "UIViewController+RevealViewControllerAddon.h"
#import "Constants.h"
#import "SYCoreDataManager.h"
#import "AppDelegate.h"
#import "SYDataLoader.h"

@interface WeatherOptionsViewController (){
    BOOL openWSelected;
    BOOL giomSelected;
    BOOL elanSelected;
    
    NSString* latitude;
    NSString*longtitude;
    NSString*ipAdrress;
    NSString*port;
    
    UITextField*tfLat;
    UITextField*tfLon;
    UITextField*tfIPAddress;
    UITextField*tfPort;
    
    UITableView*openWeatherTableView;
    UITableView*giomTableView;
    UITableView*elansTableView;
    
    Elan*OWselectedElan;
    Elan*GiomSelectedElan;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;

@end

@implementation WeatherOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:NO];
    // Do any additional setup after loading the view.
    
    openWSelected = giomSelected = elanSelected = NO;
    
    NSDictionary*coords = [[NSUserDefaults standardUserDefaults] objectForKey:@"weatherCoords"];
    latitude = [coords objectForKey:@"lat"];
    longtitude = [coords objectForKey:@"lon"];
    ipAdrress = [[NSUserDefaults standardUserDefaults] objectForKey:@"ipAddress"];
    port =[[NSUserDefaults standardUserDefaults] objectForKey:@"port"];
    
    NSString*macAddress = [[NSUserDefaults standardUserDefaults] objectForKey:@"weatherElanMac"];
    
    if(macAddress && latitude){
        OWselectedElan = [[SYCoreDataManager sharedInstance] getELANwithMAC:macAddress inContext:[[SYCoreDataManager sharedInstance]privateObjectContext]];
    }
    
    _saveButton.hidden = _deleteButton.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)saveWeather:(id)sender {
    NSMutableDictionary*dict = [NSMutableDictionary new];
    if(openWSelected){
    NSNumber *lat = [NSNumber numberWithDouble:[latitude doubleValue]];
    NSNumber *longt = [NSNumber numberWithDouble:[longtitude doubleValue]];
    
    [dict setObject:lat forKey:@"lat"];
    [dict setObject:longt forKey:@"lon"];
    
//    NSDictionary *dictToSend = [NSDictionary dictionaryWithObjectsAndKeys:dict, @"coordinates", nil];
        NSMutableDictionary *dictToSend = [[NSMutableDictionary alloc] init];
        [dictToSend setObject:dict forKey:@"coordinates"];
    
        Elan*elanToSend = nil;
        if(openWSelected && OWselectedElan){
            elanToSend = OWselectedElan;
        }
        if(elanToSend == nil)return;
        

            [[SYAPIManager sharedInstance] postWeatherCoordinates:dictToSend toElan:elanToSend success:^(AFHTTPRequestOperation *operation, id response) {
                NSLog(@"success resoponse: %@", response);
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"Error: %@ :  %@  :  %@", [error localizedDescription], error.debugDescription , error.description);
                NSString *jsonString;
                jsonString = [[NSString alloc] initWithData:operation.request.HTTPBody encoding:NSUTF8StringEncoding];
                NSLog(@"Operation :%@", jsonString);
            }];
    
    NSLog(@"%@", dictToSend);

    

    } else if(giomSelected){
        Elan*elanToSend = nil;
        elanToSend = GiomSelectedElan;
        if(elanToSend == nil)return;
        
        [dict setObject:ipAdrress forKey:@"IP address"];
        [dict setObject:port forKey:@"port"];
        [[SYAPIManager sharedInstance] postWeatherCoordinates:dict toElan:elanToSend success:^(AFHTTPRequestOperation *operation, id response) {
            NSLog(@"success resoponse: %@", response);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", [error localizedDescription]);
        }];
    }
}

- (IBAction)deleteWeather:(id)sender {
    Elan*elan = nil;
    if(OWselectedElan){
        elan = OWselectedElan;
    }
    if(GiomSelectedElan){
        elan = GiomSelectedElan;
    }
    
    if(elan == nil)return;
    [[SYAPIManager sharedInstance] deleteWeatherFromElan:elan success:^(AFHTTPRequestOperation *operation, id response) {
        NSLog(@"success delete weather");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", [error localizedDescription]);
    }];
    
    AppDelegate*app = (AppDelegate*) [[UIApplication sharedApplication]delegate];
    app.hasOPWeather = app.hasGIOMWeather = NO;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return NO;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView.tag == 2 && openWSelected)return 4;
    if(tableView.tag == 3 && giomSelected)return 4;
    if(tableView.tag == 1)return 2;
    if(tableView.tag == 5 && elanSelected)return [[[SYCoreDataManager sharedInstance] getAllRFElans] count]+1;
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1 && openWSelected && indexPath.row == 0 && elanSelected)return (4*50)+([[[SYCoreDataManager sharedInstance]getAllRFElans]count]*50) + 8;
    if(tableView.tag == 1 && giomSelected && indexPath.row == 1 && elanSelected)return (4*50)+([[[SYCoreDataManager sharedInstance]getAllRFElans]count]*50) + 8;
    if(tableView.tag == 1 && openWSelected && indexPath.row == 0)return 4*50;
    if(tableView.tag == 1 && giomSelected && indexPath.row == 1)return 4*50;
    if(tableView.tag == 2 && elanSelected && indexPath.row == 3)return [[[SYCoreDataManager sharedInstance]getAllRFElans]count]*50+50;
    if(tableView.tag == 3 && elanSelected && indexPath.row == 3)return [[[SYCoreDataManager sharedInstance]getAllRFElans]count]*50+50;
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    if(tableView.tag == 1){
        UITableViewCell * cell = nil;
        
        switch (indexPath.row) {
            case 0:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 2;
                tbv.delegate = self;
                tbv.dataSource = self;
                openWeatherTableView = tbv;
            }
                break;
            case 1:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag=3;
                tbv.delegate = self;
                tbv.dataSource = self;
                giomTableView = tbv;
            }
                break;
            default:
                break;
        }
        
        return cell;
        
        
    } else if(tableView.tag == 2){
        NSAttributedString *Placeholder = nil;
        switch (indexPath.row){
            case 0:{
                UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
                
                if(app.hasOPWeather && OWselectedElan){
                    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
                
                UIView *bgColorView = [[UIView alloc] init];
                bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
                [cell setSelectedBackgroundView:bgColorView];
                
                UILabel* label = [[cell contentView] viewWithTag:1];
                UIImageView *img = [[cell contentView ] viewWithTag:2];
                UIView *line = [[cell contentView ] viewWithTag:3];
                
                
                
                label.text = @"OpenWeatherMap";
                
                if(openWSelected){
                    img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
                    line.hidden = YES;
                        
                } else {
                    img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
                    img.hidden = NO;
                    line.hidden = NO;
                }
                return cell;
            }
                break;
            case 1:{
                UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField*tf = [[[cell contentView] subviews]firstObject];
                Placeholder = [[NSAttributedString alloc] initWithString:@"Latitude" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                tf.attributedPlaceholder = Placeholder;
                tfLat = tf;
                
                if(latitude.intValue != 0)
                tf.text = [NSString stringWithFormat:@"%f", latitude.doubleValue];
                
                tf.delegate = self;
                
                UIView *bgColorView = [[UIView alloc] init];
                cell.selectedBackgroundView = bgColorView;
                return cell;
            }
                break;
            case 2:{
                UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField*tf = [[[cell contentView] subviews]firstObject];
                Placeholder = [[NSAttributedString alloc] initWithString:@"Longtitude" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                tf.attributedPlaceholder = Placeholder;
                tfLon = tf;
                
                if(longtitude.intValue != 0)
                tf.text = [NSString stringWithFormat:@"%f", longtitude.doubleValue];
                
                tf.delegate = self;
                
                UIView *bgColorView = [[UIView alloc] init];
                cell.selectedBackgroundView = bgColorView;
                return cell;
            }
                break;
            case 3:{
                UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 5;
                tbv.delegate = self;
                tbv.dataSource = self;
                elansTableView = tbv;
                
                UIView *bgColorView = [[UIView alloc] init];
                cell.selectedBackgroundView = bgColorView;
                return cell;
            }
                break;
        }
    } else if(tableView.tag == 3){
        
        switch(indexPath.row){
            case 0:{
                UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
                
                    UIView *bgColorView = [[UIView alloc] init];
                    bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
                    [cell setSelectedBackgroundView:bgColorView];
                
                if(app.hasGIOMWeather && GiomSelectedElan){
                    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
                
                UILabel* label = [[cell contentView] viewWithTag:1];
                UIImageView *img = [[cell contentView ] viewWithTag:2];
                UIView *line = [[cell contentView ] viewWithTag:3];
                
                
                label.text = @"Giom";
                
                if(giomSelected){
                    img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
                    line.hidden = YES;
                    
                } else {
                    img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
                    img.hidden = NO;
                    line.hidden = NO;
                }
                return cell;
            }
                break;
            case 1:{
                NSAttributedString* Placeholder = [[NSAttributedString alloc] initWithString:@"ip_address" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField*tf = [[[cell contentView] subviews]firstObject];
                tfIPAddress = tf;
                
                tf.text = ipAdrress;
                
                tf.delegate = self;
                
                tf.attributedPlaceholder = Placeholder;
                
                UIView *bgColorView = [[UIView alloc] init];
                cell.selectedBackgroundView = bgColorView;
                return cell;
            }
                break;
            case 2:{
                UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField*tf = [[[cell contentView] subviews]firstObject];
                NSAttributedString* Placeholder = [[NSAttributedString alloc] initWithString:@"Port" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                tf.attributedPlaceholder = Placeholder;
                tfPort = tf;
                
                tf.delegate = self;
                tf.text = port;
                
                UIView *bgColorView = [[UIView alloc] init];
                cell.selectedBackgroundView = bgColorView;
                return cell;
            }
                break;
            case 3:{
                UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 5;
                tbv.delegate = self;
                tbv.dataSource = self;
                elansTableView = tbv;
                
                UIView *bgColorView = [[UIView alloc] init];
                cell.selectedBackgroundView = bgColorView;
                return cell;
            }
                break;
        }
    } else if(tableView == elansTableView){
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        
        
        
        switch (indexPath.row) {
            case 0:
                if(elanSelected){
                    img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
                    line.hidden = YES;
                    label.text = @"eLAN";
                } else {
                    img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
                    img.hidden = NO;
                    line.hidden = NO;
                    
                    label.text = @"eLAN";
                    
                    if(OWselectedElan && openWSelected){
                        label.text = OWselectedElan.label;
                    }
                    
                    if(GiomSelectedElan && giomSelected){
                        label.text = label.text = GiomSelectedElan.label;
                    }
                }
                break;
                
            default:
                img.hidden = YES;
                line.hidden = YES;
                Elan *myElan = [[[SYCoreDataManager sharedInstance] getAllRFElans] objectAtIndex:indexPath.row-1];
                label.text = myElan.label;
                
                NSLog(@"%@ : %@", OWselectedElan.mac, myElan.mac);
                if([OWselectedElan.mac isEqualToString:myElan.mac] && openWSelected){
                    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
                
                if([GiomSelectedElan.mac isEqualToString: myElan.mac] && giomSelected ) {
                    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
                break;
        }
        
        if(elanSelected){
            line.hidden = YES;
        } else {
            line.hidden = NO;
        }
        return cell;
    }
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    return cell;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSString*key = nil;
    if(textField == tfLat){
        latitude = textField.text;
        key = @"latitude";
    } else if(textField == tfLon){
        longtitude = textField.text;
        key = @"longtitude";
    } else if(textField == tfIPAddress){
        ipAdrress = textField.text;
        key = @"ipAddress";
    } else if(textField == tfPort){
        port = textField.text;
        key = @"port";
    }
    
    NSUserDefaults *def = [NSUserDefaults standardUserDefaults];
    [def setObject:textField.text forKey:key];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"tableview tag :%ld", (long)tableView.tag);
    if(tableView.tag == 2 && indexPath.row == 0 && !openWSelected){
        openWSelected = YES;
        giomSelected = NO;
        elanSelected = NO;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
        
    } else if (tableView.tag == 2 && indexPath.row == 0 && openWSelected){
        openWSelected = NO;
        elanSelected = NO;
        
        tableView.layer.borderColor = nil;
        tableView.layer.borderWidth = 0.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
        
    } else if (tableView.tag == 3 && indexPath.row == 0 && !giomSelected){
        giomSelected = YES;
        openWSelected = NO;
        elanSelected = NO;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
    } else if (tableView.tag == 3 && indexPath.row == 0 && giomSelected){
        giomSelected = NO;
        elanSelected = NO;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
    } else if (tableView.tag == 5 && indexPath.row == 0 && !elanSelected){
        elanSelected = YES;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
    } else if (tableView.tag == 5 && indexPath.row == 0 && elanSelected) {
        elanSelected = NO;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
    } if(tableView.tag == 5 && indexPath.row > 0){
        if(openWSelected){
            OWselectedElan = [[[SYCoreDataManager sharedInstance] getAllRFElans] objectAtIndex:indexPath.row -1];
        } else {
            GiomSelectedElan = [[[SYCoreDataManager sharedInstance] getAllRFElans] objectAtIndex:indexPath.row -1];
        }
    }
    
    if(indexPath.row == 0)
        [tableView reloadData];

    [self setTableViewBorder];
}

-(void)setTableViewBorder{
    if(openWSelected){
        openWeatherTableView.layer.borderColor =USBlueColor.CGColor;
        openWeatherTableView.layer.borderWidth = 1.0f;
        
        giomTableView.layer.borderColor = nil;
        giomTableView.layer.borderWidth = 0;
    } else {
        openWeatherTableView.layer.borderColor = nil;
        openWeatherTableView.layer.borderWidth = 0;
    }
    
    if (giomSelected){
        openWeatherTableView.layer.borderColor = nil;
        openWeatherTableView.layer.borderWidth = 0;
        
        giomTableView.layer.borderColor =  USBlueColor.CGColor;
        giomTableView.layer.borderWidth = 1.0f;
    } else {
        giomTableView.layer.borderColor = nil;
        giomTableView.layer.borderWidth = 0;
    }
    
    if(elanSelected){
        elansTableView.layer.borderColor =  USBlueColor.CGColor;
        elansTableView.layer.borderWidth = 1.0f;
    } else {
        elansTableView.layer.borderColor = nil;
        elansTableView.layer.borderWidth = 0;
    }
    
    if(openWSelected || giomSelected){
        _tableView.scrollEnabled = YES;
        _saveButton.hidden = _deleteButton.hidden = NO;
    } else {
        _tableView.scrollEnabled = NO;
        _saveButton.hidden = _deleteButton.hidden = YES;
    }
    
    [giomTableView reloadData];
    [openWeatherTableView reloadData];
    [elansTableView reloadData];
}

@end
