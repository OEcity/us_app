//
//  ConfigureDevicesViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/18/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
#import "DeviceDetailScheduleViewController.h"
#import "DeviceTimeScheduleClass.h"

@interface SYConfigureDevicesViewController : UIViewController < NSFetchedResultsControllerDelegate,UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;

@end
