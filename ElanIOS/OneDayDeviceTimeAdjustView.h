//
//  OneDayDeviceTimeAdjustView.h
//  iHC-MIRF
//
//  Created by admin on 10.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceScheduleDay.h"
#import "TempDayMode.h"

@protocol OneDayDeviceTimeAdjustViewDataSource <NSObject>
@optional
-(NSString *)getPercentOfColor:(NSInteger)index;
@optional
-(UIColor *)getColorForPosintion:(NSInteger)position;
@optional
-(BOOL)shouldShowImage;
@optional
-(BOOL)shouldShowPercents;

@end

@protocol OneDayDeviceTimeAdjustViewDelegate <NSObject>
@optional
-(void)putEditingOnScreen:(TempDayMode*)mode modePos:(int)modePos dayInWeek:(int)day;
@end


@interface OneDayDeviceTimeAdjustView : UIView<UIGestureRecognizerDelegate>

@property (nonatomic, retain) DeviceScheduleDay * mDay;
@property NSInteger dayInWeek;
@property (nonatomic, weak) id <OneDayDeviceTimeAdjustViewDataSource> dataSource;
@property (nonatomic, weak) id <OneDayDeviceTimeAdjustViewDelegate> delegate;
@property int mModePosition;

-(void) repaintDay:(DeviceScheduleDay*) day;
-(void)paintDateFirstTime:(DeviceScheduleDay *)day dayInWeek:(NSInteger)dayInWeek;
-(void)repaintContent;



@end
