//
//  DeviceDetailScheduleViewController.h
//  iHC-MIRF
//
//  Created by Tom Odler on 03.03.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectedDeviceViewController.h"
#import "IntensityConfigurationViewController.h"
#import "RGBPlanViewController.h"
#import "LedWhitePlanViewController.h"
#import "DeviceTimeScheduleClass.h"
#import "HUDWrapper.h"

@interface DeviceDetailScheduleViewController : UIViewController<deviceSelectDelegate,intensitySetterDelegate,rgbSetterDelegate, whiteSetterDelegate, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableDictionary * schedule;
@property (nonatomic, retain) NSMutableArray*usedDevicesArray;
@property (nonatomic, retain) SelectedDeviceViewController *selectedDeviceViewController;
@property (nonatomic, retain) IntensityConfigurationViewController *brightnessViewController;
@property (nonatomic, retain) RGBPlanViewController *RGBViewController;
@property (nonatomic, retain) LedWhitePlanViewController *LedWhiteController;
@property (nonatomic) DeviceTimeScheduleClass *mySchedule;
@property (nonatomic) HUDWrapper *loaderDialog;

@end

