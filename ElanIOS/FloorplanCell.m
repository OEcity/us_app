//
//  FloorplanCell.m
//  iHC-MIRF
//
//  Created by Vlastimil Venclik on 10.10.13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "FloorplanCell.h"

@implementation FloorplanCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
