//
//  DevicesCollectionViewCell.h
//  US App
//
//  Created by Tom Odler on 01.02.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DevicesCollectionViewCell : UICollectionViewCell

@end
