//
//  SYAddSceneViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/1/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Scene.h"
#import "SYListScenesViewController.h"
#import "SYBaseViewController.h"
@interface SYAddSceneViewController : SYBaseViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UITextFieldDelegate>
@property (nonatomic, retain) IBOutlet UITextField * tf;
@property (nonatomic, retain) NSString  * sceneName;
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) NSMutableArray * rooms;
@property (nonatomic, retain) Scene * scene;
@property (nonatomic, retain) SYListScenesViewController * parentController;
@property (nonatomic, retain) NSIndexPath * selectedIndexPath;
@property (nonatomic, retain) NSMutableDictionary * selectedDevices;
@property (nonatomic, retain) NSMutableDictionary * deviceActions;
@property BOOL willDismiss;

@property (weak, nonatomic) IBOutlet UILabel *scenesLabel;
@property (weak, nonatomic) IBOutlet UIButton *bottomRightButton;
@property (weak, nonatomic) IBOutlet UIButton *bottomLeftButton;
@property (weak, nonatomic) IBOutlet UILabel *iconlabel;
@property (weak, nonatomic) IBOutlet UILabel *sceneNameLabel;
@property (nonatomic)NSMutableArray *actionsArray;


@end
