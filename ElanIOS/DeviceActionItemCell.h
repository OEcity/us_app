//
//  DeviceActionItemCell.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 07/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Brightness.h"
#import "ChangeTime.h"
#import "DeviceActionsViewController.h"
@interface DeviceActionItemCell : UITableViewCell
typedef enum {
    kPlain,
    kBrightness,
    kOn,
    kTime,
    kTimeAndButton
} Type;

#define kTypeArray @"ItemButton", @"ItemSlider", @"ItemOn", @"ItemOnlyTime",@"ItemTimeAndButton", nil

@property Type type;
@property (nonatomic, retain) IBOutlet UIView * view;
@property (nonatomic, retain) IBOutlet UIImageView * topBar;
@property (nonatomic, retain) IBOutlet UILabel * label;
@property (nonatomic, copy) NSString* deviceName;
@property (nonatomic, copy) NSString* actionName;
@property (nonatomic, retain) IBOutlet UIButton * nullButton;
@property (nonatomic, retain) DeviceActionsViewController * deviceActionsViewController;
//----------- Only for ItemSlider
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (nonatomic, retain) Brightness * brightnessSettings;
@property (nonatomic, retain) NSNumber * brightnessState;
@property (nonatomic, retain) NSDictionary * bundle;
@property CGFloat stepValue;


//--------- ItemTime --------------
@property (nonatomic, retain) ChangeTime * timeSettings;
@property (nonatomic, retain) NSNumber * timeDelayTime;

@property (nonatomic, copy) NSString * timeLabel;
@property CGFloat height;
@property CGFloat normalHeight;
@property CGFloat expandedHeight;

@property (retain, nonatomic) IBOutlet UILabel *secondsLabel;
@property (retain, nonatomic) IBOutlet UILabel *minutesLabel;
@property (retain, nonatomic) IBOutlet UILabel *hoursLabel;
@property int timerStep;
@property int timerMin;
@property int timerMax;
@property int resultTime;
@property (weak, nonatomic) IBOutlet UIButton *actionClock;
@property (weak, nonatomic) IBOutlet UIButton *setTimeButton;
@property (weak, nonatomic) IBOutlet UIButton *expandArrow;
@property (nonatomic, retain) NSDictionary * nameConverter;
@property (weak, nonatomic) IBOutlet UILabel *setTimeLabel;
//-----------ItemOn ------------
@property (nonatomic, retain) NSNumber * on;

-(id)initWithType:(Type)type owner:(id)owner bundle:(NSDictionary*)bundle;

- (IBAction)nullButtonClick:(id)sender;
- (IBAction)rollDown:(id)sender;
- (IBAction)setTime:(id)sender;
-(void)shrinkCell;


@end
