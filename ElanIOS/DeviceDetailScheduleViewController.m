//
//  DeviceDetailScheduleViewController.m
//  iHC-MIRF
//
//  Created by Tom Odler on 03.03.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "DeviceDetailScheduleViewController.h"
#import "SYCoreDataManager.h"
#import "Device.h"
#import "OneWeekDeviceScheduleViewController.h"
#import "EFCircularSlider.h"
#import "EFGradientGenerator.h"
#import "SYCoreDataManager.h"
#import "Elan.h"
#import "Constants.h"

@interface DeviceDetailScheduleViewController (){
    int val2;
    int val3;
    int val4;
    int val5;
    NSString* oldMode;
    
    NSString *myScheduleName;
    
    Elan*selectedElan;

    UITableView *modesTableview;
    UITableView *elansTableView;
    
    
    BOOL eLANsSelected;
    BOOL modesSelected;
    BOOL hysteresisSelected;
}

@property (nonatomic) BOOL shutters;
@property (nonatomic) BOOL dimming;
@property (nonatomic) BOOL rgb;
@property (nonatomic) BOOL whiteLED;
@property (nonatomic) BOOL switches;
@property (nonatomic) NSMutableArray* colorArray;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;




@end

@implementation DeviceDetailScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _shutters = _switches = _dimming =_rgb = _whiteLED = NO;
    
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    
    eLANsSelected = modesSelected = NO;
    
    _tableViewHeight.constant = 4*50;
    
    [_nextButton setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    if(_mySchedule !=nil){
        myScheduleName = _mySchedule.label;
        selectedElan = _mySchedule.elan;
        
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
        [[SYAPIManager sharedInstance] getDeviceScheduleDetailsWithName:_mySchedule.devicetimescheduleclassID fromElan:selectedElan success:
     ^(AFHTTPRequestOperation *request, id response){
         
         NSDictionary*dict = response;
         _schedule = [dict mutableCopy];
         if([_schedule objectForKey:@"type"] != nil){
             oldMode = [_schedule objectForKey:@"type"];
         }
         [self initGui:dict];
         [_loaderDialog hide];
     } failure:^(AFHTTPRequestOperation * request, NSError * error){
         [_loaderDialog hide];
         NSLog(@"error loading schedule: %@", [error debugDescription]);
     }];
    }
    
    _colorArray = [[NSMutableArray alloc] init];
    [self initColorArray];
}
-(void) initGui:(NSDictionary*)schedule{
    if (schedule!=nil){
        myScheduleName = [schedule objectForKey:@"label"];
        NSString *type = [schedule objectForKey:@"type"];
        [self initGUIwithType:type];
    }
}

-(void)initGUIwithType:(NSString*)type{
    
    NSLog(@"%@", type);
    
    if ([type containsString:@"switch"]){
        _dimming = _rgb = _whiteLED = _shutters = NO;

         NSMutableDictionary * modes = [[NSMutableDictionary alloc] init];
        if(_schedule == nil){
            _schedule = [[NSMutableDictionary alloc] init];
            NSDictionary *modeOff = [[NSDictionary alloc] initWithObjectsAndKeys:@"off",@"value", nil];
            NSDictionary *modeOn = [[NSDictionary alloc] initWithObjectsAndKeys:@"on",@"value", nil];
            
            [modes setObject:modeOff forKey:@"0"];
            [modes setObject:modeOn forKey:@"1"];
            
        [_schedule setObject:modes forKey:@"modes"];
            
        } else {
            
        modes = [[_schedule objectForKey:@"modes"] mutableCopy];
            if(![oldMode isEqualToString:type]){
                [modes removeAllObjects];
                if(modes == nil){
                    modes = [NSMutableDictionary new];
                    
                    NSDictionary *modeOff = [[NSDictionary alloc] initWithObjectsAndKeys:@"off",@"value", nil];
                    NSDictionary *modeOn = [[NSDictionary alloc] initWithObjectsAndKeys:@"on",@"value", nil];
                    
                    [modes setObject:modeOff forKey:@"0"];
                    [modes setObject:modeOn forKey:@"1"];
                    
                    [_schedule setObject:modes forKey:@"modes"];
                }
            }
        }

        [_schedule setObject:@"switch" forKey:@"type"];
        _switches = YES;

        [_tableView reloadData];
        
    } else if ([type containsString:@"dim"]){
        _switches = _rgb = _whiteLED = _shutters = NO;
        
        if(_schedule == nil){
            _schedule = [[NSMutableDictionary alloc] init];
        
        
        int modeValue2 = 20;
        int modeValue3 = 50;
        int modeValue4 = 70;
        int modeValue5 = 100;
        
        NSDictionary *mode0 = [[NSDictionary alloc] initWithObjectsAndKeys:@(0),@"value", nil];
        NSDictionary *mode2 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue2),@"value", nil];
        NSDictionary *mode3 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue3),@"value", nil];
        NSDictionary *mode4 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue4),@"value", nil];
        NSDictionary *mode5 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue5),@"value", nil];
        
        NSMutableDictionary *modes = [[NSMutableDictionary alloc] init];
        
        [modes setObject:mode0 forKey:@"1"];
        [modes setObject:mode2 forKey:@"2"];
        [modes setObject:mode3 forKey:@"3"];
        [modes setObject:mode4 forKey:@"4"];
        [modes setObject:mode5 forKey:@"5"];
        
        [_schedule setObject:modes forKey:@"modes"];
        
        } else {
            
        NSMutableDictionary *modes = [[NSMutableDictionary alloc] init];
            modes = [[_schedule objectForKey:@"modes"]mutableCopy];
            
            if(![oldMode isEqualToString:type]){
                
                [modes removeAllObjects];
                if(modes == nil){
                    modes = [NSMutableDictionary new];
                }
                int modeValue2 = 20;
                int modeValue3 = 50;
                int modeValue4 = 70;
                int modeValue5 = 100;
                
                NSDictionary *mode0 = [[NSDictionary alloc] initWithObjectsAndKeys:@(0),@"value", nil];
                NSDictionary *mode2 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue2),@"value", nil];
                NSDictionary *mode3 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue3),@"value", nil];
                NSDictionary *mode4 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue4),@"value", nil];
                NSDictionary *mode5 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue5),@"value", nil];
                
                [modes setObject:mode0 forKey:@"1"];
                [modes setObject:mode2 forKey:@"2"];
                [modes setObject:mode3 forKey:@"3"];
                [modes setObject:mode4 forKey:@"4"];
                [modes setObject:mode5 forKey:@"5"];
                
                [_schedule setObject:modes forKey:@"modes"];
                
                [_schedule removeObjectForKey:@"schedule"];
            }
        }
        
        [_schedule setObject:@"diming" forKey:@"type"];
        
        _dimming = YES;

        [_tableView reloadData];
        
    } else if ([type containsString:@"shut"]){
        _dimming = _rgb = _whiteLED = _switches = NO;
        
        if(_schedule == nil){
            _schedule = [[NSMutableDictionary alloc] init];
            
            NSMutableDictionary * modes = [[NSMutableDictionary alloc] init];
            
            NSDictionary *modeUp = [[NSDictionary alloc] initWithObjectsAndKeys:@"up",@"value", nil];
            NSDictionary *modeDown = [[NSDictionary alloc] initWithObjectsAndKeys:@"down",@"value", nil];
            
            [modes setObject:modeUp forKey:@"1"];
            [modes setObject:modeDown forKey:@"2"];
            [_schedule setObject:modes forKey:@"modes"];
            
            
        } else {
            NSMutableDictionary *modes = [[_schedule objectForKey:@"modes"] mutableCopy];
            
            if(![oldMode isEqualToString:type]){
                
                [modes removeAllObjects];
                if(modes == nil){
                    modes = [NSMutableDictionary new];
                }
            
            
                [modes removeAllObjects];
                NSDictionary *modeUp = [[NSDictionary alloc] initWithObjectsAndKeys:@"up",@"value", nil];
                NSDictionary *modeDown = [[NSDictionary alloc] initWithObjectsAndKeys:@"down",@"value", nil];
            
                [modes setObject:modeUp forKey:@"1"];
                [modes setObject:modeDown forKey:@"2"];
                [_schedule setObject:modes forKey:@"modes"];
                
                [_schedule removeObjectForKey:@"schedule"];
            }
            
        }

        [_schedule setObject:@"shutters" forKey:@"type"];
        _shutters = YES;
        [_tableView reloadData];
        
    } else if([type isEqualToString:@"rgb"]){
        _dimming = _switches = _whiteLED = _shutters = NO;
        
        if(_schedule == nil){
            _schedule = [[NSMutableDictionary alloc] init];
            
            int modeValue2 = 20;
            int modeValue3 = 50;
            int modeValue4 = 70;
            int modeValue5 = 100;
            
            int rgbValue = 255;
            
            NSDictionary *mode0 = [[NSDictionary alloc] initWithObjectsAndKeys:@(0), @"brightness", @(rgbValue), @"R", @(rgbValue), @"G", @(rgbValue), @"B", nil];
            NSDictionary *mode2 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue2), @"brightness", @(rgbValue), @"R", @(rgbValue), @"G", @(rgbValue), @"B", nil];
            NSDictionary *mode3 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue3), @"brightness", @(rgbValue), @"R", @(rgbValue), @"G", @(rgbValue), @"B", nil];
            NSDictionary *mode4 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue4), @"brightness", @(rgbValue), @"R", @(rgbValue), @"G", @(rgbValue), @"B", nil];
            NSDictionary *mode5 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue5), @"brightness", @(rgbValue), @"R", @(rgbValue), @"G", @(rgbValue), @"B", nil];
            
            NSMutableDictionary *modes = [[NSMutableDictionary alloc] init];
            
            [modes setObject:mode0 forKey:@"1"];
            [modes setObject:mode2 forKey:@"2"];
            [modes setObject:mode3 forKey:@"3"];
            [modes setObject:mode4 forKey:@"4"];
            [modes setObject:mode5 forKey:@"5"];
            
            [_schedule setObject:modes forKey:@"modes"];
            
            
        } else {
            NSMutableDictionary *modes = [[NSMutableDictionary alloc] init];
            modes = [[_schedule objectForKey:@"modes"]mutableCopy];
            
            if(![oldMode isEqualToString:type]){
                
                [modes removeAllObjects];
                if(modes == nil){
                    modes = [NSMutableDictionary new];
                }
                int modeValue2 = 20;
                int modeValue3 = 50;
                int modeValue4 = 70;
                int modeValue5 = 100;
                
                int rgbValue = 255;
                
                NSDictionary *mode0 = [[NSDictionary alloc] initWithObjectsAndKeys:@(0), @"brightness", @(rgbValue), @"R", @(rgbValue), @"G", @(rgbValue), @"B", nil];
                NSDictionary *mode2 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue2), @"brightness", @(rgbValue), @"R", @(rgbValue), @"G", @(rgbValue), @"B", nil];
                NSDictionary *mode3 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue3), @"brightness", @(rgbValue), @"R", @(rgbValue), @"G", @(rgbValue), @"B", nil];
                NSDictionary *mode4 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue4), @"brightness", @(rgbValue), @"R", @(rgbValue), @"G", @(rgbValue), @"B", nil];
                NSDictionary *mode5 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue5), @"brightness", @(rgbValue), @"R", @(rgbValue), @"G", @(rgbValue), @"B", nil];
                
                [modes setObject:mode0 forKey:@"1"];
                [modes setObject:mode2 forKey:@"2"];
                [modes setObject:mode3 forKey:@"3"];
                [modes setObject:mode4 forKey:@"4"];
                [modes setObject:mode5 forKey:@"5"];
                
                [_schedule setObject:modes forKey:@"modes"];
                
                [_schedule removeObjectForKey:@"schedule"];
            }
        }
        [_schedule setObject:@"rgb" forKey:@"type"];
        
        _rgb = YES;

        [_tableView reloadData];
        
    } else if([type isEqualToString:@"white"]){
        _dimming = _rgb = _switches = _shutters = NO;
  
        if(_schedule == nil){
            _schedule = [[NSMutableDictionary alloc] init];
            
            int modeValue2 = 20;
            int modeValue3 = 50;
            int modeValue4 = 70;
            int modeValue5 = 100;
            
            NSDictionary *mode0 = [[NSDictionary alloc] initWithObjectsAndKeys:@(0),@"brightness", @(10),@"white", nil];
            NSDictionary *mode2 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue2),@"brightness",@(10),@"white", nil];
            NSDictionary *mode3 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue3),@"brightness",@(10),@"white", nil];
            NSDictionary *mode4 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue4),@"brightness",@(10),@"white", nil];
            NSDictionary *mode5 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue5),@"brightness",@(10),@"white", nil];
            
            NSMutableDictionary *modes = [[NSMutableDictionary alloc] init];
            
            [modes setObject:mode0 forKey:@"1"];
            [modes setObject:mode2 forKey:@"2"];
            [modes setObject:mode3 forKey:@"3"];
            [modes setObject:mode4 forKey:@"4"];
            [modes setObject:mode5 forKey:@"5"];
            
            [_schedule setObject:modes forKey:@"modes"];
            
        } else {
            NSMutableDictionary *modes = [[NSMutableDictionary alloc] init];
            modes = [[_schedule objectForKey:@"modes"]mutableCopy];
            
            if(![oldMode isEqualToString:type]){
                
                [modes removeAllObjects];
                if(modes == nil){
                    modes = [NSMutableDictionary new];
                }
                int modeValue2 = 20;
                int modeValue3 = 50;
                int modeValue4 = 70;
                int modeValue5 = 100;
                
                NSDictionary *mode0 = [[NSDictionary alloc] initWithObjectsAndKeys:@(0),@"brightness", @(10),@"white", nil];
                NSDictionary *mode2 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue2),@"brightness",@(10),@"white", nil];
                NSDictionary *mode3 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue3),@"brightness",@(10),@"white", nil];
                NSDictionary *mode4 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue4),@"brightness",@(10),@"white", nil];
                NSDictionary *mode5 = [[NSDictionary alloc] initWithObjectsAndKeys:@(modeValue5),@"brightness",@(10),@"white", nil];
                
                [modes setObject:mode0 forKey:@"1"];
                [modes setObject:mode2 forKey:@"2"];
                [modes setObject:mode3 forKey:@"3"];
                [modes setObject:mode4 forKey:@"4"];
                [modes setObject:mode5 forKey:@"5"];
                
                [_schedule setObject:modes forKey:@"modes"];
                
                [_schedule removeObjectForKey:@"schedule"];
            }
        }

        [_schedule setObject:@"white" forKey:@"type"];
        _whiteLED = YES;
        [_tableView reloadData];
    } else {
        _shutters = _switches = _dimming =_rgb = _whiteLED = NO;
        [_tableView reloadData];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)hysteresisButtonTap:(id)sender {
    if(!selectedElan){
        [self showAlert:NSLocalizedString(@"selectElan", nil)];
        return;
    }
    NSArray*tempIDArray = [_schedule objectForKey:@"devices"];
    NSLog(@"%@",tempIDArray);
    _selectedDeviceViewController = [[SelectedDeviceViewController alloc] initWithDelegate:self devices:tempIDArray elan:selectedElan withSchedule:_mySchedule];

    _selectedDeviceViewController.view.tag= 105;
    UIView * superView = [self.view superview];
    [_selectedDeviceViewController.view setFrame:superView.frame];
    [[_selectedDeviceViewController.view viewWithTag:1] setCenter:_selectedDeviceViewController.view.center];
    if ([superView viewWithTag:105]==nil){
        [UIView transitionWithView:superView
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve //any animation
                        animations:^ { [superView addSubview:_selectedDeviceViewController.view]; }
                        completion:nil];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    myScheduleName = textField.text;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [_schedule setObject:myScheduleName forKey:@"label"];

    NSLog(@"%@", _schedule);
    
    ((OneWeekDeviceScheduleViewController *)segue.destinationViewController).schedule = _schedule;
}


-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

- (IBAction)nextTapped:(id)sender {
    if ([myScheduleName isEqualToString:@""] || !myScheduleName){
        
        [self showAlert:NSLocalizedString(@"nameError", nil)];
        return;
        
    }

    if(_rgb || _whiteLED || _dimming || _shutters || _switches)
    [self performSegueWithIdentifier:@"time_sched" sender:sender];
}

-(NSArray*)findColors:(UIView*)myView{
    UIColor *myColor = myView.backgroundColor;
    
    CGColorRef color = [myColor CGColor];
    
    int numComponents = CGColorGetNumberOfComponents(color);
    
        const CGFloat *components = CGColorGetComponents(color);
        CGFloat red = components[0];
        CGFloat green = components[1];
        CGFloat blue = components[2];
        
        int myRed = round(red*255);
        int myGreen = round(green*255);
        int myBlue = round(blue*255);
    
        NSArray *colorArray = @[[NSNumber numberWithInt: myRed], [NSNumber numberWithInt:myGreen], [NSNumber numberWithInt:myBlue]];
    
    return colorArray;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"Tableview tag:%ld", (long)tableView.tag);
    if(tableView.tag == 2 && eLANsSelected)return [[SYCoreDataManager sharedInstance] getAllRFElans].count +1;
    if(tableView.tag == 1)return 4;
    if(tableView == modesTableview && (_rgb || _whiteLED || _dimming) && modesSelected)return 5;
    if(tableView == modesTableview && (_shutters || _switches) && modesSelected)return 3;

    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1 && indexPath.row == 1 && eLANsSelected) return [[SYCoreDataManager sharedInstance] getAllRFElans].count*50+50;
    if(tableView.tag == 1 && indexPath.row == 3 && modesSelected &&(_shutters || _switches)) return 2*50+50;
    if(tableView.tag == 1 && indexPath.row == 3 && modesSelected &&(_rgb || _dimming || _whiteLED)) return 4*50+50;
    return 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        UITableViewCell * cell = nil;
        
        switch (indexPath.row) {
            case 0:{
                NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"enterName", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField *tf = (UITextField*)[[[cell contentView] subviews] firstObject];
                tf.delegate = self;
                tf.attributedPlaceholder = namePlaceHolder;
                if(myScheduleName != nil){
                    tf.text = myScheduleName;
                }
            }
                break;
            case 1:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 2;
                elansTableView = tbv;
                tbv.delegate = self;
                tbv.dataSource = self;
            }
                break;
            case 2: {
                
                for(UIView*view in [[cell contentView] subviews]){
                    if(view.tag == 50){
                        [view removeFromSuperview];
                    }
                }
                
                cell = [tableView dequeueReusableCellWithIdentifier:@"hysteresisCell" forIndexPath:indexPath];
                UILabel *tf = (UILabel*)[[cell contentView] viewWithTag:1];
                NSArray*deviceID = [_schedule objectForKey:@"devices"];
                if(deviceID != nil){
                    tf.text  = @"";
                    for (NSString*myDeviceId in deviceID){
                        Device*myDevice =  [[SYCoreDataManager sharedInstance] getDeviceWithID:myDeviceId];
                        if(deviceID.count == 1){
                            tf.text  = myDevice.label;
                        } else {
                            tf.text  = [[tf.text stringByAppendingString:myDevice.label]stringByAppendingString: @", "];
                        }
                    }
                } else {
                    tf.text = NSLocalizedString(@"pleaseSelectDevice", nil);
                }
            }
                break;
            case 3:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 3;
                modesTableview = tbv;
                tbv.delegate = self;
                tbv.dataSource = self;
            }
                break;
                
            default:
                break;
        }
        if(!cell){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        return cell;
    }else if(tableView == elansTableView){
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        
        if(eLANsSelected){
            line.hidden = YES;
            
            if(indexPath.row>0){
                img.hidden = YES;
                label.text = ((Elan*)[[[SYCoreDataManager sharedInstance] getAllRFElans] objectAtIndex:indexPath.row-1]).label;
                if([label.text isEqualToString:selectedElan.label]){
                    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            } else {
                label.text = @"eLAN";
                
                img.hidden = NO;
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            }
        } else {
            label.text = @"eLAN";
            if(selectedElan){
                label.text = selectedElan.label;
            }
            line.hidden = NO;
            img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
        }
        if(!cell){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        return cell;
    } else if(tableView == modesTableview){
        if(indexPath.row == 0){
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
            
            
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
            
            UILabel* label = [[cell contentView] viewWithTag:1];
            UIImageView *img = [[cell contentView ] viewWithTag:2];
            UIView *line = [[cell contentView ] viewWithTag:3];
            
            label.text = NSLocalizedString(@"heat_temp_schedule_add_sub_header", nil);
            
            if(modesSelected){
                line.hidden = YES;
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            } else {
                line.hidden = NO;
                img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
            }
            return cell;
        } else {
            static NSString *CellIdentifier = @"MyCell";
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            }
            [self configureCell:cell atIndexPath:indexPath];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = USBlueColor;
            [cell setSelectedBackgroundView:bgColorView];
            
            return cell;
        }
    } else {
        UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        return cell;
    }
}

-(void)showAlert:(NSString*)string{
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Warning", nil) message:string preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction*okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    }];
    [controller addAction:okAction];
    
    [self presentViewController:controller animated:YES completion:nil];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"tableviw tag selected: %ld", (long)tableView.tag);
    if(tableView.tag == 2 && indexPath.row == 0 && !eLANsSelected){
        eLANsSelected = YES;
        modesSelected = NO;
        
        tableView.layer.borderColor = USBlueColor.CGColor;
        tableView.layer.borderWidth = 1.0f;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
        
    } else if (tableView.tag == 2 && indexPath.row == 0 && eLANsSelected){
        eLANsSelected = NO;
        
        tableView.layer.borderColor = nil;
        tableView.layer.borderWidth = 0.0f;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
    } else if(tableView.tag == 3 && indexPath.row == 0 && !modesSelected){
        if(!_rgb && !_whiteLED && !_dimming && !_shutters && !_switches){
            [self showAlert:NSLocalizedString(@"pleaseSelectDevice", nil)];
            [tableView deselectRowAtIndexPath:indexPath animated:NO];
            return;
        }

        modesSelected = YES;
        eLANsSelected = NO;
        
        tableView.layer.borderColor = USBlueColor.CGColor;
        tableView.layer.borderWidth = 1.0f;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
        
    } else if (tableView.tag == 3 && indexPath.row == 0 && modesSelected){
        modesSelected = NO;
        eLANsSelected = NO;
        
        tableView.layer.borderColor = nil;
        tableView.layer.borderWidth = 0.0f;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
    } else if(tableView.tag == 2 && indexPath.row > 0){
        selectedElan = [[[SYCoreDataManager sharedInstance] getAllRFElans] objectAtIndex:indexPath.row-1];
    } else if(tableView.tag == 3 && indexPath.row>0){
       
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        UIView *rgbView = [cell viewWithTag:2];
        UIButton *button = [cell viewWithTag:3];
       
       if(_switches || _shutters){
           [tableView deselectRowAtIndexPath:indexPath animated:NO];
       return;
       }
       
       if(_rgb){
           NSArray *colorArray = [self findColors:rgbView];
       
           int myRed = [colorArray[0] intValue];
           int myGreen = [colorArray[1] intValue];
           int myBlue = [colorArray[2] intValue];
       
           int intensity = [button.titleLabel.text intValue];
       
           _RGBViewController = [[RGBPlanViewController alloc] initWithDelegate:self red:myRed green:myGreen blue:myBlue mode:indexPath.row intensity:intensity];
       
           _RGBViewController.view.tag= 105;
           UIView * superView = [[self.view superview] superview];
           [_RGBViewController.view setFrame:superView.frame];
           [[_RGBViewController.view viewWithTag:1] setCenter:_RGBViewController.view.center];
      
           if ([superView viewWithTag:105]==nil){
                [UIView transitionWithView:superView
                                  duration:0.3
                                   options:UIViewAnimationOptionTransitionCrossDissolve //any animation
                                animations:^ { [superView addSubview:_RGBViewController.view]; }
                                completion:nil];
           }
       } else if(_whiteLED){
           NSArray *colorArray = [self findColors:rgbView];
       
           int myRed = [colorArray[0] intValue];
           int myGreen = [colorArray[1] intValue];
           int myBlue = [colorArray[2] intValue];
           int intensity = [button.titleLabel.text intValue];
       
           _LedWhiteController = [[LedWhitePlanViewController alloc] initWithDelegate:self value:intensity mode:indexPath.row red:myRed green:myGreen blue:myBlue intensity:intensity];
       
           _LedWhiteController.view.tag= 105;
           UIView * superView = [self.view superview];
           [_LedWhiteController.view setFrame:superView.frame];
           [[_LedWhiteController.view viewWithTag:1] setCenter:_LedWhiteController.view.center];
     
           if ([superView viewWithTag:105]==nil){
               [UIView transitionWithView:superView
                                 duration:0.3
                                  options:UIViewAnimationOptionTransitionCrossDissolve //any animation
                               animations:^ { [superView addSubview:_LedWhiteController.view]; }
                               completion:nil];
           }
       } else if (_dimming){
       
           if(indexPath.row == 4)
           return;
       
           float valuefrombtn = [button.titleLabel.text floatValue];
       
           _brightnessViewController = [[IntensityConfigurationViewController alloc] initWithDelegate:self value:valuefrombtn mode:indexPath.row];
       
           _brightnessViewController.view.tag= 105;
           UIView * superView = [self.view superview];
           [_brightnessViewController.view setFrame:superView.frame];
           [[_brightnessViewController.view viewWithTag:1] setCenter:_brightnessViewController.view.center];
   
           if ([superView viewWithTag:105]==nil){
               [UIView transitionWithView:superView
                                 duration:0.3
                                  options:UIViewAnimationOptionTransitionCrossDissolve //any animation
                               animations:^ { [superView addSubview:_brightnessViewController.view]; }
                               completion:nil];
           }
       }
}
    [elansTableView reloadData];
    [modesTableview reloadData];
    [self setTBVBorders];
}

-(void)setTBVBorders{
    modesTableview.layer.borderColor = elansTableView.layer.borderColor = nil;
    modesTableview.layer.borderWidth = elansTableView.layer.borderWidth = 0;
    
    if(eLANsSelected){
        elansTableView.layer.borderWidth = 1.0f;
        elansTableView.layer.borderColor = USBlueColor.CGColor;
    }
    
    if(modesSelected){
        modesTableview.layer.borderWidth = 1.0f;
        modesTableview.layer.borderColor = USBlueColor.CGColor;
    }
    
    
    if(eLANsSelected){
        _tableViewHeight.constant = (4*50)+ ([[[SYCoreDataManager sharedInstance] getAllRFElans] count]*50);
    } else if (modesSelected){
        if(_rgb || _whiteLED || _dimming){
            _tableViewHeight.constant = 4*50+4*50;
        }
        
        if(_switches || _shutters){
            _tableViewHeight.constant = 4*50 + 2*50;
        }
    } else {
        _tableViewHeight.constant = 4*50;

    }
}

-(void)initColorArray{
    UIView *myView = [[UIView alloc] initWithFrame: CGRectMake(0,0, 20, 20)];
    EFCircularSlider *mySlider = [[EFCircularSlider alloc] init];
    [mySlider setArcStartAngle:130];
    [mySlider setArcAngleLength:280];
    [mySlider setHandleType:CircularSliderHandleTypeCircleCustom];
    
    EFGradientGenerator *gradient = [[EFGradientGenerator alloc] initWithSize:myView.frame.size];
    [mySlider setUnfilledLineInnerImage:[gradient renderedImage]];
    
    [gradient setRadius:80];
    [gradient setSectors:360];
    
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:1];
    [gradient addNewColor:[UIColor colorWithRed:208.0/255.0 green:85.0/255.0 blue:32.0/255.0 alpha:1] atSector:40];
    [gradient addNewColor:[UIColor colorWithRed:240.0/255.0 green:163.0/255.0 blue:52.0/255.0 alpha:1] atSector:90];
    [gradient addNewColor:[UIColor colorWithRed:231.0/255.0 green:208.0/255.0 blue:69.0/255.0 alpha:1] atSector:180];
    [gradient addNewColor:[UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:210.0/255.0 alpha:1] atSector:270];
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:310];
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:360];
    
    [gradient renderImage];
    
    for(int i = 0; i<100;i++){
        [mySlider setCurrentArcValue:(100 - i) forStartAnglePadding:2 endAnglePadding:2];
        UIColor *myColor = [gradient getColorFromAngleFromNorth:([mySlider angleFromNorth])];
        CGColorRef color = [myColor CGColor];
        
        if(myColor == nil){
            myColor = [UIColor whiteColor];
        [_colorArray addObject:myColor];
            NSLog(@"NIL color");
         
        } else {
        
        const CGFloat *components = CGColorGetComponents(color);
        CGFloat red = components[0];
        CGFloat green = components[1];
        CGFloat blue = components[2];
        
        int myRed = round(red*255);
        int myGreen = round(green*255);
        int myBlue = round(blue*255);
        
        gradient = nil;
        myView = nil;
        mySlider = nil;
        
        NSLog(@"R: %d, G: %d, B: %d",myRed,myGreen,myBlue);
        }
    }
}

-(UIColor*)determineColorForWhiteLED:(int)value{
    NSLog(@"value: %d", value);
    

    return [UIColor whiteColor];
}

-(void)configureCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath{
    UILabel*functionNameLabel = [cell viewWithTag:1];
    UIView *rgbView = [cell viewWithTag:2];
    UIButton *button = [cell viewWithTag:3];
    
    if(_dimming){
        
        NSDictionary *modes = [_schedule objectForKey:@"modes"];
        
        int modeValue2 = 20;
        int modeValue3 = 50;
        int modeValue4 = 70;
        int modeValue5 = 100;
        
        if([modes objectForKey:@"5"] != nil){
            NSDictionary *mode5 = [modes objectForKey:@"5"];
            NSDictionary *mode4 = [modes objectForKey:@"4"];
            NSDictionary *mode3 = [modes objectForKey:@"3"];
            NSDictionary *mode2 = [modes objectForKey:@"2"];
            
            modeValue2 = [[mode2 objectForKey:@"value"] intValue];
            modeValue3 = [[mode3 objectForKey:@"value"] intValue];
            modeValue4 = [[mode4 objectForKey:@"value"] intValue];
            modeValue5 = [[mode5 objectForKey:@"value"] intValue];
        }
        
        switch (indexPath.row) {
            case 1:
            functionNameLabel.text = NSLocalizedString(@"device_mode_1", "");
            [button setTitle: [NSString stringWithFormat:@"%d",modeValue2] forState:UIControlStateNormal];
                break;
            case 2:
                functionNameLabel.text = NSLocalizedString(@"device_mode_2", "");
                [button setTitle: [NSString stringWithFormat:@"%d",modeValue3] forState:UIControlStateNormal];
                break;
            case 3:
                functionNameLabel.text = NSLocalizedString(@"device_mode_3", "");
                [button setTitle: [NSString stringWithFormat:@"%d",modeValue4] forState:UIControlStateNormal];
                
                break;
            case 4:
                functionNameLabel.text = NSLocalizedString(@"device_mode_on", "");
                [button setTitle: [NSString stringWithFormat:@"%d",modeValue5] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
        
        rgbView.hidden = YES;
        button.hidden = NO;
    }
    
    if(_rgb){
        switch (indexPath.row) {
            case 1:
                functionNameLabel.text = NSLocalizedString(@"device_mode_1", "");
                [button setTitle: [NSString stringWithFormat:@"%d",20] forState:UIControlStateNormal];
                break;
            case 2:
                functionNameLabel.text = NSLocalizedString(@"device_mode_2", "");
                [button setTitle: [NSString stringWithFormat:@"%d",50] forState:UIControlStateNormal];
                break;
            case 3:
                functionNameLabel.text = NSLocalizedString(@"device_mode_3", "");
                [button setTitle: [NSString stringWithFormat:@"%d",70] forState:UIControlStateNormal];
                break;
            case 4:
                functionNameLabel.text = NSLocalizedString(@"device_mode_4", "");
                [button setTitle: [NSString stringWithFormat:@"%d",100] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
        
        NSDictionary *modes = [_schedule objectForKey:@"modes"];
        
        if([modes objectForKey:@"4"] != nil){
            NSDictionary *modes = [_schedule objectForKey:@"modes"];
            NSArray *modeArray;
            int brightness;
            int R;
            int G;
            int B;
            UIColor *myColor;
            switch(indexPath.row){
                case 1:
                    modeArray = [modes objectForKey:@"2"];
                    R = [[modeArray valueForKey:@"R"] intValue];
                    G = [[modeArray valueForKey:@"G"] intValue];
                    B = [[modeArray valueForKey:@"B"] intValue];
                    brightness = [[modeArray valueForKey:@"brightness"] intValue];
                    [button setTitle:[NSString stringWithFormat:@"%d", brightness] forState:UIControlStateNormal];
                    myColor = [UIColor colorWithRed:R/255.0f green:G/255.0f blue:B/255.0f alpha:1];
                    rgbView.backgroundColor = myColor;
                    break;
                case 2:
                    modeArray = [modes objectForKey:@"3"];
                    R = [[modeArray valueForKey:@"R"] intValue];
                    G = [[modeArray valueForKey:@"G"] intValue];
                    B = [[modeArray valueForKey:@"B"] intValue];
                    brightness = [[modeArray valueForKey:@"brightness"] intValue];
                    [button setTitle:[NSString stringWithFormat:@"%d", brightness] forState:UIControlStateNormal];
                    myColor = [UIColor colorWithRed:R/255.0f green:G/255.0f blue:B/255.0f alpha:1];
                    rgbView.backgroundColor = myColor;
                    break;
                case 3:
                    modeArray = [modes objectForKey:@"4"];
                    R = [[modeArray valueForKey:@"R"] intValue];
                    G = [[modeArray valueForKey:@"G"] intValue];
                    B = [[modeArray valueForKey:@"B"] intValue];
                    brightness = [[modeArray valueForKey:@"brightness"] intValue];
                    [button setTitle:[NSString stringWithFormat:@"%d", brightness] forState:UIControlStateNormal];
                    myColor = [UIColor colorWithRed:R/255.0f green:G/255.0f blue:B/255.0f alpha:1];
                    rgbView.backgroundColor = myColor;
                    break;
                case 4:
                    modeArray = [modes objectForKey:@"5"];
                    R = [[modeArray valueForKey:@"R"] intValue];
                    G = [[modeArray valueForKey:@"G"] intValue];
                    B = [[modeArray valueForKey:@"B"] intValue];
                    brightness = [[modeArray valueForKey:@"brightness"] intValue];
                    [button setTitle:[NSString stringWithFormat:@"%d", brightness] forState:UIControlStateNormal];
                    myColor = [UIColor colorWithRed:R/255.0f green:G/255.0f blue:B/255.0f alpha:1];
                    rgbView.backgroundColor = myColor;
                    break;
                default:
                    break;
            }
        }
        rgbView.hidden = NO;
        button.hidden = NO;
    }
    
    if(_whiteLED){
        //comment for Mikoláš
        //Tady se konfigurují řádky pro jednotlivé nastavené hodnoty. Už jsem tam něco tehdy zkoušel ale strašně na rychlo, nedělalo to co sem potřeboval. Grafiky ve storyboardu si moc nevšímej, je dělaná na 5s a budu ju muset trochu předělat.
        
        rgbView.layer.borderWidth = 2.0f;
        rgbView.layer.borderColor = [UIColor blackColor].CGColor;
        
        switch (indexPath.row) {
            case 1:
                functionNameLabel.text = NSLocalizedString(@"device_mode_1", "");
                [button setTitle: [NSString stringWithFormat:@"%d",20] forState:UIControlStateNormal];
                break;
            case 2:
                functionNameLabel.text = NSLocalizedString(@"device_mode_2", "");
                [button setTitle: [NSString stringWithFormat:@"%d",50] forState:UIControlStateNormal];
                break;
            case 3:
                functionNameLabel.text = NSLocalizedString(@"device_mode_3", "");
                [button setTitle: [NSString stringWithFormat:@"%d",70] forState:UIControlStateNormal];
                break;
            case 4:
                functionNameLabel.text = NSLocalizedString(@"device_mode_4", "");
                [button setTitle: [NSString stringWithFormat:@"%d",100] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
        
        NSDictionary *modes = [_schedule objectForKey:@"modes"];
        int value = 245;
        
        if([modes objectForKey:@"4"] != nil){
            int brightness;
            NSArray *modeArray;
            for(int i = 2;i<6; i++){
                switch (i) {
                    case 2:
                        modeArray = [modes objectForKey:@"2"];
                        brightness = [[modeArray valueForKey:@"brightness"] intValue];
                        value = [[modeArray valueForKey:@"white"]intValue];
                        
                        if(indexPath.row == 1){
                            [button setTitle:[NSString stringWithFormat:@"%d", brightness] forState:UIControlStateNormal];
                            rgbView.backgroundColor = [self determineColorForWhiteLED:value];
                        }
                        
                        
                        break;
                    case 3:
                        modeArray = [modes objectForKey:@"3"];
                        brightness = [[modeArray valueForKey:@"brightness"] intValue];
                        value = [[modeArray valueForKey:@"white"]intValue];
                        
                        if(indexPath.row == 2){
                            [button setTitle:[NSString stringWithFormat:@"%d", brightness] forState:UIControlStateNormal];
                            rgbView.backgroundColor = [self determineColorForWhiteLED:value];
                        }
                        
                        
                        break;
                    case 4:
                        modeArray = [modes objectForKey:@"4"];
                        brightness = [[modeArray valueForKey:@"brightness"] intValue];
                        value = [[modeArray valueForKey:@"white"]intValue];
                        
                        if(indexPath.row == 3){
                            [button setTitle:[NSString stringWithFormat:@"%d", brightness] forState:UIControlStateNormal];
                            rgbView.backgroundColor = [self determineColorForWhiteLED:value];
                        }
                        
                        
                        break;
                    case 5:
                        modeArray = [modes objectForKey:@"5"];
                        brightness = [[modeArray valueForKey:@"brightness"] intValue];
                        value = [[modeArray valueForKey:@"white"]intValue];
                        
                        if(indexPath.row == 4){
                            [button setTitle:[NSString stringWithFormat:@"%d", brightness] forState:UIControlStateNormal];
                            rgbView.backgroundColor = [self determineColorForWhiteLED:value];
                        }
                        break;
                    default:
                        break;
                }
            }
        }
        rgbView.hidden = NO;
        button.hidden = NO;
    }
    
    if(_switches){
        rgbView.hidden = YES;
        button.hidden = YES;
        switch (indexPath.row) {
            case 1:
                [functionNameLabel setText:NSLocalizedString(@"device_mode_on", "")];
                break;
            case 2:
                [functionNameLabel setText:NSLocalizedString(@"device_mode_off", "")];

                break;
            default:
                break;
        }
    }
    
    if(_shutters){
        rgbView.hidden = YES;
        button.hidden = YES;
        switch (indexPath.row) {
            case 0:
                [functionNameLabel setText:NSLocalizedString(@"device_mode_up", "")];
                break;
            case 1:
                [functionNameLabel setText:NSLocalizedString(@"device_mode_down", "")];
                break;
                default:
                break;
        }
    }
}

-(void)setWhite:(int)value red:(int)cRed green:(int)cGreen blue:(int)cBlue mode:(NSInteger)mode intensity:(int)intensity{
    NSLog(@"red: %d green: %d blue: %d mode: %zd", cRed,cGreen,cBlue,mode);
    
    NSMutableDictionary *modes = [[_schedule objectForKey:@"modes"] mutableCopy];
    NSDictionary *modeDict = [[NSDictionary alloc] initWithObjectsAndKeys:@(intensity),@"brightness", [NSNumber numberWithInt:value] ,@"white", @(cRed),@"R", @(cGreen), @"G", @(cBlue), @"B",  nil];
    
    int myMode = (int)mode +1;
    [modes setObject:modeDict forKey:[NSString stringWithFormat:@"%d",myMode]];
    [_schedule setObject:modes forKey:@"modes"];
    
    [modesTableview reloadData];
}

-(void)setRGB:(int)cRed green:(int)cGreen blue:(int)cBlue mode:(NSInteger)mode intensity:(int)intensity{
    NSLog(@"red: %d green: %d blue: %d mode: %zd intensity: %d", cRed,cGreen,cBlue,mode,intensity);
    
    NSMutableDictionary *modes = [[_schedule objectForKey:@"modes"] mutableCopy];
    NSDictionary *modeDict = [[NSDictionary alloc] initWithObjectsAndKeys:@(intensity),@"brightness", @(cRed),@"R", @(cGreen), @"G", @(cBlue), @"B",  nil];
    
    int myMode = (int)mode +1;
    [modes setObject:modeDict forKey:[NSString stringWithFormat:@"%d",myMode]];
    [_schedule setObject:modes forKey:@"modes"];
    
    [modesTableview reloadData];
    
}

-(void)setBrightnessValue:(int)value mode:(NSInteger)mode{
    NSLog(@"brightness: %d mode: %zd", value,mode);
    
    NSMutableDictionary *modes = [[_schedule objectForKey:@"modes"] mutableCopy];
    
    int myMode = (int)mode +1;
    
    NSDictionary *modeDict = [[NSDictionary alloc] initWithObjectsAndKeys:@(value),@"value", nil];
    [modes setObject:modeDict forKey:[NSString stringWithFormat:@"%d",myMode]];
    [_schedule setObject:modes forKey:@"modes"];
    
    [modesTableview reloadData];
}


-(void)deviceTypeSet:(NSString *)type deviceID:(NSArray *)deviceID{
    
    if(![[_schedule objectForKey:@"type"] isEqualToString:type]){
        [_schedule removeAllObjects]; }
    
    [self initGUIwithType:type];
    NSLog(@"%@",deviceID);
    
    [_schedule setObject:deviceID forKey:@"devices"];
    
    [_tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:2 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    NSArray*tempIDArray = [_schedule objectForKey:@"devices"];
    NSLog(@"%@",tempIDArray);
}


@end
