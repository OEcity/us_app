//
//  AddDeviceModeViewController.h
//  iHC-MIRF
//
//  Created by admin on 15.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TempDayMode.h"

#import "AddModeNoDetailTVCell.h"
#import "AddModeTemperatureDetailTVCell.h"
#import "AddModeColorDetailTVCell.h"


@protocol AddDeviceModeDelegate

-(void)addDeviceModeToday:(NSInteger)day comesFromLTouch:(BOOL)edit tempDayMode:(TempDayMode*)tempdayMode position:(NSInteger)position erase:(BOOL)erase;

@end




@interface AddDeviceModeViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) id<AddDeviceModeDelegate> delegate;

@property NSInteger modePos;
@property NSInteger day;

@property (weak, nonatomic) IBOutlet UIPickerView *dayInWeekPicker;
@property (weak, nonatomic) IBOutlet UIView *dayInWeekPickerView;


@property (weak, nonatomic) IBOutlet UIView *timePickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *timePicker;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *odLabel;

@property (weak, nonatomic) IBOutlet UILabel *doLabel;
@property BOOL odCausedOpen;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightTableView;


@property (strong, nonatomic) NSArray *daysInWeekForPicker;

@property (strong, nonatomic) NSArray *temperatures;

@property BOOL shouldShowColor;
@property BOOL shouldShowZero;

@property (strong, nonatomic) NSArray *colors;

@property (strong, nonatomic) NSNumberFormatter *formatter;


@property  NSInteger timeTo;
@property  NSInteger timeFrom;
@property  NSInteger mode;

@property BOOL selectedDay;
@property BOOL selectedFrom;
@property BOOL selectedTill;
@property BOOL selectedMode;


-(void)prepareAndSendDataToDelegate;


@end
