//
//  SYSceneActionsViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/2/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
#import "URLConnectionListener.h"
#import "SYAddSceneViewController.h"
#import "SYListScenesViewController.h"
#import "Scene.h"
#import "SceneAction.h"
#import "ActionSelectViewController.h"
#import "HUDWrapper.h"
#import "SYCoreDataManager.h"
#import "iCarousel.h"

@interface SYSceneActionsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIGestureRecognizerDelegate, NSFetchedResultsControllerDelegate, iCarouselDataSource, iCarouselDelegate, UIAlertViewDelegate>


@property (nonatomic, retain) IBOutlet UISlider * slider;
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) NSIndexPath *selectedIndexPath;
@property (nonatomic, retain) NSMutableArray *selectedDevices;
@property (nonatomic, retain) Device * selectedDevice;
@property (nonatomic,retain) IBOutlet UILabel *deviceLabelTemplate;
@property (nonatomic,retain) IBOutlet UIView *deviceBackgroundTemplate;
@property (nonatomic,retain) IBOutlet UIImageView *deviceIconTemplate;
@property (nonatomic,retain) Scene* scene;
@property (nonatomic) CGPoint startPos;
@property (nonatomic, retain) NSMutableArray *deviceViews;
@property (nonatomic, retain) NSMutableArray *cells;
@property (nonatomic, retain) UISwipeGestureRecognizer *swipeGesture;
@property (nonatomic, retain) NSMutableDictionary *deviceActions;
@property (nonatomic, retain) NSMutableDictionary *selectedActions;
@property (nonatomic, retain) NSMutableDictionary *cellsForDevice;
@property (nonatomic, retain) NSMutableArray *predefinedActions;


@property (nonatomic, retain) NSString *sceneLabel;
@property (nonatomic, retain) SYListScenesViewController *sourceController;
@property (nonatomic, retain) ActionSelectViewController *actionSelectViewController;
@property (nonatomic, retain) HUDWrapper *loaderDialog;
@property (weak, nonatomic) IBOutlet UIButton *addActionButton;

@property (weak, nonatomic) IBOutlet UILabel *actionSetLabel;
@property (weak, nonatomic) IBOutlet UIButton *bottomRightButton;
@property (weak, nonatomic) IBOutlet UIButton *bottomLeftButton;
@property (weak, nonatomic) IBOutlet UIButton *addFunctionButton;

@property (weak, nonatomic) IBOutlet iCarousel *carousel;

- (IBAction)addFunction:(id)sender;
-(void)longPressEdit:(UILongPressGestureRecognizer *)gestureRecognizer;
@end
