//
//  SYCentralSourceViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/10/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "SYCentralSourceViewController.h"
#import "IconWithLabelTableViewCell.h"
#import "SYAPIManager.h"
#import "SYCoreDataManager.h"
@interface SYCentralSourceViewController ()

@end

@implementation SYCentralSourceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    [[SYAPIManager sharedInstance] getCentralSourceWithSuccess:^(AFHTTPRequestOperation *request, id response){
        _sources = [[NSMutableArray alloc] init];
        for (NSString * scheduleName in (NSArray *)response){
            [[SYAPIManager sharedInstance] getTemperatureSourceDetailsWithName:scheduleName success:
             ^(AFHTTPRequestOperation *request, id response){
                 [_sources addObject:(NSDictionary*)response];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [_tableView reloadData];
                 });
                 
             }
             failure:^(AFHTTPRequestOperation * request, NSError * error){
                 [_loaderDialog hide];
             }];
            
        }
        if ([(NSArray *)response count]==0){

            CGFloat screenHeight = self.view.frame.size.height;
            UILabel * lNoContent = [[UILabel alloc] initWithFrame:CGRectMake(0, screenHeight/2  - 10, 320, 20)];
            lNoContent.textAlignment = NSTextAlignmentCenter;
            [lNoContent setTextColor:[UIColor blackColor]];
            [lNoContent setText:NSLocalizedString(@"heat_source_settings_empty_list", nil)];
            [self.view addSubview:lNoContent];
            
        }
        [_loaderDialog hide];
        
    }
    failure:^(AFHTTPRequestOperation * request, NSError * error){
       [_loaderDialog hide];
    }];
    [_btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_btnAdd setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];


}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)dismissController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_sources count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 72.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellIdentifier = @"sourcesCell";
    IconWithLabelTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[IconWithLabelTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSDictionary * schedule = [_sources objectAtIndex:indexPath.row];
    
    [cell.lLabel setText:[schedule objectForKey:@"label"]];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [_container setCentralSourceItem:[_sources objectAtIndex:indexPath.row]];
    [_container setViewController:@"centralSourceDetail"];

    
}
#pragma mark DELETE table rows section
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return YES;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSDictionary * source = [_sources objectAtIndex:indexPath.row];

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_centralSource", nil) ,[source valueForKey:@"label"]]delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
        alert.tag = indexPath.row;
        [alert show];

    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
        NSDictionary * source = [_sources objectAtIndex:indexPath.row];
        NSMutableArray * mutableDevices = [[NSMutableArray alloc] initWithArray:_sources];
        [mutableDevices removeObjectAtIndex:indexPath.row];
        _sources = mutableDevices;
        [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
        [[SYAPIManager sharedInstance] deleteCentralSourceWithId:[source objectForKey:@"id"] success:^(AFHTTPRequestOperation * request, id object){
            
        } failure:^(AFHTTPRequestOperation * request, NSError * error){
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                              message:NSLocalizedString(@"heat_source_failed_delete", nil)
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];
            
        }];
    }
}


- (IBAction)addButtonTap:(id)sender {
    
    
    

    if ([[[SYCoreDataManager sharedInstance] getAllHeatCoolAreas] count]==0){
        
        
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"add_heat_source_error", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        
    }else{
        [_container setCentralSourceItem:nil];
        [_container setViewController:@"centralSourceDetail"];
    }



}

@end
