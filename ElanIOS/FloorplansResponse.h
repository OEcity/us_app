//
//  FloorplansResponse.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/21/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"
@interface FloorplansResponse : NSObject <Response>

@end
