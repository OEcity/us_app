//
//  SceneBlindsViewController.m
//  Click Smart
//
//  Created by Tom Odler on 08.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "SceneBlindsViewController.h"

@interface SceneBlindsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *deviceNameLabel;
@property (nonatomic) NSString *actionName;
@property (nonatomic) NSMutableDictionary *dict;

@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *upButton;
@property (weak, nonatomic) IBOutlet UIButton *downButton;

@end

@implementation SceneBlindsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _dict = [[NSMutableDictionary alloc] init];
    _deviceNameLabel.text = _device.label;
    [self loadParametersFromArray];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadParametersFromArray{
    NSArray *mySceneArray = nil;
    
    if(_controller){
        mySceneArray = _controller.actionsArray;
    } else {
        mySceneArray = _guideController.actionsArray;
    }
    
    NSInteger index = 0;
    NSMutableIndexSet *indexForDelete = [[NSMutableIndexSet alloc]init];
    for(NSDictionary*dict in mySceneArray){
        NSString *deviceID =[[dict allKeys]firstObject];
        if([deviceID isEqualToString:_device.deviceID]){
            NSDictionary *myActionsDict = [dict objectForKey:deviceID];
            if([[[myActionsDict allKeys]firstObject] isEqualToString:@"stop"]){
                _stopButton.selected = YES;
                _actionName = @"stop";
            }
            
            if([[[myActionsDict allKeys]firstObject] isEqualToString:@"roll up"]){
                _upButton.selected = YES;
                _actionName = @"roll up";
            }
            
            if([[[myActionsDict allKeys]firstObject] isEqualToString:@"roll down"]){
                _downButton.selected = YES;
                _actionName = @"roll down";
            }
            _actionName = [[myActionsDict allKeys]firstObject];
            [indexForDelete addIndex:index];
            [_dict setObject:[[NSNull alloc]init]  forKey:_actionName];
        }
        index++;
    }
    
    if ([indexForDelete count] > 0){
        if(_controller)
            [_controller.actionsArray removeObjectsAtIndexes:indexForDelete];
        else
            [_guideController.actionsArray removeObjectsAtIndexes:indexForDelete];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)setAction:(id)sender {
    UIButton *myButon = (UIButton*)sender;
    
    _stopButton.selected = NO;
    _upButton.selected = NO;
    _downButton.selected = NO;
    
    switch (myButon.tag) {
        case 1:
            _actionName = @"stop";
            myButon.selected = YES;
            break;
        case 2:
            _actionName = @"roll up";
            myButon.selected = YES;
            break;
        case 3:
            _actionName = @"roll down";
            myButon.selected = YES;
            break;
        default:
            break;
    }
//    [_dict setValue:[[NSNull alloc]init] forKey:_actionName];
    [_dict removeAllObjects];
    [_dict setObject:[[NSNull alloc]init]  forKey:_actionName];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    NSMutableDictionary *deviceWithActionDictionary = [[NSMutableDictionary alloc]init];
    [deviceWithActionDictionary setObject:_dict forKey:_device.deviceID];
    if(_controller){
        [[_controller actionsArray] addObject:deviceWithActionDictionary];
        [[self.controller selectedDevices] setObject:_device forKey:_device.deviceID];
    } else {
        [[_guideController actionsArray] addObject:deviceWithActionDictionary];
        [[self.guideController selectedDevices] setObject:_device forKey:_device.deviceID];
    }

}

@end
