//
//  NSManagedObject+NSString.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 21/02/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "NSManagedObject+NSString.h"

@implementation NSManagedObject (NSString)

-(void)setStringValue:(id)value forKey:(NSString*)key{
    if ([value isKindOfClass:[NSString class]]){
        value = [[NSNumber alloc]initWithInt:((NSString*)value).intValue];
    }
    [self setValue:value forKey:key];
}

@end
