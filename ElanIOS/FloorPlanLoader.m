//
//  FloorPlanLoader.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/21/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "FloorPlanLoader.h"
#import "FloorplansResponse.h"
#import "FloorPlanDetailResponse.h"
#import "AppDelegate.h"
#import "Util.h"
@implementation FloorPlanLoader


-(void)LoadFloorplans{
    self.floorplans = [[NSMutableArray alloc] init];
    self.url = [[URLConnector alloc] initWithAddressAndLoader:[Util getConfigUrl:@"floorplans"] activityInd:nil];
    FloorplansResponse * scenesResponse = [[FloorplansResponse alloc] init];
    [self.url setParser:scenesResponse];
    [self.url setConnectionListener:self];
    [self.url ConnectAndGetData];

}


NSInteger floorSize=0;
int floorCouter=0;

-(void)connectionResponseOK:(NSObject *)returnedObject response:(id<Response>)responseType{

    if ([responseType isKindOfClass:[FloorplansResponse class]] ){
        self.floorplans = (NSMutableArray*) returnedObject;
        //        [self saveRooms:rooms];
        //       [collections reloadData];
        floorSize=[self.floorplans count];
        floorCouter=0;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *theInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.floorplans,@"floorplans", nil];

            [[NSNotificationCenter defaultCenter] postNotificationName:@"floorplanLoaderComplete" object:self userInfo:theInfo];
        });
    }
        /*
        for (Floorplan*floorplan in self.floorplans){
            URLConnector *url = [[URLConnector alloc] initWithAddressAndLoader:floorplan.url activityInd:nil];

            FloorPlanDetailResponse * itemsResponse = [[FloorPlanDetailResponse alloc] init];
            [itemsResponse setFloorPlan:floorplan];
            [url setParser:itemsResponse];
            [url setConnectionListener:self];
            url.name = floorplan.url;
            [url ConnectAndGetData];
        }
    }else if ([responseType isKindOfClass:[FloorPlanDetailResponse class]] ){
        Floorplan * floorplan = (Floorplan*) returnedObject;
        for (Floorplan* floor in self.floorplans){
            if ([floor.url isEqualToString:floorplan.url]){
                floor.image=floorplan.image;
            }
        }

        
        floorCouter++;
        if (floorCouter>=floorSize){

            

        }
        
    }
         */
    
}




-(void) connectionResponseFailed:(NSInteger)code{
    NSLog(@"failed");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"floorplanLoaderComplete" object:self userInfo:nil];
    
}

@end
