//
//  ElementsTableViewCell.h
//  Click Smart
//
//  Created by Tom Odler on 24.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYConfigureDevicesViewController.h"
#import "HUDWrapper.h"

@interface ElementsTableViewCell : UITableViewCell<UITableViewDelegate, UITableViewDataSource,NSFetchedResultsControllerDelegate>
@property (nonatomic) NSMutableArray*dataArray;
@property (nonatomic) UITableView *tableView;
@property (nonatomic, retain) NSMutableArray * added;
@property (nonatomic) SYConfigureDevicesViewController*parentController;
@property (nonatomic, retain) HUDWrapper* loaderDialog;
@property (nonatomic, strong) NSFetchedResultsController* fetchedResultsController;


@end
