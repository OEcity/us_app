//
//  URLConnector.m
//  O2archiv
//
//  Created by Vratislav Zima on 1/15/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "URLConnector.h"

#import "URLConnectionListener.h"
#import "Util.h"
#import "AppDelegate.h"
#import "SYCoreDataManager.h"
@implementation URLConnector
@synthesize address;
@synthesize activityInd;
@synthesize name;
@synthesize request;
id <Response> MyResponse;
id <URLConnectionListener> listener;


-(id)initWithAddressAndLoader:(NSString *)address_ activityInd:(UIActivityIndicatorView *)activityInd_
{
    self = [super init];
    if (self) {
        self.address = address_;
        self.activityInd = activityInd_;
        self.receivedData = [[NSMutableData alloc] init];
    }
    return self;
}

-(void)ConnectAndGetData{
    //NSString *path;
    
    //NSLog(@"%@",[Util getServerAddress]);
    
    request = [[NSMutableURLRequest alloc]
                                     initWithURL:[NSURL URLWithString:address]];

    
    request.timeoutInterval=10;
    if (activityInd!=nil) [activityInd startAnimating];
/*
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
         int responseStatusCode = [httpResponse statusCode];
         if (activityInd!=nil) [activityInd stopAnimating];
         
         if ([data length] > 0 && error == nil && responseStatusCode == 200){
             [listener connectionResponseOK:[self parse:data id:name] response:MyResponse];
         }else if ([data length] > 0 && error == nil && responseStatusCode != 200){
             [listener connectionResponseFailed:responseStatusCode];
         }
         else if ([data length] == 0 && error == nil){
             [listener connectionResponseFailed:responseStatusCode];
         }
         else if (error != nil){
             [listener connectionResponseFailed:responseStatusCode];             
         }
         AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
     }];
    */

   self.connection = [NSURLConnection connectionWithRequest:request delegate:self];


}



-(void)ConnectAndPostData:(NSData*)requestData toElan:(Elan*)elan{
    NSString *path;
    NSString * serverAddress = [NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]];
    if ([address hasPrefix:serverAddress]){
        path=address;
    }else
        path = [[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]] stringByAppendingString:address];
    request = [[NSMutableURLRequest alloc]
                                     initWithURL:[NSURL URLWithString:path]];
    NSLog(@"path:  %@", path);
    if (self.isPost==NO){
        [request setHTTPMethod: @"PUT"];
    }else{
        [request setHTTPMethod: @"POST"];
    }
    if (self.ContentType==nil)[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    else{
        [request setValue:self.ContentType forHTTPHeaderField:@"content-type"];
    }
    [request setHTTPBody: requestData];
    request.timeoutInterval=15;

    if (activityInd!=nil) [activityInd startAnimating];
   
   // [app.myWS closeWS];
/*
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        // [app.myWS reconnect];

         NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
         int responseStatusCode = [httpResponse statusCode];
         if (activityInd!=nil) [activityInd stopAnimating];
         
         if ( error == nil && (responseStatusCode == 200 ||  responseStatusCode == 201 || responseStatusCode==202)){
             if (data!=nil){
                 [listener connectionResponseOK:[self parse:data id:name] response:MyResponse];
             }else
             [listener connectionResponseOK:nil response:nil];
         }else if ([data length] > 0 && error == nil && responseStatusCode != 200){
             NSLog(@" erro: %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
             [listener connectionResponseFailed:responseStatusCode];
         }
         else if ([data length] == 0 && error == nil){
             [listener connectionResponseFailed:responseStatusCode];
             
         }
         else if (error != nil){
             [listener connectionResponseFailed:responseStatusCode];
             
             
         }

     }];
 */
       self.connection = [NSURLConnection connectionWithRequest:request delegate:self];



}


-(void)deleteItem{
    NSString *path;
    if ([address hasPrefix:[Util getServerAddress]]){
        path=address;
    }else
        path = [[Util getServerAddress] stringByAppendingString:address];
    request = [[NSMutableURLRequest alloc]
               initWithURL:[NSURL URLWithString:path]];

        [request setHTTPMethod: @"DELETE"];
    request.timeoutInterval=15;

    if (activityInd!=nil) [activityInd startAnimating];
   // AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    //[app.myWS closeWS];
    /*
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {

         NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
         int responseStatusCode = [httpResponse statusCode];
         if (activityInd!=nil) [activityInd stopAnimating];

         if ( error == nil && (responseStatusCode == 200 ||  responseStatusCode == 201 || responseStatusCode==202)){
             if (data!=nil){
                 [listener connectionResponseOK:[self parse:data id:name] response:MyResponse];
             }else
                 [listener connectionResponseOK:nil response:nil];
         }else if ([data length] > 0 && error == nil && responseStatusCode != 200){
             NSLog(@" erro: %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
             [listener connectionResponseFailed:responseStatusCode];
         }
         else if ([data length] == 0 && error == nil){
             [listener connectionResponseFailed:responseStatusCode];

         }
         else if (error != nil){
             [listener connectionResponseFailed:responseStatusCode];


         }
         //[app.myWS reconnect];
     }];
*/
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];


}

-(void) setParser:(id <Response>) response_{
    MyResponse=response_;
    
}

-(NSObject *) parse:(NSData*)data id:(NSString*)name1{

    return [MyResponse parseResponse:data identifier:name1];


}

-(void)setConnectionListener:(id <URLConnectionListener> )listener_{
    listener=listener_;
}
-(void)setPost{
    self.isPost=YES;
}


-(void)connection:(NSURLConnection *)connection didReceiveResponse:
(NSURLResponse *)response
{
    // Discard all previously received data.
    [self.receivedData setLength:0];
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    self.statusCode = [httpResponse statusCode];

}

-(void)connection:(NSURLConnection *)connection didReceiveData:
(NSData *)data
{
    // Append the new data to the receivedData.
    [self.receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    if (![listener respondsToSelector:@selector(connectionResponseOK:response:)]){
        return;
    }
        

       if (activityInd!=nil) [activityInd stopAnimating];
    if (MyResponse==nil && self.isPost==NO) return;
    if ( (self.statusCode == 200 ||  self.statusCode == 201 || self.statusCode==202)){
        if (self.receivedData!=nil){
            [listener connectionResponseOK:[self parse:self.receivedData id:name] response:MyResponse];
        }else
            [listener connectionResponseOK:nil response:nil];
    }else if ([self.receivedData length] > 0  && self.statusCode != 200){
        [listener connectionResponseFailed:self.statusCode];
    }
    else if ([self.receivedData length] == 0 ){
        [listener connectionResponseFailed:self.statusCode];
    }
    else {
        [listener connectionResponseFailed:self.statusCode];


    }
    self.connection=nil;


}
@end
