////
////  DeviceSchedulesTableViewCell.m
////  Click Smart
////
////  Created by Tom Odler on 25.08.16.
////  Copyright © 2016 Vratislav Zima. All rights reserved.
////
//
//#import "DeviceSchedulesTableViewCell.h"
//#import "Constants.h"
//#import "SYCoreDataManager.h"
//#import "DeviceTimeScheduleClass.h"
//
//@implementation DeviceSchedulesTableViewCell
//- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
//{
//    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
//    if (self) {
//        // Initialization code
//        self.frame = CGRectMake(0, 0, 300, 50);
//        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain]; //create tableview a
//        _tableView.scrollEnabled = false;
//        _tableView.tag = 100;
//        _tableView.delegate = self;
//        _tableView.dataSource = self;
//        _tableView.separatorColor = [UIColor clearColor];
//        [self addSubview:_tableView]; // add it cell
//        
//    }
//    return self;
//}
//
//- (void)awakeFromNib {
//    [super awakeFromNib];
//    // Initialization code
//}
//
//- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
//    [super setSelected:selected animated:animated];
//    
//    // Configure the view for the selected state
//}
//
//-(void)layoutSubviews
//{
//    [super layoutSubviews];
//    //    UITableView *subMenuTableView =(UITableView *) [self viewWithTag:100];
//    _tableView.frame = CGRectMake(20, 0, 220, self.bounds.size.height);
//    _tableView.backgroundColor = [UIColor blackColor];
//    _tableView.layer.borderColor = USBlueColor.CGColor;
//    _tableView.layer.borderWidth = 1.0f;
//    //set the frames for tableview
//    
//}
//
////manage datasource and  delegate for submenu tableview
//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    
//    return 1;
//}
//
//-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//
//        NSLog(@"%lu",(unsigned long)[[SYCoreDataManager sharedInstance] getAllDeviceSchedules].count);
//        return [[SYCoreDataManager sharedInstance] getAllDeviceSchedules].count;
//    
//}
//
//-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
//    if(cell == nil)
//    {
//        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
//    }
//    cell.backgroundColor = [UIColor blackColor];
//    
//    UIView *bgColorView = [[UIView alloc] init];
//    bgColorView.backgroundColor = USBlueColor;
//    [cell setSelectedBackgroundView:bgColorView];
//    
//    cell.backgroundColor = [UIColor blackColor];
//    
//        DeviceTimeScheduleClass *schedule = [[[SYCoreDataManager sharedInstance] getAllDeviceSchedules] objectAtIndex:indexPath.row];
////        NSLog(@"Name: %@ ID: %@", schedule.label, schedule.devicetimescheduleclassID);
//        cell.textLabel.text = schedule.label;
//        cell.textLabel.textColor = [UIColor whiteColor];
//
//    return cell;
//}
//
//-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    DeviceTimeScheduleClass *schedule = [[[SYCoreDataManager sharedInstance] getAllDeviceSchedules] objectAtIndex:indexPath.row];
//    _parentController.schedule =  schedule;
//    [_parentController performSegueWithIdentifier:@"scheduleDetail" sender:nil];
//}
//
//
//- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
//    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,-60,300,60)];
//    
//    // create the label object
//    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
//    headerLabel.frame = CGRectMake(0,0,_tableView.frame.size.width,60);
//    headerLabel.backgroundColor = [UIColor blackColor];
//    headerLabel.font = [UIFont fontWithName:@"Roboto-Thin" size:18];
//    headerLabel.textAlignment = NSTextAlignmentCenter;
//    
//    UITapGestureRecognizer *singleFingerTap =
//    [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                action:@selector(elementsTapped)];
//    [customView addGestureRecognizer:singleFingerTap];
//    
//    UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectZero];
//    arrow.frame = CGRectMake(_tableView.frame.size.width - 30,20,20,20);
//    arrow.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
//    
//    headerLabel.text = @"Schedules";
//
//    
//    headerLabel.textColor = [UIColor whiteColor];
//    
//    [customView addSubview:headerLabel];
//    [customView addSubview:arrow];
//    
//    return customView;
//}
//
//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 60;
//}
//
//-(void)elementsTapped{
//    [_parentController reloadTableView];
//}
//
//@end
