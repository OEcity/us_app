//
//  RoomTypeResponse.m
//  iHC-MIRF
//
//  Created by Vlastimil Venclik on 21.11.13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "RoomTypeResponse.h"
#import "NSObject+SBJson.h"

@implementation RoomTypeResponse
-(NSObject*)parseResponse:(NSData *)inputData identifier:(NSString *)identifier{
    NSMutableArray * itemsArray = [[NSMutableArray alloc] init];
    
    NSString *inputString = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];
    
    NSDictionary* jsonObjects = (NSDictionary*)[inputString JSONValue];
    
    
    for(NSString* string in jsonObjects)
        [itemsArray addObject:string];
    
    return itemsArray;
}

@end
