//
//  SYFloorplanDetailViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/11/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYFloorplanDetailViewController.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface SYFloorplanDetailViewController ()

@end

@implementation SYFloorplanDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }


    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backButtonPressed)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [self.imageView addGestureRecognizer:singleTap];
        [self.imageView setUserInteractionEnabled:YES];
    self.operationQueue = [[NSOperationQueue alloc] init];
}

-(void)viewDidAppear:(BOOL)animated{
    if (IS_IPHONE_5){

    }else{
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        BOOL statusBarHidden = [UIApplication sharedApplication].statusBarHidden;

        CGFloat screenHeight = screenRect.size.height;
        if (!statusBarHidden) screenHeight-=20;
        CGRect origframe = self.bottomView.frame;
        origframe.origin.y = screenHeight-self.bottomView.frame.size.height;
        [self.bottomView setFrame:origframe];
        [self.imageView setFrame:CGRectMake(0, 0, 320, screenHeight- self.bottomView.frame.size.height)];
        
    }
    if (self.image != nil)
        [self.imageView setImage:self.image];
    else{
        NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadDataAsychn:) object:nil];
        [self.operationQueue addOperation:invocationOperation];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [_leftBottomButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
}
-(IBAction)backButtonPressed{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSUInteger)supportedInterfaceOrientations
{

    return UIInterfaceOrientationMaskPortrait;
}
-(void)loadDataAsychn:(NSMutableDictionary*)dict
{
    NSString * imageURl = self.floorplanURL;
    UIActivityIndicatorView * actInd = [dict objectForKey:@"ind"];

    NSData *imageData=[[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:imageURl]];
    UIImage *image=[[UIImage alloc]initWithData:imageData];
    NSMutableDictionary* imgDict = [[NSMutableDictionary alloc] init];
    [imgDict setValue:image forKey:@"image"];
    [imgDict setValue:actInd forKey:@"ind"];
    [imgDict setValue:imageURl forKey:@"imageNo"];
    [self performSelectorOnMainThread:@selector(showImage:) withObject:imgDict waitUntilDone:NO];
}
-(void)showImage:(NSMutableDictionary *)Dict
{
    UIImage * image = [Dict objectForKey:@"image"];
    [self.imageView setImage:image];
}
@end
