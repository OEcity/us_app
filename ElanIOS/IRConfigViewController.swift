//
//  IRConfigViewController.swift
//  iHC-MIIRF
//
//  Created by Tom Odler on 13.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

import UIKit
var devices : NSFetchedResultsController<IRDevice>?
var deviceToSend : IRDevice?


class IRConfigViewController: UIViewController, NSFetchedResultsControllerDelegate, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initFetchedResultController()

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        for device : IRDevice in (devices?.fetchedObjects)! {
            if(device.deviceID == nil){
                SYCoreDataManager.sharedInstance().delete(device)
            }
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (devices!.fetchedObjects?.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyCell")!
        
        let label : UILabel = cell.viewWithTag(1) as! UILabel
        label.text = (devices?.object(at: indexPath))?.label
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        deviceToSend = devices?.object(at: indexPath)
        self.performSegue(withIdentifier: "IRDeviceDetail", sender: nil)
    }
    
    func initFetchedResultController(){
        let context : NSManagedObjectContext = SYCoreDataManager .sharedInstance().managedObjectContext
        
        let fetchRequest = NSFetchRequest<IRDevice>(entityName: "IRDevice")
        let sortDescriptor = NSSortDescriptor.init(key: "label", ascending: true)
        
        let sortDescriptors = NSArray.init(objects: sortDescriptor)
       fetchRequest.predicate = NSPredicate.init(format: "NOT(self.deviceID == nil)")
        
        fetchRequest.sortDescriptors = sortDescriptors as? [NSSortDescriptor]
       
        devices = NSFetchedResultsController.init(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        devices?.delegate = self
        
        do{
            try devices?.performFetch()
        } catch let error as NSError{
            print("Error: %@", error.localizedDescription)
        }
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        let myTableView = tableView
        
        switch type {
        case .insert:
            myTableView?.insertRows(at: NSArray.init(object: newIndexPath) as! [IndexPath], with: UITableViewRowAnimation.fade)
            break
        case .delete:
            myTableView?.deleteRows(at: NSArray.init(object: indexPath) as! [IndexPath], with: UITableViewRowAnimation.fade)
            break
        case .update:
            myTableView?.reloadRows(at: [NSIndexPath (row: (indexPath?.row)!, section: (indexPath?.section)!) as IndexPath], with: UITableViewRowAnimation.none)
            break
        case .move:
            myTableView?.deleteRows(at: NSArray.init(object: indexPath) as! [IndexPath], with: UITableViewRowAnimation.fade)
            myTableView?.insertRows(at: NSArray.init(object: newIndexPath) as! [IndexPath], with: UITableViewRowAnimation.fade)
            break
        default: break
            
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let controller = segue.destination as! IRDeviceDetailViewController

        if(segue.identifier == "IRDeviceDetail"){
            if(deviceToSend != nil){
                controller.device = deviceToSend
            } else {
                controller.device = IRDevice()
            }
        } else {
  
            controller.device = nil
            
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true;
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if(editingStyle == .delete){
            let device = devices?.object(at: indexPath)
            let alert = UIAlertController(title: "Do you really want to delete device: " + (device?.label)!, message: "", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {(alert: UIAlertAction!) in self.deleteDevice(device: device!)}))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func deleteDevice(device: IRDevice){
        let device = SYCoreDataManager.sharedInstance().getDeviceWithID(device.deviceID)
        SYAPIManager.sharedInstance().deleteIRDevice(withID: device, success: { (operation: AFHTTPRequestOperation?,response: Any?) -> Void in
            print("success")
        }) { (operation: AFHTTPRequestOperation?,error: Error?) -> Void in
                print("error: " + (error?.localizedDescription)!)
        }
    }

}
