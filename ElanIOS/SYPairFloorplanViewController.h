//
//  PairFloorplanViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 05/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
#import "Room.h"
#import "SYPairDevicesViewController.h"
#import "HUDWrapper.h"
@interface SYPairFloorplanViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIView *deviceBackgroundTemplate;
@property (weak, nonatomic) IBOutlet UILabel *deviceLabelTemplate;
@property (weak, nonatomic) IBOutlet UIImageView *deviceIconTemplate;
@property (nonatomic, retain) NSMutableArray* selectedDevices;
@property (nonatomic, retain) Device * selectedDevice;
@property (nonatomic, retain) Room * selectedRoom;
@property (nonatomic, retain) HUDWrapper* loaderDialog;
@property (nonatomic, retain) NSMutableArray * deviceViews;
@property (nonatomic) CGPoint startPos;
@property (weak, nonatomic) IBOutlet UIView *canvas;
@property (retain, nonatomic)  UIImage *loadedImage;
@property (nonatomic, retain) NSMutableArray * viewsInCanvas;
@property (weak, nonatomic) IBOutlet UILabel *alertLabel;
@property BOOL landscape;
@property BOOL shouldClose;
@property (nonatomic,retain) SYPairDevicesViewController * parent;
@property (weak, nonatomic) IBOutlet UIButton *leftBottomButton;
@property (weak, nonatomic) IBOutlet UIButton *rightBottomButton;


@end
