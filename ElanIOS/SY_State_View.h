//
//  SY_State_View.h
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 18/06/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SY_State_View : UIView

+(id)initStateViewForNibName:(NSString*)name;
+(void)insertView:(UIView*)view ToSuperView:(UIView*)superVeiw;
-(void)insertViewToSuperView:(UIView*)superVeiw;
-(void)displayView;

@end
