//
//  CentralSource+CoreDataProperties.m
//  
//
//  Created by Tom Odler on 16.11.16.
//
//

#import "CentralSource+CoreDataProperties.h"

@implementation CentralSource (CoreDataProperties)

+ (NSFetchRequest<CentralSource *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"CentralSource"];
}

@dynamic centralsourceID;
@dynamic label;
@dynamic type;
@dynamic device;
@dynamic areas;
@dynamic elan;

@end
