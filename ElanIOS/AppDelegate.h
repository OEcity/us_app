//
//  AppDelegate.h
//  ElanIOS
//
//  Created by Vratislav Zima on 5/24/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import <Availability.h>
#import "DevicesLoader.h"
#import "SYWebSocket.h"
#import "SYRoomsViewController.h"
#import "MainMenuPVC.h"
#import <SRWebSocket.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
@private BOOL incomingCallExists;
@private BOOL activeCallExists;
}

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, retain) DevicesLoader * loader;
@property (nonatomic, retain) SYRoomsViewController * viewController;
@property (nonatomic, retain) NSOperationQueue *operationQueue;
@property (nonatomic, retain) NSDictionary* limitsSettings;
@property (nonatomic, retain) MainMenuPVC *mainMenuPVC;
@property BOOL inBackground;
@property BOOL loadingDevices;
@property BOOL loadingRooms;
@property BOOL loadingScenes;
@property BOOL isAfterLaunch;
@property BOOL showRooms;
@property BOOL itercomEnabled;
@property BOOL hasOPWeather;
@property BOOL hasGIOMWeather;
@property BOOL intercomShown;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
