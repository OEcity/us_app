//
//  DecreaseTime.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 15/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "ChangeTime.h"
#import "NSNumber+Type.h"

@implementation ChangeTime


-(id) initWithNSDictionary:(Action*)dict{


    NSString* type = dict.type;
    if (type==nil) type =@"int";
    _max = dict.max;
    _min = dict.min;
    _step = dict.step;
    
    return self;
    
}


@end
