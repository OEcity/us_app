//
//  LoaderHelper.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 2/14/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "LoaderHelper.h"
#import "SYAPIManager.h"
#import "AppDelegate.h"
//#import "CoreDataManager.h"
#import "Util.h"
#import "DeviceDetailResponse.h"
#import "DevicesResponse.h"
#import "StateResponse.h"
#import <Crashlytics/Crashlytics.h>
@implementation LoaderHelper{
    
}

+(LoaderHelper*) sharedLoaderHelper{
    
    static LoaderHelper *_sharedLoaderHelper = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedLoaderHelper = [[LoaderHelper alloc] init];
    });
    _sharedLoaderHelper.errorPopupped = NO;
    return _sharedLoaderHelper;
}

/*
- (void)reloadDevicesAndRoomsOnCounterChange{
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (app.loadingDevices) return;
    app.loadingDevices=YES;
    
    [[SYAPIManager sharedInstance] getConfig:^(AFHTTPRequestOperation * operation, id response){
        
        NSDictionary * configDictionary = (NSDictionary*)response;
        NSNumber * configurationChangesCounter = [configDictionary valueForKey:@"configuration changes counter"];
        
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        long savedChangesCounter = [defaults integerForKey:@"CCC"];
        
        NSString* deviceTypes =  [configDictionary valueForKey:@"device types"];
        NSString* productTypes =  [configDictionary valueForKey:@"product types"];
        NSString* roomTypes =  [configDictionary valueForKey:@"room types"];
        
        [defaults setObject:deviceTypes forKey:@"device types"];
        [defaults setObject:roomTypes forKey:@"room types"];
        [defaults setObject:productTypes forKey:@"product types"];
        
        if ([configurationChangesCounter integerValue] != savedChangesCounter){
            [defaults setInteger:[configurationChangesCounter integerValue] forKey:@"CCC"];
            
            [self reloadDevicesAndRooms];
        }else if ([CoreDataManager readDevices] == nil){
            [self reloadDevices];
        } else {
            [self reloadDeviceState];
        }
        [defaults synchronize];
        
    }failure:^(AFHTTPRequestOperation * operation, NSError * error){
        [self performSelectorOnMainThread:@selector(errorDuringLoading:) withObject:nil waitUntilDone:NO];
        
        
    }];
    
}

- (void)reloadDevicesAndRooms{
    [self performSelectorOnMainThread:@selector(startLoading) withObject:nil waitUntilDone:NO];
    NSMutableDictionary * devicesCoords = [[NSMutableDictionary alloc] init];
    
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext* objectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [objectContext setPersistentStoreCoordinator:appDelegate.persistentStoreCoordinator];
    
    [[SYAPIManager sharedInstance] getRoomsWithSuccess:^(AFHTTPRequestOperation * request, id response){
        NSDictionary * responseDict = (NSDictionary*)response;
        NSArray * rooms = [responseDict allKeys];
        __block int i =0;
        [self performSelectorOnMainThread:@selector(clearRoomsDB) withObject:nil waitUntilDone:YES];
        for (NSString * key in rooms){
            AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            if (app.loadingDevices==NO){
                [self performSelectorOnMainThread:@selector(errorDuringLoading:) withObject:nil waitUntilDone:NO];
                return;
            }
            
            [[SYAPIManager sharedInstance] getRoomsWithPath:key success:^(AFHTTPRequestOperation * request, id response){
                NSDictionary * roomDetailResponse = (NSDictionary*)response;
                
                AppDelegate* app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                
                NSEntityDescription *entity = [NSEntityDescription entityForName:@"Room" inManagedObjectContext:app.managedObjectContext];
                
                Room * room = (Room*)[[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
                
                NSDictionary* floorplanDict = [roomDetailResponse objectForKey:@"floorplan"];
                if ([floorplanDict isKindOfClass: [NSNull class]]==NO ){
                    room.floorplan = [floorplanDict objectForKey:@"image"];
                }
                NSDictionary* roomInfo = [roomDetailResponse objectForKey:@"room info"];
                NSString * label  =[roomInfo valueForKey:@"label"];
                NSString * type  =[roomInfo valueForKey:@"type"];
                
                room.name = [roomDetailResponse objectForKey:@"id"];;
                room.label = label;
                room.type = type;
                
                
                
                [CoreDataManager saveRoom:room inContext:objectContext];
                
                
                NSDictionary* deviceInfo = [roomDetailResponse objectForKey:@"devices"];
                
                for(NSString* device in deviceInfo)
                {
                    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                    if (app.loadingDevices==NO){
                        [self performSelectorOnMainThread:@selector(errorDuringLoading:) withObject:nil waitUntilDone:NO];
                        return;
                    }
                    
                    NSDictionary * value = [deviceInfo objectForKey:device];
                    NSArray * position = [value objectForKey:@"coordinates"];
                    
                    if (position && [position isKindOfClass:[NSNull class]]==NO){
                        [devicesCoords setObject:position forKey:device];
                        
                        NSDictionary * roomDevConn = [[NSDictionary alloc] initWithObjectsAndKeys:room.name,@"name",
                                                      device, @"deviceId",
                                                      position, @"position",nil];
                        [self storeRoomDeviceConnection:roomDevConn inContext:objectContext];
                        
                        
                    }else{
                        NSDictionary * roomDevConn = [[NSDictionary alloc] initWithObjectsAndKeys:room.name,@"name",
                                                      device, @"deviceId",nil];
                        [self storeRoomDeviceConnection:roomDevConn inContext:objectContext];
                        
                        
                        
                    }
                }
                if (i == [rooms count]-1){
                    NSError* error;
                    [objectContext save:&error];
                    [self reloadDevices];
                }
                    
                i++;
                
                
            }failure:^(AFHTTPRequestOperation * request, NSError * error){
                [self performSelectorOnMainThread:@selector(errorDuringLoading:) withObject:nil waitUntilDone:NO];
                CLS_LOG(@"Device loading error on address: %@ with error: %@", [request.request.URL absoluteString], error.description);
            }];
            
        }
        
        
        
        
    }failure:^(AFHTTPRequestOperation * request, NSError * error){
        [self performSelectorOnMainThread:@selector(errorDuringLoading:) withObject:nil waitUntilDone:NO];
        CLS_LOG(@"Device loading error on address: %@ with error: %@", [request.request.URL absoluteString], error.description);
    }];
    
    
    
    
    
}


-(void)reloadDevices{
    [self performSelectorOnMainThread:@selector(startLoading) withObject:nil waitUntilDone:NO];
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    NSManagedObjectContext* objectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [objectContext setPersistentStoreCoordinator:appDelegate.persistentStoreCoordinator];
    
    [[SYAPIManager sharedInstance] getDevicesWithSuccess:^(AFHTTPRequestOperation * request, id response){
        DevicesResponse * devicesResponse = [[DevicesResponse alloc] init];
        NSMutableArray * devices = (NSMutableArray *)[devicesResponse parseResponse:response identifier:nil];
        __block int i=0;
        [self performSelectorOnMainThread:@selector(clearDevicesDB) withObject:nil waitUntilDone:YES];
        for (Device* device in devices){
            AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
            if (app.loadingDevices==NO){
                [self performSelectorOnMainThread:@selector(errorDuringLoading:) withObject:nil waitUntilDone:NO];
                return;
            }
            
            [[SYAPIManager sharedInstance] getDeviceWithPath:device.name success:^(AFHTTPRequestOperation * request, id response){
                DeviceDetailResponse * detailResponse = [[DeviceDetailResponse alloc] init];
                __block Device * resp  = (Device*)[detailResponse parseResponse:response identifier:device.name];
                [[SYAPIManager sharedInstance] getDeviceStateWithPath:device.name success:^(AFHTTPRequestOperation * request, id response){
                    
                    
                    StateResponse * stateResp = [[StateResponse alloc] init];
                    State* state = (State*)[stateResp parseResponse:response identifier:device.name];
                    resp.deviceState = state;
                    [CoreDataManager saveDevice:resp inContext:objectContext];
                    if (i==[devices count]-1){
                        NSDictionary * realoadAllDict = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithBool:YES],@"reloadAll", nil];
                        NSError* error;
                        [objectContext save:&error];
                        [self performSelectorOnMainThread:@selector(complete:) withObject:realoadAllDict waitUntilDone:NO];
                    }
                    i++;
                    
                }failure:^(AFHTTPRequestOperation * request, NSError * error){
                    [self performSelectorOnMainThread:@selector(errorDuringLoading:) withObject:nil waitUntilDone:NO];
                    CLS_LOG(@"Device state loading error on address: %@ with error: %@", [request.request.URL absoluteString], error.description);
                }];
                [self.devices addObject:resp];
                
            }failure:^(AFHTTPRequestOperation * request, NSError * error){
                [self performSelectorOnMainThread:@selector(errorDuringLoading:) withObject:nil waitUntilDone:NO];
                CLS_LOG(@"Device loading error on address: %@ with error: %@", [request.request.URL absoluteString], error.description);
            }];
        }
    }failure:^(AFHTTPRequestOperation * request, NSError * error){
        CLS_LOG(@"Devices loading error on address: %@ with error: %@", [request.request.URL absoluteString], error.description);
        
        [self performSelectorOnMainThread:@selector(errorDuringLoading:) withObject:nil waitUntilDone:NO];
        
    }];
    return;
}

-(void)reloadDeviceState{
    [self performSelectorOnMainThread:@selector(startLoading) withObject:nil waitUntilDone:NO];
    
    NSArray * devices = [CoreDataManager readDevices];
    __block int i=0;
    for (__block Device* resp in devices)
        [[SYAPIManager sharedInstance] getDeviceStateWithPath:resp.name success:^(AFHTTPRequestOperation * request, id response){
            
            StateResponse * stateResp = [[StateResponse alloc] init];
            State* state = (State*)[stateResp parseResponse:response identifier:resp.name];
            
            resp.deviceState = state;
            [self performSelectorOnMainThread:@selector(updateDeviceState:) withObject:resp waitUntilDone:YES];
            if (i==[devices count]-1){
                NSDictionary * realoadAllDict = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithBool:YES],@"reloadAll", nil];
                [self performSelectorOnMainThread:@selector(complete:) withObject:realoadAllDict waitUntilDone:NO];
            }
            i++;
            
        }failure:^(AFHTTPRequestOperation * request, NSError * error){
            //[self performSelectorOnMainThread:@selector(errorDuringLoading:) withObject:nil waitUntilDone:NO];
            CLS_LOG(@"Device state loading error on address: %@ with error: %@", [request.request.URL absoluteString], error.description);
        }];
    
    return;
}


-(void)ReloadStateForDevice:( Device*)device isLast:(BOOL)isLast{
    
    
    
}

-(void)clearRoomsDB{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"aboutToCleanRoomsDB" object:nil];
    [CoreDataManager deleteAllObjects:@"DeviceInRoom"];
    [CoreDataManager deleteAllObjects:@"Room"];
    
}
-(void)clearDevicesDB{
    
    [CoreDataManager deleteAllObjects:@"Device"];
    
    
}

-(void)updateDeviceState:(id) resp{    
    [CoreDataManager updateDevice:resp];
}


-(void)storeRoomDeviceConnection:(NSDictionary*) resp inContext:(NSManagedObjectContext*)objectContext{
    
    
    if ([resp objectForKey:@"position"]!=nil){
        NSArray * position = [resp objectForKey:@"position"];
        [CoreDataManager storeRoomDeviceConnection:[resp valueForKey:@"name"] deviceId:[resp valueForKey:@"deviceId"] coordX:[position objectAtIndex:0] coordY:[position objectAtIndex:1] inContext:objectContext];
    }else{
        [CoreDataManager storeRoomDeviceConnection:[resp valueForKey:@"name"] deviceId:[resp valueForKey:@"deviceId"] inContext:objectContext];
    }
}

-(void)complete:(NSMutableDictionary *)Dict{
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.loadingDevices=NO;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self userInfo:Dict];
}

-(void)errorDuringLoading:(NSMutableDictionary *)Dict{
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.loadingDevices=NO;
    if (_errorPopupped) return;
    _errorPopupped = YES;
    //    [self performSelectorOnMainThread:@selector(clearRoomsDB) withObject:nil waitUntilDone:YES];
    //    [self performSelectorOnMainThread:@selector(clearDevicesDB) withObject:nil waitUntilDone:YES];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncError" object:self];
}



-(void)startLoading{
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.loadingDevices=YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingStarted" object:self];
    
}
*/
@end
