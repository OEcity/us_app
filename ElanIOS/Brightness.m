//
//  Brightness.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 6/20/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "Brightness.h"
#import "NSNumber+Type.h"

@implementation Brightness


-(id) initWithNSDictionary:(Action*)dict{

    NSString* type = dict.type;
    if (type==nil) type =@"int";
    _max = dict.max;
    _min = dict.min;
    _step = dict.step;
    
    return self;

}
@end
