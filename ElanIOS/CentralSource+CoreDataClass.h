//
//  CentralSource+CoreDataClass.h
//  
//
//  Created by Tom Odler on 16.11.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Device, HeatCoolArea, Elan;

NS_ASSUME_NONNULL_BEGIN

@interface CentralSource : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "CentralSource+CoreDataProperties.h"
