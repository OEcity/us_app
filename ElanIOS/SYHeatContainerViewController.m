//
//  SYHeatContainerViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/4/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "SYHeatContainerViewController.h"
#import "SYHeatingAreaContainerViewController.h"
#import "SYHeatingAreaContainerDetailViewController.h"
#import "SYTimeScheduleDetailViewController.h"
#import "SYTimeScheduleViewController.h"
#import "SYTimeScheduleIntervalViewController.h"
#import "SYCentralSourceViewController.h"
#import "SYCentralSourceDetalViewController.h"
@interface SYHeatContainerViewController (){
    BOOL changing;
}
@property (strong, nonatomic) NSString *currentSegueIdentifier;
@end

@implementation SYHeatContainerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.currentSegueIdentifier = @"timeSchedule";
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (changing) return;
    if ([segue.identifier isEqualToString:@"timeSchedule"])
    {
        [self initSubviews:segue];
        ((SYTimeScheduleViewController*)segue.destinationViewController).container=self;
    }else if ([segue.identifier isEqualToString:@"timeScheduleDetail"]){
        ((SYTimeScheduleDetailViewController*)segue.destinationViewController).schedule = [[NSMutableDictionary alloc] initWithDictionary:_timeScheduleConfig];
        ((SYTimeScheduleDetailViewController*)segue.destinationViewController).container=self;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }else if ([segue.identifier isEqualToString:@"timeScheduleInterval"]){
        ((SYTimeScheduleIntervalViewController*)segue.destinationViewController).schedule = _timeScheduleConfig;
        ((SYTimeScheduleIntervalViewController*)segue.destinationViewController).container=self;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }else if ([segue.identifier isEqualToString:@"centralSource"]){
        ((SYCentralSourceViewController*)segue.destinationViewController).container=self;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }else if ([segue.identifier isEqualToString:@"centralSourceDetail"]){
        ((SYCentralSourceDetalViewController*)segue.destinationViewController).centralSourceItem = _centralSourceItem;
        ((SYCentralSourceDetalViewController*)segue.destinationViewController).container=self;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }else if ([segue.identifier isEqualToString:@"heatingArea"]){
        ((SYHeatingAreaContainerViewController*)segue.destinationViewController).container=self;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }else if ([segue.identifier isEqualToString:@"heatingAreaDetail"]){
        ((SYHeatingAreaContainerDetailViewController*)segue.destinationViewController).container=self;
        ((SYHeatingAreaContainerDetailViewController*)segue.destinationViewController).device = _heatingDetailDevice;
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }

}

- (void)swapFromViewController:(UIViewController *)fromViewController toViewController:(UIViewController *)toViewController
{
    toViewController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    
    [fromViewController willMoveToParentViewController:nil];
    [self addChildViewController:toViewController];
    changing=YES;
    [self transitionFromViewController:fromViewController toViewController:toViewController duration:0.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:^(BOOL finished) {
        [fromViewController removeFromParentViewController];
        [toViewController didMoveToParentViewController:self];
        changing=NO;
    }];
}

-(void)initSubviews:(UIStoryboardSegue *)segue{
    if (self.childViewControllers.count > 0) {
        [self swapFromViewController:[self.childViewControllers objectAtIndex:0] toViewController:segue.destinationViewController];
    }
    else {
        [self addChildViewController:segue.destinationViewController];
        ((UIViewController *)segue.destinationViewController).view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        [self.view addSubview:((UIViewController *)segue.destinationViewController).view];
        [segue.destinationViewController didMoveToParentViewController:self];
    }
}

- (void)setViewController:(NSString*)identifier
{
    
    self.currentSegueIdentifier=identifier;
    [self performSegueWithIdentifier:self.currentSegueIdentifier sender:nil];
}


- (NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}
@end
