//
//  SelectedDeviceViewController.h
//  iHC-MIRF
//
//  Created by Tom Odler on 07.03.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device+Action.h"
#import "SYCoreDataManager.h"
@protocol deviceSelectDelegate
-(void)deviceTypeSet:(NSString*)type deviceID:(NSArray*)deviceID;
@end

@interface SelectedDeviceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray * devices;
@property (nonatomic, retain) NSArray * devicesID;
@property (nonatomic, retain) NSArray * otherDevicesID;
@property (nonatomic, retain) NSMutableArray * tempDevicesID;
@property (nonatomic, retain) Device *device;
@property (nonatomic, retain) DeviceTimeScheduleClass *mySchedule;
@property ( nonatomic, retain ) NSMutableSet *selectedDevices;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (nonatomic, retain) Elan*elan;
@property (nonatomic, retain) id <deviceSelectDelegate> delegate;
-(id)initWithDelegate:(id<deviceSelectDelegate>)delegate devices:(NSArray*)devices elan:(Elan*)elan withSchedule:(DeviceTimeScheduleClass*)schedule;

@end
