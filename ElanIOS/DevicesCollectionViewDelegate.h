//
//  DevicesCollectionViewDelegate.h
//  US App
//
//  Created by Tom Odler on 01.02.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "URLConnector.h"
#import "RGBViewController.h"
#import "IntensityConfigurationViewController.h"
#import "DeviceActionsViewController.h"
#import <CoreData/CoreData.h>

typedef enum : NSUInteger {
    DeviceCollectionModeRoom = 0,
    DeviceCollectionModeFavourite
} DeviceCollectionMode;


@interface DevicesCollectionViewDelegate : NSObject <UICollectionViewDataSource, UICollectionViewDelegate, URLConnectionListener, NSFetchedResultsControllerDelegate>

@property (nonatomic,retain) IBOutlet UIView * view;
@property (nonatomic,retain) NSMutableArray * devices;
@property (nonatomic,retain) NSMutableArray * rgbControllers;
@property (nonatomic,retain) UICollectionView* collectionView;
@property (nonatomic,strong) NSString* roomID;
@property (retain) RGBViewController * rgb;
@property (retain) IntensityConfigurationViewController * intensity;
@property (strong, nonatomic) DeviceActionsViewController * actions;
@property (nonatomic) DeviceCollectionMode deviceCollectionMode;

@property (nonatomic) BOOL editing;

@property(nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, strong) NSFetchedResultsController *fetchedResultsController2;

-(id)initWithRoomID:(NSString*)roomID;
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer;



@end
