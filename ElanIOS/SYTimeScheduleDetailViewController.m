//
//  SYTimeScheduleDetailViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 12/3/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "SYTimeScheduleDetailViewController.h"
#import "SYTimeScheduleIntervalViewController.h"
#import "OneWeekScheduleViewController.h"
#import "AppDelegate.h"
@interface SYTimeScheduleDetailViewController ()

@end

@implementation SYTimeScheduleDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_schedule!=nil){
        [_tfName setText:[_schedule objectForKey:@"label"]];
        if ([_schedule objectForKey:@"hysteresis"]) [_lHysteresisInput setText:[NSString stringWithFormat:@"%0.1f", [[_schedule objectForKey:@"hysteresis"] floatValue]]];
        NSDictionary * modes = [_schedule objectForKey:@"modes"];
        if (modes){
            [_btnMinimum setTitle:[NSString stringWithFormat:@"%0.1f",[[[modes objectForKey:@"1"] objectForKey:@"min"] floatValue]]  forState:UIControlStateNormal];
            [_btnAttenuation setTitle:[NSString stringWithFormat:@"%0.1f",[[[modes objectForKey:@"2"] objectForKey:@"min"] floatValue]]  forState:UIControlStateNormal];
            [_btnNormal setTitle:[NSString stringWithFormat:@"%0.1f",[[[modes objectForKey:@"3"] objectForKey:@"min"] floatValue]]  forState:UIControlStateNormal];
            [_btnComfort setTitle:[NSString stringWithFormat:@"%0.1f",[[[modes objectForKey:@"4"] objectForKey:@"min"] floatValue]]  forState:UIControlStateNormal];
        }
        
    }
    
    [_lNameTitle setText:NSLocalizedString(@"heat_cool_area_add_name", nil)];
    [_lHysteresisTitle setText:NSLocalizedString(@"heat_temp_schedule_add_hysteresis", nil)];
    [_lTempModeSettingsTitle setText:NSLocalizedString(@"heat_temp_schedule_add_sub_header", nil)];    
    [_lAttenuationTitle setText:NSLocalizedString(@"heat_mode_attenuation", nil)];
    [_lMinimumTitle setText:NSLocalizedString(@"heat_mode_minimum", nil)];
    [_lNormalTitle setText:NSLocalizedString(@"heat_mode_normal", nil)];
    [_lComfortTitle setText:NSLocalizedString(@"heat_mode_comfort", nil)];
    [_btnNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [_btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];

    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(rotated:) name:UIDeviceOrientationDidChangeNotification object:nil];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)rotated:(NSNotification *)notification {
    [self adjustViewsForOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void) adjustViewsForOrientation:(UIInterfaceOrientation) orientation {
    
    switch (orientation)
    {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown:
        case UIInterfaceOrientationLandscapeLeft:
        {
            //[self performSegueWithIdentifier:@"time_sched" sender:nil];
            break;
        }
        case UIInterfaceOrientationLandscapeRight:
        {
            //[self performSegueWithIdentifier:@"time_sched" sender:nil];
            break;
        }
        case UIInterfaceOrientationUnknown:break;
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)resignOnBackgroundTap:(id)sender {
    [_tfName resignFirstResponder];
}



- (IBAction)hysteresisButtonTap:(id)sender {
    
    _temperatureOffset = [[TemperatureOffsetViewController alloc] initWithDelegate:self min:0.5f max:5.0f  data:[[NSDictionary alloc] initWithObjectsAndKeys:@"hysteresis",@"block_id", nil]];
    AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (delegate.limitsSettings!=nil){
        NSDictionary * offset =[[delegate.limitsSettings objectForKey:@"timeschedule"] objectForKey:@"hysteresis"];
        [_temperatureOffset setBoundsWithMax:[[offset objectForKey:@"max"] floatValue] min:[[offset objectForKey:@"min"] floatValue]];
    }
    _temperatureOffset.realTemperature = [_lHysteresisInput.text floatValue];

    _temperatureOffset.view.tag= 100;
    UIView * superView = [[[self.view superview] superview] superview];
    [_temperatureOffset.view setFrame:superView.frame];
    [[_temperatureOffset.view viewWithTag:1] setCenter:_temperatureOffset.view.center];
    if ([superView viewWithTag:100]==nil){
        [UIView transitionWithView:superView
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve //any animation
                        animations:^ { [superView addSubview:_temperatureOffset.view]; }
                        completion:nil];
        
    }
}

- (IBAction)minimumEditTap:(id)sender {
    NSDictionary * modes = [_schedule objectForKey:@"modes"];
    _temperatureOffset = [[TemperatureOffsetViewController alloc] initWithDelegate:self min:[[[modes objectForKey:@"1"] objectForKey:@"min"] floatValue] max:/*[[[modes objectForKey:@"1"] objectForKey:@"max"] floatValue]*/35.0f data:[[NSDictionary alloc] initWithObjectsAndKeys:@"1",@"block_id", nil]];
    AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (delegate.limitsSettings!=nil){
        NSDictionary * offset =[[delegate.limitsSettings objectForKey:@"timeschedule"] objectForKey:@"mode temperature"];
        [_temperatureOffset setBoundsWithMax:[[offset objectForKey:@"max"] floatValue] min:[[offset objectForKey:@"min"] floatValue]];
    }
    _temperatureOffset.realTemperature = [((UIButton*)sender).titleLabel.text floatValue];
    _temperatureOffset.view.tag= 100;
    UIView * superView = [[[self.view superview] superview] superview];
    [_temperatureOffset.view setFrame:superView.frame];
    [[_temperatureOffset.view viewWithTag:1] setCenter:_temperatureOffset.view.center];
    if ([superView viewWithTag:100]==nil){
        [UIView transitionWithView:superView
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve //any animation
                        animations:^ { [superView addSubview:_temperatureOffset.view]; }
                        completion:nil];
        
    }


}
- (IBAction)attenuationEditTap:(id)sender {
    NSDictionary * modes = [_schedule objectForKey:@"modes"];
    _temperatureOffset = [[TemperatureOffsetViewController alloc] initWithDelegate:self min:[[[modes objectForKey:@"2"] objectForKey:@"min"] floatValue] max:/*[[[modes objectForKey:@"2"] objectForKey:@"max"] floatValue]*/35.0f data:[[NSDictionary alloc] initWithObjectsAndKeys:@"2",@"block_id", nil]];
    AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (delegate.limitsSettings!=nil){
        NSDictionary * offset =[[delegate.limitsSettings objectForKey:@"timeschedule"] objectForKey:@"mode temperature"];
        [_temperatureOffset setBoundsWithMax:[[offset objectForKey:@"max"] floatValue] min:[[offset objectForKey:@"min"] floatValue]];
    }
    _temperatureOffset.realTemperature = [((UIButton*)sender).titleLabel.text floatValue];

    _temperatureOffset.view.tag= 100;
    UIView * superView = [[[self.view superview] superview] superview];
    [_temperatureOffset.view setFrame:superView.frame];
    [[_temperatureOffset.view viewWithTag:1] setCenter:_temperatureOffset.view.center];
    if ([superView viewWithTag:100]==nil){
        [UIView transitionWithView:superView
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve //any animation
                        animations:^ { [superView addSubview:_temperatureOffset.view]; }
                        completion:nil];
        
    }

}
- (IBAction)normalEditTap:(id)sender {
    NSDictionary * modes = [_schedule objectForKey:@"modes"];
    _temperatureOffset = [[TemperatureOffsetViewController alloc] initWithDelegate:self min:[[[modes objectForKey:@"3"] objectForKey:@"min"] floatValue] max:/*[[[modes objectForKey:@"3"] objectForKey:@"max"] floatValue]*/35.0f data:[[NSDictionary alloc] initWithObjectsAndKeys:@"3",@"block_id", nil]];
    AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (delegate.limitsSettings!=nil){
        NSDictionary * offset =[[delegate.limitsSettings objectForKey:@"timeschedule"] objectForKey:@"mode temperature"];
        [_temperatureOffset setBoundsWithMax:[[offset objectForKey:@"max"] floatValue] min:[[offset objectForKey:@"min"] floatValue]];
    }
    _temperatureOffset.realTemperature = [((UIButton*)sender).titleLabel.text floatValue];

    _temperatureOffset.view.tag= 100;
    UIView * superView = [[[self.view superview] superview] superview];
    [_temperatureOffset.view setFrame:superView.frame];
    [[_temperatureOffset.view viewWithTag:1] setCenter:_temperatureOffset.view.center];
    if ([superView viewWithTag:100]==nil){
        [UIView transitionWithView:superView
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve //any animation
                        animations:^ { [superView addSubview:_temperatureOffset.view]; }
                        completion:nil];
     
    }
}
- (IBAction)comfortEditTap:(id)sender {
    NSDictionary * modes = [_schedule objectForKey:@"modes"];
    _temperatureOffset = [[TemperatureOffsetViewController alloc] initWithDelegate:self min:[[[modes objectForKey:@"4"] objectForKey:@"min"] floatValue] max:/*[[[modes objectForKey:@"4"] objectForKey:@"max"] floatValue]*/35.0f data:[[NSDictionary alloc] initWithObjectsAndKeys:@"4",@"block_id", nil]];
    AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (delegate.limitsSettings!=nil){
        NSDictionary * offset =[[delegate.limitsSettings objectForKey:@"timeschedule"] objectForKey:@"mode temperature"];
        [_temperatureOffset setBoundsWithMax:[[offset objectForKey:@"max"] floatValue] min:[[offset objectForKey:@"min"] floatValue]];


    }
    _temperatureOffset.realTemperature = [((UIButton*)sender).titleLabel.text floatValue];
    _temperatureOffset.view.tag= 100;
    UIView * superView = [[[self.view superview] superview] superview];
    [_temperatureOffset.view setFrame:superView.frame];
    [[_temperatureOffset.view viewWithTag:1] setCenter:_temperatureOffset.view.center];
    if ([superView viewWithTag:100]==nil){
        [UIView transitionWithView:superView
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve //any animation
                        animations:^ { [superView addSubview:_temperatureOffset.view]; }
                        completion:nil];
        
    }

}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}



- (IBAction)backButtonTap:(id)sender {
    [_container setViewController:@"timeSchedule"];
    
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [_schedule setObject:_tfName.text forKey:@"label"];
    
    NSMutableDictionary * modes = [[NSMutableDictionary alloc] initWithDictionary:[_schedule objectForKey:@"modes"]];
    
    NSMutableDictionary* mode1= [[NSMutableDictionary alloc] initWithDictionary:[modes objectForKey:@"1"]] ;
    [mode1 setObject:[[NSNumber alloc] initWithFloat:[_btnMinimum.titleLabel.text floatValue]] forKey:@"min"];
    [modes setObject:mode1 forKey:@"1"];
    
    NSMutableDictionary* mode2= [[NSMutableDictionary alloc] initWithDictionary:[modes objectForKey:@"2"]];
    [mode2 setObject:[[NSNumber alloc] initWithFloat:[_btnAttenuation.titleLabel.text floatValue]] forKey:@"min"];
    [modes setObject:mode2 forKey:@"2"];
    
    NSMutableDictionary* mode3= [[NSMutableDictionary alloc] initWithDictionary:[modes objectForKey:@"3"]];
    [mode3 setObject:[[NSNumber alloc] initWithFloat:[_btnNormal.titleLabel.text floatValue]] forKey:@"min"];
    [modes setObject:mode3 forKey:@"3"];
    
    NSMutableDictionary* mode4= [[NSMutableDictionary alloc] initWithDictionary:[modes objectForKey:@"4"]];
    [mode4 setObject:[[NSNumber alloc] initWithFloat:[_btnComfort.titleLabel.text floatValue]] forKey:@"min"];
    [modes setObject:mode4 forKey:@"4"];
    
    [_schedule setObject:modes forKey:@"modes"];
    
    [_schedule setObject:[[NSNumber alloc] initWithFloat:[_lHysteresisInput.text floatValue]] forKey:@"hysteresis"];
    
    
    
    ((OneWeekScheduleViewController *)segue.destinationViewController).schedule = _schedule;
    ((OneWeekScheduleViewController *)segue.destinationViewController).container = _container;

}
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([_tfName.text isEqualToString:@""] ){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:NSLocalizedString(@"nameError", nil)
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
        
    }if ( [_lHysteresisInput.text isEqualToString:@""]){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:NSLocalizedString(@"Hysteresis value cannot be empty", nil)
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
    return YES;
}

#pragma mark Temperature set delegate
- (void)temperatureSet:(float)temperature data:(NSDictionary *)data{
    NSMutableDictionary * modesParent = [[NSMutableDictionary alloc] initWithDictionary:[_schedule objectForKey:@"modes"]];

    if ([[data objectForKey:@"block_id"] isEqualToString:@"1"]){
        [_btnMinimum setTitle:[NSString stringWithFormat:@"%0.1f", temperature]  forState:UIControlStateNormal];
        NSMutableDictionary* modes= [[NSMutableDictionary alloc] initWithDictionary:[modesParent objectForKey:@"1"]];
        [modes setObject:[[NSNumber alloc] initWithFloat:temperature] forKey:@"min"];
        [modesParent setObject:modes forKey:@"1"];

        
    }else if ([[data objectForKey:@"block_id"] isEqualToString:@"2"]){
        [_btnAttenuation setTitle:[NSString stringWithFormat:@"%0.1f", temperature]  forState:UIControlStateNormal];
        NSMutableDictionary* modes= [[NSMutableDictionary alloc] initWithDictionary:[modesParent objectForKey:@"2"]];
        [modes setObject:[[NSNumber alloc] initWithFloat:temperature] forKey:@"min"];
        [modesParent setObject:modes forKey:@"2"];
    }else if ([[data objectForKey:@"block_id"] isEqualToString:@"3"]){
        [_btnNormal setTitle:[NSString stringWithFormat:@"%0.1f", temperature]  forState:UIControlStateNormal];
        NSMutableDictionary* modes= [[NSMutableDictionary alloc] initWithDictionary:[modesParent objectForKey:@"3"]];
        [modes setObject:[[NSNumber alloc] initWithFloat:temperature] forKey:@"min"];
        [modesParent setObject:modes forKey:@"3"];

    }else if ([[data objectForKey:@"block_id"] isEqualToString:@"4"]){
        [_btnComfort setTitle:[NSString stringWithFormat:@"%0.1f", temperature]  forState:UIControlStateNormal];
        NSMutableDictionary* modes= [[NSMutableDictionary alloc] initWithDictionary:[modesParent objectForKey:@"4"]];
        [modes setObject:[[NSNumber alloc] initWithFloat:temperature] forKey:@"min"];
        [modesParent setObject:modes forKey:@"4"];
    }else if ([[data objectForKey:@"block_id"] isEqualToString:@"hysteresis"]){
        [_lHysteresisInput setText:[NSString stringWithFormat:@"%0.1f", temperature]];
        [_schedule setObject:[[NSNumber alloc] initWithFloat:temperature] forKey:@"hysteresis"];


    }
    
    
    [_schedule setObject:modesParent forKey:@"modes"];

}

- (IBAction)editBegin:(id)sender {
    UITextField * tf = sender;
    [tf setBackground:[UIImage imageNamed:@"edit_text_bcg_on.png"]];
    [tf setTextColor:[UIColor whiteColor]];
    
}
- (IBAction)editEnd:(id)sender {
    UITextField * tf = sender;
    [tf setBackground:[UIImage imageNamed:@"edit_text_bcg.png"]];
    [tf setTextColor:[UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1]];
}


@end
