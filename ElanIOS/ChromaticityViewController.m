//
//  ViewController.m
//  RGBViewController
//
//  Created by admin on 21.06.16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "ChromaticityViewController.h"
#import "EFCircularSlider.h"
#import "EFGradientGenerator.h"
#import "SYCoreDataManager.h"
#import "SYAPIManager.h"
#import "UIViewController+RevealViewControllerAddon.h"
#import "Constants.h"


@interface ChromaticityViewController ()
@property (weak, nonatomic) IBOutlet UIView *topSliderView;
@property (weak, nonatomic) IBOutlet UIView *bottomSliderView;

@property (weak, nonatomic) IBOutlet UILabel *productLabel;

@property (weak, nonatomic) IBOutlet UILabel *topSlideViewValueIndicatorLabel;
@property (strong, nonatomic) EFGradientGenerator *gradientGen;
@property (weak, nonatomic) IBOutlet UIButton *whiteButton;
@property (weak, nonatomic) IBOutlet UIButton *switchButton;
@property (weak, nonatomic) IBOutlet UILabel *lisghtDescriptionLabel;
@property (strong, nonatomic) EFCircularSlider* circularSlider;
@property (strong, nonatomic) EFCircularSlider* bcircularSlider;
@property (weak, nonatomic) IBOutlet UIButton *automatButton;

@property (nonatomic) BOOL automat;

@property (nonatomic) int whiteBalance;
@property (nonatomic) int intensity;

@end

@implementation ChromaticityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _deviceName = _device.deviceID;
    [self initFetchedResultsControllerWithHCAID:_deviceName];
    
    _productLabel.text = _device.productType;
    _lisghtDescriptionLabel.text = _device.label;
    
    
    _whiteBalance = [[_device getStateValueForStateName:@"white balance"] intValue];
    _intensity = [[_device getStateValueForStateName:@"brightness"] intValue];
    
    _automatButton.userInteractionEnabled = NO;
    
    for(DeviceTimeScheduleClass*schedule in [[SYCoreDataManager sharedInstance]getAllDeviceSchedules]){
        for(Device*device in schedule.devices){
            if(_device.deviceID == device.deviceID){
                _automatButton.userInteractionEnabled = YES;
            }
        }
    }
    
    if(_device.automat.boolValue){
        [_automatButton setSelected:YES];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //CONF BUTTONS
    [[_whiteButton layer] setBorderColor:[[UIColor colorWithRed:0.0/255.0 green:192.0/255.0 blue:243.0/255.0 alpha:1.0] CGColor]];
    [[_switchButton layer] setBorderColor:[[UIColor colorWithRed:0.0/255.0 green:192.0/255.0 blue:243.0/255.0 alpha:1.0] CGColor]];
    [[_automatButton layer] setBorderColor:USBlueColor.CGColor];
    
    
    //TOP SLIDER CONF
    CGRect sliderFrame = CGRectMake(0, 0, _topSliderView.frame.size.width, _topSliderView.frame.size.height);
    _circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    [_circularSlider addTarget:self action:@selector(topSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_circularSlider addTarget:self action:@selector(saveButtonPressed) forControlEvents:UIControlEventEditingDidEnd];
    [_topSliderView addSubview:_circularSlider];
    [_circularSlider pauseAutoredrawing];
    
    CGFloat dash[]={1,15};
    [_circularSlider setHandleRadius:10];
    [_circularSlider setUnfilledLineDash:dash andCount:2];
    [_circularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_circularSlider setUnfilledColor:[UIColor whiteColor]];
    [_circularSlider setFilledColor:[UIColor whiteColor]];
    [_circularSlider setHandleColor:[UIColor whiteColor]];
    [_circularSlider setLineWidth:1];
    [_circularSlider setArcStartAngle:150];
    [_circularSlider setArcAngleLength:240];
    
    
    [_circularSlider setCurrentArcValue:(CGFloat)_intensity/2.55 forStartAnglePadding:2 endAnglePadding:2];
    
    //BOTTOM SLIDER CONF
    
    EFGradientGenerator *gradient = [[EFGradientGenerator alloc] initWithSize:_bottomSliderView.frame.size];
    [gradient setRadius:80];
    [gradient setSectors:360];
    
    
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:1];
    [gradient addNewColor:[UIColor colorWithRed:208.0/255.0 green:85.0/255.0 blue:32.0/255.0 alpha:1] atSector:40];
    [gradient addNewColor:[UIColor colorWithRed:240.0/255.0 green:163.0/255.0 blue:52.0/255.0 alpha:1] atSector:90];
    [gradient addNewColor:[UIColor colorWithRed:231.0/255.0 green:208.0/255.0 blue:69.0/255.0 alpha:1] atSector:180];
    [gradient addNewColor:[UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:210.0/255.0 alpha:1] atSector:270];
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:310];
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:360];
    
    [gradient renderImage];
    _gradientGen = gradient;
    
    CGRect bsliderFrame = CGRectMake(0, 0, _bottomSliderView.frame.size.width, _bottomSliderView.frame.size.height);
    _bcircularSlider = [[EFCircularSlider alloc] initWithFrame:bsliderFrame];
    [_bcircularSlider addTarget:self action:@selector(botSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_bcircularSlider addTarget:self action:@selector(saveButtonPressed) forControlEvents:UIControlEventEditingDidEnd];
    [_bottomSliderView addSubview:_bcircularSlider];
    [_bcircularSlider pauseAutoredrawing];
    
    [_bcircularSlider setImageBackgroundUnfilledLine:YES];
    [_bcircularSlider setLineWidth:8];
    [_bcircularSlider setFilledColor:[UIColor clearColor]];
    [_bcircularSlider setUnfilledLineStrokeBorderWidth:0];
    [_bcircularSlider setUnfilledLineStrokeBorderColor:[UIColor clearColor]];
    [_bcircularSlider setUnfilledLineInnerImage:[gradient renderedImage]];
    [_bcircularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_bcircularSlider setHandleColor:[UIColor whiteColor]];
    [_bcircularSlider setArcStartAngle:130];
    [_bcircularSlider setArcAngleLength:280];
    [_bcircularSlider setHandleRadius:10];
    [_bcircularSlider setHandleBorderSize:2];
    [_bcircularSlider setCurrentArcValue:(100 - _whiteBalance) forStartAnglePadding:2 endAnglePadding:2];
    
    //TODO: SET BOTTOM SLIDER VALUE
    
    //Configure navigation bar apperence
    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
    
    [self initAutomat];
    
    [self.circularSlider layoutIfNeeded];
}

-(void)topSliderValueChanged:(EFCircularSlider*)circularSlider {
    _topSlideViewValueIndicatorLabel.text = [NSString stringWithFormat:@"%.f", [circularSlider getCurrentArcValueForStartAnglePadding:2 endAnglePadding:2]];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:_topSlideViewValueIndicatorLabel.text];
    
    _intensity = [myNumber intValue];
    
}

-(void)botSliderValueChanged:(EFCircularSlider*)circularSlider {
    [circularSlider setHandleColor:[_gradientGen getColorFromAngleFromNorth:([circularSlider angleFromNorth])]];
    _whiteBalance = (100 - [circularSlider getCurrentArcValueForStartAnglePadding:2 endAnglePadding:2]);
}

- (IBAction)whiteButtonEvent:(id)sender {
    _whiteBalance = 50;
    [self saveButtonPressed];
}

- (IBAction)onOffButtonEvent:(id)sender {
    if (self.intensity==0){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *myIntensity = (NSNumber*)[defaults objectForKey:_deviceName];
        if (myIntensity!=nil){
            self.intensity = myIntensity.intValue;
            [defaults removeObjectForKey:_deviceName];
        }
    }else{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber * intensityNumber = [[NSNumber alloc] initWithInt:_intensity];
        [defaults setObject:intensityNumber forKey:_deviceName];
        [defaults synchronize];
        self.intensity = 0;
    }
    [self saveButtonPressed];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)initFetchedResultsControllerWithHCAID:(NSString*)hcaID {
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"State"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"device.label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.device.deviceID == %@", hcaID];
    [fetchRequest setPredicate:predicate];
    _devices = [[NSFetchedResultsController alloc]
                initWithFetchRequest:fetchRequest
                managedObjectContext:context
                sectionNameKeyPath:nil
                cacheName:nil];
    _devices.delegate = self;
    NSError *error;
    
    if (![_devices performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }else if ([[_devices fetchedObjects] count]>0){
        _device = (Device*)((State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if ([_devices.fetchedObjects count]>0){
        _device = (Device*)((State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
        
        [self syncComplete:_device];
    }
}

-(void)syncComplete:(Device *)device{
    
    if ([_deviceName isEqualToString:device.deviceID]){
        [self initAutomat];
        
        _whiteBalance = [[_device getStateValueForStateName:@"white balance"] intValue];
        _intensity = [[_device getStateValueForStateName:@"brightness"] intValue];
        
        NSLog(@"White Balance: %d", _whiteBalance);
        
        if ([[device getStateValueForStateName:@"brightness"] intValue]==0){
            [self.switchButton setTitle:@"ON" forState:UIControlStateNormal];
        }else{
            [self.switchButton setTitle:@"OFF" forState:UIControlStateNormal];
        }
        
        [_circularSlider setCurrentArcValue:_intensity/2.55 forStartAnglePadding:2 endAnglePadding:2];
        [_bcircularSlider setCurrentArcValue:100 - (_whiteBalance/2.55) forStartAnglePadding:2 endAnglePadding:2];
    }
}

-(void)saveButtonPressed{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:[NSNumber numberWithInt:_intensity*2.55] forKey:@"brightness"];
    [dict setObject:[NSNumber numberWithInt:_whiteBalance*2.55] forKey:@"white balance"];
    
    NSLog(@"%@",dict);
    
    [[SYAPIManager sharedInstance] putDeviceAction:dict device:[[SYCoreDataManager sharedInstance] getDeviceWithID: _deviceName]
                                           success:^(AFHTTPRequestOperation* operation, id response){
                                               NSLog(@"update complete: %@",response);
                                           }
                                           failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                               NSLog(@"update error: %@",[error description]);
                                           }];
}

-(void)initAutomat{
    _automatButton.selected = NO;
    _automat = NO;
    
    if([[_device getStateValueForStateName:@"automat"]boolValue]){
        _automatButton.selected = YES;
        _automat = YES;
    }
}
@end

