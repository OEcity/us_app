//
//  SYHeatingAreaViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/4/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYHeatContainerViewController.h"
@interface SYHeatingAreaViewController : UIViewController


@property (nonatomic, retain) SYHeatContainerViewController * containerViewController;
@property (weak, nonatomic) IBOutlet UIButton *bTimeSchedule;
@property (weak, nonatomic) IBOutlet UIImageView *iVTimeSchedule;
@property (weak, nonatomic) IBOutlet UIButton *bHeatingArea;
@property (weak, nonatomic) IBOutlet UIImageView *iVHeatingArea;
@property (weak, nonatomic) IBOutlet UIButton *bCentralSource;
@property (weak, nonatomic) IBOutlet UIImageView *iVCentralSource;
@property (weak, nonatomic) IBOutlet UILabel *lSectionName;
@property (weak, nonatomic) IBOutlet UILabel *lTimeScheduleTitle;
@property (weak, nonatomic) IBOutlet UILabel *lHeatingAreaTitle;
@property (weak, nonatomic) IBOutlet UILabel *lCentralSourceTitle;


@end
