//
//  DecreaseTime.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 15/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Action.h"
@interface ChangeTime : NSObject
@property (nonatomic, retain) NSNumber * min;
@property (nonatomic, retain) NSNumber * max;
@property (nonatomic, copy) NSString * type;
@property (nonatomic, retain) NSNumber * step;

-(id) initWithNSDictionary:(Action*)dict;

@end
