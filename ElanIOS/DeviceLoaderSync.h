//
//  DeviceLoaderSync.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/24/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceLoaderSync : NSObject
@property (nonatomic, retain) NSMutableArray * rooms;
@property (nonatomic, retain) NSMutableArray * devices;

-(void) LoadDevices:(id)sender;

@end
