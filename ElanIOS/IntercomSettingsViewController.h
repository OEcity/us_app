//
//  IntercomSettingsViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 10.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntercomSettingsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@end
