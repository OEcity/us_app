//
//  GuideRoomsListViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 29.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Room.h"

@interface GuideRoomsListViewController : UIViewController<NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController* rooms;
@end
