//
//  SYConfigurationMainViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/18/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYConfigurationMainViewController.h"
#import "SYFloorplanDetailViewController.h"
#import "AppDelegate.h"
@interface SYConfigurationMainViewController ()

@end

@implementation SYConfigurationMainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"embedContainer"]) {
        self.containerViewController = segue.destinationViewController;
    }else if ([segue.identifier isEqualToString:@"floorplanDetail"]) {
        ((SYFloorplanDetailViewController*)segue.destinationViewController).image = self.image;
        ((SYFloorplanDetailViewController*)segue.destinationViewController).floorplanURL = self.floorPlanURL;

    }
}
-(void)viewWillAppear:(BOOL)animated{
    [_configurationLabel setText:NSLocalizedString(@"floorPlans", nil)];
    [_floorplansLabel setText:NSLocalizedString(@"floorPlans", nil)];
    [_roomsLabel setText:NSLocalizedString(@"rooms", nil)];
    [_devicesLabel setText:NSLocalizedString(@"devices", nil)];
    [_assignLabel setText:NSLocalizedString(@"pairDevice", nil)];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncComplete:)
                                                 name:@"dataSyncComplete"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loadingStarted:)
                                                 name:@"loadingStarted"
                                               object:nil];

}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(IBAction)Sparovat:(id)sender{
    [self.rooms setBackgroundImage:[UIImage imageNamed:@"dlazdice_off"] forState:UIControlStateNormal];
    [self.devices setBackgroundImage:[UIImage imageNamed:@"dlazdice_off"] forState:UIControlStateNormal];
    [self.floorplan setBackgroundImage:[UIImage imageNamed:@"dlazdice_off"] forState:UIControlStateNormal];
    [self.sync setBackgroundImage:[UIImage imageNamed:@"dlazdice_off"] forState:UIControlStateNormal];

    [_floorplansImage setImage:[UIImage imageNamed:@"tabitem_floorplan_off"]];
    [_roomsImage setImage:[UIImage imageNamed:@"tabitem_mistnost_off"]];
    [_devicesImage setImage:[UIImage imageNamed:@"tabitem_zarizeni_off"]];
    [_assignImage setImage:[UIImage imageNamed:@"tabitem_sparovat_off"]];
    
    _floorplansImage.contentMode = UIViewContentModeScaleAspectFill;
    _roomsImage.contentMode = UIViewContentModeScaleAspectFill;
    _devicesImage.contentMode = UIViewContentModeScaleAspectFill;
    _assignImage.contentMode = UIViewContentModeScaleAspectFill;
    
    [self.devices setBackgroundImage:[UIImage imageNamed:@"dlazdice_off"] forState:UIControlStateNormal];
    [self.floorplan setBackgroundImage:[UIImage imageNamed:@"dlazdice_off"] forState:UIControlStateNormal];
    [self.sync setBackgroundImage:[UIImage imageNamed:@"dlazdice_off"] forState:UIControlStateNormal];

    UIButton* button = (UIButton*)sender;
    [button setBackgroundImage:[UIImage imageNamed:@"dlazdice_on"] forState:UIControlStateNormal];
    switch (((UIView*)sender).tag) {
        case 1:
            [self.containerViewController setViewController:@"floorPlan"];
                [_floorplansImage setImage:[UIImage imageNamed:@"tabitem_floorplan_on"]];
            _floorplansImage.contentMode = UIViewContentModeScaleAspectFill;
            [_configurationLabel setText:NSLocalizedString(@"floorPlans", nil)];

            break;
        case 2:
            [self.containerViewController setViewController:@"embedSecond"];
            [_configurationLabel setText:NSLocalizedString(@"rooms", nil)];

            [_roomsImage setImage:[UIImage imageNamed:@"tabitem_mistnost_on"]];
            _roomsImage.contentMode = UIViewContentModeScaleAspectFill;
            break;
        case 3:
            [self.containerViewController setViewController:@"embedFirst"];
            [_devicesImage setImage:[UIImage imageNamed:@"tabitem_zarizeni_on"]];
            _devicesImage.contentMode = UIViewContentModeScaleAspectFill;
            [_configurationLabel setText:NSLocalizedString(@"devices", nil)];

            break;
        case 4:
            [self.containerViewController setViewController:@"sparovatDevices"];
            [_assignImage setImage:[UIImage imageNamed:@"tabitem_sparovat_on"]];
            _assignImage.contentMode = UIViewContentModeScaleAspectFill;
            [_configurationLabel setText:NSLocalizedString(@"pairDevice", nil)];

            break;

        default:
            break;
    }
}

- (NSUInteger)supportedInterfaceOrientations
{

    return UIInterfaceOrientationMaskPortrait;
}

-(void)loadingStarted:(NSNotification *)note{
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];}


-(void)syncComplete:(NSNotification *)note{
    [_loaderDialog hide];
}

@end
