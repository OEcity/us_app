//
//  UniNavViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 21.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UniNavViewController : UINavigationController

@end
