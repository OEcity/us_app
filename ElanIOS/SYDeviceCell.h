//
// Created by Daniel Rutkovský on 12/06/15.
// Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SYDeviceCell : UITableViewCell

@property(nonatomic, weak) IBOutlet NSLayoutConstraint *cLockWidth;
@property(nonatomic, weak) IBOutlet NSLayoutConstraint *cBatteryWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cLockRightMargin;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cBatteryRightMargin;

@end