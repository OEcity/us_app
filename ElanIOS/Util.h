//
//  Util.h
//  O2archiv
//
//  Created by Vratislav Zima on 1/16/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataObjects.h"


@interface Util : NSObject  {
    NSString* serverAddress;
    NSString* socketAddress;
}



+(BOOL)hasConnectivity ;
+(NSString*) getServerAddress;
+(NSString*) getConfigUrl:(NSString*) key;
//+(NSString*) getAddressWith:(NSString*)suffix;
+(void) setServerAddress:(NSString*)address;
+(UIColor*)convertToUIColorRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue ;
+(UIImage*)determineImage:(Device*)device;
+(UIImage*)determineImageOn:(Device*)device;
+(UIImage*)determineImageGrey:(Device*)device;

+(NSDictionary*)generateNames;
+(NSDictionary*)serializeIRDevice:(IRDevice*)device;


@end
