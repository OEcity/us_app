//
//  ItemsResponse.h
//  O2archiv
//
//  Created by Vratislav Zima on 1/16/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"
@interface ItemsResponse : NSObject <Response>

    -(NSObject *)parseResponse:(NSData *)inputData identifier:(NSString *)identifier;

@end
