//
//  MotionJpegImageView.mm
//  VideoTest
//
//  Created by Matthew Eagar on 10/3/11.
//  Copyright 2011 ThinkFlood Inc. All rights reserved.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is furnished
// to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#import "MotionJpegImageView.h"


#pragma mark - Constants

#define END_MARKER_BYTES { 0xFF, 0xD9 }

static NSData *_endMarkerData = nil;

#pragma mark - Private Method Declarations

@interface MotionJpegImageView ()
- (void)cleanupConnection;

@end

#pragma mark - Implementation

@implementation MotionJpegImageView

@synthesize url = _url;
@synthesize username = _username;
@synthesize password = _password;
@synthesize type = _type;
@synthesize staticImage;
@synthesize allowSelfSignedCertificates = _allowSelfSignedCertificates;
@synthesize allowClearTextCredentials = _allowClearTextCredentials;
@dynamic isPlaying;

- (BOOL)isPlaying {
    return !(_connection == nil);
}

#pragma mark - Initializers

- (void) awakeFromNib{
    [self setup];
}

- (void) setup
{
    _url = nil;
    _receivedData = nil;
    _username = nil;
    _password = nil;
    _allowSelfSignedCertificates = NO;
    _allowClearTextCredentials = YES;
    
    if (_endMarkerData == nil) {
        uint8_t endMarker[2] = END_MARKER_BYTES;
        _endMarkerData = [[NSData alloc] initWithBytes:endMarker length:2];
    }
    
    self.contentMode = UIViewContentModeScaleToFill;
    
    [self.layer setCornerRadius:10.0f];
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        [self setup];
    }
    
    return self;
}

#pragma mark - Overrides


#pragma mark - Public Methods

- (void) getSingleImageWith: (NSString *) userName And: (NSString *) password And: (NSString *) type{
    if (_url) {
        self.username = userName;
        self.password = password;
        self.type = type;
        [self loadStaticImage];
    }

}

- (void)playWith: (NSString *) userName And: (NSString *) password And: (NSString *) type{
    if (_connection) {
        // continue
    }
    else if (_url) {
        self.username = userName;
        self.password = password;
        self.type = type;
        [self start];
    }
}

- (void)pause {
    if (_connection) {
        [_connection cancel];
        [self cleanupConnection];
    }else if(_staticJPGTimer){
        [_staticJPGTimer invalidate];
        [self cleanupConnection];
    }
}

- (void) start{
    
   // if ([_type isEqualToString:NSLocalizedString(@"camera_type_elko_ep", "iNELS cam")]) {
    //     _staticJPGTimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(loadStaticImage) userInfo:nil repeats:YES];
    //}else
    _connection = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:_url cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: 5] delegate:self];

}

- (void) startForIntercom{
    
    //    if ([_type isEqualToString:NSLocalizedString(@"camera_type_elko_ep", "iNELS cam")]) {
    _staticJPGTimer = [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(loadStaticImage) userInfo:nil repeats:YES];
    
    //    _connection = [[NSURLConnection alloc] initWithRequest:[NSURLRequest requestWithURL:_url cachePolicy: NSURLRequestUseProtocolCachePolicy timeoutInterval: 5] delegate:self];
}

- (void)loadStaticImage
{
    [self performSelectorInBackground:@selector(loadData) withObject:nil];
}

- (void)loadData
{
    NSString * URLAsString = [NSString stringWithFormat:@"%@", self.url];
    
    NSString * protocol = [URLAsString hasPrefix:@"https"] ? @"https://" : @"http://";
    
    NSString * URLWithoutProtocol = [URLAsString stringByReplacingOccurrencesOfString:protocol withString:@""];
    
    NSURL * staticImageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@:%@@%@",protocol,self.username,self.password,URLWithoutProtocol]];
    
    if (staticImageURL == nil) return;
    
    NSData * imageData = [NSData dataWithContentsOfURL:staticImageURL options:NSDataReadingUncached error:nil];
    UIImage * img = [UIImage imageWithData:imageData];
    
    if (img) {
        self.image = img;
    }
    
}

- (void)clear {
    self.image = nil;
}

- (void)stop {
    [self pause];
    [self clear];
}

#pragma mark - Private Methods

- (void)cleanupConnection {
    if (_connection) {
        _connection = nil;
    }
    
    if (_receivedData) {
        _receivedData = nil;
    }
    if (_staticJPGTimer) {
        _staticJPGTimer = nil;
    }
}

#pragma mark - NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    
    _receivedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [_receivedData appendData:data];
    
    NSRange endRange = [_receivedData rangeOfData:_endMarkerData 
                                          options:0 
                                            range:NSMakeRange(0, _receivedData.length)];
    
    long long endLocation = endRange.location + endRange.length;
    if (_receivedData.length >= endLocation) {
        NSData *imageData = [_receivedData subdataWithRange:NSMakeRange(0, endLocation)];
        UIImage *receivedImage = [UIImage imageWithData:imageData];
        if (receivedImage) {
            self.image = receivedImage;
            if([self.type isEqualToString:@"Custom"] && self.staticImage){
                [self pause];
            }
        }
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [self cleanupConnection];
}

-                    (BOOL)connection:(NSURLConnection *)connection 
canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
    BOOL allow = NO;
    if ([protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        allow = _allowSelfSignedCertificates;
    }
    else {
        allow = _allowClearTextCredentials;
    }
    
    return allow;
}

-                (void)connection:(NSURLConnection *)connection 
didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
    if ([challenge previousFailureCount] == 0 &&
        _username && _username.length > 0 &&
        _password && _password.length > 0) {
        NSURLCredential *credentials = 
            [NSURLCredential credentialWithUser:_username
                                       password:_password
                                    persistence:NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:credentials
               forAuthenticationChallenge:challenge];
    }
    else {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        [self cleanupConnection];
        
    }
}

- (BOOL)connectionShouldUseCredentialStorage:(NSURLConnection *)connection {
    return YES;
}

- (void)connection:(NSURLConnection *)connection 
  didFailWithError:(NSError *)error {
    [self cleanupConnection];
    
    [self start];
}

@end
