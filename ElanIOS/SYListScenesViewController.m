//
//  SYListScenesViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 7/30/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYListScenesViewController.h"
#import "SYCoreDataManager.h"
#import "SceneLoader.h"
#import "SYAddSceneViewController.h"
#import "AppDelegate.h"
#import "SYAPIManager.h"
#import "Constants.h"
@interface SYListScenesViewController (){}

@property (nonatomic, strong) NSFetchedResultsController* scenes;

@end

@implementation SYListScenesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
           }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    [self initFetchedResultsController];
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Scene"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _scenes = [[NSFetchedResultsController alloc]
              initWithFetchRequest:fetchRequest
              managedObjectContext:context
              sectionNameKeyPath:nil
              cacheName:nil];
    _scenes.delegate = self;
    NSError *error;
    
    if (![_scenes performFetch:&error]) {
        NSLog(@"error fetching Scenes: %@",[error description]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods
//- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
//    [self.tableView beginUpdates];
//}
//
//
//- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
//       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
//      newIndexPath:(NSIndexPath *)newIndexPath {
//    
//    UITableView *tableView = self.tableView;
//    
//    switch(type) {
//            
//        case NSFetchedResultsChangeInsert:
//            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
//                             withRowAnimation:UITableViewRowAnimationFade];
//            break;
//            
//        case NSFetchedResultsChangeDelete:
//            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
//                             withRowAnimation:UITableViewRowAnimationFade];
//            //            self.emptyLabel.hidden = [_fetchedResultsController.fetchedObjects count] > 0;
//            break;
//            
//        case NSFetchedResultsChangeUpdate:
//            [self configureCell:[_tableView cellForRowAtIndexPath:indexPath]
//                    atIndexPath:indexPath];
//            break;
//            
//        case NSFetchedResultsChangeMove:
//            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
//                             withRowAnimation:UITableViewRowAnimationFade];
//            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
//                             withRowAnimation:UITableViewRowAnimationFade];
//            break;
//    }
//}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
//    [self.tableView endUpdates];
    [self.tableView reloadData];
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Scene* scene =[self.scenes objectAtIndexPath:indexPath];
    
    UILabel * cellName  = (UILabel*)[cell viewWithTag:1];
    cellName.text = scene.label;

}


- (void)initLocalizableStrings {
    [_scenesLabel setText:NSLocalizedString(@"scenes", nil)];
    [_bottomRightButton setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
    [_bottomLeftButton setTitle:NSLocalizedString(@"menu", nil) forState:UIControlStateNormal];

}
-(void)tableView:(UITableView*)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    UIView * view1 = [cell viewWithTag:2];

    [UIView animateWithDuration:0.2f animations:^{
        CGRect theFrame = view1.frame;
        theFrame.size.width -= 50;
        view1.frame = theFrame;
        [view1 layoutIfNeeded];
    }];
    [UIView commitAnimations];

}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        Scene* scene = [self.scenes objectAtIndexPath:indexPath];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_scene", nil), scene.label==nil?@"":scene.label] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
        alert.tag = indexPath.row;
        [alert show];

    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
        self.deletedIndex = indexPath;
        NSString* name = ((Scene*)[self.scenes objectAtIndexPath:indexPath]).sceneID;
        Scene *scene = [self.scenes objectAtIndexPath:indexPath];
        if (name==nil){
            return;

        }
        
        [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
        [[SYAPIManager sharedInstance] deleteSceneWithID:name fromElan:scene.elan success:^(AFHTTPRequestOperation * operation, id object)
        {
            [_loaderDialog hide];
            
            
        } failure:^(AFHTTPRequestOperation * operation, NSError * error)
        {
            [_loaderDialog hide];
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString( @"",nil) message:NSLocalizedString(@"cannotDeleteScene",nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }];
    }
}


#pragma mark UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {


    return [self.scenes.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"MyCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    
    UIView *myBackView = [[UIView alloc] initWithFrame:cell.frame];
    myBackView.backgroundColor = USBlueColor;
    cell.selectedBackgroundView = myBackView;
    [self configureCell:cell atIndexPath:indexPath];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    _scene = [_scenes objectAtIndexPath:indexPath];

    [self performSegueWithIdentifier:@"addScene" sender:self];


    
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if (self.scene!=nil){
        SYAddSceneViewController* destViewController = (SYAddSceneViewController*)segue.destinationViewController;
            destViewController.scene = self.scene;
        destViewController.parentController = self;
    }else{
        SYAddSceneViewController* destViewController = (SYAddSceneViewController*)segue.destinationViewController;
        destViewController.scene = nil;
        destViewController.parentController = self;
    }

}




-(IBAction)nextStep:(id)sender{
    self.scene=nil;
    [self performSegueWithIdentifier:@"addScene" sender:self];

}
-(IBAction)back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}
@end
