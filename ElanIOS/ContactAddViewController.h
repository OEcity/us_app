//
//  ContactAddViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 11.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYCoreDataManager.h"


@interface ContactAddViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
@property(nonatomic, retain) IntercomContact*contact;

@end
