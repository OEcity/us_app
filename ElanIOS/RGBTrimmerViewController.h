//
//  TrimmerViewController.h
//  ElanIOS
//
//  Created by Vratislav Zima on 6/2/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//




@interface RGBTrimmerViewController : UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil value:(int)value maxValue:(int)maxValue_ color:(UIColor*)color_;

@property (nonatomic, retain) UIView * circleView;
@property (nonatomic, retain) UIColor * color;

@end
