//
//  HCASchedule+Dictionary.h
//  US App
//
//  Created by Tom Odler on 14.07.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//
#import "HeatTimeSchedule.h"
#import "SYSerializeProtocol.h"

@interface HeatTimeSchedule (Dictionary) <SYSerializeProtocol>

@end


