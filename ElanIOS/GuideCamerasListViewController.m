//
//  GuideCamerasListViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 07.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "GuideCamerasListViewController.h"
#import "SYCoreDataManager.h"
#import "GuideCameraAddViewController.h"

@interface GuideCamerasListViewController ()
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;
@property (nonatomic, retain)Camera *selectedCamera;

@end

@implementation GuideCamerasListViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initFetchedResultsController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"editCamera"]){
        GuideCameraAddViewController *controller = segue.destinationViewController;
        controller.camera = _selectedCamera;
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [[_cameras fetchedObjects] count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"roomLabel" forIndexPath:indexPath];
    
    UILabel *name = [[cell contentView]viewWithTag:1];
    Camera *camera = [_cameras objectAtIndexPath:indexPath];
    name.text = camera.label;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectedCamera = [_cameras objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"editCamera" sender:nil];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Camera * selectedCamera = [_cameras objectAtIndexPath:indexPath];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_camera", nil), selectedCamera.label] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil),nil];
        alert.tag = indexPath.row;
        [alert show];
        
    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"address = %@",[(Camera*)[_cameras objectAtIndexPath:[NSIndexPath indexPathForRow:alertView.tag inSection:0]] address]];
        NSArray *previous = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Camera" withPredicate:predicate sortDescriptor:nil inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        Camera* camera = [previous firstObject];
        [[SYCoreDataManager sharedInstance] deleteManagedObject:camera];
        
    }
}


#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Camera"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _cameras = [[NSFetchedResultsController alloc]
               initWithFetchRequest:fetchRequest
               managedObjectContext:context
               sectionNameKeyPath:nil
               cacheName:nil];
    _cameras.delegate = self;
    NSError *error;
    
    if (![_cameras performFetch:&error]) {
        NSLog(@"error fetching Cameras: %@",[error description]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            self.emptyLabel.hidden = [_cameras.fetchedObjects count] > 0;
            
            break;
            
        case NSFetchedResultsChangeUpdate:{
            NSMutableArray *indexPaths = [NSMutableArray new];
            [indexPaths addObject:indexPath];
            [tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
        }
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
    self.emptyLabel.hidden = [_cameras.fetchedObjects count] > 0;
}

@end
