//
//  SYAPIManager.m
//  iHCMirf
//
//  Created by Vratislav Zima on 7/14/14.
//  Copyright (c) 2014 Quiche. All rights reserved.
//

#import "SYAPIManager.h"
#import "Util.h"
#import "State.h"
#import "Device+State.h"
#import "SYCoreDataManager.h"

#define MAX_CONCURENT_REQUESTS 1
#define REQUEST_TIMEOUT 20
@implementation SYAPIManager

+(SYAPIManager*) sharedInstance{
    
    static SYAPIManager *_sharedManager = nil;
    static dispatch_once_t oncePredicate;

    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[SYAPIManager alloc] init];
    });
    
    return _sharedManager;
}

- (id)init
{
    self = [super init];
    if (self) {
        _manager = _manager = [[AFHTTPRequestOperationManager alloc] init];
         [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
         [_manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
         _manager.requestSerializer.timeoutInterval = REQUEST_TIMEOUT;
         _manager.operationQueue.maxConcurrentOperationCount = MAX_CONCURENT_REQUESTS;
    }
    return self;
}

-(void)reinitWithUrl:(NSURL*)url{
    _manager = [_manager initWithBaseURL:url];
    [_manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [_manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    _manager.requestSerializer.timeoutInterval = REQUEST_TIMEOUT;
    _manager.operationQueue.maxConcurrentOperationCount = MAX_CONCURENT_REQUESTS;
}

-(void)cancelAllOperations{
    
    [_manager.operationQueue cancelAllOperations];
}

#pragma mark WEATHER methods

-(void)postWeatherCoordinates:(NSDictionary*)data toElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                      failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    
    [_manager POST:@"weather" parameters:data success:success failure:failure];
}

-(void)getWeatherFromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                      failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];

    [_manager GET:@"weather" parameters:nil success:success failure:failure];
}

-(void)deleteWeatherFromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    [_manager DELETE:@"weather" parameters:nil success:success failure:failure];
}

#pragma mark Room api methods
-(void)getRoomsWithSuccessFromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                                  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];

    [_manager GET:@"rooms" parameters:nil success:success failure:failure];
}

-(void)getRoomWithID:(NSString *) ID fromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
   [self reinitWithUrl:baseURL];
    
    [_manager GET:[NSString stringWithFormat:@"rooms/%@", ID ] parameters:nil success:success failure:failure];
    
}
-(void)deleteRoom:(Room*)room success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[room.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    [_manager DELETE:[NSString stringWithFormat:@"rooms/%@", room.roomID ] parameters:nil success:success failure:failure];
}

-(void)createRoomWithDictionary:(NSDictionary*)roomDictionary toElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    [_manager POST:@"rooms" parameters:roomDictionary success:success failure:failure];
    
}

-(void)updateRoomWithDictionary:(NSDictionary*)roomDictionary toElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    NSLog(@"Dictionary: %@, url: %@", roomDictionary,[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]);
    
    [_manager POST:@"rooms" parameters:roomDictionary success:success failure:failure];
    
}

-(void) saveIRActionWithSuccessToElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    _manager.requestSerializer.timeoutInterval = 20;
    [_manager POST:[NSString stringWithFormat:@"ir"] parameters:nil success:success failure:failure];
}

-(void) saveIRAction:(NSString*)id success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [_manager POST:[NSString stringWithFormat:@"ir/%@",id] parameters:nil success:success failure:failure];
}


#pragma mark Device api methods
-(void)getDevicesWithSuccessForElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    
    [self reinitWithUrl:baseURL];
    [_manager GET:@"devices" parameters:nil success:success failure:failure];
    
}

-(void)getDeviceWithID:(NSString *) ID fromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager GET:[NSString stringWithFormat:@"devices/%@", ID ] parameters:nil success:success failure:failure];
    
}

-(void)deleteDevice:(Device*)device success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[device.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    [_manager DELETE:[NSString stringWithFormat:@"devices/%@", device.deviceID ] parameters:nil success:success failure:failure];
    
}

-(void)deleteDeviceScheduleWithId:(NSString*)id fromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager DELETE:[NSString stringWithFormat:@"automat/schedules/%@", id ] parameters:nil success:success failure:failure];
    
}

-(void)getDeviceState:(NSString *) device fromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                 failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager GET:[NSString stringWithFormat:@"devices/%@/state", device] parameters:nil success:success failure: failure];
    
}

-(void)createDeviceWithLabel:(NSString *) label deviceType:(NSString*)deviceType productType:(NSString*)productType address:(NSNumber*)address toElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                      failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSDictionary * deviceInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 productType,@"product type",
                                 deviceType, @"type",
                                 label, @"label",
                                 address, @"address", nil];
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager POST:@"devices" parameters:[[NSDictionary alloc] initWithObjectsAndKeys:deviceInfo,@"device info", nil] success:success failure: failure];
    
}

-(void)updateDeviceWithLabel:(NSString *) label deviceType:(NSString*)deviceType productType:(NSString*)productType address:(NSNumber*)address id:(NSString*)deviceID toElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSDictionary * deviceInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 productType,@"product type",
                                 deviceType, @"type",
                                 label, @"label",
                                 address, @"address", nil];
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    [_manager POST:@"devices" parameters:[[NSDictionary alloc] initWithObjectsAndKeys:deviceInfo,@"device info", deviceID, @"id", nil] success:success failure: failure];
    
}

- (void)putDeviceAction:(NSDictionary*)action device:(Device*)device

                success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[device.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager PUT:[NSString stringWithFormat:@"devices/%@", device.deviceID ] parameters:action success:success failure:failure];
}

-(void)putIRDeviceAction:(NSDictionary *)action deviceID:(NSString *)deviceID toElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *, id))success failure:(void (^)(AFHTTPRequestOperation *, NSError *))failure{
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    [_manager PUT:[NSString stringWithFormat:@"devices/%@", deviceID ] parameters:action success:success failure:failure];
}

-(void) sendIRDevice:(NSString *) label deviceType:(NSString*)deviceType productType:(NSString*)productType success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    NSDictionary * deviceInfo = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 productType,@"product type",
                                 deviceType, @"type",
                                 label, @"label",nil];
    
    [_manager POST:[NSString stringWithFormat:@"devices"] parameters: [[NSDictionary alloc] initWithObjectsAndKeys: deviceInfo, @"device info", nil] success:success failure:failure];
}

-(void)updateIRDeviceWithDict:(NSDictionary*)dict toElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    
    [self reinitWithUrl:baseURL];
    
    [_manager POST:@"devices" parameters:dict success:success failure:failure];
}

-(void) deleteIRDeviceWithID:(Device*)dev success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[dev.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager DELETE:[NSString stringWithFormat:@"devices/%@", dev.deviceID ] parameters:nil success:success failure:failure];
    
}

#pragma Device State methods

-(void)updateDeviceStateWithDictionary:(NSDictionary *) state device:(Device*)device success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[device.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager PUT:[NSString stringWithFormat:@"devices/%@", device.deviceID ] parameters:state success:success failure:failure];
    
}

#pragma mark Scenes api methods

-(void) getScenesWithSuccessFromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager GET:@"scenes" parameters:nil success:success failure:failure];
    
}

-(void)getSceneWithID:(NSString *) ID fromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
             failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager GET:[NSString stringWithFormat:@"scenes/%@", ID ] parameters:nil success:success failure:failure];
    
}

-(void)deleteSceneWithID:(NSString*)id fromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                       failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    [_manager DELETE:[NSString stringWithFormat:@"scenes/%@", id ] parameters:nil success:success failure:failure];
     
}

-(void)performScene:(Scene*)scene success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                   failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[scene.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager PUT:[NSString stringWithFormat:@"scenes/%@", scene.sceneID ] parameters:nil success:success failure:failure];
}

-(void) postSceneWithID:(NSString*)id withDict:(NSDictionary*)data success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [_manager POST:[NSString stringWithFormat:@"scenes"] parameters:data success:success failure:failure];
    
}


#pragma mark Update Temperature API functions

-(void)postTemperatureForDevice:(Device*)device temperature:(NSNumber*) temperature
                success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[device.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    NSDictionary * dict = [[NSDictionary alloc] initWithObjectsAndKeys:temperature,@"requested temperature", nil];
    

    [_manager PUT:[NSString stringWithFormat:@"devices/%@", device.deviceID] parameters:dict success:success failure:failure];
}

-(void)postCorrectionForDevice:(Device*)device correction:(NSNumber*)correction
                        success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[device.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    NSDictionary * dict = [[NSDictionary alloc] initWithObjectsAndKeys:correction,@"correction", nil];
    
    
    [_manager PUT:[NSString stringWithFormat:@"devices/%@", device.deviceID] parameters:dict success:success failure:failure];
}


-(void)postModeForDevice:(Device*)device
                       success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                       failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[device.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    NSDictionary * dict = [[NSDictionary alloc] initWithObjectsAndKeys:[device getStateValueForStateName:@"mode"],@"mode", nil];
    
    
    [_manager PUT:[NSString stringWithFormat:@"devices/%@", device.deviceID] parameters:dict success:success failure:failure];
}

-(void)postControllForDevice:(Device*)device
                     success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[device.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    NSDictionary * dict = [[NSDictionary alloc] initWithObjectsAndKeys:[device getStateValueForStateName:@"controll"],@"controll", nil];
    
    
    [_manager PUT:[NSString stringWithFormat:@"devices/%@", device.deviceID] parameters:dict success:success failure:failure];
}


-(void)postWindowSensitivityForDevice:(Device*)device sensitivity:(NSNumber*)sensitivity
                        success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[device.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    NSDictionary * dict = [[NSDictionary alloc] initWithObjectsAndKeys:sensitivity,@"open window sensitivity", nil];
    
    
    [_manager PUT:[NSString stringWithFormat:@"devices/%@", device.deviceID] parameters:dict success:success failure:failure];
}

-(void)postWindowOffTimeForDevice:(Device*)device offtime:(NSNumber*)offtime
                              success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[device.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    NSDictionary * dict = [[NSDictionary alloc] initWithObjectsAndKeys:offtime,@"open window off time", nil];
    
    
    [_manager PUT:[NSString stringWithFormat:@"devices/%@", device.deviceID] parameters:dict success:success failure:failure];
}

#pragma mark TEMPERATURE Schedule

-(void)getTemperatureSchedulesWithSuccessFromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                                  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager GET:@"temperature/schedules" parameters:nil success:success failure:failure];
    
}


-(void) getDeviceSchedulesWithSuccessFromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager GET:@"automat/schedules" parameters:nil success:success failure:failure];
    
}

-(void)getDeviceScheduleDetailsWithName:(NSString*)name fromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager GET:[NSString stringWithFormat:@"automat/schedules/%@", name ] parameters:nil success:success failure:failure];
}

-(void)getScheduleWithID:(NSString *) ID fromElan:(Elan*)elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager GET:[NSString stringWithFormat:@"temperature/schedules/%@", ID ] parameters:nil success:success failure:failure];
    
}


-(void)deleteTemperatureScheduleWithId:(NSString*)id success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                                    failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [_manager DELETE:[NSString stringWithFormat:@"temperature/schedules/%@", id ] parameters:nil success:success failure:failure];
    
}

#pragma mark Central source

-(void)getCentralSourceWithSuccess:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                                  failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [_manager GET:@"temperature/sources" parameters:nil success:success failure:failure];
    
}

-(void)getTemperatureSourceDetailsWithName:(NSString*)name success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    
    [_manager GET:[NSString stringWithFormat:@"temperature/sources/%@", name ] parameters:nil success:success failure:failure];
}


-(void)postDataWithTemperatureSources:(NSDictionary*)TemperatureSourceSettings
                        success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [_manager POST:@"temperature/sources" parameters:TemperatureSourceSettings success:success failure:failure];
    
}

-(void)deleteTemperatureSourceWithId:(NSString*)id
                              success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [_manager DELETE:[NSString stringWithFormat:@"temperature/sources/%@", id ] parameters:nil success:success failure:failure];
    
}


#pragma mark Heating/Cooling area

-(void)postDataWithHeatCoolAreaToElan:(Elan*)elan dict:(NSDictionary*)HeatCoolAreaSettings
                       success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                       failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    
    [_manager POST:@"devices" parameters:HeatCoolAreaSettings success:success failure:failure];
}


#pragma mark Configuration API
-(void)getLimitsConfig:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                           failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [_manager GET:@"configuration/limits" parameters:nil success:success failure:failure];
    
}

-(void)getConfigForElan:(Elan*)elan
        success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
        failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    _manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
    
    [_manager GET:@"configuration" parameters:nil success:success failure:failure];
    
}

#pragma mark Device Actions
-(void)postDeviceActionWithDictionary:(NSDictionary*)deviceAction device:(Device*)device success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[device.elan baseAddress]]];
    [self reinitWithUrl:baseURL];
    [_manager PUT:[NSString stringWithFormat:@"devices/%@", device.deviceID] parameters:deviceAction success:success failure:failure];
    
}

-(void)postIRDeviceAction:(NSDictionary*)deviceAction devicename:(NSString*)deviceName elan:(Elan*)Elan success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    [_manager POST:[NSString stringWithFormat:@"devices/%@", deviceName] parameters:deviceAction success:success failure:failure];
    
}


#pragma mark fetch /API
-(void)getAPIRootWithSuccessForElan:(Elan*)elan
                    success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                     failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure

{
    NSURL *baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/api/",[elan baseAddress]]];
    [self reinitWithUrl:baseURL];

    [_manager GET:@"" parameters:nil success:success failure:failure];    
}

-(void)getAPIRootForURL:(NSURL*)url success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    _manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [_manager GET:@"api" parameters:nil success:success failure:failure];
    _manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    NSURL *baseURL = [NSURL URLWithString:[Util getServerAddress]];
    _manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];


}

-(void)getFWFromUpdateServer:(NSURL*)url success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure
{
    _manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:url];
    [_manager GET:@"" parameters:nil success:success failure:failure];


}


#pragma mark Schedule api
-(void)postScheduleWithDictionary:(NSDictionary*)scheduleDictionary  success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    
    [_manager POST:@"temperature/schedules" parameters:scheduleDictionary success:success failure:failure];

}

-(void)postDeviceScheduleWithDictionary:(NSDictionary*)scheduleDictionary  success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                                failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    
    [_manager POST:@"automat/schedules" parameters:scheduleDictionary success:success failure:failure];
    
}



#pragma mark Central Source api
-(void)postCentralSourceWithDictionary:(NSDictionary*)sourceDictionary  success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                          failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    
    [_manager POST:@"temperature/sources" parameters:sourceDictionary success:success failure:failure];

}

-(void)deleteCentralSourceWithId:(NSString*)sourceName  success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    
    [_manager DELETE:[NSString stringWithFormat:@"temperature/sources/%@", sourceName] parameters:nil success:success failure:failure];
    
}

#pragma get data from url
-(void)getDataFromURL:(NSString*)url  success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
                               failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    manager.requestSerializer.timeoutInterval = REQUEST_TIMEOUT;
    manager.operationQueue.maxConcurrentOperationCount = MAX_CONCURENT_REQUESTS;
    [manager GET:url parameters:nil success:success failure:failure];
}

-(void)getDataFromURL:(NSString*)url user: (NSString*) user password: (NSString*) password  success:(void (^)(AFHTTPRequestOperation *operation, id  response))success
              failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failure{
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] init];
    
    
    NSString * basicAuthCredentials = [NSString stringWithFormat:@"%@:%@", user, password];
    NSString * value = [NSString stringWithFormat:@"Basic %@", AFBase64EncodedStringFromString(basicAuthCredentials)];
    
    [manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    [manager setResponseSerializer:[AFJSONResponseSerializer serializer]];
    [manager.requestSerializer setValue:value forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer.timeoutInterval = REQUEST_TIMEOUT;
    manager.operationQueue.maxConcurrentOperationCount = MAX_CONCURENT_REQUESTS;
    [manager GET:url parameters:nil success:success failure:failure];
}

static NSString * AFBase64EncodedStringFromString(NSString *string) {
    NSData *data = [NSData dataWithBytes:[string UTF8String] length:[string lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
    NSUInteger length = [data length];
    NSMutableData *mutableData = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    
    uint8_t *input = (uint8_t *)[data bytes];
    uint8_t *output = (uint8_t *)[mutableData mutableBytes];
    
    for (NSUInteger i = 0; i < length; i += 3) {
        NSUInteger value = 0;
        for (NSUInteger j = i; j < (i + 3); j++) {
            value <<= 8;
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        static uint8_t const kAFBase64EncodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        
        NSUInteger idx = (i / 3) * 4;
        output[idx + 0] = kAFBase64EncodingTable[(value >> 18) & 0x3F];
        output[idx + 1] = kAFBase64EncodingTable[(value >> 12) & 0x3F];
        output[idx + 2] = (i + 1) < length ? kAFBase64EncodingTable[(value >> 6)  & 0x3F] : '=';
        output[idx + 3] = (i + 2) < length ? kAFBase64EncodingTable[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:mutableData encoding:NSASCIIStringEncoding];
}

static NSString * const kAFCharactersToBeEscapedInQueryString = @":/?&=;+!@#$()',*";



@end
