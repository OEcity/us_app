//
//  HeatTimeScheduleView.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 1/23/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TempScheduleDay.h"
#import "TempDayMode.h"
@interface HeatTimeScheduleView : UIView <UIGestureRecognizerDelegate>


@property (nonatomic, retain) TempScheduleDay * mDay;
@property (nonatomic, retain) id parentViewController;
-(id)initWithFrameAndDay:(CGRect)frame tempScheduleDay:(TempScheduleDay*)day topOffset:(int)topOffset parentViewController:(id)viewController;
-(void) addMode:(TempDayMode*) mode position:(int) position;
-(void) setDay:(TempScheduleDay*) day;
-(TempScheduleDay*) getDay ;
-(void) deleteMode:(int) result;

@end
