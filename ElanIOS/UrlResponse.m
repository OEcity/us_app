//
//  UrlResponse.m
//  iHC-MIRF
//
//  Created by Vlastimil Venclik on 05.09.13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "UrlResponse.h"
#import "SBJson.h"
@implementation UrlResponse

-(NSObject*) parseResponse:(id )inputData identifier:(NSString *)identifier{
    
    SBJsonParser *parser2 = [[SBJsonParser alloc] init];
    NSDictionary* jsonObjects;
    if ([inputData isKindOfClass:[NSData class]]){
        NSString *inputString = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];
        
        jsonObjects = [parser2 objectWithString:inputString];
    }else{
        jsonObjects = (NSDictionary*)inputData;
    }
    
    NSString* ws = [jsonObjects valueForKey:@"notifications"];
    NSString* room = [jsonObjects valueForKey:@"rooms"];
    NSString* devices = [jsonObjects valueForKey:@"devices"];
    NSString* configuration = [jsonObjects valueForKey:@"configuration"];
    NSString* floorplans = [jsonObjects valueForKey:@"floorplans"];
    NSString* icons = [jsonObjects valueForKey:@"icons"];
    NSString* scenes = [jsonObjects valueForKey:@"scenes"];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:ws forKey:@"notification"];
    [defaults setObject:room forKey:@"rooms"];
    [defaults setObject:devices forKey:@"devices"];
    [defaults setObject:configuration forKey:@"configuration"];
    [defaults setObject:floorplans forKey:@"floorplans"];
    [defaults setObject:icons forKey:@"icons"];
    [defaults setObject:scenes forKey:@"scenes"];
    
    [defaults synchronize];
    return  nil;
    
}

@end
