//
//  RoomTypeLoader.h
//  iHC-MIRF
//
//  Created by Vlastimil Venclik on 21.11.13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "URLConnectionListener.h"
#import "URLConnector.h"

@interface RoomTypeLoader : NSObject <URLConnectionListener>


@property (nonatomic, retain) NSMutableArray * types;
@property (nonatomic, retain) URLConnector * url;
-(void)LoadTypes;

@end
