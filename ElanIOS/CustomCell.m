//
//  CustomCell.m
//  Click Smart
//
//  Created by Tom Odler on 26.07.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "CustomCell.h"
#import "SYCoreDataManager.h"
#import "SYAPIManager.h"
#import "Device.h"
#import "Constants.h"
#import "Room.h"



@implementation CustomCell
@synthesize dataArray; //array to hold submenu data


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
        _tableView = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain]; //create tableview a
        _tableView.scrollEnabled = false;
        _tableView.tag = 100;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorColor = [UIColor clearColor];
        [self addSubview:_tableView]; // add it cell
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)layoutSubviews
{
    [super layoutSubviews];
//    UITableView *subMenuTableView =(UITableView *) [self viewWithTag:100];
    _tableView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    _tableView.backgroundColor = [UIColor blackColor];
    _tableView.layer.borderColor = USBlueColor.CGColor;
    _tableView.layer.borderWidth = 1.0f;
    //set the frames for tableview
    
}

//manage datasource and  delegate for submenu tableview
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[SYCoreDataManager sharedInstance] getAllDevicesForElan:_room.elan].count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellID"];
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellID"];
    }
    cell.backgroundColor = [UIColor blackColor];
    CGRect frame = CGRectMake(210, 15   , 15, 15);
    UIImageView *checkImg = [[UIImageView alloc]initWithFrame:frame];
    checkImg.tag = 50;
    for(UIView *subView in cell.subviews){
        if(subView.tag == 50)
            [subView removeFromSuperview];
    }
    [cell addSubview:checkImg];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = USBlueColor;
    [cell setSelectedBackgroundView:bgColorView];
    
    cell.backgroundColor = [UIColor blackColor];
    
    Device *myDevice = [[[SYCoreDataManager sharedInstance] getAllDevicesForElan:_room.elan] objectAtIndex:indexPath.row];
    
    if(_added != nil){
        if([_added containsObject:myDevice.deviceID]){
            checkImg.image = [UIImage imageNamed:@"fajfka_vyber_prvku.png"];
        } else {
            checkImg.image = [UIImage imageNamed:@"krizek_vyber_prvku.png"];
        }
    }
    
    
    cell.textLabel.text = myDevice.label;
    cell.textLabel.textColor = [UIColor whiteColor];
    

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
        Device *myDevice = [[[SYCoreDataManager sharedInstance] getAllDevicesForElan:_room.elan] objectAtIndex:indexPath.row];
    if([_added containsObject:myDevice.deviceID]){
        [_added removeObject:myDevice.deviceID];
    } else {
        [_added addObject:myDevice.deviceID];
    }
    [_tableView reloadData];
}
-(void) getRoomInCell{
    NSLog(@"Got room %@", _room.label);
    self.added = [[NSMutableArray alloc] init];

    for (DeviceInRoom * deviceInRoom in _room.devicesInRoom){
        [self.added addObject:[deviceInRoom.device.deviceID copy]];
        NSLog(@"device: %@", deviceInRoom.device.label);
    }
    [_tableView reloadData];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,0,300,60)];
    
    // create the label object
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.frame = CGRectMake(0,0,_tableView.frame.size.width,60);
    headerLabel.backgroundColor = [UIColor blackColor];
    headerLabel.font = [UIFont fontWithName:@"Roboto-Thin" size:18];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(elementsTapped)];
    [customView addGestureRecognizer:singleFingerTap];
    
    UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectZero];
    arrow.frame = CGRectMake(_tableView.frame.size.width - 30,20,20,20);
    arrow.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
    
    headerLabel.text = @"Elements";
    
    if(_room)
        headerLabel.text = _room.label;
    
    headerLabel.textColor = [UIColor whiteColor];
    
    [customView addSubview:headerLabel];
    [customView addSubview:arrow];
    
    return customView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
        return 50;
}

-(void)elementsTapped{
    [_parentController reloadTableView];
}

-(void)saveRoomWithDevices{
    for(NSString*device in _added){
        NSLog(@"Device: %@", device);
    }
    
    [[SYCoreDataManager sharedInstance] unpairAllDevicesForRoom:self.room];
    
    for (NSString * deviceID in self.added){
        Device *myDevice = [[SYCoreDataManager sharedInstance] getDeviceWithID:deviceID];
        [[SYCoreDataManager sharedInstance] pairDevice:myDevice andRoom:self.room withCoordinates:[self getCoordsFor:myDevice]];
    }
    
    NSDictionary * dict = [[[SYCoreDataManager sharedInstance] getRoomWithID:self.room.roomID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]] serializeToDictionary];
    
    
    [_parentController.loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    [[SYAPIManager sharedInstance] updateRoomWithDictionary:dict toElan:_room.elan success:^(AFHTTPRequestOperation * operation, id object)
     {
         [_parentController.loaderDialog hide];
         
         
     }failure:^(AFHTTPRequestOperation * operation, NSError * error)
     {
         [_parentController.loaderDialog hide];
         UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Could not pair devices. Please retry again later", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];

}
-(struct SYDevicePosition)getCoordsFor:(Device*)device{
    struct SYDevicePosition position;
    NSMutableArray * result = [[NSMutableArray alloc] init];
    NSNumber* val1 = [NSNumber numberWithFloat:0] ;
    [result addObject:val1];
    NSNumber* val2 =[NSNumber numberWithFloat:0];
    
    [result addObject:val2];
    
    return position;
}
@end
