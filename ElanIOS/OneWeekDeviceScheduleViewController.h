//
//  OneWeekDeviceScheduleViewController.h
//  iHC-MIRF
//
//  Created by admin on 10.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SYHeatContainerViewController.h"
//#import "SYContainerViewController.h"
#import "OneDayDeviceTimeScheduleView.h"
#import "OneDayDeviceTimeAdjustView.h"

#import "AddDeviceModeViewController.h"

//změnit při přeimportu
#import "Click Smart-Swift.h"

@interface OneWeekDeviceScheduleViewController : UIViewController<OneDayDeviceTimeScheduleViewDataSource, OneDayDeviceTimeAdjustViewDataSource, AddDeviceModeDelegate, OneDayDeviceTimeAdjustViewDelegate>

//@property (nonatomic, retain) SYHeatContainerViewController * container;
@property (nonatomic, retain) NSMutableDictionary * schedule;


//@property (nonatomic, retain) SYContainerViewController *containerViewController;

-(void)putEditingOnScreen:(TempDayMode*)mode modePos:(int)modePos dayInWeek:(int)day;

@end
