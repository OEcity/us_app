//
//  HeatTimeSchedule+CoreDataClass.h
//  
//
//  Created by Tom Odler on 07.11.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Elan, HeatCoolArea;

NS_ASSUME_NONNULL_BEGIN

@interface HeatTimeSchedule : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "HeatTimeSchedule+CoreDataProperties.h"
