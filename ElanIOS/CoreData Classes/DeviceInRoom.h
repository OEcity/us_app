//
//  DeviceInRoom.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 16.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Device, Room;

@interface DeviceInRoom : NSManagedObject

@property (nonatomic, retain) NSNumber * coordX;
@property (nonatomic, retain) NSNumber * coordY;
@property (nonatomic, retain) Device *device;
@property (nonatomic, retain) Room *room;

@end
