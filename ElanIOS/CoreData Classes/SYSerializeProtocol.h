//
//  SYSerializeProtocol.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 18.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SYSerializeProtocol <NSObject>

@required
/**
 Update managedObject with dictionary recieved from API. If update needed (data from APi are different from data in database), return value is YES and data in object updated. Otherwise, return value is NO.
 */
- (BOOL)updateWithDictionary:(NSDictionary*)dict;

@optional
/**
 Creates NSDictionary for the API POST request.
 */
- (NSDictionary*)serializeToDictionary;

@end
