
//
//  Action+DictUpdate.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 18.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Action+DictUpdate.h"

@implementation Action (DictUpdate)

- (BOOL)updateWithDictionary:(NSDictionary *)dict {
    BOOL updated = NO;
    
    NSString* parsedType = [dict objectForKey:@"type"];
    NSNumber* parsedIR = [dict objectForKey:@"ir LED"];
    
    if ([parsedType isKindOfClass:[NSNull class]] && [parsedIR isKindOfClass:[NSNull class]]){
        return NO;
    }
    
    if(parsedIR != nil){
        updated = ([self updateTypeNumber:dict] || updated);
    }
    
    if([parsedType isKindOfClass: [NSNull class]])
        return updated;
    
    if (![self.type isEqualToString:parsedType]) {
        self.type = parsedType;
        updated = YES;
    }
    
    if ([parsedType isEqualToString:@"int"] || [parsedType isEqualToString:@"number"]) {
        updated = ([self updateTypeNumber:dict] || updated);
    }
    return updated;
}

- (BOOL)updateWithDictionary:(NSDictionary *)dict andName:(NSString*)name {
    BOOL updated = NO;
    
    if (![self.name isEqualToString:name]) {
        self.name = name;
        updated = YES;
    }
    
    updated = ([self updateWithDictionary:dict] || updated);
    
    return updated;
}

#pragma mark - Helper methods

- (BOOL)updateTypeNumber:(NSDictionary*)dict {
    BOOL updated = NO;
    
    NSNumber* min = [dict objectForKey:@"min"];
    
    if(min == nil)
        min = [dict objectForKey:@"ir LED"];
    
    NSNumber* max = [dict objectForKey:@"max"];
    NSNumber* step = [dict objectForKey:@"step"];
    
    if (![self.min isEqualToNumber:min]) {
        self.min = min;
        updated = YES;
    }
    
    if( max == nil && step == nil)
        return updated;
    
    if (![self.max isEqualToNumber:max]) {
        self.max = max;
        updated = YES;
    }
    
    if (![self.step isEqualToNumber:step]) {
        self.step = step;
        updated = YES;
    }
    
    return updated;
}

@end
