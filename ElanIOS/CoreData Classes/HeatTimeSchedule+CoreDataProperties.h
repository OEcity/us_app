//
//  HeatTimeSchedule+CoreDataProperties.h
//  
//
//  Created by Tom Odler on 07.11.16.
//
//

#import "HeatTimeSchedule.h"


NS_ASSUME_NONNULL_BEGIN

@interface HeatTimeSchedule (CoreDataProperties)

+ (NSFetchRequest<HeatTimeSchedule *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *heattimescheduleID;
@property (nullable, nonatomic, copy) NSNumber *hysteresis;
@property (nullable, nonatomic, copy) NSString *label;
@property (nullable, nonatomic, copy) NSNumber *temp1;
@property (nullable, nonatomic, copy) NSNumber *temp2;
@property (nullable, nonatomic, copy) NSNumber *temp3;
@property (nullable, nonatomic, copy) NSNumber *temp4;
@property (nullable, nonatomic, retain) Elan *elan;
@property (nullable, nonatomic, retain) HeatCoolArea *inSchedule;

@end

NS_ASSUME_NONNULL_END
