//
//  Elan+Dictionary.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 20.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Elan+Dictionary.h"

@implementation Elan (Dictionary)

- (BOOL)updateWithDictionary:(NSDictionary *)dict {
    BOOL updated = NO;
    
    NSDictionary* info = [dict objectForKeyedSubscript:@"info"];
    NSString* parsedVersion = [info objectForKey:@"ELAN-RF version"];
    NSString* parsedApiVersion = [info objectForKey:@"API version"];
    NSString* parsedMAC = [info objectForKey:@"MAC address"];
    
    if ([self.version isEqualToString:parsedVersion]) {
        self.version = parsedVersion;
        updated = YES;
    }
    
    if ([self.apiVersion isEqualToString:parsedApiVersion]) {
        self.apiVersion = parsedApiVersion;
        updated = YES;
    }
    
    if ([self.mac isEqualToString:parsedMAC]) {
        self.mac = parsedMAC;
        updated = YES;
    }
    
    return updated;
}

@end
