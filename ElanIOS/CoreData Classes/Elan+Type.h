//
//  Elan+Type.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 29.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Elan.h"

@interface Elan (Type)

- (void)parseTypeAndVersion:(NSDictionary*)dict;

@end
