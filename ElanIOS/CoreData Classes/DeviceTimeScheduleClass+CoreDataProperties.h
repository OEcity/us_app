//
//  DeviceTimeScheduleClass+CoreDataProperties.h
//  
//
//  Created by Tom Odler on 27.10.16.
//
//

#import "DeviceTimeScheduleClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DeviceTimeScheduleClass (CoreDataProperties)

+ (NSFetchRequest<DeviceTimeScheduleClass *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *devicetimescheduleclassID;
@property (nullable, nonatomic, copy) NSString *label;
@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, retain) NSSet<Device *> *devices;
@property (nullable, nonatomic, retain) Elan *elan;

@end

@interface DeviceTimeScheduleClass (CoreDataGeneratedAccessors)

- (void)addDevicesObject:(Device *)value;
- (void)removeDevicesObject:(Device *)value;
- (void)addDevices:(NSSet<Device *> *)values;
- (void)removeDevices:(NSSet<Device *> *)values;

@end

NS_ASSUME_NONNULL_END
