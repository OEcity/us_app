//
//  Elan+CoreDataProperties.m
//  
//
//  Created by Tom Odler on 26.10.16.
//
//

#import "Elan+CoreDataProperties.h"

@implementation Elan (CoreDataProperties)

+ (NSFetchRequest<Elan *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Elan"];
}

@dynamic apiVersion;
@dynamic baseAddress;
@dynamic configurationCounter;
@dynamic firmware;
@dynamic label;
@dynamic mac;
@dynamic selected;
@dynamic state;
@dynamic type;
@dynamic update;
@dynamic version;
@dynamic socketAddress;
@dynamic addresses;
@dynamic cameras;
@dynamic devices;
@dynamic irDevices;
@dynamic rooms;
@dynamic scenes;

@end
