//
//  IntercomContact+CoreDataProperties.m
//  
//
//  Created by Tom Odler on 12.10.16.
//
//

#import "IntercomContact+CoreDataProperties.h"

@implementation IntercomContact (CoreDataProperties)

+ (NSFetchRequest<IntercomContact *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"IntercomContact"];
}

@dynamic ihcDevice;
@dynamic label;
@dynamic sipName;
@dynamic ipAddress;
@dynamic username;
@dynamic password;
@dynamic switchCode;

@end
