//
//  HeatCoolArea+CoreDataProperties.h
//  
//
//  Created by Tom Odler on 13.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "HeatCoolArea.h"

NS_ASSUME_NONNULL_BEGIN

@interface HeatCoolArea (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *schedule;
@property (nullable, nonatomic, retain) NSNumber *temperatureOffset;
@property (nullable, nonatomic, retain) NSSet<Device *> *centralSources;
@property (nullable, nonatomic, retain) NSSet<Device *> *coolingDevices;
@property (nullable, nonatomic, retain) NSSet<Device *> *heatingDevices;
@property (nullable, nonatomic, retain) Device *temperatureSensor;
@property (nullable, nonatomic, retain) NSManagedObject *timeSchedule;

@end

@interface HeatCoolArea (CoreDataGeneratedAccessors)

- (void)addCentralSourcesObject:(Device *)value;
- (void)removeCentralSourcesObject:(Device *)value;
- (void)addCentralSources:(NSSet<Device *> *)values;
- (void)removeCentralSources:(NSSet<Device *> *)values;

- (void)addCoolingDevicesObject:(Device *)value;
- (void)removeCoolingDevicesObject:(Device *)value;
- (void)addCoolingDevices:(NSSet<Device *> *)values;
- (void)removeCoolingDevices:(NSSet<Device *> *)values;

- (void)addHeatingDevicesObject:(Device *)value;
- (void)removeHeatingDevicesObject:(Device *)value;
- (void)addHeatingDevices:(NSSet<Device *> *)values;
- (void)removeHeatingDevices:(NSSet<Device *> *)values;

@end

NS_ASSUME_NONNULL_END
