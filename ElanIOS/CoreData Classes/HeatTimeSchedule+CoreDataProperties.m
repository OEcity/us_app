//
//  HeatTimeSchedule+CoreDataProperties.m
//  
//
//  Created by Tom Odler on 07.11.16.
//
//

#import "HeatTimeSchedule+CoreDataProperties.h"

@implementation HeatTimeSchedule (CoreDataProperties)

+ (NSFetchRequest<HeatTimeSchedule *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"HeatTimeSchedule"];
}

@dynamic heattimescheduleID;
@dynamic hysteresis;
@dynamic label;
@dynamic temp1;
@dynamic temp2;
@dynamic temp3;
@dynamic temp4;
@dynamic elan;
@dynamic inSchedule;

@end
