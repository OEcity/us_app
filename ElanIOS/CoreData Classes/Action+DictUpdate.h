//
//  Action+DictUpdate.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 18.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Action.h"
#import "SYSerializeProtocol.h"

@interface Action (DictUpdate) <SYSerializeProtocol>

- (BOOL)updateWithDictionary:(NSDictionary *)dict andName:(NSString*)name;

@end
