//
//  Device+Dictionary.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 19.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Device.h"
#import "SYSerializeProtocol.h"

@interface Device (Dictionary) <SYSerializeProtocol>

- (Action*)actionWithName:(NSString*)actionName;
- (Action*)primaryActionWithName:(NSString *)actionName ;
@end
