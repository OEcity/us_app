//
//  Elan+Dictionary.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 20.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Elan.h"
#import "SYSerializeProtocol.h"

@interface Elan (Dictionary) <SYSerializeProtocol>

@end
