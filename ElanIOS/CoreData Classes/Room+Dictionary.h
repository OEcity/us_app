//
//  Room+DictUpdate.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 16.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Room.h"
#import "SYSerializeProtocol.h"

@interface Room (Dictionary) <SYSerializeProtocol>

@end
