//
//  Device.m
//  
//
//  Created by Daniel Rutkovský on 08/07/15.
//
//

#import "Device.h"
#import "Action.h"
#import "DeviceInRoom.h"
#import "Elan.h"
#import "HeatCoolArea.h"
#import "SceneAction.h"
#import "SecondaryAction.h"
#import "State.h"


@implementation Device

@dynamic address;
@dynamic deviceID;
@dynamic favourite;
@dynamic iconPrefix;
@dynamic label;
@dynamic productType;
@dynamic type;
@dynamic url;
@dynamic actions;
@dynamic deviceCooling;
@dynamic deviceHeating;
@dynamic elan;
@dynamic inRooms;
@dynamic inSceneActions;
@dynamic primaryActions;
@dynamic secondaryActions;
@dynamic states;
@dynamic centralSource;
@dynamic automat;

- (void)addSecondaryActionsObject:(SecondaryAction *)value {
    NSMutableOrderedSet* tempSet = [NSMutableOrderedSet orderedSetWithOrderedSet:self.secondaryActions];
    [tempSet addObject:value];
    self.secondaryActions = tempSet;
}

@end
