//
//  Elan+CoreDataProperties.h
//  
//
//  Created by Tom Odler on 26.10.16.
//
//

#import "Elan.h"


NS_ASSUME_NONNULL_BEGIN

@interface Elan (CoreDataProperties)

+ (NSFetchRequest<Elan *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *apiVersion;
@property (nullable, nonatomic, copy) NSString *baseAddress;
@property (nullable, nonatomic, copy) NSNumber *configurationCounter;
@property (nullable, nonatomic, copy) NSString *firmware;
@property (nullable, nonatomic, copy) NSString *label;
@property (nullable, nonatomic, copy) NSString *mac;
@property (nullable, nonatomic, copy) NSNumber *selected;
@property (nullable, nonatomic, copy) NSNumber *state;
@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, copy) NSNumber *update;
@property (nullable, nonatomic, copy) NSString *version;
@property (nullable, nonatomic, copy) NSString *socketAddress;
@property (nullable, nonatomic, retain) NSSet<Address *> *addresses;
@property (nullable, nonatomic, retain) NSSet<Camera *> *cameras;
@property (nullable, nonatomic, retain) NSSet<Device *> *devices;
@property (nullable, nonatomic, retain) NSSet<IRDevice *> *irDevices;
@property (nullable, nonatomic, retain) NSSet<Room *> *rooms;
@property (nullable, nonatomic, retain) NSSet<Scene *> *scenes;

@end

@interface Elan (CoreDataGeneratedAccessors)

- (void)addAddressesObject:(Address *)value;
- (void)removeAddressesObject:(Address *)value;
- (void)addAddresses:(NSSet<Address *> *)values;
- (void)removeAddresses:(NSSet<Address *> *)values;

- (void)addCamerasObject:(Camera *)value;
- (void)removeCamerasObject:(Camera *)value;
- (void)addCameras:(NSSet<Camera *> *)values;
- (void)removeCameras:(NSSet<Camera *> *)values;

- (void)addDevicesObject:(Device *)value;
- (void)removeDevicesObject:(Device *)value;
- (void)addDevices:(NSSet<Device *> *)values;
- (void)removeDevices:(NSSet<Device *> *)values;

- (void)addIrDevicesObject:(IRDevice *)value;
- (void)removeIrDevicesObject:(IRDevice *)value;
- (void)addIrDevices:(NSSet<IRDevice *> *)values;
- (void)removeIrDevices:(NSSet<IRDevice *> *)values;

- (void)addRoomsObject:(Room *)value;
- (void)removeRoomsObject:(Room *)value;
- (void)addRooms:(NSSet<Room *> *)values;
- (void)removeRooms:(NSSet<Room *> *)values;

- (void)addScenesObject:(Scene *)value;
- (void)removeScenesObject:(Scene *)value;
- (void)addScenes:(NSSet<Scene *> *)values;
- (void)removeScenes:(NSSet<Scene *> *)values;

@end

NS_ASSUME_NONNULL_END
