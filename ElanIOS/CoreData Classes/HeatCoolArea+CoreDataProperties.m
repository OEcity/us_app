//
//  HeatCoolArea+CoreDataProperties.m
//  
//
//  Created by Tom Odler on 13.07.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "HeatCoolArea+CoreDataProperties.h"

@implementation HeatCoolArea (CoreDataProperties)

@dynamic schedule;
@dynamic temperatureOffset;
@dynamic centralSources;
@dynamic coolingDevices;
@dynamic heatingDevices;
@dynamic temperatureSensor;
@dynamic timeSchedule;

@end
