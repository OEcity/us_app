//
//  Camera+DictUpdate.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 18.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Camera+Dictionary.h"

@implementation Camera (Dictionary)

- (BOOL)updateWithDictionary:(NSDictionary *)dict {
    BOOL updated = NO;

    NSString* parsedLabel = [dict objectForKey:@"label"];
    NSString* parsedAddress = [dict objectForKey:@"address"];
    NSString* parsedType = [dict objectForKey:@"type"];
    NSString* parsedUsername = [dict objectForKey:@"username"];
    NSString* parsedPassword = [dict objectForKey:@"password"];
    
    if (![self.label isEqualToString:parsedLabel]) {
        self.label = parsedLabel;
        updated = YES;
    }
    
    if (![self.address isEqualToString:parsedAddress]) {
        self.address = parsedAddress;
        updated = YES;
    }
    
    if (![self.type isEqualToString:parsedType]) {
        self.type = parsedType;
        updated = YES;
    }
    
    if (![self.username isEqualToString:parsedUsername]) {
        self.username = parsedUsername;
        updated = YES;
    }
    
    if (![self.password isEqualToString:parsedPassword]) {
        self.password = parsedPassword;
        updated = YES;
    }
    
    return updated;
}

@end
