//
//  DeviceTimeScheduleClass+CoreDataProperties.m
//  
//
//  Created by Tom Odler on 27.10.16.
//
//

#import "DeviceTimeScheduleClass+CoreDataProperties.h"

@implementation DeviceTimeScheduleClass (CoreDataProperties)

+ (NSFetchRequest<DeviceTimeScheduleClass *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"DeviceTimeScheduleClass"];
}

@dynamic devicetimescheduleclassID;
@dynamic label;
@dynamic type;
@dynamic devices;
@dynamic elan;

@end
