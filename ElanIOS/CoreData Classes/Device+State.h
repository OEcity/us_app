//
//  Device+State.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 19.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Device.h"

@interface Device (State)

- (id)getStateValueForStateName:(NSString*)stateName;
- (BOOL)hasStateName:(NSString*)stateName;
- (void)setStateValue:(id)stateValue forName:(NSString*)stateName;
- (void)removeStateForName:(NSString*)stateName;

@end
