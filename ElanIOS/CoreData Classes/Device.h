//
//  Device.h
//  
//
//  Created by Daniel Rutkovský on 08/07/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Action, DeviceInRoom, Elan, HeatCoolArea, SceneAction, SecondaryAction, State, IRDevice;

@interface Device : NSManagedObject

@property (nonatomic, retain) NSNumber * address;
@property (nonatomic, retain) NSString * deviceID;
@property (nonatomic, retain) NSNumber * favourite;
@property (nonatomic, retain) NSNumber * automat;
@property (nonatomic, retain) NSString * iconPrefix;
@property (nonatomic, retain) NSString * label;
@property (nonatomic, retain) NSString * productType;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSSet *actions;
@property (nonatomic, retain) HeatCoolArea *deviceCooling;
@property (nonatomic, retain) HeatCoolArea *deviceHeating;
@property (nonatomic, retain) Elan *elan;
@property (nonatomic, retain) NSSet *inRooms;
@property (nonatomic, retain) NSSet *inSceneActions;
@property (nonatomic, retain) NSSet *primaryActions;
@property (nonatomic, retain) NSOrderedSet *secondaryActions;
@property (nonatomic, retain) NSSet *states;
@property (nonatomic, retain) HeatCoolArea *centralSource;
@end

@interface Device (CoreDataGeneratedAccessors)

- (void)addActionsObject:(Action *)value;
- (void)removeActionsObject:(Action *)value;
- (void)addActions:(NSSet *)values;
- (void)removeActions:(NSSet *)values;

- (void)addInRoomsObject:(DeviceInRoom *)value;
- (void)removeInRoomsObject:(DeviceInRoom *)value;
- (void)addInRooms:(NSSet *)values;
- (void)removeInRooms:(NSSet *)values;

- (void)addInSceneActionsObject:(SceneAction *)value;
- (void)removeInSceneActionsObject:(SceneAction *)value;
- (void)addInSceneActions:(NSSet *)values;
- (void)removeInSceneActions:(NSSet *)values;

- (void)addPrimaryActionsObject:(Action *)value;
- (void)removePrimaryActionsObject:(Action *)value;
- (void)addPrimaryActions:(NSSet *)values;
- (void)removePrimaryActions:(NSSet *)values;

- (void)insertObject:(SecondaryAction *)value inSecondaryActionsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromSecondaryActionsAtIndex:(NSUInteger)idx;
- (void)insertSecondaryActions:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeSecondaryActionsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInSecondaryActionsAtIndex:(NSUInteger)idx withObject:(SecondaryAction *)value;
- (void)replaceSecondaryActionsAtIndexes:(NSIndexSet *)indexes withSecondaryActions:(NSArray *)values;
- (void)addSecondaryActionsObject:(SecondaryAction *)value;
- (void)removeSecondaryActionsObject:(SecondaryAction *)value;
- (void)addSecondaryActions:(NSOrderedSet *)values;
- (void)removeSecondaryActions:(NSOrderedSet *)values;
- (void)addStatesObject:(State *)value;
- (void)removeStatesObject:(State *)value;
- (void)addStates:(NSSet *)values;
- (void)removeStates:(NSSet *)values;

@end
