//
//  SceneAction.m
//  
//
//  Created by Daniel Rutkovský on 02/06/15.
//
//

#import "SceneAction.h"
#import "Action.h"
#import "Device.h"
#import "Scene.h"


@implementation SceneAction

@dynamic value;
@dynamic action;
@dynamic device;
@dynamic scene;

@end
