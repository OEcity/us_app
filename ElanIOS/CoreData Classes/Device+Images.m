//
//  Device+Images.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 20.03.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Device+Images.h"
#import "Device+State.h"
#import "HeatCoolArea.h"
#import "ResourceTiles.h"
#import <objc/runtime.h>

@implementation Device (Images)

- (UIImage *)imageOff {
    if (self.iconPrefix){
        return [UIImage imageNamed:[self.iconPrefix stringByAppendingString:@"_off.png"]];
    }
    Tile * iconTile = [ResourceTiles generateDeviceTileForName:self.type];
    if (iconTile.tile_off==nil){
        iconTile = [ResourceTiles generateDeviceTileForName:@"all"];
    }
    return iconTile.tile_off;
}

- (void)setImageOff:(UIImage *)imageOff {
    objc_setAssociatedObject(self, @selector(imageOff), imageOff, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIImage *)imageOn {
    if (self.iconPrefix){
        if ([self isKindOfClass:[HeatCoolArea class]]){
            NSString* iconPrefix =@"teplomer";
            for (Device* heatingDevice in [[(HeatCoolArea*)self heatingDevices] allObjects]) {
                if ([heatingDevice getStateValueForStateName:@"on"] !=nil ){
                    if ([[heatingDevice getStateValueForStateName:@"on"] boolValue]==YES){
                        iconPrefix = @"teplomer_heating";
                    }
                } else if ([heatingDevice getStateValueForStateName:@"open valve"] != nil) {
                    if ([[heatingDevice getStateValueForStateName:@"open valve"] floatValue] > 0){
                        iconPrefix = @"teplomer_heating";
                    }
                }
            }

            for (Device* coolingDevice in [[(HeatCoolArea*)self coolingDevices] allObjects]) {
                if ([coolingDevice getStateValueForStateName:@"on"] !=nil){
                    if ([[coolingDevice getStateValueForStateName:@"on"] boolValue]==YES){
                        iconPrefix = @"teplomer_cooling";
                    }
                } else if ([coolingDevice getStateValueForStateName:@"open valve"] != nil) {
                    if ([[coolingDevice getStateValueForStateName:@"open valve"] floatValue] > 0){
                        iconPrefix = @"teplomer_cooling";
                    }
                }
            }
            return [UIImage imageNamed:[iconPrefix stringByAppendingString:@"_on.png"]];
        }
        return [UIImage imageNamed:[self.iconPrefix stringByAppendingString:@"_on.png"]];
    }
    Tile * iconTile = [ResourceTiles generateDeviceTileForName:self.type];
    if (iconTile.tile_off==nil){
        iconTile = [ResourceTiles generateDeviceTileForName:@"all"];
    }
    return iconTile.tile_on;
    //    return objc_getAssociatedObject(self, @selector(imageOn));
}

- (void)setImageOn:(UIImage *)imageOn {
    objc_setAssociatedObject(self, @selector(imageOn), imageOn, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
- (UIImage *)imageSeda {
    if (self.iconPrefix){
        return [UIImage imageNamed:[self.iconPrefix stringByAppendingString:@"_seda.png"]];
    }
    Tile * iconTile = [ResourceTiles generateDeviceTileForName:self.type];
    if (iconTile.tile_off==nil){
        iconTile = [ResourceTiles generateDeviceTileForName:@"all"];
    }
    return iconTile.tile_seda;
    //    return objc_getAssociatedObject(self, @selector(imageOn));
}

- (void)setImageSeda:(UIImage *)imageSeda {
    objc_setAssociatedObject(self, @selector(imageSeda), imageSeda, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}



@end
