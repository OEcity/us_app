//
//  Scene.m
//  
//
//  Created by Daniel Rutkovský on 02/06/15.
//
//

#import "Scene.h"
#import "Elan.h"
#import "SceneAction.h"


@implementation Scene

@dynamic label;
@dynamic sceneID;
@dynamic elan;
@dynamic sceneAction;

@end
