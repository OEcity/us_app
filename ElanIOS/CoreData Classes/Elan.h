//
//  Elan+CoreDataClass.h
//  
//
//  Created by Tom Odler on 26.10.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Address, Camera, Device, IRDevice, Room, Scene;

NS_ASSUME_NONNULL_BEGIN

@interface Elan : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Elan+CoreDataProperties.h"
