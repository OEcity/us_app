//
//  Camera+DictUpdate.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 18.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Camera.h"
#import "SYSerializeProtocol.h"

@interface Camera (Dictionary) <SYSerializeProtocol>

@end
