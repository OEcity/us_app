//
//  Device+State.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 19.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Device+State.h"
#import "SYCoreDataManager.h"

@implementation Device (State)

- (id)getStateValueForStateName:(NSString*)stateName {
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.name MATCHES %@", stateName];
    NSSet * filteredStatesSet = [self.states filteredSetUsingPredicate:predicate];
    if ([filteredStatesSet count] == 1) {
        State * tempState = [filteredStatesSet anyObject];
        if ([tempState.value isKindOfClass:[NSNull class]]) return nil;
        return tempState.value;
    }
    
    return nil;
}


- (BOOL)hasStateName:(NSString*)stateName {
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.name MATCHES %@", stateName];
    NSSet * filteredStatesSet = [self.states filteredSetUsingPredicate:predicate];
    if ([filteredStatesSet count] >= 1) {
        return YES;
    }
    return NO;
}

- (void)setStateValue:(id)stateValue forName:(NSString*)stateName {
    NSSet * currentStatesFiltered = [self.states filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"self.name MATCHES %@", stateName]];

    State* state;
    
    if ([currentStatesFiltered count] < 1) {
        state = [[SYCoreDataManager sharedInstance] createEntityStateInContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
    } else {
        state = [currentStatesFiltered anyObject];
    }
    state.device = self;
    state.value = stateValue;
    state.name = stateName;
    
    [[SYCoreDataManager sharedInstance] saveContext];
}

- (void)removeStateForName:(NSString*)stateName{
    NSSet * currentStatesFiltered = [self.states filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"self.name MATCHES %@", stateName]];
    for (State* state in currentStatesFiltered){
        [[SYCoreDataManager sharedInstance] deleteManagedObject:state];
    }
    [[SYCoreDataManager sharedInstance] saveContext];

}


@end
