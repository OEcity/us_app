//
//  Room.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 16.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Room.h"
#import "DeviceInRoom.h"
#import "Elan.h"


@implementation Room

@dynamic floorPlan;
@dynamic label;
@dynamic roomID;
@dynamic type;
@dynamic url;
@dynamic devicesInRoom;
@dynamic elan;

@end
