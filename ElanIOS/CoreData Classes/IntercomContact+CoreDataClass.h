//
//  IntercomContact+CoreDataClass.h
//  
//
//  Created by Tom Odler on 12.10.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface IntercomContact : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "IntercomContact+CoreDataProperties.h"
