//
//  Address.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 16.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Address.h"
#import "Elan.h"


@implementation Address

@dynamic name;
@dynamic url;
@dynamic elan;

@end
