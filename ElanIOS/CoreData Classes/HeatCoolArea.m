//
//  HeatCoolArea.m
//  
//
//  Created by Daniel Rutkovský on 08/07/15.
//
//

#import "HeatCoolArea.h"
#import "Device.h"


@implementation HeatCoolArea

@dynamic schedule;
@dynamic holidaySchedule;
@dynamic temperatureOffset;
@dynamic centralSources;
@dynamic coolingDevices;
@dynamic heatingDevices;
@dynamic temperatureSensor;

@end
