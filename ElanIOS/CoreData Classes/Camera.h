//
//  Camera.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 18.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Elan;

@interface Camera : NSManagedObject

@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSString * label;
@property (nonatomic, retain) Elan *elan;

@end
