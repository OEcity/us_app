//
//  Camera.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 18.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Camera.h"
#import "Elan.h"


@implementation Camera

@dynamic address;
@dynamic password;
@dynamic type;
@dynamic username;
@dynamic label;
@dynamic elan;

@end
