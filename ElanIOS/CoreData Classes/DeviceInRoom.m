//
//  DeviceInRoom.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 16.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "DeviceInRoom.h"
#import "Device.h"
#import "Room.h"


@implementation DeviceInRoom

@dynamic coordX;
@dynamic coordY;
@dynamic device;
@dynamic room;

@end
