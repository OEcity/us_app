//
//  Camera+Setup.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 24.03.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Camera.h"

@interface Camera (Setup)

-(NSString*) imageAddress;
-(NSString*) videoAddress;
-(NSString*) left;
-(NSString*) right;
-(NSString*) up;
-(NSString*) down;
-(NSString*) zoomOut;
-(NSString*) zoomIn;


+(NSArray*) listTypes;

@end
