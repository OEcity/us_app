//
//  SceneAction.h
//  
//
//  Created by Daniel Rutkovský on 02/06/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Action, Device, Scene;

@interface SceneAction : NSManagedObject

@property (nonatomic, retain) id value;
@property (nonatomic, retain) Action *action;
@property (nonatomic, retain) Device *device;
@property (nonatomic, retain) Scene *scene;

@end
