//
//  SecondaryAction+CoreDataProperties.h
//  
//
//  Created by Tom Odler on 01.09.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SecondaryAction.h"

NS_ASSUME_NONNULL_BEGIN

@interface SecondaryAction (CoreDataProperties)

@property (nullable, nonatomic, retain) NSSet<Action *> *actions;
@property (nullable, nonatomic, retain) Device *onDevice;
@property (nullable, nonatomic, retain) IRDevice *onIRDevice;

@end

@interface SecondaryAction (CoreDataGeneratedAccessors)

- (void)addActionsObject:(Action *)value;
- (void)removeActionsObject:(Action *)value;
- (void)addActions:(NSSet<Action *> *)values;
- (void)removeActions:(NSSet<Action *> *)values;

@end

NS_ASSUME_NONNULL_END
