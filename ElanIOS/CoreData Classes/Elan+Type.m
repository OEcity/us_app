//
//  Elan+Type.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 29.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Elan+Type.h"

@implementation Elan (Type)

- (void)parseTypeAndVersion:(NSDictionary*)dict {
    
    for (NSString* elanType in ELAN_TYPES) {
        NSString* version = [dict valueForKey:[elanType stringByAppendingString:@" version"]];
        
        if (version != nil) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            
            [defaults setValue:elanType forKey:@"elanType"];
            [defaults synchronize];
            self.type = elanType;
            self.firmware =  version;
        }
    }
}

@end
