//
//  HeatCoolArea.h
//  
//
//  Created by Daniel Rutkovský on 08/07/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"


@class Device;

@interface HeatCoolArea : Device

@property (nonatomic, retain) NSString * schedule;
@property (nonatomic, retain) NSString * holidaySchedule;
@property (nonatomic, retain) NSNumber * temperatureOffset;
@property (nonatomic, retain) NSSet *centralSources;
@property (nonatomic, retain) NSSet *coolingDevices;
@property (nonatomic, retain) NSSet *heatingDevices;
@property (nonatomic, retain) Device *temperatureSensor;
@end

@interface HeatCoolArea (CoreDataGeneratedAccessors)

- (void)addCentralSourcesObject:(Device *)value;
- (void)removeCentralSourcesObject:(Device *)value;
- (void)addCentralSources:(NSSet *)values;
- (void)removeCentralSources:(NSSet *)values;

- (void)addCoolingDevicesObject:(Device *)value;
- (void)removeCoolingDevicesObject:(Device *)value;
- (void)addCoolingDevices:(NSSet *)values;
- (void)removeCoolingDevices:(NSSet *)values;

- (void)addHeatingDevicesObject:(Device *)value;
- (void)removeHeatingDevicesObject:(Device *)value;
- (void)addHeatingDevices:(NSSet *)values;
- (void)removeHeatingDevices:(NSSet *)values;

@end
