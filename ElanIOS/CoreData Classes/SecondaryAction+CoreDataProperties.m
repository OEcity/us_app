//
//  SecondaryAction+CoreDataProperties.m
//  
//
//  Created by Tom Odler on 01.09.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "SecondaryAction+CoreDataProperties.h"

@implementation SecondaryAction (CoreDataProperties)

@dynamic actions;
@dynamic onDevice;
@dynamic onIRDevice;

@end
