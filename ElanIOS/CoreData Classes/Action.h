//
//  Action.h
//  
//
//  Created by Tom Odler on 01.09.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Device, SceneAction, IRDevice, SecondaryAction;

NS_ASSUME_NONNULL_BEGIN

@interface Action : NSManagedObject

// Insert code here to declare functionality of your managed object subclass

@end

NS_ASSUME_NONNULL_END

#import "Action+CoreDataProperties.h"
