//
//  CoreDataObjects.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 20.03.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#ifndef iHC_MIRF_CoreDataObjects_h
#define iHC_MIRF_CoreDataObjects_h
#import "Action+DictUpdate.h"
#import "Action.h"
#import "Address.h"
#import "Camera+Dictionary.h"
#import "Camera+Setup.h"
#import "Camera.h"
#import "Device+Dictionary.h"
#import "Device+Images.h"
#import "Device+State.h"
#import "Device.h"
#import "DeviceInRoom.h"
#import "Elan+Dictionary.h"
#import "Elan+Type.h"
#import "Elan.h"
#import "HeatCoolArea+Dictionary.h"
#import "HeatCoolArea.h"
#import "Room+Dictionary.h"
#import "Room.h"
#import "SYSerializeProtocol.h"
#import "Scene.h"
#import "SceneAction.h"
#import "State.h"
#import "HeatTimeSchedule.h"
#import "DeviceTimeScheduleClass.h"
#import "IRDevice.h"
#import "IRAction.h"
#import "IntercomContact+CoreDataClass.h"

#endif
