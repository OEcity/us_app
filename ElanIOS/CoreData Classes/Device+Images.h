//
//  Device+Images.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 20.03.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Device.h"

@interface Device (Images)

@property (nonatomic,retain) UIImage* imageOff;
@property (nonatomic,retain) UIImage* imageOn;
@property (nonatomic,retain) UIImage* imageSeda;

@end
