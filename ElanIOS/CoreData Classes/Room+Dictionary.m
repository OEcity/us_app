//
//  Room+DictUpdate.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 16.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Room+Dictionary.h"
#import "SYCoreDataManager.h"
#import "CoreDataObjects.h"
#import "DeviceInRoom.h"

@implementation Room (Dictionary)

- (BOOL)updateWithDictionary:(NSDictionary*)dict {
    BOOL updated = NO;

    NSString* parsedID = [dict objectForKey:@"id"];
    
    NSDictionary* parsedRoomInfo = [dict objectForKey:@"room info"];
    NSString* parsedLabel  = [parsedRoomInfo valueForKey:@"label"];
    NSString* parsedType  = [parsedRoomInfo valueForKey:@"type"];
    
    if (![self.roomID isEqualToString:parsedID]) {
        self.roomID = parsedID;
        updated = YES;
    }
    if (![self.label isEqualToString:parsedLabel]) {
        self.label = parsedLabel;
        updated = YES;
    }
    if (![self.type isEqualToString:parsedType]) {
        self.type = parsedType;
        updated = YES;
    }
    
    NSDictionary* floorplanDict = [dict objectForKey:@"floorplan"];
    
    if (![floorplanDict isKindOfClass: [NSNull class]]){
        if (![self.floorPlan isEqualToString:[floorplanDict objectForKey:@"image"]]) {
            self.floorPlan = [floorplanDict objectForKey:@"image"];
            updated = YES;
        }
    }
    
    NSString * predString = [NSString stringWithFormat:@"self.room.roomID == '%@'", self.roomID];
    
    for (NSString* deviceID in [[dict objectForKey:@"devices"] allKeys]) {
        
        predString = [predString stringByAppendingString:[NSString stringWithFormat:@" AND self.device.deviceID != '%@'", deviceID]];
        
        NSArray* devicePosition = [[[dict objectForKey:@"devices"] objectForKey:deviceID] objectForKey:@"coordinates"];
        
        Device* device = [[SYCoreDataManager sharedInstance] getDeviceWithID:deviceID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        NSLog(@"Pairing ROOM:%@ with device:%@", self.roomID, device.deviceID);
        struct SYDevicePosition position;
        
        if ([devicePosition isKindOfClass:[NSNull class]]) {
            position.coordX = 0.0;
            position.coordY = 0.0;
        } else {
            position.coordX = [[devicePosition objectAtIndex:0] floatValue];
            position.coordY = [[devicePosition objectAtIndex:1] floatValue];
        }
        
        [[SYCoreDataManager sharedInstance] pairDevice:device andRoom:self withCoordinates:position];

    }
    
    NSArray* toDelete = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"DeviceInRoom" withPredicate:[NSPredicate predicateWithFormat:predString]];
    for (DeviceInRoom* device in toDelete){
        [[SYCoreDataManager sharedInstance] deleteManagedObject:device];
    }
    
    
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pairingRooms" object:nil];
    return updated;
}

- (NSDictionary*)serializeToDictionary {
    NSMutableDictionary* roomDict = [NSMutableDictionary new];
    
    [roomDict setObject:self.roomID forKey:@"id"];
    [roomDict setObject:@{@"label" : self.label,
                          @"type" : self.type}
                 forKey:@"room info"];
    [roomDict setObject:self.floorPlan == nil? (id)[NSNull null] : @{@"image" : self.floorPlan}
                 forKey:@"floorplan"];
    NSMutableDictionary* devicesDict = [NSMutableDictionary new];
    
    for (DeviceInRoom* deviceInRoom in self.devicesInRoom) {
        [devicesDict setObject:@{@"coordinates" : self.floorPlan == nil ? (id)[NSNull null] : @[deviceInRoom.coordX,deviceInRoom.coordY] }
                        forKey:deviceInRoom.device.deviceID];
    }
    [roomDict setObject:devicesDict forKey:@"devices"];
    
    return roomDict;
}

@end
