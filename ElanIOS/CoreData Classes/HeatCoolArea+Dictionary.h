//
//  HeatCoolArea+Dictionary.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 19.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "HeatCoolArea.h"
#import "SYSerializeProtocol.h"

@interface HeatCoolArea (Dictionary) <SYSerializeProtocol>


@end
