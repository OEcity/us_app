//
//  Room.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 16.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DeviceInRoom, Elan;

@interface Room : NSManagedObject

@property (nonatomic, retain) NSString * floorPlan;
@property (nonatomic, retain) NSString * label;
@property (nonatomic, retain) NSString * roomID;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSSet *devicesInRoom;
@property (nonatomic, retain) Elan *elan;
@end

@interface Room (CoreDataGeneratedAccessors)

- (void)addDevicesInRoomObject:(DeviceInRoom *)value;
- (void)removeDevicesInRoomObject:(DeviceInRoom *)value;
- (void)addDevicesInRoom:(NSSet *)values;
- (void)removeDevicesInRoom:(NSSet *)values;

@end
