//
//  Camera+Setup.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 24.03.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Camera+Setup.h"

@implementation Camera (Setup)

-(NSString*) imageAddress {
    NSString * addr =[@"http://" stringByAppendingString:self.address];
    if ([self.type isEqualToString:@"Axis"]){
        return [addr stringByAppendingString:@"/jpg/image.jpg"];
    } else if ([self.type isEqualToString:@"iNELS cam"]){
        return [addr stringByAppendingString:@"/image/jpeg.cgi"];
    } else if ([self.type isEqualToString:@"Custom"]){
        return addr;
    }
    
    return nil;
}

-(NSString*) videoAddress {
    NSString * addr =[@"http://" stringByAppendingString:self.address];
    if ([self.type isEqualToString:@"Axis"]){
        return [addr stringByAppendingString:@"/mjpg/video.mjpg"];
    } else if ([self.type isEqualToString:@"iNELS cam"]){
        return [addr stringByAppendingString:@"/video/mjpg.cgi"];
    } else if ([self.type isEqualToString:@"Custom"]){
        return addr;
    }
    return nil;
}

-(NSString*) left {
    NSString * addr =[@"http://" stringByAppendingString:self.address];
    return [addr stringByAppendingString:@"/axis-cgi/com/ptz.cgi?move=left"];
}

-(NSString*) right {
    NSString * addr =[@"http://" stringByAppendingString:self.address];
    return [addr stringByAppendingString:@"/axis-cgi/com/ptz.cgi?move=right"];
}

-(NSString*) up {
    NSString * addr =[@"http://" stringByAppendingString:self.address];
    return [addr stringByAppendingString:@"/axis-cgi/com/ptz.cgi?move=up"];
}

-(NSString*) down {
    NSString * addr =[@"http://" stringByAppendingString:self.address];
    return [addr stringByAppendingString:@"/axis-cgi/com/ptz.cgi?move=down"];
}

-(NSString*) zoomOut {
    NSString * addr =[@"http://" stringByAppendingString:self.address];
    return [addr stringByAppendingString:@"/axis-cgi/com/ptz.cgi?rzoom=-300"];
}

-(NSString*) zoomIn {
    NSString * addr =[@"http://" stringByAppendingString:self.address];
    return [addr stringByAppendingString:@"/axis-cgi/com/ptz.cgi?rzoom=300"];
}

+(NSArray*) listTypes{
    return @[NSLocalizedString(@"camera_type_elko_ep", ""), NSLocalizedString(@"camera_type_axis", ""),@"Custom"];
}

@end
