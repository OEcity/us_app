//
//  Action+CoreDataProperties.m
//  
//
//  Created by Tom Odler on 01.09.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Action+CoreDataProperties.h"

@implementation Action (CoreDataProperties)

@dynamic max;
@dynamic min;
@dynamic name;
@dynamic step;
@dynamic type;
@dynamic device;
@dynamic inScenes;
@dynamic primaryOnDevice;
@dynamic secondaryOnDevice;
@dynamic irDevice;

@end
