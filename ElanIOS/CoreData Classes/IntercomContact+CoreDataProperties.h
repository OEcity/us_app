//
//  IntercomContact+CoreDataProperties.h
//  
//
//  Created by Tom Odler on 12.10.16.
//
//

#import "IntercomContact+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface IntercomContact (CoreDataProperties)

+ (NSFetchRequest<IntercomContact *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *ihcDevice;
@property (nullable, nonatomic, copy) NSString *label;
@property (nullable, nonatomic, copy) NSString *sipName;
@property (nullable, nonatomic, copy) NSString *ipAddress;
@property (nullable, nonatomic, copy) NSString *username;
@property (nullable, nonatomic, copy) NSString *password;
@property (nullable, nonatomic, copy) NSString *switchCode;

@end

NS_ASSUME_NONNULL_END
