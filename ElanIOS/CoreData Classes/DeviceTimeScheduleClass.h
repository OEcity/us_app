//
//  DeviceTimeScheduleClass+CoreDataClass.h
//  
//
//  Created by Tom Odler on 27.10.16.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Device, Elan;

NS_ASSUME_NONNULL_BEGIN

@interface DeviceTimeScheduleClass : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "DeviceTimeScheduleClass+CoreDataProperties.h"
