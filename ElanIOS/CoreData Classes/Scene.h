//
//  Scene.h
//  
//
//  Created by Daniel Rutkovský on 02/06/15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Elan, SceneAction;

@interface Scene : NSManagedObject

@property (nonatomic, retain) NSString * label;
@property (nonatomic, retain) NSString * sceneID;
@property (nonatomic, retain) Elan *elan;
@property (nonatomic, retain) NSOrderedSet *sceneAction;
@end

@interface Scene (CoreDataGeneratedAccessors)

- (void)insertObject:(SceneAction *)value inSceneActionAtIndex:(NSUInteger)idx;
- (void)removeObjectFromSceneActionAtIndex:(NSUInteger)idx;
- (void)insertSceneAction:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeSceneActionAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInSceneActionAtIndex:(NSUInteger)idx withObject:(SceneAction *)value;
- (void)replaceSceneActionAtIndexes:(NSIndexSet *)indexes withSceneAction:(NSArray *)values;
- (void)addSceneActionObject:(SceneAction *)value;
- (void)removeSceneActionObject:(SceneAction *)value;
- (void)addSceneAction:(NSOrderedSet *)values;
- (void)removeSceneAction:(NSOrderedSet *)values;
@end
