//
//  Device+Dictionary.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 19.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "HeatCoolArea+Dictionary.h"
#import "SYCoreDataManager.h"
#import "Device+Dictionary.h"

@implementation HeatCoolArea (Dictionary)

- (void)updateWithDictionary:(NSDictionary *)dict {
    
    
    //    [(HeatCoolArea*)device setSchedule:[(NSDictionary*)object objectForKey:@"schedule"]];
    //Parsing basic information
    NSArray* heatingDevices = [dict objectForKey:@"heating devices"];
    NSArray* coolingDevices = [dict objectForKey:@"cooling devices"];
    [self setCentralSources:[NSSet new]];
    [self setHeatingDevices:[NSSet new]];
    [self setCoolingDevices:[NSSet new]];
    for (id element in heatingDevices) {
        Device * dev = [[SYCoreDataManager sharedInstance] getDeviceWithID:[element objectForKey:@"id"] inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        if (dev == nil){
            dev = [[SYCoreDataManager sharedInstance] createEntityDevice];
            [dev setDeviceID:[element objectForKey:@"id"]];
            [[SYCoreDataManager sharedInstance] saveContext];
        }
        if ([element[@"central source"] boolValue]){
            [self addCentralSourcesObject:dev];
        }
        
        [self addHeatingDevicesObject:dev];
    }
    
    for (id element in coolingDevices) {
        Device * dev = [[SYCoreDataManager sharedInstance] getDeviceWithID:[element objectForKey:@"id"] inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        if (dev == nil){
            dev = [[SYCoreDataManager sharedInstance] createEntityDevice];
            [dev setDeviceID:[element objectForKey:@"id"]];
            [[SYCoreDataManager sharedInstance] saveContext];
        }
        if ([element[@"central source"] boolValue]){
            [self addCentralSourcesObject:dev];
        }
        
        [self addCoolingDevicesObject:dev];
    }
    
    NSDictionary* tempSenzor = [dict objectForKey:@"temperature sensor"];
    
    if (tempSenzor != nil) {
        if ([[tempSenzor allKeys] count] > 0) {
            Device* tempSensor = [[SYCoreDataManager sharedInstance]
                                  getDeviceWithID:[[[dict objectForKey:@"temperature sensor"] allKeys] firstObject]
                                  inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
            [self setTemperatureSensor:tempSensor];
        }
    }
    
    
    NSNumber* tempOffset = [dict objectForKey:@"temperature offset"];
    [self setTemperatureOffset:tempOffset];
    
    if ([[dict objectForKey:@"schedule2"] intValue] != 0) {
        [self setHolidaySchedule:[dict objectForKey:@"schedule2" ]];
    }
        [self setSchedule:[dict objectForKey:@"schedule"]];
    //Parsing basic information
    NSString* parsedID = [dict objectForKey:@"id"];
    
    NSDictionary* deviceInfo = [dict objectForKey:@"device info"];
    NSString* parsedProductType = [deviceInfo objectForKey:@"product type"];
    NSString* parsedType = [deviceInfo objectForKey:@"type"];
    NSString* parsedLabel = [deviceInfo objectForKey:@"label"];
    NSNumber* parsedAddress = [deviceInfo objectForKey:@"address"];
    
    //    if (parsedLabel ==nil) return false;
    
    //If info is same for all attributes, skip updating
    if (!([self.deviceID isEqualToString:parsedID] &&
          [self.productType isEqualToString:parsedProductType] &&
          [self.type isEqualToString:parsedType] &&
          [self.label isEqualToString:parsedLabel] &&
          [self.address isEqual:parsedAddress])) {
        
        self.deviceID = parsedID;
        self.productType = parsedProductType;
        self.type = parsedType;
        self.label = parsedLabel;
        self.address = parsedAddress;
        
        
    }
    [self determineIconPrefix];
    [self parseActions:[dict objectForKey:@"actions info"] withPrimaryActions:[dict objectForKey:@"primary actions"] andSecondaryActions:[dict objectForKey:@"secondary actions"]];
    return;
}

#pragma mark - Helper methods

- (BOOL)parseActions:(NSDictionary*)anActionsInfo withPrimaryActions:(NSArray*)aPrimaryActions andSecondaryActions:(NSArray*)aSecondaryActions {
    BOOL updated = NO;
    
    //Parsing actions info from dictionary
    for (NSString* actionName in [anActionsInfo allKeys]) {
        Action* action = [self actionWithName:actionName];
        
        if (action == nil) {
            //Context issue fix
            action = [[SYCoreDataManager sharedInstance] createEntityActionInContext:self.managedObjectContext];
            updated = YES;
        }
        
        updated = ([action updateWithDictionary:[anActionsInfo objectForKey:actionName] andName:actionName]||updated);
        BOOL isPrimary = NO;
        BOOL isSecondary = NO;
        for (NSString *primaryKey in aPrimaryActions){
            //  if ([primaryKey isEqualToString:actionName]){
            //      isPrimary = YES;
            //  }
        }
        
        //Assigning secondary actions
        for (id secondaryActionObject in aSecondaryActions) {
            if ([secondaryActionObject isKindOfClass:[NSString class]]) {
                if ([secondaryActionObject isEqualToString:actionName]) isSecondary = YES;
            } else if ([secondaryActionObject isKindOfClass:[NSArray class]]) {
                for (NSString* secActionName in secondaryActionObject) {
                    if ([secActionName isEqualToString:actionName]) isSecondary = YES;
                }
            }
            
        }
        if (action && ![self.primaryActions containsObject:action] && isPrimary) {
            [self addPrimaryActionsObject:action];
            updated = YES;
        }
        if (action && ![self.secondaryActions containsObject:action] && isSecondary) {
#warning Secondary actions not currently used
            
            //   [self addSecondaryActionsObject:[[NSSet alloc] initWithObjects:action, nil]];
            //   updated = YES;
        }
        
        if (action && ![self.actions containsObject:action] ) {
            [self addActionsObject: action];
            updated = YES;
        }
        [[SYCoreDataManager sharedInstance] saveContext];
        
    }
    
    /*
     //Assigning secondary actions
     for (id secondaryActionObject in aSecondaryActions) {
     NSArray* dictionaryActions;
     
     NSPredicate* predicate;
     
     if ([secondaryActionObject isKindOfClass:[NSString class]]) {
     predicate = [NSPredicate predicateWithFormat:@"(SUBQUERY(actions,$action,$action.name == %@).@count == 1)",(NSString*)secondaryActionObject];
     } else if ([secondaryActionObject isKindOfClass:[NSArray class]]) {
     predicate = [NSPredicate predicateWithFormat:@"(SUBQUERY(actions,$action,$action.name == %@).@count == 1) AND (SUBQUERY(actions,$action,$action.name == %@).@count == 1)",[secondaryActionObject objectAtIndex:0],[secondaryActionObject objectAtIndex:1]];
     }
     
     NSOrderedSet* filteredSecondaryActions = [self.secondaryActions filteredOrderedSetUsingPredicate:predicate];
     
     if ([filteredSecondaryActions count] < 1) {
     SecondaryAction* secondaryAction = [[SYCoreDataManager sharedInstance] createEntitySecondaryAction];
     
     if ([secondaryActionObject isKindOfClass:[NSString class]]) {
     [secondaryAction addActionsObject:[self actionWithName:(NSString*)secondaryActionObject]];
     } else if ([secondaryActionObject isKindOfClass:[NSArray class]]) {
     for (NSString* actionName in secondaryActionObject) {
     [secondaryAction addActionsObject:[self actionWithName:actionName]];
     }
     }
     
     updated = YES;
     }
     }
     
     */
    return updated;
}

- (Action*)actionWithName:(NSString *)actionName {
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"name MATCHES %@",actionName];
    
    NSSet* filteredActions = [self.actions filteredSetUsingPredicate:predicate];
    
    if ([filteredActions count] > 0) {
        return [filteredActions anyObject];
    } else {
        return nil;
    }
}
- (Action*)primaryActionWithName:(NSString *)actionName {
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"name MATCHES %@",actionName];
    
    NSSet* filteredActions = [self.primaryActions filteredSetUsingPredicate:predicate];
    
    if ([filteredActions count] > 0) {
        return [filteredActions anyObject];
    } else {
        return nil;
    }
}

-(void)determineIconPrefix{
    self.iconPrefix = nil;
    if ([self.type isEqualToString:@"heating"] ||
        [self.type isEqualToString:@"thermometer"] ||
        [self.type isEqualToString:@"temperature regulation area"]){
        if (self.deviceHeating !=nil) {
            self.iconPrefix = @"teplomer_heating";
        } else if (self.deviceCooling!=nil){
            self.iconPrefix = @"teplomer_cooling";
        } else {
            self.iconPrefix =@"teplomer";
        }
    }
}

@end
