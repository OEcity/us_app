//
//  State.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 16.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "State.h"
#import "Device.h"


@implementation State

@dynamic name;
@dynamic value;
@dynamic device;

@end
