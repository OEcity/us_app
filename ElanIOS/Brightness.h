//
//  Brightness.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 6/20/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Action.h"
@interface Brightness : NSObject

@property (nonatomic, retain) NSNumber * min;
@property (nonatomic, retain) NSNumber * max;
@property (nonatomic, retain) NSNumber * step;
@property (nonatomic, copy) NSString * type;
-(id) initWithNSDictionary:(Action*)dict;

@end
