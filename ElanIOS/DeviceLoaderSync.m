//
//  DeviceLoaderSync.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/24/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "DeviceLoaderSync.h"
#import "SYCoreDataManager.h"
#import "Util.h"
#import "ItemsResponse.h"
#import "RoomDetailResponse.h"
#import "DevicesResponse.h"
#import "DeviceDetailResponse.h"
#import "StateResponse.h"
#import "State.h"
#import "HUDWrapper.h"
#import "AppDelegate.h"
@implementation DeviceLoaderSync

/*
-(void) LoadDevices:(id)sender{

    [self performSelectorOnMainThread:@selector(startLoading) withObject:nil waitUntilDone:NO];

    dispatch_async(dispatch_get_main_queue(), ^{
        [CoreDataManager deleteAllObjects:@"DeviceInRoom"];
        [CoreDataManager deleteAllObjects:@"Device"];

    });
    NSMutableDictionary * devicesCoords = [[NSMutableDictionary alloc] init];

    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];

    
    NSManagedObjectContext* objectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [objectContext setPersistentStoreCoordinator:appDelegate.persistentStoreCoordinator];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[Util getAddressWith:@"rooms"]]];
    [request setHTTPMethod:@"GET"];
    NSURLResponse* response;
    NSError* error = nil;

        //Capturing server response
    NSData* result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error==nil && result!=nil){
        dispatch_async(dispatch_get_main_queue(), ^{
            [CoreDataManager deleteAllObjects:@"Room"];
        });
    }
    else{
        NSLog(@"Download error");
    }
    
    ItemsResponse * roomsResponse = [[ItemsResponse alloc] init];
    NSMutableArray * rooms = (NSMutableArray *)[roomsResponse parseResponse:result identifier:nil];

    self.rooms = [[NSMutableArray alloc] init];
    for (Room * room in rooms){
        NSString*requestString = [NSString stringWithFormat:@"%@/%@", [Util getAddressWith:@"rooms"],room.name];
        request = [[NSMutableURLRequest alloc] init] ;
        [request setURL:[NSURL URLWithString:requestString]];
        [request setHTTPMethod:@"GET"];
        result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if (error==nil && result!=nil){

        }
        else{
            // throw error. Rollback
        }
        RoomDetailResponse * roomsResponse = [[RoomDetailResponse alloc] init];
        NSMutableArray * resp  = (NSMutableArray*)[roomsResponse parseResponse:result identifier:room.name];
        [self.rooms addObject:[resp objectAtIndex:0]];

        dispatch_async(dispatch_get_main_queue(), ^{
            [CoreDataManager saveRoom:(Room*)[resp objectAtIndex:0] inContext:objectContext];
        });
        for (int i=1; i < [resp count];i++){
            NSString * name = [[[resp objectAtIndex:i] allKeys] objectAtIndex:0];
            NSArray * value = [[[resp objectAtIndex:i] allValues] objectAtIndex:0];
            if ([value isKindOfClass:[NSNull class]]==NO){
                [devicesCoords setObject:value forKey:name];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CoreDataManager storeRoomDeviceConnection:room.name deviceId:name coordX:[value objectAtIndex:0] coordY:[value objectAtIndex:1] inContext:objectContext];
                });
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    [CoreDataManager storeRoomDeviceConnection:room.name deviceId:name inContext:objectContext];
                });
            }
        }
    }
    
    [objectContext save:&error];

    request = [[NSMutableURLRequest alloc] init] ;
    [request setURL:[NSURL URLWithString:[Util getAddressWith:@"devices"]]];
    [request setHTTPMethod:@"GET"];
    //Capturing server response
    result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    if (error==nil && result!=nil){
        dispatch_async(dispatch_get_main_queue(), ^{
            [CoreDataManager deleteAllObjects:@"Device"];
        });
    }
    else{
        // throw error. Rollback
    }
    DevicesResponse * devicesResponse = [[DevicesResponse alloc] init];
    NSMutableArray * devices = (NSMutableArray *)[devicesResponse parseResponse:result identifier:nil];
    self.devices = [[NSMutableArray alloc] init];
    int i=0;
    for (Device* device in devices){
        NSLog(@"%d",i);
        i++;

        NSString* devicesString = [NSString stringWithFormat:@"%@/%@", [Util getAddressWith:@"devices"],device.name];
        request = [[NSMutableURLRequest alloc] init] ;
        [request setURL:[NSURL URLWithString:devicesString]];
        [request setHTTPMethod:@"GET"];
        result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if (error==nil && result!=nil){
        }
        else{
            NSLog(@"Error loading device");
            // throw error. Rollback
        }
        DeviceDetailResponse * detailResponse = [[DeviceDetailResponse alloc] init];
        Device * resp  = (Device*)[detailResponse parseResponse:result identifier:device.name];
        devicesString = [NSString stringWithFormat:@"%@/%@/%@", [Util getAddressWith:@"devices"],device.name, @"state"];

        request = [[NSMutableURLRequest alloc] init] ;
        [request setURL:[NSURL URLWithString:devicesString]];
        [request setHTTPMethod:@"GET"];
        result = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        if (error==nil && result!=nil){

        }
        else{
            NSLog(@"Error loading device");
            // throw error. Rollbackg
        }
        StateResponse * stateResp = [[StateResponse alloc] init];
        State* state = (State*)[stateResp parseResponse:result identifier:device.name];
        resp.deviceState = state;
        
        [CoreDataManager saveDevice:resp inContext:objectContext];
        [self.devices addObject:resp];
    }
    
    [objectContext save:&error];
    
    [self performSelectorOnMainThread:@selector(complete:) withObject:nil waitUntilDone:NO];

}

-(void)complete:(NSMutableDictionary *)Dict{
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.loadingDevices=NO;

    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self];
}

-(void)startLoading{
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.loadingDevices=YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingStarted" object:self];

}
*/
@end
