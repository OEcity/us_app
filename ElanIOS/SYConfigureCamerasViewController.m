//
//  SYConfigureCamerasViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/14/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYConfigureCamerasViewController.h"
#import "SYAddCameraViewController.h"
#import "AppDelegate.h"
#import "MainMenuPVC.h"
#import "UIViewController+RevealViewControllerAddon.h"
@interface SYConfigureCamerasViewController ()
@property (nonatomic, strong) NSFetchedResultsController* cameras;

@end

@implementation SYConfigureCamerasViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self.emtyLabel setText:NSLocalizedString(@"noCamera", nil)];
    [self initFetchedResultsController];
    

//    [[self navigationController]setNavigationBarHidden:NO];
    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [_camerasLabel setText:NSLocalizedString(@"cameras", nil)];
    [_bottomRightButton setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
    [_bottomLeftButton setTitle:NSLocalizedString(@"menu", nil) forState:UIControlStateNormal];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    [self.tableView reloadData];
    self.emtyLabel.hidden = [self.cameras.fetchedObjects count] > 0;
}


#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Camera"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _cameras = [[NSFetchedResultsController alloc]
              initWithFetchRequest:fetchRequest
              managedObjectContext:context
              sectionNameKeyPath:nil
              cacheName:nil];
    _cameras.delegate = self;
    NSError *error;
    
    if (![_cameras performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            self.emtyLabel.hidden = [_cameras.fetchedObjects count] > 0;
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[_tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    MainMenuPVC*menu = app.mainMenuPVC;
    
    [menu refreshPVC:@"camera controller did change content"];

}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Camera* camera =[self.cameras objectAtIndexPath:indexPath];
    
    UILabel * cellName  = (UILabel *) [cell viewWithTag:1];
    cellName.text = camera.label;


}
#pragma mark UITableView DataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {


    return [self.cameras.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"MyCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
   
    return cell;
}


-(IBAction)AddCamera:(id)sender{
    self.selectedCamera = nil;

    AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
#warning TODO: load limits from proper source
    if (delegate.limitsSettings!=nil){
        NSNumber * maxCount =[[delegate.limitsSettings objectForKey:@"camera"] objectForKey:@"max count"];
        if ([_cameras.fetchedObjects count] >= [maxCount intValue] ){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Cannot add another camera", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
    }
    [self performSegueWithIdentifier:@"addCamera" sender:self];

}
-(IBAction)back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark UItableView delegate

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedCamera = (Camera*)[self.cameras objectAtIndexPath:indexPath];
    [self performSegueWithIdentifier:@"addCamera" sender:self];


}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Camera * selectedCamera = [_cameras objectAtIndexPath:indexPath];

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_camera", nil), selectedCamera.label] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil),nil];
        alert.tag = indexPath.row;
        [alert show];
        
    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"address = %@",[(Camera*)[_cameras objectAtIndexPath:[NSIndexPath indexPathForRow:alertView.tag inSection:0]] address]];
        NSArray *previous = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Camera" withPredicate:predicate sortDescriptor:nil inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        Camera* camera = [previous firstObject];
        [[SYCoreDataManager sharedInstance] deleteManagedObject:camera];
        
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"addCamera"]){
        SYAddCameraViewController* dest =  (SYAddCameraViewController*)segue.destinationViewController;
        dest.camera = self.selectedCamera;
    }

}
- (NSUInteger)supportedInterfaceOrientations
{

    return UIInterfaceOrientationMaskPortrait;
}

@end
