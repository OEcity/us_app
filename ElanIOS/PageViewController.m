//
//  PageViewController.m
//  SAM_11_NewDesign
//
//  Created by admin on 06.07.16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "PageViewController.h"

@interface PageViewController () <UIPageViewControllerDelegate, UIPageViewControllerDataSource>

@property (strong, nonatomic) UIPageViewController *pageViewController;

@end

@implementation PageViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    
    _looped = NO;
    
    //Configure page view controller
    _currentOrderedContentArray = [[NSMutableArray alloc] init];
    _futureOrderedContentArray = [[NSMutableArray alloc] init];
    UIViewController *initialVC = [[UIViewController alloc] init];
    [_currentOrderedContentArray addObject:initialVC];
    [_futureOrderedContentArray addObject:initialVC];
    self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageViewController.delegate = self;
    self.pageViewController.dataSource = self;
    UIViewController *startingViewController = [_currentOrderedContentArray lastObject];
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    [self addChildViewController:self.pageViewController];
    [self.view addSubview:self.pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    
    if (_pageControl) {
        [_pageControl setNumberOfPages:[_currentOrderedContentArray count]];
        [_pageControl setCurrentPage:0];
        [self.view bringSubviewToFront:_pageControl];
    }
}

#pragma mark - managing data source
-(void)applyFutureStructureWithListedPageIndex:(NSUInteger)index{
    if (!_futureOrderedContentArray) {
        return;
    }
    
    if ([_futureOrderedContentArray count] == 0) {
        return;
    }
    
    if (index >= [_futureOrderedContentArray count]) {
        index = 0;
    }
    
    [self.pageViewController setViewControllers:@[[_futureOrderedContentArray objectAtIndex:index]]
                                      direction:UIPageViewControllerNavigationDirectionForward
                                       animated:NO
                                     completion:^(BOOL finished) {}];
    
    [_currentOrderedContentArray removeAllObjects];
    [_currentOrderedContentArray addObjectsFromArray:_futureOrderedContentArray];
    
    if (_pageControl) {
        [self.pageControl setNumberOfPages:[_currentOrderedContentArray count]];
        self.pageControl.currentPage = index;
    }
    
}

-(void)dropStructureAndFillWithCurrent{
    if (!_futureOrderedContentArray) {
        _futureOrderedContentArray = [[NSMutableArray alloc] init];
    }
    
    [_futureOrderedContentArray removeAllObjects];
    [_futureOrderedContentArray addObjectsFromArray:_currentOrderedContentArray];
}

-(void)setPageControl:(UIPageControl *)pageControl{
    _pageControl = pageControl;
    [_pageControl setNumberOfPages:[_currentOrderedContentArray count]];
    [_pageControl setCurrentPage:[_currentOrderedContentArray indexOfObject:self.pageViewController.viewControllers[0]]];
    [self.view bringSubviewToFront:_pageControl];
}

#pragma mark - page controller sector
- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController{
    NSUInteger index = [_currentOrderedContentArray indexOfObject:viewController];
    if (index == NSNotFound) {
        return nil;
    }else if(index == 0){
        if (_looped) {
            index = [_currentOrderedContentArray count]-1;
        }else{
            return nil;
        }
    }else{
        index--;
    }
    
    return [_currentOrderedContentArray objectAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController{
    NSUInteger index = [_currentOrderedContentArray indexOfObject:viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [_currentOrderedContentArray count]) {
        if (_looped) {
            index = 0;
        }else{
            return nil;
        }
    }
    return [_currentOrderedContentArray objectAtIndex:index];
}

- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation {
    if (UIInterfaceOrientationIsPortrait(orientation) || ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone)) {
        // In portrait orientation or on iPhone: Set the spine position to "min" and the page view controller's view controllers array to contain just one view controller. Setting the spine position to 'UIPageViewControllerSpineLocationMid' in landscape orientation sets the doubleSided property to YES, so set it to NO here.
        
        UIViewController *currentViewController = self.pageViewController.viewControllers[0];
        NSArray *viewControllers = @[currentViewController];
        [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
        
        self.pageViewController.doubleSided = NO;
        return UIPageViewControllerSpineLocationMin;
    }
    
    // In landscape orientation: Set set the spine location to "mid" and the page view controller's view controllers array to contain two view controllers. If the current page is even, set it to contain the current and next view controllers; if it is odd, set the array to contain the previous and current view controllers.
    UIViewController *currentViewController = self.pageViewController.viewControllers[0];
    NSArray *viewControllers = nil;
    
    NSUInteger indexOfCurrentViewController = [_currentOrderedContentArray indexOfObject:currentViewController];
    if (indexOfCurrentViewController == 0 || indexOfCurrentViewController % 2 == 0) {
        UIViewController *nextViewController = [self pageViewController:self.pageViewController viewControllerAfterViewController:currentViewController];
        viewControllers = @[currentViewController, nextViewController];
    } else {
        UIViewController *previousViewController = [self pageViewController:self.pageViewController viewControllerBeforeViewController:currentViewController];
        viewControllers = @[previousViewController, currentViewController];
    }
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    
    
    return UIPageViewControllerSpineLocationMid;
}

-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed{
    if (_pageControl) {
        [_pageControl setCurrentPage:[_currentOrderedContentArray indexOfObject:self.pageViewController.viewControllers[0]]];
    }
}
@end
