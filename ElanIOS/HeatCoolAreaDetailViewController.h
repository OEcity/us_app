//
//  HeatCoolAreaDetailViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 25.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeatCoolArea.h"
#import "SYAPIManager.h"
#import "HUDWrapper.h"

@interface HeatCoolAreaDetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, retain) HeatCoolArea*heatCoolArea;
@property (weak, nonatomic) IBOutlet UIView *pickerViewContainer;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) HUDWrapper *loaderDialog;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (nonatomic, retain) NSArray * availableHeaters;
@property (nonatomic, retain) NSMutableArray * heatersSelected;

@end
