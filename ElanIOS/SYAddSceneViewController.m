//
//  SYAddSceneViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/1/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYAddSceneViewController.h"
#import "SYCoreDataManager.h"
#import "SYSceneActionsViewController.h"
#import "Constants.h"
#import "SceneRGBViewController.h"
#import "Device+State.h"
#import "SceneBlindsViewController.h"
#import "SceneDimmingViewController.h"
#import "SceneChromacityViewController.h"
#import "SYAPIManager.h"

@interface SYAddSceneViewController (){
    NSString *name;
}
@property (nonatomic, strong) NSFetchedResultsController* fetchedResultsController;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (nonatomic) Device *selectedDevice;



@property (nonatomic, retain) NSMutableDictionary *selectedActions;

@end

@implementation SYAddSceneViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.selectedDevices = [[NSMutableDictionary alloc] init];
    self.deviceActions =[[NSMutableDictionary alloc] init];
    self.actionsArray =[[NSMutableArray alloc] init];
    _tf.delegate = self;
    
    NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"enterName", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
    
    _tf.attributedPlaceholder = namePlaceHolder;
    
    if(name){
        _tf.text = name;
    }
    
    if (self.scene!=nil){
        [self.tf setText:self.scene.label];
        
        NSMutableArray *arrayForParse = [[NSMutableArray alloc]init];
        NSMutableDictionary *dictToArray = [[NSMutableDictionary alloc]init];
        
        for (SceneAction * action in self.scene.sceneAction) {
            if (action.device == nil) continue;
            
            [dictToArray setObject:action.action.name forKey:action.action.device.deviceID];
            [arrayForParse addObject:dictToArray];
            [dictToArray removeAllObjects];
            
            NSString * deviceName = [action.device.deviceID copy];
            Device * d = [[SYCoreDataManager sharedInstance] getDeviceWithID:deviceName];
            if(d) {
                [self.selectedDevices setObject:d forKey:deviceName];
                [self.tableView reloadData];
            }
        }
    }else{
        if (_sceneName==nil)
            [self.tf setText:NSLocalizedString(@"hintForRoom", nil)];
        else
            [self.tf setText:_sceneName];
    }
    [self initFetchedResultsController];
    
    _tableView.layer.borderColor = USBlueColor.CGColor;
    _tableView.layer.borderWidth = 1.0f;
    _tableView.hidden = YES;
    [self loadSceneActionsFromDB];
}

-(void)loadSceneActionsFromDB{
    _selectedActions = [[NSMutableDictionary alloc] init];
    for (Device * device in _fetchedResultsController.fetchedObjects){
        NSString * deviceName = device.deviceID;
        NSMutableArray * deviceArray = [_selectedActions objectForKey:deviceName];
        deviceArray = [[NSMutableArray alloc] init];
        if(!device)
            continue;
        
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.scene.sceneID == %@", _scene.sceneID];
        [deviceArray addObjectsFromArray:[[device.inSceneActions filteredSetUsingPredicate:predicate] allObjects]];
        
        NSMutableDictionary *myDict = [[NSMutableDictionary alloc]init];
        for (SceneAction * res in deviceArray){
            [myDict setObject:res.value forKey:res.action.name];
        }
        
        if ([myDict count]>0){
        NSMutableDictionary *deviceDict = [[NSMutableDictionary alloc]init];
        [deviceDict setObject:myDict forKey:device.deviceID];
        [_actionsArray addObject:deviceDict];
        }
    }
    NSLog(@"Actions array: %@", _actionsArray);
}

- (void)initLocalizableStrings {
    [self.tf setText:NSLocalizedString(@"hintForRoom", nil)];
    [_scenesLabel setText:NSLocalizedString(@"scenes", nil)];
    [_bottomRightButton setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    [_bottomLeftButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_iconlabel setText:NSLocalizedString(@"icon", nil)];
    [_sceneNameLabel setText:NSLocalizedString(@"nameForScene", nil)];
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (self.willDismiss==YES){
        [self dismissViewControllerAnimated:NO completion:nil];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //_tableViewHeight.constant = [_fetchedResultsController.fetchedObjects count]*65;
    NSLog(@"actions device array: %@", _actionsArray);
    [_tableView reloadData];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Init

- (void)initFetchedResultsController {
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Device"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    //    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.actions[COUNT] > 0 AND SUBQUERY(self.states,$state,$state.name == 'locked' AND $state.value != 0).@count == 0"];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.actions.@count > 0 AND self.address != nil AND self.states.@count > 0 AND SUBQUERY(self.states,$s,($s.name MATCHES 'locked' AND $s.value != 0) OR $s.name MATCHES 'temperature' OR $s.name MATCHES 'temperature IN' OR $s.name MATCHES 'temperature OUT').@count == 0"]; //
    [fetchRequest setPredicate:predicate];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetchRequest
                                 managedObjectContext:context
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    _fetchedResultsController.delegate = self;
    NSError *error;
    
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
    [self.tableView reloadData];
}

#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            //            self.emptyLabel.hidden = [_fetchedResultsController.fetchedObjects count] > 0;
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:[_tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Device *selectedDevice = [_fetchedResultsController objectAtIndexPath:indexPath];
    
    UIButton * deviceFavourite = (UIButton*)[cell viewWithTag:3];
    if ([self.selectedDevices objectForKey:selectedDevice.deviceID] != nil){
        
        [deviceFavourite setImage:[UIImage imageNamed:@"fajfka_vyber_prvku.png"] forState:UIControlStateNormal];
    }else{
        [deviceFavourite setImage:[UIImage imageNamed:@"krizek_vyber_prvku.png"]forState:UIControlStateNormal];
    }
    [deviceFavourite addTarget:nil action:@selector(removeDeviceFromArray:) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel * deviceName = (UILabel*)[cell viewWithTag:2];
    
    [deviceName setTextAlignment:NSTextAlignmentLeft];

        while ([cell viewWithTag:201]!=nil){
        [[cell viewWithTag:201] removeFromSuperview];
    }
    deviceName.text = selectedDevice.label;

    [[cell viewWithTag:99] removeFromSuperview];

    
}

-(void)removeDeviceFromArray:(UIButton*)sender{

    
    UIButton *button = (UIButton *)sender;
    CGRect buttonFrame = [button convertRect:button.bounds toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonFrame.origin];
    NSLog(@"indexpathrow: %ld", (long)indexPath.row);
    Device *selectedDevice = [_fetchedResultsController objectAtIndexPath:indexPath];
    [self.selectedDevices removeObjectForKey:selectedDevice.deviceID];
    NSUInteger index = 0;
    NSMutableIndexSet *itemsToRemove = [[NSMutableIndexSet alloc] init];
    
    for (NSDictionary*dict in _actionsArray){
        if([[[dict allKeys] firstObject] isEqualToString:selectedDevice.deviceID]){
            [itemsToRemove addIndex:index];
        }
        index++;
    }
    
    [_actionsArray removeObjectsAtIndexes:itemsToRemove];
    NSLog(@"%@", _actionsArray);
    
    [_tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_fetchedResultsController.fetchedObjects count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *CellIdentifier1 = @"MyCell";
    UITableViewCell *cell = nil;
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier1];
    }
    [self configureCell:cell atIndexPath:indexPath];
    
    UIView *myBackView = [[UIView alloc] initWithFrame:cell.frame];
    myBackView.backgroundColor = USBlueColor;
    cell.selectedBackgroundView = myBackView;
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    _selectedDevice = [_fetchedResultsController objectAtIndexPath:indexPath];
    NSString* name  = _selectedDevice.deviceID;

    [self.selectedDevices setObject:_selectedDevice forKey:name];
    
    [self determineSegueWithDevice:_selectedDevice];
    
    self.selectedIndexPath = indexPath;
    
    [tableView reloadData];
}

-(void)determineSegueWithDevice:(Device*)device{
    NSLog(@"Device type: %@", device.productType);
    NSSet *actionsInfo = device.actions;

    if ([device hasStateName:@"brightness"] && ([device hasStateName:@"white balance"])){
        
        [self performSegueWithIdentifier:@"chromacitySceneSegue" sender:nil];
        
    }else if([device hasStateName:@"red"] && [device hasStateName:@"green"] && [device hasStateName:@"blue"] && [device hasStateName:@"demo"] && [device hasStateName:@"brightness"]) {
        
        [self performSegueWithIdentifier:@"rgbSceneSegue" sender:nil];
        
    } else if ([device hasStateName:@"brightness"]){
        
        [self performSegueWithIdentifier:@"dimmingSceneSegue" sender:nil];
        
    } else if ([device getStateValueForStateName:@"roll up"]) {
        
        [self performSegueWithIdentifier:@"blindsSceneSegue" sender:nil];
        
    } else if([[actionsInfo filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"name == 'delayed on' OR name == 'delayed off'"]]count] > 1){
        
        [self performSegueWithIdentifier:@"releSceneSegue" sender:nil];
        
    }
}


-(IBAction)hideTF:(id)sender{
    [self.tf resignFirstResponder];
    
}
-(IBAction)nextStep:(id)sender{
    
    if ([self.selectedDevices count]==0){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"pleaseSelectDevice",nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    if ([self.tf.text isEqualToString:NSLocalizedString(@"hintForRoom",nil)]){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"pleaseAddName", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }
    
    [self performSegueWithIdentifier:@"sceneSettings" sender:self];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"rgbSceneSegue"]) {
        SceneRGBViewController *controller = (SceneRGBViewController*) [segue destinationViewController];
        controller.device = _selectedDevice;
        controller.controller = self;
    } else if ([segue.identifier isEqualToString:@"dimmingSceneSegue"]){
        SceneDimmingViewController *controller = (SceneDimmingViewController*) [segue destinationViewController];
        controller.device = _selectedDevice;
        controller.controller = self;
    } else if ([segue.identifier isEqualToString:@"blindsSceneSegue"]){
        SceneBlindsViewController *controller = (SceneBlindsViewController*) [segue destinationViewController];
        controller.device = _selectedDevice;
        controller.controller = self;
    } else if ([segue.identifier isEqualToString:@"chromacitySceneSegue"]){
        SceneChromacityViewController *controller = (SceneChromacityViewController*) [segue destinationViewController];
        controller.device = _selectedDevice;
        controller.controller = self;
    }
}
- (NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}
-(IBAction)textfieldDidbegin:(id)sender{
    
    if ([self.tf.text isEqualToString:NSLocalizedString(@"hintForRoom",nil) ])
    {
        self.tf.text=@"";
    }
    
    //change back to the text color you use
    [sender setTextColor:[UIColor whiteColor]];
    
    
}
-(IBAction)changeColor:(id)sender{
    
    [self.tf setTextColor:[UIColor whiteColor]];
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView* customView = [[UIView alloc] initWithFrame:CGRectMake(0,-60,300,60)];
    
    // create the label object
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.frame = CGRectMake(0,0,_tableView.frame.size.width,60);
    headerLabel.backgroundColor = [UIColor blackColor];
    headerLabel.font = [UIFont fontWithName:@"Roboto-Thin" size:18];
    headerLabel.textAlignment = NSTextAlignmentCenter;
    
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(elementsTapped)];
    [customView addGestureRecognizer:singleFingerTap];
    
    UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectZero];
    arrow.frame = CGRectMake(_tableView.frame.size.width - 30,20,20,20);
    arrow.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
    
    headerLabel.text = @"Elements";
    headerLabel.textColor = [UIColor whiteColor];
    
    [customView addSubview:headerLabel];
    [customView addSubview:arrow];
    
    return customView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 60;
}

-(void)elementsTapped{
    _tableView.hidden = YES;
}
- (IBAction)elementsButtonTapped:(id)sender {
    _tableView.hidden = NO;
}

-(NSDictionary*) storeToJSON{
    NSMutableDictionary* data = [NSMutableDictionary new];
    NSMutableArray* actions = [NSMutableArray new];
    
    [data setObject:self.tf.text forKey:@"label"];

    [data setObject:_actionsArray forKey:@"actions"];

    if (self.scene == nil){
        return data;
    }
    [data setValue:_scene.sceneID forKey:@"id"];
    return data;
}
- (IBAction)saveScene:(id)sender {
    if(self.tf.text == nil || [self.tf.text isEqualToString:@""]){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"nameError", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    NSDictionary* json = [self storeToJSON];
    
    [[SYAPIManager sharedInstance] postSceneWithID:_scene.sceneID withDict:json success:^(AFHTTPRequestOperation *operation, id response) {
        
        [[self navigationController]popViewControllerAnimated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"cannotAddScene", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    name = textField.text;
}

@end
