//
//  DeviceSchedule+CoreDataProperties.m
//  
//
//  Created by Tom Odler on 24.08.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "DeviceSchedule+CoreDataProperties.h"

@implementation DeviceSchedule (CoreDataProperties)

@dynamic devicescheduleID;
@dynamic label;
@dynamic devices;

@end
