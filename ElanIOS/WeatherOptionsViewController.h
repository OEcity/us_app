//
//  WeatherOptionsViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 04.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeatherOptionsViewController : UIViewController<UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource>

@end
