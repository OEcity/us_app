//
//  AddFloorplanViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/1/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYContainerViewController.h"
#import "Floorplan.h"
#import "PickerViewController.h"
@interface SYAddFloorplanViewController : UIViewController < UIPickerViewDataSource, UIPickerViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, PickerViewDelegate>


@property (nonatomic, retain) IBOutlet UIImage * imageData;
@property (nonatomic, retain) IBOutlet UIImageView * sourceImage;
@property (nonatomic, retain) IBOutlet UITextField * name;
@property (nonatomic, retain) IBOutlet UIButton * save;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * actInd;
@property (nonatomic, retain) IBOutlet UIPickerView * pickerView;
@property (nonatomic, retain) SYContainerViewController* containerViewController;
@property (nonatomic, retain) Floorplan* floorplan;
@property (nonatomic, retain) NSOperationQueue *operationQueue;
@property BOOL fromCamera;
@property (weak, nonatomic) IBOutlet UIButton *leftBottomButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (nonatomic, retain) PickerViewController * pickerViewController;


@end
