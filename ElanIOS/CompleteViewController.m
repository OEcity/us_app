//
//  CompleteViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 07.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "CompleteViewController.h"

@interface CompleteViewController ()

@end

@implementation CompleteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
        [NSTimer scheduledTimerWithTimeInterval:4 target:self selector:@selector(call) userInfo:nil repeats: YES];
}

-(void)call{
    [self performSegueWithIdentifier:@"start" sender:nil];}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
