//
//  USTextFieldSubClass.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 27.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "USTextFieldSubClass.h"
#import "Constants.h"

@implementation USTextFieldSubClass


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [self setBackgroundColor:([UIColor blackColor])];
    
    CALayer *TopBorder = [CALayer layer];
    TopBorder.frame = CGRectMake(0.0f, rect.size.height - 1.0f, rect.size.width, 1.0f);
    TopBorder.backgroundColor = USBlueColor.CGColor;
    [self.layer addSublayer:TopBorder];
    
    [self setNeedsDisplay];
    
}

- (id)initWithCoder:(NSCoder *)inCoder {
    if (self = [super initWithCoder:inCoder]) {
//        self.delegate = self;
        [self setBackgroundColor:([UIColor blackColor])];
        [self setTextColor: [UIColor whiteColor]];
        [self setTintColor: [UIColor whiteColor]];
        [self setBorderStyle: UITextBorderStyleNone];
        
        UIFont *customFont = [UIFont fontWithName:@"Roboto-Light" size:17];
        [self setFont:customFont];
        
    }
    return self;
}


@end
