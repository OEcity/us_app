//
//  MainMenuPVC.m
//  SAM_11_NewDesign
//
//  Created by admin on 06.07.16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "MainMenuPVC.h"
#import "UIViewController+RevealViewControllerAddon.h"
#import "SYCoreDataManager.h"
#import "AppDelegate.h"

@interface MainMenuPVC ()

@property (weak, nonatomic) IBOutlet UIPageControl *refToPageControll;
@property (nonatomic, retain) NSMutableArray *orderedArray;

@end

@implementation MainMenuPVC

//PETR TEST COMMIT

//REQUIRED! CONF PAGE VIEW AND NAVIGATION BAR APPEARENCE
-(void)viewDidLoad{
    [super viewDidLoad];
    
    [super setPageControl:_refToPageControll];
    
    [self refreshPVC:@"self"];
    
    //old view controller instances, adding only for hint
    //[self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Rooms"]];
    //[self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Cameras"]];
    //[self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Alarmy"]];
    //[self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Energy"]];
    //[self addChildViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"Intercom"]];
    
    //Configure navigation bar apperence
    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
    [self setRevealViewNavigationBarHidden:YES animated:NO];
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
}

//REQUIRED!
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.mainMenuPVC = self;
    
    [self setRevealViewNavigationBarHidden:YES animated:YES];
}

-(UIViewController*)getCurrentViewController{
    _orderedArray = [super currentOrderedContentArray];
    UIViewController*viewController = [_orderedArray objectAtIndex:_refToPageControll.currentPage];
    
    return viewController;
}

-(void)refreshPVC:(id)sender{
    NSLog(@"refresh PVC sender: %@",sender);
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSMutableArray *futureArray = [super futureOrderedContentArray];
    
    [futureArray removeAllObjects];
    
    [futureArray addObject:[self.storyboard instantiateViewControllerWithIdentifier:@"Rooms"]];
    
    if([[[SYCoreDataManager sharedInstance] getAllCameras] count] > 0)
        [futureArray addObject:[self.storyboard instantiateViewControllerWithIdentifier:@"Cameras"]];
    
        [futureArray addObject:[self.storyboard instantiateViewControllerWithIdentifier:@"Scenes"]];
    
    if(app.itercomEnabled){
        [futureArray addObject:[self.storyboard instantiateViewControllerWithIdentifier:@"Intercom"]];
    }
    
    if(app.hasOPWeather || app.hasGIOMWeather){
        [futureArray addObject:[self.storyboard instantiateViewControllerWithIdentifier:@"Weather"]];
    }
    
    [super applyFutureStructureWithListedPageIndex:0];
    
    if([futureArray count] > 1){
        [super setLooped:YES];
    } else {
        [super setLooped:NO];
    }
    
}



@end
