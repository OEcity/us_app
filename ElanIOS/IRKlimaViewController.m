//
//  IRKlimaViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 06.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "IRKlimaViewController.h"
#import "IRAction.h"
#import "Constants.h"
#import "SYAPIManager.h"
#import "Util.h"

@interface IRKlimaViewController ()
@property (nonatomic) NSMutableArray*removedActions;
@property (nonatomic) NSMutableArray*addedActions;
@property (nonatomic) NSMutableArray *deviceDict;
@property BOOL saved;
@end

@implementation IRKlimaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setGrayButtons];
    [self setActiveButtons];
    
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self cancelable:NO];
    
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveDevice)];
    
    if(_inSettings){
        [self navigationItem].rightBarButtonItem = saveButton;
        _deviceDict = [NSMutableArray new];
    }
    // Do any additional setup after loading the view.
    
    for(IRAction *action in _device.actions){
        NSLog(@"%@ ir LED: %@", action.name, action.irLed);
    }
    
    _removedActions = _addedActions = [NSMutableArray new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _saved = NO;
    [self setActiveButtons];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if(!_saved)
        [_device.managedObjectContext rollback];
    
}

- (IBAction)sendAction:(id)sender {
    NSString *action = [self getActionName:sender];
    
    IRAction *existingAction = nil;
    if(_inSettings){
        
        for(IRAction*deviceAction in _device.actions){
            if([action isEqualToString: deviceAction.name]){
                existingAction = deviceAction;
            }
        }
        
        [_loaderDialog showWithLabel:@"Waiting for IR Code" ];
        
        [[SYAPIManager sharedInstance] saveIRActionWithSuccessToElan:_device.elan success:^(AFHTTPRequestOperation *operation, id response) {
            [_loaderDialog hide];
            NSDictionary *responseDict = response;
            NSLog(@"%@",responseDict);
            
            NSLog(@"%@", _device.led);
            
            if(_device.led.intValue == 0){
                [self showActiocSheet];
            }
            
            if(existingAction == nil){
                IRAction *action = [[SYCoreDataManager sharedInstance] createEntityIRActionInContext:_device.managedObjectContext];
                action.type = nil;
                action.irLed = _device.led;
                action.irCodes = [NSKeyedArchiver archivedDataWithRootObject:[responseDict objectForKey:@"ir code"]];
                action.name = [self getActionName:sender];
                
                [_device addActionsObject:action];
                [_addedActions addObject:action];
            } else {
                for(IRAction*ac in _device.actions){
                    if([existingAction.name isEqualToString: ac.name]){
                        ac.irCodes = [NSKeyedArchiver archivedDataWithRootObject:[responseDict objectForKey:@"ir code"]];
                    }
                }
            }
            
            
            [self setActiveButtons];
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [_loaderDialog hide];
            NSLog(@"Error: %@", [error localizedDescription]);
            
        }];
        
        
    } else {
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNull alloc]init],action, nil];
        
        [[SYAPIManager sharedInstance] putDeviceAction:dict device:_device success:^(AFHTTPRequestOperation *operation, id response) {
            NSLog(@"IR SENT");
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", [error localizedDescription]);
            if(error.code == 503){
                
            }
        }];
    }
}

-(void)showActiocSheet{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Select LED" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"1", @"2", @"3", nil];
    
    [sheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    _device.led = [NSNumber numberWithInteger:buttonIndex +1];
    
}

-(void)saveDevice{
    _saved = YES;
    NSDictionary *dict = [Util serializeIRDevice:_device];
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", @"")];
    
    [[SYAPIManager sharedInstance] updateIRDeviceWithDict:dict toElan:_device.elan success:^(AFHTTPRequestOperation *operation, id response) {
        NSLog(@"Device updated!");
        [_loaderDialog hide];
        NSInteger currentIndex = [self.navigationController.viewControllers indexOfObject:self];
        if( currentIndex-2 >= 0 ) {
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:currentIndex-2] animated:YES];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error update device: %@", [error localizedDescription]);
        [_loaderDialog hide];
    }];
    
}

-(void)longPressOnButton:(UILongPressGestureRecognizer*)lpgr{
    UIButton *button = (UIButton*)[lpgr view];
    if(button != nil){
        IRAction *actionToDelete = nil;
        for(IRAction *action in _device.actions){
            if([action.name isEqualToString: [self getActionName:button]]){
                actionToDelete = action;
            }
        }
        
        if(actionToDelete != nil){
            [_removedActions addObject:actionToDelete];
            
            [_device removeActionsObject:actionToDelete];
            
            [self setActiveButtons];
        }
    }
    
    
}


-(void)setGrayButtons{
    for(UIButton*button in _buttonCollection){
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressOnButton:)];
        lpgr.minimumPressDuration = 0.5;
        lpgr.delegate = self;
        
        if(_inSettings){
            [button addGestureRecognizer:lpgr];
        }
        switch (button.tag) {
                
            case 1:
                [button setImage: [UIImage imageNamed:@"on_off_nastaveni.png"]forState:UIControlStateNormal];
                
                button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                break;
            case 2:
                [button setImage: [UIImage imageNamed:@"mute_nastaveni.png"]forState:UIControlStateNormal];
                
                button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                break;
            case 3:
                [button setTitleColor:USGrayColor forState:UIControlStateNormal];
                break;
            case 4:
                [button setImage: [UIImage imageNamed:@"info_nastaveni.png" ]forState:UIControlStateNormal];

                button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                break;
            case 11:
                [button setImage: [UIImage imageNamed:@"klima_kolecko_tlacitka_horni_off_nastaveni2.png" ]forState:UIControlStateNormal];
                break;
            case 12:
                [button setImage: [UIImage imageNamed:@"klima_kolecko_tlacitka_leva_off_nastaveni2.png" ]forState:UIControlStateNormal];
                break;
            case 13:
                [button setImage: [UIImage imageNamed:@"klima_kolecko_tlacitka_prava_off_nastaveni2.png" ]forState:UIControlStateNormal];
                break;
            case 14:
                [button setImage: [UIImage imageNamed:@"klima_kolecko_tlacitka_spodni_off_nastaveni2.png" ]forState:UIControlStateNormal];
                break;
            case 15:
                [button setImage:[UIImage imageNamed:@"klima_vypnout_seda.png"] forState:UIControlStateNormal];
                
                button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                break;
            case 21:
                [button setImage: [UIImage imageNamed:@"klima_nocni_OFF.png" ]forState:UIControlStateNormal];
                button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 22:
                [button setImage: [UIImage imageNamed:@"klima_odvlhceni_OFF.png" ]forState:UIControlStateNormal];
                button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 23:
                [button setImage: [UIImage imageNamed:@"klima_ventilace_OFF.png" ]forState:UIControlStateNormal];
                button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 24:
                [button setImage: [UIImage imageNamed:@"klima_otaceni_klapek.png" ]forState:UIControlStateNormal];
                button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 25:
                [button setImage: [UIImage imageNamed:@"klima_casovac_OFF.png" ]forState:UIControlStateNormal];
                button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 26:
                [button setImage: [UIImage imageNamed:@"klima_sipka_dolu_nastaveni.png" ]forState:UIControlStateNormal];
                button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 27:
                [button setImage: [UIImage imageNamed:@"klima_sipka_nahoru_nastaveni.png" ]forState:UIControlStateNormal];
                button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
        }
    }
}

-(void)setActiveButtons{
    for(UIButton *button in _buttonCollection){
        
        for(IRAction *action in _device.actions){
            if ([[self getActionName:button] isEqualToString:action.name]) {
                switch (button.tag) {
                        
                    case 1:
                        [button setImage: [UIImage imageNamed:@"on_off.png"]forState:UIControlStateNormal];
                        
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        break;
                    case 2:
                        [button setImage: [UIImage imageNamed:@"mute.png"]forState:UIControlStateNormal];
                        
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        break;
                    case 3:
                        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                        break;
                    case 4:
                        [button setImage: [UIImage imageNamed:@"info.png" ]forState:UIControlStateNormal];
                        
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        break;
                    case 11:
                        [button setImage: [UIImage imageNamed:@"klima_kolecko_tlacitka_horni_off2.png" ]forState:UIControlStateNormal];
                        break;
                    case 12:
                        [button setImage: [UIImage imageNamed:@"klima_kolecko_tlacitka_leva_off2.png" ]forState:UIControlStateNormal];
                        break;
                    case 13:
                        [button setImage: [UIImage imageNamed:@"klima_kolecko_tlacitka_prava_off2.png" ]forState:UIControlStateNormal];
                        break;
                    case 14:
                        [button setImage: [UIImage imageNamed:@"klima_kolecko_tlacitka_spodni_off2.png" ]forState:UIControlStateNormal];
                        break;
                    case 15:
                        [button setImage:[UIImage imageNamed:@"klima_vypnout_bila.png"] forState:UIControlStateNormal];
                        
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        break;
                    case 21:
                        [button setImage: [UIImage imageNamed:@"klima_nocni_ON.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 22:
                        [button setImage: [UIImage imageNamed:@"klima_odvlhceni_ON.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 23:
                        [button setImage: [UIImage imageNamed:@"klima_ventilace_ON.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 24:
                        [button setImage: [UIImage imageNamed:@"klima_otaceni_klapek_ON.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 25:
                        [button setImage: [UIImage imageNamed:@"klima_casovac_ON.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 26:
                        [button setImage: [UIImage imageNamed:@"klima_sipka_dolu.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 27:
                        [button setImage: [UIImage imageNamed:@"klima_sipka_nahoru.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                }

                break;
            } else {
            
                switch (button.tag) {
                        
                    case 1:
                        [button setImage: [UIImage imageNamed:@"on_off_nastaveni.png"]forState:UIControlStateNormal];
                        
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        break;
                    case 2:
                        [button setImage: [UIImage imageNamed:@"mute_nastaveni.png"]forState:UIControlStateNormal];
                        
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        break;
                    case 3:
                        [button setTitleColor:USGrayColor forState:UIControlStateNormal];
                        break;
                    case 4:
                        [button setImage: [UIImage imageNamed:@"info_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        break;
                    case 11:
                        [button setImage: [UIImage imageNamed:@"klima_kolecko_tlacitka_horni_off_nastaveni2.png" ]forState:UIControlStateNormal];
                        break;
                    case 12:
                        [button setImage: [UIImage imageNamed:@"klima_kolecko_tlacitka_leva_off_nastaveni2.png" ]forState:UIControlStateNormal];
                        break;
                    case 13:
                        [button setImage: [UIImage imageNamed:@"klima_kolecko_tlacitka_prava_off_nastaveni2.png" ]forState:UIControlStateNormal];
                        break;
                    case 14:
                        [button setImage: [UIImage imageNamed:@"klima_kolecko_tlacitka_spodni_off_nastaveni2.png" ]forState:UIControlStateNormal];
                        break;
                    case 15:
                        [button setImage:[UIImage imageNamed:@"klima_vypnout_seda.png"] forState:UIControlStateNormal];
                        
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        break;
                    case 21:
                        [button setImage: [UIImage imageNamed:@"klima_nocni_OFF.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 22:
                        [button setImage: [UIImage imageNamed:@"klima_odvlhceni_OFF.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 23:
                        [button setImage: [UIImage imageNamed:@"klima_ventilace_OFF.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 24:
                        [button setImage: [UIImage imageNamed:@"klima_otaceni_klapek.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 25:
                        [button setImage: [UIImage imageNamed:@"klima_casovac_OFF.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 26:
                        [button setImage: [UIImage imageNamed:@"klima_sipka_dolu_nastaveni.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 27:
                        [button setImage: [UIImage imageNamed:@"klima_sipka_nahoru_nastaveni.png" ]forState:UIControlStateNormal];
                        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                }

                
            }
        }
    }
}

-(NSString*)getActionName:(UIButton*)button{
    NSString*actionName = nil;
    switch (button.tag) {
        case 1:
            actionName = @"power_off";
            break;
        case 2:
            actionName = @"mute";
            break;
        case 3:
            actionName = @"exit";
            break;
        case 4:
            actionName = @"info";
            break;
        case 11:
            actionName = @"arrow_up";
            break;
        case 12:
            actionName = @"arrow_left";
            break;
        case 13:
            actionName = @"arrow_right";
            break;
        case 14:
            actionName = @"arrow_down";
            break;
        case 15:
            actionName = @"ok";
            break;
        case 21:
            actionName = @"button_one";
            break;
        case 22:
            actionName = @"button_two";
            break;
        case 23:
            actionName = @"button_three";
            break;
        case 24:
            actionName = @"button_four";
            break;
        case 25:
            actionName = @"button_five";
            break;
        case 26:
            actionName = @"channel_up";
            break;
        case 27:
            actionName = @"channel_down";
            break;
            

        default:
            actionName = @"";
            break;
    }
    
    return actionName;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
