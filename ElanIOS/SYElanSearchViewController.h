//
//  SYElanSearchViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 1/17/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchService.h"
#import "SYBaseViewController.h"
@interface SYElanSearchViewController : SYBaseViewController <SearchServiceDelegate, UITabBarDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *loadingTopView;
@property (nonatomic, retain) UILabel * noElan;
@property (nonatomic, retain) NSMutableArray * elanArray;
@property (nonatomic, retain) NSArray * savedElans;
@property (nonatomic, retain) Elan * selectedElan;
//@property (nonatomic, retain) NSTimer * broadcastTimer;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loadingTopViewHeightConstrint;
@property (weak, nonatomic) IBOutlet UILabel *lSearchingElanTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lTitle;
@property (weak, nonatomic) IBOutlet UILabel *lnoElan;


@end
