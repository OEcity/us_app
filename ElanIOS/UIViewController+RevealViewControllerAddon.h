//
//  UIViewController+RevealViewControllerAddon.h
//  SAM_10_reveal
//
//  Created by admin on 01.07.16.
//  Copyright © 2016 admin. All rights reserved.
//

/*
 *  This category purpose is to add support for side menu and view to all view controllers in entire app.
 */

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface UIViewController (RevealViewControllerAddon)
/*
 *  @property settingsButton    contains reference to settings button. This is in runtime provided property. Be careful.
 */
@property (strong, nonatomic) UIBarButtonItem* settingsButton;

/*
 *  @property coverView         This view is behind almost all the time. Only in case of revealing sidebar menu, this view is brought to top and used to handle gestures.
 */
@property (strong, nonatomic) UIView* coverView;

/*
 *  This method adds button with specified image to navigation bar if exists. If vew controller is not situated in navigation conroller and/or nil image was passed method is terminated instantly.
 *
 *  @param  image   image for navigation button
 */
-(void)addButtonToNaviagationControllerWithImage:(UIImage *)image;

/*
 *  This method reveals sidebar menu. This is target of bar button item added by addButtonToNaviagationControllerWithImage: method.
 */
-(void)revealMenuView;

/*
 *  This method hide sidebar menu. This is target of bar button item added by addButtonToNaviagationControllerWithImage: method.
 */
-(void)hideMenuView;

/*
 *  This method disables all übersuperview of this view controller content with exception for cover view and add reveal view controller gestures and change button target
 */
-(void)presentationInteractionDisable;

/*
 *  This method enables all übersuperview of this view controller content with exception for cover view and add reveal view controller gestures and change button target
 */
-(void)presentationInteractionEnable;

/*
 *  This method is used to manage navigation controller of your SWRevealViewController
 *
 *  @param  hidden      if NO navigation bar is not hidden, if YES navigation bar is hidden
 *  @param  animated    if YES state change is made with animation, otherwise NO
 */
-(void)setRevealViewNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated;
@end
