//
//  RoomTypeLoader.m
//  iHC-MIRF
//
//  Created by Vlastimil Venclik on 21.11.13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "RoomTypeLoader.h"
#import "Util.h"
#import "RoomTypeResponse.h"

@implementation RoomTypeLoader

-(void)LoadTypes{
    self.types = [[NSMutableArray alloc] init];
    self.url = [[URLConnector alloc] initWithAddressAndLoader:[Util getConfigUrl:@"room types"] activityInd:nil];
    RoomTypeResponse * scenesResponse = [[RoomTypeResponse alloc] init];
    [self.url setParser:scenesResponse];
    [self.url setConnectionListener:self];
    [self.url ConnectAndGetData];
    
}



-(void)connectionResponseOK:(NSObject *)returnedObject response:(id<Response>)responseType{
    
    if ([responseType isKindOfClass:[RoomTypeResponse class]] ){
        self.types = (NSMutableArray*) returnedObject;
        dispatch_async(dispatch_get_main_queue(), ^{
            NSDictionary *theInfo = [NSDictionary dictionaryWithObjectsAndKeys:self.types,@"roomTypes", nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"roomTypeLoaderComplete" object:self userInfo:theInfo];
        });
    }
}




-(void) connectionResponseFailed:(NSInteger)code{
    NSLog(@"failed");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"roomTypeLoaderComplete" object:self userInfo:nil];
    
}

@end
