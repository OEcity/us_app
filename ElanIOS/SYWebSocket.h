//
//  SYWebSocket.h
//  ElanIOS
//
//  Created by Vratislav Zima on 6/5/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SRWebSocket.h>
#import "URLConnectionListener.h"
#import "URLConnector.h"
#import <SystemConfiguration/SystemConfiguration.h>
@interface SYWebSocket : NSObject <SRWebSocketDelegate>



@property (nonatomic, retain) SRWebSocket* _webSocket;
//@property (nonatomic, retain) NSTimer * timer;
@property SCNetworkReachabilityRef reachability;
@property BOOL connected;
@property BOOL connecting;


+(SYWebSocket*) sharedInstance;
- (void) reconnect;
- (void) connect;
- (void) disconnect;

@end
