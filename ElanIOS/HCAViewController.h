//
//  HCAViewController.h
//  US App
//
//  Created by Tom Odler on 24.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device+State.h"

@interface HCAViewController : UIViewController<NSFetchedResultsControllerDelegate>
@property Device* device;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *modesOffCollection;
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *modesOnCollection;


@end
