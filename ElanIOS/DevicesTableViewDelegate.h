//
//  DevicesTableView.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 12/02/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "URLConnector.h"
#import "RGBViewController.h"
#import "IntensityConfigurationViewController.h"
#import "DeviceActionsViewController.h"
#import <CoreData/CoreData.h>

typedef enum : NSUInteger {
    DeviceListModeRoom = 0,
    DeviceListModeFavourite
} DeviceListMode;

@interface DevicesTableViewDelegate : NSObject <UITableViewDataSource, UITableViewDelegate, URLConnectionListener, NSFetchedResultsControllerDelegate>
@property (nonatomic,retain) IBOutlet UIView * view;
@property (nonatomic,retain) NSMutableArray * devices;
@property (nonatomic,retain) NSMutableArray * rgbControllers;
@property (nonatomic,retain) UITableView* tableView;
@property (nonatomic,strong) NSString* roomID;
@property (retain) RGBViewController * rgb;
@property (retain) IntensityConfigurationViewController * intensity;
@property (strong, nonatomic) DeviceActionsViewController * actions;
@property (nonatomic) DeviceListMode deviceListMode;

@property(nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property(nonatomic, strong) NSFetchedResultsController *fetchedResultsController2;

-(id)initWithRoomID:(NSString*)roomID;
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer;



@end
