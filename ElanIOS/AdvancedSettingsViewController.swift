//
//  AdvancedSettingsViewController.swift
//  iHC-MIIRF
//
//  Created by Tom Odler on 13.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

import UIKit

var stringArray = [String]()

class AdvancedSettingsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        stringArray = [NSLocalizedString("cameras", comment: ""), "Intercom", NSLocalizedString("heating", comment: ""), "Audio/Video", NSLocalizedString("weather",comment: ""), NSLocalizedString("others", comment: "")]
        
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    print(devices?.fetchedObjects?.count)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stringArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell :UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyCell")!
        
        let label : UILabel = cell.viewWithTag(1) as! UILabel
        label.text = stringArray[indexPath.row]
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.row){
        case 0:
            self.performSegue(withIdentifier: "cameraSegue", sender: nil)
            break
        case 1:
            self.performSegue(withIdentifier: "intercomSettings", sender: nil)
            break
        case 2:
            self.performSegue(withIdentifier: "heatCoolSettings", sender: nil)
            break
        case 3:
            self.performSegue(withIdentifier: "IRConfigSegue", sender: nil)
            break
        case 4:
            self.performSegue(withIdentifier:"weatherSettings", sender:nil)
            break
        case 5:
            
            break
        default:
            break
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
