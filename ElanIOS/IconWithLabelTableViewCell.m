//
//  IconWithLabelTableViewCell.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/8/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "IconWithLabelTableViewCell.h"

@implementation IconWithLabelTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
