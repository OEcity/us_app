//
//  BlindsViewController.h
//  US App
//
//  Created by Tom Odler on 16.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"

@interface BlindsViewController : UIViewController<NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) NSFetchedResultsController* devices;
@property (weak, nonatomic) IBOutlet UIImageView *imgZaluzie;
@property (weak, nonatomic) IBOutlet UIImageView *imgZaluzieUp;
@property ( nonatomic, retain ) Device *device;
@end
