//
//  UIViewController+RevealViewControllerAddon.m
//  SAM_10_reveal
//
//  Created by admin on 01.07.16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "UIViewController+RevealViewControllerAddon.h"
#import <objc/runtime.h>

static void const *buttonKey = 0x00000000;
static void const *viewKey = 0x00000001;

@implementation UIViewController (RevealViewControllerAddon)

@dynamic settingsButton;
@dynamic coverView;

-(void)addButtonToNaviagationControllerWithImage:(UIImage *)image{
    UINavigationController *navigationController = self.navigationController;
    
    if (navigationController && image) {
        
        //Add button to navigation bar
        self.settingsButton = [[UIBarButtonItem alloc] initWithImage:image style: UIBarButtonItemStylePlain target:self action: @selector(revealMenuView)];
        [[self navigationItem] setRightBarButtonItem:self.settingsButton];
        //Add event to button if reveals exists
        SWRevealViewController *revealViewController = self.revealViewController;
        if ( revealViewController )
        {
            [self.settingsButton setTarget: self];
            [self.settingsButton setAction: @selector(revealMenuView)];
        }
    }
}

-(void)prepareCoverView{
    self.coverView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view addSubview:self.coverView];
    [self.view sendSubviewToBack:self.coverView];
}

-(void)coverViewSetCurrentFrame{
    self.coverView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

-(void)revealMenuView{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [revealViewController rightRevealToggle:self.settingsButton];
        
        [self presentationInteractionDisable];
    }
}

-(void)hideMenuView{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [revealViewController rightRevealToggle:self.settingsButton];
        [self presentationInteractionEnable];
    }
}

-(void)presentationInteractionDisable{
    if (!self.coverView) {
        [self prepareCoverView];
    }else{
        [self coverViewSetCurrentFrame];
    }
    
    if (self.settingsButton) {
        [self.settingsButton setTarget: self];
        [self.settingsButton setAction: @selector( hideMenuView)];
    }
    
    for (UIView *sv in [self.view subviews]) {
        [sv setUserInteractionEnabled:NO];
    }
    
    [self.view bringSubviewToFront:self.coverView];
    [self.coverView setUserInteractionEnabled:YES];
    [self.coverView addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    [self.coverView addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

-(void)presentationInteractionEnable{
    if (!self.coverView) {
        [self prepareCoverView];
    }else{
        [self coverViewSetCurrentFrame];
    }
    
    if (self.settingsButton) {
        [self.settingsButton setTarget: self];
        [self.settingsButton setAction: @selector( revealMenuView)];
    }
    
    for (UIView *sv in [self.view subviews]) {
        [sv setUserInteractionEnabled:YES];
    }
    
    [self.view sendSubviewToBack:self.coverView];
    [self.coverView setUserInteractionEnabled:NO];
    [self.coverView removeGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    [self.coverView removeGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

-(void)setRevealViewNavigationBarHidden:(BOOL)hidden animated:(BOOL)animated{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        if (revealViewController.navigationController) {
            if (revealViewController.navigationController.navigationBar.isHidden != hidden){
                [revealViewController.navigationController setNavigationBarHidden:hidden animated:animated];
            }
        }
    }
}

#pragma mark - dynamic properties getters and setters
-(void)setSettingsButton:(UIBarButtonItem *)settingsButton{
    objc_setAssociatedObject(self, buttonKey, settingsButton, OBJC_ASSOCIATION_RETAIN);
}
-(UIBarButtonItem *)settingsButton{
    return objc_getAssociatedObject(self, buttonKey);
}

-(void)setCoverView:(UIView *)coverView{
    objc_setAssociatedObject(self, viewKey, coverView, OBJC_ASSOCIATION_RETAIN);
}
-(UIView *)coverView{
    return objc_getAssociatedObject(self, viewKey);
}
@end
