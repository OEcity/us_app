//
//  IRDeviceDetailViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 15.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "IRDeviceDetailViewController.h"
#import "IRControllerViewController.h"
#import "Util.h"
#import "SYAPIManager.h"
#import "Constants.h"
#import "ResourceTiles.h"
#import "SYCoreDataManager.h"
#import "SYDataLoader.h"
#import "IRKlimaViewController.h"

@interface IRDeviceDetailViewController (){
    NSString *name;
    NSString*type;
    
    Elan*myElan;
    
    UITableView*elansTableView;
    UITableView*irLEDTableView;
    
    BOOL elansSelected;
    BOOL irLedSelected;
    
    NSArray*elans;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbvHeight;
@property (nonatomic, retain) NSNumber *myIRLED;
@property (nonatomic, retain) NSArray* typesArray;
@property (weak, nonatomic) IBOutlet UIButton *nextButton;

@end

@implementation IRDeviceDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"typesList" ofType:@"plist"]];
    
    _typesArray = [dict objectForKey:@"irDeviceTypes"];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.scrollEnabled = YES;
    
    _myIRLED = [NSNumber numberWithInt:1];
    
    
    elans = [[SYCoreDataManager sharedInstance] getAllIRElans];
   
    irLedSelected = elansSelected = NO;
    
    if(_device != nil){
        name = _device.label;
        type = _device.type;
        _myIRLED = _device.led;
        myElan = _device.elan;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"IR_klima"]){
        IRKlimaViewController *controller = (IRKlimaViewController*) segue.destinationViewController;
        controller.device = sender;
        controller.inSettings = true;
    } else {
    IRControllerViewController *controller = (IRControllerViewController*) segue.destinationViewController;
    controller.device = sender;
    controller.inSettings = true;
    }
}

- (IBAction)storeDevice:(id)sender {
    IRDevice *localDevice = _device;
    
    if([name isEqualToString:@""]){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"pleaseAddName",nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if(type == nil || [type isEqualToString:@""]){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"selectProductType",nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;

    }
    
    for(Device*device in [[SYCoreDataManager sharedInstance]getAllDevices]){
        if([localDevice.label isEqualToString:device.label])continue;
        NSLog(@"Device: '%@', myString:'%@'", device.label, name);
        if([[device.label stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:[name stringByReplacingOccurrencesOfString:@" " withString:@""]]){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"duplicateName",nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
    }
    
    if(localDevice != nil){
        localDevice.label = name;
        localDevice.type = type;
        localDevice.led = _myIRLED;
        if(localDevice.led != _myIRLED){
            for(IRAction*ac in _device.actions){
                ac.irLed = _myIRLED;
            }
        }
    } else {
        localDevice = [[SYCoreDataManager sharedInstance] createEntityIRDevice];
        localDevice.label = name;
        localDevice.led = _myIRLED;
        
        [[NSUserDefaults standardUserDefaults] setObject: _myIRLED forKey:@"irDeviceLed"];
        
        Elan*elan = [[SYCoreDataManager sharedInstance] getELANwithMAC:myElan.mac inContext:localDevice.managedObjectContext];
        
        localDevice.elan = elan;
        localDevice.type = type;
        _device = localDevice;
    }
    
    if([type containsString:@"clima"]){
        [self performSegueWithIdentifier:@"IR_klima" sender:localDevice];
    } else {
        [self performSegueWithIdentifier:@"IRDeviceConfig" sender:localDevice];
    }

}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == elansTableView) return elans.count+1;
    return 4;
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1 && indexPath.row == 1 && elansSelected) return [elans count]*50+50;
    if(tableView.tag == 1 && indexPath.row == 3 && irLedSelected) return 3*50+50;
    
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        switch (indexPath.row){
            case 0: {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell"];
                UITextField*tf = [[[cell contentView] subviews]firstObject];
                
                NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"enterName", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                
                tf.attributedPlaceholder = namePlaceHolder;
                if(name){
                    tf.text = name;
                }
                tf.delegate = self;
                return cell;
            }break;
            case 1:{
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                
                elansTableView = tbv;
                
                tbv.delegate = self;
                tbv.dataSource = self;
                [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
                return cell;
            }break;
            case 2:{
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"roomsCell"forIndexPath:indexPath];
                
                UICollectionView *collection = nil;
                for(UIView *view in [[cell contentView] subviews]){
                    if ([view isKindOfClass:[UICollectionView class]]){
                        collection = (UICollectionView*)view;
                    }
                }
                if(collection){
                    collection.delegate = self;
                    collection.dataSource = self;
                    
                }
                return cell;
            }break;
            case 3:{
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                
                irLEDTableView = tbv;
                
                tbv.delegate = self;
                tbv.dataSource = self;
                return cell;
            }break;
                
        }
    } else if(tableView == elansTableView){
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        if(elansSelected){
            line.hidden = YES;
        } else {
            line.hidden = NO;
        }
        

        
        if(elansSelected && indexPath.row>0){
            Elan*elan = [elans objectAtIndex:indexPath.row-1];
            label.text = elan.label;
            img.hidden = YES;
            if(myElan){
                if([myElan.mac isEqualToString:elan.mac]){
                    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            }
        } else {
            if(myElan && !elansSelected){
                label.text = myElan.label;
            } else {
                label.text = @"eLAN";
            }
        }
        
        if(indexPath.row == 0){
            img.hidden = NO;
            
            if(elansSelected){
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            } else {
                img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
            }
        }
        return cell;
        
    } else if(tableView == irLEDTableView){
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        if(irLedSelected){
            line.hidden = YES;
        } else {
            line.hidden = NO;
        }
        
        if(irLedSelected && indexPath.row>0){
            label.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
            img.hidden = YES;
            if(_myIRLED.intValue == label.text.intValue){
                [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
        } else {
            label.text = [NSString stringWithFormat:@"IR LED: %@", _myIRLED];
        }
        
        if(indexPath.row == 0){
            img.hidden = NO;
            
            if(irLedSelected){
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            } else {
                img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
            }
        }
        return cell;
    }
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == elansTableView){
        if(indexPath.row == 0 && !elansSelected){
            elansSelected = YES;
            irLedSelected = NO;
            
            [irLEDTableView reloadData];
            
            [_tableView beginUpdates];
            [_tableView endUpdates];
        } else if(indexPath.row == 0 && elansSelected){
            elansSelected = NO;
            irLedSelected = NO;
            
            [_tableView beginUpdates];
            [_tableView endUpdates];
        } else if(indexPath.row>0){
            myElan = [elans objectAtIndex:indexPath.row-1];
        }
    } else if(tableView == irLEDTableView){
        if(indexPath.row == 0 && !irLedSelected){
            elansSelected = NO;
            irLedSelected = YES;
            
            [elansTableView reloadData];
            
            [_tableView beginUpdates];
            [_tableView endUpdates];
        } else if(indexPath.row == 0 && irLedSelected){
            elansSelected = NO;
            irLedSelected = NO;
            
            [_tableView beginUpdates];
            [_tableView endUpdates];
        } else if(indexPath.row>0){
            _myIRLED = [NSNumber numberWithInteger:indexPath.row];
        }
    }
    
    
    if(indexPath.row == 0)
        [tableView reloadData];
    
    [self selectTBVs];
}

-(void)selectTBVs{
    if(elansSelected){
        elansTableView.layer.borderColor = USBlueColor.CGColor;
        elansTableView.layer.borderWidth = 1.0f;
    } else {
        elansTableView.layer.borderColor = nil;
        elansTableView.layer.borderWidth = 0.0f;
    }
    
    if(irLedSelected){
        irLEDTableView.layer.borderColor = USBlueColor.CGColor;
        irLEDTableView.layer.borderWidth = 1.0f;
    } else {
        irLEDTableView.layer.borderColor = nil;
        irLEDTableView.layer.borderWidth = 0.0f;
    }
}

#pragma mark - UICollectionViewDelegate
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_typesArray count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"roomCell" forIndexPath:indexPath];
    
    
    NSString * iconType = [_typesArray objectAtIndex:indexPath.item];
    NSString * recourceName = [[ResourceTiles getDevicesDict] valueForKey:iconType];
    
    UIImage * iconImage = [UIImage imageNamed:[recourceName stringByAppendingString:@"_off"]];
    UIImage *onIconImage = [UIImage imageNamed:[recourceName stringByAppendingString:@"_on"]];
    if (iconImage==nil){
        iconImage =[UIImage imageNamed:@"jine_off"];
    }
    
    if (onIconImage==nil){
        onIconImage =[UIImage imageNamed:@"jine_on"];
        
    }
    
    UIImageView *img = [[[cell contentView] subviews]firstObject];

    if([iconType isEqualToString:type]){
        [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        img.image = onIconImage;
    } else {
        img.image = iconImage;
    }
    return cell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * roomType = [_typesArray objectAtIndex:indexPath.item];
    NSLog(@"room type: %@", roomType);
    type = roomType;
    
    [collectionView reloadData];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    name = textField.text;
    [textField resignFirstResponder];
    return NO;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    name = textField.text;
    [textField resignFirstResponder];
}


@end
