//
//  ViewController.m
//  RGBViewController
//
//  Created by admin on 21.06.16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "RGBViewController.h"
#import "EFCircularSlider.h"
#import "EFGradientGenerator.h"
#import "SYCoreDataManager.h"
#import "Constants.h"
#import "SYAPIManager.h"
//#import "UIViewController+RevealViewControllerAddon.h"

@interface RGBViewController ()
@property (weak, nonatomic) IBOutlet UIView *topSliderView;
@property (weak, nonatomic) IBOutlet UIView *bottomSliderView;

@property (weak, nonatomic) IBOutlet UILabel *productLabel;

@property (weak, nonatomic) IBOutlet UILabel *topSlideViewValueIndicatorLabel;
@property (strong, nonatomic) EFGradientGenerator *gradientGen;
@property (weak, nonatomic) IBOutlet UIButton *cirkusButton;
@property (weak, nonatomic) IBOutlet UIButton *switchButton;
@property (weak, nonatomic) IBOutlet UILabel *lisghtDescriptionLabel;
@property (strong, nonatomic) EFCircularSlider* circularSlider;
@property (strong, nonatomic) EFCircularSlider* bcircularSlider;
@property (weak, nonatomic) IBOutlet UIButton *automatButton;
@property (nonatomic) BOOL automat;

@property (nonatomic) Byte red;
@property (nonatomic) Byte green;
@property (nonatomic) Byte blue;
@property (nonatomic) Byte intensity;

@property (nonatomic) BOOL cirkus;

@end

@implementation RGBViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
     _deviceName = _device.deviceID;
    [self initFetchedResultsControllerWithHCAID:_deviceName];
    
    _productLabel.text = _device.productType;
    _lisghtDescriptionLabel.text = _device.label;
    
    _cirkus = [[_device getStateValueForStateName:@"demo"] boolValue];
    if(_cirkus){
        _cirkusButton.selected = YES;
    } else {
        _cirkusButton.selected = NO;
    }
    
    _red = [[_device getStateValueForStateName:@"red"] intValue];
    _green = [[_device getStateValueForStateName:@"green"] intValue];
    _blue = [[_device getStateValueForStateName:@"blue"] intValue];
    _intensity = [[_device getStateValueForStateName:@"brightness"] intValue]/2.55;
    

    //Configure navigation bar apperence
    //[self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //TOP SLIDER CONF
    CGRect sliderFrame = CGRectMake(0, 0, _topSliderView.frame.size.width, _topSliderView.frame.size.height);
    _circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    [_circularSlider addTarget:self action:@selector(topSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_circularSlider addTarget:self action:@selector(saveButtonPressed) forControlEvents:UIControlEventEditingDidEnd];
    [_topSliderView addSubview:_circularSlider];
    [_circularSlider pauseAutoredrawing];
    
    CGFloat dash[]={1,15};
    [_circularSlider setHandleRadius:10];
    [_circularSlider setUnfilledLineDash:dash andCount:2];
    [_circularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_circularSlider setUnfilledColor:[UIColor whiteColor]];
    [_circularSlider setFilledColor:[UIColor whiteColor]];
    [_circularSlider setHandleColor:[UIColor whiteColor]];
    [_circularSlider setLineWidth:1];
    [_circularSlider setArcStartAngle:150];
    [_circularSlider setArcAngleLength:240];
    
    
    [_circularSlider setCurrentArcValue:(CGFloat)_intensity forStartAnglePadding:2 endAnglePadding:2];
    
    //BOTTOM SLIDER CONF
    
    EFGradientGenerator *gradient = [[EFGradientGenerator alloc] initWithSize:_bottomSliderView.frame.size];
    [gradient setRadius:80];
    [gradient setSectors:360];
    
    
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0] atSector:36];  //WHITE OfR
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:37];  //WHITE
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:40];  //WHITE
    [gradient addNewColor:[UIColor colorWithRed:1 green:0 blue:1 alpha:1] atSector:90];  //PURPLE
    [gradient addNewColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:1] atSector:135]; //BLUE
    [gradient addNewColor:[UIColor colorWithRed:0 green:1 blue:1 alpha:1] atSector:180]; //AZURE
    [gradient addNewColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:1] atSector:225]; //GREEN
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:0 alpha:1] atSector:260]; //YELLOW
    [gradient addNewColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1] atSector:318]; //RED
    [gradient addNewColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1] atSector:323]; //RED
    [gradient addNewColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0] atSector:324]; //RED OfR
    
    [gradient renderImage];
    _gradientGen = gradient;
    
    CGRect bsliderFrame = CGRectMake(0, 0, _bottomSliderView.frame.size.width, _bottomSliderView.frame.size.height);
    _bcircularSlider = [[EFCircularSlider alloc] initWithFrame:bsliderFrame];
    [_bcircularSlider addTarget:self action:@selector(botSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_bcircularSlider addTarget:self action:@selector(saveButtonPressed) forControlEvents:UIControlEventEditingDidEnd];
    [_bottomSliderView addSubview:_bcircularSlider];
    [_bcircularSlider pauseAutoredrawing];
    
    [_bcircularSlider setImageBackgroundUnfilledLine:YES];
    [_bcircularSlider setLineWidth:8];
    [_bcircularSlider setFilledColor:[UIColor clearColor]];
    [_bcircularSlider setUnfilledLineStrokeBorderWidth:0];
    [_bcircularSlider setUnfilledLineStrokeBorderColor:[UIColor clearColor]];
    [_bcircularSlider setUnfilledLineInnerImage:[gradient renderedImage]];
    [_bcircularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_bcircularSlider setHandleColor:[UIColor whiteColor]];
    [_bcircularSlider setArcStartAngle:130];
    [_bcircularSlider setArcAngleLength:280];
    [_bcircularSlider setHandleRadius:10];
    [_bcircularSlider setHandleBorderSize:2];
    
    [self setColorSliderPosition];
    [self initAutomat];
}

-(void)initAutomat{
    _automatButton.selected = NO;
    _automat = NO;
    
    if([[_device getStateValueForStateName:@"automat"]boolValue]){
        _automatButton.selected = YES;
        _automat = YES;
    }
}

-(void)topSliderValueChanged:(EFCircularSlider*)circularSlider {
    _topSlideViewValueIndicatorLabel.text = [NSString stringWithFormat:@"%.f", [circularSlider getCurrentArcValueForStartAnglePadding:2 endAnglePadding:2]];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:_topSlideViewValueIndicatorLabel.text];
    
    _intensity =  (2.55) * [myNumber intValue];
    
}

-(void)botSliderValueChanged:(EFCircularSlider*)circularSlider {
    UIColor *myColor = [_gradientGen getColorFromAngleFromNorth:([circularSlider angleFromNorth])];
    
    [circularSlider setHandleColor:[_gradientGen getColorFromAngleFromNorth:([circularSlider angleFromNorth])]];
    CGColorRef color = [myColor CGColor];
    CGFloat *components = CGColorGetComponents(color);
    
    _red = (Byte)(components[0]*255);
    _green = (Byte)(components[1]*255);
    _blue = (Byte)(components[2]*255);
    

    NSLog(@"onChange: RED: %d GREEN: %d BLUE: %d", _red, _green, _blue);
}

- (IBAction)cirkusButtonEvent:(id)sender {
    URLConnector * url = [[URLConnector alloc] initWithAddressAndLoader:[@"devices/" stringByAppendingString:_deviceName] activityInd:nil];
    [url setConnectionListener:self];
    NSString * outputString = @"{\"demo\":";
    //    NSLog(@"demo value: %d",device.rgbColor.demo.intValue);
    if ([[_device getStateValueForStateName:@"demo"] boolValue]==YES){
        self.cirkus=NO;
        outputString = [outputString stringByAppendingString:@"false"];
    }else{
        self.cirkus=YES;
        outputString = [outputString stringByAppendingString:@"true"];
    }
    if(_cirkus){
        _cirkusButton.selected = YES;
    } else {
        _cirkusButton.selected = NO;
    }
    
    outputString = [outputString stringByAppendingString:@"}"];
    NSLog(@"output string %@", outputString);
    [url ConnectAndPostData:[outputString dataUsingEncoding:NSUTF8StringEncoding] toElan:_device.elan ];
}

- (IBAction)onOffButtonEvent:(id)sender {
    if (self.intensity==0){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *myIntensity = (NSNumber*)[defaults objectForKey:_deviceName];
        if (myIntensity!=nil){
            self.intensity = myIntensity.intValue;
            [defaults removeObjectForKey:_deviceName];
        }
    }else{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber * intensityNumber = [[NSNumber alloc] initWithInt:_intensity];
        [defaults setObject:intensityNumber forKey:_deviceName];
        [defaults synchronize];
        self.intensity = 0;
    }
    [self saveButtonPressed];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)initFetchedResultsControllerWithHCAID:(NSString*)hcaID {
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"State"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"device.label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.device.deviceID == %@", hcaID];
    [fetchRequest setPredicate:predicate];
    _devices = [[NSFetchedResultsController alloc]
                initWithFetchRequest:fetchRequest
                managedObjectContext:context
                sectionNameKeyPath:nil
                cacheName:nil];
    _devices.delegate = self;
    NSError *error;
    
    if (![_devices performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }else if ([[_devices fetchedObjects] count]>0){
        _device = (Device*)((State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if ([_devices.fetchedObjects count]>0){
        _device = (Device*)((State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
        
        [self syncComplete:_device];
    }
}

-(void)syncComplete:(Device *)device{
    //    device = [CoreDataManager readDevice:@"RFRGB"];
    // Device * newDevice =[CoreDataManager readDevice:@"RFRGB"];
    
    if ([_deviceName isEqualToString:device.deviceID]){
        if ([[device getStateValueForStateName:@"demo"] intValue]==0){
            [self.cirkusButton setSelected:NO];
        }else{
            [self.cirkusButton setSelected:YES];
        }
        if ([[device getStateValueForStateName:@"brightness"] intValue]==0){
            [self.switchButton setTitle:@"ON" forState:UIControlStateNormal];
        }else{
            [self.switchButton setTitle:@"OFF" forState:UIControlStateNormal];
        }
        
        _intensity = [[device getStateValueForStateName:@"brightness"] intValue];
        
        [_circularSlider setCurrentArcValue:(CGFloat)_intensity/(2.55) forStartAnglePadding:2 endAnglePadding:2];
        
        int redC = [[device getStateValueForStateName:@"red"] intValue];
        int greenC = [[device getStateValueForStateName:@"green"] intValue];
        int blueC = [[device getStateValueForStateName:@"blue"] intValue];
        if(redC == 0 && greenC == 0 && blueC == 0){
            redC = greenC = blueC = 255;
        }
        
        _red = (Byte)redC;
        _green = (Byte)greenC;
        _blue = (Byte)blueC;
        
        NSLog(@"\n\n");
        NSLog(@"onSync: RED: %d GREEN: %d BLUE: %d", _red, _green, _blue);
        [self setColorSliderPosition];
        [self initAutomat];
    }
    
}

-(void)saveButtonPressed{
    
    //    NSString * path =[Util getAddressWith:[@"devices/" stringByAppendingString:deviceName]]
    URLConnector * url = [[URLConnector alloc] initWithAddressAndLoader:[@"devices/" stringByAppendingString:_deviceName] activityInd:nil];
    [url setConnectionListener:self];
    NSString * outputString = @"{\"red\":";
    outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"%d", _red]];
    outputString = [outputString stringByAppendingString:@", \"green\":"];
    outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"%d", _green]];
    outputString = [outputString stringByAppendingString:@", \"blue\":"];
    outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"%d", _blue]];
    outputString = [outputString stringByAppendingString:@", \"brightness\":"];
    outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"%d", _intensity]];
    outputString = [outputString stringByAppendingString:@"}"];
    NSLog(@"output string %@", outputString);
    [url ConnectAndPostData:[outputString dataUsingEncoding:NSUTF8StringEncoding] toElan:_device.elan];
    //    [self.view removeFromSuperview];
    
    
}

-(void)setColorSliderPosition{
    if (!_gradientGen || !_bcircularSlider) {
        return;
    }
    
    NSArray *gradientColors = [_gradientGen getSegmentForUIColor:[UIColor colorWithRed:(CGFloat)_red/255.0 green:(CGFloat)_green/255.0 blue:(CGFloat)_blue/255.0 alpha:1.0] ];
    if ([gradientColors count] == 0) {
        [_bcircularSlider setCurrentArcValue:0.0];
    }else{
        NSNumber *number = [gradientColors objectAtIndex:0];
            CGFloat angle = [_gradientGen getAngleFromNorthForSector:[number unsignedIntegerValue]];
            NSLog(@"SettingColor: %lu %f", [number unsignedIntegerValue], angle);
            if ([_bcircularSlider isAngleValidArcAngle:angle]) {
                [_bcircularSlider setAngleFromNorth:angle];
                [self botSliderValueChanged:_bcircularSlider];
            }else{
                [_bcircularSlider setAngleFromNorth:[_bcircularSlider arcStartAngle]];
                [self botSliderValueChanged:_bcircularSlider];
            }
    }

}
- (IBAction)sendAutomat:(id)sender {
    NSDictionary *dict = [[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithBool:!_automat],@"automat", nil];
    
    [[SYAPIManager sharedInstance]putDeviceAction:dict device:_device success:^(AFHTTPRequestOperation *operation, id response) {
        NSLog(@"Automat success");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"automat fail: %@", [error localizedDescription]);
    }];
}
@end

