//
//  FloorPlanViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/28/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYRoomsViewController.h"
#import "SYContainerViewController.h"
#import "LoaderViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "URLConnector.h"
#import <HUDWrapper.h>
@interface SYFloorPlanViewController : UIViewController <UITableViewDataSource, UITableViewDataSource, UIImagePickerControllerDelegate, URLConnectionListener, NSURLConnectionDelegate>

@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;

@property (nonatomic, retain) SYContainerViewController* containerViewController;
@property (nonatomic, retain) NSMutableArray* floorplans;
@property (nonatomic, retain) IBOutlet UITableView* tableView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView* actInd;
@property (nonatomic, retain)  HUDWrapper* loaderDialog;
@property (nonatomic, retain)  NSIndexPath * deletedIndex;
@property (nonatomic, copy) NSString * ContentType;
@property (nonatomic, retain) NSMutableData * receivedData;
@property (nonatomic, copy) NSString * responseURL;
@property (nonatomic, retain) NSMutableArray * loading;
@property (nonatomic, retain) NSOperationQueue *operationQueue;
@property (nonatomic, retain) NSMutableDictionary *actInds;
@property NSInteger statusCode;
@property (weak, nonatomic) IBOutlet UIButton *rightBottomButton;
@property (weak, nonatomic) IBOutlet UIButton *leftBottomButton;
@end
