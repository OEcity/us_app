//
//  MenuViewController.h
//  US App
//
//  Created by Tom Odler on 17.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface MenuViewController : UIViewController
@property (retain) SWRevealViewController *oldController;
@end
