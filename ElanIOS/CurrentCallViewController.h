//
//  CurrentCallViewController.h
//  iHC
//
//  Created by Pavel Gajdoš on 07.09.13.
//  Copyright (c) 2013 Pavel Gajdoš. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LinphoneManager.h"

#import "MotionJpegImageView.h"

//#import "VoIPContactsModel.h"

@interface CurrentCallViewController : UIViewController {

@private IBOutlet UILabel * callerNameLabel;
@private IBOutlet UILabel * callTimeLabel;
    
@private IBOutlet UIButton * speakerButton;
    
@private IBOutlet UIButton * unlockButton;
    
@private IBOutlet UIButton * endButton;
    
@private NSTimer * callDurationTimer;
    
@private
    IBOutlet UIImageView * noStreamView;
    IBOutlet MotionJpegImageView * cameraView;
    
    CGRect originalCameraViewFrame;
    
    NSString * switchCode;
    
}

@property (nonatomic, assign) LinphoneCall * call;

- (IBAction)endCallButtonPressed:(id)sender;
- (IBAction)fullscreenButtonPressed:(id)sender;
- (IBAction)speakerButtonPressed:(UIButton *)sender;

- (IBAction)unlockButtonPressed:(id)sender;

@end
