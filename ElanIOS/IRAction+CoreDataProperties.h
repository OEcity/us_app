//
//  IRAction+CoreDataProperties.h
//  
//
//  Created by Tom Odler on 12.09.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "IRAction.h"

NS_ASSUME_NONNULL_BEGIN

@interface IRAction (CoreDataProperties)

@property (nullable, nonatomic, retain) NSString *id;
@property (nullable, nonatomic, retain) NSData *irCodes;
@property (nullable, nonatomic, retain) NSNumber *irLed;
@property (nullable, nonatomic, retain) IRDevice *device;

@end

NS_ASSUME_NONNULL_END
