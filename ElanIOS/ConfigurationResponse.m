//
//  ConfigurationResponse.m
//  ElanIOS
//
//  Created by Vratislav Zima on 6/2/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "ConfigurationResponse.h"
#import "SBJson.h"
@implementation ConfigurationResponse

-(NSObject*) parseResponse:(NSData *)inputData identifier:(NSString *)identifier{
    
    SBJsonParser *parser2 = [[SBJsonParser alloc] init];
    NSString *inputString = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];
    
    NSDictionary* jsonObjects = [parser2 objectWithString:inputString];
    
    NSNumber* counter = [jsonObjects valueForKey:@"configuration changes counter"];
    
    NSString* deviceTypes =  [jsonObjects valueForKey:@"device types"];
    NSString* productTypes =  [jsonObjects valueForKey:@"product types"];
    NSString* roomTypes =  [jsonObjects valueForKey:@"room types"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:deviceTypes forKey:@"device types"];
    [defaults setObject:roomTypes forKey:@"room types"];
    [defaults setObject:productTypes forKey:@"product types"];
    
    [defaults synchronize];
    
    
    return counter;
}

@end
