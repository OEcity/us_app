//
//  CentralSource+Dictionary.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 16.11.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "CentralSource+Dictionary.h"
#import "SYCoreDataManager.h"

@implementation CentralSource (Dictionary)
- (void)updateWithDictionary:(NSDictionary *)dict {
    
    NSArray* areas = [dict objectForKey:@"areas"];
    [self setAreas:[NSSet new]];
    
    for (id element in areas) {
        HeatCoolArea * dev = [[SYCoreDataManager sharedInstance] getHCADeviceWithID:element inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        if (dev == nil){
            dev = [[SYCoreDataManager sharedInstance] createEntityHeatCoolArea];
            [dev setDeviceID:element];
            [[SYCoreDataManager sharedInstance] saveContext];
        }
        [self addAreasObject:dev];
    }
    
    NSString* parsedDevice = [dict objectForKey:@"device"];
    Device *dev = [[SYCoreDataManager sharedInstance] getDeviceWithID:parsedDevice inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
    if (dev == nil){
        dev = [[SYCoreDataManager sharedInstance] createEntityDevice];
        [dev setDeviceID:parsedDevice];
        [[SYCoreDataManager sharedInstance] saveContext];
    }
    [self setDevice:dev];

    
    //Parsing basic information
    NSString* parsedID = [dict objectForKey:@"id"];
    
    NSString* parsedType = [dict objectForKey:@"type"];

    NSString* parsedLabel = [dict objectForKey:@"label"];
    
    
    //    if (parsedLabel ==nil) return false;
    
    //If info is same for all attributes, skip updating
    if (!([self.centralsourceID isEqualToString:parsedID] &&
          [self.type isEqualToString:parsedType] &&
          [self.label isEqualToString:parsedLabel])) {
        
        self.centralsourceID = parsedID;
        self.type = parsedType;
        self.label = parsedLabel;
    }
}



@end
