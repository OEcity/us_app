//
//  GuideCameraAddViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 05.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Camera.h"

@interface GuideCameraAddViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) Camera*camera;

- (IBAction)textFieldDidBeginEditing:(UITextField *)textField;
- (IBAction)textFieldDidEndEditing:(UITextField *)textField;

-(void) keyboardWillShow:(NSNotification *)note;
@end
