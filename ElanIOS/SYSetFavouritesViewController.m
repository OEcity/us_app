//
//  SYSetFavouritesViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 7/29/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYSetFavouritesViewController.h"
@interface SYSetFavouritesViewController ()

@end

@implementation SYSetFavouritesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncComplete:)
                                                 name:@"dataSyncComplete"
                                               object:nil];

    _devices = [[SYCoreDataManager sharedInstance] getAllDevices];
    
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"favourite == 1"];
    NSArray * favouriteDevs = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Device" withPredicate:predicate];
    _devicesToBeAdded = [[NSMutableSet alloc] init];
    
    for (Device * device in favouriteDevs){
        [_devicesToBeAdded addObject:device.deviceID];
    }
    
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_favouritesLabel setText:NSLocalizedString(@"favourite", nil)];
    [_bottomRightButton setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
    [_bottomLeftButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
     
    
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    Device *selectedDevice = [_devices objectAtIndex:indexPath.row];
    UILabel * deviceName = (UILabel*)[cell viewWithTag:2];
    UIImageView * deviceIcon = (UIImageView*)[cell viewWithTag:1];
    UIImageView * deviceFavourite = (UIImageView*)[cell viewWithTag:3];
    
    [deviceName setTextAlignment:NSTextAlignmentLeft];
    //[deviceName setTextColor:[UIColor colorWithRed:1.0 green:1 blue:1 alpha:1]];
    UIFont* boldFont = [UIFont systemFontOfSize:17];
    while ([cell viewWithTag:201]!=nil){
        [[cell viewWithTag:201] removeFromSuperview];
    }
    [deviceName setFont:boldFont];
    
    
    deviceName.text = selectedDevice.label;
    if (deviceName.text==nil){
        deviceName.text = selectedDevice.label;
    }
    [[cell viewWithTag:99] removeFromSuperview];
    
    deviceIcon.image = selectedDevice.imageOff;
    deviceIcon.highlightedImage = selectedDevice.imageOn;
    
    
 if (![_devicesToBeAdded containsObject:selectedDevice.deviceID]){
     [deviceFavourite setImage:[UIImage imageNamed:@"krizek.png"]];
    }else{
     [deviceFavourite setImage:[UIImage imageNamed:@"fajfka.png"]];
    }

    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {


    return [self.devices count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"MyCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
   
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    Device *selectedDevice = [self.devices objectAtIndex:indexPath.row];
    NSString* name  = selectedDevice.deviceID;

    if ([_devicesToBeAdded containsObject:selectedDevice.deviceID]){
        [_devicesToBeAdded removeObject:name];
    }else{
        [_devicesToBeAdded addObject:name];
    }
    [self.tableView reloadData];


}


-(IBAction)saveFavourites:(id)sender{
    
    for (Device * dev in _devices){
        [[SYCoreDataManager sharedInstance] setFavouriteValue:[[NSNumber alloc] initWithBool:NO] forDevice:dev.deviceID ];
    }
    
    for (NSString * deviceID in _devicesToBeAdded){
        [[SYCoreDataManager sharedInstance] setFavouriteValue:[[NSNumber alloc] initWithBool:YES] forDevice:deviceID ];
    }
    [self dismissViewControllerAnimated:YES completion:nil];

    
}
-(IBAction)back:(id)sender{


    [self dismissViewControllerAnimated:YES completion:nil];
    
}



- (NSUInteger)supportedInterfaceOrientations
{

    return UIInterfaceOrientationMaskPortrait;
}

@end
