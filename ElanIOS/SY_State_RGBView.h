//
//  SY_State_RGBView.h
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 18/06/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "SY_State_View.h"

@interface SY_State_RGBView : SY_State_View

@property int maxValue;
@property int value;
@property (nonatomic, retain) UIColor * color;
@property (nonatomic, strong) IBOutlet UIView * vCircleView;
@property (nonatomic, strong) IBOutlet UIView * vColorView;
@property (nonatomic, strong) IBOutlet UILabel * lBrightness;

@end
