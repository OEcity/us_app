//
//  SceneDetailResponse.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 7/31/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"
@interface SceneDetailResponse : NSObject <Response>

@end
