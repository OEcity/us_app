//
//  SYTimeScheduleViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/4/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HUDWrapper.h"
#import "SYHeatContainerViewController.h"
@interface SYTimeScheduleViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, retain) NSMutableArray * schedules;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) HUDWrapper * loaderDialog;
@property (nonatomic, retain) SYHeatContainerViewController * container;
@property (nonatomic, retain) UILabel * lNoContent;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;
@end
