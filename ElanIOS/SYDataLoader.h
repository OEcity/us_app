//
//  SYDataLoader.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 16.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SYCoreDataManager.h"

@interface SYDataLoader : NSObject

@property (nonatomic, retain) NSArray * devices;
@property int statesToDownload;

+(SYDataLoader*) sharedInstance;

- (void)reloadIfNeeded;
- (void)loadDataFromElan:(Elan*)elan;
- (void)reloadFWVersion:(UIImageView *)myView;
- (void)loadDevicesForElan:(Elan*)elan;
- (void)loadRoomsFromElan:(Elan*)elan;
- (void)loadConfigurationForElan:(Elan*)elan;
- (void)loadScenesFromElan:(Elan*)elan;
- (void)updateConfigurationWithDictionary:(NSDictionary*)config;
- (void)updateDeviceStates;
- (void)updateStateForDeviceID:(NSString*)deviceID fromElan:(Elan*)elan;
- (void)rebuildDatabase;
- (void)reloadDevices;
- (void)loadSHCASchedulesFromElan:(Elan*)elan;
- (void)loadDeviceSchedulesFromElan:(Elan*)elan;
- (void)loadWeatherFromElan:(Elan*)elan;
@end
