//
//  MainMenuPVC.h
//  SAM_11_NewDesign
//
//  Created by admin on 06.07.16.
//  Copyright © 2016 admin. All rights reserved.
//

#import "PageViewController.h"


@interface MainMenuPVC : PageViewController
-(UIViewController*)getCurrentViewController;
-(void)refreshPVC:(id)sender;



@end
