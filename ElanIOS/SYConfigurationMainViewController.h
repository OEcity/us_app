//
//  SYConfigurationMainViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/18/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYContainerViewController.h"
#import "HUDWrapper.h"
@interface SYConfigurationMainViewController : UIViewController
@property (nonatomic, retain) IBOutlet UIButton * devices;
@property (nonatomic, retain) IBOutlet UIButton * rooms;
@property (nonatomic, retain) IBOutlet UIButton * floorplan;
@property (nonatomic, retain) IBOutlet UIButton * sync;
@property (nonatomic, retain) IBOutlet UIView * tab;
@property (nonatomic, retain) UIImage * image;
@property (nonatomic, retain) NSString * floorPlanURL;
@property (nonatomic, retain) SYContainerViewController *containerViewController;
//-(void)startFloorPlan:(UIImage*)floorplan;
@property (weak, nonatomic) IBOutlet UIImageView *floorplansImage;
@property (weak, nonatomic) IBOutlet UIImageView *roomsImage;
@property (weak, nonatomic) IBOutlet UIImageView *devicesImage;
@property (weak, nonatomic) IBOutlet UIImageView *assignImage;
@property (nonatomic, retain) HUDWrapper * loaderDialog;
@property (weak, nonatomic) IBOutlet UILabel *configurationLabel;
@property (weak, nonatomic) IBOutlet UILabel *floorplansLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomsLabel;
@property (weak, nonatomic) IBOutlet UILabel *devicesLabel;
@property (weak, nonatomic) IBOutlet UILabel *assignLabel;



@end
