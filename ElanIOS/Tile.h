//
//  Tile.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 08/11/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Tile : NSObject
@property (nonatomic, retain) UIImage * tile_on;
@property (nonatomic, retain) UIImage * tile_off;
@property (nonatomic, retain) UIImage * tile_seda;

@end
