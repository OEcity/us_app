//
//  SettingsViewController.swift
//  iHC-MIIRF
//
//  Created by Tom Odler on 20.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

import UIKit

class SettingsViewController:  UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tbvHeight: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stringArray = [NSLocalizedString("elan_settigns", comment: ""),NSLocalizedString( "devices", comment: ""), NSLocalizedString("rooms", comment: ""), NSLocalizedString("scenes", comment: ""), NSLocalizedString("advanced_settings",comment: "")]

        // Do any additional setup after loading the view.
        print(devices?.fetchedObjects?.count)
        self.navigationController?.navigationBar.isHidden = false;
        self.navigationController?.navigationBar.setBackgroundImage(UIImage.init(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage.init()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if((self.navigationController) != nil){
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        }
        
        SYWebSocket.sharedInstance().reconnect()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stringArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell :UITableViewCell = tableView.dequeueReusableCell(withIdentifier: "MyCell")!
        
        let label : UILabel = cell.viewWithTag(1) as! UILabel
        label.text = stringArray[indexPath.row]
        
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.row){
        case 0:
            self.performSegue(withIdentifier: "ipSettings", sender: nil)
            break
        case 1:
            self.performSegue(withIdentifier: "elements", sender: nil);
            break
        case 2:
            self.performSegue(withIdentifier: "rooms", sender: nil);
            break
        case 3:
            self.performSegue(withIdentifier: "scenes", sender: nil)
            break
        case 4:
            self.performSegue(withIdentifier:"advancedSettings", sender:nil)
            break
        default:
            break
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
