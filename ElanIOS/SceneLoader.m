//
//  SceneLoader.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 7/31/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SceneLoader.h"
#import "SceneResponse.h"
#import "SceneDetailResponse.h"
#import "AppDelegate.h"
#import "Util.h"
@implementation SceneLoader

@synthesize isLoading;


- (id)init {
    self = [super init];


    return self;
}

id _sender;
#warning TODO: nebude se pouzivat - nechavam z duvodu moznosti zaimplementovani do obecneho loaderu
/*
-(void) LoadScenes:(id)sender{
    isLoading = YES;
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.loadingScenes=YES;
    [CoreDataManager deleteAllObjects:@"Scene"];
    [CoreDataManager deleteAllObjects:@"Action"];
    _sender = sender;
    URLConnector * url = [[URLConnector alloc] initWithAddressAndLoader:[Util getConfigUrl:@"scenes"] activityInd:nil];
    SceneResponse * scenesResponse = [[SceneResponse alloc] init];
    [url setParser:scenesResponse];
    [url setConnectionListener:self];
    [url ConnectAndGetData];

}



NSInteger sceneSize=0;
NSInteger sceneCouter=0;

-(void)connectionResponseOK:(NSObject *)returnedObject response:(id<Response>)responseType{

    if ([responseType isKindOfClass:[SceneResponse class]] ){
        self.scenes = [[NSMutableArray alloc] initWithArray:(NSArray*) returnedObject];
        sceneSize=[self.scenes count];
        sceneCouter=0;
        for (Scene*scene in self.scenes){
            URLConnector * url = [[URLConnector alloc] initWithAddressAndLoader:scene.url activityInd:nil];

            SceneDetailResponse * itemsResponse = [[SceneDetailResponse alloc] init];

            [url setParser:itemsResponse];
            [url setConnectionListener:self];
            url.name = scene.sceneID;
            [url ConnectAndGetData];
        }
    }else if ([responseType isKindOfClass:[SceneDetailResponse class]] ){
        Scene * scene = (Scene*) returnedObject;
        
        AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];

        [CoreDataManager saveScene:scene inContext:appDelegate.managedObjectContext];
        NSError* error;
        [appDelegate.managedObjectContext save:&error];
        sceneCouter++;
        if (sceneCouter>=sceneSize){
            AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];


            dispatch_async(dispatch_get_main_queue(), ^{
                app.loadingScenes=NO;
                [[NSNotificationCenter defaultCenter] postNotificationName:@"sceneLoaderComplete" object:self];
            });

        }

    }

}




-(void) connectionResponseFailed:(NSInteger)code{
    
    
    
}
*/
@end
