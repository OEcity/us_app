//
//  SYTimeScheduleViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/4/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "SYTimeScheduleViewController.h"
#import "SYAPIManager.h"
#import "IconWithLabelTableViewCell.h"
@interface SYTimeScheduleViewController ()

@end

@implementation SYTimeScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];

    [[SYAPIManager sharedInstance] getTemperatureSchedulesWithSuccess:^(AFHTTPRequestOperation *request, id response){
        _schedules = [[NSMutableArray alloc] init];
        long arraySize = [((NSArray*)response) count];
        if (!((NSArray*)response) || ![((NSArray*)response) count]){
            
            CGFloat screenHeight = self.view.frame.size.height;
            _lNoContent = [[UILabel alloc] initWithFrame:CGRectMake(0, screenHeight/2  - 10, 320, 20)];
            _lNoContent.textAlignment = NSTextAlignmentCenter;
            [_lNoContent setTextColor:[UIColor blackColor]];
            [_lNoContent setText:NSLocalizedString(@"temp_schedule_settings_empty_list", nil)];
            [self.view addSubview:_lNoContent];
            [_loaderDialog hide];
            return;
            
        }
        for (NSString * scheduleName in (NSArray *)response){
            [[SYAPIManager sharedInstance] getTemperatureScheduleDetailsWithName:scheduleName success:
             ^(AFHTTPRequestOperation *request, id response){
                 [_schedules addObject:(NSDictionary*)response];
                 if ([_schedules count]==arraySize){
                     NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"label" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                     
                     _schedules=[[NSMutableArray alloc] initWithArray:[_schedules sortedArrayUsingDescriptors:@[sort]]];
                     dispatch_async(dispatch_get_main_queue(), ^{
                         
                         [_tableView reloadData];
                         [_loaderDialog hide];
                     });
                 }
                 
             }
            failure:^(AFHTTPRequestOperation * request, NSError * error){
                [_loaderDialog hide];
            }];

       }
        
        
        


        
    }
    failure:^(AFHTTPRequestOperation * request, NSError * error){
        [_loaderDialog hide];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"deviceCommFailed", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
    }];
    
    [_btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_btnAdd setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)dismissController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_schedules count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 72.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellIdentifier = @"scheduleCell";
    IconWithLabelTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[IconWithLabelTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSDictionary * schedule = [_schedules objectAtIndex:indexPath.row];
    
    [cell.lLabel setText:[schedule objectForKey:@"label"]];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_container setTimeScheduleConfig:[_schedules objectAtIndex:indexPath.row]];
    [_container setViewController:@"timeScheduleDetail"];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSDictionary * schedule = [_schedules objectAtIndex:indexPath.row];

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_timeSchedule", nil), [schedule valueForKey:@"label"]] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
        alert.tag = indexPath.row;
        [alert show];

    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
        NSDictionary * schedule = [_schedules objectAtIndex:indexPath.row];
        NSMutableArray * mutableSchedules = [[NSMutableArray alloc] initWithArray:_schedules];
        [mutableSchedules removeObjectAtIndex:indexPath.row];
        _schedules = mutableSchedules;
        [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
        [[SYAPIManager sharedInstance] deleteTemperatureScheduleWithId:[schedule objectForKey:@"id"] success:^(AFHTTPRequestOperation * request, id object){
            
        } failure:^(AFHTTPRequestOperation * request, NSError * error){
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                              message:NSLocalizedString(@"temp_schedule_failed_delete", nil)
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];
        }];
    }
}



- (IBAction)addSchedule:(id)sender {
    [_container setTimeScheduleConfig:nil];
    [_container setViewController:@"timeScheduleDetail"];
    
}




@end
