//
//  IncomingCallViewController.h
//  iHC
//
//  Created by Pavel Gajdoš on 06.09.13.
//  Copyright (c) 2013 Pavel Gajdoš. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LinphoneManager.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>




@interface IncomingCallViewController : UIViewController {

@private IBOutlet UILabel * callerNameLabel;
    
@private IBOutlet UIButton * acceptButton;
    
@private IBOutlet UIButton * endButton;
    
@private IBOutlet UIButton * unlockButton;
    
    
@private
    NSString * switchCode;
}

@property (nonatomic, strong) AVAudioPlayer *player;
@property (nonatomic, assign) LinphoneCall * call;

- (IBAction)acceptCallButtonPressed:(id)sender;
- (IBAction)declineCallButtonPressed:(id)sender;

- (IBAction)unlockButtonPressed:(id)sender;

- (void)update;

@end
