//
//  FloorPlanDetailResponse.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/28/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Floorplan.h"
#import "Response.h"
@interface FloorPlanDetailResponse : NSObject <Response>
@property Floorplan * floorPlan;

@end
