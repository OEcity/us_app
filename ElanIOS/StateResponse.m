//
//  StateResponse.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 05/11/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "StateResponse.h"
#import "SBJson.h"
#import "State.h"
@implementation StateResponse


-(NSObject*) parseResponse:(id )inputData identifier:(NSString *)identifier{
    //NSMutableArray * itemsArray = [[NSMutableArray alloc] init];
    
    SBJsonParser *parser2 = [[SBJsonParser alloc] init];
    NSDictionary* jsonObjects;
    if ([inputData isKindOfClass:[NSData class]]){
        NSString *inputString = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];
        
        jsonObjects = [parser2 objectWithString:inputString];
    }else{
        jsonObjects = (NSDictionary*)inputData;
    }

    State * state = [[State alloc] init];
    NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
    [f setDecimalSeparator:@","];
    [f setNumberStyle:NSNumberFormatterDecimalStyle];
    /*
    state.on = [jsonObjects objectForKey:@"on"];
    if ([state.on isKindOfClass:[NSNull class]]){
        state.on = [[NSNumber alloc] initWithInt:-1];
    }
    state.brightness = [jsonObjects objectForKey:@"brightness"];

    if ([state.brightness isKindOfClass:[NSString class]]){
        state.brightness = [f numberFromString:(NSString*)state.brightness];
    }else if ([state.brightness isKindOfClass:[NSNull class]]){
        state.brightness = [[NSNumber alloc] initWithInt:-1];
    }
    state.up = [jsonObjects objectForKey:@"roll up"];
    if ([state.up isKindOfClass:[NSNull class]]){
        state.up = [[NSNumber alloc] initWithInt:-1];
    }
    state.temperature = [jsonObjects valueForKey:@"temperature"];
    if ([jsonObjects valueForKey:@"temperature IN"] !=nil){
        state.temperature = [jsonObjects valueForKey:@"temperature IN"];
    }
    if ([state.temperature isKindOfClass:[NSNull class]]){
        state.temperature = [[NSNumber alloc] initWithInt:-1];
    }
    if ([state.temperature isKindOfClass:[NSString class]]){
        state.temperature = [f numberFromString:(NSString*)state.temperature];
    }
    state.temperatureOut = [jsonObjects valueForKey:@"temperature OUT"];
    if ([state.temperatureOut isKindOfClass:[NSNull class]]){
        state.temperatureOut = [[NSNumber alloc] initWithInt:-1];
    }
    if ([state.temperatureOut isKindOfClass:[NSString class]]){
        state.temperatureOut = [f numberFromString:(NSString*)state.temperatureOut];
    }

    state.requestedTemperature = [jsonObjects valueForKey:@"requested temperature"];

    if ([state.requestedTemperature isKindOfClass:[NSNull class]]){
        state.requestedTemperature = [[NSNumber alloc] initWithInt:-1];
    }
    if ([state.requestedTemperature isKindOfClass:[NSString class]]){
        state.requestedTemperature = [f numberFromString:(NSString*)state.requestedTemperature];
    }

    
    
    state.red = [jsonObjects valueForKey:@"red"];
    if ([state.red isKindOfClass:[NSString class]]){
        state.red = [f numberFromString:(NSString*)state.red];
    }
    if ([state.red isKindOfClass:[NSNull class]]){
        state.red = [[NSNumber alloc] initWithInt:-1];
    }
    state.green = [jsonObjects valueForKey:@"green"];
    if ([state.green isKindOfClass:[NSString class]]){
        state.green = [f numberFromString:(NSString*)state.green];
    }
    if ([state.green isKindOfClass:[NSNull class]]){
        state.green = [[NSNumber alloc] initWithInt:-1];
    }
    state.blue = [jsonObjects valueForKey:@"blue"];
    if ([state.blue isKindOfClass:[NSString class]]){
        state.blue = [f numberFromString:(NSString*)state.blue];
    }
    if ([state.blue isKindOfClass:[NSNull class]]){
        state.blue = [[NSNumber alloc] initWithInt:-1];
    }
    state.demo = [jsonObjects valueForKey:@"demo"];
    if ([state.demo isKindOfClass:[NSNull class]]){
        state.demo = [[NSNumber alloc] initWithInt:-1];
    }


    if ([jsonObjects valueForKey:@"delayed on: set time"]!=nil)
    state.delayedOnTime = [jsonObjects valueForKey:@"delayed on: set time"];

    if ([jsonObjects valueForKey:@"delayed off: set time"]!=nil)
    state.delayedOffTime = [jsonObjects valueForKey:@"delayed off: set time"];
    if ([jsonObjects valueForKey:@"increase: set time"]!=nil)
    state.delayedOnTime = [jsonObjects valueForKey:@"increase: set time"];
    if ([jsonObjects valueForKey:@"decrease: set time"]!=nil)
    state.delayedOffTime = [jsonObjects valueForKey:@"decrease: set time"];
    if ([jsonObjects valueForKey:@"set time"]!=nil)
    state.delayedOnTime = [jsonObjects valueForKey:@"set time"];

    if ([state.delayedOnTime isKindOfClass:[NSNull class]]){
        state.delayedOnTime = [[NSNumber alloc] initWithInt:-1];
    }
    if ([state.delayedOffTime isKindOfClass:[NSNull class]]){
        state.delayedOffTime = [[NSNumber alloc] initWithInt:-1];
    }
    
    state.correction = [jsonObjects valueForKey:@"correction"];
    
    if ([state.correction isKindOfClass:[NSNull class]]){
        state.correction = [[NSNumber alloc] initWithInt:-1];
    }
    if ([state.correction isKindOfClass:[NSString class]]){
        state.correction = [f numberFromString:(NSString*)state.correction];
    }
    
    
    state.mode = [jsonObjects valueForKey:@"mode"];

    if ([state.mode isKindOfClass:[NSNull class]]){
        state.mode = [[NSNumber alloc] initWithInt:-1];
    }
    if ([state.mode isKindOfClass:[NSString class]]){
        state.mode = [f numberFromString:(NSString*)state.mode];
    }
    
    state.openWindowSensitivity = [jsonObjects valueForKey:@"open window sensitivity"];

    if ([state.openWindowSensitivity isKindOfClass:[NSNull class]]){
        state.openWindowSensitivity = [[NSNumber alloc] initWithInt:-1];
    }
    if ([state.openWindowSensitivity isKindOfClass:[NSString class]]){
        state.openWindowSensitivity = [f numberFromString:(NSString*)state.openWindowSensitivity];
    }
    
    state.openWindow = [jsonObjects valueForKey:@"open window"];
    
    if ([state.openWindow isKindOfClass:[NSNull class]]){
        state.openWindow = [[NSNumber alloc] initWithInt:-1];
    }
    if ([state.openWindow isKindOfClass:[NSString class]]){
        state.openWindow = [f numberFromString:(NSString*)state.openWindow];
    }
    state.battery = [jsonObjects valueForKey:@"battery"];
    
    if ([state.battery isKindOfClass:[NSNull class]]){
        state.battery = [[NSNumber alloc] initWithInt:-1];
    }
    if ([state.battery isKindOfClass:[NSString class]]){
        state.battery = [f numberFromString:(NSString*)state.battery];
    }
    state.openWindowOffTime = [jsonObjects valueForKey:@"open window off time"];
    if ([state.openWindowOffTime isKindOfClass:[NSNull class]]){
        state.openWindowOffTime = [[NSNumber alloc] initWithInt:-1];
    }
    if ([state.openWindowOffTime isKindOfClass:[NSString class]]){
        state.openWindowOffTime = [f numberFromString:(NSString*)state.openWindowOffTime];
    }
    
    state.openValve = [jsonObjects valueForKey:@"open valve"];
    
    if ([state.openValve isKindOfClass:[NSNull class]]){
        state.openValve = [[NSNumber alloc] initWithInt:-1];
    }
    if ([state.openValve isKindOfClass:[NSString class]]){
        state.openValve = [f numberFromString:(NSString*)state.openValve];
    }
    state.locked = [jsonObjects valueForKey:@"locked"];
    
    if ([state.locked isKindOfClass:[NSNull class]]){
        state.locked = [[NSNumber alloc] initWithInt:-1];
    }
    if ([state.locked isKindOfClass:[NSString class]]){
        state.locked = [f numberFromString:(NSString*)state.locked];
    }
    */
    return state;
    
}
@end
