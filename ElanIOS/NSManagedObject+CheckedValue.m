//
//  NSManagedObject+NeccessaryValue.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 15/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "NSManagedObject+CheckedValue.h"

@implementation NSManagedObject (CheckedValue)


-(void) setCheckedValue:(id)value forKey:(NSString*)key{
    if ( ![value isKindOfClass:[NSNull class]]){
        [self setValue:value forKey:key];
    }else{
        [self setValue:[[NSNumber alloc] initWithBool:YES] forKey:@"hasError"];
    }
}



@end
