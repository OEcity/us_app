//
//  IntercomSettingsViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 10.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "IntercomSettingsViewController.h"
#import "LinphoneManager.h"
#import "Constants.h"
#import "AppDelegate.h"
#import "MainMenuPVC.h"

@interface IntercomSettingsViewController (){
@private UITextField *userName;
@private UITextField *password;
@private UITextField *serverAddress;
}
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;

@property NSString *sUserName;
@property NSString *sPassword;
@property NSString *sServerAddress;
@property BOOL intercomEnabled;


@end

@implementation IntercomSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    _sUserName = [[NSUserDefaults standardUserDefaults] objectForKey:@"voipUsername"];
    _sPassword = [[NSUserDefaults standardUserDefaults] objectForKey:@"voipPassword"];
    _sServerAddress = [[NSUserDefaults standardUserDefaults] objectForKey:@"voipServer"];
    
    _intercomEnabled = NO;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:INTERCOM_ENABLED] != nil){
        _intercomEnabled = [[defaults objectForKey:INTERCOM_ENABLED] boolValue];
    } else {
        [defaults setObject:[NSNumber numberWithBool:_intercomEnabled] forKey:INTERCOM_ENABLED];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateRegistrationState:)
                                                 name:kLinphoneRegistrationUpdate
                                               object:nil];
    
    [[LinphoneManager instance] refreshRegisters];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kLinphoneRegistrationUpdate object:nil];
    
     NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[NSNumber numberWithBool:_intercomEnabled] forKey:INTERCOM_ENABLED];
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.itercomEnabled = _intercomEnabled;
    
    MainMenuPVC*menu = app.mainMenuPVC;
    [menu refreshPVC:@"viewwildisappear intercom settings"];

}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 4 || indexPath.row == 0){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"addContact" forIndexPath:indexPath];
        UILabel *label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView] viewWithTag:2];
        
        if(indexPath.row == 4){
           label.text = NSLocalizedString(@"addContact", @"");
            img.hidden = YES;
        } else {
            label.text = NSLocalizedString(@"enableIntercom", @"");
            img.hidden = NO;
            _intercomEnabled ? (img.image = [UIImage imageNamed:@"vyber_modre_kolecko.png"]) :(img.image = [UIImage imageNamed:@"vyber_sede_kolecko.png"]);
        }
        return cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"myCell" forIndexPath:indexPath];
    UITextField *tf = [[[cell contentView] subviews] firstObject];
    NSAttributedString *placeholder = nil;
    
    switch(indexPath.row){
        case 1:
            placeholder = [[NSAttributedString alloc] initWithString: NSLocalizedString(@"username", @"") attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
            userName = tf;
            if(_sUserName != nil){
                tf.text = _sUserName;
            }
            tf.secureTextEntry = NO;
            break;
        case 2:
            placeholder = [[NSAttributedString alloc] initWithString: NSLocalizedString(@"password", @"") attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
            password = tf;
            if(_sPassword != nil){
                tf.text = _sPassword;
            }
            tf.secureTextEntry = YES;
            break;
        case 3:
            placeholder = [[NSAttributedString alloc] initWithString: NSLocalizedString(@"serverAddress", @"") attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
            serverAddress = tf;
            if(_sServerAddress != nil){
                tf.text = _sServerAddress;
            }
            tf.secureTextEntry = NO;
            break;
    }
    
    tf.attributedPlaceholder = placeholder;
    tf.delegate = self;
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 4){
        [self performSegueWithIdentifier:@"contacts" sender:nil];
    }
    if(indexPath.row == 0){
        _intercomEnabled = !_intercomEnabled;
        _intercomEnabled ? [[LinphoneManager instance] becomeActive] : [[LinphoneManager instance] resignActive];
        [_tableView reloadData];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    NSString * key = nil;
    
    if(textField == userName){
        key = @"voipUsername";
    } else if(textField == password){
        key = @"voipPassword";
    } else if(textField == serverAddress){
        key = @"voipServer";
    }
    
    if (key == nil) {
        return;
    }
    
    [[NSUserDefaults standardUserDefaults] setObject:textField.text forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)signIn:(id)sender {
    [[LinphoneManager instance] authenticate];
}

- (void)updateRegistrationState:(NSNotification *)notif
{
    __unused LinphoneProxyConfig *cfg = [[notif.userInfo objectForKey: @"cfg"] pointerValue];
    __unused LinphoneCallState state = [[notif.userInfo objectForKey: @"state"] intValue];
    NSString *message = [notif.userInfo objectForKey: @"message"];
    UIColor*color = nil;
    NSString*statusString = nil;
    if([message containsString:@"success"]){
        color = [UIColor greenColor];
        statusString = @"ON";
    } else {
        color = [UIColor redColor];
        statusString = @"OFF";
    }
    
    [_stateLabel setTextColor:color];
    [_stateLabel setText:[NSString stringWithFormat:@"%@", statusString]];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
