//
//  IRAction+Dictionary.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 13.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "IRAction+Dictionary.h"

@implementation IRAction (DictUpdate)

- (BOOL)updateWithDictionary:(NSDictionary *)dict {
    BOOL updated = NO;
    
    NSString* parsedType = [dict objectForKey:@"type"];
    NSNumber* parsedIR = [dict objectForKey:@"ir LED"];
    
    if ([parsedType isKindOfClass:[NSNull class]] && [parsedIR isKindOfClass:[NSNull class]]){
        return NO;
    }
    
    if(parsedIR != nil){
        updated = ([self updateTypeNumber:dict] || updated);
    }
    
    return updated;
}

- (BOOL)updateWithDictionary:(NSDictionary *)dict andName:(NSString*)name {
    BOOL updated = NO;
    
    if (![self.name isEqualToString:name]) {
        self.name = name;
        updated = YES;
    }
    
    updated = ([self updateWithDictionary:dict] || updated);
    
    return updated;
}

#pragma mark - Helper methods

- (BOOL)updateTypeNumber:(NSDictionary*)dict {
    BOOL updated = NO;
    
    NSNumber* irLED = [dict objectForKey:@"ir LED"];
    NSArray* irCodes = [dict objectForKey:@"ir codes"];
    
    if(irLED == nil)
        irLED = [NSNumber numberWithInt:1];
    
    if(irCodes != nil)
    self.irCodes = [NSKeyedArchiver archivedDataWithRootObject:irCodes];
    
    if (![self.irLed isEqualToNumber:irLED]) {
        self.irLed = irLED;
        updated = YES;
    }
    
    return updated;
}

@end
