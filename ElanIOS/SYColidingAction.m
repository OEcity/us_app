//
//  SYColidingAction.m
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 06/07/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "SYColidingAction.h"
#import "Action.h"

@implementation SYColidingAction

#define COLIDING_ACTIONS_JSON_FILENAME @"relations"
#define COLIDING_ACTIONS_JSON_DIRECTORY @"Supporting Files"

- (instancetype)init{
    self = [super init];
    if (self){
        _colidingActions = [self parseActionsFromJson];
    }
    return self;
}

- (NSArray*) getColidingActionsForActionName:(NSString*)actionName{
    return [[[_colidingActions filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"action == %@", actionName]] firstObject] valueForKey:@"collisions"];
}

- (NSArray*) filterColidingActionsForAtions:(NSSet*)actions andSelectedActions:(NSSet*)selectedActions{
    NSMutableArray *colidingActions = [NSMutableArray new];
    for (Action* action in selectedActions){
        NSArray* coliding = [self getColidingActionsForActionName:action.name];
        NSString *colidingPredicate;
        for (NSString* c in coliding){
            if (colidingPredicate == nil){
                colidingPredicate = [NSString stringWithFormat:@"name == '%@'", c];
            } else {
                colidingPredicate = [colidingPredicate stringByAppendingString:[NSString stringWithFormat: @" OR name == '%@'", c]];
            }
        }
        if (!colidingPredicate) continue;
        NSSet *filtered =[actions filteredSetUsingPredicate:[NSPredicate predicateWithFormat:colidingPredicate]];
        [colidingActions addObjectsFromArray:[filtered allObjects]];
    }
    return colidingActions;
    
}



-(NSArray*) parseActionsFromJson {
    NSString *filePath = [[NSBundle mainBundle] pathForResource:COLIDING_ACTIONS_JSON_FILENAME ofType:@"json"];
    NSData *data = [NSData dataWithContentsOfFile:filePath];
    NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    return json;
}

@end
