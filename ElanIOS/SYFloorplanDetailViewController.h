//
//  SYFloorplanDetailViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/11/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SYFloorplanDetailViewController : UIViewController


@property (nonatomic, retain) IBOutlet UIView * bottomView;
@property (nonatomic, retain) IBOutlet UIImageView * imageView;
@property (nonatomic, retain) IBOutlet UIButton * backButton;
@property (nonatomic, retain)  UIImage * image;
@property (nonatomic, copy)  NSString * floorplanURL;
@property (nonatomic, retain) NSOperationQueue *operationQueue;
@property (weak, nonatomic) IBOutlet UIButton *leftBottomButton;

@end
