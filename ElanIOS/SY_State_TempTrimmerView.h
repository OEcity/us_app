//
//  SY_State_RGBView.h
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 18/06/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "SY_State_View.h"

@interface SY_State_TempTrimmerView : SY_State_View

@property int maxValue;
@property int value;
@property BOOL on;
@property BOOL showTrimmer;
@property (nonatomic, strong) NSNumber *temp;
@property (nonatomic, retain) UIColor * color;
@property (nonatomic, strong) IBOutlet UIView * vCircleView;
@property (nonatomic, strong) IBOutlet UIImageView * vImageCircle;
@property (nonatomic, strong) IBOutlet UILabel * lTemp;

@end
