//
//  SYOtherSettingsViewController.swift
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 11/05/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//
/*
import UIKit

class SYOtherSettingsViewController: UIViewController, NSURLSessionDownloadDelegate, NSURLSessionDataDelegate, NSURLSessionTaskDelegate, NSURLSessionDelegate {

    @IBOutlet weak var lShowFavourites: UILabel!
    @IBOutlet weak var lNotifyForUpdates: UILabel!
    @IBOutlet weak var lDonwloadUpdate: UILabel!
    @IBOutlet weak var lUploadUpdate: UILabel!
    
    @IBOutlet weak var iShowFavouritesSelected: UIImageView!
    @IBOutlet weak var iNotifyForUpdatesSelected: UIImageView!
    
    @IBOutlet weak var updateView: UIView!
    @IBOutlet weak var bBottomLeft: UIButton!
    @IBOutlet weak var bBottomRight: UIButton!
    
    var backgroundSession: NSURLSession!
    var downloadTask: NSURLSessionDownloadTask!
    var uploadTask: NSURLSessionUploadTask!
    var internetFWVersion: String = ""
    var upFWURL: String = ""
    var elan: Elan!
    var downloadDialog: HUDWrapper!
    var download: Bool = true
    
    var myElan : Elan?

    override func viewDidDisappear(animated: Bool) {
        backgroundSession.finishTasksAndInvalidate()
    }
    
    override func viewDidLoad() {
        lShowFavourites.text = NSLocalizedString("favouritesAfterStart", comment: "")
        lNotifyForUpdates.text = NSLocalizedString("label.text.notification.getNotificationsForFirmwareUpdates", comment: "")
        lDonwloadUpdate.text = NSLocalizedString("download_new_firmware", comment: "")
        lUploadUpdate.text = NSLocalizedString("update_eLAN", comment: "")
        bBottomLeft.setTitle(NSLocalizedString("menu", comment: ""), forState: UIControlState.Normal)
        loadShowFavouritesView()
        
        let backgroundSessionConfiguration = NSURLSessionConfiguration.backgroundSessionConfigurationWithIdentifier("backgroundSession")
        backgroundSession = NSURLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        
        let defaults = NSUserDefaults.standardUserDefaults()
        print(defaults.valueForKey("configuration"))
        
        myElan = SYCoreDataManager.sharedInstance().getCurrentElan()
        
        if let tempElan = self.myElan{
            if(tempElan.update!.boolValue == false){
            updateView.hidden = true
            } else {
                updateView.hidden = false
            }
            
        tempElan.type = defaults.valueForKey("elanType") as? String
        } else {
            
            updateView.hidden = true
        
        }
        
        self.downloadDialog = HUDWrapper.init(rootController: self)
    }
    
    func loadShowFavouritesView(){
        
        let defaults = NSUserDefaults.standardUserDefaults()
        if (defaults.boolForKey(PREF_SHOW_FAVOURITES_AFTER_BOOT)){
            iShowFavouritesSelected.image = UIImage(named: "fajfka")
        } else {
            iShowFavouritesSelected.image = UIImage(named: "krizek")
        }
    }
    @IBAction func showFavouritesPressed(sender: AnyObject) {
        let defaults = NSUserDefaults.standardUserDefaults()
        if (defaults.boolForKey(PREF_SHOW_FAVOURITES_AFTER_BOOT)){
            defaults.setObject(false, forKey:PREF_SHOW_FAVOURITES_AFTER_BOOT)
        } else {
            defaults.setObject(true, forKey:PREF_SHOW_FAVOURITES_AFTER_BOOT)
        }
        loadShowFavouritesView()
    }
    
    @IBAction func notifyForUpdatesPressed(sender: AnyObject) {
    }
    
    @IBAction func downloadUpdatePressed(sender: AnyObject) {
        if let _ = myElan {
        getElanUpdateAddress()
        
        // create the alert
        var alert = UIAlertController.init()
        let defaults = NSUserDefaults.standardUserDefaults()
        let latestDownloaded = defaults.valueForKey("downloadedFW")
        let latestDownloadedWI = defaults.valueForKey("downloadedFW-WI")
        
        var myString = String()
        var myString2 = String()
        if(latestDownloaded != nil){  myString = NSLocalizedString("fw.download.alert.question", comment: "").stringByAppendingString("\n").stringByAppendingString(NSLocalizedString("fw.donwload.alert.version", comment: "")).stringByAppendingString("ELAN-RF: " + (latestDownloaded! as! String))
        } else {
            myString = ""
        }
        
        if(latestDownloadedWI != nil){
            myString2 = "\n" + NSLocalizedString("fw.download.alert.version", comment: "") + "RF-WI: " + (latestDownloadedWI! as! String)} else
        {   myString2 = ""}
        
        
        //Pokud je uloženo číslo předchozího staženého FW
        if ((latestDownloaded) != nil || latestDownloadedWI != nil){
            alert = UIAlertController(title: NSLocalizedString("fw.download.alert.title", comment: ""), message: myString.stringByAppendingString(myString2), preferredStyle: UIAlertControllerStyle.Alert)}
        else {
            alert = UIAlertController(title: NSLocalizedString("fw.download.alert.title", comment: ""), message: NSLocalizedString("fw.download.alert.question", comment: ""), preferredStyle: UIAlertControllerStyle.Alert)
        }
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: NSLocalizedString("download", comment: ""), style: UIAlertActionStyle.Default, handler: handleDownload))
        alert.addAction(UIAlertAction(title: NSLocalizedString("back", comment: ""), style: UIAlertActionStyle.Cancel, handler: nil))
        
        // show the alert
        self.presentViewController(alert, animated: true, completion: nil)
            
        } else {
            
            let alert = UIAlertController(title: NSLocalizedString("elanNotSelected", comment: ""), message: "", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
    }
    
    @IBAction func uploadUpdatePresssed(sender: AnyObject) {
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath:String = path[0]
        
        let manager = NSFileManager()
        
        if (manager.fileExistsAtPath(documentDirectoryPath.stringByAppendingString("/"+myElan!.type!+".bin"))){
            handleUpload()
        } else {
            let alert = UIAlertController(title: NSLocalizedString("upfilenotexists", comment: ""), message: "", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
    }
    
    @IBAction func backPressed(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func handleDownload(alertView: UIAlertAction!){
        download = true
        let url = NSURL(string: upFWURL)!
        downloadTask = backgroundSession.downloadTaskWithURL(url)
        downloadTask.resume()
        
    }
    
    func handleUpload(){
        download = false
        
        let request = NSMutableURLRequest.init(URL: NSURL(string:"http://" + myElan!.baseAddress!.stringByAppendingString("/api/configuration/firmware"))!)
        request.HTTPMethod = "POST"
        
        let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
        let documentDirectoryPath:String = path[0]
        
        let destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/"+myElan!.type!+".bin"))
        print(destinationURLForFile)
        
        uploadTask = backgroundSession.uploadTaskWithRequest(request, fromFile: destinationURLForFile)
        uploadTask.resume()
        self.downloadDialog.showWithLabel("uploadFW")
        
    }
    
    
        private func getElanUpdateAddress() {
     
            self.elan = SYCoreDataManager.sharedInstance().getCurrentElan()
            SYAPIManager.sharedInstance().getDataFromURL("http://217.197.144.56:4434/fw/files/fw.info.json", success: {(operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                
                let dict = responseObject as! [String : AnyObject]
                let infoDict = dict[self.myElan!.type!] as! [String : String]
                self.internetFWVersion = infoDict["version"]!
                self.upFWURL = infoDict["file"]!
                print(self.upFWURL)
                
                return
                
                }, failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
                    
                    return
            })
    }

    
    
    // 1
    func URLSession(session: NSURLSession,
        downloadTask: NSURLSessionDownloadTask,
        didFinishDownloadingToURL location: NSURL){
            
            let path = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory, NSSearchPathDomainMask.UserDomainMask, true)
            let documentDirectoryPath:String = path[0]
            let fileManager = NSFileManager()
            
            let destinationURLForFile = NSURL(fileURLWithPath: documentDirectoryPath.stringByAppendingString("/"+myElan!.type!+".bin"))
            print(destinationURLForFile)
            
            let defaults = NSUserDefaults.standardUserDefaults()
            
            self.downloadDialog.hide()
            
            if(myElan!.type == "ELAN-RF"){
                defaults.setValue(internetFWVersion, forKey: "downloadedFW")}
            if(myElan!.type == "ELAN-RF-WI"){
                defaults.setValue(internetFWVersion, forKey: "downloadedFW-WI")
            }
            
            
            //Pokud již soubor existuje
            if fileManager.fileExistsAtPath(destinationURLForFile.path!){
                do{
                    try fileManager.replaceItemAtURL(destinationURLForFile, withItemAtURL: location, backupItemName: nil, options: NSFileManagerItemReplacementOptions.UsingNewMetadataOnly, resultingItemURL: AutoreleasingUnsafeMutablePointer.init())
                    
                    print("přeuloženo")
                }
                catch {
                    print("neuloženo")
                }
            }
            else{
                do {
                    try fileManager.moveItemAtURL(location, toURL: destinationURLForFile)
                    print("uloženo")
                    
                }catch{
                    print("An error occurred while moving file to destination url")
                    let alert = UIAlertController(title: NSLocalizedString("notsaved", comment: ""), message: "", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            }
    }
    // 2
    func URLSession(session: NSURLSession,
        downloadTask: NSURLSessionDownloadTask,
        didWriteData bytesWritten: Int64,
        totalBytesWritten: Int64,
        totalBytesExpectedToWrite: Int64){

        self.downloadDialog.showWithLabel(NSLocalizedString("waitPls", comment: ""))
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        if (error != nil)
        {
          print("ERROR:")
          print(error)
        } else {
            if(download == true){
                print("downloaded")
                let alert = UIAlertController(title: NSLocalizedString("download.done", comment: ""), message: "", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            } else{

                print("uploaded")
                self.downloadDialog.showWithLabel(NSLocalizedString("elanUpdating", comment: ""))
                _ = NSTimer.scheduledTimerWithTimeInterval(10, target: self, selector: #selector(SYOtherSettingsViewController.updateEnd), userInfo: nil, repeats: false)}
        }
    }
    
    func updateEnd(){
        self.downloadDialog.hide()
        myElan!.update = NSNumber.init(bool: false)
        self.updateView.hidden = true
        
        let alert = UIAlertController(title: NSLocalizedString("update.done", comment: ""), message: "", preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
        
        let defaults = NSUserDefaults.standardUserDefaults()
        myElan!.firmware = defaults.valueForKey("downloadedFW")?.string
        
        
    }
    
    

}*/
