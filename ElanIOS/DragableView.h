//
//  DragableView.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 05/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
@interface DragableView : UIImageView

@property CGPoint originalPosition;
@property CGPoint touchOffset;
@property (nonatomic, retain) NSMutableArray * selectedDevices;
@property (nonatomic, retain) NSMutableArray * deviceViews;
@property BOOL landscape;
@end
