//
//  SYRoomsViewController.h
//  ElanIOS
//
//  Created by Vratislav Zima on 5/24/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceLoaderSync.h"
#import "RGBViewController.h"
#import "HUDWrapper.h"
#import "DevicesTableViewDelegate.h"
#import "RGBViewController.h"
#import "DeviceActionsViewController.h"
#import <CoreData/CoreData.h>
#import "DevicesCollectionViewDelegate.h"
#import "CHTCollectionViewWaterfallLayout.h"

@interface SYRoomsViewController : UIViewController <UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout, UIGestureRecognizerDelegate, NSFetchedResultsControllerDelegate>


@property (nonatomic) NSInteger currentRoomID;

@property (nonatomic, retain) IBOutlet UICollectionView * collections;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * activityInd;
@property (nonatomic, retain) DeviceLoaderSync * loader;
@property (nonatomic, copy) IBOutlet UILabel * noServerFoundLabel;
@property ( retain) RGBViewController * rgbViewController;
@property (nonatomic, retain) HUDWrapper * loaderDialog;
@property (nonatomic, retain) DevicesTableViewDelegate * tableViewDelegate;
@property (nonatomic, retain) DevicesCollectionViewDelegate * collectionViewDelegate;
@property (retain, nonatomic) IBOutlet UIButton *leftBottomButton;
@property (retain, nonatomic) IBOutlet UIButton *rightBottomButton;
@property (weak, nonatomic) IBOutlet UILabel *ipTitle;
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) NSMutableArray * devices;
@property (retain) RGBViewController * rgb;
@property (retain) DeviceActionsViewController * actions;
@property (weak, nonatomic) IBOutlet UIImageView *updateBadge;
@property (nonatomic, retain) Device *segueDevice;



//-(void)syncComplete:(NSNotification *)note;
-(IBAction)settingsPressed:(id)sender;
@end
