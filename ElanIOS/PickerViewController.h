//
//  PickerViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 12/15/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PickerViewDelegate

-(void)DoneButtonTappedForRow:(NSInteger)row inComponent:(NSInteger)component;

@end

@interface PickerViewController : UIViewController <UIPickerViewDelegate>
@property (nonatomic, retain) id <PickerViewDelegate> delegate;
@property NSInteger row;
@property NSInteger component;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIButton *bDone;

@end
