//
//  AddHeatingModeViewController.h
//  US App
//
//  Created by admin on 13.07.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TempDayMode.h"

#import "AddModeNoDetailTVCell.h"
#import "AddModeTemperatureDetailTVCell.h"
#import "AddModeColorDetailTVCell.h"




@protocol AddHeatingDelegate

-(void)addHeatingModeToday:(NSInteger)day comesFromLTouch:(BOOL)edit tempDayMode:(TempDayMode*)tempdayMode position:(NSInteger)position erase:(BOOL)erase;

@end



@interface AddHeatingModeViewController : UIViewController<UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) id<AddHeatingDelegate> delegate;
@property NSInteger modePos;
@property NSInteger day;

@property (weak, nonatomic) IBOutlet UIPickerView *dayInWeekPicker;
@property (weak, nonatomic) IBOutlet UIView *dayInWeekPickerView;


@property (weak, nonatomic) IBOutlet UIView *timePickerView;
@property (weak, nonatomic) IBOutlet UIPickerView *timePicker;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *odLabel;

@property (weak, nonatomic) IBOutlet UILabel *doLabel;
@property BOOL odCausedOpen;



@property (strong, nonatomic) NSArray *daysInWeekForPicker;

@property (strong, nonatomic) NSArray *temperatures;

@property (strong, nonatomic) NSNumberFormatter *formatter;


@property  NSInteger timeTo;
@property  NSInteger timeFrom;
@property  NSInteger mode;

@property BOOL selectedDay;
@property BOOL selectedFrom;
@property BOOL selectedTill;
@property BOOL selectedMode;


-(void)prepareAndSendDataToDelegate;

@end
