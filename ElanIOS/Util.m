//
//  Util.m
//  O2archiv
//
//  Created by Vratislav Zima on 1/16/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "Util.h"
#import <sys/socket.h>
#import <netinet/in.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "SYCoreDataManager.h"
#import "CoreDataObjects.h"

@implementation Util

+(NSString*) getConfigUrl:(NSString*) key{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    return [defaults valueForKey:key];
    
}

+(NSString*) getServerAddress{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSString * address = [defaults valueForKey:@"server"];
    return address;
}

+(void) setServerAddress:(NSString*)address{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
    [defaults setObject:address forKey:@"server"];
    [defaults synchronize];
}

+(BOOL)hasConnectivity {
    struct sockaddr_in zeroAddress;
    bzero(&zeroAddress, sizeof(zeroAddress));
    zeroAddress.sin_len = sizeof(zeroAddress);
    zeroAddress.sin_family = AF_INET;
    
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr*)&zeroAddress);
    if(reachability != NULL) {
        //NetworkStatus retVal = NotReachable;
        SCNetworkReachabilityFlags flags;
        if (SCNetworkReachabilityGetFlags(reachability, &flags)) {
            if ((flags & kSCNetworkReachabilityFlagsReachable) == 0)
            {
                // if target host is not reachable
                return NO;
            }
            
            if ((flags & kSCNetworkReachabilityFlagsConnectionRequired) == 0)
            {
                // if target host is reachable and no connection is required
                //  then we'll assume (for now) that your on Wi-Fi
                return YES;
            }
            
            
            if ((((flags & kSCNetworkReachabilityFlagsConnectionOnDemand ) != 0) ||
                 (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic) != 0))
            {
                // ... and the connection is on-demand (or on-traffic) if the
                //     calling application is using the CFSocketStream or higher APIs
                
                if ((flags & kSCNetworkReachabilityFlagsInterventionRequired) == 0)
                {
                    // ... and no [user] intervention is needed
                    return YES;
                }
            }
            
            if ((flags & kSCNetworkReachabilityFlagsIsWWAN) == kSCNetworkReachabilityFlagsIsWWAN)
            {
                // ... but WWAN connections are OK if the calling application
                //     is using the CFNetwork (CFSocketStream?) APIs.
                return YES;
            }
        }
    }
    
    return NO;
}

+(UIColor*)convertToUIColorRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue {
    return [[UIColor alloc] initWithRed:red/255 green:green/255 blue:blue/255 alpha:1.0];
    
}

//+(NSString*) getAddressWith:(NSString*)suffix{
//    NSString * result = [NSString stringWithFormat:@"http://%@/api/%@", [[[SYCoreDataManager  sharedInstance] getCurrentElan] baseAddress], suffix];
//    return  result;
//}

+(UIImage*)determineImage:(Device*)device{
    UIImage * imageOFF = [device imageOff];
    UIImage * imageON = [device imageOn];
    UIImage * resultImage = imageOFF;
    if ([device isKindOfClass:[HeatCoolArea class]]) {
        if ([device getStateValueForStateName:@"on"] !=nil){
            if ([[device getStateValueForStateName:@"on"] boolValue]==YES){
                return  imageON;
            }

        }
    } else {
        if ([device getStateValueForStateName:@"on"] !=nil){
            if ([[device getStateValueForStateName:@"on"] boolValue]==YES){
                return imageON;
            }
        }
        if ([device getStateValueForStateName:@"brightness"]!=nil){
            if ([[device getStateValueForStateName:@"brightness"] intValue]>0){
                return imageON;
            }
            
        }
        if ([device getStateValueForStateName:@"roll up"]!=nil){
            if ([[device getStateValueForStateName:@"roll up"] boolValue]==YES){
                return imageON;
            }
            
        }
    }
    
    return  resultImage;
    
}

+(UIImage*)determineImageOn:(Device*)device{
    UIImage * imageON = [device imageOn];
       
    return  imageON;
    
}

+(UIImage*)determineImageGrey:(Device *)device{
    UIImage *imageGrey = [device imageSeda];
    
    return imageGrey;
}

+(NSDictionary*)serializeIRDevice:(IRDevice*)device{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    if(device.deviceID != nil){
        [dict setObject: device.deviceID forKey: @"id"];
    }
    NSMutableDictionary *devInfo = [NSMutableDictionary new];
    [devInfo setObject:@"IR" forKey:@"product type"];
    [devInfo setObject:device.label forKey:@"label"];
    [devInfo setObject:device.type forKey:@"type"];
    
    [dict setObject:devInfo forKey:@"device info"];
    
    NSMutableDictionary *actionsInfo = [NSMutableDictionary new];
    for(IRAction*ac in device.actions){
        NSMutableDictionary*acDict = [NSMutableDictionary new];
        [acDict setObject: [NSNull null] forKey:@"type"];
        
        NSNumber *irCodes = [NSKeyedUnarchiver unarchiveObjectWithData:ac.irCodes];
        NSMutableArray *irCodesAr = [NSMutableArray new];
        [irCodesAr addObject:irCodes];
        
        [acDict setObject:irCodesAr forKey:@"ir codes"];
        
        [acDict setObject:ac.irLed forKey:@"ir LED"];
        
        [actionsInfo setObject:acDict forKey:ac.name];
    }
    
    [dict setObject:actionsInfo forKey:@"actions info"];
    
    NSMutableArray *secondaryActions = [NSMutableArray new];
    [dict setObject:secondaryActions forKey:@"secondary actions"];

    return dict;
}

/*
 +(id)fetchValuesForState:(NSSet*)states name:(NSString*)stateName{
 NSSet * currentTemperatureSet = [states filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"SUBQUERY(self.state,$state,$state.name == '%@' AND $state.value != nil)", stateName]];
 if ([currentTemperatureSet allObjects].count==1) {
 State * tempState =[[currentTemperatureSet allObjects] objectAtIndex:0];
 return tempState.value;
 }
 return nil;
 
 }
 
 
 +(State*)fetchStateForState:(NSSet*)states name:(NSString*)stateName{
 NSSet * currentTemperatureSet = [states filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"SUBQUERY(self.state,$state,$state.name == '%@' AND $state.value != nil)", stateName]];
 if ([currentTemperatureSet allObjects].count==1) {
 State * tempState =[[currentTemperatureSet allObjects] objectAtIndex:0];
 return tempState;
 }
 return nil;
 
 }
 */
+(NSDictionary*)generateNames{
    return [[NSDictionary alloc] initWithObjectsAndKeys:
            @"actionBrightness", @"brightness",
            @"actionImpulse",@"impulse",
            @"actionDecrease",@"decrease",
            @"actionDecreaseTime",@"decrease: set time",
            @"actionIncrease",@"increase",
            @"actionIncreaseTime",@"increase: set time",
            @"actionOn",@"on",
            @"actionDemo",@"demo",
            @"actionRollDown",@"roll down",
            @"actionRollDownPress",@"roll down press",
            @"actionRollDownRelease" ,@"roll down release",
            @"actionRollDownTime", @"roll down: set time",
            @"actionRollUp", @"roll up",
            @"actionRollUpPress",@"roll up press",
            @"actionRollUpRelease", @"roll up release",
            @"actionRollUpTime",@"roll up: set time",
            @"actionTimedOn", @"delayed off",
            @"actionTimedOnDelay",@"delayed off: set time",
            @"actionTimedOff",@"delayed on",
            @"actionTimedOffDelay",@"delayed on: set time",
            @"actionSetTime",@"set time",
            @"actionRed",@"red",
            @"actionGreen",@"green",
            @"actionBlue",@"blue",
            nil ];
}

@end
