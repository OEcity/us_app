//
//  SYConfigureRoomsViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/21/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Room.h"
#import "URLConnector.h"
#import "SYAddRoomConfigurationViewController.h"
#import "HUDWrapper.h"

@interface SYConfigureRoomsViewController : UIViewController <NSFetchedResultsControllerDelegate>
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSIndexPath * deletedIndex;
@property (weak, nonatomic) IBOutlet UILabel *emptyLabel;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (nonatomic)SYAddRoomConfigurationViewController*roomViewController;
@property (nonatomic, retain) HUDWrapper *loaderDialog;
-(void)reloadTableView;

@end
