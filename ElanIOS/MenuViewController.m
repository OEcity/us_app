//
//  MenuViewController.m
//  US App
//
//  Created by Tom Odler on 17.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "MenuViewController.h"

@interface MenuViewController ()
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _versionLabel.text =[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)menuAction:(id)sender {
    UIButton *menuButton = (UIButton*)sender;
    switch (menuButton.tag) {
        case 1:
            
            break;
            
        case 2:
            [_oldController performSegueWithIdentifier:@"eshopSegue" sender:nil];
            break;
        case 3:
            break;
        default:
            break;
    }
}

@end
