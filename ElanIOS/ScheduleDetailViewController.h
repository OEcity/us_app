//
//  ScheduleDetailViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 25.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HeatTimeSchedule.h"
#import "SYAPIManager.h"
#import "HUDWrapper.h"

@interface ScheduleDetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, retain) HeatTimeSchedule *schedule;
@property (weak, nonatomic) IBOutlet UIView *pickerViewContainer;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) HUDWrapper *loaderDialog;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@end
