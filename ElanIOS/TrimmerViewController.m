//
//  TrimmerViewController.m
//  ElanIOS
//
//  Created by Vratislav Zima on 6/2/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "TrimmerViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface TrimmerViewController ()

@end

@implementation TrimmerViewController

@synthesize number;

int _value;
int maxValue;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil value:(int)value maxValue:(int)maxValue_
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _value =value;
        maxValue = maxValue_;
        if (maxValue == 0) maxValue=100;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [number setText:[NSString stringWithFormat:@"%d", _value]];
    //[super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    int max =50;
    float onValue =(50.00/maxValue);
    onValue *=_value;
    UIImageView * myImage;

    for (int i=0; i < max; i++){
        
        if (i>=onValue){
            myImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stmivani_off.png"]];
        }else{
            myImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stmivani_on1.png"]];
        }
        myImage.frame = CGRectMake(self.view.frame.size.width/2 - myImage.frame.size.width/2, 0, myImage.frame.size.width, myImage.frame.size.height);
        [self.view addSubview:myImage];
        float consMove =(360.0/max);
        [self rotateView:myImage aroundPoint:self.view.center degrees:i*consMove - (360/4)];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#define DEGREES_TO_RADIANS(angle) (angle/180.0*M_PI)

- (void)rotateView:(UIView *)view
       aroundPoint:(CGPoint)rotationPoint
           degrees:(CGFloat)degrees {
    
      CGPoint anchorPoint = CGPointMake((rotationPoint.x - CGRectGetMinX(view.frame))/CGRectGetWidth(view.bounds),
                                      (rotationPoint.y - CGRectGetMinY(view.frame))/CGRectGetHeight(view.bounds));
    
    [[view layer] setAnchorPoint:anchorPoint];
    [[view layer] setPosition:rotationPoint]; // change the position here to keep the frame
    CGAffineTransform rotationTransform =  CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(degrees));
    view.transform = rotationTransform;
    
}

@end
