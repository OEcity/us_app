//
//  ViewController.h
//  RGBViewController
//
//  Created by admin on 21.06.16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device+State.h"
#import "URLConnector.h"
#import "URLConnectionListener.h"

@interface RGBViewController : UIViewController<NSFetchedResultsControllerDelegate,URLConnectionListener>
@property Device *device;
@property (nonatomic, strong) NSFetchedResultsController* devices;
@property (nonatomic, copy)  NSString * deviceName;


@end

