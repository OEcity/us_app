//
//  CentralSource+CoreDataProperties.h
//  
//
//  Created by Tom Odler on 16.11.16.
//
//

#import "CentralSource+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CentralSource (CoreDataProperties)

+ (NSFetchRequest<CentralSource *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *centralsourceID;
@property (nullable, nonatomic, copy) NSString *label;
@property (nullable, nonatomic, copy) NSString *type;
@property (nullable, nonatomic, retain) Device *device;
@property (nullable, nonatomic, retain) Elan *elan;
@property (nullable, nonatomic, retain) NSSet<HeatCoolArea *> *areas;

@end

@interface CentralSource (CoreDataGeneratedAccessors)

- (void)addAreasObject:(HeatCoolArea *)value;
- (void)removeAreasObject:(HeatCoolArea *)value;
- (void)addAreas:(NSSet<HeatCoolArea *> *)values;
- (void)removeAreas:(NSSet<HeatCoolArea *> *)values;

@end

NS_ASSUME_NONNULL_END
