//
//  SelectedDeviceViewController.m
//  iHC-MIRF
//
//  Created by Tom Odler on 07.03.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "SelectedDeviceViewController.h"
#import "SelectedDevicesTableViewCell.h"
#import "Util.h"
#import "Constants.h"

@interface SelectedDeviceViewController (){
    BOOL dimming;
    BOOL switching;
    BOOL shutters;
    BOOL rgb;
    BOOL white;
}
@property (weak, nonatomic) IBOutlet UIView *ViewForAll;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttons;


@end

@implementation SelectedDeviceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _selectedDevices = [NSMutableSet set];
    
    for(UIButton*button in _buttons){
        button.layer.borderWidth = 1.0f;
        button.layer.borderColor = USBlueColor.CGColor;
    }
    
    _ViewForAll.layer.borderWidth = 1.0f;
    _ViewForAll.layer.borderColor = USBlueColor.CGColor;
    
    
    [_cancelButton setTitle:NSLocalizedString(@"confirmation.cancel", nil) forState:UIControlStateNormal];

    // Do any additional setup after loading the view from its nib.
    dimming = switching = shutters = rgb = white = false;
    

    _devices = [[[SYCoreDataManager sharedInstance] getScheduleDevicesForElan:_elan] mutableCopy];
    
    NSMutableArray*tempDevArray = [[NSMutableArray alloc] init];
    
    for(DeviceTimeScheduleClass*schedule in [[SYCoreDataManager sharedInstance] getDeviceSchedulesForElan:_elan]){
        if([schedule.devicetimescheduleclassID isEqualToString:_mySchedule.devicetimescheduleclassID])continue;
        for(Device*device in schedule.devices){
            [tempDevArray addObject:device];
        }
    }
    
    [_devices removeObjectsInArray:tempDevArray];
    
    for(Device*device in _devices){
        for(NSString*myDeviceID in _devicesID){
            if([device.deviceID isEqualToString:myDeviceID]){
                [_selectedDevices addObject:device];
                
                if([device.productType containsString:@"RFSA"] || [device.productType containsString:@"RFSC"] || [device.productType containsString:@"RFUS"]){
                    switching = true;
                } else if ([device.productType containsString:@"RFJA"]){
                    shutters = true;
                } else if (([device.productType containsString:@"RFDA"] && ![device.productType containsString:@"RGB"]) || [device.productType containsString:@"RFDEL"] || [device.productType containsString:@"RFDSC"] ){
                    dimming = true;
                } else if ([device.productType containsString:@"RGB"]) {
                    rgb = true;
                } else if ([device.productType containsString:@"White"]){
                    white = true;
                }
            }
        }
    }
    
    
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    SelectedDevicesTableViewCell * cell = (SelectedDevicesTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    bool wasChecked = true;
    
    _device = _devices[indexPath.row];
    if(cell.checker.hidden){
        [cell.checker setHidden:NO];
        [_selectedDevices addObject:_device];
        wasChecked = false;
    } else {
        [cell.checker setHidden:YES];
        [_selectedDevices removeObject:_device];    
    }

    
    if(_selectedDevices.count == 0){
        dimming = switching = shutters = rgb = white = false;
    }
    

    [self filterDevices:_device check:wasChecked];

 }

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SelectedDevicesTableViewCell* cell = (SelectedDevicesTableViewCell *)[[[NSBundle mainBundle] loadNibNamed:@"SelectedDevicesTableViewCell" owner:self options:nil] objectAtIndex:0];
    
    _device = _devices[indexPath.row];
    if([_selectedDevices containsObject:_device]){
        cell.checker.hidden = false;
        if([_device.productType containsString:@"RFSA"] || [_device.productType containsString:@"RFSC"] || [_device.productType containsString:@"RFUS"]){
            switching = true;
        } else if ([_device.productType containsString:@"RFJA"]){
            shutters = true;
        } else if (([_device.productType containsString:@"RFDA"] && ![_device.productType containsString:@"RGB"])|| [_device.productType containsString:@"RFDEL"] || [_device.productType containsString:@"RFDSC"]){
            dimming = true;
        } else if ([_device.productType containsString:@"RGB"]){
            rgb = true;
        } else if ([_device.productType containsString:@"White"]){
            white = true;
        }
    } else {
        cell.checker.hidden = true;
    }
    
    [self filterRows:cell device:_device];
    cell.deviceName.text = _device.label;
    cell.deviceImage.image = [_device imageOff];
    
    
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _devices.count;
}

-(id)initWithDelegate:(id<deviceSelectDelegate>)delegate  devices:(NSArray*)devices elan:(Elan*)elan withSchedule:(DeviceTimeScheduleClass*)schedule{
    self = [[SelectedDeviceViewController alloc] initWithNibName:@"SelectedDeviceViewController" bundle:nil];
    
    if(self != nil){
        _devicesID = devices;
        _delegate = delegate;
        _elan = elan;
        _mySchedule = schedule;
    }
    [_tableView reloadData];

    return self;
}


- (IBAction)cancelPressed:(id)sender {
    [self.view removeFromSuperview];
}

-(void)filterDevices:(Device *)checkedDevice check:(bool)wasCheckedHere{
    
    if(wasCheckedHere == false){
    if([checkedDevice.productType containsString:@"RFSA"] || [checkedDevice.productType containsString:@"RFSC"] || [checkedDevice.productType containsString:@"RFUS"]){
        switching = true;
    } else if ([checkedDevice.productType containsString:@"RFJA"]){
        shutters = true;
    } else if (([checkedDevice.productType containsString:@"RFDA"] && ![checkedDevice.productType containsString:@"RGB"])|| [checkedDevice.productType containsString:@"RFDEL"] || [checkedDevice.productType containsString:@"RFDSC"]){
        dimming = true;
    } else if ([checkedDevice.productType containsString:@"RGB"]){
        rgb = true;
    } else if ([checkedDevice.productType containsString:@"White"]) {
        white = true;
    }
    }
    [_tableView reloadData];
}

-(void)filterRows:(SelectedDevicesTableViewCell *)cell device:(Device *)device{
    
    if(dimming){
        if (([device.productType containsString:@"RFDA"] && ![device.productType containsString:@"RGB"]) || [device.productType containsString:@"RFDEL"] || [device.productType containsString:@"RFDSC"] ){
            cell.vOverlay.hidden = true;
            cell.userInteractionEnabled = true;
            
        } else {
            cell.userInteractionEnabled = false;
            cell.vOverlay.hidden = false;
            
        }
    } else if (rgb){
        if([device.productType containsString:@"RGB"]){
            cell.vOverlay.hidden = true;
            cell.userInteractionEnabled = true;
        } else {
            cell.userInteractionEnabled = false;
            cell.vOverlay.hidden = false;
        }
        
    }else if (switching){
        if([device.productType containsString:@"RFSA"] || [device.productType containsString:@"RFSC"] || [device.productType containsString:@"RFUS"]){
            cell.vOverlay.hidden = true;
            cell.userInteractionEnabled = true;
            
        } else {
            cell.userInteractionEnabled = false;
            cell.vOverlay.hidden = false;
        }
    } else if(shutters){
        if ([device.productType containsString:@"RFJA"]){
            cell.vOverlay.hidden = true;
            cell.userInteractionEnabled = true;
        } else {
            cell.userInteractionEnabled = false;
            cell.vOverlay.hidden = false;
        }
    } else if(white){
        if ([device.productType containsString:@"White"]){
            cell.vOverlay.hidden = true;
            cell.userInteractionEnabled = true;
        } else {
            cell.userInteractionEnabled = false;
            cell.vOverlay.hidden = false;
        }
    } else {
        cell.userInteractionEnabled = true;
        cell.vOverlay.hidden = true;
    }
}

- (IBAction)okPressed:(id)sender {
    NSString*tempType;
    if(dimming){
        tempType = @"diming";
    }else if (shutters){
        tempType = @"shuttters";
    }else if (switching){
        tempType = @"switch";
    } else if(rgb){
        tempType = @"rgb";
    } else if(white){
        tempType = @"white";
    } else {
        tempType = @"";
    }
    
    NSMutableArray*tempDeviceArray = [[NSMutableArray alloc]init];
    for(Device *myDevice in _selectedDevices){
        [tempDeviceArray addObject:myDevice.deviceID];
    }
    NSArray*arrayForSend = [tempDeviceArray copy];
    [_delegate deviceTypeSet:tempType deviceID:arrayForSend];
    [self.view removeFromSuperview];
}


@end
