//
//  SYServerInputViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 6/27/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYBaseViewController.h"
#import "Elan.h"

@protocol SYServerInputViewControllerDelegate

- (void)didSaveElan;

@end

@interface SYServerInputViewController : SYBaseViewController

@property (nonatomic, retain) IBOutlet UITextField *name;
@property (nonatomic, retain) IBOutlet UITextField *address;
@property (nonatomic, retain) IBOutlet UITextField *port;
@property (weak, nonatomic) IBOutlet UITextField *tfMacAddress;
@property (weak, nonatomic) IBOutlet UITextField *tfFirmware;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cTBottomOffset;
@property (weak, nonatomic) IBOutlet UILabel *lTitle;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ipAddressLabel;
@property (weak, nonatomic) IBOutlet UILabel *portLabel;
@property (weak, nonatomic) IBOutlet UILabel *lMacAddress;
@property (weak, nonatomic) IBOutlet UILabel *lFirmware;


@property (weak, nonatomic) IBOutlet UIButton *bottomRightButton;
@property (weak, nonatomic) IBOutlet UIButton *bottomLeftButton;

@property (nonatomic) id<SYServerInputViewControllerDelegate> delegate;

@property (strong, nonatomic) Elan* elan;

-(IBAction)addServer:(id)sender;
-(IBAction)backToList:(id)sender;
-(IBAction)hideKeyboard:(id)sender;

@end
