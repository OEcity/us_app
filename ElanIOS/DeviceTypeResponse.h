//
//  DeviceTypeResponse.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/1/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"
@interface DeviceTypeResponse : NSObject <Response>



@end
