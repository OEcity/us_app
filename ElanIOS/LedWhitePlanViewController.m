//
//  LedWhitePlanViewController.m
//  iHC-MIRF
//
//  Created by Tom Odler on 18.04.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//


#import <QuartzCore/QuartzCore.h>
#import "Util.h"
#import "LedWhitePlanViewController.h"
#import "EFCircularSlider.h"
#import "EFGradientGenerator.h"


#define VALUE_PER_DEGREE 0.83928571428


@interface LedWhitePlanViewController ()
@property (weak, nonatomic) IBOutlet UILabel *topSlideViewValueIndicatorLabel;
@property (weak, nonatomic) IBOutlet UIView *topSliderView;
@property (weak, nonatomic) IBOutlet UIView *bottomSliderView;

@property (strong, nonatomic) EFGradientGenerator *gradientGen;
@property (strong, nonatomic) EFCircularSlider* circularSlider;
@property (strong, nonatomic) EFCircularSlider* bcircularSlider;
@end

@implementation LedWhitePlanViewController
@synthesize colorPicker;
@synthesize colorView;
@synthesize colorViewCenter;
@synthesize colorTaste;
@synthesize deviceName;
@synthesize activityIndicator;
@synthesize circleView;
@synthesize colors;
@synthesize whiteBalance;
@synthesize kelvin;




#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define COLOR_PALETE_ROTATION_OFFSET 225
#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

-(id) initWithDelegate:(id<whiteSetterDelegate>)delegate value:(int)value mode:(NSInteger)mode red:(int)cRed green:(int)cGreen blue:(int)cBlue intensity:(int)intensity{
    
    self = [[LedWhitePlanViewController alloc] initWithNibName:@"LedWhitePlanViewController" bundle:nil];
    
    if(self != nil){
        _mode = mode;
        whiteBalance = value;
        _myDelegate = delegate;
        _cRed = cRed/255.0f;
        _cGreen = cGreen/255.0f;
        _cBlue = cBlue/255.0f;
        _intensity = intensity;
    }
    return self;    
}




- (void)viewDidLoad {
    [super viewDidLoad];
    
    //TOP SLIDER CONF
    CGRect sliderFrame = CGRectMake(0, 0, _topSliderView.frame.size.width, _topSliderView.frame.size.height);
    _circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    [_circularSlider addTarget:self action:@selector(topSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_topSliderView addSubview:_circularSlider];
    [_circularSlider pauseAutoredrawing];
    
    CGFloat dash[]={1,15};
    [_circularSlider setHandleRadius:10];
    [_circularSlider setUnfilledLineDash:dash andCount:2];
    [_circularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_circularSlider setUnfilledColor:[UIColor whiteColor]];
    [_circularSlider setFilledColor:[UIColor whiteColor]];
    [_circularSlider setHandleColor:[UIColor whiteColor]];
    [_circularSlider setLineWidth:1];
    [_circularSlider setArcStartAngle:150];
    [_circularSlider setArcAngleLength:240];
    
    
    [_circularSlider setCurrentArcValue:(CGFloat)_intensity forStartAnglePadding:2 endAnglePadding:2];
    
    //BOTTOM SLIDER CONF
    
    EFGradientGenerator *gradient = [[EFGradientGenerator alloc] initWithSize:_bottomSliderView.frame.size];
    [gradient setRadius:80];
    [gradient setSectors:360];
    
    
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:1];
    [gradient addNewColor:[UIColor colorWithRed:208.0/255.0 green:85.0/255.0 blue:32.0/255.0 alpha:1] atSector:40];
    [gradient addNewColor:[UIColor colorWithRed:240.0/255.0 green:163.0/255.0 blue:52.0/255.0 alpha:1] atSector:90];
    [gradient addNewColor:[UIColor colorWithRed:231.0/255.0 green:208.0/255.0 blue:69.0/255.0 alpha:1] atSector:180];
    [gradient addNewColor:[UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:210.0/255.0 alpha:1] atSector:270];
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:310];
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:360];
    
    [gradient renderImage];
    _gradientGen = gradient;
    
    CGRect bsliderFrame = CGRectMake(0, 0, _bottomSliderView.frame.size.width, _bottomSliderView.frame.size.height);
    _bcircularSlider = [[EFCircularSlider alloc] initWithFrame:bsliderFrame];
    [_bcircularSlider addTarget:self action:@selector(botSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_bottomSliderView addSubview:_bcircularSlider];
    [_bcircularSlider pauseAutoredrawing];
    
    [_bcircularSlider setImageBackgroundUnfilledLine:YES];
    [_bcircularSlider setLineWidth:8];
    [_bcircularSlider setFilledColor:[UIColor clearColor]];
    [_bcircularSlider setUnfilledLineStrokeBorderWidth:0];
    [_bcircularSlider setUnfilledLineStrokeBorderColor:[UIColor clearColor]];
    [_bcircularSlider setUnfilledLineInnerImage:[gradient renderedImage]];
    [_bcircularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_bcircularSlider setHandleColor:[UIColor whiteColor]];
    [_bcircularSlider setArcStartAngle:130];
    [_bcircularSlider setArcAngleLength:280];
    [_bcircularSlider setHandleRadius:10];
    [_bcircularSlider setHandleBorderSize:2];
    [_bcircularSlider setCurrentArcValue:(100 - whiteBalance) forStartAnglePadding:2 endAnglePadding:2];
    

}

-(void)topSliderValueChanged:(EFCircularSlider*)circularSlider {
    _topSlideViewValueIndicatorLabel.text = [NSString stringWithFormat:@"%.f", [circularSlider getCurrentArcValueForStartAnglePadding:2 endAnglePadding:2]];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:_topSlideViewValueIndicatorLabel.text];
    
    _intensity = [myNumber intValue];
    
}

-(void)botSliderValueChanged:(EFCircularSlider*)circularSlider {
    UIColor *myColor = [_gradientGen getColorFromAngleFromNorth:([circularSlider angleFromNorth])];
    
    [circularSlider setHandleColor:[_gradientGen getColorFromAngleFromNorth:([circularSlider angleFromNorth])]];
    CGColorRef color = [myColor CGColor];
    CGFloat *components = CGColorGetComponents(color);
    
    _cRed = (Byte)(components[0]*255);
    _cGreen = (Byte)(components[1]*255);
    _cBlue = (Byte)(components[2]*255);
    
    whiteBalance = (100 - [circularSlider getCurrentArcValueForStartAnglePadding:2 endAnglePadding:2]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_onOffLabel setText:NSLocalizedString(@"colorPickerDialogOnOff", nil)];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(UIColor*)colorWithHexString:(NSString*)hex
{
    NSString *cString = [[hex stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}



-(IBAction)backButtonPressed{
    [_myDelegate setWhite:whiteBalance red:_cRed*255 green:_cGreen*255 blue:_cBlue*255 mode:_mode intensity:_intensity];
    [self.view removeFromSuperview];
}

-(void)setColorSliderPosition{
    if (!_gradientGen || !_bcircularSlider) {
        return;
    }
    
    NSArray *gradientColors = [_gradientGen getSegmentForUIColor:[UIColor colorWithRed:(CGFloat)_cRed/255.0 green:(CGFloat)_cGreen/255.0 blue:(CGFloat)_cBlue/255.0 alpha:1.0] ];
    if ([gradientColors count] == 0) {
        [_bcircularSlider setCurrentArcValue:0.0];
    }else{
        NSNumber *number = [gradientColors objectAtIndex:0];
        CGFloat angle = [_gradientGen getAngleFromNorthForSector:[number unsignedIntegerValue]];
        NSLog(@"SettingColor: %lu %f", [number unsignedIntegerValue], angle);
        if ([_bcircularSlider isAngleValidArcAngle:angle]) {
            [_bcircularSlider setAngleFromNorth:angle];
            [self botSliderValueChanged:_bcircularSlider];
        }else{
            [_bcircularSlider setAngleFromNorth:[_bcircularSlider arcStartAngle]];
            [self botSliderValueChanged:_bcircularSlider];
        }
    }
    
}







@end
