//
//  ListCamerasViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/14/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYListCamerasViewController.h"
#import "SYCoreDataManager.h"
#import <QuartzCore/QuartzCore.h>
#import "SYCameraDetailViewController.h"
#import "MotionJpegImageView.h"
#import "AppDelegate.h"
#import "UIViewController+RevealViewControllerAddon.h"
#import "MainMenuPVC.h"

@interface SYListCamerasViewController ()

@end

@implementation SYListCamerasViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.loading = [[NSMutableArray alloc] init];
    self.operationQueue = [[NSOperationQueue alloc]init];
    
    [[self navigationController] setNavigationBarHidden:NO];
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    
    
    //Configure navigation bar apperence
    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];

}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.cameras = [[SYCoreDataManager sharedInstance] getAllCameras];
    [self.collectionView reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [_leftBottomButton setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    [_rightBottomButton setTitle:NSLocalizedString(@"menu", nil) forState:UIControlStateNormal];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {

    return [self.cameras count];
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return 1;
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"myTile" forIndexPath:indexPath];
    

    
    UILabel * cameraName = (UILabel*)[cell viewWithTag:2];
    MotionJpegImageView * imageView = (MotionJpegImageView*)[cell viewWithTag:3];
    [imageView setImage:nil];

    Camera* camera =(Camera*)[self.cameras objectAtIndex:indexPath.item];


    NSString* urlstring = [camera imageAddress];
    [imageView setUrl:[NSURL URLWithString:urlstring]] ;
    [imageView playWith:camera.username And:camera.password And:camera.type];
    imageView.staticImage = true;
    //    [imageView getSingleImageWith:camera.username And:camera.password And:camera.type];
    [cameraName setText:camera.label];



    return cell;
}

- (CGSize)collectionViewContentSize
{
    NSInteger rowCount = [self.collectionView numberOfSections] / 2;
    // make sure we count another row if one is only partially filled
    if ([self.collectionView numberOfSections] % 2) rowCount++;

    CGFloat height = 20 +
    rowCount * 104 + (rowCount - 1) * 10 +
    20;

    return CGSizeMake(self.collectionView.bounds.size.width, height);
}


NSString * roomName;
NSString * roomLabel;
#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{

    self.currentCameraID = indexPath.row;
    [self performSegueWithIdentifier:@"cameraDetail" sender:self];    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    if ([[segue identifier] isEqualToString:@"cameraDetail"])
    {

        SYCameraDetailViewController *vc = [segue destinationViewController];
        vc.camera = [self.cameras objectAtIndex:self.currentCameraID];
        
    }
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{

    return UIInterfaceOrientationMaskPortrait;
}

-(IBAction)help:(id)sender{
    [self performSegueWithIdentifier:@"showManual" sender:self];

}


@end
