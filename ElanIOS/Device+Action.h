//
//  Device+Action.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 4/20/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Device.h"

@interface Device (Action)
- (id)getActionForActionName:(NSString*)actionName;
- (BOOL) hasActionWithName:(NSString*)actionName;

@end
