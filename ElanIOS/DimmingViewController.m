//
//  DimmingViewController.m
//  US App
//
//  Created by Tom Odler on 23.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "DimmingViewController.h"
#import "EFCircularSlider.h"
#import "EFGradientGenerator.h"
#import "Constants.h"
#import <QuartzCore/QuartzCore.h>
#import "SYCoreDataManager.h"
#import "SYAPIManager.h"
#import "UIViewController+RevealViewControllerAddon.h"


@interface DimmingViewController ()
@property (weak, nonatomic) IBOutlet UIView *topSliderView;
@property (weak, nonatomic) IBOutlet UILabel *topSlideViewValueIndicatorLabel;
@property (weak, nonatomic) IBOutlet UIButton *increaseButton;
@property (weak, nonatomic) IBOutlet UIButton *decreaseButton;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *automatButton;
@property EFCircularSlider* circularSlider;

@property (nonatomic) BOOL automat;

- (IBAction)handleButons:(id)sender;

@end

@implementation DimmingViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    _typeLabel.text = _device.productType;
    _nameLabel.text = _device.label;

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    CGRect sliderFrame = CGRectMake(0, 0, _topSliderView.frame.size.width, _topSliderView.frame.size.height);
    _circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    [_circularSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [_circularSlider addTarget:self action:@selector(touchCancel:) forControlEvents:UIControlEventEditingDidEnd];
    [_topSliderView addSubview:_circularSlider];
    [_circularSlider pauseAutoredrawing];
    
    if([_device getStateValueForStateName:@"brightness"] != nil)
        _brightness = [[_device getStateValueForStateName:@"brightness"]intValue];
    
    //GET DEVICE BRIGHTNESS
    
    CGFloat dash[]={1,15};
    
    [_circularSlider setHandleRadius:10];
    [_circularSlider setUnfilledLineDash:dash andCount:2];
    [_circularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_circularSlider setUnfilledColor:[UIColor whiteColor]];
    [_circularSlider setFilledColor:[UIColor whiteColor]];
    [_circularSlider setHandleColor:[UIColor whiteColor]];
    [_circularSlider setLineWidth:1];
    [_circularSlider setArcStartAngle:150];
    [_circularSlider setArcAngleLength:240];
    
    [_circularSlider setCurrentArcValue:_brightness forStartAnglePadding:10 endAnglePadding:10];
    //[circularSlider setUnfilledLineInnerImage: [EFGradientGenerator getDefaultGradient]];
    
    _increaseButton.layer.borderColor = [USBlueColor CGColor];
    _decreaseButton.layer.borderColor = [USBlueColor CGColor];
    
    _increaseButton.layer.borderWidth = 1.0f;
    _decreaseButton.layer.borderWidth = 1.0f;
    
    //Configure navigation bar apperence
    //    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
    
    [self initFetchedResultsControllerWithHCAID:_device.deviceID];
    [self initAutomat];
}

-(void)valueChanged:(EFCircularSlider*)circularSlider {
    _topSlideViewValueIndicatorLabel.text = [NSString stringWithFormat:@"%i", [self nearestNumber:(int)[circularSlider getCurrentArcValueForStartAnglePadding:10 endAnglePadding:10] step:10]];
}

-(void)touchCancel:(EFCircularSlider*)circularSlider {
    _brightness = [self nearestNumber:(int)[circularSlider getCurrentArcValueForStartAnglePadding:10 endAnglePadding:10] step:10];
    //_brightness = (int)circularSlider.currentArcValue;
    
    [self putDataAsychn:[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithFloat:_brightness],@"brightness", nil]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initFetchedResultsControllerWithHCAID:(NSString*)hcaID {
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"State"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"device.label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.device.deviceID == %@", hcaID];
    [fetchRequest setPredicate:predicate];
    _devices = [[NSFetchedResultsController alloc]
                initWithFetchRequest:fetchRequest
                managedObjectContext:context
                sectionNameKeyPath:nil
                cacheName:nil];
    _devices.delegate = self;
    NSError *error;
    
    if (![_devices performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }else if ([[_devices fetchedObjects] count]>0){
        _device = (Device*)((State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if ([_devices.fetchedObjects count]>0){
        _device = (Device*)((State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
        
        [self syncComplete:_device];
    }
}

-(void)syncComplete:(Device *)device{
    if ([_device.deviceID isEqualToString:device.deviceID]){

    if([_device getStateValueForStateName:@"brightness"])
        _brightness = [[_device getStateValueForStateName:@"brightness"]intValue];
    
    [self initAutomat];
    
    [_circularSlider setCurrentArcValue:_brightness forStartAnglePadding:10 endAnglePadding:10];
    }
}

-(void)putDataAsychn:(NSMutableDictionary*)dict
{
    NSLog(@"Dictionary: %@", dict);
    [[SYAPIManager sharedInstance] putDeviceAction:dict device:[[SYCoreDataManager sharedInstance] getDeviceWithID:_device.deviceID]
                                           success:^(AFHTTPRequestOperation* operation, id response){
                                               NSLog(@"update complete: %@",response);
                                           }
                                           failure:^(AFHTTPRequestOperation* operation, NSError *error) {
                                               NSLog(@"update error: %@",[error description]);
                                           }];
}


- (IBAction)handleButons:(id)sender {
    UIButton*senderButton = (UIButton*)sender;
    switch (senderButton.tag) {
        case 1:
            _brightness = 50;
            
            [self putDataAsychn:[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithFloat:_brightness],@"brightness", nil]];
            break;
        case 2:
            _brightness = 0;
            
            [self putDataAsychn:[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithFloat:_brightness],@"brightness", nil]];
            break;
            
        default:
            break;
    }
}

-(int) nearestNumber:(int)num step:(int)step{
    NSLog(@"input number : %d step is %d", num, step);
    if (num % step == 0)
        NSLog(@"OK");
    else if (num % step < ((float)step/2.0))
        num = num - num % step;
    else
        num = num + (step - num % step);
    return num;
}

-(void)initAutomat{
    _automatButton.selected = NO;
    _automat = NO;
    
    if([[_device getStateValueForStateName:@"automat"]boolValue]){
        _automatButton.selected = YES;
        _automat = YES;
    }
}
@end

