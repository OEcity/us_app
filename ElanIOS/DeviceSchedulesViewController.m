//
//  DeviceSchedulesViewController.m
//  Click Smart
//
//  Created by Tom Odler on 17.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "DeviceSchedulesViewController.h"
#import "SYAPIManager.h"
#import "IconWithLabelTableViewCell.h"
#import "Constants.h"

@interface DeviceSchedulesViewController ()
@property (nonatomic) NSMutableDictionary* schedule;
@end

@implementation DeviceSchedulesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _schedule = [[NSMutableDictionary alloc] init];
    
    _tableView.layer.borderColor = USBlueColor.CGColor;
    _tableView.layer.borderWidth = 1.0f;
    
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    
//    [[SYAPIManager sharedInstance] getDeviceSchedulesWithSuccess:^(AFHTTPRequestOperation *request, id response){
//        _schedules = [[NSMutableArray alloc] init];
//        _devicesInSchedules = [[NSMutableArray alloc] init];
//        _tempDevices = [[NSMutableArray alloc] init];
//        long arraySize = [((NSArray*)response) count];
//        if (!((NSArray*)response) || ![((NSArray*)response) count]){
//            
//            CGFloat screenHeight = self.view.frame.size.height;
//            _lNoContent = [[UILabel alloc] initWithFrame:CGRectMake(0, screenHeight/2  - 10, 320, 20)];
//            _lNoContent.textAlignment = NSTextAlignmentCenter;
//            [_lNoContent setTextColor:[UIColor blackColor]];
//            [_lNoContent setText:NSLocalizedString(@"time_schedule_settings_empty_list", nil)];
//            [self.view addSubview:_lNoContent];
//            [_loaderDialog hide];
//            return;
//            
//        }
//        for (NSString * scheduleName in (NSArray *)response){
//            [[SYAPIManager sharedInstance] getDeviceScheduleDetailsWithName:scheduleName success:
//             ^(AFHTTPRequestOperation *request, id response){
//                 [_schedules addObject:(NSDictionary*)response];
//                 
//                 NSDictionary*dict = response;
//                 [_devicesInSchedules addObject: [dict objectForKey:@"devices"]];
//                 
//                 if ([_schedules count]==arraySize){
//                     NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"label" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
//                     
//                     _schedules=[[NSMutableArray alloc] initWithArray:[_schedules sortedArrayUsingDescriptors:@[sort]]];
//                     
//                     dispatch_async(dispatch_get_main_queue(), ^{
//                         
//                         [_tableView reloadData];
//                         [_loaderDialog hide];
//                     });
//                 }
//                 
//             }
//                                                                    failure:^(AFHTTPRequestOperation * request, NSError * error){
//                                                                        [_loaderDialog hide];
//                                                                    }];
//            
//        }
//    }
//                                                         failure:^(AFHTTPRequestOperation * request, NSError * error){
//                                                             [_loaderDialog hide];
//                                                             UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
//                                                                                                               message:NSLocalizedString(@"deviceCommFailed", nil)
//                                                                                                              delegate:nil
//                                                                                                     cancelButtonTitle:@"OK"
//                                                                                                     otherButtonTitles:nil];
//                                                             [message show];
//                                                         }];
    
    [_btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_btnAdd setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)dismissController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if([segue.identifier isEqualToString:@"scheduleDetail"]){
     _nextController = (DeviceDetailScheduleViewController*) [segue destinationViewController];
     _nextController.schedule = _schedule;
     } else {
         _schedule =nil;
         _nextController = (DeviceDetailScheduleViewController*) [segue destinationViewController];
         _nextController.schedule = _schedule;
     }
    
 }


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_schedules count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 72.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellIdentifier = @"scheduleCell";
    IconWithLabelTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[IconWithLabelTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    NSDictionary * schedule = [_schedules objectAtIndex:indexPath.row];
    
    [cell.lLabel setText:[schedule objectForKey:@"label"]];
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        _schedule = [[_schedules objectAtIndex:indexPath.row] mutableCopy];

      [self performSegueWithIdentifier:@"scheduleDetail" sender:nil];
    //    [_containerViewController setTimeScheduleConfig:[_schedules objectAtIndex:indexPath.row]];
    //    [_containerViewController setUsedDevicesArray:_devicesInSchedules];
    //    [_containerViewController setViewController:@"timeScheduleDetails"];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSDictionary * schedule = [_schedules objectAtIndex:indexPath.row];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_timeSchedule", nil), [schedule valueForKey:@"label"]] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
        alert.tag = indexPath.row;
        [alert show];
        
    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
        NSDictionary * schedule = [_schedules objectAtIndex:indexPath.row];
        NSMutableArray * mutableSchedules = [[NSMutableArray alloc] initWithArray:_schedules];
        [mutableSchedules removeObjectAtIndex:indexPath.row];
        _schedules = mutableSchedules;
        [_tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
//        [[SYAPIManager sharedInstance] deleteDeviceScheduleWithId:[schedule objectForKey:@"id"] success:^(AFHTTPRequestOperation * request, id object){
//            
//            _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
//            [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
//            
////            [[SYAPIManager sharedInstance] getDeviceSchedulesWithSuccess:^(AFHTTPRequestOperation *request, id response){
//                _devicesInSchedules = [[NSMutableArray alloc] init];
//                _tempDevices = [[NSMutableArray alloc] init];
//                if (!((NSArray*)response) || ![((NSArray*)response) count]){
//                    
//                    [_loaderDialog hide];
//                    return;
//                    
//                }
//                for (NSString * scheduleName in (NSArray *)response){
//                    [[SYAPIManager sharedInstance] getDeviceScheduleDetailsWithName:scheduleName success:
//                     ^(AFHTTPRequestOperation *request, id response){
//                         
//                         NSDictionary*dict = response;
//                         [_devicesInSchedules addObject: [dict objectForKey:@"devices"]];
//                         
//                         dispatch_async(dispatch_get_main_queue(), ^{
//                             
//                             
//                             [_loaderDialog hide];
//                         });
//                     }
//                     
//                     
//                                                                            failure:^(AFHTTPRequestOperation * request, NSError * error){
//                                                                                [_loaderDialog hide];
//                                                                            }];
//                    
//                }
//            }
//                                                                 failure:^(AFHTTPRequestOperation * request, NSError * error){
//                                                                     [_loaderDialog hide];
//                                                                     UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
//                                                                                                                       message:NSLocalizedString(@"deviceCommFailed", nil)
//                                                                                                                      delegate:nil
//                                                                                                             cancelButtonTitle:@"OK"
//                                                                                                             otherButtonTitles:nil];
//                                                                     [message show];
//                                                                 }];
//            
//            
//            
//            
//            
//        } failure:^(AFHTTPRequestOperation * request, NSError * error){
//            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
//                                                              message:NSLocalizedString(@"temp_schedule_failed_delete", nil)
//                                                             delegate:nil
//                                                    cancelButtonTitle:@"OK"
//                                                    otherButtonTitles:nil];
//            [message show];
//        }]
    }
}



- (IBAction)addSchedule:(id)sender {
    //    [_containerViewController setTimeScheduleConfig:nil];
    //    [_containerViewController setUsedDevicesArray:_devicesInSchedules];
    //    [_containerViewController setViewController:@"timeScheduleDetails"];
    
}
@end
