//
//  SceneDimmingViewController.m
//  Click Smart
//
//  Created by Tom Odler on 29.07.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "SceneDimmingViewController.h"
#import "EFCircularSlider.h"
#import "Constants.h"

@interface SceneDimmingViewController ()
@property EFCircularSlider* circularSlider;
@property (weak, nonatomic) IBOutlet UIView *topSliderView;
@property (weak, nonatomic) IBOutlet UIButton *increaseButton;
@property (weak, nonatomic) IBOutlet UIButton *decreaseButton;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *topSlideViewValueIndicatorLabel;

@property (nonatomic) NSMutableDictionary *dict;

@property (nonatomic) int intensity;

@end

@implementation SceneDimmingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    _dict = [[NSMutableDictionary alloc] init];
    

    //[circularSlider setUnfilledLineInnerImage: [EFGradientGenerator getDefaultGradient]];
    
    _increaseButton.layer.borderColor = [USBlueColor CGColor];
    _decreaseButton.layer.borderColor = [USBlueColor CGColor];
    
    _increaseButton.layer.borderWidth = 1.0f;
    _decreaseButton.layer.borderWidth = 1.0f;
    
    _nameLabel.text = _device.label;
    
    
    //Configure navigation bar apperence
    //    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
    
    _typeLabel.text = _device.productType;
    
      
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    CGRect sliderFrame = CGRectMake(0, 0, _topSliderView.frame.size.width, _topSliderView.frame.size.height);
    _circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    [_circularSlider addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
    [_circularSlider addTarget:self action:@selector(touchCancel:) forControlEvents:UIControlEventEditingDidEnd];
    [_topSliderView addSubview:_circularSlider];
    [_circularSlider pauseAutoredrawing];
    
    _intensity = 0;
    
    [self loadParametersFromArray];
    
    [_circularSlider setCurrentArcValue:_intensity forStartAnglePadding:2 endAnglePadding:2];
    //GET DEVICE BRIGHTNESS
    
    CGFloat dash[]={1,15};
    
    [_circularSlider setHandleRadius:10];
    [_circularSlider setUnfilledLineDash:dash andCount:2];
    [_circularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_circularSlider setUnfilledColor:[UIColor whiteColor]];
    [_circularSlider setFilledColor:[UIColor whiteColor]];
    [_circularSlider setHandleColor:[UIColor whiteColor]];
    [_circularSlider setLineWidth:1];
    [_circularSlider setArcStartAngle:150];
    [_circularSlider setArcAngleLength:240];
    
    [self loadParametersFromArray];
    
    [_circularSlider setCurrentArcValue:_intensity forStartAnglePadding:10 endAnglePadding:10];
}

-(void)loadParametersFromArray{
    NSArray *mySceneArray = nil;
    if(_controller)
        mySceneArray = _controller.actionsArray;
      else
          mySceneArray = _guideController.actionsArray;
    
    
    
    NSInteger index = 0;
    NSMutableIndexSet *indexForDelete = [[NSMutableIndexSet alloc]init];
    for(NSDictionary*dict in mySceneArray){
        NSString *deviceID =[[dict allKeys]firstObject];
        if([deviceID isEqualToString:_device.deviceID]){
            NSDictionary *myActionsDict = [dict objectForKey:deviceID];
            _intensity = [[myActionsDict objectForKey:@"brightness"]intValue];
            
            [indexForDelete addIndex:index];
        }
        index++;
    }
    
    if ([indexForDelete count] > 0){
        if(_controller)
            [_controller.actionsArray removeObjectsAtIndexes:indexForDelete];
        else
            [_guideController.actionsArray removeObjectsAtIndexes:indexForDelete];
    }
}

-(void)valueChanged:(EFCircularSlider*)circularSlider {
    _topSlideViewValueIndicatorLabel.text = [NSString stringWithFormat:@"%i", [self nearestNumber:(int)[circularSlider getCurrentArcValueForStartAnglePadding:10 endAnglePadding:10] step:10]];
    _intensity =[self nearestNumber:(int)[circularSlider getCurrentArcValueForStartAnglePadding:10 endAnglePadding:10] step:10];
}

-(void)touchCancel:(EFCircularSlider*)circularSlider {
   // _brightness = [self nearestNumber:(int)[circularSlider getCurrentArcValueForStartAnglePadding:10 endAnglePadding:10] step:10];
    //_brightness = (int)circularSlider.currentArcValue;
    
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(int) nearestNumber:(int)num step:(int)step{
    NSLog(@"input number : %d step is %d", num, step);
    if (num % step == 0)
        NSLog(@"OK");
    else if (num % step < ((float)step/2.0))
        num = num - num % step;
    else
        num = num + (step - num % step);
    return num;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [_dict setObject:[NSNumber numberWithInt:_intensity ] forKey:@"brightness"];
    NSMutableDictionary *deviceWithActionDictionary = [[NSMutableDictionary alloc]init];
    [deviceWithActionDictionary setObject:_dict forKey:_device.deviceID];
    if(_controller){
        [[_controller actionsArray] addObject:deviceWithActionDictionary];
        [[self.controller selectedDevices] setObject:_device forKey:_device.deviceID];
    }
    else {
        [[_guideController actionsArray] addObject:deviceWithActionDictionary];
        [[self.guideController selectedDevices] setObject:_device forKey:_device.deviceID];
    }
    }

@end

