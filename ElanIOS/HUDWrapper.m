//
//  HUDWrapper.m
//  KJ PDA
//
//  Created by Kaktus on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "HUDWrapper.h"
#import "SYAPIManager.h"
#import "AppDelegate.h"
@implementation HUDWrapper

@synthesize HUD, rootController;
-(HUDWrapper *) initWithRootController:(UIViewController *) controller{
    self = [self initWithRootController:controller cancelable:YES];
    return self;
}

-(HUDWrapper *) initWithRootController:(UIViewController *) controller cancelable:(BOOL)canCancel{
    self = [super init];
    _isCancelable = &canCancel;
    self.rootController = controller;
    if (self.HUD) {
        [self hide];
        HUD = nil;
    }
    if (self.rootController.navigationController.view != nil)
    {
        self.HUD = [[MBProgressHUD alloc] initWithView:self.rootController.navigationController.view];
        [self.rootController.navigationController.view addSubview:self.HUD];
    }
    else
    {
        self.HUD = [[MBProgressHUD alloc] initWithView:self.rootController.view];
        [self.rootController.view addSubview:self.HUD];
    }
    
    self.HUD.delegate = self;
    [self.HUD addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hudWasCancelled)]];

    return self;
}
- (void)hudWasCancelled {
    if (!_isCancelable){
        return;
    }
    //AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    //[[SYAPIManager sharedManager] cancelAllOperations];
    [self.HUD hide:YES];
    //app.loadingDevices=NO;

    /*UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                      message:NSLocalizedString(@"deviceCommFailed", nil)
                                                     delegate:nil
                                            cancelButtonTitle:@"OK"
                                            otherButtonTitles:nil];
    [message show];*/
}
- (void) setViewController: (UIViewController *) controller {
    [self.HUD removeFromSuperview];
    self.rootController = controller;
    if (self.HUD) {
        [self hide];
        HUD = nil;
    }
    if (self.rootController.navigationController.view != nil)
    {
        self.HUD = [[MBProgressHUD alloc] initWithView:self.rootController.navigationController.view];
        [self.rootController.navigationController.view addSubview:HUD];
    }
    else
    {
        self.HUD = [[MBProgressHUD alloc] initWithView:self.rootController.view];
        [self.rootController.view addSubview:HUD];
    }
}

-(void) show{
    HUD.labelText = @"";
    [HUD show:NO];
}

-(void) showWithLabel: (NSString *) text{
    HUD.labelText = text;
    [HUD show:NO];
}

-(void) hide{
    [HUD hide:NO];
}

- (void)hudWasHidden:(MBProgressHUD *)hud{
    //do nothing
}
    
    
@end
