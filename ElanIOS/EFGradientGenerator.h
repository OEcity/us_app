//
//  EFGradientGenerator.h
//  RGBViewController
//
//  Created by admin on 21.06.16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


/*
 *  Supporting class for storing colors in colors array.
 */
@interface ColorContainer : NSObject
    @property (strong, nonatomic) UIColor *color;
    @property (nonatomic) NSUInteger sector;
@end




@interface EFGradientGenerator : NSObject

/*
 *  This class purpose is to generate gradient circle. Use init with frame, specify position of target colors and then render image. We consider 0° EAST as 0° in output circle matrics.
 */

/*
 *  In circle is finite number of sectors with colors. There is no antialiasing, so you need to have enough secotrs to provide nice image. Minimum number of sectors is 1. WARNING: sectors setter dont allow to set O. It sets 1 instead. DO NOT! chnage this property without rendering gradient imediately!
 */
@property (nonatomic) NSUInteger sectors;

/*
 *  Array of colors. If any color out of range [0, sectors - 1] is removed by setter or in image rendering time. Default color is [UIColor clearColor]
 */
@property (strong, readonly, nonatomic) NSMutableArray <ColorContainer *> *colors;

/*
 *  Color behind circle (if is circle smaller than frame)
 */
@property (strong, nonatomic) UIColor *backgroundColor;

/*
 *  Output image property.
 */
@property (strong, readonly, nonatomic) UIImage *renderedImage;

/*
 *  Radius of the circle.
 */
@property (nonatomic) CGFloat radius;

/*
 *  Size of the output image.
 */
@property (nonatomic) CGSize size;

/*
 * Contains UIColor for each sector.
 */
@property (readonly, strong, nonatomic) NSMutableArray <UIColor *> *colorArray;

-(id)initWithSize:(CGSize)size;

-(void)addNewColor:(UIColor *)color atSector:(NSUInteger)sector;
-(void)removeColorFromIndex:(NSUInteger)index;

-(UIColor *)getColorFromAngleFromNorth:(CGFloat)angle;

-(void)renderImage;

-(CGFloat)getAngleFromNorthForSector:(NSUInteger)sector;

//All collors are numbers in range [0,1]. For better look I recommend to pass tolerances like x/255.0 numbers. Many problems are caused by passing int instead.
-(NSArray<NSNumber *> *)getSegmentForUIColor:(UIColor *)color;


+(EFGradientGenerator *)getStaticInstanceForKey:(NSString *)key;
+(EFGradientGenerator *)createStaticInstanceForKey:(NSString *)key withSize:(CGSize)size;
+(BOOL)hasStaticInstanceForKey:(NSString *)key;
+(NSArray <NSString *> * )allStaticInstanceKeys;

@end
