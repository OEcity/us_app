////
////  DeviceTimeScheduleView.m
////  iHC-MIRF
////
////  Created by Tom Odler on 15.03.16.
////  Copyright © 2016 Vratislav Zima. All rights reserved.
////
//
//#import "DeviceTimeScheduleView.h"
//
//
//
//#define WATCHES_TOUCH_PADDING 0
//
//#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
//
//@implementation DeviceTimeScheduleView{
//    UIColor * paint;
//    UIColor * modeTimePaint;
//    float WIDTH_TO_TIME;
//    float LINE_TOP_OFFSET;
//    int mModePosition;
//    UIImageView *mWatches;
//    NSMutableArray *modesStartX;
//    NSMutableArray *modesEndX;
//    NSMutableArray * watchRects;
//    int offsetTop;
//    UIImageView * draggedView;
//    int lastPan;
//}
//
///*
// // Only override drawRect: if you perform custom drawing.
// // An empty implementation adversely affects performance during animation.
// - (void)drawRect:(CGRect)rect {
// // Drawing code
// }
// */
//
//-(id)initWithFrameAndDay:(CGRect)frame tempScheduleDay:(DeviceScheduleDay*)day topOffset:(int)topOffset type:(NSString*)type parentViewController:(id)viewController{
//    self = [super initWithFrame:frame];
//    if (self){
//        offsetTop = topOffset;
//        _mDay = day;
//        _type = type;
//        mModePosition=-1;
//        mWatches = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"action_clock_off"]];
//        UIPanGestureRecognizer * panGestRecogn = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
//        panGestRecogn.delegate=self;
//        [self addGestureRecognizer:panGestRecogn];
//        _parentViewController = viewController;
//        [self repaintContent:_type];
//    }
//    return  self;
//}
//-(void)drawSwitchModes{
//    WIDTH_TO_TIME = self.frame.size.width / 1440;
//    NSInteger frameHeight  =(self.frame.size.height - offsetTop);
//    if ([_mDay.modes count] == 0)
//        return;
//    modesStartX = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
//    modesEndX = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
//    
//    int i=0;
//    float startX = 0;
//    for (TempDayMode *mode in _mDay.modes) {
//        modesStartX[i] = [[NSNumber alloc ] initWithFloat:startX + [mode.startTime intValue] * WIDTH_TO_TIME] ;
//        modesEndX[i] = [[NSNumber alloc ] initWithFloat: ([mode.endTime intValue] - [mode.startTime intValue]) * WIDTH_TO_TIME];
//        
//        switch ([mode.mode intValue]) {
//            case 4:
//                paint = UIColorFromRGB(0xEE7768);
//                modeTimePaint = [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//                
//            case 3:
//                paint = UIColorFromRGB(0xF5A783);
//                modeTimePaint= [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//                
//            case 2:
//                paint = UIColorFromRGB(0xFFF7BF);
//                modeTimePaint = [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//                
//            case 1:
//                paint = UIColorFromRGB(0x5BC5F2);
//                modeTimePaint = [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//            default:
//                paint = nil;
//                modeTimePaint = nil;
//                break;
//        }
//        float lineTopOffset = 1.2f;
//        NSLog(@"%@", mode.type);
//        
//        UIView * areaRect =[[UIView alloc] initWithFrame:CGRectMake([modesStartX[i] floatValue],offsetTop+ frameHeight*(-1+[mode.mode intValue]) + lineTopOffset, [modesEndX[i] floatValue], frameHeight-lineTopOffset)];
//        
//        [areaRect setBackgroundColor:paint];
//        areaRect.tag = i+10;
//        [self addSubview:areaRect];
//        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(areaTapped:)];
//        [areaRect addGestureRecognizer:tapRecognizer];
//        
//        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
//                                              initWithTarget:self action:@selector(handleLongPress:)];
//        lpgr.minimumPressDuration = 0.5; //seconds
//        lpgr.delegate = self;
//        [areaRect addGestureRecognizer:lpgr];
//        
//        
//        NSString* time =[NSString stringWithFormat:@"%@-%@",  [self getTimeFromMinute:[mode.startTime intValue]], [self getTimeFromMinute:[mode.endTime intValue]]];
//        
//        
//        UILabel * timeLabel = [[UILabel alloc] initWithFrame:areaRect.frame];
//        timeLabel.font = [UIFont systemFontOfSize:13];
//        [timeLabel setTextColor:modeTimePaint];
//        [timeLabel setTextAlignment:NSTextAlignmentCenter];
//        [timeLabel setText:time];
//        CGSize stringBoundingBox = [time sizeWithAttributes:@{NSFontAttributeName: timeLabel.font}];
//        
//        if (stringBoundingBox.width < timeLabel.frame.size.width){
//            //add
//            [self addSubview:timeLabel];
//        }
//        i++;
//    }
//}
//
//-(void)drawShuttersModes{
//    WIDTH_TO_TIME = self.frame.size.width / 1440;
//    NSInteger frameHeight  =(self.frame.size.height - offsetTop)/2;
//    if ([_mDay.modes count] == 0)
//        return;
//    modesStartX = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
//    modesEndX = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
//    
//    int i=0;
//    float startX = 0;
//    for (TempDayMode *mode in _mDay.modes) {
//        modesStartX[i] = [[NSNumber alloc ] initWithFloat:startX + [mode.startTime intValue] * WIDTH_TO_TIME] ;
//        modesEndX[i] = [[NSNumber alloc ] initWithFloat: ([mode.endTime intValue] - [mode.startTime intValue]) * WIDTH_TO_TIME];
//        
//        switch ([mode.mode intValue]) {
//            case 4:
//                paint = UIColorFromRGB(0xEE7768);
//                modeTimePaint = [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//                
//            case 3:
//                paint = UIColorFromRGB(0xF5A783);
//                modeTimePaint= [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//                
//            case 2:
//                paint = UIColorFromRGB(0xFFF7BF);
//                modeTimePaint = [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//                
//            case 1:
//                paint = UIColorFromRGB(0x5BC5F2);
//                modeTimePaint = [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//            default:
//                paint = nil;
//                modeTimePaint = nil;
//                break;
//        }
//        float lineTopOffset = 1.2f;
//        
//        UIView * areaRect =[[UIView alloc] initWithFrame:CGRectMake([modesStartX[i] floatValue],offsetTop+ frameHeight*(-1+[mode.mode intValue]) + lineTopOffset, [modesEndX[i] floatValue], frameHeight-lineTopOffset)];
//        
//        [areaRect setBackgroundColor:paint];
//        areaRect.tag = i+10;
//        [self addSubview:areaRect];
//        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(areaTapped:)];
//        [areaRect addGestureRecognizer:tapRecognizer];
//        
//        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
//                                              initWithTarget:self action:@selector(handleLongPress:)];
//        lpgr.minimumPressDuration = 0.5; //seconds
//        lpgr.delegate = self;
//        [areaRect addGestureRecognizer:lpgr];
//        
//        
//        NSString* time =[NSString stringWithFormat:@"%@-%@",  [self getTimeFromMinute:[mode.startTime intValue]], [self getTimeFromMinute:[mode.endTime intValue]]];
//        
//        
//        UILabel * timeLabel = [[UILabel alloc] initWithFrame:areaRect.frame];
//        timeLabel.font = [UIFont systemFontOfSize:13];
//        [timeLabel setTextColor:modeTimePaint];
//        [timeLabel setTextAlignment:NSTextAlignmentCenter];
//        [timeLabel setText:time];
//        CGSize stringBoundingBox = [time sizeWithAttributes:@{NSFontAttributeName: timeLabel.font}];
//        
//        if (stringBoundingBox.width < timeLabel.frame.size.width){
//            //add
//            [self addSubview:timeLabel];
//        }
//        i++;
//    }
//}
//
//-(void)drawDimmingModes{
//    WIDTH_TO_TIME = self.frame.size.width / 1440;
//    NSInteger frameHeight  =(self.frame.size.height - offsetTop)/4;
//    if ([_mDay.modes count] == 0)
//        return;
//    modesStartX = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
//    modesEndX = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
//    
//    int i=0;
//    float startX = 0;
//    for (TempDayMode *mode in _mDay.modes) {
//        modesStartX[i] = [[NSNumber alloc ] initWithFloat:startX + [mode.startTime intValue] * WIDTH_TO_TIME] ;
//        modesEndX[i] = [[NSNumber alloc ] initWithFloat: ([mode.endTime intValue] - [mode.startTime intValue]) * WIDTH_TO_TIME];
//        int modeDraw = [mode.mode intValue];
//
//        
//        switch ([mode.mode intValue]) {
//            case 4:
//                paint = UIColorFromRGB(0xEE7768);
//                modeTimePaint = [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//                
//            case 3:
//                paint = UIColorFromRGB(0xF5A783);
//                modeTimePaint= [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//                
//            case 2:
//                paint = UIColorFromRGB(0xFFF7BF);
//                modeTimePaint = [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//                
//            case 5:
//                paint = UIColorFromRGB(0x5BC5F2);
//                modeTimePaint = [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//            default:
//                paint = [UIColor clearColor];
//                modeTimePaint = [UIColor clearColor];
//                break;
//        }
//        float lineTopOffset = 1.2f;
//
//                if(modeDraw == 5){
//                    modeDraw = 1;
//                } else if(modeDraw == 1){
//                    modeDraw = 5;
//                }
//
//        
//        UIView * areaRect =[[UIView alloc] initWithFrame:CGRectMake([modesStartX[i] floatValue],offsetTop+ frameHeight*(-1+modeDraw) + lineTopOffset, [modesEndX[i] floatValue], frameHeight-lineTopOffset)];
//        
//        [areaRect setBackgroundColor:paint];
//        areaRect.tag = i+10;
//        [self addSubview:areaRect];
//        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(areaTapped:)];
//        [areaRect addGestureRecognizer:tapRecognizer];
//        
//        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
//                                              initWithTarget:self action:@selector(handleLongPress:)];
//        lpgr.minimumPressDuration = 0.5; //seconds
//        lpgr.delegate = self;
//        [areaRect addGestureRecognizer:lpgr];
//        
//        
//        NSString* time =[NSString stringWithFormat:@"%@-%@",  [self getTimeFromMinute:[mode.startTime intValue]], [self getTimeFromMinute:[mode.endTime intValue]]];
//        
//        if(modeDraw != 5){
//        UILabel * timeLabel = [[UILabel alloc] initWithFrame:areaRect.frame];
//        timeLabel.font = [UIFont systemFontOfSize:13];
//
//        
//        [timeLabel setTextColor:modeTimePaint];
//        [timeLabel setTextAlignment:NSTextAlignmentCenter];
//        [timeLabel setText:time];
//        CGSize stringBoundingBox = [time sizeWithAttributes:@{NSFontAttributeName: timeLabel.font}];
//        
//        if (stringBoundingBox.width < timeLabel.frame.size.width){
//            //add
//            [self addSubview:timeLabel];
//        }
//        }
//
//        i++;
//    }
//
//}
//
//- (void)areaTapped:(UITapGestureRecognizer*)sender {
//    if ([_mDay.modes count] == 1)
//        return;
//    
//    if (sender.view.tag>=10){
//        mModePosition = (int)sender.view.tag-10;
//        
//    }
//    [self repaintContent:_type];
//}
//
//
//
//-(void)drawModeSelection{
//    float timeAxisEndY = self.frame.size.height-offsetTop;
//    float timeAxisStartY = offsetTop;
//    
//    if (mModePosition < 0 || mModePosition >= [_mDay.modes count]) {
//        return;
//    }
//    float offsetLeft = mModePosition == 0 ? 1.5f/2 : 0;
//    if (mModePosition != 0){
//        UIView * leftLine = [[UIView alloc] initWithFrame:CGRectMake([modesStartX[mModePosition] floatValue]+ offsetLeft, timeAxisStartY, 1.5f/*modesStartX[mModePosition] + offsetLeft*/, timeAxisEndY)];
//        [leftLine setBackgroundColor:[UIColor blackColor]];
//        [self addSubview:leftLine];
//        
//    }
//    
//    float offsetRight = mModePosition == [_mDay.modes count] - 1 ? -1.5f / 2 : 0;
//    if (mModePosition != [_mDay.modes count] - 1){
//        UIView * rightLine = [[UIView alloc] initWithFrame:CGRectMake([modesEndX[mModePosition] floatValue] + offsetRight + [modesStartX[mModePosition] floatValue], timeAxisStartY, 1.5f/*modesEndX[mModePosition] + offsetRight*/, timeAxisEndY)];
//        [rightLine setBackgroundColor:[UIColor blackColor]];
//        [self addSubview:rightLine];
//        
//    }
//    // If not in touch, init rects
//    
//    float x = [modesStartX[mModePosition] floatValue]  - 15 ;
//    float y = timeAxisStartY - 25;
//    if (watchRects==nil) watchRects = [[NSMutableArray alloc] init];
//    if (mModePosition != 0){
//        [mWatches setFrame:CGRectMake(x - WATCHES_TOUCH_PADDING, y,  30 + WATCHES_TOUCH_PADDING,  30 + WATCHES_TOUCH_PADDING)];
//        UIImageView * copyOfImage = [[UIImageView alloc] initWithFrame:mWatches.frame];
//        [copyOfImage setImage:mWatches.image];
//        [watchRects addObject:copyOfImage];
//        copyOfImage.tag=101;
//        
//        
//    }
//    
//    x = [modesEndX[mModePosition] floatValue]+[modesStartX[mModePosition] floatValue]  - 15;
//    y = timeAxisStartY - 25;
//    if (mModePosition != [_mDay.modes count] - 1){
//        [mWatches setFrame:CGRectMake(x - WATCHES_TOUCH_PADDING, y,  30 + WATCHES_TOUCH_PADDING,  30 + WATCHES_TOUCH_PADDING)];
//        UIImageView * copyOfImage = [[UIImageView alloc] initWithFrame:mWatches.frame];
//        [copyOfImage setImage:mWatches.image];
//        [watchRects addObject:copyOfImage];
//        copyOfImage.tag=102;
//        
//        
//    }
//    
//    for (UIImageView * image in watchRects){
//        [self addSubview:image];
//    }
//}
//- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer {
//    if(recognizer.state == UIGestureRecognizerStateEnded)
//    {
//        //Confirm mode changes
//        draggedView= nil;
//        
//        [_mDay redefineModes];
//        mModePosition = [_mDay joinAndDeleteModes:mModePosition ];
//        [self repaintContent: _type];
//        
//        
//    } if (recognizer.state == UIGestureRecognizerStateBegan){
//        lastPan = 0;
//        CGPoint location = [recognizer locationInView:self];
//        for (UIView * subview in [recognizer.view subviews]){
//            if (subview.tag>100 && subview.frame.origin.x -10 < location.x
//                && subview.frame.origin.x + subview.frame.size.width + 20 > location.x
//                && subview.frame.origin.y -10 < location.y
//                && subview.frame.origin.y + subview.frame.size.height + 20 > location.y){
//                draggedView = (UIImageView*)subview;
//                
//            }
//            
//        }
//        
//    }else if (draggedView!=nil){
//        
//        [self processTouch:recognizer];
//        
//    }
//    
//}
//-(NSString*) getTimeFromMinute:(int) minutes {
//    int hours = minutes / 60;
//    return [NSString stringWithFormat:@"%02d:%02d", hours,minutes - hours * 60];
//}
//
//-(void)repaintContent:(NSString*)type{
//    NSArray *viewsToRemove = [self subviews];
//    if (watchRects != nil){
//        [watchRects removeAllObjects];
//    }
//    for (UIView *v in viewsToRemove) {
//        [v removeFromSuperview];
//    }
//    NSLog(@"MY TYPE: %@", type);
//    if([type isEqualToString:@"switch"]){
//        [self drawSwitchModes];
//    } else if([type isEqualToString:@"shutters"]){
//        [self drawShuttersModes];
//    } else if([type isEqualToString:@"diming"]){
//        [self drawDimmingModes];
//    } else if([type isEqualToString:@"rgb"] || [type isEqualToString:@"white"]){
//        [self drawRGBmodes];
//    }
//    
//    [self drawModeSelection];
//    
//    
//}
//
//-(void) processTouch:(UIPanGestureRecognizer *)recognizer {
//    UIImageView * image = draggedView;
//    TempDayMode* mode = [_mDay.modes objectAtIndex:mModePosition];
//    
//    float tempStart = [mode.startTime floatValue];
//    float tempEnd = [mode.endTime floatValue];
//    BOOL isLeftDirection = image.tag==101?YES:NO;
//    
//    CGPoint translation = [recognizer translationInView:self];
//    //recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
//    //                                    recognizer.view.center.y + translation.y);
//    
//    
//    
//    
//    if (isLeftDirection)
//        tempStart = [mode.startTime intValue] + (translation.x-lastPan) / WIDTH_TO_TIME;
//    else
//        tempEnd = [mode.endTime intValue] + (translation.x-lastPan) / WIDTH_TO_TIME;
//    
//    lastPan=translation.x;
//    
//    if ((tempEnd - tempStart) * WIDTH_TO_TIME < mWatches.frame.size.width )
//        return;
//    else {
//        mode.startTime = [[NSNumber alloc] initWithInt: tempStart];
//        mode.endTime = [[NSNumber alloc] initWithInt: tempEnd];
//    }
//    
//    
//    if ([mode.startTime intValue] < 0)
//        mode.startTime = 0;
//    else if ([mode.endTime intValue] > 1440)
//        mode.endTime = [[NSNumber alloc] initWithInt: 1440];
//    
//    [_mDay recountModes:isLeftDirection modePosition:mModePosition];
//    
//    [self repaintContent:_type];
//}
//
//
//-(void) addMode:(TempDayMode*) mode position:(int) position{
//    mModePosition = [_mDay editMode:mode modePosition:position] ;
//    [self repaintContent:_type];
//}
//
//-(void) deleteMode:(int) result type:(NSString*)type{
//    mModePosition = [_mDay deleteMode:result type:type];
//    [self repaintContent:_type];
//}
//
//
//-(void) setDay:(DeviceScheduleDay*) day {
//    _mDay = day;
//    mModePosition = -1;
//    [self repaintContent:_type];
//}
//
//-(DeviceScheduleDay*) getDay {
//    if(_mDay == nil)
//        return nil;
//    
//    for (TempDayMode* mode in _mDay.modes) {
//        mode.duration = [[NSNumber alloc] initWithInt:[mode.endTime intValue] - [mode.startTime intValue]];
//    }
//    
//    return _mDay;
//}
//
//-(void)drawRGBmodes{
//    WIDTH_TO_TIME = self.frame.size.width / 1440;
//    NSInteger frameHeight  =(self.frame.size.height - offsetTop)/4;
//    if ([_mDay.modes count] == 0)
//        return;
//    modesStartX = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
//    modesEndX = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
//    
//    int i=0;
//    float startX = 0;
//    for (TempDayMode *mode in _mDay.modes) {
//        modesStartX[i] = [[NSNumber alloc ] initWithFloat:startX + [mode.startTime intValue] * WIDTH_TO_TIME] ;
//        modesEndX[i] = [[NSNumber alloc ] initWithFloat: ([mode.endTime intValue] - [mode.startTime intValue]) * WIDTH_TO_TIME];
//        
//        switch ([mode.mode intValue]) {
//            case 5:
//                paint = UIColorFromRGB(0xEE7768);
//                modeTimePaint = [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//                
//            case 4:
//                paint = UIColorFromRGB(0xF5A783);
//                modeTimePaint= [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//                
//            case 3:
//                paint = UIColorFromRGB(0xFFF7BF);
//                modeTimePaint = [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//                
//            case 2:
//                paint = UIColorFromRGB(0x5BC5F2);
//                modeTimePaint = [UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1];
//                break;
//            default:
//                paint = [UIColor clearColor];
//                modeTimePaint = [UIColor clearColor];
//                break;
//        }
//        float lineTopOffset = 1.2f;
//        
//        UIView * areaRect =[[UIView alloc] initWithFrame:CGRectMake([modesStartX[i] floatValue],offsetTop+ frameHeight*(-2+[mode.mode intValue]) + lineTopOffset, [modesEndX[i] floatValue], frameHeight-lineTopOffset)];
//        
//        [areaRect setBackgroundColor:paint];
//        areaRect.tag = i+10;
//        [self addSubview:areaRect];
//        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(areaTapped:)];
//        [areaRect addGestureRecognizer:tapRecognizer];
//        
//        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
//                                              initWithTarget:self action:@selector(handleLongPress:)];
//        lpgr.minimumPressDuration = 0.5; //seconds
//        lpgr.delegate = self;
//        [areaRect addGestureRecognizer:lpgr];
//        
//        
//        NSString* time =[NSString stringWithFormat:@"%@-%@",  [self getTimeFromMinute:[mode.startTime intValue]], [self getTimeFromMinute:[mode.endTime intValue]]];
//        
//        if(mode.mode != [[NSNumber alloc]initWithInteger:1]){
//            UILabel * timeLabel = [[UILabel alloc] initWithFrame:areaRect.frame];
//            timeLabel.font = [UIFont systemFontOfSize:13];
//            
//            
//            [timeLabel setTextColor:modeTimePaint];
//            [timeLabel setTextAlignment:NSTextAlignmentCenter];
//            [timeLabel setText:time];
//            CGSize stringBoundingBox = [time sizeWithAttributes:@{NSFontAttributeName: timeLabel.font}];
//            
//            if (stringBoundingBox.width < timeLabel.frame.size.width){
//                //add
//                [self addSubview:timeLabel];
//            }
//        }
//        
//        i++;
//    }
//    
//}
//
//
//-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
//{
//    
//    int modePos = -1;
//    
//    if (gestureRecognizer.view.tag>=10){
//        modePos = (int)gestureRecognizer.view.tag-10;
//        
//    }
//    if (modePos==-1)
//        return;
//    
//    
//    ((DeviceSheduleIntervalViewController*)_parentViewController).addDeviceInterval = [[AddDeviceScheduleIntervalViewController alloc] initWithNibName:@"AddDeviceScheduleIntervalViewController" bundle:nil];
//    ((DeviceSheduleIntervalViewController*)_parentViewController).addDeviceInterval.delegate = _parentViewController;
//    ((DeviceSheduleIntervalViewController*)_parentViewController).addDeviceInterval.mMode = [_mDay.modes objectAtIndex:modePos];
//    ((DeviceSheduleIntervalViewController*)_parentViewController).addDeviceInterval.modePos = modePos;
//      ((DeviceSheduleIntervalViewController*)_parentViewController).addDeviceInterval.type = _type;
//    [_parentViewController presentViewController:((DeviceSheduleIntervalViewController*)_parentViewController).addDeviceInterval animated:YES completion:nil];
//    
//    
//    
//    /*
//     
//     HeatScheduleEditDialog dialog = HeatScheduleEditDialog.newInstance(mDay.modes.get(modePos), modePos, mDay.modes.size() == 1);
//     dialog.setOnIntervalDialogEndListener((OnIntervalDialogEndListener) getContext());
//     dialog.show(((FragmentActivity) getContext()).getSupportFragmentManager(), "scheduleEditDialog");
//     */
//}
//
//
//
//@end
