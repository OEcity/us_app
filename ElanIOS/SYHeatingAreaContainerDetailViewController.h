//
//  SYHeatingAreaContainerDetailViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/17/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TemperatureOffsetViewController.h"
#import "HUDWrapper.h"
#import "SYHeatContainerViewController.h"
#import "PickerViewController.h"

@interface SYHeatingAreaContainerDetailViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate, UITableViewDataSource, UITableViewDelegate, TemperatureSetterDelegate, PickerViewDelegate>
@property (nonatomic, retain) NSArray * temperatureSensors;
@property (nonatomic, retain) Device * temperatureSensor;

@property (nonatomic, retain) NSArray * availableHeaters;
@property (nonatomic, retain) NSMutableArray * heatersSelected;

@property (nonatomic, retain) NSMutableArray * temperatureSchedules;
@property (nonatomic, retain) NSMutableDictionary * selectedSettings;
@property ( nonatomic, retain) TemperatureOffsetViewController * temperatureOffset;
@property float realTemperature;
@property (weak, nonatomic) IBOutlet UITableView *tableView;


@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UITextField *tFName;
@property (weak, nonatomic) IBOutlet UILabel *lTemperatureSensor;
@property (weak, nonatomic) IBOutlet UILabel *lTemperatureOfset;
@property (weak, nonatomic) IBOutlet UILabel *lTemperatureSchedule;
@property (nonatomic, retain) NSString * temperatureSchedule;
@property (nonatomic, retain) HUDWrapper * loaderDialog;
@property (weak, nonatomic) IBOutlet UITableView *tVHeatings;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) SYHeatContainerViewController * container;
@property (nonatomic, retain) HeatCoolArea * device;
@property (nonatomic, retain) NSString * deviceId;
@property (weak, nonatomic) IBOutlet UIButton *addHeatingButton;
@property (weak, nonatomic) IBOutlet UILabel *lNameTitle;
@property (weak, nonatomic) IBOutlet UILabel *lAddTemperatureSensorTitle;
@property (weak, nonatomic) IBOutlet UILabel *lTemperatureOffsetTitle;
@property (weak, nonatomic) IBOutlet UILabel *lTemperatureScheduleTitle;
@property (weak, nonatomic) IBOutlet UILabel *lAddHeatingTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnHeatingArea;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

//-(void)setTemperature:(float)realTemperature;
@property (nonatomic, retain) PickerViewController * pickerViewController;

@end
