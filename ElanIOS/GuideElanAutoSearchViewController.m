//
//  GuideElanAutoSearchViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 28.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "GuideElanAutoSearchViewController.h"
#import "IconWithLabelTableViewCell.h"
#import "SYCoreDataManager.h"
#import "AppDelegate.h"
#import "Constants.h"

#define ELAN_STATE_SAVED 1
#define ELAN_STATE_MODIFIED 2
#define ELAN_STATE_NEW 3

@interface GuideElanAutoSearchViewController ()
@property (nonatomic, strong) NSFetchedResultsController* elans;


@end

@implementation GuideElanAutoSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _savedElans = [[SYCoreDataManager sharedInstance] getAllElans];
    [[SearchService sharedSearchService] sendUdpDiscoverySocketWithDelegate:self];
    
    _btnRefresh.layer.borderWidth = 1.0f;
    _btnRefresh.layer.borderColor = [USBlueColor CGColor];
    
    //    _broadcastTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(aTime) userInfo:nil repeats:YES];
    //CGFloat screenHeight = self.view.frame.size.height;
    // _lnoElan = [[UILabel alloc] initWithFrame:CGRectMake(0, 30+ screenHeight/2  - 10, 320, 20)];
    //_lnoElan.textAlignment = NSTextAlignmentCenter;
    
    
    
}
-(void)initLocalizableStrings{
    [_lnoElan setText:NSLocalizedString(@"no_elan_text", nil)];
    [_lSearchingElanTitle setText:NSLocalizedString(@"searching_elan_title", nil)];
    [_btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_btnRefresh setTitle:NSLocalizedString(@"control_box_refresh", nil) forState:UIControlStateNormal];
    [_lTitle setText:NSLocalizedString(@"elan_settigns", nil)];
}


-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[SearchService sharedSearchService] stopUdpDiscovery];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Elan"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _elans = [[NSFetchedResultsController alloc]
              initWithFetchRequest:fetchRequest
              managedObjectContext:context
              sectionNameKeyPath:nil
              cacheName:nil];
    _elans.delegate = self;
    NSError *error;
    
    if (![_elans performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            //            self.emptyLabel.hidden = [_fetchedResultsController.fetchedObjects count] > 0;
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(IconWithLabelTableViewCell*)[_tableView cellForRowAtIndexPath:indexPath]
                    atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}
- (void)configureCell:(IconWithLabelTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
    Elan * elan = [_elanArray objectAtIndex:indexPath.row];
    
    
    [cell.lLabel setText:elan.baseAddress];
    
    
    switch ([self determineElanState:elan]) {
        case ELAN_STATE_NEW:
            [cell.lSubLabel setText:NSLocalizedString(@"elan_new_elan",nil)];
            [cell.ivIcon setImage:[UIImage imageNamed:@"ic_elan_new"]];
            break;
        case ELAN_STATE_SAVED:
            [cell.lSubLabel setText:NSLocalizedString(@"elan_exists_error",nil)];
            [cell.ivIcon setImage:[UIImage imageNamed:@"ic_elan_exists"]];
            break;
        case ELAN_STATE_MODIFIED:
            [cell.lSubLabel setText:NSLocalizedString(@"elan_updated_elan",nil)];
            [cell.ivIcon setImage:[UIImage imageNamed:@"ic_elan_exists"]];
            break;
            
        default:
            [cell.ivIcon setImage:[UIImage imageNamed:@"elan_new_elan"]];
            break;
    }
    
    if (elan.label!=nil){
        [cell.lLabel setText:elan.label];
        
    }
    
}

- (IBAction)reloadButtonTap:(id)sender {
    _loadingTopViewHeightConstrint.constant =60;
    _loadingTopView.hidden=NO;
    _lnoElan.hidden=NO;
    [_elanArray removeAllObjects];
    [_tableView reloadData];
    
    [[SearchService sharedSearchService] sendUdpDiscoverySocketWithDelegate:self];
}

-(void)aTime{
    [[SearchService sharedSearchService] sendUdpDiscoverySocketWithDelegate:self];
}


- (IBAction)backButtonTap:(id)sender {
    [self performSegueWithIdentifier:@"elanSearched" sender:self];
    //[self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark SearchServiceDelegate methods

-(void)searchServiceFinishedWithResult:(Elan*)discoveredElan{
    if (discoveredElan==nil){
        _lnoElan.hidden = NO;
        return;
    }
    
    
    _lnoElan.hidden=YES;
    if (!_elanArray) _elanArray = [[NSMutableArray alloc] init];
    if ([self isELANContainedInArray:discoveredElan]) return;
    [_elanArray addObject:discoveredElan];
    [_tableView reloadData];
    
}

-(BOOL)isELANContainedInArray:(Elan*)newElan{
    for (Elan * elan in _elanArray){
        if ([elan.baseAddress isEqualToString:newElan.baseAddress] && [elan.mac isEqualToString:newElan.mac]) return YES;
    }
    return NO;
}


-(void)searchServiceTimedOut{
    _loadingTopViewHeightConstrint.constant =0;
    _loadingTopView.hidden=YES;
}

#pragma mark UITableViewDataSource methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_elanArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString* cellIdentifier = @"FoundElanCell";
    IconWithLabelTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[IconWithLabelTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}
#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _selectedElan = [_elanArray objectAtIndex:indexPath.row];
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected=NO;
    if ([_selectedElan.state intValue] == ELAN_STATE_NEW){
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"elan_name_dialog_title", nil)
                                                          message:nil
                                                         delegate:self
                                                cancelButtonTitle:NSLocalizedString(@"confirmation.cancel",nil)
                                                otherButtonTitles:NSLocalizedString(@"confirmation.done", nil), nil];
        [message setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [message show];
    }else if ([_selectedElan.state intValue]==ELAN_STATE_MODIFIED){
        
        for (Elan * elan in _savedElans){
            if ([elan.mac isEqualToString:_selectedElan.mac]){
                elan.baseAddress = [_selectedElan.baseAddress copy];
                elan.firmware = [_selectedElan.firmware copy];
                [_elanArray removeObject:_selectedElan];
            }
            
        }
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if([title isEqualToString:NSLocalizedString(@"confirmation.done", nil)])
    {
        
        Elan * elan = [[SYCoreDataManager sharedInstance] createEntityElanInContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        
        elan.label = [alertView textFieldAtIndex:0].text;
        elan.baseAddress = _selectedElan.baseAddress;
        elan.mac = _selectedElan.mac;
        elan.firmware = _selectedElan.firmware;
        [[SYCoreDataManager sharedInstance] saveContext];
        [self backButtonTap:nil];
        
    }
}


-(int)determineElanState:(Elan*)elan{
    for (Elan * savedElan in _savedElans){
        
        
        if ([savedElan.baseAddress isEqualToString:elan.baseAddress] && [savedElan.mac isEqualToString:elan.mac]){
            elan.state = [[NSNumber alloc] initWithInt:ELAN_STATE_SAVED];
            elan.label = [savedElan.label copy];
            return [elan.state intValue];
            
        }else if (![savedElan.baseAddress isEqualToString:elan.baseAddress] && [savedElan.mac isEqualToString:elan.mac]){
            elan.state = [[NSNumber alloc] initWithInt:ELAN_STATE_MODIFIED];
            elan.label = savedElan.label;
            return [elan.state intValue];
            
        }
        
        
    }
    elan.state = [[NSNumber alloc] initWithInt:ELAN_STATE_NEW];
    return [elan.state intValue];
    
}


- (NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}

@end
