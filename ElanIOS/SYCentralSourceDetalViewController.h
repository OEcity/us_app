//
//  SYCentralSourceDetalViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 12/5/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYHeatContainerViewController.h"
#import "HUDWrapper.h"
#import "PickerViewController.h"

@interface SYCentralSourceDetalViewController : UIViewController <PickerViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
@property (nonatomic, retain) SYHeatContainerViewController * container;
@property (nonatomic, retain) NSArray * HeatCoolArea;
@property (nonatomic, retain) NSArray * centralSourceElements;
@property (nonatomic, retain) NSMutableArray * heatersSelected;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lHeatingArea;
@property (nonatomic, retain) Device * device;
@property (nonatomic, retain) Device * centralSourceElement;
@property (nonatomic, retain) PickerViewController * pickerViewController;
@property (nonatomic, retain) NSDictionary * centralSourceItem;

@property (weak, nonatomic) IBOutlet UITextField *tfName;
@property (weak, nonatomic) IBOutlet UILabel *lElement;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (nonatomic, retain) HUDWrapper* loaderDialog;
@property (weak, nonatomic) IBOutlet UILabel *lElementTitle;
@property (weak, nonatomic) IBOutlet UILabel *lNameTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@end
