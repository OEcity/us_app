//
//  DimmingCollectionViewCell.h
//  US App
//
//  Created by Tom Odler on 30.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device+State.h"
#import "EFCircularSlider.h"

@interface DimmingCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *deviceLabel;
@property (weak, nonatomic) IBOutlet UIView *circleView;
@property (weak, nonatomic) IBOutlet UIImageView *deviceImage;
@property (weak, nonatomic) IBOutlet UILabel *automatLabel;
@property (weak, nonatomic) IBOutlet UIImageView *delayed;
@property (weak, nonatomic) IBOutlet UIImageView *warning;
@property (nonatomic) Device *device;
@property (weak, nonatomic) IBOutlet UIView *heatingView;
@property (nonatomic) EFCircularSlider *circularSlider;
@end
