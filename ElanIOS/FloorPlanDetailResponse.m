//
//  FloorPlanDetailResponse.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/28/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "FloorPlanDetailResponse.h"
#import <UIKit/UIKit.h>

@implementation FloorPlanDetailResponse

-(NSObject*) parseResponse:(NSData *)inputData identifier:(NSString *)identifier{
    self.floorPlan = [[Floorplan alloc] init];
    self.floorPlan.url = identifier;
    [self.floorPlan setImage:[[UIImage alloc] initWithData:inputData]];
    return self.floorPlan;
    
}

@end
