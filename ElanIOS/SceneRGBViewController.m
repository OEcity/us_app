//
//  SceneRGBViewController.m
//  Click Smart
//
//  Created by Tom Odler on 29.07.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "SceneRGBViewController.h"
#import "EFCircularSlider.h"
#import "EFGradientGenerator.h"

@interface SceneRGBViewController ()
@property (strong, nonatomic) EFGradientGenerator *gradientGen;
@property (strong, nonatomic) EFCircularSlider* circularSlider;
@property (strong, nonatomic) EFCircularSlider* bcircularSlider;

@property (weak, nonatomic) IBOutlet UIView *bottomSliderView;
@property (weak, nonatomic) IBOutlet UIButton *cirkusButton;
@property (weak, nonatomic) IBOutlet UILabel *lisghtDescriptionLabel;

@property (weak, nonatomic) IBOutlet UILabel *productLabel;
@property (weak, nonatomic) IBOutlet UIButton *switchButton;
@property (weak, nonatomic) IBOutlet UIView *topSliderView;
@property (weak, nonatomic) IBOutlet UILabel *topSlideViewValueIndicatorLabel;

@property (nonatomic) Byte red;
@property (nonatomic) Byte green;
@property (nonatomic) Byte blue;
@property (nonatomic) Byte intensity;

@property (nonatomic) BOOL cirkus;

@property (nonatomic) NSMutableDictionary *dict;


@end

@implementation SceneRGBViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _dict = [[NSMutableDictionary alloc] init];
    
    _productLabel.text = _device.productType;
    _lisghtDescriptionLabel.text = _device.label;
    //Configure navigation bar apperence
    //[self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    _red = 255;
    _green = 255;
    _blue = 255;
    
    //CONF BUTTONS
    [[_cirkusButton layer] setBorderColor:[[UIColor colorWithRed:0.0/255.0 green:192.0/255.0 blue:243.0/255.0 alpha:1.0] CGColor]];
    [[_switchButton layer] setBorderColor:[[UIColor colorWithRed:0.0/255.0 green:192.0/255.0 blue:243.0/255.0 alpha:1.0] CGColor]];
    
    
    //TOP SLIDER CONF
    CGRect sliderFrame = CGRectMake(0, 0, _topSliderView.frame.size.width, _topSliderView.frame.size.height);
    _circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    [_circularSlider addTarget:self action:@selector(topSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_topSliderView addSubview:_circularSlider];
    [_circularSlider pauseAutoredrawing];
    
    CGFloat dash[]={1,15};
    [_circularSlider setHandleRadius:10];
    [_circularSlider setUnfilledLineDash:dash andCount:2];
    [_circularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_circularSlider setUnfilledColor:[UIColor whiteColor]];
    [_circularSlider setFilledColor:[UIColor whiteColor]];
    [_circularSlider setHandleColor:[UIColor whiteColor]];
    [_circularSlider setLineWidth:1];
    [_circularSlider setArcStartAngle:150];
    [_circularSlider setArcAngleLength:240];
    
    
    [_circularSlider setCurrentArcValue:(CGFloat)_intensity forStartAnglePadding:2 endAnglePadding:2];
    
    //BOTTOM SLIDER CONF
    
    _gradientGen = [self setupSharedGradientGeneratorForKey:@"BigRGBViewControllerRGBGradient"];
    
    
    CGRect bsliderFrame = CGRectMake(0, 0, _bottomSliderView.frame.size.width, _bottomSliderView.frame.size.height);
    _bcircularSlider = [[EFCircularSlider alloc] initWithFrame:bsliderFrame];
    [_bcircularSlider addTarget:self action:@selector(botSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_bottomSliderView addSubview:_bcircularSlider];
    [_bcircularSlider pauseAutoredrawing];
    
    [_bcircularSlider setImageBackgroundUnfilledLine:YES];
    [_bcircularSlider setLineWidth:8];
    [_bcircularSlider setFilledColor:[UIColor clearColor]];
    [_bcircularSlider setUnfilledLineStrokeBorderWidth:0];
    [_bcircularSlider setUnfilledLineStrokeBorderColor:[UIColor clearColor]];
    [_bcircularSlider setUnfilledLineInnerImage:[_gradientGen renderedImage]];
    [_bcircularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_bcircularSlider setHandleColor:[UIColor whiteColor]];
    [_bcircularSlider setArcStartAngle:130];
    [_bcircularSlider setArcAngleLength:280];
    [_bcircularSlider setHandleRadius:10];
    [_bcircularSlider setHandleBorderSize:2];
    
    [self setColorSliderPosition];
    
    
    
    
    _intensity = 0;
    _red = 0;
    _green = 0;
    _blue = 0;
    _cirkus = NO;
    
    [self loadParametersFromArray];
}

-(EFGradientGenerator *)setupSharedGradientGeneratorForKey:(NSString *)key{
    if ([EFGradientGenerator hasStaticInstanceForKey:key]) {
        return [EFGradientGenerator getStaticInstanceForKey:key];
    }
    
    EFGradientGenerator *gradient = [EFGradientGenerator createStaticInstanceForKey:key withSize:_bottomSliderView.frame.size];
    [gradient setRadius:80];
    [gradient setSectors:360];
    
    
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0] atSector:36];  //WHITE OfR
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:37];  //WHITE
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:40];  //WHITE
    [gradient addNewColor:[UIColor colorWithRed:1 green:0 blue:1 alpha:1] atSector:90];  //PURPLE
    [gradient addNewColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:1] atSector:135]; //BLUE
    [gradient addNewColor:[UIColor colorWithRed:0 green:1 blue:1 alpha:1] atSector:180]; //AZURE
    [gradient addNewColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:1] atSector:225]; //GREEN
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:0 alpha:1] atSector:260]; //YELLOW
    [gradient addNewColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1] atSector:318]; //RED
    [gradient addNewColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1] atSector:323]; //RED
    [gradient addNewColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0] atSector:324]; //RED OfR
    
    [gradient renderImage];
    
    return gradient;
}

-(void)loadParametersFromArray{
    NSArray *mySceneArray = nil;
    
    if(_controller != nil){
        mySceneArray = _controller.actionsArray;
    } else {
        mySceneArray = _guideController.actionsArray;
    }
    
    NSInteger index = 0;
    NSMutableIndexSet *indexForDelete = [[NSMutableIndexSet alloc]init];
    for(NSDictionary*dict in mySceneArray){
        NSString *deviceID =[[dict allKeys]firstObject];
        if([deviceID isEqualToString:_device.deviceID]){
            NSDictionary *myActionsDict = [dict objectForKey:deviceID];
            if([myActionsDict objectForKey:@"demo"]){
                _cirkus = [[myActionsDict objectForKey:@"demo"]boolValue];
                [_cirkusButton setSelected:YES];
            } else{
                _intensity = [[myActionsDict objectForKey:@"brightness"]intValue]/2.55;
                _red = [[myActionsDict objectForKey:@"red"]intValue];
                _green = [[myActionsDict objectForKey:@"green"]intValue];
                _blue = [[myActionsDict objectForKey:@"blue"]intValue];
                [self setColorSliderPosition];
                [_circularSlider setCurrentArcValue:_intensity forStartAnglePadding:2 endAnglePadding:2];
            }
            [indexForDelete addIndex:index];
        }
        index++;
    }
    
    if ([indexForDelete count] > 0){
        if(_guideController)
            [_guideController.actionsArray removeObjectsAtIndexes:indexForDelete];
        else
            [_controller.actionsArray removeObjectsAtIndexes:indexForDelete];
    }
}

-(void)topSliderValueChanged:(EFCircularSlider*)circularSlider {
    if (_cirkus){
        _cirkus = NO;
    }
    [self determineCirkusButton];
    
    _topSlideViewValueIndicatorLabel.text = [NSString stringWithFormat:@"%.f", [circularSlider getCurrentArcValueForStartAnglePadding:2 endAnglePadding:2]];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:_topSlideViewValueIndicatorLabel.text];
    
    _intensity =  (2.55) * [myNumber intValue];
    
}

-(void)botSliderValueChanged:(EFCircularSlider*)circularSlider {
    if (_cirkus){
        _cirkus = NO;
    }
    [self determineCirkusButton];
    
    UIColor *myColor = [_gradientGen getColorFromAngleFromNorth:([circularSlider angleFromNorth])];
    
    [circularSlider setHandleColor:[_gradientGen getColorFromAngleFromNorth:([circularSlider angleFromNorth])]];
    
    CGFloat components[4];
    
    [myColor getRed:&components[0] green:&components[1] blue:&components[2] alpha:&components[3]];
    
    _red = (Byte)(components[0]*255);
    _green = (Byte)(components[1]*255);
    _blue = (Byte)(components[2]*255);
    
    NSLog(@"onChange: RED: %d GREEN: %d BLUE: %d", _red, _green, _blue);
}

- (IBAction)onOffButtonEvent:(id)sender {
    if (self.intensity==0){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *myIntensity = (NSNumber*)[defaults objectForKey:_device.deviceID];
        if (myIntensity!=nil){
            self.intensity = myIntensity.intValue;
            [defaults removeObjectForKey:_device.deviceID];
        }
    }else{
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber * intensityNumber = [[NSNumber alloc] initWithInt:_intensity];
        [defaults setObject:intensityNumber forKey:_device.deviceID];
        [defaults synchronize];
        self.intensity = 0;
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
-(void)saveButtonPressed{
    NSString * outputString = @"{\"red\":";
    outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"%d", _red]];
    outputString = [outputString stringByAppendingString:@", \"green\":"];
    outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"%d", _green]];
    outputString = [outputString stringByAppendingString:@", \"blue\":"];
    outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"%d", _blue]];
    outputString = [outputString stringByAppendingString:@", \"brightness\":"];
    outputString = [outputString stringByAppendingString:[NSString stringWithFormat:@"%d", _intensity]];
    outputString = [outputString stringByAppendingString:@"}"];
    NSLog(@"output string %@", outputString);
}
*/
-(void)setColorSliderPosition{
    if (!_gradientGen || !_bcircularSlider) {
        return;
    }
    
    NSArray *gradientColors = [_gradientGen getSegmentForUIColor:[UIColor colorWithRed:(CGFloat)_red/255.0 green:(CGFloat)_green/255.0 blue:(CGFloat)_blue/255.0 alpha:1.0] ];
    if ([gradientColors count] == 0) {
        [_bcircularSlider setCurrentArcValue:0.0];
    }else{
        NSNumber *number = [gradientColors objectAtIndex:0];
        CGFloat angle = [_gradientGen getAngleFromNorthForSector:[number unsignedIntegerValue]];
        NSLog(@"SettingColor: %lu %f", [number unsignedIntegerValue], angle);
        if ([_bcircularSlider isAngleValidArcAngle:angle]) {
            [_bcircularSlider setAngleFromNorth:angle];
            [self botSliderValueChanged:_bcircularSlider];
        }else{
            [_bcircularSlider setAngleFromNorth:[_bcircularSlider arcStartAngle]];
            [self botSliderValueChanged:_bcircularSlider];
        }
    }
    
}
- (IBAction)cirkusTapped:(id)sender {
    if(_cirkus){
        _cirkus = NO;
    } else {
        _cirkus = YES;
    }
    [self determineCirkusButton];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if(!_cirkus){
        [_dict setObject:[NSNumber numberWithInt:(int)_red] forKey:@"red"];
        [_dict setObject:[NSNumber numberWithInt:(int)_green] forKey:@"green"];
        [_dict setObject:[NSNumber numberWithInt:(int)_blue] forKey:@"blue"];
        [_dict setObject:[NSNumber numberWithInt:(int)_intensity] forKey:@"brightness"];
    } else {
        [_dict setObject:[[NSNull alloc]init]  forKey:@"demo"];
    }
    NSMutableDictionary *deviceWithActionDictionary = [[NSMutableDictionary alloc]init];
    [deviceWithActionDictionary setObject:_dict forKey:_device.deviceID];
    
    if(_controller){
        [[_controller actionsArray] addObject:deviceWithActionDictionary];
        [[self.controller selectedDevices] setObject:_device forKey:_device.deviceID];
    }
    else{
        [[_guideController actionsArray] addObject:deviceWithActionDictionary];
        [[self.guideController selectedDevices] setObject:_device forKey:_device.deviceID];
    }
}
-(void)determineCirkusButton{
    if(_cirkus)
    {
        [_circularSlider setCurrentArcValue:0 forStartAnglePadding:2 endAnglePadding:2];
        [_bcircularSlider setCurrentArcValue:0 forStartAnglePadding:2 endAnglePadding:2];
        [_cirkusButton setSelected:YES];
    } else {
        [_cirkusButton setSelected:NO];
    }
}

@end
