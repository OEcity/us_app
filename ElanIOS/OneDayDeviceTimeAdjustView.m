//
//  OneDayDeviceTimeAdjustView.m
//  iHC-MIRF
//
//  Created by admin on 10.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "OneDayDeviceTimeAdjustView.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation OneDayDeviceTimeAdjustView{
    
    
    float WIDTH_TO_TIME;
    UIColor * paint;
    
    UIImageView *timeFrameImage;
    
    NSMutableArray *modesStartY;
    NSMutableArray *modesEndY;
    
    
    NSMutableArray * watchRects;
    NSMutableArray * timeLabels;
    
    UIImageView * draggedView;
    int lastPan;
    
    
}


/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */
-(void)paintDateFirstTime:(DeviceScheduleDay *)day dayInWeek:(NSInteger)dayInWeek{
    _dayInWeek = dayInWeek;
    _mDay = day;
    timeFrameImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ramecekPlan.png"]];
    UIPanGestureRecognizer * panGestRecogn = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    panGestRecogn.delegate=self;
    [self addGestureRecognizer:panGestRecogn];
    
    [self repaintContent];
    
    
}
-(void)repaintDay:(DeviceScheduleDay *)day{
    _mDay = day;
    [self repaintContent];
    
    
}


-(void)repaintContent{
    NSArray *viewsToRemove = [self subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    if (watchRects != nil){
        [watchRects removeAllObjects];
    }
    
    
    if(timeLabels!=nil){
        for (UILabel *timeLabel in timeLabels){
            [timeLabel removeFromSuperview];
        }
        [timeLabels removeAllObjects];
    }
    
    [self drawModes];
    [self drawModeSelection];
    
    
}
-(void)drawModes{
    WIDTH_TO_TIME = self.frame.size.height / 1440;
    
    if ([_mDay.modes count] == 0)
        return;
    modesStartY = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
    modesEndY = [[NSMutableArray alloc] initWithCapacity:[_mDay.modes count]];
    
    int i=0;
    float startX = 0;
    for (TempDayMode *mode in _mDay.modes) {
        
        
        modesStartY[i] = [[NSNumber alloc ] initWithFloat:startX + [mode.startTime intValue] * WIDTH_TO_TIME] ;
        modesEndY[i] = [[NSNumber alloc ] initWithFloat: ([mode.endTime intValue] - [mode.startTime intValue]) * WIDTH_TO_TIME];
        
        paint = [self.dataSource getColorForPosintion:[mode.mode integerValue]];
        
        
        UIView * areaRect = [[UIView alloc] initWithFrame:CGRectMake(0, [modesStartY[i] floatValue], self.frame.size.width/2,[modesEndY[i] floatValue])];
        
        [areaRect setBackgroundColor:paint];
        areaRect.tag = i+10;
        [self addSubview:areaRect];
        
        if ([self.dataSource shouldShowPercents]){
            //NSNumber *temperature = [NSNumber numberWithInteger:[self.dataSource getPercentOfColor:[mode.mode integerValue]]];
            //NSString *temperatureString = [[temperature stringValue]stringByAppendingString:@" %"];
            NSString *temperatureString = [self.dataSource getPercentOfColor:[mode.mode integerValue]];
            UITextField * temperatureLabel = [[UITextField alloc] initWithFrame:areaRect.frame];
            
            temperatureLabel.font = [UIFont fontWithName:@"Roboto-Light" size:15];
            [temperatureLabel setTextColor: [UIColor blackColor]];
             //[UIColor colorWithRed:109.0f/255.0f green:110.0f/255.0f blue:113.0f/255.0f alpha:1.0f]];
             
            [temperatureLabel setTextAlignment:NSTextAlignmentRight];
            [temperatureLabel setContentVerticalAlignment:UIControlContentVerticalAlignmentBottom];
            temperatureLabel.userInteractionEnabled = NO;
            [temperatureLabel setText:temperatureString];
            CGSize stringBoundingBox = [temperatureString sizeWithAttributes:@{NSFontAttributeName: temperatureLabel.font}];
            if (stringBoundingBox.height < temperatureLabel.frame.size.height){
                
                [self addSubview:temperatureLabel];
            }
        }
        else if ([self.dataSource shouldShowImage]){
            UIImageView *upDownImage = nil;
            switch([mode.mode intValue]){
                case 1:
                    
                    //upDownImage = [[UIImageView alloc] initWithFrame:areaRect.bounds];
                    upDownImage = [[UIImageView alloc] initWithFrame:CGRectMake(areaRect.bounds.origin.x + areaRect.bounds.size.width/4, areaRect.bounds.origin.y, areaRect.bounds.size.width/2, areaRect.bounds.size.height)];
                    [upDownImage setImage:[UIImage imageNamed:@"zaluzieNahoru"]];
                    break;
                case 2:
                    //upDownImage = [[UIImageView alloc] initWithFrame:areaRect.bounds];
                    upDownImage = [[UIImageView alloc] initWithFrame:CGRectMake(areaRect.bounds.origin.x + areaRect.bounds.size.width/4, areaRect.bounds.origin.y, areaRect.bounds.size.width/2, areaRect.bounds.size.height)];
                    //upDownImage = [[UIImageView alloc] init];
                    [upDownImage setImage:[UIImage imageNamed:@"zaluziedolu"]];
                    break;
                default:
                    break;
            }
            if (upDownImage){
                [upDownImage setContentMode:UIViewContentModeScaleAspectFit];
                
                [areaRect addSubview:upDownImage];
                
            }
        }
        
        
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(areaTapped:)];
        [areaRect addGestureRecognizer:tapRecognizer];
        
        
        UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(areaPressed:)];
        [areaRect addGestureRecognizer:longPress];
        
        i++;
    }
}

-(void)drawModeSelection{
    
    if (_mModePosition < 0 || _mModePosition >= [_mDay.modes count]) {
        return;
    }
    
    float timeAxisEndX = self.frame.size.width;
    float timeAxisStartX = 0;
    
    float offsetLeft = _mModePosition == 0 ? 1.5f/2 : 0;
    if (_mModePosition != 0){
        UIView * leftLine = [[UIView alloc] initWithFrame:CGRectMake( timeAxisStartX, [modesStartY[_mModePosition] floatValue] + offsetLeft, ((timeAxisStartX +timeAxisEndX)/2), 1.5f)];
        [leftLine setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:leftLine];
        
    }
    //Linky
    float offsetRight = _mModePosition == [_mDay.modes count] - 1 ? -1.5f / 2 : 0;
    if (_mModePosition != [_mDay.modes count] - 1){
        UIView * rightLine = [[UIView alloc] initWithFrame:CGRectMake(timeAxisStartX,[modesEndY[_mModePosition] floatValue] + offsetRight + [modesStartY[_mModePosition] floatValue], (timeAxisStartX +timeAxisEndX)/2, 1.5f)];
        [rightLine setBackgroundColor:[UIColor whiteColor]];
        [self addSubview:rightLine];
        
    }
    // If not in touch, init rects
    
    TempDayMode *mode =  [_mDay.modes objectAtIndex:_mModePosition];
    
    //pozice hodinek
    float width =  self.frame.size.width/2;
    float height = (((width)/130)*47);
    
    float x = timeAxisStartX + self.frame.size.width/2;
    float y = [modesStartY[_mModePosition] floatValue] - height/2;
    if (watchRects==nil) watchRects = [[NSMutableArray alloc] init];
    if (timeLabels==nil) timeLabels= [[NSMutableArray alloc] init];
    if (_mModePosition != 0){
        [timeFrameImage setFrame:CGRectMake(x, y, width, height)];
        UIImageView * copyOfImage = [[UIImageView alloc] initWithFrame:timeFrameImage.frame];
        [copyOfImage setImage:timeFrameImage.image];
        [watchRects addObject:copyOfImage];
        copyOfImage.tag=101;
        
        
        NSString *beginTime = [self getTimeFromMinute:[[mode startTime] intValue]];
        
        UILabel *beginLabel = [[UILabel alloc] initWithFrame:CGRectMake(x+((width/130)*17), y, width - ((width/130)*17), height)];
        [beginLabel setText:beginTime];
        
        [beginLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:15]];
        [beginLabel setTextColor:[UIColor whiteColor]];
        [beginLabel setTextAlignment:NSTextAlignmentCenter];
        
        [timeLabels addObject:beginLabel];
        
    }
    
    x = timeAxisStartX + self.frame.size.width/2;
    y = [modesEndY[_mModePosition] floatValue]+[modesStartY[_mModePosition] floatValue]  - height/2;
    if (_mModePosition != [_mDay.modes count] - 1){
        [timeFrameImage setFrame:CGRectMake(x, y,  width,  height)];
        UIImageView * copyOfImage = [[UIImageView alloc] initWithFrame:timeFrameImage.frame];
        [copyOfImage setImage:timeFrameImage.image];
        [watchRects addObject:copyOfImage];
        copyOfImage.tag=102;
        
        NSString *endTime = [self getTimeFromMinute:[[mode endTime] intValue]];
        
        UILabel *endLabel = [[UILabel alloc] initWithFrame:CGRectMake(x+((width/130)*17), y, width - ((width/130)*17), height)];
        [endLabel setText:endTime];
        [endLabel setFont:[UIFont fontWithName:@"Roboto-Light" size:15]];
        
        [endLabel setTextColor:[UIColor whiteColor]];
        [endLabel setTextAlignment:NSTextAlignmentCenter];
        
        
        
        
        [timeLabels addObject:endLabel];
        
        
    }
    
    for(UILabel * timeLabel in timeLabels){
        [self addSubview:timeLabel];
    }
    
    
    
    for (UIImageView * image in watchRects){
        [self addSubview:image];
    }
    
    
}



-(NSString*) getTimeFromMinute:(int) minutes {
    int hours = minutes / 60;
    return [NSString stringWithFormat:@"%02d:%02d", hours,minutes - hours * 60];
}

-(void)handlePan:(UIPanGestureRecognizer *)recognizer {
    if(recognizer.state == UIGestureRecognizerStateEnded)
    {
        //Confirm mode changes
        draggedView= nil;
        
        [_mDay redefineModes];
        _mModePosition = [_mDay joinAndDeleteModes:_mModePosition ];
        [self repaintContent];
        
        
    } if (recognizer.state == UIGestureRecognizerStateBegan){
        lastPan = 0;
        CGPoint location = [recognizer locationInView:self];
        //test
        for (UIView * subview in [recognizer.view subviews]){
            if (subview.tag>100 && self.frame.origin.x -10 < location.x
                && subview.frame.origin.x + subview.frame.size.width + 20 > location.x
                && subview.frame.origin.y -10 < location.y
                && subview.frame.origin.y + subview.frame.size.height + 20 > location.y){
                draggedView = (UIImageView*)subview;
                
            }
            
        }
        
    }else if (draggedView!=nil){
        
        [self processTouch:recognizer];
        
    }
    
}

-(void) processTouch:(UIPanGestureRecognizer *)recognizer {
    UIImageView * image = draggedView;
    TempDayMode* mode = [_mDay.modes objectAtIndex:_mModePosition];
    
    float tempStart = [mode.startTime floatValue];
    float tempEnd = [mode.endTime floatValue];
    BOOL isLeftDirection = image.tag==101?YES:NO;
    
    CGPoint translation = [recognizer translationInView:self];
    //recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
    //                                    recognizer.view.center.y + translation.y);
    
    
    
    
    if (isLeftDirection)
        tempStart = [mode.startTime intValue] + (translation.y-lastPan) / WIDTH_TO_TIME;
    else
        tempEnd = [mode.endTime intValue] + (translation.y-lastPan) / WIDTH_TO_TIME;
    
    lastPan=translation.y;
    
    if ((tempEnd - tempStart) * WIDTH_TO_TIME < timeFrameImage.frame.size.height )
        return;
    else {
        mode.startTime = [[NSNumber alloc] initWithInt: tempStart];
        mode.endTime = [[NSNumber alloc] initWithInt: tempEnd];
    }
    
    
    if ([mode.startTime intValue] < 0)
        mode.startTime = 0;
    else if ([mode.endTime intValue] > 1440)
        mode.endTime = [[NSNumber alloc] initWithInt: 1440];
    
    [_mDay recountModes:isLeftDirection modePosition:_mModePosition];
    
    [self repaintContent];
    
}

- (void)areaTapped:(UITapGestureRecognizer*)sender {
    
    if ([_mDay.modes count] == 1)
        return;
    
    if (sender.view.tag>=10){
        _mModePosition = (int)sender.view.tag-10;
        
    }
    [self repaintContent];
    
}

-(void)areaPressed:(UILongPressGestureRecognizer*)sender{
    
    if(sender.state == UIGestureRecognizerStateBegan){
        int modePos = -1;
        if (sender.view.tag>=10){
            modePos = (int)sender.view.tag-10;
        }
        if (modePos==-1)
            return;
        [self.delegate putEditingOnScreen:[_mDay.modes objectAtIndex:modePos] modePos:modePos dayInWeek:(int)_dayInWeek];
    }
}


@end
