//
//  SYManualViewController.m
//  iHC-MIRF
//
//  Created by Vlastimil Venclik on 21.11.13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYManualViewController.h"

@interface SYManualViewController ()

@end

@implementation SYManualViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"iHC-MIRF v2" ofType:@"pdf"];
    NSURL *targetURL = [NSURL fileURLWithPath:path];
    NSURLRequest *request = [NSURLRequest requestWithURL:targetURL];
    self.webView.scalesPageToFit=YES;
    [self.webView loadRequest:request];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_leftBottomButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backTap:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
