//
//  Floorplan.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/21/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface Floorplan : NSObject
@property (nonatomic,copy) NSString * name;
@property (nonatomic,copy) NSString * url;
@property (nonatomic,retain) UIImage * image;
@end
