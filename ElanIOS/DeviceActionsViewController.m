//
//  DeviceActionsViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 07/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "DeviceActionsViewController.h"
#import "DeviceActionItemCell.h"
#import "SecondaryAction.h"
#import "SYCoreDataManager.h"

@interface DeviceActionsViewController ()
@property(nonatomic, strong) NSFetchedResultsController *devices;
@end

@implementation DeviceActionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil actionType:(ActionType)type {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        _type = type;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableViewBackground setTranslatesAutoresizingMaskIntoConstraints:YES];
    [self.tableView setTranslatesAutoresizingMaskIntoConstraints:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _expandedeRows = [NSMutableSet new];
    [self initFetchedResultsControllerWithDeviceID:_device.deviceID];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    _devices = nil;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Init

- (void)initFetchedResultsControllerWithDeviceID:(NSString *)deviceID {
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"State"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"device.label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.device.deviceID == %@", deviceID];
    [fetchRequest setPredicate:predicate];
    _devices = [[NSFetchedResultsController alloc]
                initWithFetchRequest:fetchRequest
                managedObjectContext:context
                sectionNameKeyPath:nil
                cacheName:nil];
    _devices.delegate = self;
    NSError *error;
    
    if (![_devices performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@", [error description]);
    } else if ([[_devices fetchedObjects] count] > 0) {
        _device = ((State *) [[_devices fetchedObjects] objectAtIndex:0]).device;
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if ([_devices.fetchedObjects count] > 0) {
        //        _device = (State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
        //        _actions = [[NSMutableArray alloc] init];
        //        [self getArray];
        [self.tableView reloadData];
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_device == nil || _device.secondaryActions == nil) {
        return 0;
    }
    return [_device.secondaryActions count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //    return [self.actions objectAtIndex:indexPath.row];
    DeviceActionItemCell *cell = (DeviceActionItemCell *) [self getActionCellWithIndexPath:indexPath];
    if (!cell){
         return [[UITableViewCell alloc]  initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath row] == [_device.secondaryActions count]-1) {
        [self changeViewTableSize];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    DeviceActionItemCell *cell = (DeviceActionItemCell *) [self getActionCellWithIndexPath:indexPath];
    //    if ([_expandedeRows containsObject:[NSNumber numberWithInteger:indexPath.row]]) {
    //        return cell.expandedHeight;
    //    } else {
    return cell.height;
    //    }
}


- (id)getActionCellWithIndexPath:(NSIndexPath *)indexPath {
    if (_device == nil || _device.secondaryActions == nil || indexPath.row >= [_device.secondaryActions count]) {
        return nil;
    }
    SecondaryAction *secondaryAction = [_device.secondaryActions objectAtIndex:indexPath.row];
    NSMutableDictionary *bundle = [[NSMutableDictionary alloc] init];
    [bundle setObject:indexPath forKey:@"indexPath"];
    if ([secondaryAction.actions count] == 1) {
        //            NSDictionary * actionInf = [_device.actionsInfo valueForsecondaryAction:secondaryAction];
        Action *action = [secondaryAction.actions anyObject];
        NSString *name = action.name;
        NSString *type = action.type;
        if ([type isKindOfClass:[NSNull class]] || type == nil) {
            NSMutableDictionary *bundle = [[NSMutableDictionary alloc] init];
            [bundle setObject:name forKey:@"label"];
            //            [_actions addObject:[self createCustomCell:kPlain object:bundle]];
            return [self createCustomCell:kPlain object:bundle];
        }
        if ([type isEqualToString:@"int"] && ![name isEqualToString:@"brightness"]) {
            ChangeTime *bright = [[ChangeTime alloc] initWithNSDictionary:action];
            [bundle setObject:name forKey:@"label"];
            [bundle setObject:bright forKey:@"time"];
            [bundle setObject:name forKey:@"time_label"];
            
            if ([name isEqualToString:@"delayed on: set time"]) {
                if ([_device getStateValueForStateName:@"delayed on: set time"] != nil) [bundle setObject:[_device getStateValueForStateName:@"delayed on: set time"] forKey:@"delayedTime"];
            } else if ([name isEqualToString:@"delayed off: set time"]) {
                if ([_device getStateValueForStateName:@"delayed off: set time"] != nil) [bundle setObject:[_device getStateValueForStateName:@"delayed off: set time"] forKey:@"delayedTime"];
            }
            //            [_actions addObject:
            return [self createCustomCell:kTime object:bundle];
        }
        if ([name isEqualToString:@"brightness"]){
            Brightness *bright = [[Brightness alloc] initWithNSDictionary:action];
            [bundle setObject:@"brightness" forKey:@"label"];
            [bundle setObject:bright forKey:@"brightnessSettings"];
            if ([_device getStateValueForStateName:@"brightness"]) {
                [bundle setObject:[_device getStateValueForStateName:@"brightness"] forKey:@"brightnessState"];
                //            [_actions insertObject:
            }else{
                [bundle setObject:[NSNumber numberWithInt:0] forKey:@"brightnessState"];
            }
            return [self createCustomCell:kBrightness object:bundle];
        }
        if ([type isEqualToString:@"bool"]) {
            [bundle setObject:name forKey:@"label"];
            if ([_device getStateValueForStateName:@"on"] != nil)
                [bundle setObject:[_device getStateValueForStateName:@"on"] forKey:@"on"];
            //            [_actions addObject:
            return [self createCustomCell:kOn object:bundle];
        }
    } else if ([secondaryAction.actions count] > 1) {
        Action *a1 = [secondaryAction.actions allObjects][0];
        Action *a2 = [secondaryAction.actions allObjects][1];
        if (a1 == nil || a1.name == nil || a2 == nil || a2.name == nil) {
            return nil;
        }
        if ([a1.name rangeOfString:@"time"].length > 0) {
            Action *tmp = a1;
            a1 = a2;
            a2 = tmp;
        }
        NSString *type1 = a1.type;
        NSString *type2 = a2.type;
        if (type1 == nil && [type2 isEqualToString:@"int"] && ![a2.name isEqualToString:@"brightness"]) {
            ChangeTime *bright = [[ChangeTime alloc] initWithNSDictionary:a2];
            [bundle setObject:a1.name forKey:@"label"];
            [bundle setObject:bright forKey:@"time"];
            [bundle setObject:a2.name forKey:@"time_label"];
            if ([a1.name isEqualToString:@"increase"] || [a1.name isEqualToString:@"delayed on"]) {
                if ([_device getStateValueForStateName:@"delayed on: set time"] != nil) {
                    [bundle setObject:[_device getStateValueForStateName:@"delayed on: set time"] forKey:@"delayedTime"];
                } else if ([_device getStateValueForStateName:@"increase: set time"] != nil) {
                    [bundle setObject:[_device getStateValueForStateName:@"increase: set time"] forKey:@"delayedTime"];
                }
            }
            if ([a1.name isEqualToString:@"decrease"] || [a1.name isEqualToString:@"delayed off"]) {
                if ([_device getStateValueForStateName:@"delayed off: set time"] != nil) {
                    [bundle setObject:[_device getStateValueForStateName:@"delayed off: set time"] forKey:@"delayedTime"];
                } else if ([_device getStateValueForStateName:@"decrease: set time"] != nil) {
                    [bundle setObject:[_device getStateValueForStateName:@"decrease: set time"] forKey:@"delayedTime"];
                }
            }
            //            [_actions addObject:
            return [self createCustomCell:kTimeAndButton object:bundle];
        }
    }
    return nil;
}


- (DeviceActionItemCell *)createCustomCell:(int)type object:(NSMutableDictionary *)bundle {
    
    [bundle setObject:_device.deviceID forKey:@"device"];
    [bundle setObject:self forKey:@"parent"];
    DeviceActionItemCell *cell = [[DeviceActionItemCell alloc] initWithType:type owner:self bundle:bundle];
    
    //    if ([_actions count]>0){
    //        [cell.topBar setImage:nil];
    //    }
    
    return cell;
}

- (void)changeViewTableSize {
    CGFloat tableHeight = 0.0f;
    for (int i = 0; i < [_device.secondaryActions count]; i++) {
        tableHeight += [self tableView:self.tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if (tableHeight + 10 > self.view.frame.size.height) {
            tableHeight = self.view.frame.size.height - 10;
        }//[self tableView:self.tableView heightForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, tableHeight);
    if (tableHeight < self.view.frame.size.height - 10) {
        self.tableView.center = self.view.center;
    } else {
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, 20, self.tableView.frame.size.width, tableHeight);
    }
    self.tableViewBackground.frame = CGRectMake(self.tableViewBackground.frame.origin.x,
                                                self.tableView.frame.origin.y - self.tableView.frame.size.height * 0.025,
                                                self.tableViewBackground.frame.size.width,
                                                self.tableView.frame.size.height * 1.06);
    NSLog(@"size %f", self.tableViewBackground.frame.size.height);
    [_closeButton setFrame:CGRectMake(_closeButton.frame.origin.x, _tableViewBackground.frame.origin.y - _closeButton.frame.size.height / 3, _closeButton.frame.size.width, _closeButton.frame.size.height)];
}

- (IBAction)closeView:(id)sender {
    [self.view removeFromSuperview];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (void)showExpandedItems {
    [self changeViewTableSize];
    [_tableView reloadData];
}

//- (void)getArray {
//    if (_device == nil || _device.secondaryActions == nil)
//        return;
//    NSSet *keys = (NSSet *) _device.secondaryActions;
//    for (SecondaryAction *key in keys) {
//        if ([key.actions count] == 1) {
////            NSDictionary * actionInf = [_device.actionsInfo valueForKey:key];
//
//            Action *action = [key.actions anyObject];
//            NSString *name = action.name;
//            NSString *type = action.type;
//            if ([type isKindOfClass:[NSNull class]] || type == nil) {
//                NSMutableDictionary *bundle = [[NSMutableDictionary alloc] init];
//                [bundle setObject:name forKey:@"label"];
//                [_actions addObject:[self createCustomCell:kPlain object:bundle]];
//                continue;
//            }
//            if ([type isEqualToString:@"int"] && ![name isEqualToString:@"brightness"]) {
//                ChangeTime *bright = [[ChangeTime alloc] initWithNSDictionary:action];
//                NSMutableDictionary *bundle = [[NSMutableDictionary alloc] init];
//                [bundle setObject:name forKey:@"label"];
//                [bundle setObject:bright forKey:@"time"];
//                [bundle setObject:name forKey:@"time_label"];
//
//                if ([name isEqualToString:@"delayed on: set time"]) {
//                    if ([_device getStateValueForStateName:@"delayed on: set time"] != nil) [bundle setObject:[_device getStateValueForStateName:@"delayed on: set time"] forKey:@"delayedTime"];
//                } else if ([name isEqualToString:@"delayed off: set time"]) {
//                    if ([_device getStateValueForStateName:@"delayed off: set time"] != nil) [bundle setObject:[_device getStateValueForStateName:@"delayed off: set time"] forKey:@"delayedTime"];
//                }
//                [_actions addObject:[self createCustomCell:kTime object:bundle]];
//            }
//            if ([name isEqualToString:@"brightness"] && [_device getStateValueForStateName:@"brightness"]) {
//                Brightness *bright = [[Brightness alloc] initWithNSDictionary:action];
//                NSMutableDictionary *bundle = [[NSMutableDictionary alloc] init];
//                [bundle setObject:@"brightness" forKey:@"label"];
//                [bundle setObject:bright forKey:@"brightnessSettings"];
//                [bundle setObject:[_device getStateValueForStateName:@"brightness"] forKey:@"brightnessState"];
//                [_actions insertObject:[self createCustomCell:kBrightness object:bundle] atIndex:0];
//            }
//            if ([type isEqualToString:@"bool"]) {
//                NSMutableDictionary *bundle = [[NSMutableDictionary alloc] init];
//                [bundle setObject:name forKey:@"label"];
//                if ([_device getStateValueForStateName:@"on"] != nil)
//                    [bundle setObject:[_device getStateValueForStateName:@"on"] forKey:@"on"];
//                [_actions addObject:[self createCustomCell:kOn object:bundle]];
//            }
//        } else if ([key.actions count] > 1) {
//            Action *a1 = [key.actions allObjects][0];
//            Action *a2 = [key.actions allObjects][1];
//            if (a1 == nil || a1.name == nil || a2 == nil || a2.name == nil) {
//                return;
//            }
//            if ([a1.name rangeOfString:@"time"].length > 0) {
//                Action *tmp = a1;
//                a1 = a2;
//                a2 = tmp;
//            }
//            NSString *type1 = a1.type;
//            NSString *type2 = a2.type;
//            if (type1 == nil && [type2 isEqualToString:@"int"] && ![a2.name isEqualToString:@"brightness"]) {
//                ChangeTime *bright = [[ChangeTime alloc] initWithNSDictionary:a2];
//                NSMutableDictionary *bundle = [[NSMutableDictionary alloc] init];
//                [bundle setObject:a1.name forKey:@"label"];
//                [bundle setObject:bright forKey:@"time"];
//                [bundle setObject:a2.name forKey:@"time_label"];
//                if ([a1.name isEqualToString:@"increase"] || [a1.name isEqualToString:@"delayed on"]) {
//                    if ([_device getStateValueForStateName:@"delayed on: set time"] != nil) [bundle setObject:[_device getStateValueForStateName:@"delayed on: set time"] forKey:@"delayedTime"];
//                }
//                if ([a1.name isEqualToString:@"decrease"] || [a1.name isEqualToString:@"delayed off"]) if ([_device getStateValueForStateName:@"delayed off: set time"] != nil) [bundle setObject:[_device getStateValueForStateName:@"delayed off: set time"] forKey:@"delayedTime"];
//                [_actions addObject:[self createCustomCell:kTimeAndButton object:bundle]];
//
//
//            }
//
//        }
//
//    }
//
//}


@end
