//
//  IRDevice+Dictionary.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 12.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "IRDevice+Dictionary.h"
#import "SYCoreDataManager.h"
#import "Action.h"
#import "Device+Dictionary.h"
#import "ResourceTiles.h"
#import "IRAction.h"
#import "SecondaryAction.h"

@implementation IRDevice (Dictionary)
- (BOOL)updateWithDictionary:(NSDictionary *)dict {
    BOOL updated = NO;
    
    //Parsing basic information
    NSString* parsedID = [dict objectForKey:@"id"];
    
    NSDictionary* deviceInfo = [dict objectForKey:@"device info"];
    NSString* parsedProductType = [deviceInfo objectForKey:@"product type"];
    NSString* parsedType = [deviceInfo objectForKey:@"type"];
    NSString* parsedLabel = [deviceInfo objectForKey:@"label"];
    NSNumber* parsedAddress = [deviceInfo objectForKey:@"address"];
    
    
    //    if (parsedLabel ==nil) return false;
    
    //If info is same for all attributes, skip updating
    if (!([self.deviceID isEqualToString:parsedID] &&
          [self.productType isEqualToString:parsedProductType] &&
          [self.type isEqualToString:parsedType] &&
          [self.label isEqualToString:parsedLabel] &&
          [self.address isEqual:parsedAddress])) {
        
        self.deviceID = parsedID;
        self.productType = parsedProductType;
        self.type = parsedType;
        self.label = parsedLabel;
        self.address = parsedAddress;
        
        
        updated = YES;
    }
    [self determineIconPrefix];
    updated = ([self parseActions:[dict objectForKey:@"actions info"] withSecondaryActions:[dict objectForKey:@"secondary actions"]] || updated);
    return updated;
}

#pragma mark - Helper methods

- (BOOL)parseActions:(NSDictionary*)anActionsInfo withSecondaryActions:(NSArray*)aSecondaryActions {
    BOOL updated = NO;
    
    //Parsing actions info from dictionary
    for (NSString* actionName in [anActionsInfo allKeys]) {
        IRAction* action = [self irActionWithName:actionName];
        if (action == nil) {
            //Context issue fix
            action = [[SYCoreDataManager sharedInstance] createEntityIRActionInContext:self.managedObjectContext];
            updated = YES;
        }
        
        updated = ([action updateWithDictionary:[anActionsInfo objectForKey:actionName] andName:actionName]||updated);
        
        self.led = action.irLed;
        
        if (self.led == nil || self.led == 0){
            self.led = [[NSUserDefaults standardUserDefaults] objectForKey:@"irDeviceLed"];
        }
        
        BOOL isSecondary = NO;
        
        //Assigning secondary actions
        for (id secondaryActionObject in aSecondaryActions) {
            if ([secondaryActionObject isKindOfClass:[NSString class]]) {
                if ([secondaryActionObject isEqualToString:actionName]) isSecondary = YES;
            } else if ([secondaryActionObject isKindOfClass:[NSArray class]]) {
                for (NSString* secActionName in secondaryActionObject) {
                    if ([secActionName isEqualToString:actionName]) isSecondary = YES;
                }
            }
        }

        //        if (action && ![self.secondaryActions containsObject:action] && isSecondary) {
        //            #warning Secondary actions not currently used
        ////            [self addSecondaryActionsObject:action];
        //            updated = YES;
        //        }
        
        if (action && ![self.actions containsObject:action] ) {
            [self addActionsObject: action];
            updated = YES;
        }
        [[SYCoreDataManager sharedInstance] saveContext];
    }
    
    //Assigning secondary actions
    for (id secondaryActionObject in aSecondaryActions) {
        NSPredicate* predicate;
        
        if ([secondaryActionObject isKindOfClass:[NSString class]]) {
            predicate = [NSPredicate predicateWithFormat:@"(SUBQUERY(actions,$action,$action.name == %@).@count == 1)",(NSString*)secondaryActionObject];
        } else if ([secondaryActionObject isKindOfClass:[NSArray class]]) {
            predicate = [NSPredicate predicateWithFormat:@"(SUBQUERY(actions,$action,$action.name == %@).@count == 1) AND (SUBQUERY(actions,$action,$action.name == %@).@count == 1)",[secondaryActionObject objectAtIndex:0],[secondaryActionObject objectAtIndex:1]];
        }
        
        if (self.secondaryActions !=nil  && [self.secondaryActions count]>0 && predicate != nil){
            NSOrderedSet* filteredSecondaryActions = [self.secondaryActions filteredOrderedSetUsingPredicate:predicate];
            if ([filteredSecondaryActions count] < 1) {
                [self createSecondaryAction:secondaryActionObject];
                updated = YES;
            }
        } else {
            [self createSecondaryAction:secondaryActionObject];
            updated = YES;
        }
    }
    return updated;
}

- (void) createSecondaryAction:(id)secondaryActionObject {
    SecondaryAction* secondaryAction = (SecondaryAction*)[[SYCoreDataManager sharedInstance] createEntitySecondaryAction];
    //[secondaryAction setOnDevice:self];
    [self addSecondaryActionsObject:secondaryAction];
    
    if ([secondaryActionObject isKindOfClass:[NSString class]]) {
        IRAction * action = [[SYCoreDataManager sharedInstance] getIRActionWithName:(NSString*)secondaryActionObject withDeviceID:self.deviceID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        if(action != nil)
            [secondaryAction addActionsObject:action];
    } else if ([secondaryActionObject isKindOfClass:[NSArray class]]) {
        for (NSString* actionName in secondaryActionObject) {
            IRAction * action = [[SYCoreDataManager sharedInstance] getIRActionWithName:actionName withDeviceID:self.deviceID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
            if(action != nil)
                [secondaryAction addActionsObject:action];
            
        }
    }
    [[SYCoreDataManager sharedInstance] saveContext];
    
}

- (IRAction*)irActionWithName:(NSString *)actionName {
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"name MATCHES %@",actionName];
    
    NSSet* filteredActions = [self.actions filteredSetUsingPredicate:predicate];
    
    if ([filteredActions count] > 0) {
        return [filteredActions anyObject];
    } else {
        return nil;
    }
}


-(void)determineIconPrefix{
    self.iconPrefix = nil;
    //    if ([self.type isEqualToString:@"heating"] ||
    //        [self.type isEqualToString:@"thermometer"] ||
    //        [self.type isEqualToString:@"temperature regulation area"]){
    //        if (self.deviceHeating !=nil) {
    //            self.iconPrefix = @"teplomer_heating";
    //        } else if (self.deviceCooling!=nil){
    //            self.iconPrefix = @"teplomer_cooling";
    //        } else {
    //            self.iconPrefix =@"teplomer";
    //        }
    //    }
}


@end
