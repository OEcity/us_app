//
//  DismissSegue.h
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 10/06/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DismissSegue : UIStoryboardSegue

@end
