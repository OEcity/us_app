//
//  AddDeviceViewController.h
//  Click Smart
//
//  Created by Tom Odler on 22.07.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
#import "HUDWrapper.h"

@interface AddDeviceViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, NSFetchedResultsControllerDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) HUDWrapper *loaderDialog;
@property (nonatomic, strong) NSFetchedResultsController* devices;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property(nonatomic, retain) Device*device;


@end
