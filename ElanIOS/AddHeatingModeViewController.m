//
//  AddHeatingModeViewController.m
//  US App
//
//  Created by admin on 13.07.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "AddHeatingModeViewController.h"


@interface AddHeatingModeViewController ()

@end

@implementation AddHeatingModeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_dayInWeekPickerView setHidden:YES];
    [_timePickerView setHidden:YES];
    
    //initialize settings Bool
    _selectedDay = NO;
    _selectedFrom = NO;
    _selectedTill = NO;
    _selectedMode = NO;
    
    //initialize day picker
    _daysInWeekForPicker = @[@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", @"Sunday"];
    _dayInWeekPicker.dataSource = self;
    _dayInWeekPicker.delegate = self;
    
    //initialize time picker
    _timePicker.dataSource = self;
    _timePicker.delegate = self;
    
    _odCausedOpen = NO;
    
    _formatter = [[NSNumberFormatter alloc] init];
    
    [_formatter setNumberStyle:NSNumberFormatterNoStyle];
    [_formatter setMaximumIntegerDigits:2];
    [_formatter setMinimumIntegerDigits:2];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    }

- (IBAction)saveButtonPressed:(id)sender {
    if(_selectedDay && _selectedMode && _selectedTill && _selectedFrom){
        if (_timeTo > _timeFrom){
            [self prepareAndSendDataToDelegate];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"To time is bigger than from time." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else{
        //warning terrible English
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Values was not set properly." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    
    
   
}
-(void)prepareAndSendDataToDelegate{
    
    TempDayMode *newMode = [[TempDayMode alloc] init];
    
    newMode.mode = [[NSNumber alloc] initWithInteger:_mode];
    newMode.startTime = [[NSNumber alloc] initWithInteger:_timeFrom];
    newMode.endTime = [[NSNumber alloc] initWithInteger:_timeTo];
    newMode.duration = [[NSNumber alloc] initWithInteger:_timeTo-_timeFrom];
    if (_delegate!=nil)
    [_delegate addHeatingModeToday:_day comesFromLTouch:NO tempDayMode:newMode position:(int)_modePos erase:NO];
    
}

- (IBAction)odButtonTouched:(id)sender {
    [_timePickerView setHidden:NO];
    [_timePicker selectRow:_timeFrom/60 inComponent:0 animated:NO];
    [_timePicker selectRow:_timeFrom % 60 inComponent:1 animated:NO];
    _odCausedOpen = YES;
    
}
- (IBAction)doButtonTouched:(id)sender {
    [_timePickerView setHidden:NO];
    [_timePicker selectRow:_timeTo/60 inComponent:0 animated:NO];
    [_timePicker selectRow:_timeTo % 60 inComponent:1 animated:NO];
    _odCausedOpen = NO;
    
}
- (IBAction)dayInWeekChoosed:(id)sender {
    [_dayInWeekPickerView setHidden:YES];
    _day = [_dayInWeekPicker selectedRowInComponent:0];
    [_dayLabel setText:[_daysInWeekForPicker objectAtIndex:_day]];
    _selectedDay = YES;

}
- (IBAction)timeChoosed:(id)sender {
    [_timePickerView setHidden:YES];
    
    if (_odCausedOpen){
        NSNumber *help = [[NSNumber alloc] initWithInteger:[_timePicker selectedRowInComponent:0]];
        NSMutableString *stringToShow = [[NSMutableString alloc] initWithString:@"From "];
        [stringToShow appendString:[_formatter stringFromNumber:help]];
        [stringToShow appendString:@":"];
        NSNumber *help2 = [[NSNumber alloc] initWithInteger:[_timePicker selectedRowInComponent:1]];
        [stringToShow appendString:[_formatter stringFromNumber:help2]];
        
        [_odLabel setText: stringToShow];
        _timeFrom = ([_timePicker selectedRowInComponent:0]*60) + [_timePicker selectedRowInComponent:1];
        _selectedFrom = YES;
        
    }
    else{
        
        NSNumber *help = [[NSNumber alloc] initWithInteger:[_timePicker selectedRowInComponent:0]];
        NSMutableString *stringToShow = [[NSMutableString alloc] initWithString:@"Till "];
        [stringToShow appendString:[_formatter stringFromNumber:help]];
        [stringToShow appendString:@":"];
        NSNumber *help2 = [[NSNumber alloc] initWithInteger:[_timePicker selectedRowInComponent:1]];
        [stringToShow appendString:[_formatter stringFromNumber:help2]];
        
        [_doLabel setText: stringToShow];
        _timeTo = ([_timePicker selectedRowInComponent:0]*60) + [_timePicker selectedRowInComponent:1];
        if (_timeTo == 0){
            _timeTo = 1440;
        }
        _selectedTill = YES;
        
        
    }
    _odCausedOpen = NO;
    
}
- (IBAction)denButtonPressed:(id)sender {
    [_dayInWeekPickerView setHidden:NO];
}




#pragma mark day in week picker data source and delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView == _timePicker){
        return 2;
    }
    else return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == _timePicker){
        if (component == 1){
            return 60;
        }
        else return 24;
    }
    else return _daysInWeekForPicker.count;
    
    
}


-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (pickerView == _timePicker){
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width/2-25, 44)];
        label.backgroundColor = [UIColor blackColor];
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont fontWithName:@"Roboto-Light" size:18];
        if (component == 0){
            label.textAlignment = NSTextAlignmentRight;
            label.text = [NSString stringWithFormat:@"%li",(long)(row)];
            
        }
        else {
            label.textAlignment = NSTextAlignmentLeft;
            label.text = [_formatter stringFromNumber:[NSNumber numberWithInteger:row]];
        }
        
        
        
        return label;

    }
    else{
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 44)];
        label.backgroundColor = [UIColor blackColor];
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont fontWithName:@"Roboto-Light" size:18];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = _daysInWeekForPicker[row];
        return label;
    }
}

#pragma mark table view delegate, data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    AddModeTemperatureDetailTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"temperatureDetail" forIndexPath:indexPath];
    switch (indexPath.row){
        case 0:
            [cell.label setText:@"Minimum"];
            [cell.temperatureLabel setTextColor:[UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
            [cell.temperatureLabel setHighlightedTextColor:[UIColor whiteColor]];
            break;
        case 1:
            [cell.label setText:@"Attenutation"];
            [cell.temperatureLabel setTextColor:[UIColor colorWithRed:147.0f/255.0f green:162.0f/255.0f blue:108.0f/255.0f alpha:1.0f]];
            break;
        case 2:
            [cell.label setText:@"Normal"];
            [cell.temperatureLabel setTextColor:[UIColor colorWithRed:245.0f/255.0f green:134.0f/255.0f blue:60.0f/255.0f alpha:1.0f]];
            break;
        case 3:
            [cell.label setText:@"Comfort"];
            [cell.temperatureLabel setTextColor:[UIColor colorWithRed:248.0f/255.0f green:45.0f/255.0f blue:38.0f/255.0f alpha:1.0f]];
            break;
    }

    [cell.temperatureLabel setText:[_temperatures objectAtIndex:indexPath.row]];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    [cell setSelectedBackgroundView:bgColorView];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectedMode = true;
    _mode = indexPath.row + 1;
    
}

/*
#pragma mark tap gesture

-(void)modeTap:(NSInteger)mode{
    switch (mode) {
        case 1:
            [self minimumTap:nil];
            break;
        case 2:
            [self utlumTap:nil];
            break;
        case 3:
            [self normalTap:nil];
            break;
        case 4:
            [self komfortTap:nil];
            break;
        default:
            break;
    }
}
 
 */
/*
- (IBAction)minimumTap:(id)sender {
    [self allToBlack];
    [_minView setBackgroundColor:[UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
    [_minTempLabel setTextColor:[UIColor whiteColor]];
    _mode=1;
    _selectedMode = YES;
    
}
- (IBAction)utlumTap:(id)sender {
    [self allToBlack];
    [_utlView setBackgroundColor:[UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
    _mode=2;
    _selectedMode = YES;
    
    
}
- (IBAction)normalTap:(id)sender {
    [self allToBlack];
    [_norView setBackgroundColor:[UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
    _mode=3;
    _selectedMode = YES;
    
}
- (IBAction)komfortTap:(id)sender {
    [self allToBlack];
    [_komView setBackgroundColor:[UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
    _mode=4;
    _selectedMode = YES;
    
}

-(void)allToBlack{
    for (UIView *view in _modesView){
        [view setBackgroundColor: [UIColor blackColor]];
    }
    [_minTempLabel setTextColor:[UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
}

*/


@end
