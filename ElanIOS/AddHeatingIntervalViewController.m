//
//  AddHeatingIntervalViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 1/26/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "AddHeatingIntervalViewController.h"
#define MINIMUM 1
#define ATTENUATION 2
#define NORMAL 3
#define COMFORT 4

@interface AddHeatingIntervalViewController ()
{
    NSTimer *_timer;
}

@end

@implementation AddHeatingIntervalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _timerStep=1;
    _timerMax=60*24;
    _timerMin=0;
    _mode = 1;
    _eraseButton.hidden=YES;
    if (_mMode){
        _eraseButton.hidden=NO;
        _mode= [_mMode.mode intValue];
        _timeFrom = [_mMode.startTime intValue];
        _timeTo = [_mMode.endTime intValue];
        if (_timeFrom==0 && _timeTo==24*60){
            _eraseButton.hidden=YES;
            
            for (UIButton * button in _timeSetButtons){
                button.userInteractionEnabled=NO;
            }
        }
        [_secondsLabel setText:[NSString stringWithFormat:@"%02ld", (long)_timeFrom % 60 ]];
        [_minutesLabel setText:[NSString stringWithFormat:@"%02ld", (long)(_timeFrom / 60)]];
        [_secondsToLabel setText:[NSString stringWithFormat:@"%02ld", (long)_timeTo % 60 ]];
        [_minutesToLabel setText:[NSString stringWithFormat:@"%02ld", (long)(_timeTo / 60)]];
        for (UIButton* button in _heatModeCollection){
            [button setImage:[UIImage imageNamed:@"kolecko_sede_narail"] forState:UIControlStateNormal];
            
            if (button.tag == _mode){
                switch (button.tag) {
                    case MINIMUM:
                        [button setImage:[UIImage imageNamed:@"minimum"] forState:UIControlStateNormal];
                        break;
                    case ATTENUATION:
                        [button setImage:[UIImage imageNamed:@"attenuation_"] forState:UIControlStateNormal];
                        
                        
                        break;
                    case NORMAL:
                        [button setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
                        
                        
                        break;
                    case COMFORT:
                        [button setImage:[UIImage imageNamed:@"heat"] forState:UIControlStateNormal];
                        
                        
                        break;
                    default:
                        break;
                }
                
            }
            
        }
        
    }
    [_lWindowStateTitle setText:NSLocalizedString(@"heat_valve_dialog_window_state", nil)];
    [_lHeatModeTitle setText:NSLocalizedString(@"heat_dialog_heat_mode_title", nil)];
    [_lToTitle setText:NSLocalizedString(@"temp_schedule_dialog_to", nil)];
    [_lMinimumTitle setText:NSLocalizedString(@"heat_mode_minimum", nil)];
    [_lAttenuationTitle setText:NSLocalizedString(@"heat_mode_attenuation", nil)];
    [_lNormalTitle setText:NSLocalizedString(@"heat_mode_normal", nil)];
    [_lComfortTitle setText:NSLocalizedString(@"heat_mode_comfort", nil)];
    
    [_lBackTitle setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_lSaveTitle setTitle:NSLocalizedString(@"temp_schedule_dialog_save", nil) forState:UIControlStateNormal];
    [_lEraseTitle setTitle:NSLocalizedString(@"temp_schedule_dialog_erase", nil) forState:UIControlStateNormal];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
    //
    for(UIButton *button in _timeSetButtons){
        UILongPressGestureRecognizer *lpgr; lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self
                                                                                                 action:@selector(handleLongPress:)];
        
        lpgr.minimumPressDuration = 0.5; //seconds
        lpgr.delegate = self;
        [button addGestureRecognizer:lpgr];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewDidLayoutSubviews{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
    CGFloat screenHeight = screenRect.size.height;
    [_scrollView setContentSize:CGSizeMake(screenHeight, 500)];
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer{
    UIButton * button = (UIButton *)gestureRecognizer.view;
    NSLog(@"gesture %lu",(unsigned long)[button state]);
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        NSLog(@"start");
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.2f
                                                  target:self
                                                selector:@selector(_timerFired:)
                                                userInfo:gestureRecognizer.view
                                                 repeats:YES];
        [button setSelected:YES];
    }else if (gestureRecognizer.state == UIGestureRecognizerStateEnded ){
        NSLog(@"stop");
        if (_timer != nil)
        {
            [_timer invalidate];
            _timer = nil;
        }
        [button setSelected:NO];
    }
    
}


- (void)_timerFired:(NSTimer*)info
{
    id sender = [info userInfo];
    if ([_bFromTimeHourUp isEqual:sender] || [_bFromTimeHourDown isEqual:sender] || [_bFromTimeMinuteUp isEqual:sender] || [_bFromTimeMinuteDown isEqual:sender]){
        [self changeTime:sender];
    } else if ([_bToTimeHourUp isEqual:sender] || [_bToTimeHourDown isEqual:sender] || [_bToTimeMinuteUp isEqual:sender] || [_bToTimeMinuteDown isEqual:sender]){
        [self changeToTime:sender];
    }
}


- (IBAction)backButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskLandscape;
    
}

#pragma mark From Timer cell settings
- (IBAction)changeTime:(id)sender {
    UIButton * button = (UIButton *)sender;
    NSInteger secondsValue = [_secondsLabel.text intValue];
    NSInteger minutesValue = [_minutesLabel.text intValue];
    NSInteger hoursValue = [_hoursLabel.text intValue];
    switch (button.tag) {
        case 1:
            //seconds up
            
            if (secondsValue + _timerStep+ hoursValue*3600 + minutesValue*60 <= _timerMax){
                secondsValue+=_timerStep;
                if ((int)(secondsValue/60) == 1) {
                    secondsValue = secondsValue % 60;
                    minutesValue+=1;
                }
            }
            
            break;
        case 2:
            // senconds down
            
            if (secondsValue - _timerStep + hoursValue*3600 + minutesValue*60 >= _timerMin){
                secondsValue-=_timerStep;
                if (secondsValue<0){
                    
                    if (_timerMax<60) {
                        secondsValue = _timerMax;
                    }else{
                        secondsValue = 59;
                    }
                    if (minutesValue>0) minutesValue-=1;
                    
                }
                
            }
            break;
        case 3:
            // minutes up
            
            if ((minutesValue*60 + _timerStep*60)+secondsValue + hoursValue*3600 <= _timerMax){
                minutesValue+=_timerStep;
                if (secondsValue==_timerMin){
                    secondsValue = 0;
                }
                if ((int)(minutesValue/60) == 1){
                    minutesValue = minutesValue % 60;
                    hoursValue+=1;
                }
                
            }
            break;
        case 4:
            // minutes down
            
            if ((minutesValue*60 - _timerStep*60) + secondsValue + hoursValue*3600 >= _timerMin){
                minutesValue-=_timerStep;
                if (hoursValue*3600+minutesValue*60+secondsValue<_timerMin){
                    secondsValue = _timerMin % 60;
                }
                if (minutesValue<0){
                    if (_timerMax<3600) {
                        minutesValue = _timerMax/60;
                    }else{
                        minutesValue = 59;
                    }
                    if (hoursValue>0) hoursValue-=1;
                }
                
            }else if ((minutesValue*60 - _timerStep*60) + secondsValue + hoursValue*3600 >= 0 && (minutesValue*60 - _timerStep*60) + secondsValue + hoursValue*3600 < _timerMin){
                if (_timerMin < 60){
                    minutesValue-=_timerStep;
                    secondsValue = _timerMin;
                }
            }
            
            break;
        default:
            break;
    }
    
    [_secondsLabel setText:[NSString stringWithFormat:@"%02ld", (long)secondsValue]];
    [_minutesLabel setText:[NSString stringWithFormat:@"%02ld", (long)minutesValue]];
    
    if (_timerMax<=60){
        [_minutesLabel setEnabled:NO];
        [_minutesLabel setText:@"--"];
    }
    _timeFrom =hoursValue*3600+minutesValue*60+secondsValue;
    
    
}


#pragma mark From Timer cell settings
- (IBAction)changeToTime:(id)sender {
    UIButton * button = (UIButton *)sender;
    NSInteger secondsValue = [_secondsToLabel.text intValue];
    NSInteger minutesValue = [_minutesToLabel.text intValue];
    NSInteger hoursValue = [_hoursLabel.text intValue];
    switch (button.tag) {
        case 1:
            //seconds up
            
            if (secondsValue + _timerStep+ hoursValue*3600 + minutesValue*60 <= _timerMax){
                secondsValue+=_timerStep;
                if ((int)(secondsValue/60) == 1) {
                    secondsValue = secondsValue % 60;
                    minutesValue+=1;
                }
            }
            
            break;
        case 2:
            // senconds down
            
            if (secondsValue - _timerStep + hoursValue*3600 + minutesValue*60 >= _timerMin){
                secondsValue-=_timerStep;
                if (secondsValue<0){
                    
                    if (_timerMax<60) {
                        secondsValue = _timerMax;
                    }else{
                        secondsValue = 59;
                    }
                    if (minutesValue>0) minutesValue-=1;
                    
                }
                
            }
            break;
        case 3:
            // minutes up
            
            if ((minutesValue*60 + _timerStep*60)+secondsValue + hoursValue*3600 <= _timerMax){
                minutesValue+=_timerStep;
                if (secondsValue==_timerMin){
                    secondsValue = 0;
                }
                if ((int)(minutesValue/60) == 1){
                    minutesValue = minutesValue % 60;
                    hoursValue+=1;
                }
                
            }
            break;
        case 4:
            // minutes down
            
            if ((minutesValue*60 - _timerStep*60) + secondsValue + hoursValue*3600 >= _timerMin){
                minutesValue-=_timerStep;
                if (hoursValue*3600+minutesValue*60+secondsValue<_timerMin){
                    secondsValue = _timerMin % 60;
                }
                if (minutesValue<0){
                    if (_timerMax<3600) {
                        minutesValue = _timerMax/60;
                    }else{
                        minutesValue = 59;
                    }
                    if (hoursValue>0) hoursValue-=1;
                }
                
            }else if ((minutesValue*60 - _timerStep*60) + secondsValue + hoursValue*3600 >= 0 && (minutesValue*60 - _timerStep*60) + secondsValue + hoursValue*3600 < _timerMin){
                if (_timerMin < 60){
                    minutesValue-=_timerStep;
                    secondsValue = _timerMin;
                }
            }
            
            break;
        default:
            break;
    }
    
    [_secondsToLabel setText:[NSString stringWithFormat:@"%02ld", (long)secondsValue]];
    [_minutesToLabel setText:[NSString stringWithFormat:@"%02ld", (long)minutesValue]];
    
    if (_timerMax<=60){
        [_minutesToLabel setEnabled:NO];
        [_minutesToLabel setText:@"--"];
    }
    _timeTo =hoursValue*3600+minutesValue*60+secondsValue;
    
    
}


- (IBAction)heatModeChange:(id)sender {
    UIButton * button =(UIButton*)sender;
    
    for (UIButton* button in _heatModeCollection){
        [button setImage:[UIImage imageNamed:@"kolecko_sede_narail"] forState:UIControlStateNormal];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
        
    }
    _mode=button.tag;
    switch (button.tag) {
        case MINIMUM:
            [button setImage:[UIImage imageNamed:@"minimum"] forState:UIControlStateNormal];
            break;
        case ATTENUATION:
            [button setImage:[UIImage imageNamed:@"attenuation_"] forState:UIControlStateNormal];
            
            
            break;
        case NORMAL:
            [button setImage:[UIImage imageNamed:@"normal"] forState:UIControlStateNormal];
            
            
            break;
        case COMFORT:
            [button setImage:[UIImage imageNamed:@"heat"] forState:UIControlStateNormal];
            
            
            break;
        default:
            break;
    }
    
}


- (IBAction)saveSchedule:(id)sender {
    if(_timeTo <= _timeFrom)
        [self addSplitInterval];
    else
        [self addSingleInterval];
    
    
}

-(void) addSingleInterval{
    _mMode = [[TempDayMode alloc] init];
    _mMode.mode = [[NSNumber alloc] initWithInteger:_mode];
    _mMode.startTime = [[NSNumber alloc] initWithInteger:_timeFrom];
    _mMode.endTime = [[NSNumber alloc] initWithInteger:_timeTo];
    _mMode.duration = [[NSNumber alloc] initWithInteger:_timeTo-_timeFrom];
    if (_delegate!=nil)
        [_delegate addHeatingModeToday:YES tempDayMode:_mMode position:(int)_modePos erase:NO];
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void) addSplitInterval{
    _mMode = [[TempDayMode alloc] init];
    _mMode.mode = [[NSNumber alloc] initWithInteger:_mode];
    _mMode.startTime = [[NSNumber alloc] initWithInteger:_timeFrom];
    _mMode.endTime = [[NSNumber alloc] initWithInteger:1440];
    _mMode.duration = [[NSNumber alloc] initWithInteger:1440 - _timeFrom];
    
    
    //if(mListener != null)
    //   mListener.onDialogEnd(OnIntervalDialogEndListener.IntervalDialogType.addCurrentDaySchedule, mMode, mModePosition);
    if (_delegate!=nil)
        [_delegate addHeatingModeToday:YES tempDayMode:_mMode position:(int)_modePos erase:NO];
    
    
    _mMode = [[TempDayMode alloc] init];
    _mMode.mode = [[NSNumber alloc] initWithInteger:_mode];
    _mMode.startTime = [[NSNumber alloc] initWithInteger:0];
    _mMode.endTime = [[NSNumber alloc] initWithInteger:_timeTo];
    _mMode.duration = [[NSNumber alloc] initWithInteger:_timeTo];
    
    //Dont add zero duration interval
    if (_delegate!=nil && [_mMode.duration intValue]!=0)
        [_delegate addHeatingModeToday:NO tempDayMode:_mMode position:-1 erase:NO];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)eraseButtonTap:(id)sender {
    [_delegate addHeatingModeToday:NO tempDayMode:_mMode position:(int)_modePos erase:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


@end
