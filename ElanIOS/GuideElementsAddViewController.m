//
//  GuideElementsAddViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 30.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "GuideElementsAddViewController.h"
#import "SYCoreDataManager.h"
#import "ResourceTiles.h"
#import "Constants.h"
#import "SYAPIManager.h"

@interface GuideElementsAddViewController (){
    NSArray *types;
    NSMutableArray *productTypes;
    
    NSString *myDeviceName;
    NSString *myAddress;
    NSString *myProductType;
    NSString *myType;
    
    Room *selectedRoom;
    
    Elan*selectedElan;
    
    BOOL eLANsSelected;
    BOOL roomsSelected;
    BOOL roomWasSelected;
    BOOL requestSent;
}
@property (weak, nonatomic) IBOutlet UIView *pickerViewContainer;

@end

@implementation GuideElementsAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    
    NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"typesList" ofType:@"plist"]];
    
    types = [dict objectForKey:@"rfDeviceTypes"];

    productTypes = [[dict objectForKey:@"productTypes"] mutableCopy];
    
    
    
    [self initFetchedResultsController];
    
    eLANsSelected = roomsSelected= roomWasSelected = NO;
    
    _pickerViewContainer.hidden = YES;
    
    requestSent = NO;
    
    if(_device != nil){
        myDeviceName = _device.label;
        myAddress = [NSString stringWithFormat:@"%06X", (unsigned int)self.device.address.intValue];
        myProductType = _device.productType;
        myType = _device.type;
        selectedElan = _device.elan;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedNotification)
                                                 name:LOADING_DEVICES_ENDED
                                               object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = NO;
    
    [self.view layoutIfNeeded];
}

#pragma mark textField Delegate
-(void)textFieldDidEndEditing:(UITextField *)textField{
    if(textField.tag == 3){
        myAddress = textField.text;
    } else {
        myDeviceName = textField.text;
    }
   
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return NO;
}

#pragma mark - UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"Tableview tag:%ld", (long)tableView.tag);
    if(tableView.tag == 6 && roomsSelected)return [[_rooms fetchedObjects] count]+1;
    if(tableView.tag == 2 && eLANsSelected)return [[SYCoreDataManager sharedInstance] getAllElans].count +1;
    if(tableView.tag == 1)return 6;
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1 && indexPath.row == 1 && eLANsSelected) return [[SYCoreDataManager sharedInstance] getAllElans].count*50+50;
    if(tableView.tag == 1 && indexPath.row == 5 && roomsSelected) return [[_rooms fetchedObjects]count]*50+50;
    return 50;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        UITableViewCell * cell = nil;
        
        switch (indexPath.row) {
            case 0:{
                NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"enterName", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField *tf = (UITextField*)[[[cell contentView] subviews] firstObject];
                tf.delegate = self;
                tf.attributedPlaceholder = namePlaceHolder;
                if(myDeviceName != nil){
                    tf.text = myDeviceName;
                }
            }
                break;
            case 1:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.delegate = self;
                tbv.dataSource = self;
            }
                break;
            case 2:{
                NSAttributedString *addressPlaceHolder = [[NSAttributedString alloc] initWithString:@"Address" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField *tf = (UITextField*)[[[cell contentView] subviews] firstObject];
                tf.tag = 3;
                tf.delegate = self;
                tf.attributedPlaceholder = addressPlaceHolder;
                if(myAddress != nil){
                    tf.text = myAddress;
                }
            }
                break;
            case 3:{
                for(UIView*view in [[cell contentView] subviews]){
                    if(view.tag == 50){
                        [view removeFromSuperview];
                    }
                }
                
                cell = [tableView dequeueReusableCellWithIdentifier:@"productTypeCell" forIndexPath:indexPath];
                UILabel *tf = (UILabel*)[[cell contentView] viewWithTag:1];
                if(myProductType != nil){
                    tf.text = myProductType;
                } else {
                    tf.text = @"Type";
                }
            }
                break;
            case 4:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"iconsCell"forIndexPath:indexPath];
                
                UICollectionView *collection = nil;
                for(UIView *view in [[cell contentView] subviews]){
                    if ([view isKindOfClass:[UICollectionView class]]){
                        collection = (UICollectionView*)view;
                    }
                }
                if(collection){
                    collection.delegate = self;
                    collection.dataSource = self;
                }
                
            }
                break;
            case 5:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"roomsCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.delegate = self;
                tbv.dataSource = self;
            }
                break;
            default:
                break;
        }
        
        return cell;
    }else if(tableView.tag == 2){
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        
        if(eLANsSelected){
            line.hidden = YES;
            
            if(indexPath.row>0){
                img.hidden = YES;
                label.text = ((Elan*)[[[SYCoreDataManager sharedInstance] getAllElans] objectAtIndex:indexPath.row-1]).label;
//                label.text = [[SYCoreDataManager sharedInstance] getCurrentElan].label;
            } else {
                label.text = @"eLAN";
                img.hidden = NO;
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            }
        } else {
            label.text = @"eLAN";
            line.hidden = NO;
            img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
        }
        return cell;
    } else {
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        
        if(roomsSelected){
            line.hidden = YES;
            
            if(indexPath.row>0){
                NSIndexPath *myPath = [NSIndexPath indexPathForRow:indexPath.row -1 inSection:indexPath.section];
                Room *room = [_rooms objectAtIndexPath:myPath];
                label.text = room.label;
                img.hidden = YES;
            } else {
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
                label.text = @"Rooms";
                img.hidden = NO;
            }
        } else {
            label.text = @"Rooms";
            line.hidden = NO;
            img.hidden = NO;
            img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
        }
        return cell;
    }

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"tableviw tag selected: %ld", (long)tableView.tag);
    if(tableView.tag == 2 && indexPath.row == 0 && !eLANsSelected){
        eLANsSelected = YES;
        
        tableView.layer.borderColor = USBlueColor.CGColor;
        tableView.layer.borderWidth = 1.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
//        NSIndexPath *mIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
        
    } else if (tableView.tag == 2 && indexPath.row == 0 && eLANsSelected){
        eLANsSelected = NO;
        
        tableView.layer.borderColor = nil;
        tableView.layer.borderWidth = 0.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
//        NSIndexPath *mIndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
    } else if(tableView.tag == 6 && indexPath.row == 0 && !roomsSelected){
        roomsSelected = YES;
        
        tableView.layer.borderColor = USBlueColor.CGColor;
        tableView.layer.borderWidth = 1.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
//        NSIndexPath *mIndexPath = [NSIndexPath indexPathForRow:5 inSection:0];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
        
    } else if (tableView.tag == 6 && indexPath.row == 0 && roomsSelected){
        roomsSelected = NO;
        
        tableView.layer.borderColor = nil;
        tableView.layer.borderWidth = 0.0f;
        
        NSMutableArray *indexpatharray = [NSMutableArray new];
//        NSIndexPath *mIndexPath = [NSIndexPath indexPathForRow:5 inSection:0];
        [indexpatharray addObject:indexPath];
        
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        [_tableView insertRowsAtIndexPaths:indexpatharray withRowAnimation:UITableViewRowAnimationFade];
        
        [_tableView endUpdates];
        
    } else if(tableView.tag == 6 && indexPath.row > 0){
        roomWasSelected = YES;
        selectedRoom = [_rooms objectAtIndexPath: [NSIndexPath indexPathForRow:indexPath.row-1 inSection:indexPath.section]];
    } else if(tableView.tag == 2 && indexPath.row > 0){
        selectedElan = [[[SYCoreDataManager sharedInstance] getAllElans] objectAtIndex:indexPath.row-1];
    }
    
    if(indexPath.row == 0)
        [tableView reloadData];
}

#pragma mark - UICollectionViewDelegate
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [types count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"roomCell" forIndexPath:indexPath];
    
    
    NSString * roomType = [types objectAtIndex:indexPath.item];
    NSString * recourceName = [[ResourceTiles getDevicesDict] valueForKey:roomType];
    
    UIImage * iconImage = [UIImage imageNamed:[recourceName stringByAppendingString:@"_off"]];
    UIImage *onIconImage = [UIImage imageNamed:[recourceName stringByAppendingString:@"_on"]];
    if (iconImage==nil){
        iconImage =[UIImage imageNamed:@"zarizeni_off"];
    }
    
    if (onIconImage==nil){
        onIconImage =[UIImage imageNamed:@"zarizeni_on"];
        
    }
    
    UIImageView *img = [[[cell contentView] subviews]firstObject];
    
    if([roomType isEqualToString:myType]){
        [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
        img.image = onIconImage;
    } else {
        img.image = iconImage;
    }
    
    return cell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString * roomType = [types objectAtIndex:indexPath.item];
    NSLog(@"room type: %@", roomType);
    myType = roomType;
    
    [collectionView reloadData];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Room"];
    NSFetchRequest *fetchRequest2 = [[NSFetchRequest alloc] initWithEntityName:@"Device"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    [fetchRequest2 setSortDescriptors:sortDescriptors];
    
    _devices = [[NSFetchedResultsController alloc]
                initWithFetchRequest:fetchRequest2
                managedObjectContext:context
                sectionNameKeyPath:nil
                cacheName:nil];
    _devices.delegate = self;
    
    _rooms = [[NSFetchedResultsController alloc]
              initWithFetchRequest:fetchRequest
              managedObjectContext:context
              sectionNameKeyPath:nil
              cacheName:nil];
    _rooms.delegate = self;
    NSError *error;
    
    if (![_rooms performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
    
    if (![_devices performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if(controller == _rooms)
    [self.tableView reloadData];
}

#pragma mark - UIPickerView methods
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return[types count];
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    NSString* product = (NSString*)[productTypes objectAtIndex:row];
    UILabel * label = [[UILabel alloc] init];
    [label setText:product];
    [label setTextColor:[UIColor whiteColor]];
    return label;

}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 60.f;
}

- (IBAction)saveDevice:(id)sender {
    if([myDeviceName isEqualToString:@""] || myDeviceName == nil){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"nameError", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if ([myType isEqualToString:@""] || myType == nil ) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"deviceType", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if ([myProductType isEqualToString:@""] || myProductType == nil ) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"selectProductType", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    for(Device*device in [[SYCoreDataManager sharedInstance]getAllDevices]){
        NSLog(@"Room: '%@', myString:'%@'", device.label, myDeviceName);
        if([[device.label stringByReplacingOccurrencesOfString:@" " withString:@""] isEqualToString:[myDeviceName stringByReplacingOccurrencesOfString:@" " withString:@""]]){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"duplicateName",nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
        }
    }
    
    NSString *regEx = @"[^0-9a-fA-F]";
    NSRange range = [myAddress rangeOfString:regEx options:NSRegularExpressionSearch];
    if ([myAddress length]>6 || range.location != NSNotFound) {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"",nil) message:NSLocalizedString(@"deviceErrorAddress",nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if(selectedElan == nil){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"selectElan", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    
    unsigned resultInt = 0;
    NSScanner *scanner = [NSScanner scannerWithString:myAddress];
    
    [scanner setScanLocation:0]; // bypass '#' character
    [scanner scanHexInt:&resultInt];
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", @"")];
    
    if (_device.deviceID!=nil)
        [[SYAPIManager sharedInstance] updateDeviceWithLabel:myDeviceName deviceType:myType productType:myProductType address:[[NSNumber alloc] initWithInt:resultInt] id:_device.deviceID toElan:selectedElan success:^(AFHTTPRequestOperation * operation, id response)
         {
             [_loaderDialog hide];
         } failure:^(AFHTTPRequestOperation * operation, NSError * error)
         {
             [_loaderDialog hide];
             UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"cannotAddDevice", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }];
    
    else
        [[SYAPIManager sharedInstance] createDeviceWithLabel:myDeviceName deviceType:myType productType:myProductType address:[[NSNumber alloc] initWithInt:resultInt] toElan:selectedElan success:^(AFHTTPRequestOperation * request, id object)
         {
             if(selectedRoom != nil){
             } else {
                 [_loaderDialog hide];
                 [self performSegueWithIdentifier:@"devicesList" sender:nil];
             }
         } failure:^(AFHTTPRequestOperation * operation, NSError * error)
         {
             [_loaderDialog hide];
             UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"cannotAddDevice", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
             [alert show];
         }];
}


-(void)pairWithRoom{
    [[SYCoreDataManager sharedInstance] pairDevice:_device andRoom:selectedRoom withCoordinates:[self getCoordsFor:_device ]];
    
    NSDictionary * dict = [[[SYCoreDataManager sharedInstance] getRoomWithID:selectedRoom.roomID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]] serializeToDictionary];
    
    Elan*elan = selectedRoom.elan;
    
    [[SYAPIManager sharedInstance] updateRoomWithDictionary:dict toElan:elan success:^(AFHTTPRequestOperation * operation, id object)
     {
         [_loaderDialog hide];
         [self performSegueWithIdentifier:@"devicesList" sender:nil];
         
     }failure:^(AFHTTPRequestOperation * operation, NSError * error)
     {
         [_loaderDialog hide];
      
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Could not pair devices. Please retry again later", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
         [alert show];
     }];
}

-(struct SYDevicePosition)getCoordsFor:(Device*)device{
    struct SYDevicePosition position;
    NSMutableArray * result = [[NSMutableArray alloc] init];
    NSNumber* val1 = [NSNumber numberWithFloat:0] ;
    [result addObject:val1];
    NSNumber* val2 =[NSNumber numberWithFloat:0];
    
    [result addObject:val2];
    
    return position;
}

- (IBAction)showPicker:(id)sender {
    _pickerViewContainer.hidden = NO;
}

- (IBAction)doneButtonTapped:(id)sender {
    _pickerViewContainer.hidden = YES;
    myProductType = [productTypes objectAtIndex: [_pickerView selectedRowInComponent:0]];
    
    NSIndexPath*ip = [NSIndexPath indexPathForRow:3 inSection:0];
    NSArray*ips = [[NSArray alloc] initWithObjects:ip, nil];
    [_tableView reloadRowsAtIndexPaths:ips withRowAnimation:UITableViewRowAnimationNone];
}

-(void)recievedNotification{
    if(selectedRoom == nil || requestSent)return;
    for(Device*myDev in [[SYCoreDataManager sharedInstance] getAllDevices]){
        if([myDev.label isEqualToString:myDeviceName]){
            _device = myDev;
            [self pairWithRoom];
            requestSent = YES;
        }
    }
}

@end
