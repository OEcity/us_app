//
//  DevicesTableView.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 12/02/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "DevicesTableViewDelegate.h"
#import "RGBTrimmerViewController.h"
#import "Device.h"
#import "TrimmerViewController.h"
#import "Util.h"
#import "URLConnector.h"
#import "NSObject+SBJson.h"
#import "SYCoreDataManager.h"
#import "AppDelegate.h"
#import "DeviceInRoom.h"
#import "Device+State.h"
#import "Device+Action.h"
#import "SYAPIManager.h"
#import "SYDeviceCell.h"
#import "SY_State_HCAView.h"
#import "SY_State_TemperatureView.h"
#import "SY_State_RGBView.h"
#import "SY_State_TempTrimmerView.h"

@interface DevicesTableViewDelegate ()


- (void)initFetchedResultsController;

@end

@implementation DevicesTableViewDelegate

- (id)initWithRoomID:(NSString *)roomID {
    id res = [super init];
    if (res != nil) {
        _roomID = roomID;
        [self initFetchedResultsController];
        [_tableView reloadData];
    }
    return res;
}

- (id)init {
    self = [super init];
    
    if (self) {
        _deviceListMode = DeviceListModeRoom;
        
    }
    
    return self;
}

#pragma mark - Properties

- (void)setRoomID:(NSString *)roomID {
    _roomID = roomID;
    [self initFetchedResultsController];
}

- (void)setDeviceListMode:(DeviceListMode)deviceListMode {
    _deviceListMode = deviceListMode;
    
    [self initFetchedResultsController];
}

#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:_deviceListMode == DeviceListModeRoom ? @"DeviceInRoom" : @"Device"];
    // [[NSFetchRequest alloc] init];
    
    //  NSEntityDescription *entity = [NSEntityDescription entityForName: _deviceListMode==DeviceListModeRoom?@"DeviceInRoom":@"Device"  inManagedObjectContext:context];
    
    NSPredicate *searchFilter;
    if (_deviceListMode == DeviceListModeRoom) {
        searchFilter = [NSPredicate predicateWithFormat:@"room.roomID == %@", _roomID];
    } else if (_deviceListMode == DeviceListModeFavourite) {
        searchFilter = [NSPredicate predicateWithFormat:@"favourite == YES"];
        //        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.favourite == 1"];
        //        _favourites = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Device" withPredicate:predicate];
    }
    
    //    [fetchRequest setRelationshipKeyPathsForPrefetching:[NSArray arrayWithObjects:@"device.state", nil]];
    [fetchRequest setIncludesSubentities:YES];
    //[fetchRequest setEntity:entity];
    [fetchRequest setPredicate:searchFilter];
    
    [fetchRequest setReturnsObjectsAsFaults:NO];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:_deviceListMode == DeviceListModeRoom ? @"device.label" : @"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetchRequest
                                 managedObjectContext:context
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    _fetchedResultsController.delegate = self;
    NSError *error;
    
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"Error fetching devices: %@", error.description);
    }
    
    NSFetchRequest *fetchRequest2 = [[NSFetchRequest alloc] initWithEntityName:@"State"];
    NSPredicate *searchFilter2;
    if (_deviceListMode == DeviceListModeRoom) {
        searchFilter2 = [NSPredicate predicateWithFormat:@"(SUBQUERY(self.device.inRooms, $r, $r.room.roomID == %@).@count > 0 OR \
                         SUBQUERY(self.device.deviceCooling.inRooms, $r1, $r1.room.roomID == %@).@count > 0 OR \
                         SUBQUERY(self.device.deviceHeating.inRooms, $r1, $r1.room.roomID == %@).@count > 0)", _roomID, _roomID, _roomID];
    } else {
        searchFilter2 = [NSPredicate predicateWithFormat:@"self.device.favourite == YES"];
    }
    [fetchRequest2 setPredicate:searchFilter2];
    NSSortDescriptor *sortDescriptor2 = [[NSSortDescriptor alloc] initWithKey:@"device.label" ascending:YES];
    [fetchRequest2 setSortDescriptors:[[NSArray alloc] initWithObjects:sortDescriptor2, nil]];
    
    [fetchRequest2 setReturnsObjectsAsFaults:NO];
    
    _fetchedResultsController2 = [[NSFetchedResultsController alloc]
                                  initWithFetchRequest:fetchRequest2
                                  managedObjectContext:context
                                  sectionNameKeyPath:nil
                                  cacheName:nil];
    _fetchedResultsController2.delegate = self;
    if (![_fetchedResultsController2 performFetch:&error]) {
        NSLog(@"Error fetching devices: %@", error.description);
    }
    
    
    
}

#pragma mark - NSfetchRequestResultsDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView reloadData];
    NSLog(@"Reloading tableView data");
    //    if (_deviceListMode == DeviceListModeFavourite) {
    //        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"self.favourite == '1'"];
    //        _favourites = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Device" withPredicate:predicate];
    //    }
}

/*- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
 NSLog(@"updating row %ld",(long)indexPath.row);
 switch (type) {
 case NSFetchedResultsChangeInsert:
 [self.tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
 break;
 case  NSFetchedResultsChangeDelete:
 [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
 break;
 case  NSFetchedResultsChangeMove:
 [self.tableView moveRowAtIndexPath:indexPath toIndexPath:newIndexPath];
 break;
 case  NSFetchedResultsChangeUpdate:
 [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
 break;
 
 default:
 break;
 }
 }*/

#pragma mark - TableView handling


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //    if (_deviceListMode == DeviceListModeFavourite) {
    //        return [_favourites count];
    //    }
    if ([[_fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects];
    } else
        return 0;
}

RGBTrimmerViewController *nonSystemsController;
// Customize the appearance of table view cells.

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SYDeviceCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SYDeviceCell"];
    
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SYDeviceCell" owner:self options:nil] lastObject];
    } else {
        cell.cBatteryWidth.constant = 0;
        cell.cLockWidth.constant = 0;
        cell.cLockRightMargin.constant = 0;
        cell.cBatteryRightMargin.constant = 0;
    }
    
    UIView * actionView = [cell viewWithTag:1000];
    for (UIView* view in [actionView subviews]){
        [view removeFromSuperview];
    }
    [actionView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    Device *selectedDevice;
    
    if (_deviceListMode == DeviceListModeRoom) {
        DeviceInRoom *deviceInRoom = (DeviceInRoom *) [_fetchedResultsController objectAtIndexPath:indexPath];
        selectedDevice = deviceInRoom.device;
    } else {
        selectedDevice = (Device *) [_fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    
    if (selectedDevice == nil) return cell;
    UILabel *deviceName = (UILabel *) [cell viewWithTag:2];
    UIImageView *deviceIcon = (UIImageView *) [cell viewWithTag:1];
    UIImageView *deviceControl = (UIImageView *) [cell viewWithTag:3];
    UIImageView *ivLockedDevice = (UIImageView *) [cell viewWithTag:6];
    UIImageView *ivLowBatt = (UIImageView *) [cell viewWithTag:7];
    ivLockedDevice.hidden = YES;
    ivLowBatt.hidden = YES;
    cell.userInteractionEnabled = YES;
    if ([selectedDevice hasStateName:@"locked"]) {
        if ([[selectedDevice getStateValueForStateName:@"locked"] boolValue] == YES) {
            ivLockedDevice.hidden = NO;
            cell.cLockWidth.constant = 25;
            cell.cLockRightMargin.constant = 5;
            cell.userInteractionEnabled = NO;
        }
    }
    if ([selectedDevice hasStateName:@"battery"]) {
        if ([[selectedDevice getStateValueForStateName:@"battery"] boolValue] == NO) {
            cell.cBatteryWidth.constant = 25;
            cell.cBatteryRightMargin.constant = 5;
            ivLowBatt.hidden = NO;
        }
    }
    [deviceName setTextAlignment:NSTextAlignmentLeft];
    //[deviceName setTextColor:[UIColor colorWithRed:176.0 / 255.0 green:176.0 / 255.0 blue:176.0 / 255.0 alpha:1.0]];
    [deviceName setText:[deviceName.text stringByAppendingString:@" °C"]];
    UIFont *boldFont = [UIFont systemFontOfSize:17];
    while ([cell viewWithTag:201] != nil) {
        [[cell viewWithTag:201] removeFromSuperview];
    }
    [deviceName setFont:boldFont];
    
    deviceName.text = selectedDevice.label;
    deviceControl.hidden = NO;
    while ([cell viewWithTag:99] != nil)
        [[cell viewWithTag:99] removeFromSuperview];
    while ([cell viewWithTag:100] != nil)
        [[cell viewWithTag:100] removeFromSuperview];
    
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.roundingIncrement = [NSNumber numberWithDouble:0.1];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.decimalSeparator = @",";
    
    // Show cell for RGB
    if ([selectedDevice hasStateName:@"red"] && [selectedDevice hasStateName:@"green"] && [selectedDevice hasStateName:@"blue"] && [selectedDevice hasStateName:@"demo"] && [selectedDevice hasStateName:@"brightness"]) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSNumber *intensity = (NSNumber *) [defaults objectForKey:selectedDevice.deviceID];
        if (intensity == nil) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSNumber *intensityNumber = [[NSNumber alloc] initWithInt:[[selectedDevice getStateValueForStateName:@"brightness"] intValue]];
            [defaults setObject:intensityNumber forKey:selectedDevice.deviceID];
            [defaults synchronize];
        }
        
        if ([selectedDevice getStateValueForStateName:@"brightness"] == nil || [selectedDevice getStateValueForStateName:@"red"] == nil || [selectedDevice getStateValueForStateName:@"green"] == nil || [selectedDevice getStateValueForStateName:@"blue"] == nil || [selectedDevice getStateValueForStateName:@"demo"] == nil) {
            [self showUknownDeviceStateForCell:cell deviceControl:deviceControl];
        } else {
            UIColor *colorUI = [Util convertToUIColorRed:(CGFloat) [[selectedDevice getStateValueForStateName:@"red"] floatValue] green:(CGFloat) [[selectedDevice getStateValueForStateName:@"green"] floatValue] blue:(CGFloat) [[selectedDevice getStateValueForStateName:@"blue"] floatValue]];
            
            SY_State_RGBView* deviceStateView = [SY_State_View initStateViewForNibName:@"SY_State_RGBView"];
            [deviceStateView insertViewToSuperView:actionView];
            [deviceStateView setMaxValue:[((Action *) [selectedDevice getActionForActionName:@"brightness"]).max intValue]];
            [deviceStateView setValue:[[selectedDevice getStateValueForStateName:@"brightness"] intValue]];
            [deviceStateView setColor:colorUI];
            [deviceStateView displayView];
            [cell setUserInteractionEnabled:YES];
            deviceControl.hidden = YES;
        }
        
        // Show cell for Blinds or Gate
    } else if ([selectedDevice getStateValueForStateName:@"roll up"]) {
        
        if ([[selectedDevice getStateValueForStateName:@"roll up"] boolValue]) {
            deviceControl.image = [UIImage imageNamed:@"blinds_up_sip.png"];
            deviceControl.highlightedImage = [UIImage imageNamed:@"blinds_up_sip_on.png"];
            deviceControl.contentMode = UIViewContentModeScaleAspectFit;
            //            [deviceIcon setImage:[UIImage imageNamed:@"zaluzie_off.png"]];
        } else {
            deviceControl.image = [UIImage imageNamed:@"blinds_down_sip.png"];
            deviceControl.highlightedImage = [UIImage imageNamed:@"blinds_down_sip_on.png"];
            deviceControl.contentMode = UIViewContentModeScaleAspectFit;
            //            [deviceIcon setImage:[UIImage imageNamed:@"zaluzie_on_off.png"]];
        }
        
        if ([selectedDevice getStateValueForStateName:@"roll up"] == nil) {
            [cell setUserInteractionEnabled:NO];
            
            [deviceControl setImage:[UIImage imageNamed:@"invalide_device_state"]];
        } else {
            [cell setUserInteractionEnabled:YES];
            
        }
    } else if ([selectedDevice hasStateName:@"temperature IN"] && [selectedDevice hasStateName:@"temperature OUT"]) {
        if ([selectedDevice getStateValueForStateName:@"temperature IN"] == nil || [selectedDevice getStateValueForStateName:@"temperature OUT"] == nil) {
            [self showUknownDeviceStateForCell:cell deviceControl:deviceControl];
        } else {
            SY_State_TemperatureView* deviceStateView = [SY_State_View initStateViewForNibName:@"SY_State_TemperatureView"];
            [deviceStateView insertViewToSuperView:actionView];
            [deviceStateView setModeTempINnOUT];
            [deviceStateView.lTempIn setTextColor:[UIColor colorWithRed:87 / 255.0 green:87 / 255.0 blue:87 / 255.0 alpha:1.0]];
            [deviceStateView.lTempOut setTextColor:[UIColor colorWithRed:87 / 255.0 green:87 / 255.0 blue:87 / 255.0 alpha:1.0]];
            [deviceStateView.lTempIn setText:[NSString stringWithFormat:@"%@ ºC", [formatter stringFromNumber:[selectedDevice getStateValueForStateName:@"temperature IN"]]]];
            [deviceStateView.lTempOut setText:[NSString stringWithFormat:@"%@ ºC", [formatter stringFromNumber:[selectedDevice getStateValueForStateName:@"temperature OUT"]]]];
            deviceControl.hidden = YES;
            [deviceStateView displayView];
            if ([selectedDevice getActionForActionName:@"on"] == nil) {
                [cell setUserInteractionEnabled:NO];
            }
        }
        // HCA prvek
    } else if ([selectedDevice hasStateName:@"temperature"] && [selectedDevice hasStateName:@"mode"] && [selectedDevice hasStateName:@"correction"]) {
        cell.userInteractionEnabled = NO;
//        [self showUknownDeviceStateForCell:cell deviceControl:deviceControl];
        if ([selectedDevice getStateValueForStateName:@"temperature"] == nil || [selectedDevice getStateValueForStateName:@"mode"] == nil) {
            [self showUknownDeviceStateForCell:cell deviceControl:deviceControl];
        } else {
            SY_State_HCAView* deviceStateView = [SY_State_View initStateViewForNibName:@"SY_State_HCAView"];
            [deviceStateView insertViewToSuperView:actionView];
            [deviceStateView.lTemp setText:[NSString stringWithFormat:@"%@ ºC", [formatter stringFromNumber:[selectedDevice getStateValueForStateName:@"temperature"]]]];

            [deviceStateView.lHeatingMode setTextColor:[UIColor colorWithRed:87 / 255.0 green:87 / 255.0 blue:87 / 255.0 alpha:1.0]];
            [deviceStateView.lHeatingMode sizeToFit];
            if ([[selectedDevice getStateValueForStateName:@"on"] boolValue] == NO) {
                [deviceStateView.lHeatingMode setText:NSLocalizedString(@"hca_off", nil)];
                deviceStateView.cModeImageWidth.constant = 0;
            } else {
                deviceStateView.cModeImageWidth.constant = DEFAULT_MODE_IMAGE_WIDTH;
                switch ([[selectedDevice getStateValueForStateName:@"mode"] intValue]) {
                    case 1: {
                        [deviceStateView.lHeatingMode setText:NSLocalizedString(@"heat_mode_minimum", nil)];
                        [deviceStateView.iHeatingMode setImage:[UIImage imageNamed:@"kolecko_minimum"]];
                        break;
                    }

                    case 2: {
                        [deviceStateView.lHeatingMode setText:NSLocalizedString(@"heat_mode_attenuation", nil)];
                        [deviceStateView.iHeatingMode setImage:[UIImage imageNamed:@"kolecko_utlum"]];
                        break;
                    }
                    case 3: {
                        [deviceStateView.lHeatingMode setText:NSLocalizedString(@"heat_mode_normal", nil)];
                        [deviceStateView.iHeatingMode setImage:[UIImage imageNamed:@"kolecko_normal"]];
                        break;

                    }
                    case 4: {
                        [deviceStateView.lHeatingMode setText:NSLocalizedString(@"heat_mode_comfort", nil)];
                        [deviceStateView.iHeatingMode setImage:[UIImage imageNamed:@"kolecko_komfort"]];
                        break;

                    }
                    default:
                        [deviceStateView.lHeatingMode setText:NSLocalizedString(@"Minimum", nil)];
                        [deviceStateView.iHeatingMode setImage:[UIImage imageNamed:@"kolecko_minimum"]];

                        break;
                }
            }
            [deviceStateView displayView];
             deviceControl.hidden = YES;
//            return cell;
        }
    } else if ([selectedDevice hasStateName:@"temperature"] && [selectedDevice hasStateName:@"open window"] && [selectedDevice hasStateName:@"open valve"] && [selectedDevice hasStateName:@"battery"] && [selectedDevice hasStateName:@"requested temperature"] && [selectedDevice hasStateName:@"open window sensitivity"] && [selectedDevice hasStateName:@"open window off time"]) {
        if ([selectedDevice getStateValueForStateName:@"temperature"] == nil || [selectedDevice getStateValueForStateName:@"open window"] == nil || [selectedDevice getStateValueForStateName:@"open valve"] == nil || [selectedDevice getStateValueForStateName:@"battery"] == nil || [selectedDevice getStateValueForStateName:@"requested temperature"] == nil || [selectedDevice getStateValueForStateName:@"open window sensitivity"] == nil || [selectedDevice getStateValueForStateName:@"open window off time"] == nil) {
            [self showUknownDeviceStateForCell:cell deviceControl:deviceControl];
        } else {
            SY_State_TempTrimmerView* deviceStateView = [SY_State_View initStateViewForNibName:@"SY_State_TempTrimmerView"];
            [deviceStateView insertViewToSuperView:actionView];
            [deviceStateView setMaxValue:100];
            [deviceStateView setValue:[[selectedDevice getStateValueForStateName:@"open valve"] intValue]];
            [deviceStateView setTemp:[selectedDevice getStateValueForStateName:@"temperature"]];
            [deviceStateView setShowTrimmer:YES];
            [deviceStateView displayView];
             deviceControl.hidden = YES;
        }
    } else if ([selectedDevice hasStateName:@"temperature"]) {
        if ([selectedDevice getStateValueForStateName:@"temperature"] == nil) {
            [self showUknownDeviceStateForCell:cell deviceControl:deviceControl];
        } else {
            SY_State_TempTrimmerView* deviceStateView = [SY_State_View initStateViewForNibName:@"SY_State_TempTrimmerView"];
            [deviceStateView insertViewToSuperView:actionView];
            [deviceStateView setTemp:[selectedDevice getStateValueForStateName:@"temperature"]];
            if ([selectedDevice getStateValueForStateName:@"on"] != nil){
                [deviceStateView setOn:[[selectedDevice getStateValueForStateName:@"on"] boolValue]];
            }
            [deviceStateView setShowTrimmer:NO];
            [deviceStateView displayView];
            deviceControl.hidden = YES;
        }
    } else if ([selectedDevice hasStateName:@"brightness"]) {
        int intensity = [[selectedDevice getStateValueForStateName:@"brightness"] intValue];
        int maxValue = [((Action *) [selectedDevice getActionForActionName:@"brightness"]).max intValue];
        if (intensity > maxValue) intensity = maxValue;
        //NSLog(@"device name:%@", selectedDevice.deviceID);
        if (intensity != 0) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSNumber *intensityNumber = [selectedDevice getStateValueForStateName:@"brightness"];
            [defaults setObject:intensityNumber forKey:selectedDevice.deviceID];
            [defaults synchronize];
        }
        if ([selectedDevice getStateValueForStateName:@"brightness"] == nil) {
            [self showUknownDeviceStateForCell:cell deviceControl:deviceControl];
        } else {
            SY_State_RGBView* deviceStateView = [SY_State_View initStateViewForNibName:@"SY_State_RGBView"];
            [deviceStateView insertViewToSuperView:actionView];
            [deviceStateView setMaxValue:maxValue];
            [deviceStateView setValue:intensity];
            [deviceStateView setColor:nil];
            
            [deviceStateView displayView];
            [cell setUserInteractionEnabled:YES];
            deviceControl.hidden = YES;
        }
    } else if ([selectedDevice hasStateName:@"on"]) {
        
        //        Action * on = [selectedDevice getActionForActionName:@"on"];
        
        if ([[selectedDevice getStateValueForStateName:@"on"] boolValue] == YES) {
            //                [deviceIcon setImage:[UIImage imageNamed:@"zarovka_on.png"]];
            [deviceControl setImage:[UIImage imageNamed:@"room_stav_on.png"]];
            deviceControl.contentMode = UIViewContentModeCenter;
        } else {
            //                [deviceIcon setImage:[UIImage imageNamed:@"zarovka_off.png"]];
            [deviceControl setImage:[UIImage imageNamed:@"room_stav_off.png"]];
            deviceControl.contentMode = UIViewContentModeCenter;
        }
        if ([selectedDevice getStateValueForStateName:@"on"] == nil) {
            [cell setUserInteractionEnabled:NO];
            [deviceControl setImage:[UIImage imageNamed:@"unknown_device_state"]];
        }
    }
    
    deviceIcon.image = [Util determineImage:selectedDevice];
    deviceIcon.highlightedImage = [Util determineImageOn:selectedDevice];
    [deviceIcon setContentMode:UIViewContentModeScaleAspectFit];
    
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    [cell setSelectedBackgroundView:bgColorView];
    return cell;
}

- (void) showUknownDeviceStateForCell:(SYDeviceCell*)cell deviceControl:(UIImageView*)deviceControl{
//    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 46, 44)];
//    [image setImage:[UIImage imageNamed:@"unknown_device_state"]];
//    [image setContentMode:UIViewContentModeScaleAspectFit];
//    //            [image setCenter:[actionView center]];
//    image.tag = 100;
//    [image setCenter:CGPointMake(actionView.frame.origin.x + actionView.frame.size.width/2,
//                                 actionView.frame.origin.y + actionView.frame.size.height/2)];
//    [cell addSubview:image];
//    [cell bringSubviewToFront:image];
//    [cell setUserInteractionEnabled:NO];
    [deviceControl setImage:[UIImage imageNamed:@"unknown_device_state"]];
    [deviceControl setHidden:NO];
    [cell setUserInteractionEnabled:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    /*for (RGBTrimmerViewController * controller in self.rgbControllers){
     [nonSystemsController.circleView setBackgroundColor:controller.color];
     }*/
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Device *selectedDevice;
    
    if (_deviceListMode == DeviceListModeRoom) {
        DeviceInRoom *deviceInRoom = (DeviceInRoom *) [_fetchedResultsController objectAtIndexPath:indexPath];
        selectedDevice = (Device *) deviceInRoom.device;
        deviceInRoom.coordX = [(NSManagedObject *) deviceInRoom valueForKey:@"coordX"];
        deviceInRoom.coordY = [(NSManagedObject *) deviceInRoom valueForKey:@"coordY"];
        
    } else {
        selectedDevice = (Device *) [_fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    
    if (selectedDevice == nil) return;
    if (selectedDevice.states == nil)return;
    if ([selectedDevice getStateValueForStateName:@"locked"] != nil) if ([[selectedDevice getStateValueForStateName:@"locked"] boolValue] == YES) return;
    
    if ([selectedDevice getStateValueForStateName:@"roll up"]) {
        NSDictionary *dict = nil;
        if ([[selectedDevice getStateValueForStateName:@"roll up"] boolValue] == YES)
            dict = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNull alloc] init], @"roll down", nil];
        else
            dict = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNull alloc] init], @"roll up", nil];
        
        //  NSLog(@"post data to %@ with value %@",[@"device/" stringByAppendingString:selectedDevice.name], postDataString );
        [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:dict device:selectedDevice success:^(AFHTTPRequestOperation *operation, id object) {
        }                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        }];
    } else if ([selectedDevice getStateValueForStateName:@"red"] != nil && [selectedDevice getStateValueForStateName:@"green"] != nil && [selectedDevice getStateValueForStateName:@"blue"] != nil) {
        
        //UIColor * colorUI = [Util convertToUIColorRed:(CGFloat)[[selectedDevice getStateValueForStateName:@"red"] floatValue] green:(CGFloat)[[selectedDevice getStateValueForStateName:@"green"] floatValue] blue:(CGFloat)[[selectedDevice getStateValueForStateName:@"blue"] floatValue]];
        //[nonSystemsController.circleView setBackgroundColor:colorUI];
        /*for (RGBTrimmerViewController * controller in self.rgbControllers){
         [controller.circleView setBackgroundColor:colorUI];
         }*/
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
#warning STOP SETTING BRIGHTNESS
        if ([[selectedDevice getStateValueForStateName:@"brightness"] boolValue] == NO) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSNumber *intensity = (NSNumber *) [defaults objectForKey:selectedDevice.deviceID];
            if (intensity != nil && intensity.intValue != 0) {
                //                [selectedDevice setStateValue:intensity forName:@"brightness"];
                [dict setObject:intensity forKey:@"brightness"];
            } else {
                //                [selectedDevice setStateValue:[[selectedDevice getActionForActionName:@"brightness"] max] forName:@"brightness"];
                [dict setObject:[[selectedDevice getActionForActionName:@"brightness"] max] forKey:@"brightness"];
            }
        } else {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            NSNumber *intensityNumber = [[NSNumber alloc] initWithInt:[[selectedDevice getStateValueForStateName:@"brightness"] intValue]];
            [defaults setObject:intensityNumber forKey:selectedDevice.deviceID];
            [defaults synchronize];
            //            [selectedDevice setStateValue:[[NSNumber alloc] initWithInt:0] forName:@"brightness"];
            [dict setObject:[[selectedDevice getActionForActionName:@"brightness"] min] forKey:@"brightness"];
        }
        [dict setObject:[selectedDevice getStateValueForStateName:@"green"] forKey:@"green"];
        [dict setObject:[selectedDevice getStateValueForStateName:@"red"] forKey:@"red"];
        [dict setObject:[selectedDevice getStateValueForStateName:@"blue"] forKey:@"blue"];
        [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:dict device:selectedDevice success:^(AFHTTPRequestOperation *operation, id object) {
        }                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        }];
        
    } else {
        if ([selectedDevice getStateValueForStateName:@"on"] != nil) {
            NSDictionary *stateDictionary = nil;
            if ([[selectedDevice getStateValueForStateName:@"on"] intValue] == 1)
                stateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithBool:NO], @"on", nil];
            else
                stateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithBool:YES], @"on", nil];
            
            NSLog(@"deviceID: %@", selectedDevice.deviceID);
            [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:stateDictionary device:selectedDevice success:^(AFHTTPRequestOperation *operation, id object) {
                NSLog(@"sucessfully updated");
            }                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                NSLog(@"update err");
            }];
            [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
        if ([selectedDevice getActionForActionName:@"brightness"] != nil) {
            //            NSNumber * newValue = nil;
            //            if ([[selectedDevice getStateValueForStateName:@"brightness"] intValue]==0){
            //                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            //                NSNumber *intensity = (NSNumber*)[defaults objectForKey:selectedDevice.deviceID];
            //                if (intensity.intValue!=0){
            //                    newValue = intensity;
            //                    //[defaults removeObjectForKey:selectedDevice.name];
            //                }else{
            //                    newValue = [[selectedDevice getActionForActionName:@"brightness"] max];
            //                }
            //            }else{
            //
            //                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            //                NSNumber * intensityNumber = [[NSNumber alloc] initWithInt:[[selectedDevice getStateValueForStateName:@"brightness"] intValue]];
            //                [defaults setObject:intensityNumber forKey:selectedDevice.deviceID];
            //                [defaults synchronize];
            //                newValue = [[NSNumber alloc] initWithInt:0];
            //            }
            //            NSString * postDataString = @"{\"brightness\":";
            //
            //            postDataString = [postDataString stringByAppendingString:[NSString stringWithFormat:@"%d", [[selectedDevice getStateValueForStateName:@"brightness"] intValue]]];
            //            postDataString = [postDataString stringByAppendingString:@"}"];
            //            NSData* data = [postDataString dataUsingEncoding:NSUTF8StringEncoding];
            //            //            NSLog(@"post data to %@ with value %@",[@"device/" stringByAppendingString:selectedDevice.name], postDataString );
            //            NSDictionary * stateDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:newValue, @"brightness", nil];
            //
            //            [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:stateDictionary device:selectedDevice success:^(AFHTTPRequestOperation * operation, id object){} failure:^(AFHTTPRequestOperation * operation, NSError * error){}];
            
            Action *brigtnessAction = [selectedDevice getActionForActionName:@"brightness"];
            id brigtnessValue = [selectedDevice getStateValueForStateName:@"brightness"];
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            if ([brigtnessValue isEqualToNumber:brigtnessAction.min]) {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSNumber *intensity = (NSNumber *) [defaults objectForKey:selectedDevice.deviceID];
                if (intensity != nil && ![intensity isEqualToNumber:brigtnessAction.min]) {
                    //                    [selectedDevice setStateValue:intensity forName:@"brightness"];
                    [dict setObject:intensity forKey:@"brightness"];
                } else {
                    //                    [selectedDevice setStateValue:brigtnessAction.max forName:@"brightness"];
                    [dict setObject:brigtnessAction.max forKey:@"brightness"];
                }
            } else {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                NSNumber *intensityNumber = [[NSNumber alloc] initWithInt:[brigtnessValue intValue]];
                [defaults setObject:intensityNumber forKey:selectedDevice.deviceID];
                [defaults synchronize];
                //                [selectedDevice setStateValue:brigtnessAction.min forName:@"brightness"];
                [dict setObject:brigtnessAction.min forKey:@"brightness"];
            }
            [[SYAPIManager sharedInstance] updateDeviceStateWithDictionary:dict device:selectedDevice success:^(AFHTTPRequestOperation *operation, id object) {
            }                                                      failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                                  message:NSLocalizedString(@"elan_not_reacheable", nil)
                                                                 delegate:nil
                                                        cancelButtonTitle:@"OK"
                                                        otherButtonTitles:nil];
                [message show];
                NSLog(@"Elan did not respond fot action!");
            }];
            [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
            
        }
        
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        CGPoint p = [gestureRecognizer locationInView:self.tableView];
        
        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
        Device *device;
        
        
        if (_deviceListMode == DeviceListModeRoom) {
            DeviceInRoom *deviceInRoom = (DeviceInRoom *) [_fetchedResultsController objectAtIndexPath:indexPath];
            device = deviceInRoom.device;
        } else {
            device = (Device *) [_fetchedResultsController objectAtIndexPath:indexPath];
            
        }
        if ([device getStateValueForStateName:@"locked"] != nil) if ([[device getStateValueForStateName:@"locked"] boolValue] == YES) return;
        if ([device getActionForActionName:@"red"] != nil && [device getActionForActionName:@"green"] != nil && [device getActionForActionName:@"blue"] != nil && [device getActionForActionName:@"demo"] != nil && [device getActionForActionName:@"brightness"] != nil) {
            if ([self.view viewWithTag:60] == nil) {
                //_rgb = [[RGBViewController alloc] initWithNibName:@"RGBViewController" bundle:nil device:device];
                _rgb.view.tag = 60;
                
                [self.view addSubview:_rgb.view];
            }
        } else {
            if ([self.view viewWithTag:50] == nil) {
                if (![device getActionForActionName:@"requested temperature"] && ![device getActionForActionName:@"correction"]) {
                    
                    if ([device.secondaryActions count] <= 0) {
                        return;
                    }
                    
                    _actions = [[DeviceActionsViewController alloc] initWithNibName:@"DeviceActionsViewController" bundle:nil];
                    [_actions setDevice:device];
                    //                    [_actions setActions:device.secondaryActions];
                    _actions.view.tag = 50;
                    [self.view addSubview:_actions.view];
                } else if ([device getStateValueForStateName:@"temperature"] != nil && [device getStateValueForStateName:@"open window"] != nil && [device getStateValueForStateName:@"open valve"] != nil && [device getStateValueForStateName:@"battery"] != nil && [device getStateValueForStateName:@"requested temperature"] != nil && [device getStateValueForStateName:@"open window sensitivity"] != nil && [device getStateValueForStateName:@"open window off time"] != nil) {
                    if ([self.view viewWithTag:40] == nil) {
//                        
//                        _thermView = [[ThermostatViewController alloc] initWithNibName:@"ThermostatViewController" bundle:nil device:device];
//                        _thermView.view.tag = 40;
//                        [self.view addSubview:_thermView.view];
                    }
                } else if ([device getStateValueForStateName:@"correction"] != nil && [device getStateValueForStateName:@"mode"] != nil && [device getStateValueForStateName:@"temperature"] != nil) {
                    if ([self.view viewWithTag:40] == nil) {
//                        _heatView = [[HeatCoolAreaViewController alloc] initWithNibName:@"HeatCoolAreaViewController" bundle:nil device:(HeatCoolArea*) device];
//                        
//                        _heatView.view.tag = 40;
//                        [self.view addSubview:_heatView.view];
                    }
                    
                }
            }
        }
    }
    
    /*CGPoint p = [gestureRecognizer locationInView:self.tableView];
     
     NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
     
     Device * device;
     
     if (_deviceListMode == DeviceListModeRoom) {
     DeviceInRoom* deviceInRoom = (DeviceInRoom*)[_fetchRequestController objectAtIndexPath:indexPath];
     device = (Device* )[CoreDataManager readDevice:deviceInRoom.deviceName];
     } else {
     Favourites* favourite = (Favourites*)[_fetchRequestController objectAtIndexPath:indexPath];
     device = (Device* )[CoreDataManager readDevice:favourite.deviceName];
     }
     
     
     if ([device.type isEqualToString:@"rgb light"] ){
     if ([self.view viewWithTag:60]==nil){
     _rgb = [[RGBViewController alloc] initWithNibName:@"RGBViewController" bundle:nil device:device];
     _rgb.view.tag = 60;
     
     [self.view addSubview:_rgb.view];
     }
     }else{
     if ([self.view viewWithTag:50]==nil){
     if (device.secondaryActions!=nil && [device.secondaryActions count]>0){
     _actions = [[DeviceActionsViewController alloc] initWithNibName:@"DeviceActionsViewController" bundle:nil ];
     [_actions setDevice:device];
     _actions.view.tag = 50;
     [self.view addSubview:_actions.view];
     }
     }
     }*/
}

- (void)connectionResponseOK:(NSObject *)returnedObject response:(id <Response>)responseType {
    
    
}

- (void)connectionResponseFailed:(NSInteger)code {
    
}

- (void)drawTrimmerWithValue:(int)value maxValue:(int)maxValue view:(UIView *)view {
    //    [number setText:[NSString stringWithFormat:@"%d", value]];
    int max = 50;
    float onValue = (50.00 / maxValue);
    onValue *= value;
    UIImageView *myImage;
    
    for (int i = 0; i < max; i++) {
        
        if (i >= onValue) {
            myImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stmivani_off.png"]];
        } else {
            myImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"stmivani_on1.png"]];
        }
        myImage.frame = CGRectMake(320 - 36 - myImage.frame.size.width / 2, 5, myImage.frame.size.width, myImage.frame.size.height);
        myImage.tag = 100;
        [view addSubview:myImage];
        float consMove = (360.0 / max);
        [self rotateView:myImage aroundPoint:CGPointMake(320 - 36, 35) degrees:i * consMove - (360 / 4)];
    }
    
}


#define DEGREES_TO_RADIANS(angle) (angle/180.0*M_PI)

- (void)rotateView:(UIView *)view
       aroundPoint:(CGPoint)rotationPoint
           degrees:(CGFloat)degrees {
    
    CGPoint anchorPoint = CGPointMake((rotationPoint.x - CGRectGetMinX(view.frame)) / CGRectGetWidth(view.bounds),
                                      (rotationPoint.y - CGRectGetMinY(view.frame)) / CGRectGetHeight(view.bounds));
    
    [[view layer] setAnchorPoint:anchorPoint];
    [[view layer] setPosition:rotationPoint]; // change the position here to keep the frame
    CGAffineTransform rotationTransform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(degrees));
    view.transform = rotationTransform;
    
}


@end
