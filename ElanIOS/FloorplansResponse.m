//
//  FloorplansResponse.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/21/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "FloorplansResponse.h"
#import "SBJson.h"
#import "Floorplan.h"
@implementation FloorplansResponse
-(NSObject*)parseResponse:(NSData *)inputData identifier:(NSString *)identifier{
    NSMutableArray * itemsArray = [[NSMutableArray alloc] init];

    SBJsonParser *parser2 = [[SBJsonParser alloc] init];
    NSString *inputString = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];

    NSDictionary* jsonObjects = [parser2 objectWithString:inputString];


    for(NSString* dict in jsonObjects)
    {



        Floorplan *plan = [[Floorplan alloc] init];

        @try {
            plan.name = dict;
            NSString* newDict = [jsonObjects valueForKey:dict];
            plan.url = newDict;

        }
        @catch (NSException *exception) {
            //handle this exception !!

        }@finally {
            [itemsArray addObject:plan];
        }
        
    }
    return itemsArray;
    
}


@end
