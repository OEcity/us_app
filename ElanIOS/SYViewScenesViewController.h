//
//  SYViewScenesViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/7/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Scene.h"
#import "URLConnector.h"
#import "LoaderViewController.h"
#import "HUDWrapper.h"
#import "SYCoreDataManager.h"
#import "SYBaseViewController.h"
@interface SYViewScenesViewController : SYBaseViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) HUDWrapper * loaderDialog;
@end
