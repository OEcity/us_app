//
//  Elan-types.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 29.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#ifndef iHC_MIRF_Elan_types_h
#define iHC_MIRF_Elan_types_h

#define ELAN_TYPES @[@"ELAN-RF-WI",@"ELAN-RF",@"ELAN-IR"]

#endif
