//
//  LoadAppViewController.m
//  Click Smart
//
//  Created by Tom Odler on 23.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "LoadAppViewController.h"

@interface LoadAppViewController ()
@property (weak, nonatomic) IBOutlet UIProgressView *progressBar;
@property (weak, nonatomic) IBOutlet UIImageView *loveIMG;

@end

@implementation LoadAppViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _progressBar.progress = 0;
    _loveIMG.alpha = 0;
    
    [UIView animateWithDuration:1.0 animations:^{_loveIMG.alpha = 1; }];
    
    [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(callAfterSixtySecond:) userInfo:nil repeats: YES];
}

-(void) callAfterSixtySecond:(NSTimer*) t
{
    _progressBar.progress += 0.1;
    dispatch_async(dispatch_get_main_queue(), ^{
        _loveIMG.alpha += 0.1;
    });
    
    
    if(_progressBar.progress == 1){
        [self performSegueWithIdentifier:@"firstSegue" sender:nil];
        [t invalidate];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}


@end
