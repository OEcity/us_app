//
//  ChromaticityViewController.h
//  US App
//
//  Created by admin on 30.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device+State.h"
#import "URLConnector.h"
#import "URLConnectionListener.h"

@interface ChromaticityViewController : UIViewController<NSFetchedResultsControllerDelegate,URLConnectionListener>
@property Device *device;
@property (nonatomic, strong) NSFetchedResultsController* devices;
@property (nonatomic, copy)  NSString * deviceName;

@end
