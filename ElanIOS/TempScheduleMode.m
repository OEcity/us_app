//
//  TempScheduleMode.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 1/21/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "TempScheduleMode.h"

@implementation TempScheduleMode

@synthesize id = _id;
@synthesize min = _min;
@synthesize max = _max;

-(id)initWithId:(NSNumber*)id min:(NSNumber*)min max:(NSNumber*)max{
    self = [super init];
    if (self){
        
        _id = id;
        _min = min;
        _max = max;        
    }
    return self;
}


@end
