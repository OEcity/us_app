//
//  GuideRoomsAddViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 28.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Room.h"
#import "Elan.h"

@interface GuideRoomsAddViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, retain) Room*room;


@end
