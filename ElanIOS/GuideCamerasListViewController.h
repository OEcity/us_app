//
//  GuideCamerasListViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 07.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Camera.h"

@interface GuideCamerasListViewController : UIViewController<NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) NSFetchedResultsController* cameras;

@end
