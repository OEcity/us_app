//
//  WeatherViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 05.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "WeatherViewController.h"
#import "SYAPIManager.h"
#import "SYCoreDataManager.h"
#import "AppDelegate.h"
#import "MainMenuPVC.h"

@interface WeatherViewController (){
    NSNumber *vlhkost;
    NSNumber *vitr;
    NSNumber *tlak;
    NSNumber *teplota;
    NSString *mesto;
    NSString *countryCode;
    NSString *source;
    
    UIView *pointer;
}
@property (weak, nonatomic) IBOutlet UIImageView *tempImage;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;

@end

@implementation WeatherViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    pointer = [[UIView alloc] initWithFrame:CGRectMake(_tempImage.frame.origin.x, _tempImage.frame.origin.y, _tempImage.frame.size.width/2, 5)];
    pointer.backgroundColor = [UIColor redColor];
    [self.view addSubview:pointer];
    
    
    
    _cityLabel.hidden = YES;
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self setPointerY];
    NSString *mac = [[NSUserDefaults standardUserDefaults] objectForKey:@"weatherElanMac"];
    Elan*elan= [[SYCoreDataManager sharedInstance] getELANwithMAC:mac inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
    
    [[SYAPIManager sharedInstance] getWeatherFromElan:elan success:^(AFHTTPRequestOperation *operation, id response) {
        NSDictionary *dict = (NSDictionary*)response;
        [self actualizeView:dict];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", [error localizedDescription]);
    }];

}

-(void)actualizeView:(NSDictionary*)dict{
    if([dict objectForKey:@"temp"]){
        source = [dict objectForKey:@"source"];
        if([source containsString:@"3000"]){
            teplota = [dict objectForKey:@"temp"];
            tlak = [dict objectForKey:@"pressure"];
            vitr= [dict objectForKey:@"wind speed"];
            _imgVitr.transform = CGAffineTransformMakeRotation([[dict objectForKey:@"wind direction"]floatValue]);
            vlhkost = [dict objectForKey:@"relative humidity"];
            [self writeGiomNumbers];
            [self setPointerY];
        } else {
            vlhkost =[dict objectForKey:@"humidity"];
            vitr= [[dict objectForKey:@"wind"]objectForKey:@"speed"];
            tlak = [dict objectForKey:@"pressure"];
            teplota = [dict objectForKey:@"temp"];
            _imgVitr.transform = CGAffineTransformMakeRotation([[[dict objectForKey:@"wind"]objectForKey:@"deg"]floatValue]);
            mesto = [dict objectForKey:@"name"];
            countryCode = [dict objectForKey:@"country"];
            source = [dict objectForKey:@"source"];
            [self writeNumbers];
            [self setPointerY];
        }
    } else {
        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication]delegate];
        app.hasGIOMWeather = app.hasOPWeather = NO;
        
        MainMenuPVC*menu = app.mainMenuPVC;
        [menu refreshPVC:@"viewwillappear weather view"];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    
    [self loadWeather];
    
    double yValue = ((_tempImage.frame.size.height-13)/80)*(80-(0+30))+13;
    [pointer setFrame:CGRectMake(_tempImage.frame.origin.x, yValue, _tempImage.frame.size.width/2, 5)];
}

-(void)setPointerY{
    double yValue = ((_tempImage.frame.size.height-13)/80)*(80-([self kelvinToDegrees:teplota].doubleValue+30))+13;
    [pointer setFrame:CGRectMake(_tempImage.frame.origin.x, yValue, _tempImage.frame.size.width/2, 5)];
}

-(void)loadWeather{
    [NSTimer scheduledTimerWithTimeInterval: 30.0 target: self
                                                      selector: @selector(updateWeather) userInfo: nil repeats: YES];
}

-(void)updateWeather{
    NSString *mac = [[NSUserDefaults standardUserDefaults] objectForKey:@"weatherElanMac"];
    Elan*elan= [[SYCoreDataManager sharedInstance] getELANwithMAC:mac inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
    
    [[SYAPIManager sharedInstance] getWeatherFromElan:elan success:^(AFHTTPRequestOperation *operation, id response) {
        NSDictionary *dict = (NSDictionary*)response;
        if([dict objectForKey:@"temp"]){
            source = [dict objectForKey:@"source"];
            if([source containsString:@"3000"]){
                teplota = [dict objectForKey:@"temp"];
                tlak = [dict objectForKey:@"pressure"];
                vitr= [dict objectForKey:@"wind speed"];
                _imgVitr.transform = CGAffineTransformMakeRotation([[dict objectForKey:@"wind direction"]floatValue]);
                vlhkost = [dict objectForKey:@"relative humidity"];
                [self writeGiomNumbers];
                [self setPointerY];
            } else {
                vlhkost =[dict objectForKey:@"humidity"];
                vitr= [[dict objectForKey:@"wind"]objectForKey:@"speed"];
                tlak = [dict objectForKey:@"pressure"];
                teplota = [dict objectForKey:@"temp"];
                _imgVitr.transform = CGAffineTransformMakeRotation([[[dict objectForKey:@"wind"]objectForKey:@"deg"]floatValue]);
                mesto = [dict objectForKey:@"name"];
                countryCode = [dict objectForKey:@"country"];
                source = [dict objectForKey:@"source"];
                [self writeNumbers];
                [self setPointerY];
            }
        } else {
            AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            app.hasGIOMWeather = app.hasOPWeather = NO;
            
            MainMenuPVC*menu = app.mainMenuPVC;
            [menu refreshPVC:@"viewwillappear weather view"];
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", [error localizedDescription]);
    }];

}
-(void)writeGiomNumbers{
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.##"];
    _lVlhkost.text = [NSString stringWithFormat:@"%@ %%", [fmt stringFromNumber:vlhkost]];
    _lVitr.text =  [NSString stringWithFormat:@"%@ m/s", [fmt stringFromNumber:vitr]];
    _lTlak.text = [NSString stringWithFormat:@"%@ hPa", [fmt stringFromNumber:tlak]];
    
    _cityLabel.hidden = YES;
    
    _lTeplota.text = [NSString stringWithFormat:@"%@°",[fmt stringFromNumber:teplota]];
    _lPocitovaTeplota.text =[NSString stringWithFormat:@"%@°", [fmt stringFromNumber:[NSNumber numberWithDouble:teplota.doubleValue - 2 ]]];
}

-(void)writeNumbers{
    NSNumberFormatter *fmt = [[NSNumberFormatter alloc] init];
    [fmt setPositiveFormat:@"0.##"];
    
    _lVlhkost.text = [NSString stringWithFormat:@"%@ %%", [fmt stringFromNumber:vlhkost]];
    _lVitr.text =  [NSString stringWithFormat:@"%@ m/s", [fmt stringFromNumber:vitr]];
    _lTlak.text = [NSString stringWithFormat:@"%@ hPa", [fmt stringFromNumber:tlak]];
    
    _cityLabel.hidden = NO;
    _cityLabel.text = [NSString stringWithFormat:@"%@, %@", mesto,countryCode];
    
        _lTeplota.text = [NSString stringWithFormat:@"%@°",[fmt stringFromNumber:[self kelvinToDegrees:teplota]]];
        _lPocitovaTeplota.text =[NSString stringWithFormat:@"%@°", [fmt stringFromNumber:[self kelvinToDegreesFeelin:teplota]]];
}

-(NSNumber*)kelvinToDegrees:(NSNumber*)kelvin{
    double degrees = kelvin.doubleValue -273.15;
    return [NSNumber numberWithDouble:degrees];
}

-(NSNumber*)kelvinToDegreesFeelin:(NSNumber*)kelvin{
    double degrees = kelvin.doubleValue -275.15;
    return [NSNumber numberWithDouble:degrees];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
