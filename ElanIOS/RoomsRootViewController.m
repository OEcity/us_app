//
//  RoomsRootViewController.m
//  Click Smart
//
//  Created by Tom Odler on 21.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "RoomsRootViewController.h"
#import "SYDevicesViewController.h"
#import "BlindsViewController.h"
#import "RGBViewController.h"
#import "DimmingViewController.h"
#import "HCAViewController.h"
#import "ChromaticityViewController.h"
#import "UIViewController+RevealViewControllerAddon.h"
#import "ReleViewController.h"
#import "DimmingViewController.h"
#import "HCAViewController.h"

@interface RoomsRootViewController ()
@property NSUInteger pendingIndex;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControll;
@property (weak, nonatomic) IBOutlet UIView *container;

@end

@implementation RoomsRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
    
    // Do any additional setup after loading the view.
    self.pageViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"RoomsPageController"];
    self.pageViewController.dataSource = self;
    self.pageViewController.delegate = self;
    
    SYDevicesViewController *startingViewController = [self viewControllerAtIndex:_indexForOpen];
    startingViewController.pageIndex = _indexForOpen;
    NSArray *viewControllers = @[startingViewController];
    [self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:_pageViewController];
    [self.view addSubview:_pageViewController.view];
    [self.pageViewController didMoveToParentViewController:self];
    [self.view bringSubviewToFront:_pageControll];
    
    _pageControll.numberOfPages = [_rooms count];
    _pageControll.currentPage = _indexForOpen;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (SYDevicesViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if (([self.rooms count] == 0) || (index >= [self.rooms count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    SYDevicesViewController *pageContentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SYDevicesViewController"];
    pageContentViewController.room = [_rooms objectAtIndex:index];
    pageContentViewController.pageIndex = index;
    
    return pageContentViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = ((SYDevicesViewController*) viewController).pageIndex;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = ((SYDevicesViewController*) viewController).pageIndex;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.rooms count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

-(void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers{
    SYDevicesViewController*vc = (SYDevicesViewController*)pendingViewControllers.firstObject;
    _pendingIndex = vc.pageIndex;
}

-(void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed{
    if(completed){
        
            _pageControll.currentPage = _pendingIndex;
        
    }
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([segue.identifier isEqualToString:@"blindsSegue"]){
        BlindsViewController *controller = (BlindsViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
        
    } else if([segue.identifier isEqualToString:@"RGBSegue"]){
        RGBViewController *controller = (RGBViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
        
    } else if([segue.identifier isEqualToString:@"dimmingSegue"]){
        DimmingViewController *controller = (DimmingViewController*) [segue destinationViewController];
        controller.brightness = [[_segueDevice getStateValueForStateName:@"brightness"] intValue];
        controller.device = _segueDevice;
        
    } else if([segue.identifier isEqualToString:@"HCASegue"]){
        HCAViewController *controller = (HCAViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
        
    } else if ([segue.identifier isEqualToString:@"LightChtomaticitySegue"]){
        ChromaticityViewController *controller = (ChromaticityViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
    } else if ([segue.identifier isEqualToString:@"releSegue"]){
        ReleViewController *controller = (ReleViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
    }
}


@end
