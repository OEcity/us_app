//
//  SYCoreDataManager.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 16.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "SYCoreDataManager.h"
#import "AppDelegate.h"

@interface SYCoreDataManager ()

- (NSManagedObject*)getObjectForEntityName:(NSString*)entityName withID:(NSString*)anID;
- (NSArray*)getAllObjectsForEntitySortByLabel:(NSString *)entityName;

@end


@implementation SYCoreDataManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize privateObjectContext = _privateObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

#pragma Singleton management

+(SYCoreDataManager*) sharedInstance {
    static SYCoreDataManager *_sharedManager = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[SYCoreDataManager alloc] init];
    });
    
    return _sharedManager;
}

- (id)init {
    self = [super init];
    
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(contextDidSave:)
                                                     name:NSManagedObjectContextDidSaveNotification
                                                   object:nil];
    }
    
    return self;
}

#pragma mark - Database management

- (void)saveContext {
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.privateObjectContext;
    if (managedObjectContext != nil) {
        [managedObjectContext setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - General methods for creating and deleting

/**
 Default method for creating NSManagedObjects. They are created on privateObjectContext.
 */
- (NSManagedObject*)createEntityWithName:(NSString*)entityName {
    NSManagedObject* object = [self createEntityWithName:entityName inContext:self.privateObjectContext];

    [self saveContext];

    return object;
}

- (NSManagedObject*)createEntityWithName:(NSString*)entityName inContext:(NSManagedObjectContext*)context {
    if (context == nil){
        NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:[self managedObjectContext]];
        return [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    }
    NSManagedObject* object = [NSEntityDescription
                               insertNewObjectForEntityForName:entityName
                               inManagedObjectContext:context];
    return object;
}

- (void)deleteManagedObject:(NSManagedObject*)object {
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:[object entity]];
    
    NSPredicate *predicate =
    [NSPredicate predicateWithFormat:@"self == %@", object];
    [request setPredicate:predicate];
    
    NSError *error;
    
    NSArray *array = [self.privateObjectContext executeFetchRequest:request error:&error];
    if (array != nil) {
        if ([array count] > 0) {
            [self deleteManagedObject:[array firstObject] inContext:self.privateObjectContext];
            [self saveContext];
        }
    }
    else {
        NSLog(@"Error deleting object: %@",[error localizedDescription]);
    }
    
    
    
    
}

- (void)deleteManagedObject:(NSManagedObject*)object inContext:(NSManagedObjectContext*)context {
    [context deleteObject:object];
}

-(void)deleteObjectsForElan:(Elan*)elan{
    NSMutableArray *objectsToDelete = [NSMutableArray new];
    
    for(Device *device in [self getAllDevices]){
        if([device.elan.mac isEqualToString: elan.mac]){
            [objectsToDelete addObject:device];
        }
    }
    
    for(Room *room in [self getAllRooms]){
        if([elan.type containsString:@"IR"]){
            if([room.roomID isEqualToString:[NSString stringWithFormat:@"roomTitle:%@", elan.mac]]){
                [objectsToDelete addObject:room];
            }
        } else {
            if([room.elan.mac isEqualToString:elan.mac]){
                [objectsToDelete addObject:room];
            }
        }
    }
    
    for(Scene*scene in [self getAllScenes]){
        if([scene.elan.mac isEqualToString: elan.mac]){
            [objectsToDelete addObject:scene];
        }
    }
    
    for(HeatTimeSchedule*schedule in [self getAllHCASchedules]){
        if([schedule.elan.mac isEqualToString:elan.mac]){
            [objectsToDelete addObject:schedule];
        }
    }
    
    for(DeviceTimeScheduleClass*schedule in [self getAllDeviceSchedules]){
        if([schedule.elan.mac isEqualToString:elan.mac]){
            [objectsToDelete addObject:schedule];
        }
    }

    for(NSManagedObject*object in objectsToDelete){
        [self deleteManagedObject:object];
    }
    [self saveContext];
    
}

#pragma mark - Custom methods for managing entities

- (Action*)createEntityAction {
    return (Action*)[self createEntityWithName:@"Action"];
}

- (Action*)createEntityActionInContext:(NSManagedObjectContext*)context {
    return (Action*)[self createEntityWithName:@"Action" inContext:context];
}

- (IRAction*)createEntityIRAction {
    return (IRAction*)[self createEntityWithName:@"IRAction"];
}

- (IRAction*)createEntityIRActionInContext:(NSManagedObjectContext*)context {
    return (IRAction*)[self createEntityWithName:@"IRAction" inContext:context];
}

- (Address*)createEntityAddress {
    return (Address*)[self createEntityWithName:@"Address"];
}

- (Address*)createEntityAddressInContext:(NSManagedObjectContext*)context {
    return (Address*)[self createEntityWithName:@"Address" inContext:context];
}

- (Camera*)createEntityCamera {
    return (Camera*)[self createEntityWithName:@"Camera"];
}

- (Camera*)createEntityCameraInContext:(NSManagedObjectContext*)context {
    return (Camera*)[self createEntityWithName:@"Camera" inContext:context];
}

- (Device*)createEntityDevice {
    return (Device*)[self createEntityWithName:@"Device"];
}

- (Device*)createEntityDeviceInContext:(NSManagedObjectContext*)context {
    return (Device*)[self createEntityWithName:@"Device" inContext:context];
}

- (IRDevice*)createEntityIRDevice {
    return (IRDevice*)[self createEntityWithName:@"IRDevice"];
}

- (IRDevice*)createEntityIRDeviceInContext:(NSManagedObjectContext*)context {
    return (IRDevice*)[self createEntityWithName:@"IRDevice" inContext:context];
}

- (DeviceInRoom*)createEntityDeviceInRoom {
    return (DeviceInRoom*)[self createEntityWithName:@"DeviceInRoom"];
}

- (DeviceInRoom*)createEntityDeviceInRoomInContext:(NSManagedObjectContext*)context {
    return (DeviceInRoom*)[self createEntityWithName:@"DeviceInRoom" inContext:context];
}

- (Elan*)createEntityElan {
    return (Elan*)[self createEntityWithName:@"Elan"];
}

- (Elan*)createEntityElanInContext:(NSManagedObjectContext*)context {
    return (Elan*)[self createEntityWithName:@"Elan" inContext:context];
}

- (HeatCoolArea*)createEntityHeatCoolArea {
    return (HeatCoolArea*)[self createEntityWithName:@"HeatCoolArea"];
}

- (HeatCoolArea*)createEntityHeatCoolAreaInContext:(NSManagedObjectContext*)context {
    return (HeatCoolArea*)[self createEntityWithName:@"HeatCoolArea" inContext:context];
}

- (HeatTimeSchedule*)createEntityHCASchedule {
    return (HeatTimeSchedule*)[self createEntityWithName:@"HeatTimeSchedule"];
}

- (HeatTimeSchedule*)createEntityHCAScheduleInContext:(NSManagedObjectContext*)context {
    return (HeatTimeSchedule*)[self createEntityWithName:@"HeatTimeSchedule" inContext:context];
}

- (DeviceTimeScheduleClass*)createEntityDeviceSchedule {
    return (DeviceTimeScheduleClass*)[self createEntityWithName:@"DeviceTimeScheduleClass"];
}

- (DeviceTimeScheduleClass*)createEntityDeviceScheduleInContext:(NSManagedObjectContext*)context {
    return (DeviceTimeScheduleClass*)[self createEntityWithName:@"DeviceTimeScheduleClass" inContext:context];
}

- (Room*)createEntityRoom {
    return (Room*)[self createEntityWithName:@"Room"];
}

- (Room*)createEntityRoomInContext:(NSManagedObjectContext*)context {
    return (Room*)[self createEntityWithName:@"Room" inContext:context];
}

- (Scene*)createEntityScene {
    return (Scene*)[self createEntityWithName:@"Scene"];
}

- (Scene*)createEntitySceneInContext:(NSManagedObjectContext*)context {
    return (Scene*)[self createEntityWithName:@"Scene" inContext:context];
}

- (SceneAction*)createEntitySceneAction {
    return (SceneAction*)[self createEntityWithName:@"SceneAction"];
}

- (SceneAction*)createEntitySceneActionInContext:(NSManagedObjectContext*)context {
    return (SceneAction*)[self createEntityWithName:@"SceneAction" inContext:context];
}

- (SecondaryAction*)createEntitySecondaryAction {
    return (SecondaryAction*)[self createEntityWithName:@"SecondaryAction"];
}

- (SecondaryAction*)createEntitySecondaryActionInContext:(NSManagedObjectContext*)context {
    return (SecondaryAction*)[self createEntityWithName:@"SecondaryAction" inContext:context];
}

- (State*)createEntityState {
    return (State*)[self createEntityWithName:@"State"];
}

- (State*)createEntityStateInContext:(NSManagedObjectContext*)context {
    return (State*)[self createEntityWithName:@"State" inContext:context];
}

-(IntercomContact*)createEntityIntercomContact{
    return (IntercomContact*)[self createEntityWithName:@"IntercomContact"];
}

-(IntercomContact*)createEntityIntercomContactInContext:(NSManagedObjectContext*)context{
    return (IntercomContact*)[self createEntityWithName:@"IntercomContact" inContext:context];
}





- (void)pairDevice:(Device*)aDevice andRoom:(Room*)aRoom withCoordinates:(struct SYDevicePosition)position {
    if (!aDevice || !aDevice.deviceID || !aRoom || !aRoom.roomID) return;
    aDevice = [[SYCoreDataManager sharedInstance] getDeviceWithID:aDevice.deviceID
                                                        inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
    aRoom = [[SYCoreDataManager sharedInstance] getRoomWithID:aRoom.roomID
                                                    inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"device.deviceID == %@ AND room.roomID == %@", aDevice.deviceID, aRoom.roomID];
    NSArray * devicesInRoom = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"DeviceInRoom" withPredicate:predicate inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
    DeviceInRoom * deviceInRoom = nil;
    if (!devicesInRoom || [devicesInRoom count] != 1){
        
        deviceInRoom = [[SYCoreDataManager sharedInstance] createEntityDeviceInRoomInContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        deviceInRoom.device = aDevice;
        deviceInRoom.room = aRoom;

    }else{
        deviceInRoom = [devicesInRoom objectAtIndex:0];
    }
    
    [deviceInRoom setCoordX:[[NSNumber alloc] initWithDouble:position.coordX]];
    [deviceInRoom setCoordY:[[NSNumber alloc] initWithDouble:position.coordY]];
    [self saveContext];

}

- (void)pairDevice:(Device*)aDevice andRoom:(Room*)aRoom withCoordinates:(struct SYDevicePosition)position inContext:(NSManagedObjectContext *)context {
    if (!aDevice || !aDevice.deviceID || !aRoom || !aRoom.roomID) return;
    aDevice = [[SYCoreDataManager sharedInstance] getDeviceWithID:aDevice.deviceID
                                                        inContext:context];
    aRoom = [[SYCoreDataManager sharedInstance] getRoomWithID:aRoom.roomID
                                                    inContext:context];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"device.deviceID == %@ AND room.roomID == %@", aDevice.deviceID, aRoom.roomID];
    NSArray * devicesInRoom = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"DeviceInRoom" withPredicate:predicate inContext:context];
    DeviceInRoom * deviceInRoom = nil;
    if (!devicesInRoom || [devicesInRoom count] != 1){
        
        deviceInRoom = [[SYCoreDataManager sharedInstance] createEntityDeviceInRoomInContext:context];
        deviceInRoom.device = aDevice;
        deviceInRoom.room = aRoom;
        
    }else{
        deviceInRoom = [devicesInRoom objectAtIndex:0];
    }
    
    [deviceInRoom setCoordX:[[NSNumber alloc] initWithDouble:position.coordX]];
    [deviceInRoom setCoordY:[[NSNumber alloc] initWithDouble:position.coordY]];
    [self saveContext];
    
}


- (void)unpairDevice:(Device*)aDevice andRoom:(Room*)aRoom {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"device.deviceID == %@ AND room.roomID == %@", aDevice.deviceID, aRoom.roomID];
    DeviceInRoom * devicesInRoom = [[[SYCoreDataManager sharedInstance] getObjectsForEntity:@"DeviceInRoom" withPredicate:predicate] firstObject];
    if (devicesInRoom != nil){
        [[SYCoreDataManager sharedInstance] deleteManagedObject:devicesInRoom];
    }
    [self saveContext];
}

- (void)unpairAllDevicesForRoom:(Room*)aRoom{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"room.roomID == %@", aRoom.roomID];
    NSArray * devicesInRoom = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"DeviceInRoom" withPredicate:predicate] ;
    for (DeviceInRoom * deviceInRoom in devicesInRoom) {
        if (devicesInRoom != nil){
            [[SYCoreDataManager sharedInstance] deleteManagedObject:deviceInRoom];
        }
    }
    [self saveContext];
}

-(void)setFavouriteValue:(NSNumber*)favourite forDevice:(NSString*)deviceID{
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"deviceID == %@", deviceID];
    NSArray* array = [self getObjectsForEntity:@"Device" withPredicate:predicate inContext:[self privateObjectContext]];
    if (!array || [array count]!= 1) return;
    Device * dev = [array objectAtIndex:0];
    dev.favourite = favourite;
    [self saveContext];
    
    
}

-(void)deleteAllObjectForEntity:(NSString*)entityName{
    NSFetchRequest * allCars = [[NSFetchRequest alloc] init];
    [allCars setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:[self managedObjectContext]]];
    [allCars setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * cars = [[self managedObjectContext] executeFetchRequest:allCars error:&error];
    //error handling goes here
    for (NSManagedObject * car in cars) {
        [[self managedObjectContext] deleteObject:car];
    }
    NSError *saveError = nil;
    [[self managedObjectContext] save:&saveError];
}

-(void)deleteAllObjects{
    [self deleteAllObjectForEntity:@"Device"];
    [self deleteAllObjectForEntity:@"IRDevice"];
    [self deleteAllObjectForEntity:@"DeviceTimeScheduleClass"];
    [self deleteAllObjectForEntity:@"HeatTimeSchedule"];
    [self deleteAllObjectForEntity:@"Room"];
    [self deleteAllObjectForEntity:@"Scene"];
    [self deleteAllObjectForEntity:@"State"];
    [self deleteAllObjectForEntity:@"SceneAction"];
    //[self deleteAllObjectForEntity:@"Camera"];
}
#pragma mark - Fetching objects from database

- (NSArray*)getObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate*)predicate sortDescriptor:(NSSortDescriptor*)sortDescriptor inContext:(NSManagedObjectContext * )context{
    NSManagedObjectContext* objectContext = [self managedObjectContext];//[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
//    [objectContext setPersistentStoreCoordinator:_persistentStoreCoordinator];
    if (context){
        objectContext = context;
    }
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity =
    [NSEntityDescription entityForName:entityName
                inManagedObjectContext:objectContext];
    [request setEntity:entity];
    if (predicate)
    [request setPredicate:predicate];
    if (sortDescriptor)
    [request setSortDescriptors:@[sortDescriptor]];
    
    NSError *error;
    NSArray *array = [objectContext executeFetchRequest:request error:&error];
    
    if (array != nil) {
        return array;
    } else {
        NSLog(@"Error fetching objects for entity %@: %@",entityName, [error localizedDescription]);
    }
    
    return nil;
}

- (NSArray*)getObjectsForEntity:(NSString *)entityName withPredicate:(NSPredicate *)predicate {

    return [self getObjectsForEntity:entityName withPredicate:predicate sortDescriptor:nil inContext:nil];
}

- (NSArray*)getObjectsForEntity:(NSString *)entityName withPredicate:(NSPredicate *)predicate inContext:(NSManagedObjectContext*)context{
    
    return [self getObjectsForEntity:entityName withPredicate:predicate sortDescriptor:nil inContext:context];
}


- (NSArray*)getAllObjectsForEntity:(NSString *)entityName withSortDescriptor:(NSSortDescriptor*)sortDescriptor {
    return [self getObjectsForEntity:entityName withPredicate:nil sortDescriptor:sortDescriptor inContext:nil];
}

- (NSArray*)getAllObjectsForEntity:(NSString *)entityName {
    return [self getAllObjectsForEntity:entityName withSortDescriptor:nil];
}

- (NSArray*)getAllObjectsForEntitySortByLabel:(NSString *)entityName {
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc]
                                        initWithKey:@"label" ascending:YES];
    return [self getAllObjectsForEntity:entityName withSortDescriptor:sortDescriptor];
}

-(NSArray*)getAllRFElans{
    NSPredicate*predicate = [NSPredicate predicateWithFormat:@"NOT(self.type == 'ELAN-IR') AND (self.selected == 1)"];
    return [self getObjectsForEntity:@"Elan" withPredicate:predicate];
}

-(NSArray*)getAllIRElans{
    NSPredicate*predicate = [NSPredicate predicateWithFormat:@"(self.type == 'ELAN-IR') AND (self.selected == 1)"];
    return [self getObjectsForEntity:@"Elan" withPredicate:predicate];
}

-(NSArray*)getScheduleDevicesForElan:(Elan*)elan{
    NSArray *myProductTypes = [NSArray arrayWithObjects: @"RFSA-11B", @"RFUS-11", @"RFSC-11", @"RFSA-61M", @"RFUS-61", @"RFSC-61", @"RFSAI-61B", @"RFSA-61B", @"RFSA-62B", @"RFSA-66M", @"RFJA-12B", @"RFDA-11B", @"RFDSC-11", @"RFDA-71B", @"RFDSC-71", @"RFDAC-71B", @"RFDEL-71B", @"RFDA-73M/RGB", @"RF-White-LED-675", @"RF-RGB-LED-550",nil];
    

    
    NSPredicate*predicate =[NSPredicate predicateWithFormat:@"(self.elan.mac == %@) AND (self.productType IN %@)", elan.mac, myProductTypes];
    return [self getObjectsForEntity:@"Device" withPredicate:predicate];
    
}

-(NSArray*)getAllDevicesForElan:(Elan*)elan{
    NSPredicate*predicate =[NSPredicate predicateWithFormat:@"(self.elan.mac == %@)", elan.mac];
    return [self getObjectsForEntity:@"Device" withPredicate:predicate];
}

-(NSArray*)getDeviceSchedulesForElan:(Elan*)elan{
    NSPredicate*predicate =[NSPredicate predicateWithFormat:@"(self.elan.mac == %@)", elan.mac];
    return [self getObjectsForEntity:@"DeviceTimeScheduleClass" withPredicate:predicate];
}

- (NSArray*)getAllActions {
    return [self getAllObjectsForEntity:@"Action"];
}

- (NSArray*)getAllIRActions {
    return [self getAllObjectsForEntity:@"IRAction"];
}

- (NSArray*)getAllCameras {
    return [self getAllObjectsForEntitySortByLabel:@"Camera"];
}

- (NSArray*)getAllDevices {
    return [self getAllObjectsForEntitySortByLabel:@"Device"];
}

- (NSArray*)getAllIRDevices {
    return [self getAllObjectsForEntitySortByLabel:@"IRDevice"];
}

- (NSArray*)getAllElans {
    return [self getAllObjectsForEntitySortByLabel:@"Elan"];
}

- (NSArray*)getAllHeatCoolAreas {
    return [self getAllObjectsForEntitySortByLabel:@"HeatCoolArea"];
}

- (NSArray*)getAllRooms {
    return [self getAllObjectsForEntitySortByLabel:@"Room"];
}

- (NSArray*)getAllScenes {
    return [self getAllObjectsForEntitySortByLabel:@"Scene"];
}

- (NSArray*)getAllHCASchedules {
    return [self getAllObjectsForEntitySortByLabel:@"HeatTimeSchedule"];
}

- (NSArray*)getAllDeviceSchedules {
    return [self getAllObjectsForEntitySortByLabel:@"DeviceTimeScheduleClass"];
}

-(NSArray*)getAllIntercomContacts {
    return [self getAllObjectsForEntity:@"IntercomContact"];
}

- (NSArray*)getCurrentElans {
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"self.selected == 1"];
    
    NSArray* elans = [self getObjectsForEntity:@"Elan" withPredicate:predicate];
    
    return elans;
}

- (NSNumber*)getCurrentConfigCounter {
    Elan* current = [[self getCurrentElans]firstObject];
    
    return current.configurationCounter;
}

- (void)setCurrentElan:(Elan*)newElan {
    
    [self deleteAllObjects];
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"self.selected == 1"];
    NSPredicate* predicateNewElan = [NSPredicate predicateWithFormat:@"self == %@",newElan];
    
    NSArray* elans = [self getObjectsForEntity:@"Elan" withPredicate:predicate inContext:[self privateObjectContext]];
    
    for (Elan * elan in elans){
        elan.selected =[[NSNumber alloc] initWithBool:NO];
    }
    
    Elan* dbElan = [[self getObjectsForEntity:@"Elan" withPredicate:predicateNewElan inContext:[self privateObjectContext]] firstObject];
    
    dbElan.selected = [[NSNumber alloc] initWithBool:YES];
    dbElan.configurationCounter = 0;
    
    [self saveContext];
    
}

-(void)setSelectedElan:(Elan*)newElan{
    NSPredicate* predicateNewElan = [NSPredicate predicateWithFormat:@"self == %@",newElan];
    Elan* dbElan = [[self getObjectsForEntity:@"Elan" withPredicate:predicateNewElan inContext:[self privateObjectContext]] firstObject];
    
    dbElan.selected = [[NSNumber alloc] initWithBool:YES];
    dbElan.configurationCounter =[NSNumber numberWithInt:0];
    
    [self saveContext];
}

-(void)setUnselectedElan:(Elan*)elan{
    NSPredicate* predicateNewElan = [NSPredicate predicateWithFormat:@"self == %@",elan];
    Elan* dbElan = [[self getObjectsForEntity:@"Elan" withPredicate:predicateNewElan inContext:[self privateObjectContext]] firstObject];
    
    dbElan.selected = [[NSNumber alloc] initWithBool:NO];
    dbElan.configurationCounter = [NSNumber numberWithInt:0];
    
    [self saveContext];
}

-(void)setElanTypeAndWSAddress:(Elan*)elan type:(NSString*)type wsAddress:(NSString*)address{
    NSPredicate* predicateNewElan = [NSPredicate predicateWithFormat:@"self == %@",elan];
    Elan* dbElan = [[self getObjectsForEntity:@"Elan" withPredicate:predicateNewElan inContext:[self privateObjectContext]] firstObject];
    
    dbElan.type = type;
    dbElan.socketAddress = address;
    
    [self saveContext];
}

- (NSManagedObject*)getObjectForEntityName:(NSString *)entityName withID:(NSString *)anID {
    NSManagedObjectContext* objectContext = [self managedObjectContext];//[[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
//    [objectContext setPersistentStoreCoordinator:_persistentStoreCoordinator];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity =
    [NSEntityDescription entityForName:entityName
                inManagedObjectContext:objectContext];
    [request setEntity:entity];
    
    NSString * predicateString = [NSString stringWithFormat:@"self.%@ID == %%@", entityName.lowercaseString];
    NSPredicate *predicate =
    [NSPredicate predicateWithFormat:predicateString, anID];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [objectContext executeFetchRequest:request error:&error];
    
    NSManagedObject* object = nil;
    
    if (array != nil) {
        NSUInteger count = [array count]; // May be 0 if the object has been deleted.
        if (count == 1) {
            object = [array firstObject];
        }
    } else {
        NSLog(@"Error fetching %@ with ID: %@",entityName,anID);
    }
    
    return object;
}

- (NSManagedObject*)getObjectForEntityName:(NSString *)entityName withID:(NSString *)anID inContext:(NSManagedObjectContext*)context{
    NSManagedObjectContext* objectContext = context;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity =
    [NSEntityDescription entityForName:entityName
                inManagedObjectContext:objectContext];
    [request setEntity:entity];
    
    NSString * predicateString = [NSString stringWithFormat:@"self.%@ID == %%@", entityName.lowercaseString];
    NSPredicate *predicate =
    [NSPredicate predicateWithFormat:predicateString, anID];
    [request setPredicate:predicate];
    
    NSError *error;
    NSArray *array = [objectContext executeFetchRequest:request error:&error];
    
    NSManagedObject* object = nil;
    
    if (array != nil) {
        NSUInteger count = [array count]; // May be 0 if the object has been deleted.
        if (count == 1) {
            object = [array firstObject];
        }
    } else {
        NSLog(@"Error fetching %@ with ID: %@",entityName,anID);
    }
    
    return object;
}


//- (NSManagedObject*)getObjectForEntityName:(NSString *)entityName withName:(NSString *)aName withDeviceID:(NSString*)anID {
//    NSManagedObjectContext* objectContext = [self managedObjectContext];
//    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    NSEntityDescription *entity =
//    [NSEntityDescription entityForName:entityName
//                inManagedObjectContext:objectContext];
//    [request setEntity:entity];
//    
//    NSString * predicateString = [NSString stringWithFormat:@"self.name == %@", aName];
//    NSPredicate * predicate = [NSPredicate predicateWithFormat:predicateString];
//    [request setPredicate:predicate];
//    
//    NSError *error;
//    NSArray *array = [objectContext executeFetchRequest:request error:&error];
//    
//    NSManagedObject* object = nil;
//    
//    if (array != nil) {
//        NSUInteger count = [array count]; // May be 0 if the object has been deleted.
//        if (count == 1) {
//            object = [array firstObject];
//        }
//    } else {
//        NSLog(@"Error fetching %@ with ID: %@",entityName,aName);
//    }
//    
//    return object;
//}


- (Elan*)getCurrentElanInContext:(NSManagedObjectContext*)context{
    NSPredicate* predicate = [NSPredicate predicateWithFormat:@"self.selected == 1"];
    NSArray* elans = [self getObjectsForEntity:@"Elan" withPredicate:predicate inContext:context];
    
    if (elans) {
        if ([elans count] > 1) {
            NSLog(@"Error fetching Elans: More than one Elan selected!");
            return nil;
        } else if ([elans count] < 1){
            NSLog(@"Error fetching Elans: No Elan selected!");
            return nil;
        }
    } else {
        return nil;
    }
    
    return (Elan*)[elans firstObject];
}

- (Elan*)getELANwithMAC:(NSString*)aMacAddress inContext:(NSManagedObjectContext*)context {
    
    NSPredicate* predicate = [NSPredicate predicateWithFormat:[NSString stringWithFormat:@"self.mac == '%@'", aMacAddress]];
    NSArray* elans = [self getObjectsForEntity:@"Elan" withPredicate:predicate inContext:context];
    if (!elans) {
        return nil;
    }
    
    return (Elan*)[elans firstObject];
}

- (Action*)getActionWithName:(NSString*)aName withDeviceID:(NSString*)deviceID inContext:(NSManagedObjectContext*) context{
    NSString * predicateString = [NSString stringWithFormat:@"self.name == '%@' AND self.device.deviceID = '%@'", aName, deviceID];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:predicateString];
    return (Action*)[(NSArray *)[self getObjectsForEntity:@"Action" withPredicate:predicate inContext:context] firstObject];
}

- (IRAction*)getIRActionWithName:(NSString*)aName withDeviceID:(NSString*)deviceID inContext:(NSManagedObjectContext*) context{
    NSString * predicateString = [NSString stringWithFormat:@"self.name == '%@' AND self.device.deviceID = '%@'", aName, deviceID];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:predicateString];
    return (IRAction*)[(NSArray *)[self getObjectsForEntity:@"IRAction" withPredicate:predicate inContext:context] firstObject];
}

- (Device*)getDeviceWithID:(NSString*)anID {
    return (Device*)[self getObjectForEntityName:@"Device" withID:anID];
}

- (Device*)getDeviceWithID:(NSString*)anID inContext:(NSManagedObjectContext*)context {
    return (Device*)[self getObjectForEntityName:@"Device" withID:anID inContext:context];
}

- (IRDevice*)getIRDeviceWithID:(NSString*)anID {
    return (IRDevice*)[self getObjectForEntityName:@"IRDevice" withID:anID];
}

- (IRDevice*)getIRDeviceWithID:(NSString*)anID inContext:(NSManagedObjectContext*)context {
    return (IRDevice*)[self getObjectForEntityName:@"IRDevice" withID:anID inContext:context];
}

- (HeatCoolArea*)getHCADeviceWithID:(NSString*)anID{
    return (HeatCoolArea*)[self getObjectForEntityName:@"HeatCoolArea" withID:anID];
}


- (HeatCoolArea*)getHCADeviceWithID:(NSString*)anID inContext:(NSManagedObjectContext*)context{
    return (HeatCoolArea*)[self getObjectForEntityName:@"HeatCoolArea" withID:anID inContext:context];
}

- (Room*)getRoomWithID:(NSString*)anID {
    return (Room*)[self getObjectForEntityName:@"Room" withID:anID];
}

- (Room*)getRoomWithID:(NSString*)anID inContext:(NSManagedObjectContext *)context {
    return (Room*)[self getObjectForEntityName:@"Room" withID:anID inContext:context];
}


- (Scene*)getSceneWithID:(NSString*)anID inContext:(NSManagedObjectContext*)context {
    return (Scene*)[self getObjectForEntityName:@"Scene" withID:anID inContext:context];
}

- (HeatTimeSchedule*)getScheduleWithID:(NSString*)anID{
    return (HeatTimeSchedule*)[self getObjectForEntityName:@"HeatTimeSchedule" withID:anID];
}

- (HeatTimeSchedule*)getScheduleWithID:(NSString*)anID inContext:(NSManagedObjectContext*)context {
    return (HeatTimeSchedule*)[self getObjectForEntityName:@"HeatTimeSchedule" withID:anID inContext:context];
}

- (DeviceTimeScheduleClass*)getDeviceScheduleWithID:(NSString*)anID{
    return (DeviceTimeScheduleClass*)[self getObjectForEntityName:@"DeviceTimeScheduleClass" withID:anID];
}

- (DeviceTimeScheduleClass*)getDeviceScheduleWithID:(NSString*)anID inContext:(NSManagedObjectContext*)context {
    return (DeviceTimeScheduleClass*)[self getObjectForEntityName:@"DeviceTimeScheduleClass" withID:anID inContext:context];
}

- (SceneAction*)getSceneActionWithSceneID:(NSString*)anID withDeviceID:(NSString*)deviceID withActionName:(NSString*)aName inContext:(NSManagedObjectContext*)context {
    NSString * predicateString = [NSString stringWithFormat:@"self.action.name == '%@' AND self.scene.sceneID = '%@' AND self.device.deviceID = '%@'", aName, anID, deviceID];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:predicateString];
    return (SceneAction*)[(NSArray *)[self getObjectsForEntity:@"SceneAction" withPredicate:predicate inContext:context] firstObject];
}

- (IntercomContact*)getIntercomContact:(NSString*)aName {
    NSString * predicateString = [NSString stringWithFormat:@"sipName == '%@'", aName];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:predicateString];
    return (IntercomContact*)[(NSArray *)[self getObjectsForEntity:@"IntercomContact" withPredicate:predicate] firstObject];
}

- (IntercomContact*)getIntercomContact:(NSString*)aName inContext:(NSManagedObjectContext*) context{
    NSString * predicateString = [NSString stringWithFormat:@"self.name == '%@'", aName];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:predicateString];
    return (IntercomContact*)[(NSArray *)[self getObjectsForEntity:@"IntercomContact" withPredicate:predicate inContext:context] firstObject];
}


#pragma mark - Core Data stack

- (NSManagedObjectContext *)privateObjectContext {
    if (_privateObjectContext != nil) {
        return _privateObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _privateObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [_privateObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    
    
    return _privateObjectContext;
}

- (NSManagedObjectContext *)managedObjectContext {
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CoreDataSourceProject" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (_persistentStoreCoordinator) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Store.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        NSFileManager *fm = [NSFileManager defaultManager];
        
        // Move Incompatible Store
        if ([fm fileExistsAtPath:[storeURL path]]) {
            NSURL *corruptURL = [[self applicationIncompatibleStoresDirectory] URLByAppendingPathComponent:[self nameForIncompatibleStore]];
            
            // Move Corrupt Store
            NSError *errorMoveStore = nil;
            [fm moveItemAtURL:storeURL toURL:corruptURL error:&errorMoveStore];
            
            if (errorMoveStore) {
                NSLog(@"Unable to move corrupt store.");
            }
        }
        
        NSError *errorAddingStore = nil;
        if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&errorAddingStore]) {
            NSLog(@"Unable to create persistent store after recovery. %@, %@", errorAddingStore, errorAddingStore.localizedDescription);
        }
        
        // Show Alert View
//        NSString *title = @"Warning";
//        NSString *applicationName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
//        NSString *message = [NSString stringWithFormat:@"A serious application error occurred while %@ tried to read your data. Please contact support for help.", applicationName];
//        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alertView show];
    }
    return _persistentStoreCoordinator;
}

- (NSURL *)applicationStoresDirectory {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *applicationApplicationSupportDirectory = [[fm URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask] lastObject];
    NSURL *URL = [applicationApplicationSupportDirectory URLByAppendingPathComponent:@"Stores"];
    
    if (![fm fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        [fm createDirectoryAtURL:URL withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error) {
            NSLog(@"Unable to create directory for data stores.");
            
            return nil;
        }
    }
    
    return URL;
}

- (NSURL *)applicationIncompatibleStoresDirectory {
    NSFileManager *fm = [NSFileManager defaultManager];
    NSURL *URL = [[self applicationStoresDirectory] URLByAppendingPathComponent:@"Incompatible"];
    
    if (![fm fileExistsAtPath:[URL path]]) {
        NSError *error = nil;
        [fm createDirectoryAtURL:URL withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error) {
            NSLog(@"Unable to create directory for corrupt data stores.");
            
            return nil;
        }
    }
    
    return URL;
}

- (NSString *)nameForIncompatibleStore {
    // Initialize Date Formatter
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    // Configure Date Formatter
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"yyyy-MM-dd-HH-mm-ss"];
    
    return [NSString stringWithFormat:@"%@.sqlite", [dateFormatter stringFromDate:[NSDate date]]];
}


- (void)contextDidSave:(NSNotification*)notification {
    //NSLog(@"hit store context...");
    @synchronized(self) {
        [self.managedObjectContext performBlock:^{
            [self.managedObjectContext mergeChangesFromContextDidSaveNotification:notification];
            [self.managedObjectContext processPendingChanges];
        }];
    }
}

- (void)deleteAllSceneActions{
    [self deleteAllObjectForEntity:@"SceneAction"];
    [self saveContext];
}


@end
