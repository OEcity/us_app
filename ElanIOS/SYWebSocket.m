//
//  MyWebSocket.m
//  ElanIOS
//
//  Created by Vratislav Zima on 6/5/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//


#import "DeviceDetailResponse.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "SYCoreDataManager.h"
#import "Util.h"
#import "SceneLoader.h"
#import "StateResponse.h"
#import "DevicesResponse.h"
#import "DeviceLoaderSync.h"
#import "SYDataLoader.h"


#import <arpa/inet.h>

#define WEBSOCKET_RECONNECT 120.0


@interface SYWebSocket (){
    NSOperationQueue *operationQueue;
    NSArray *colors;
    int reconnectCount;
}

@property(nonatomic, retain)NSMutableArray*socketArray;
@end


@implementation SYWebSocket;


+(SYWebSocket*) sharedInstance{
    
    static SYWebSocket *_sharedManager = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedManager = [[SYWebSocket alloc] init];
    });
    
    return _sharedManager;
}

@synthesize _webSocket;
@synthesize connected;
@synthesize connecting;
@synthesize reachability;
dispatch_queue_t delegateQueue;


#pragma mark Web Socket
-(void)initWithURL:(NSURL*)url{
    if(!url){
        return;
    }
    
    if(!_socketArray){
        _socketArray = [[NSMutableArray alloc] init];
    }
    
    _webSocket = [[SRWebSocket alloc] initWithURL:url];
    _webSocket.delegate = self;
    [_webSocket open];
    
    [_socketArray addObject:_webSocket];
    NSLog(@"Socket array count: %lu", (unsigned long)_socketArray.count);
}

-(void)disconnect{
    for(SRWebSocket*socket in _socketArray){
        [socket close];
    }
    
    [_socketArray removeAllObjects];
}

-(void)connect{
    for(Elan*elan in [[SYCoreDataManager sharedInstance] getAllElans]){
        
        if(!elan.selected.boolValue || !elan.socketAddress || [elan.socketAddress  isEqual: @""]){
            continue;
        }
        
        NSURL*url = [NSURL URLWithString:elan.socketAddress];
        [self initWithURL:url];
    }
}

-(void)reconnect{
    for(SRWebSocket*socket in _socketArray){
        [socket close];
        socket.delegate = nil;
    }
    [_socketArray removeAllObjects];
    
    [self connect];
}



static void ReachabilityCallback(SCNetworkReachabilityRef target, SCNetworkReachabilityFlags flags, void* info) {
    NSLog(@"reachability changed");
}

#pragma mark Lifecycle
- (id)init
{
    self = [super init];
    if (self)
    {
        operationQueue = [[NSOperationQueue alloc] init];
        
        
        //        delegateQueue = dispatch_queue_create("operationQueue", NULL);
        
        //make sure to use the right url, it must point to your specific web socket endpoint or the handshake will fail
        //create a connect config and set all our info here
        //         WebSocketConnectConfig* config = [WebSocketConnectConfig configWithURLString:[Util getSocketAddress] origin:nil protocols:nil tlsSettings:nil headers:nil verifySecurityKey:YES extensions:nil ];
        //         config.closeTimeout = 15.0;
        //         config.keepAlive = 15.0; //sends a ws ping every 15s to keep socket alive
        
        //         //setup dispatch queue for delegate logic (not required, the websocket will create its own if not supplied)
        //         if (delegateQueue==nil)
        //         delegateQueue = dispatch_queue_create("myWebSocketQueue", NULL);
        
        //open using the connect config, it will be populated with server info, such as selected protocol/etc
        //         _webSocket = [WebSocket webSocketWithConfig:config queue:delegateQueue delegate:self];
        
        //connected=NO;
        // _webSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:[Util getSocketAddress]]];
        //[_webSocket setDelegate:self];
        //[_webSocket open];
        //release queue, it is retained by the web socket
    }
    return self;
}


-(void)webSocketDidOpen:(SRWebSocket *)webSocket{
    NSLog(@"Websocket Connected!");
}

-(void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error{
    NSLog(@":( Websocket Failed With Error %@", error);
}

-(void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message{
    NSString* aMessage = (NSString*)message;
    NSLog(@"Did receive message: %@", aMessage);
    
    
    NSURL* url = [NSURL URLWithString:aMessage];
    // Not valid url, skip processing
    if (!url) {
        return;
    }
    
    NSURL *receivedUrl = [NSURL URLWithString:message]; 
    
    Elan*myElan = nil;
    for(Elan*elan in [[SYCoreDataManager sharedInstance] getAllElans]){
        if([elan.baseAddress containsString:[receivedUrl host]]){
            myElan = elan;
        }
    }
    
    if(!myElan || !myElan.selected.boolValue)return;
    
    
    if ([aMessage hasSuffix:@"devices/"] || [aMessage hasSuffix:@"devices"]){
        NSDictionary *argumentDictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"allDevices", @"type", nil];
        [self performSelectorOnMainThread:@selector(startLoading:) withObject:argumentDictionary waitUntilDone:NO];
        
        [[SYDataLoader sharedInstance] loadDevicesForElan:myElan];
    } else if ([aMessage hasSuffix:@"rooms/"] || [aMessage hasSuffix:@"rooms"]){
        [[SYDataLoader sharedInstance] loadRoomsFromElan:myElan];
    } else if ([aMessage hasSuffix:@"scenes/"] || [aMessage hasSuffix:@"scenes"]){
        [[SYDataLoader sharedInstance] loadScenesFromElan:myElan];
    } else if([aMessage hasSuffix:@"weather/"] || [aMessage hasSuffix:@"weather"]){
        [[SYDataLoader sharedInstance] loadWeatherFromElan:myElan];
    } else if([aMessage hasSuffix:@"automat/"] || [aMessage hasSuffix:@"automat"]){
        [[SYDataLoader sharedInstance] loadDeviceSchedulesFromElan:myElan];
    } else {
        NSArray *array = [aMessage componentsSeparatedByString:@"/"];
        NSString *type = array[array.count-2];
        NSString *id = array[array.count-1];
        if ([type isEqual:@"devices"]){
            [[SYDataLoader sharedInstance] updateStateForDeviceID:id fromElan:myElan];
        } else if ([type isEqual:@"rooms"]){
            [[SYDataLoader sharedInstance] loadRoomsFromElan:myElan];
        } else if ([type isEqual:@"scenes"]){
            [[SYDataLoader sharedInstance] loadScenesFromElan:myElan];
        } else if([type isEqualToString:@"automat"]){
            [[SYDataLoader sharedInstance] loadDeviceSchedulesFromElan:myElan];
        }
    }
}

-(void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean{
    NSLog(@"Websocket closed with code %ld, reason %@, wasClean %hhd", (long)code, reason, wasClean);

}


#pragma mark - SRWebSocketDelegate

//- (void)webSocketDidOpen:(SRWebSocket *)webSocket;
//{
//    NSLog(@"Websocket Connected!");
//    connected=YES;
//    connecting = NO;
////    if (_timer != nil)
////    {
////        [_timer invalidate];
////        _timer = nil;
////    }
////    _timer = [NSTimer scheduledTimerWithTimeInterval:WEBSOCKET_RECONNECT
////                                              target:self
////                                            selector:@selector(reconnect)
////                                            userInfo:nil
////                                             repeats:YES];
//}
//
//- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
//{
//    NSLog(@":( Websocket Failed With Error %@", error);
////    if (_timer != nil)
////    {
////        [_timer invalidate];
////        _timer = nil;
////    }
//    connected=NO;
//    connecting=NO;
//    _webSocket = nil;
//    
//    [self performSelector:@selector(connect) withObject:nil afterDelay:1.0];
//}
//
//
//- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
//{
//    NSString* aMessage = (NSString*)message;
//    NSLog(@"Did receive message: %@", aMessage);
//    
//    
//    NSURL* url = [NSURL URLWithString:aMessage];
//    // Not valid url, skip processing
//    if (!url) {
//        return;
//    }
//    
//    if ([aMessage hasSuffix:@"devices/"] || [aMessage hasSuffix:@"devices"]){
//        NSDictionary *argumentDictionary = [NSDictionary dictionaryWithObjectsAndKeys:@"allDevices", @"type", nil];
//        [self performSelectorOnMainThread:@selector(startLoading:) withObject:argumentDictionary waitUntilDone:NO];
//        [[SYDataLoader sharedInstance] loadDevices];
//    } else if ([aMessage hasSuffix:@"rooms/"] || [aMessage hasSuffix:@"rooms"]){
//        [[SYDataLoader sharedInstance] loadRooms];
//    } else if ([aMessage hasSuffix:@"scenes/"] || [aMessage hasSuffix:@"scenes"]){
//        [[SYDataLoader sharedInstance] loadScenes];
//    } else {
//        NSArray *array = [aMessage componentsSeparatedByString:@"/"];
//        NSString *type = array[array.count-2];
//        NSString *id = array[array.count-1];
//        if ([type isEqual:@"devices"]){
//            [[SYDataLoader sharedInstance] updateStateForDeviceID:id];
//        } else if ([type isEqual:@"rooms"]){
//            [[SYDataLoader sharedInstance] loadRooms];
//        } else if ([type isEqual:@"scenes"]){
//            [[SYDataLoader sharedInstance] loadScenes];
//        }
//    }
//}
//
//-(void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean{
//    NSLog(@"Websocket closed with code %ld, reason %@, wasClean %hhd", (long)code, reason, wasClean);
////    if (_timer != nil)
////    {
////        [_timer invalidate];
////        _timer = nil;
////    }
//    if (wasClean==NO){
//        connected=NO;
//        [self connect];
//    }
//    connecting=NO;
//}

-(void)startLoading:(NSDictionary*)object{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingStarted" object:self userInfo:object];
}


@end
