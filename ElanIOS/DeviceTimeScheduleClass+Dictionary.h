//
//  DeviceTimeScheduleClass+Dictionary.h
//  Click Smart
//
//  Created by Tom Odler on 24.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "DeviceTimeScheduleClass.h"
#import "SYSerializeProtocol.h"

@interface DeviceTimeScheduleClass (Dictionary) <SYSerializeProtocol>

@end
