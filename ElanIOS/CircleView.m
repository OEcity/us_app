//
//  CircleView.m
//  iHC-MIRF
//
//  Created by Tom Odler on 29.03.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "CircleView.h"

@implementation CircleView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.*/

- (void)drawRect:(CGRect)rect {
    // Drawing code
    CGContextRef ctx = UIGraphicsGetCurrentContext();
       CGContextAddArc(ctx,
                    self.frame.size.width/2,
                    0,
                    103,
                    0.5,
                    7,
                    1);
    if(_circleColor != nil){
//    CGFloat *mycomponents = CGColorGetComponents(_circleColor.CGColor);
//    NSLog(@"C: %f", mycomponents[0]);
//    NSLog(@"C: %f", mycomponents[1]);
//    NSLog(@"C: %f", mycomponents[2]);
    }
    
    
    CGContextSetFillColor(ctx, CGColorGetComponents([_circleColor CGColor]));
    CGContextFillPath(ctx);

}




@end
