//
//  LinphoneManager.m
//  iHC
//
//  Created by Pavel Gajdoš on 06.09.13.
//  Copyright (c) 2013 Pavel Gajdoš. All rights reserved.
//

#import "LinphoneManager.h"

#include "linphone/linphonecore_utils.h"

static LinphoneCore* theLinphoneCore = nil;
static LinphoneManager* theLinphoneManager = nil;

const char *const LINPHONERC_APPLICATION_KEY = "app";

NSString *const kLinphoneCoreUpdate = @"LinphoneCoreUpdate";
NSString *const kLinphoneDisplayStatusUpdate = @"LinphoneDisplayStatusUpdate";
NSString *const kLinphoneTextReceived = @"LinphoneTextReceived";
NSString *const kLinphoneCallUpdate = @"LinphoneCallUpdate";
NSString *const kLinphoneRegistrationUpdate = @"LinphoneRegistrationUpdate";
NSString *const kLinphoneAddressBookUpdate = @"LinphoneAddressBookUpdate";
NSString *const kLinphoneMainViewChange = @"LinphoneMainViewChange";
NSString *const kLinphoneLogsUpdate = @"LinphoneLogsUpdate";
NSString *const kLinphoneSettingsUpdate = @"LinphoneSettingsUpdate";
NSString *const kLinphoneBluetoothAvailabilityUpdate = @"LinphoneBluetoothAvailabilityUpdate";


extern void libmsilbc_init();
#ifdef HAVE_AMR
extern void libmsamr_init();
#endif

#ifdef HAVE_X264
extern void libmsx264_init();
#endif

#if defined (HAVE_SILK)
extern void libmssilk_init();
#endif

#if HAVE_G729
extern  void libmsbcg729_init();
#endif



@implementation LinphoneManager

@synthesize speakerEnabled;

+ (LinphoneManager *)instance
{
    if (theLinphoneManager == nil) {
        theLinphoneManager = [[LinphoneManager alloc] init];
    }
    return theLinphoneManager;
}

#pragma mark - Linphone Core Functions

+ (LinphoneCore *)getLinphoneCore
{
	if (theLinphoneCore==nil) {
		@throw([NSException exceptionWithName:@"LinphoneCoreException" reason:@"Linphone core not initialized yet" userInfo:nil]);
	}
	return theLinphoneCore;
}

+ (BOOL)isLinphoneCoreReady
{
    return theLinphoneCore != nil;
}

#pragma mark - Call State functions

- (void)onCall:(LinphoneCall*)call StateChanged:(LinphoneCallState)state withMessage:(const char *)message {
    
    
    NSString * address = @"user name";
    
	if (state == LinphoneCallIncomingReceived) {
        
		/*first step is to re-enable ctcall center*/
		CTCallCenter* lCTCallCenter = [[CTCallCenter alloc] init];
		
		/*should we reject this call ?*/
		if ([lCTCallCenter currentCalls]!=nil) {

			linphone_core_decline_call(theLinphoneCore, call,LinphoneReasonBusy);

			return;
		}
		
		if(	[[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)]
		   && [UIApplication sharedApplication].applicationState != UIApplicationStateActive) {
			
			LinphoneCallLog* callLog=linphone_call_get_call_log(call);
			NSString* callId=[NSString stringWithUTF8String:linphone_call_log_get_call_id(callLog)];
			
            NSLog(@"creating notification..");
				// Create a new local notification
				UILocalNotification * notification = [[UILocalNotification alloc] init];
				if (notification) {
					notification.repeatInterval = 0;
					notification.alertBody =[NSString  stringWithFormat:NSLocalizedString(@"Incoming call from %@",nil), address];
					notification.alertAction = NSLocalizedString(@"Answer", nil);
					notification.soundName = @"ring.caf";
					notification.userInfo = [NSDictionary dictionaryWithObject:callId forKey:@"callId"];
					
					[[UIApplication sharedApplication] presentLocalNotificationNow:notification];
					
					if (!incallBgTask){
						incallBgTask = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:^{}];
					}
	
				}
 
			
		}
	}
    
    // Disable speaker when no more call
    if ((state == LinphoneCallEnd || state == LinphoneCallError)) {
        if(linphone_core_get_calls_nb(theLinphoneCore) == 0) {

            /*IOS specific*/
            linphone_core_start_dtmf_stream(theLinphoneCore);
		}
		if (incallBgTask) {
			[[UIApplication sharedApplication]  endBackgroundTask:incallBgTask];
			incallBgTask=0;
		}
        
        /*if(data != nil && data->notification != nil) {
            LinphoneCallLog *log = linphone_call_get_call_log(call);
            
            // cancel local notif if needed
            [[UIApplication sharedApplication] cancelLocalNotification:data->notification];
            [data->notification release];
            data->notification = nil;
            
            if(log == NULL || linphone_call_log_get_status(log) == LinphoneCallMissed) {
                UILocalNotification *notification = [[UILocalNotification alloc] init];
                notification.repeatInterval = 0;
                notification.alertBody = [NSString stringWithFormat:NSLocalizedString(@"You miss %@ call", nil), address];
                notification.alertAction = NSLocalizedString(@"Show", nil);
                notification.userInfo = [NSDictionary dictionaryWithObject:[NSString stringWithUTF8String:linphone_call_log_get_call_id(log)] forKey:@"callLog"];
                [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
                [notification release];
            }
            
        }*/
    }
    
	if(state == LinphoneCallReleased) {
/*        if(data != NULL) {
            [data release];
            linphone_call_set_user_pointer(call, NULL);
        }
 */
    }

    if (state == LinphoneCallConnected && !mCallCenter) {
		/*only register CT call center CB for connected call*/
//		[self setupGSMInteraction];
	}
    // Post event
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSValue valueWithPointer:call], @"call",
                          [NSNumber numberWithInt:state], @"state",
                          [NSString stringWithUTF8String:message], @"message", nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLinphoneCallUpdate object:self userInfo:dict];
}


static void linphone_iphone_call_state(LinphoneCore *lc, LinphoneCall* call, LinphoneCallState state,const char* message) {
	[(__bridge LinphoneManager*)linphone_core_get_user_data(lc) onCall:call StateChanged: state withMessage:  message];
}

#pragma mark - Registration state functions

- (void)onRegister:(LinphoneCore *)lc cfg:(LinphoneProxyConfig*) cfg state:(LinphoneRegistrationState) state message:(const char*) message {
    
    // Post event
    NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:
                          [NSNumber numberWithInt:state], @"state",
                          [NSValue valueWithPointer:cfg], @"cfg",
                          [NSString stringWithUTF8String:message], @"message",
                          nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLinphoneRegistrationUpdate object:self userInfo:dict];
}

static void linphone_iphone_registration_state(LinphoneCore *lc, LinphoneProxyConfig* cfg, LinphoneRegistrationState state,const char* message) {
	[(__bridge LinphoneManager*)linphone_core_get_user_data(lc) onRegister:lc cfg:cfg state:state message:message];
}



#pragma mark - basic functions

- (void)iterate
{
    linphone_core_iterate(theLinphoneCore);
}

- (void)startLibLinphone {
    if (theLinphoneCore != nil) {
        return;
    }
	
	//get default config from bundle
//	NSString* factoryConfig = [LinphoneManager bundleFile:[LinphoneManager runningOnIpad]?@"linphonerc-factory~ipad":@"linphonerc-factory"];
//	NSString *confiFileName = [LinphoneManager documentFile:@".linphonerc"];
//	NSString *zrtpSecretsFileName = [LinphoneManager documentFile:@"zrtp_secrets"];
//	const char* lRootCa = [[LinphoneManager bundleFile:@"rootca.pem"] cStringUsingEncoding:[NSString defaultCStringEncoding]];
//	connectivity = none;
//	signal(SIGPIPE, SIG_IGN);
	//log management
	
    
    NSString* factoryConfig = [LinphoneManager bundleFile:@"linphonerc-factory"];
    NSString *confiFileName = [LinphoneManager documentFile:@".linphonerc"];
	NSString *zrtpSecretsFileName = [LinphoneManager documentFile:@"zrtp_secrets"];
	const char* lRootCa = [[LinphoneManager bundleFile:@"rootca.pem"] cStringUsingEncoding:[NSString defaultCStringEncoding]];

    
    
   // libmsilbc_init();
#if defined (HAVE_SILK)
    libmssilk_init();mi
#endif
#ifdef HAVE_AMR
    libmsamr_init(); //load amr plugin if present from the liblinphone sdk
#endif
#ifdef HAVE_X264
	libmsx264_init(); //load x264 plugin if present from the liblinphone sdk
#endif
    
#if HAVE_G729
	libmsbcg729_init(); // load g729 plugin
#endif
    
	/* Initialize linphone core*/
    
	theLinphoneCore = linphone_core_new(&linphonec_vtable, [confiFileName cStringUsingEncoding:[NSString defaultCStringEncoding]], [factoryConfig cStringUsingEncoding:[NSString defaultCStringEncoding]], (__bridge void *)(self));
  
//    theLinphoneCore = linphone_core_new(&linphonec_vtable, NULL, NULL, (__bridge void *)(self));
    

	
    
    //linphone_core_enable_keep_alive(theLinphoneCore, TRUE);
    
//    int timeout = linphone_core_get_in_call_timeout(theLinphoneCore);
    
    
	linphone_core_set_user_agent(theLinphoneCore ,"iHC-ELKO",
                                 [[[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString*)kCFBundleVersionKey] UTF8String]);
    
    linphone_core_set_root_ca(theLinphoneCore, lRootCa);

	// Set audio assets
	const char* lRing = [[LinphoneManager bundleFile:@"ring.wav"] cStringUsingEncoding:[NSString defaultCStringEncoding]];
	linphone_core_set_ring(theLinphoneCore, lRing);
	const char* lRingBack = [[LinphoneManager bundleFile:@"ringback.wav"] cStringUsingEncoding:[NSString defaultCStringEncoding]];
	linphone_core_set_ringback(theLinphoneCore, lRingBack);
    const char* lPlay = [[LinphoneManager bundleFile:@"hold.wav"] cStringUsingEncoding:[NSString defaultCStringEncoding]];
	linphone_core_set_play_file(theLinphoneCore, lPlay);
	
    linphone_core_set_zrtp_secrets_file(theLinphoneCore, [zrtpSecretsFileName cStringUsingEncoding:[NSString defaultCStringEncoding]]);

    
   //[self setupNetworkReachabilityCallback];
	
	// start scheduler
	iterateTimer = [NSTimer scheduledTimerWithTimeInterval:0.02
													 target:self
												   selector:@selector(iterate)
												   userInfo:nil
													repeats:YES];

	//init audio session
	AVAudioSession *audioSession = [AVAudioSession sharedInstance];
	BOOL bAudioInputAvailable = [audioSession isInputAvailable];

    // TODO zaregistrovat notifications pro delegate
    //    [audioSession setDelegate:self];
    
/*    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(beginInterruption)
                                                 name:
                                               object:nil];*/
	
	NSError* err;
	[audioSession setActive:NO error: &err];
	if(!bAudioInputAvailable){
		UIAlertView* error = [[UIAlertView alloc]	initWithTitle:NSLocalizedString(@"No microphone",nil)
														message:NSLocalizedString(@"You need to plug a microphone to your device to use this application.",nil)
													   delegate:nil
											  cancelButtonTitle:NSLocalizedString(@"Ok",nil)
											  otherButtonTitles:nil ,nil];
		[error show];
	}
    
   // NSUInteger cpucount = [[NSProcessInfo processInfo] processorCount];
	//ms_set_cpu_count(cpucount);
    
    [self authenticate];

    if ([[UIDevice currentDevice] respondsToSelector:@selector(isMultitaskingSupported)]
		&& [UIApplication sharedApplication].applicationState ==  UIApplicationStateBackground) {
		//go directly to bg mode
		[self resignActive];
	}
    
    // Post event
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSValue valueWithPointer:theLinphoneCore] forKey:@"core"];
    [[NSNotificationCenter defaultCenter] postNotificationName:kLinphoneCoreUpdate object:[LinphoneManager instance] userInfo:dict];
}

- (void)destroyLibLinphone {
	[iterateTimer invalidate];

	//just in case
	//[self removeCTCallCenterCb];
	   
	if (theLinphoneCore != nil) { //just in case application terminate before linphone core initialization
    
		linphone_core_destroy(theLinphoneCore);
		theLinphoneCore = nil;
        
        // Post event
        NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSValue valueWithPointer:theLinphoneCore] forKey:@"core"];
        [[NSNotificationCenter defaultCenter] postNotificationName:kLinphoneCoreUpdate object:[LinphoneManager instance] userInfo:dict];
    }
}

- (BOOL)enterBackgroundMode
{
    LinphoneProxyConfig* proxyCfg;
	//linphone_core_get_default_proxy(theLinphoneCore, &proxyCfg);
    proxyCfg = linphone_core_get_default_proxy_config(theLinphoneCore);
	
	
	/*if ((proxyCfg || linphone_core_get_calls_nb(theLinphoneCore) > 0)) {
        
        if(proxyCfg != NULL) {
            //For registration register
            [self refreshRegisters];
            //wait for registration answer
            int i=0;
            while (!linphone_proxy_config_is_registered(proxyCfg) && i++<40 ) {
                linphone_core_iterate(theLinphoneCore);
                usleep(100000);
            }
        }
		//register keepaliv
		BOOL backgroundHandlerRegistered =
        [[UIApplication sharedApplication] setKeepAliveTimeout:600//(NSTimeInterval)linphone_proxy_config_get_expires(proxyCfg)
														   handler:^{
															   if (theLinphoneCore == nil) {
                                                                   // deactivated linphone core
																   return;
															   }
															   //kick up network cnx, just in case
															   [self refreshRegisters];
                                                               [self iterate];
                                                               
														   }
         ];*/
    
    //First refresh registration
    linphone_core_refresh_registers(theLinphoneCore);
    //wait for registration answer
    int i=0;
    while (!linphone_proxy_config_is_registered(proxyCfg) && i++<40 ) {
        linphone_core_iterate(theLinphoneCore);
        usleep(100000);
    }
    
    
    //register keepalive handler
    BOOL backgroundHandlerRegistered =[[UIApplication sharedApplication] setKeepAliveTimeout:600/*minimal interval is 600 s*/
                                                   handler:^{
                                                       //refresh sip registration
                                                       linphone_core_refresh_registers(theLinphoneCore);
                                                       //make sure sip REGISTER is sent
                                                       linphone_core_iterate(theLinphoneCore);
                                                   }];

    
        if (backgroundHandlerRegistered) {
            NSLog(@"background handler registered");
            return YES;
        }
        else {
            NSLog(@"background handler NOT registered");
            return NO;
        }

    if ([UIApplication sharedApplication].applicationState !=  UIApplicationStateActive) {
        // Create a new notification
        UILocalNotification* notif = [[UILocalNotification alloc] init];
        if (notif) {
            notif.repeatInterval = 0;
            notif.alertBody =@"New incoming call";
            notif.alertAction = @"Answer";
            notif.soundName = @"oldphone-mono-30s.caf";
            
            [[UIApplication sharedApplication]  presentLocalNotificationNow:notif];
        }
    }
    
}

- (void)becomeActive
{
    [self refreshRegisters];
    if (pausedCallBgTask) {
		[[UIApplication sharedApplication]  endBackgroundTask:pausedCallBgTask];
		pausedCallBgTask=0;
	}
    if (incallBgTask) {
		[[UIApplication sharedApplication]  endBackgroundTask:incallBgTask];
		incallBgTask=0;
	}
	
	/*IOS specific*/
	linphone_core_start_dtmf_stream(theLinphoneCore);

}

- (BOOL)resignActive {
	linphone_core_stop_dtmf_stream(theLinphoneCore);
    
    return YES;
}

- (void)refreshRegisters
{
    if(!theLinphoneCore) [self startLibLinphone];
   linphone_core_refresh_registers(theLinphoneCore);//just to make sure REGISTRATION is up to date
}

#pragma mark - Auto accept functions

- (void)acceptCallForCallId:(NSString*)callid {
    //first, make sure this callid is not already involved in a call
	if ([LinphoneManager isLinphoneCoreReady]) {
		MSList* calls = (MSList*)linphone_core_get_calls(theLinphoneCore);
        MSList* call = ms_list_find_custom(calls, (MSCompareFunc)comp_call_id, [callid UTF8String]);
		if (call != NULL) {
            [self acceptCall:(LinphoneCall*)call->data];
			return;
		};
	}
}

static int comp_call_id(const LinphoneCall* call , const char *callid) {
	return strcmp(linphone_call_log_get_call_id(linphone_call_get_call_log(call)), callid);
}


#pragma mark - Authentication

- (void)authenticate
{
    NSDictionary * defaults = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    
    NSString * username = defaults[@"voipUsername"];
    NSString * password = defaults[@"voipPassword"];
    
    if ([username length] == 0 || [password length] == 0) {
        return;
    }
    
    const char * sipAddress = [[LinphoneManager sipAddressWithUsername:username] cStringUsingEncoding:NSUTF8StringEncoding];
    
    linphone_core_clear_all_auth_info(theLinphoneCore);
    
    LinphoneProxyConfig * proxy_cfg = linphone_proxy_config_new();
    
    LinphoneAddress * from = linphone_address_new(sipAddress);
    
    if(from == nil)
        return;
    
    LinphoneAuthInfo * info = linphone_auth_info_new(linphone_address_get_username(from), NULL, [password cStringUsingEncoding:NSUTF8StringEncoding], NULL, NULL,NULL);
    
    linphone_core_add_auth_info(theLinphoneCore, info);
    
    linphone_proxy_config_set_identity(proxy_cfg,sipAddress); /*set identity with user name and domain*/
    const char* server_addr = linphone_address_get_domain(from); /*extract domain address from identity*/
    linphone_proxy_config_set_server_addr(proxy_cfg,server_addr); /* we assume domain = proxy server address*/
    linphone_proxy_config_enable_register(proxy_cfg,TRUE); /*activate registration for this proxy config*/
    linphone_address_destroy(from); /*release resource*/
    
    linphone_core_add_proxy_config(theLinphoneCore,proxy_cfg); /*add proxy config to linphone core*/
    linphone_core_set_default_proxy(theLinphoneCore,proxy_cfg); /*set to default proxy*/
}


#pragma mark - Manage Call functions

- (void)acceptCall:(LinphoneCall *)call
{
    linphone_core_accept_call(theLinphoneCore, call);
}

- (void)declineCall:(LinphoneCall *)call
{
    linphone_core_decline_call(theLinphoneCore, call, LinphoneReasonDeclined);
}

- (void)endCall:(LinphoneCall *)call
{
    linphone_core_terminate_call(theLinphoneCore, call);
}

- (void)call:(NSString *)address
{
    linphone_core_invite(theLinphoneCore, [address cStringUsingEncoding:NSUTF8StringEncoding]);
}

#pragma mark - Other functions

- (void)sendStringUsingDTMF:(NSString *)string
{
    if (![LinphoneManager checkUnlockCode:string])
        return;
    
    const char * c_string = [string cStringUsingEncoding:NSUTF8StringEncoding];
    
    linphone_core_set_use_info_for_dtmf(theLinphoneCore, YES);
    linphone_core_set_use_rfc2833_for_dtmf(theLinphoneCore, NO);
    
    for (int i=0; i<[string length]; i++) {
        
        linphone_call_send_dtmf(linphone_core_get_current_call(theLinphoneCore), c_string[i]);
    }
}

+ (BOOL)checkUnlockCode:(id)object
{
    return ([object isKindOfClass:[NSString class]] && [object respondsToSelector:@selector(length)]);
}

- (BOOL)isSpeakerEnabled
{
    return speakerEnabled;
}

- (void)enableSpeaker:(BOOL)enable
{
    static float lastVolume;
    
    //
  /*  MPVolumeView * volumeView = [MPVolumeView new];
    
    UISlider *volumeViewSlider;
    
    for (UIView *view in [volumeView subviews]){
        if ([[[view class] description] isEqualToString:@"MPVolumeSlider"]) {
            volumeViewSlider = (UISlider *) view;
        }
    }*/
    
    //
    
    speakerEnabled = enable;
    
    /*if (enable) {
        
        if (!lastVolume) {
            lastVolume = 100;
        }
        
        [volumeViewSlider setValue:lastVolume];
    }
    else {
        lastVolume = [volumeViewSlider value];
        [volumeViewSlider setValue:0];
    }*/
    
    
   if(enable) {
        UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_Speaker;
        AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute
                                 , sizeof (audioRouteOverride)
                                 , &audioRouteOverride);
    } else {
        UInt32 audioRouteOverride = kAudioSessionOverrideAudioRoute_None;
        AudioSessionSetProperty (kAudioSessionProperty_OverrideAudioRoute
                                 , sizeof (audioRouteOverride)
                                 , &audioRouteOverride);
    }
}


#pragma mark - Linphone Core VTable

static LinphoneCoreVTable linphonec_vtable = {
	.show = NULL,
	.call_state_changed = (LinphoneCoreCallStateChangedCb)linphone_iphone_call_state,
	.registration_state_changed = linphone_iphone_registration_state,
	.notify_presence_received = NULL,
	.new_subscription_requested = NULL,
	.auth_info_requested = NULL,
	.display_status = NULL, //linphone_iphone_display_status,
	.display_message= NULL,//linphone_iphone_log,
	.display_warning= NULL,//linphone_iphone_log,
	.display_url= NULL,
	.text_received= NULL,
	.message_received= NULL,//linphone_iphone_message_received,
	.dtmf_received= NULL,
    .transfer_state_changed= NULL,//linphone_iphone_transfer_state_changed
};


#pragma mark - Utils


+ (NSString *)sipAddressWithUsername:(NSString *)username
{
    NSDictionary * defaults = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
    
    NSString * server = defaults[@"voipServer"];
    NSNumber * port = @5060;
    
    if ([server length] == 0) {
        return @"";
    }
    
    NSString * address = [NSString stringWithFormat:@"sip:%@@%@:%@", username, server, port];
    
    return address;
}

+ (NSString*)bundleFile:(NSString*)file {
    return [[NSBundle mainBundle] pathForResource:[file stringByDeletingPathExtension] ofType:[file pathExtension]];
}

+ (NSString*)documentFile:(NSString*)file {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    return [documentsPath stringByAppendingPathComponent:file];
}


@end
