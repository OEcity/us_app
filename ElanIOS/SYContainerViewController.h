//
//  SYContainerViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/21/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FloorPlanLoader.h"
#import "Device.h"
#import "Room.h"
#import "Floorplan.h"
@interface SYContainerViewController : UIViewController
- (void)setViewController:(NSString*)identifier;
@property (nonatomic, retain) FloorPlanLoader * loader;
@property (nonatomic, retain) UIImage * floorPlanImageData;
@property (nonatomic, retain) NSMutableArray * devicesForRoom;
@property (nonatomic, retain) Device * device;
@property (nonatomic, retain) Room * room;
@property (nonatomic, retain) Floorplan * floorplan;

@end
