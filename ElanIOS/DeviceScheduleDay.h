//
//  DeviceScheduleDay.h
//  iHC-MIRF
//
//  Created by Tom Odler on 16.03.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TempDayMode.h"

@interface DeviceScheduleDay : NSObject<NSCopying>
@property (nonatomic, copy) NSString * id;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSMutableArray * modes;

-(id)initWithNameAndType:(NSString *)name type:(NSString*)type;
-(void)setupModes;
-(void) recountModes:(BOOL)isLeftDirection modePosition:(int) mModePosition ;
-(void) redefineModes;
-(int) joinAndDeleteModes:(int)modePosition;
-(int) editMode:(TempDayMode*) edit modePosition:(int) modePosition;
-(int) deleteMode:(NSInteger) result type:(NSString*)type;
- (id)copyWithZone:(NSZone *)zone;

@end
