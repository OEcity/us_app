//
//  CustomCell.h
//  Click Smart
//
//  Created by Tom Odler on 26.07.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Room.h"
#import "SYConfigureRoomsViewController.h"
#import "HUDWrapper.h"

@interface CustomCell : UITableViewCell<UITableViewDelegate, UITableViewDataSource>
@property (nonatomic) NSMutableArray*dataArray;
@property (nonatomic) UITableView *tableView;
@property (nonatomic) Room *room;
@property (nonatomic, retain) NSMutableArray * added;
@property (nonatomic) SYConfigureRoomsViewController*parentController;




-(void) getRoomInCell;
-(void)saveRoomWithDevices;

@end
