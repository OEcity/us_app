//
//  SYCoreDataManager.h
//  iHC-MIRF
//
//  Created by Marek Žehra on 16.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CoreDataObjects.h"
#import "Address.h"
#import "Scene.h"

struct SYDevicePosition {
    double coordX;
    double coordY;
};

@interface SYCoreDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectContext *privateObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

#pragma Singleton management

+(SYCoreDataManager*) sharedInstance;

#pragma mark - Database management

- (void)saveContext;

#pragma mark - General methods for creating and deleting

/**
 Method for creating entity in database. Method creates entity and returns pointer to created NSManagedObject.
 SaveContext is autmaticaly called.
 */
- (NSManagedObject*)createEntityWithName:(NSString*)entityName;
/**
 Method for creating entity in database. Method creates entity in specified context and returns pointer to created NSManagedObject.
 SaveContext is NOT called.
 */
- (NSManagedObject*)createEntityWithName:(NSString*)entityName inContext:(NSManagedObjectContext*)context;

/**
 Deletes object from database.
 SaveContext is autmaticaly called.
 */
- (void)deleteManagedObject:(NSManagedObject*)object;
/**
 Deletes object from database.
 SaveContext is NOT called.
 */
- (void)deleteManagedObject:(NSManagedObject*)object inContext:(NSManagedObjectContext*)context;
/**
 Deletes all object from db
 */
-(void)deleteAllObjects;

-(void)deleteObjectsForElan:(Elan*)elan;


-(void)deleteAllObjectForEntity:(NSString*)entityName;
    

#pragma mark - Custom methods for managing entities

- (Action*)createEntityAction;
- (Action*)createEntityActionInContext:(NSManagedObjectContext*)context;

- (IRAction*)createEntityIRAction;
- (IRAction*)createEntityIRActionInContext:(NSManagedObjectContext*)context;

- (Address*)createEntityAddress;
- (Address*)createEntityAddressInContext:(NSManagedObjectContext*)context;

- (Camera*)createEntityCamera;
- (Camera*)createEntityCameraInContext:(NSManagedObjectContext*)context;

- (Device*)createEntityDevice;
- (Device*)createEntityDeviceInContext:(NSManagedObjectContext*)context;

- (IRDevice*)createEntityIRDevice;
- (IRDevice*)createEntityIRDeviceInContext:(NSManagedObjectContext*)context;

- (DeviceInRoom*)createEntityDeviceInRoom;
- (DeviceInRoom*)createEntityDeviceInRoomInContext:(NSManagedObjectContext*)context;

- (Elan*)createEntityElan;
- (Elan*)createEntityElanInContext:(NSManagedObjectContext*)context;

- (HeatCoolArea*)createEntityHeatCoolArea;
- (HeatCoolArea*)createEntityHeatCoolAreaInContext:(NSManagedObjectContext*)context;

- (HeatTimeSchedule*)createEntityHCASchedule;
- (HeatTimeSchedule*)createEntityHCAScheduleInContext:(NSManagedObjectContext*)context;

- (DeviceTimeScheduleClass*)createEntityDeviceSchedule;
- (DeviceTimeScheduleClass*)createEntityDeviceScheduleInContext:(NSManagedObjectContext*)context;

- (Room*)createEntityRoom;
- (Room*)createEntityRoomInContext:(NSManagedObjectContext*)context;

- (Scene*)createEntityScene;
- (Scene*)createEntitySceneInContext:(NSManagedObjectContext*)context;

- (SceneAction*)createEntitySceneAction;
- (SceneAction*)createEntitySceneActionInContext:(NSManagedObjectContext*)context;

- (SecondaryAction*)createEntitySecondaryAction;
- (SecondaryAction*)createEntitySecondaryActionInContext:(NSManagedObjectContext*)context;

- (State*)createEntityState;
- (State*)createEntityStateInContext:(NSManagedObjectContext*)context;

-(IntercomContact*)createEntityIntercomContact;
-(IntercomContact*)createEntityIntercomContactInContext:(NSManagedObjectContext*)context;

-(void)setFavouriteValue:(NSNumber*)favourite forDevice:(NSString*)device;
- (void)pairDevice:(Device*)aDevice andRoom:(Room*)aRoom withCoordinates:(struct SYDevicePosition)position;
- (void)pairDevice:(Device*)aDevice andRoom:(Room*)aRoom withCoordinates:(struct SYDevicePosition)position inContext:(NSManagedObjectContext*)context;
- (void)unpairDevice:(Device*)aDevice andRoom:(Room*)aRoom;
- (void)unpairAllDevicesForRoom:(Room*)aRoom;
#pragma mark - Fetching objects from database

- (NSArray*)getObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate*)predicate sortDescriptor:(NSSortDescriptor*)sortDescriptor inContext:(NSManagedObjectContext * )context;
- (NSArray*)getObjectsForEntity:(NSString*)entityName withPredicate:(NSPredicate*)predicate;
- (NSArray*)getAllObjectsForEntity:(NSString *)entityName withSortDescriptor:(NSSortDescriptor*)sortDescriptor;
- (NSArray*)getAllObjectsForEntity:(NSString *)entityName;

- (NSArray*)getAllRFElans;
- (NSArray*)getAllIRElans;
- (NSArray*)getScheduleDevicesForElan:(Elan*)elan;
- (NSArray*)getDeviceSchedulesForElan:(Elan*)elan;
- (NSArray*)getAllDevicesForElan:(Elan*)elan;
- (NSArray*)getAllActions;
- (NSArray*)getAllIRActions;
- (NSArray*)getAllCameras;
- (NSArray*)getAllDevices;
- (NSArray*)getAllIRDevices;
- (NSArray*)getAllElans;
- (NSArray*)getAllHeatCoolAreas;
- (NSArray*)getAllRooms;
- (NSArray*)getAllScenes;
- (NSArray*)getAllHCASchedules;
- (NSArray*)getAllDeviceSchedules;
- (NSArray*)getAllIntercomContacts;

- (NSArray*)getCurrentElans;
//- (NSNumber*)getCurrentConfigCounter;
//- (void)setCurrentElan:(Elan*)newElan;
-(void)setSelectedElan:(Elan*)newElan;
-(void)setUnselectedElan:(Elan*)elan;
-(void)setElanTypeAndWSAddress:(Elan*)elan type:(NSString*)type wsAddress:(NSString*)address;

//- (Elan*)getCurrentElanInContext:(NSManagedObjectContext*)context;
- (Elan*)getELANwithMAC:(NSString*)aMacAddress inContext:(NSManagedObjectContext*)context;
//-(Elan*)getElanWithIpAdress:(NSString*)ipAddress;

- (Action*)getActionWithName:(NSString*)aName withDeviceID:(NSString*)deviceID inContext:(NSManagedObjectContext*) context;
- (IRAction*)getIRActionWithName:(NSString*)aName withDeviceID:(NSString*)deviceID inContext:(NSManagedObjectContext*) context;
- (Device*)getDeviceWithID:(NSString*)anID;
- (Device*)getDeviceWithID:(NSString*)anID inContext:(NSManagedObjectContext*)context;
- (IRDevice*)getIRDeviceWithID:(NSString*)anID;
- (IRDevice*)getIRDeviceWithID:(NSString*)anID inContext:(NSManagedObjectContext*)context;
- (HeatCoolArea*)getHCADeviceWithID:(NSString*)anID;
- (HeatCoolArea*)getHCADeviceWithID:(NSString*)anID inContext:(NSManagedObjectContext*)context;
- (Room*)getRoomWithID:(NSString*)anID;
- (Room*)getRoomWithID:(NSString*)anID inContext:(NSManagedObjectContext *)context;
- (HeatTimeSchedule*)getScheduleWithID:(NSString*)anID;
- (HeatTimeSchedule*)getScheduleWithID:(NSString*)anID inContext:(NSManagedObjectContext *)context;
- (DeviceTimeScheduleClass*)getDeviceScheduleWithID:(NSString*)anID;
- (DeviceTimeScheduleClass*)getDeviceScheduleWithID:(NSString*)anID inContext:(NSManagedObjectContext *)context;


- (Scene*)getSceneWithID:(NSString*)anID;
- (Scene*)getSceneWithID:(NSString*)anID inContext:(NSManagedObjectContext *)context;

- (SceneAction*)getSceneActionWithSceneID:(NSString*)anID withDeviceID:(NSString*)deviceID withActionName:(NSString*)aName inContext:(NSManagedObjectContext*)context;

- (IntercomContact*)getIntercomContact:(NSString*)aName;
- (IntercomContact*)getIntercomContact:(NSString*)aName inContext:(NSManagedObjectContext*) context;

-(void) deleteAllSceneActions;

@end
