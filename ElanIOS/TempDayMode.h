//
//  TempDayMode.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 1/21/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface TempDayMode : NSObject<NSCopying>
@property (nonatomic, retain) NSNumber* duration;
@property (nonatomic, retain) NSNumber* mode;
@property (nonatomic, retain) NSString* type;
@property (nonatomic, retain) NSNumber* startTime;
@property (nonatomic, retain) NSNumber* originalStartTime;
@property (nonatomic, retain) NSNumber* originalEndTime;
@property (nonatomic, retain) NSNumber* endTime;

-(id)initWithDayMode:(NSNumber*)mode duration:(NSNumber*)duration type:(NSString*)type;
- (id)copyWithZone:(NSZone *)zone;

@end
