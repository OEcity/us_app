//
//  SYColidingAction.h
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 06/07/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SYColidingAction : NSObject

@property (nonatomic, strong)   NSArray *colidingActions;

- (instancetype)init;
- (NSArray*) getColidingActionsForActionName:(NSString*)actionName;
- (NSArray*) filterColidingActionsForAtions:(NSSet*)actions andSelectedActions:(NSSet*)selectedActions;

@end
