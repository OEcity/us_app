//
//  IRDeviceDetailViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 15.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IRDevice.h"

@interface IRDeviceDetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource>
@property (nonatomic, retain) IRDevice *device;
@end
