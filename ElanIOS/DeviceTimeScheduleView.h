////
////  DeviceTimeScheduleView.h
////  iHC-MIRF
////
////  Created by Tom Odler on 15.03.16.
////  Copyright © 2016 Vratislav Zima. All rights reserved.
////
//
//#import <UIKit/UIKit.h>
//#import "DeviceScheduleDay.h"
//#import "TempDayMode.h"
//
//@interface DeviceTimeScheduleView : UIView<UIGestureRecognizerDelegate>
//@property (nonatomic, retain) DeviceScheduleDay * mDay;
//@property (nonatomic, retain) NSString * type;
//@property (nonatomic, retain) id parentViewController;
//-(id)initWithFrameAndDay:(CGRect)frame tempScheduleDay:(DeviceScheduleDay*)day topOffset:(int)topOffset type:(NSString*)type parentViewController:(id)viewController;
//-(void) addMode:(TempDayMode*) mode position:(int) position;
//-(void) setDay:(DeviceScheduleDay*) day;
//-(DeviceScheduleDay*) getDay ;
//-(void) deleteMode:(int) result type:(NSString*)type;
//
//
//@end
