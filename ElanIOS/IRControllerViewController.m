//
//  IRControllerViewController.m
//  Click Smart
//
//  Created by Tom Odler on 16.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "IRControllerViewController.h"
#import "IRAction.h"
#import "SecondaryAction.h"
#import "SYAPIManager.h"
#import "SYCoreDataManager.h"
#import "SYDataLoader.h"
#import "Constants.h"
#import "Util.h"
#import "HUDWrapper.h"
#import "AppDelegate.h"


@interface IRControllerViewController ()
@property (weak, nonatomic) IBOutlet UIView *arrowView;
@property (weak, nonatomic) IBOutlet UIView *numbersView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (nonatomic, retain) IRDevice *myDevice;
@property (nonatomic) NSMutableArray *deviceDict;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *ButtonCollection;


@property (nonatomic) NSMutableArray*removedActions;
@property (nonatomic) NSMutableArray*addedActions;
@property BOOL saved;

@end

@implementation IRControllerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _scrollView.delegate = self;
    
    _pageControl.userInteractionEnabled = NO;
    _pageControl.transform = CGAffineTransformMakeRotation(M_PI_2);
    
    [self setGrayButtons];
    [self setActiveButtons];
    
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self cancelable:NO];
    
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(saveDevice)];
    
    
    if(_inSettings){
        [self navigationItem].rightBarButtonItem = saveButton;
        _deviceDict = [NSMutableArray new];
    }
    // Do any additional setup after loading the view.
    
    NSLog(@"device Name: %@", _device.label);
    for(IRAction *action in _device.actions){
        NSLog(@"%@ ir LED: %@", action.name, action.irLed);
    }
    
    _removedActions = _addedActions = [NSMutableArray new];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _saved = NO;
    [self setActiveButtons];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    if(!_saved)
    [_device.managedObjectContext rollback];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)sendAction:(id)sender {
    NSString *action = [self getActionName:sender];
    
    IRAction *existingAction = nil;
    if(_inSettings){
        
        for(IRAction*deviceAction in _device.actions){
            if([action isEqualToString: deviceAction.name]){
                existingAction = deviceAction;
            }
        }
            [_loaderDialog showWithLabel:@"Waiting for IR Code"];
            
        [[SYAPIManager sharedInstance] saveIRActionWithSuccessToElan:_device.elan success:^(AFHTTPRequestOperation *operation, id response) {
                [_loaderDialog hide];
                NSDictionary *responseDict = response;
                NSLog(@"%@",responseDict);
                
                NSLog(@"%@", _device.led);
                
                if(_device.led.intValue == 0){
                    [self showActiocSheet];
                }
                
                if(existingAction == nil){
                    IRAction *action = [[SYCoreDataManager sharedInstance] createEntityIRActionInContext:_device.managedObjectContext];
                    action.type = nil;
                    action.irLed = _device.led;
                    action.irCodes = [NSKeyedArchiver archivedDataWithRootObject:[responseDict objectForKey:@"ir code"]];
                    action.name = [self getActionName:sender];
                
                [_device addActionsObject:action];
                [_addedActions addObject:action];
                } else {
                    for(IRAction*ac in _device.actions){
                        if([existingAction.name isEqualToString: ac.name]){
                            ac.irCodes = [NSKeyedArchiver archivedDataWithRootObject:[responseDict objectForKey:@"ir code"]];
                        }
                    }
                }
                
                
                [self setActiveButtons];
                
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [_loaderDialog hide];
                NSLog(@"Error: %@", [error localizedDescription]);
                
            }];
        
        
    } else {
        NSDictionary *dict = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNull alloc]init],action, nil];
        
        [[SYAPIManager sharedInstance] putDeviceAction:dict device:_device success:^(AFHTTPRequestOperation *operation, id response) {
            NSLog(@"IR SENT");
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", [error localizedDescription]);
            if(error.code == 503){
                
            }
        }];
    }
}

-(void)showActiocSheet{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Select LED" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"1", @"2", @"3", nil];

    [sheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    _device.led = [NSNumber numberWithInteger:buttonIndex +1];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)setGrayButtons{
    for(UIButton*button in _ButtonCollection){
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressOnButton:)];
        lpgr.minimumPressDuration = 0.5;
        lpgr.delegate = self;
        
        if(_inSettings){
            [button addGestureRecognizer:lpgr];
        }
        
        
        switch (button.tag) {
            case 201:
                [button setImage: [UIImage imageNamed:@"on_off_nastaveni.png" ]forState:UIControlStateNormal];
                break;
            case 202:
                [button setImage: [UIImage imageNamed:@"mute_nastaveni.png" ]forState:UIControlStateNormal];
                break;
            case 203:
                [button setTitleColor: USGrayColor forState:UIControlStateNormal];
                break;
            case 204:
                [button setImage: [UIImage imageNamed:@"info_nastaveni.png" ]forState:UIControlStateNormal];
                break;
            case 1:
                [button setBackgroundImage:[UIImage imageNamed:@"kolecko_tlacitka_horni_off2_nastaveni.png"] forState:UIControlStateNormal];
                
                break;
            case 2:
                [button setBackgroundImage:[UIImage imageNamed:@"kolecko_tlacitka_leva_off2_nastaveni.png"] forState:UIControlStateNormal];
                
                break;
            case 3:
                [button setBackgroundImage:[UIImage imageNamed: @"kolecko_tlacitka_prava_off2_nastaveni.png"] forState:UIControlStateNormal];
                
                break;
            case 4:
                [button setBackgroundImage:[UIImage imageNamed:@"kolecko_tlacitka_spodni_off2_nastaveni.png"] forState:UIControlStateNormal];
                break;
            case 6:
                [button setImage: [UIImage imageNamed:@"sipka_nahoru_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 7:
                [button setImage: [UIImage imageNamed:@"sipka_dolu_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 8:
                [button setImage: [UIImage imageNamed:@"sipka_nahoru_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 9:
                
                [button setImage: [UIImage imageNamed:@"sipka_dolu_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 10:
                [button setImage: [UIImage imageNamed:@"menu_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 11:
                [button setImage: [UIImage imageNamed:@"source_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 12:
                [button setImage: [UIImage imageNamed:@"bublina_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 13:
                [button setImage: [UIImage imageNamed:@"nastroje_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 14:
                [button setImage: [UIImage imageNamed:@"smart_on_nastaveni.png" ]forState:UIControlStateNormal];
                
                break;
            case 111:
                [button setImage: [UIImage imageNamed:@"kanal_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 112:
                [button setImage: [UIImage imageNamed:@"krok_zpet_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 130:
                [button setImage: [UIImage imageNamed:@"zpet_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 131:
                [button setImage: [UIImage imageNamed:@"pause_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 132:
                [button setImage: [UIImage imageNamed:@"dopredu_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 133:
                [button setImage: [UIImage imageNamed:@"rec_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 134:
                [button setImage: [UIImage imageNamed:@"play_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 135:
                [button setImage: [UIImage imageNamed:@"stop_nastaveni.png" ]forState:UIControlStateNormal];
                
                button.layer.borderColor = USGrayColor.CGColor;
                button.layer.borderWidth = 1.0f;
                break;
            case 120:
                [button setBackgroundImage:[UIImage imageNamed:@"abcd_on.png"] forState:UIControlStateNormal ];
                break;
            case 121:
                [button setBackgroundImage:[UIImage imageNamed:@"abcd_on.png"] forState:UIControlStateNormal ];
                break;
            case 122:
                [button setBackgroundImage:[UIImage imageNamed:@"abcd_on.png"] forState:UIControlStateNormal ];
                break;
            case 123:
                [button setBackgroundImage:[UIImage imageNamed:@"abcd_on.png"] forState:UIControlStateNormal ];
                break;
            case 100:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
                button.layer.borderColor = USGrayColor.CGColor;
                [button setTitleColor: USGrayColor forState:UIControlStateNormal];
                button.layer.borderWidth = 1.0f;
                break;
            default:
                break;
        }

    }
}

-(void)setActiveButtons{
    for(UIButton *button in _ButtonCollection){
        
        for(IRAction *action in _device.actions){
            if ([[self getActionName:button] isEqualToString:action.name]) {
                switch(button.tag){
                    case 201:
                        [button setImage: [UIImage imageNamed:@"on_off.png" ]forState:UIControlStateNormal];
                        break;
                    case 202:
                        [button setImage: [UIImage imageNamed:@"mute.png" ]forState:UIControlStateNormal];
                        break;
                    case 203:
                        [button setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
                        break;
                    case 204:
                        [button setImage: [UIImage imageNamed:@"info.png" ]forState:UIControlStateNormal];
                        break;
                    case 6:
                        [button setImage: [UIImage imageNamed:@"sipka_nahoru_IR.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 7:
                        [button setImage: [UIImage imageNamed:@"sipka_dolu_IR.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 8:
                        [button setImage: [UIImage imageNamed:@"sipka_nahoru_IR.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 9:
                        [button setImage: [UIImage imageNamed:@"sipka_dolu_IR.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 10:
                        [button setImage: [UIImage imageNamed:@"menu.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 11:
                        [button setImage: [UIImage imageNamed:@"source.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 12:
                        [button setImage: [UIImage imageNamed:@"bublina.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 13:
                        [button setImage: [UIImage imageNamed:@"nastroje.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                    case 14:
                        [button setImage: [UIImage imageNamed:@"smart_off.png" ]forState:UIControlStateNormal];
                        break;
                    case 1:
                        [button setBackgroundImage:[UIImage imageNamed:@"kolecko_tlacitka_horni_off2.png"] forState:UIControlStateNormal];
                        
                        break;
                    case 2:
                        [button setBackgroundImage:[UIImage imageNamed:@"kolecko_tlacitka_leva_off2.png"] forState:UIControlStateNormal];
                        
                        break;
                    case 3:
                        [button setBackgroundImage:[UIImage imageNamed:@"kolecko_tlacitka_prava_off2.png"] forState:UIControlStateNormal];
                        
                        break;
                    case 4:
                        [button setBackgroundImage:[UIImage imageNamed:@"kolecko_tlacitka_spodni_off2.png"] forState:UIControlStateNormal];
                        break;
                    case 111:
                        [button setImage: [UIImage imageNamed:@"kanal.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 112:
                        [button setImage: [UIImage imageNamed:@"krok_zpet.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 130:
                        [button setImage: [UIImage imageNamed:@"zpet.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 131:
                        [button setImage: [UIImage imageNamed:@"pause.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 132:
                        [button setImage: [UIImage imageNamed:@"dopredu.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 133:
                        [button setImage: [UIImage imageNamed:@"rec.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USBlueColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 134:
                        [button setImage: [UIImage imageNamed:@"play.png" ]forState:UIControlStateNormal];
                        break;
                    case 135:
                        [button setImage: [UIImage imageNamed:@"stop.png" ]forState:UIControlStateNormal];
                        break;
                    case 120:
                        [button setBackgroundImage:[UIImage imageNamed:@"abcd_cervena_off.png"] forState:UIControlStateNormal ];
                        break;
                    case 121:
                        [button setBackgroundImage:[UIImage imageNamed:@"abcd_zelena.png"] forState:UIControlStateNormal ];
                        break;
                    case 122:
                        [button setBackgroundImage:[UIImage imageNamed:@"abcd_zluta.png"] forState:UIControlStateNormal ];
                        break;
                    case 123:
                        [button setBackgroundImage:[UIImage imageNamed:@"abcd_modra.png"] forState:UIControlStateNormal ];
                        break;
                    case 100:
                    case 101:
                    case 102:
                    case 103:
                    case 104:
                    case 105:
                    case 106:
                    case 107:
                    case 108:
                    case 109:
                        button.layer.borderColor = USBlueColor.CGColor;
                        [button setTitleColor: USBlueColor forState:UIControlStateNormal];
                        button.layer.borderWidth = 1.0f;
                        break;
                }
                break;
            } else {
                
                switch (button.tag) {
                    case 201:
                        [button setImage: [UIImage imageNamed:@"on_off_nastaveni.png" ]forState:UIControlStateNormal];
                        break;
                    case 202:
                        [button setImage: [UIImage imageNamed:@"mute_nastaveni.png" ]forState:UIControlStateNormal];
                        break;
                    case 203:
                        [button setTitleColor: USGrayColor forState:UIControlStateNormal];
                        break;
                    case 204:
                        [button setImage: [UIImage imageNamed:@"info_nastaveni.png" ]forState:UIControlStateNormal];
                        break;
                    case 1:
                        [button setBackgroundImage:[UIImage imageNamed:@"kolecko_tlacitka_horni_off2_nastaveni.png"] forState:UIControlStateNormal];
                        
                        break;
                    case 2:
                        [button setBackgroundImage:[UIImage imageNamed:@"kolecko_tlacitka_leva_off2_nastaveni.png"] forState:UIControlStateNormal];
                        
                        break;
                    case 3:
                        [button setBackgroundImage:[UIImage imageNamed: @"kolecko_tlacitka_prava_off2_nastaveni.png"] forState:UIControlStateNormal];
                        
                        break;
                    case 4:
                        [button setBackgroundImage:[UIImage imageNamed:@"kolecko_tlacitka_spodni_off2_nastaveni.png"] forState:UIControlStateNormal];
                        break;
                    case 6:
                        [button setImage: [UIImage imageNamed:@"sipka_nahoru_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 7:
                        [button setImage: [UIImage imageNamed:@"sipka_dolu_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 8:
                        [button setImage: [UIImage imageNamed:@"sipka_nahoru_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 9:
                        
                        [button setImage: [UIImage imageNamed:@"sipka_dolu_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 10:
                        [button setImage: [UIImage imageNamed:@"menu_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 11:
                        [button setImage: [UIImage imageNamed:@"source_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 12:
                        [button setImage: [UIImage imageNamed:@"bublina_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 13:
                        [button setImage: [UIImage imageNamed:@"nastroje_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 14:
                        [button setImage: [UIImage imageNamed:@"smart_on_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        break;
                    case 111:
                        [button setImage: [UIImage imageNamed:@"kanal_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 112:
                        [button setImage: [UIImage imageNamed:@"krok_zpet_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 130:
                        [button setImage: [UIImage imageNamed:@"zpet_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 131:
                        [button setImage: [UIImage imageNamed:@"pause_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 132:
                        [button setImage: [UIImage imageNamed:@"dopredu_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 133:
                        [button setImage: [UIImage imageNamed:@"rec_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 134:
                        [button setImage: [UIImage imageNamed:@"play_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 135:
                        [button setImage: [UIImage imageNamed:@"stop_nastaveni.png" ]forState:UIControlStateNormal];
                        
                        button.layer.borderColor = USGrayColor.CGColor;
                        button.layer.borderWidth = 1.0f;
                        break;
                    case 120:
                        [button setBackgroundImage:[UIImage imageNamed:@"abcd_on.png"] forState:UIControlStateNormal ];
                        break;
                    case 121:
                        [button setBackgroundImage:[UIImage imageNamed:@"abcd_on.png"] forState:UIControlStateNormal ];
                        break;
                    case 122:
                        [button setBackgroundImage:[UIImage imageNamed:@"abcd_on.png"] forState:UIControlStateNormal ];
                        break;
                    case 123:
                        [button setBackgroundImage:[UIImage imageNamed:@"abcd_on.png"] forState:UIControlStateNormal ];
                        break;
                    case 100:
                    case 101:
                    case 102:
                    case 103:
                    case 104:
                    case 105:
                    case 106:
                    case 107:
                    case 108:
                    case 109:
                        button.layer.borderColor = USGrayColor.CGColor;
                        [button setTitleColor: USGrayColor forState:UIControlStateNormal];
                        button.layer.borderWidth = 1.0f;
                        break;
                }
            }
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    static NSInteger previousPage = 0;
    CGFloat pageHeight = scrollView.frame.size.height;
    float fractionalPage = scrollView.contentOffset.y / pageHeight;
    NSInteger page = lround(fractionalPage);
    if (previousPage != page) {
        
        previousPage = page;
        [_pageControl setCurrentPage:page];
    }
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

-(BOOL)shouldAutorotate{
    return YES;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationPortrait;
}
    
-(void)saveDevice{
    _saved = YES;
    NSDictionary *dict = [Util serializeIRDevice:_device];
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", @"")];
    
    [[SYAPIManager sharedInstance] updateIRDeviceWithDict:dict toElan:_device.elan success:^(AFHTTPRequestOperation *operation, id response) {
        NSLog(@"Device updated!");
        [_loaderDialog hide];
        NSInteger currentIndex = [self.navigationController.viewControllers indexOfObject:self];
        if( currentIndex-2 >= 0 ) {
            [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:currentIndex-2] animated:YES];
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error update device: %@", [error localizedDescription]);
        [_loaderDialog hide];
    }];
    
    }

-(NSString*)getActionName:(UIButton*)button{
    NSString*actionName = nil;
    switch (button.tag) {
        case 1:
            actionName = @"arrow_up";
            break;
        case 2:
            actionName = @"arrow_left";
            break;
        case 3:
            actionName = @"arrow_right";
            break;
        case 4:
            actionName = @"arrow_down";
            break;
        case 5:
            actionName = @"ok";
            break;
        case 6:
            actionName = @"volume_up";
            break;
        case 7:
            actionName = @"volume_down";
            break;
        case 8:
            actionName = @"channel_up";
            break;
        case 9:
            actionName = @"channel_down";
            break;
        case 10:
            actionName = @"menu";
            break;
        case 11:
            actionName = @"source";
            break;
        case 12:
            actionName = @"info";
            break;
        case 13:
            actionName = @"tools";
            break;
        case 14:
            actionName = @"smart";
            break;
        case 101:
            actionName = @"button_one";
            break;
        case 102:
            actionName = @"button_two";
            break;
        case 103:
            actionName = @"button_three";
            break;
        case 104:
            actionName = @"button_four";
            break;
        case 105:
            actionName = @"button_five";
            break;
        case 106:
            actionName = @"button_six";
            break;
        case 107:
            actionName = @"button_seven";
            break;
        case 108:
            actionName = @"button_eight";
            break;
        case 109:
            actionName = @"button_nine";
            break;
        case 100:
            actionName = @"button_zero";
            break;
        case 111:
            actionName = @"button_channel";
            break;
        case 112:
            actionName = @"return";
            break;
        case 120:
            actionName = @"button_a";
            break;
        case 121:
            actionName = @"button_b";
            break;
        case 122:
            actionName = @"button_c";
            break;
        case 123:
            actionName = @"button_d";
            break;
        case 130:
            actionName = @"ff_rw";
            break;
        case 131:
            actionName = @"pause";
            break;
        case 132:
            actionName = @"ff_fw";
            break;
        case 133:
            actionName = @"rec";
            break;
        case 134:
            actionName = @"play";
            break;
        case 135:
            actionName = @"stop";
            break;
        case 201:
            actionName = @"power_off";
            break;
        case 202:
            actionName = @"mute";
            break;
        case 203:
            actionName = @"exit";
            break;
        case 204:
            actionName = @"info";
            break;
        default:
            actionName = @"";
            break;
    }
    
    return actionName;
}

-(void)longPressOnButton:(UILongPressGestureRecognizer*)lpgr{
    UIButton *button = (UIButton*)[lpgr view];
    if(button != nil){
        IRAction *actionToDelete = nil;
        for(IRAction *action in _device.actions){
            if([action.name isEqualToString: [self getActionName:button]]){
                actionToDelete = action;
            }
        }
        
        if(actionToDelete != nil){
            [_removedActions addObject:actionToDelete];
            
            [_device removeActionsObject:actionToDelete];
            
            [self setActiveButtons];
        }
    }

    
}

@end
