//
//  SYConfigureRoomsViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/21/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYConfigureRoomsViewController.h"
#import "SYCoreDataManager.h"
#import "AppDelegate.h"
#import "SYAPIManager.h"
#import "ResourceTiles.h"
#import "CustomCell.h"

@interface SYConfigureRoomsViewController ()
@property (nonatomic, strong) NSFetchedResultsController* fetchedResultsController;

@property (nonatomic) BOOL isExpanded;
@property (nonatomic) NSIndexPath *indexPathForExpand;
@end

@implementation SYConfigureRoomsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.emptyLabel.text = NSLocalizedString(@"noRooms", nil);
    _isExpanded = false;
    _editButton.hidden = YES;
    _saveButton.hidden = YES;
    
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{

    [self initFetchedResultsController];
    [super viewWillAppear:animated];
    self.emptyLabel.hidden = _fetchedResultsController?_fetchedResultsController.fetchedObjects.count>0:NO;
    
    _editButton.hidden = YES;
    _saveButton.hidden = YES;
}

-(void) viewWillDisappear:(BOOL)animated{
    if (self.tableView.editing){
        self.tableView.editing = NO;
    }
    [super viewWillDisappear:animated];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    _fetchedResultsController = nil;
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSString* name = ((Room*)[_fetchedResultsController objectAtIndexPath:indexPath]).label;

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_room", nil) ,name] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
        alert.tag = indexPath.row;
        [alert show];
    }
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
        self.deletedIndex = indexPath;
        NSString* roomID = ((Room*)[_fetchedResultsController objectAtIndexPath:indexPath]).roomID;
        Room*room=[_fetchedResultsController objectAtIndexPath:indexPath];
        if (roomID!=nil){
            [[SYAPIManager sharedInstance] deleteRoom:room success:^(AFHTTPRequestOperation * operation, id object)
             {
                 [[SYCoreDataManager sharedInstance] deleteManagedObject:[[SYCoreDataManager sharedInstance] getRoomWithID:roomID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]]];
                 
             }failure:^(AFHTTPRequestOperation * operation, NSError * error){
                 UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"cannotDeleteRoom"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];

            }];
        }
    }
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(_isExpanded)
        return [_fetchedResultsController.fetchedObjects count];
    
    return [_fetchedResultsController.fetchedObjects count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"MyCell";
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if(indexPath.row == _indexPathForExpand.row && _isExpanded){
    CustomCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
//    if(cell == nil)
//    {
        cell = [[CustomCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        [self configureCell:cell atIndexPath:indexPath];
        cell.backgroundColor = [UIColor blackColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        return  cell;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
    [cell setSelectedBackgroundView:bgColorView];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    Room *selectedRoom = [_fetchedResultsController objectAtIndexPath:indexPath];
//    NSMutableArray* devicesForRoom =[[NSMutableArray alloc] initWithArray:[selectedRoom.devicesInRoom allObjects]];
//    NSIndexPath *oldIndexPathForReload = [NSIndexPath indexPathForRow:_indexPathForExpand.row -1 inSection:0] ;
    
    [self displayDevicesForRowAtIndexPath:indexPath];
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    
//    [tableView reloadRowsAtIndexPaths:@[indexPath, oldIndexPathForReload] withRowAnimation:UITableViewRowAnimationAutomatic];

}

-(void)displayDevicesForRowAtIndexPath:(NSIndexPath*)indexPath{
    [self.tableView beginUpdates];
    
    if(_isExpanded && (indexPath.row == _indexPathForExpand.row)){
        //ZDE hidden = YES;
        _editButton.hidden = YES;
        _saveButton.hidden = YES;
 
        _isExpanded = false;
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_indexPathForExpand.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        
        [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_indexPathForExpand.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        
        
    } else if(_isExpanded){
        //zde hidden = NO;
        
        _editButton.hidden = NO;
        _saveButton.hidden = NO;
        _isExpanded = false;
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_indexPathForExpand.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        
        _indexPathForExpand = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        _isExpanded = true;
        
        NSArray *indexPaths = @[_indexPathForExpand];
        [self.tableView insertRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
        
        
    } else {
        //zde hidden = NO;
        _editButton.hidden = NO;
        _saveButton.hidden = NO;
        
        _indexPathForExpand = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
        _isExpanded = true;
        
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:_indexPathForExpand.row inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
        
        NSArray *indexPaths = @[[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
        [self.tableView insertRowsAtIndexPaths:indexPaths
                              withRowAnimation:UITableViewRowAnimationFade];
        
    }
    
    [self.tableView endUpdates];

    
}

-(void)reload{
    [self.tableView reloadData];
}


-(IBAction)nextStep:(id)sender{

}
-(IBAction)back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}



#pragma mark - Init

- (void)initFetchedResultsController {

    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Room"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    NSPredicate*predicate = [NSPredicate predicateWithFormat:@"NOT(self.label == 'roomTitle')"];
    [fetchRequest setPredicate:predicate];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetchRequest
                                 managedObjectContext:context
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    _fetchedResultsController.delegate = self;
    NSError *error;
    
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            self.emptyLabel.hidden = [_fetchedResultsController.fetchedObjects count] > 0;

            break;
            
        case NSFetchedResultsChangeUpdate:
            [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
                             withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    _isExpanded = false;
    [self.tableView endUpdates];
    self.emptyLabel.hidden = [_fetchedResultsController.fetchedObjects count] > 0;
        [_tableView reloadData];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Room *selectedDevice = nil;
    if(_isExpanded){
        if (indexPath.row < _indexPathForExpand.row) {
                selectedDevice = [_fetchedResultsController objectAtIndexPath:indexPath];
        } else if (indexPath.row > _indexPathForExpand.row) {
                selectedDevice = [_fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row  inSection:0]];
        } else {
            selectedDevice = [_fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0]];
        }
    }
        else {
    selectedDevice = [_fetchedResultsController objectAtIndexPath:indexPath];
}
    
    if(indexPath.row == _indexPathForExpand.row && _isExpanded){
        CustomCell *mycell = (CustomCell*)cell;
        NSLog(@"selected room: %@", selectedDevice.label);
        mycell.room = selectedDevice;
        mycell.parentController = self;
        [mycell getRoomInCell];
        return;
    }
    
    UILabel * deviceName = (UILabel*)[cell viewWithTag:2];
    
    //[deviceName setTextColor:[UIColor colorWithRed:1.0 green:1 blue:1 alpha:1]];
    while ([cell viewWithTag:201]!=nil){
        [[cell viewWithTag:201] removeFromSuperview];
    }
    
    deviceName.text = selectedDevice.label;
    [[cell viewWithTag:99] removeFromSuperview];
}

//-(void) connectionResponseOK:(NSObject *)returnedObject response:(id<Response>)responseType{

    /*Room * dev = [self.rooms objectAtIndex:self.deletedIndex.row];
    [CoreDataManager removeRoom:dev.name];
    [self.rooms removeObjectAtIndex:self.deletedIndex.row];
    [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:self.deletedIndex] withRowAnimation:YES];
     */

//}

-(void)connectionResponseFailed:(NSInteger)code{
    NSLog(@"cannot delete %ld", (long)code);
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:@"cannotDeleteRoom"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskPortrait;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == _indexPathForExpand.row && _isExpanded){
        Room*room = [_fetchedResultsController objectAtIndexPath:indexPath];
        return (CGFloat)[[[SYCoreDataManager sharedInstance]getAllDevicesForElan:room.elan]count]*50+50;
    }
    
    
    return (CGFloat)50;
}

-(void)reloadTableView{
    [self tableView:_tableView didSelectRowAtIndexPath:_indexPathForExpand];
}

- (IBAction)saveRoom:(id)sender {
    CustomCell *myCell = (CustomCell*) [_tableView cellForRowAtIndexPath:_indexPathForExpand];
    
    [myCell saveRoomWithDevices];
}

- (IBAction)addRoomTapped:(id)sender {
    [self performSegueWithIdentifier:@"roomDetailSegue" sender:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
            SYAddRoomConfigurationViewController *controller = (SYAddRoomConfigurationViewController*) [segue destinationViewController];
    if(sender == _editButton){
        controller.room = (Room*)[_fetchedResultsController objectAtIndexPath:_indexPathForExpand];
    } else {
        controller.room = nil;
    }
}

@end
