//
//  SYCameraDetailViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/14/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYCameraDetailViewController.h"
#import "Camera+Setup.h"
#import "SYAPIManager.h"

@interface SYCameraDetailViewController ()
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeRight;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeLeft;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeDown;
@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeUp;
@property (strong, nonatomic) IBOutlet UIPinchGestureRecognizer *zoomInRecognizer;
@property (nonatomic) float startValue;

@end

@implementation SYCameraDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.operationQueue = [[NSOperationQueue alloc]init];

    _startValue = 0;

    _swipeUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(upButton:)];
    [_swipeUp setDirection:UISwipeGestureRecognizerDirectionUp];
    
    
    _swipeDown = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(downButton:)];
    [_swipeDown setDirection:UISwipeGestureRecognizerDirectionDown];
    
    
    _swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(leftButton:)];
    [_swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    
        _swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rightButton:)];
    [_swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    
    _zoomInRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(twoFingerPinch:)];
    

    
    [self.view addGestureRecognizer:_swipeRight];
    [self.view addGestureRecognizer:_swipeLeft];
    [self.view addGestureRecognizer:_swipeUp];
    [self.view addGestureRecognizer:_swipeDown];
    [self.view addGestureRecognizer:_zoomInRecognizer];
    

}

- (void)twoFingerPinch:(UIPinchGestureRecognizer *)recognizer;
{
    //NSLog(@"Pinch scale: %f %i", recognizer.scale, recognizer.state);
    if (recognizer.state == UIGestureRecognizerStateBegan)
    {
        _startValue = recognizer.scale;
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        if (_startValue > recognizer.scale)
        {
            //NSLog(@"Pinch in");
            [self zoomOutButton:self];
        }
        else
        {
            //NSLog(@"Pinch out");
            [self zoomInButton:self];
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self.player stop];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   // self.player =  [[MotionJpegImageView alloc] initWithFrame:self.view.frame];
    [self.player setUrl:[NSURL URLWithString:[self.camera videoAddress]]] ;
    [self.player playWith:self.camera.username And:self.camera.password And:self.camera.type];
//    [self.bottomControlBox setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pozadi_pod_menu-01.png"]]];
    [self showActionViews];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)onTap:(id)sender{
    [self showActionViews];
}

-(void)showActionViews{
    for(UIButton *myButton in _controllCollection){
        dispatch_async(dispatch_get_main_queue(), ^{
            myButton.alpha = 1;
        });
        
    }
    for(UIButton *myButton in _controllCollection){
        dispatch_async(dispatch_get_main_queue(), ^{
            myButton.hidden = NO;
        });
    }
    self.hideTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(hideActionViews) userInfo:nil repeats:NO];
}

-(void)hideActionViews{
    [self.hideTimer invalidate];
    self.hideTimer=nil;
    [UIView animateWithDuration:0.3 animations:^{
        for(UIButton *myButton in _controllCollection){
            dispatch_async(dispatch_get_main_queue(), ^{
                myButton.alpha = 0;
            });
            
        }
    } completion: ^(BOOL finished) {
        for(UIButton *myButton in _controllCollection){
            dispatch_async(dispatch_get_main_queue(), ^{
                myButton.hidden = YES;
            });
            
        }
    }];


}


-(IBAction)leftButton:(id)sender{
        [self sendPTZActionWithUrl:self.camera.left];
}
-(IBAction)rightButton:(id)sender{
        [self sendPTZActionWithUrl:self.camera.right];
}

-(IBAction)upButton:(id)sender{
        [self sendPTZActionWithUrl:self.camera.up];
}

-(IBAction)downButton:(id)sender{
    [self sendPTZActionWithUrl:self.camera.down];
}

-(IBAction)zoomInButton:(id)sender{
        [self sendPTZActionWithUrl:self.camera.zoomIn];
}

-(IBAction)zoomOutButton:(id)sender{
    [self sendPTZActionWithUrl:self.camera.zoomOut];
}

-(void) sendPTZActionWithUrl:(NSString*) url{
    
    [[SYAPIManager sharedInstance] getDataFromURL:url
                    user:self.camera.username password:self.camera.password success:^(AFHTTPRequestOperation *operation, id response) {
                                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                             }];

    [self.hideTimer invalidate];
    self.hideTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(hideActionViews) userInfo:nil repeats:NO];
}


-(IBAction)CloseButton:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
-(IBAction)changeSizeButton:(id)sender{
    UIButton* changeButton = (UIButton*)sender;

    switch (changeButton.tag) {
        case 1:

            self.player.contentMode = UIViewContentModeScaleAspectFill;

            [changeButton setBackgroundImage:[UIImage imageNamed:@"kamera_dalsi_volby_prizpusobeni.png"] forState:UIControlStateNormal];
            [changeButton setTag:2];
            break;
        case 2:
            self.player.contentMode = UIViewContentModeScaleAspectFit;
            [changeButton setBackgroundImage:[UIImage imageNamed:@"akamera_dalsi_volby_auto.png"] forState:UIControlStateNormal];

            [changeButton setTag:3];
            break;

        case 3:
            self.player.contentMode = UIViewContentModeScaleToFill;
            [changeButton setBackgroundImage:[UIImage imageNamed:@"kamera_dalsi_volby_roztazeni.png"] forState:UIControlStateNormal];
            [changeButton setTag:1];
            break;

        default:
            break;
    }


    [self.hideTimer invalidate];
    self.hideTimer=nil;
    self.hideTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(hideActionViews) userInfo:nil repeats:NO];

    
}
-(void)loadDataAsychn:(NSString*)url
{


//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init] ;
//    [request setURL:[NSURL URLWithString:url]];
//    [request setHTTPMethod:@"GET"];
//      NSURLResponse* response;
//    NSError* error = nil;

    //Capturing server response
//    NSData* result = [NSURLConnection sendSynchronousRequest:request  returningResponse:&response error:&error];
    
}



- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{

        return UIInterfaceOrientationMaskAll;
}
@end
