//
//  IRKlimaViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 06.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HUDWrapper.h"
#import "IRDevice.h"
#import "SYCoreDataManager.h"

@interface IRKlimaViewController : UIViewController<UIActionSheetDelegate, UIGestureRecognizerDelegate>

@property (nonatomic, retain) IRDevice* device;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *buttonCollection;
@property (nonatomic, retain) HUDWrapper *loaderDialog;

@property (nonatomic) BOOL inSettings;
@end
