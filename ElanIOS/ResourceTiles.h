//
//  ResourceTiles.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 08/11/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Tile.h"
@interface ResourceTiles : NSObject

+(Tile*)generateTileForName:(NSString*)name;
+(NSDictionary*)getDict;
+(NSDictionary*)getDevicesDict;
+(Tile*)generateDeviceTileForName:(NSString*)name;
+(NSString*)getDevicesTranslationName:(NSString*)deviceName;



@end
