//
//  AddRoomConfigurationViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/21/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Room.h"
#import "Elan.h"

@interface SYAddRoomConfigurationViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate, NSFetchedResultsControllerDelegate>

@property (nonatomic, retain) NSMutableArray *devicesForRoom;
@property (nonatomic, retain) Room *room;

@property (nonatomic, copy) NSString* roomType;
@property BOOL deviceIsStored;


@end
