//
//  NSNumber+Type.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 15/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Type)
-(id) initWithType:(NSString*)type value:(NSString*)value;

@end
