//
//  TempScheduleDay.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 1/21/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TempDayMode.h"

@interface TempScheduleDay : NSObject<NSCopying>
    
@property (nonatomic, copy) NSString * id;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSMutableArray * modes;


-(id)initWithName:(NSString *)name;
-(void)setupModes;
-(void) recountModes:(BOOL)isLeftDirection modePosition:(int) mModePosition ;
-(void) redefineModes;
-(int) joinAndDeleteModes:(int)modePosition;
-(int) editMode:(TempDayMode*) edit modePosition:(int) modePosition;
-(int) deleteMode:(NSInteger) result;
- (id)copyWithZone:(NSZone *)zone;


@end
