//
//  DeviceTimeScheduleClass+Dictionary.m
//  Click Smart
//
//  Created by Tom Odler on 24.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "DeviceTimeScheduleClass+Dictionary.h"
#import "SYCoreDataManager.h"
#import "CoreDataObjects.h"

@implementation  DeviceTimeScheduleClass (Dictionary)
- (BOOL)updateWithDictionary:(NSDictionary*)dict {
    BOOL updated = NO;
    
    NSString* parsedID = [dict objectForKey:@"id"];
    NSString* parsedLabel  = [dict objectForKey:@"label"];
    NSString* parsedType  = [dict objectForKey:@"type"];
    
    
    
    if (![self.devicetimescheduleclassID isEqualToString:parsedID]) {
        self.devicetimescheduleclassID = parsedID;
        updated = YES;
    }
    if (![self.label isEqualToString:parsedLabel]) {
        self.label = parsedLabel;
        updated = YES;
    }
    if (![self.type isEqualToString:parsedType]) {
        self.type = parsedType;
        updated = YES;
    }
    
    NSArray* devices = [dict objectForKey:@"devices"];
    
    
    [self setDevices:[NSSet new]];
    for (id element in devices) {
        Device * dev = [[SYCoreDataManager sharedInstance] getDeviceWithID:element inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        if (dev == nil){
            dev = [[SYCoreDataManager sharedInstance] createEntityDevice];
            [dev setDeviceID:element];
            [[SYCoreDataManager sharedInstance] saveContext];
        }
        [self addDevicesObject:dev];
    }
    
    return updated;
}


@end
