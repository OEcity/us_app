//
//  SYRoomsViewController.m
//  ElanIOS
//
//  Created by Vratislav Zima on 5/24/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYRoomsViewController.h"
#import "URLConnector.h"
#import "ItemsResponse.h"
#import "Room.h"
#import "AppDelegate.h"
#import "Device.h"
#import "DevicesResponse.h"
#import "DeviceDetailResponse.h"
#import "SYDevicesViewController.h"
#import "Util.h"
#import "ConfigurationResponse.h"
#import "RoomDetailResponse.h"
#import "UrlResponse.h"
#import "SYCoreDataManager.h"
#import <QuartzCore/QuartzCore.h>
#import "Tile.h"
#import "ResourceTiles.h"
#import "LoaderHelper.h"
#import "SYDataLoader.h"
#import "Constants.h"
#import "UIScrollView+SVPullToRefresh.h"
#import "UIViewController+RevealViewControllerAddon.h"
#import "HeatTimeSchedule.h"
#import "UIImageViewAligned.h"
#import "BlindsViewController.h"
#import "DimmingViewController.h"
#import "HCAViewController.h"
#import "ChromaticityViewController.h"
#import "ReleViewController.h"
#import "IRKlimaViewController.h"
#import "IRControllerViewController.h"
#import "RoomsRootViewController.h"

#import "LinphoneManager.h"


@interface SYRoomsViewController (){
    BOOL isLoading;
    BOOL firstLoad;
    BOOL didAppear;
}

@property (nonatomic, strong) NSString * roomName;
@property (nonatomic, strong) NSString * roomLabel;
@property (nonatomic, strong) NSFetchedResultsController* fetchedResultsController;
@property (weak, nonatomic) IBOutlet UICollectionView *devicesCollection;
@property NSUInteger indexForRoom;


- (void)cleaningDB:(NSNotification*)notification;
- (void)recievedNotification:(NSNotification*)notification;
- (void)initFetchedResultsController;

@end

@implementation SYRoomsViewController

@synthesize collections;
@synthesize activityInd;
@synthesize loader;
@synthesize noServerFoundLabel;
@synthesize rgbViewController;
@synthesize roomName;
@synthesize roomLabel;


#pragma mark - View handling

- (void)viewDidLoad
{
    [super viewDidLoad];
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self cancelable:YES];
    AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    app.viewController = self;
    firstLoad = app.isAfterLaunch;
    if (app.isAfterLaunch){
        app.isAfterLaunch = false;
        [[SYDataLoader sharedInstance] reloadIfNeeded];
    }
    /*[[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(syncComplete:)
     name:@"dataSyncComplete"
     object:app.myWS];*/
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }
    
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.5; //seconds
    lpgr.delegate = self;
    [self.devicesCollection addGestureRecognizer:lpgr];
    if ([Util getServerAddress]!=nil){
        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        
        [defaults setObject:[[NSNumber alloc] initWithInt:0] forKey:@"CCC"];
        [defaults synchronize];
        //[[LoaderHelper sharedLoaderHelper] reloadDevicesAndRoomsOnCounterChange];
    }
    
    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    self.collections.contentInset = UIEdgeInsetsZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGSize CellSize = CGSizeMake(155,240);
    
    if(indexPath.item == 0){
        CellSize = CGSizeMake(155, 120);
    }
    
    return CellSize;
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initFetchedResultsController];
    
    CHTCollectionViewWaterfallLayout *layout =
    (CHTCollectionViewWaterfallLayout *)self.collections.collectionViewLayout;
    layout.itemRenderDirection  = CHTCollectionViewWaterfallLayoutItemRenderDirectionRightToLeft;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedNotification:)
                                                 name:LOADING_ROOMS_ENDED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(recievedNotification:)
                                                 name:LOADING_ROOMS
                                               object:nil];
    
    
    [self.leftBottomButton setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    [self.rightBottomButton setTitle:NSLocalizedString(@"menu", nil) forState:UIControlStateNormal];
    [_ipTitle setText:NSLocalizedString(@"ipTitle", nil)];
    
    
    [self showHideCollection];
    
    [[SYWebSocket sharedInstance] reconnect];
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    if (app.loadingDevices==YES){
        [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
        _ipTitle.hidden=YES;
    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notifcation:)
                                                 name:LOADING_DATA_ENDED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(notifcation:)
                                                 name:LOADING_STATES_ENDED
                                               object:nil];
    
    
}


-(void)notifcation:(NSNotification*)notification{
    if ([[notification name] isEqualToString:LOADING_DATA_ENDED] || [[notification name] isEqualToString:LOADING_STATES_ENDED]){
    }
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.fetchedResultsController = nil;
}


- (void)showHideCollection {
    if ([_fetchedResultsController.fetchedObjects count]==0){
        if ([[SYCoreDataManager sharedInstance] getCurrentElans].count >0){


            _ipTitle.hidden=YES;
            collections.hidden=NO;
        }else{
            _ipTitle.hidden=NO;
            collections.hidden=YES;
        }
    }else{
        if ([_fetchedResultsController.fetchedObjects count]==1){
            [collections setHidden:YES];
            [_devicesCollection setHidden:NO];
            
            Room * room = [_fetchedResultsController.fetchedObjects objectAtIndex:0];
            
            self.currentRoomID = 0;
            
            self.roomName = room.roomID;
            self.roomLabel = room.label;
            
            
            _collectionViewDelegate = [[DevicesCollectionViewDelegate alloc]init];
            [_collectionViewDelegate setView:self.view];
            [_devicesCollection setDelegate:_collectionViewDelegate];
            [_devicesCollection setDataSource:_collectionViewDelegate];
            [_collectionViewDelegate setCollectionView:_devicesCollection];
            
            UINib *dimmingNib = [UINib nibWithNibName:@"DimmingCollectionViewCell" bundle:nil];
            [self.devicesCollection registerNib:dimmingNib forCellWithReuseIdentifier:@"dimmingCell"];
            
            if (![_collectionViewDelegate.roomID isEqualToString:room.roomID]) {
                [_collectionViewDelegate setRoomID:room.roomID];
                [_collectionViewDelegate.collectionView reloadData];
            }
        } else {
            [_devicesCollection setHidden:YES];
            [collections setHidden:NO];
        }
        _ipTitle.hidden=YES;
        [collections reloadData];
        [_devicesCollection reloadData];
    }
    [collections reloadData];
}


-(void)recievedNotification:(NSNotification*)notification{
    if ([notification.name isEqualToString:LOADING_ROOMS_ENDED]){
        [_loaderDialog hide];
    } else if ([notification.name  isEqual:LOADING_ROOMS]){
    }
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [[SYDataLoader sharedInstance]reloadFWVersion:_updateBadge];
    
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.showRooms=false;

/*
    if(!app.hasGIOMWeather && !app.hasOPWeather){
        for(Elan*elan in [[SYCoreDataManager sharedInstance]getAllElans]){
            if(elan.selected.boolValue || ![elan.type containsString:@"IR"]){
                [[SYDataLoader sharedInstance]loadWeatherFromElan:elan];
            }
        }
    }
 */
    
    if (firstLoad){
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        if ([defaults boolForKey:@PREF_SHOW_FAVOURITES_AFTER_BOOT]){
            [self performSegueWithIdentifier:@"viewFavourites" sender:self];
        }
        firstLoad = false;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init

- (void)initFetchedResultsController {
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Room"];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
//    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"elan.mac == %@", [[[SYCoreDataManager sharedInstance] getCurrentElan] mac]]];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _fetchedResultsController = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetchRequest
                                 managedObjectContext:context
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    _fetchedResultsController.delegate = self;
    NSError *error;
    
    if (![_fetchedResultsController performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if ([_fetchedResultsController.fetchedObjects count]==0){
        _ipTitle.hidden=NO;
        
    }else{
        _ipTitle.hidden=YES;
    }
    [self showHideCollection];
}

#pragma mark - Gesture handling

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    [self.collectionViewDelegate handleLongPress:gestureRecognizer];
}

#pragma mark - UICollectionView Datasource
// 1
- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section {
    
    if ([[_fetchedResultsController sections] count] > 0) {
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
        return [sectionInfo numberOfObjects]+1;
    } else
        return 0;
}
// 2
- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    return [[_fetchedResultsController sections] count];
}
// 3
- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BOOL isLeft = NO;
    
    UICollectionViewCell *cell = [cv dequeueReusableCellWithReuseIdentifier:@"myRightTile" forIndexPath:indexPath];
    NSString *bcg_name = @"kolecko_prava";
    
    
    
    if(indexPath.item % 2 != 0){
        cell = [cv dequeueReusableCellWithReuseIdentifier:@"myTile" forIndexPath:indexPath];
        bcg_name = @"kolecko_leva";
        isLeft = YES;
    }
    
    if(indexPath.item == 0){
        cell = [cv dequeueReusableCellWithReuseIdentifier:@"myRightTileFirst" forIndexPath:indexPath];
        return cell;
    }

    
    
    UILabel * roomNameLBL = (UILabel*)[cell viewWithTag:2];
    
    
    NSIndexPath *path = [NSIndexPath indexPathForItem:indexPath.item-1 inSection:indexPath.section];
    
    Room* room =(Room*)[_fetchedResultsController objectAtIndexPath:path];
    
    [roomNameLBL setText:room.label];
    if([room.label isEqualToString:@"roomTitle"]){
        [roomNameLBL setText:[NSString stringWithFormat:@"IR %@", NSLocalizedString(@"roomTitle", nil) ]];
    }
    
    
    UIView * temperatureView = [cell viewWithTag:4];
    
    UILabel * temperatureLabel = (UILabel*)[temperatureView viewWithTag:1];
    UIImageView* imageV = (UIImageView*)[cell viewWithTag:1];
    UIImageViewAligned* roomIcon = (UIImageViewAligned*)[cell viewWithTag:3];
    
    if(isLeft){
        roomIcon.alignRight = NO;
    [roomIcon setAlignLeft:YES];
    } else {
        roomIcon.alignLeft = NO;
    [roomIcon setAlignRight:YES];
    }
    
    Tile * tile = [ResourceTiles generateTileForName:room.type];
    [roomIcon setImage:tile.tile_off];
    [imageV setImage:[UIImage imageNamed:bcg_name]];
    [temperatureLabel setTextColor:[UIColor colorWithRed:0.56 green:0.78 blue:0.05 alpha:1]];
    
#warning Temperature info for room is hidden
    //    NSNumber* temperature =nil;//[SYCoreDataManager temperatureForRoom:room.name];
    //    NSSortDescriptor * desc = [[NSSortDescriptor alloc] initWithKey:@"value" ascending:NO];
    //    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"(self.name=='temperature IN' OR  self.name=='temperature' OR  self.name=='temperature OUT') AND self.device.inRooms.@count > 0 AND SUBQUERY(self.device.inRooms,$inRoom,$inRoom.room.roomID == %@ ).@count > 0", room.roomID];
    //
    //    NSArray * states = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"State" withPredicate:predicate sortDescriptor:desc inContext:nil];
    //    State * state;
    //    if ([states count]>0){
    //        state = [states objectAtIndex:0];
    //        temperature = state.value;
    //    }
    //    if (temperature!=nil && ![temperature isMemberOfClass:[NSNull class]] && [temperature intValue]!=-1){
    //
    //        temperatureView.hidden=NO;
    //        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    //        formatter.roundingIncrement = [NSNumber numberWithDouble:1];
    //        formatter.numberStyle = NSNumberFormatterDecimalStyle;
    //        [formatter setDecimalSeparator:@","];
    //        NSLog(@"heatin value: %f", temperature.doubleValue);
    //        temperatureLabel.text = [[formatter stringFromNumber:temperature] stringByAppendingString:@" °C"];
    //
    //    }else{
    //        temperatureView.hidden=YES;
    //    }
    temperatureView.hidden=YES;
    
    return cell;
}
- (CGSize)collectionViewContentSize
{
    NSInteger rowCount = [self.collections numberOfSections] / 2;
    // make sure we count another row if one is only partially filled
    if ([self.collections numberOfSections] % 2) rowCount++;
    
    CGFloat height = 20 +
    rowCount * 104 + (rowCount - 1) * 10 +
    20;
    
    return CGSizeMake(self.collections.bounds.size.width, height);
}

#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.item == 0)
        return;
    
    NSLog(@"indexPathTapped: %ld", (long)indexPath.item);
    
    NSIndexPath *indexPathForCell = [NSIndexPath indexPathForItem:indexPath.item-1 inSection:indexPath.section];

    NSLog(@"indexPathGot: %ld", (long)indexPathForCell.item);
    
    Room * room = (Room*)[_fetchedResultsController objectAtIndexPath:indexPathForCell];
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    self.currentRoomID = indexPathForCell.row;
      UIImageView* roomImageView = (UIImageView*)[cell viewWithTag:3];
        Tile * tile = [ResourceTiles generateTileForName:room.type];
        [roomImageView setImage:tile.tile_on];
    self.roomName = room.roomID;
    self.roomLabel = room.label;
    _indexForRoom = indexPath.item-1;
    //UIImageView* bg = (UIImageView*)[[collectionView cellForItemAtIndexPath:indexPath] viewWithTag:1];
    
    [self performSegueWithIdentifier:@"roomDevices" sender:self];
    
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
    if ([[segue identifier] isEqualToString:@"roomDevices"])
    {
        RoomsRootViewController *rpvc = (RoomsRootViewController*) segue.destinationViewController;
        //        SYDevicesViewController *vc = (SYDevicesViewController*) [segue destinationViewController];
        //        vc.headerName = self.roomName;
        //        vc.headerLabel = self.roomLabel;
        //         vc.currentRoomID = self.currentRoomID;
        id <NSFetchedResultsSectionInfo> sectionInfo = [[_fetchedResultsController sections] objectAtIndex:0];
        //        vc.rooms = [sectionInfo objects];
        rpvc.rooms = [sectionInfo objects];
        rpvc.indexForOpen = _indexForRoom;
        
        
    }  else if([segue.identifier isEqualToString:@"blindsROOMSegue"]){
        BlindsViewController *controller = (BlindsViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
        
    } else if([segue.identifier isEqualToString:@"RGBROOMSegue"]){
        RGBViewController *controller = (RGBViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
        
    } else if([segue.identifier isEqualToString:@"dimmingROOMSegue"]){
        DimmingViewController *controller = (DimmingViewController*) [segue destinationViewController];
        controller.brightness = [[_segueDevice getStateValueForStateName:@"brightness"] intValue];
        controller.device = _segueDevice;
        
    } else if([segue.identifier isEqualToString:@"HCAROOMSegue"]){
        HCAViewController *controller = (HCAViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
        
    } else if ([segue.identifier isEqualToString:@"LightChtomaticityROOMSegue"]){
        ChromaticityViewController *controller = (ChromaticityViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
    } else if ([segue.identifier isEqualToString:@"releROOMSegue"]){
        ReleViewController *controller = (ReleViewController*) [segue destinationViewController];
        controller.device = _segueDevice;
    } else if([segue.identifier isEqualToString:@"IR_klima"]){
        IRKlimaViewController *controller = (IRKlimaViewController*) [segue destinationViewController];
        controller.device = sender;
    } else if( [segue.identifier isEqualToString:@"IRControllerROOMSegue"]){
        IRControllerViewController *controller = (IRControllerViewController*) [segue destinationViewController];
        controller.device = sender;
    }

}



//-(void)connectionResponseOK:(NSObject *)returnedObject response:(id<Response>)responseType{
//
//
//
//    if ([responseType isKindOfClass:[ConfigurationResponse class]] ){
//        NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//        NSNumber* counter  = [defaults valueForKey:@"CCC"];
//        if (counter!=nil){
//            if (((NSNumber*)returnedObject).intValue==counter.intValue){
//                NSLog(@"counter is the same");
//                return;
//            }
//        }
//
//        NSLog(@"counter is different :%d, old counter:%d",((NSNumber*)returnedObject).intValue, counter.intValue);
//        [defaults setObject:((NSNumber*)returnedObject) forKey:@"CCC"];
//        [defaults synchronize];
//        //else clear Room db and get new rooms and devices in separat thread
//
//        // dispatch_async(queue,^{
//        NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadDataAsychn:) object:nil];
//        AppDelegate * app= (AppDelegate*)[[UIApplication sharedApplication] delegate];
//        if (app.loadingDevices==NO)[app.operationQueue addOperation:invocationOperation];
//
//
//
//        //});
//
//    } else{
//
//        if ([SYWebSocket sharedInstance].connected==NO){
//            [[SYWebSocket sharedInstance] connect];
//        }
//        URLConnector * url = [[URLConnector alloc] initWithAddressAndLoader:[Util getConfigUrl:@"configuration"] activityInd:activityInd];
//
//        ConfigurationResponse * configResponse = [[ConfigurationResponse alloc] init];
//
//        [url setParser:configResponse];
//        [url setConnectionListener:self];
//
//        [url ConnectAndGetData];
//
//
//    }
//
//}

-(void)loadDataAsychn:(NSMutableDictionary*)dict
{
    //    self.loader = [[DeviceLoaderSync alloc] init];
    //    [self.loader LoadDevices:self];
    //
    //    [self performSelectorOnMainThread:@selector(clearScenes) withObject:nil waitUntilDone:NO];
    
}
/*
 -(void)clearScenes{
 [CoreDataManager deleteAllObjects:@"Scene"];
 
 }
 -(void) connectionResponseFailed:(NSInteger)code{
 noServerFoundLabel.hidden=NO;
 
 }
 
 
 -(void)syncError:(NSNotification *)note{
 [_loaderDialog hide];
 
 }
 
 -(void)syncComplete:(NSNotification *)note{
 [_loaderDialog hide];
 isLoading=NO;
 
 rooms = [CoreDataManager readRooms];
 [collections reloadData];
 _tableView.hidden=YES;
 collections.hidden=NO;
 _tableView.userInteractionEnabled=NO;
 
 if ([rooms count]==1){
 Room * room = [rooms objectAtIndex:0];
 
 self.currentRoomID = 0;
 
 self.roomName = room.name;
 self.roomLabel = room.label;
 
 
 //_devices = [[NSMutableArray alloc] initWithArray:[CoreDataManager getDevicesInRoom:room.name]];
 //    [tableView reloadData];
 _tableViewDelegate = [[DevicesTableViewDelegate alloc] init];
 [_tableViewDelegate setView:self.view];
 [_tableView setDelegate:_tableViewDelegate];
 [_tableView setDataSource:_tableViewDelegate];
 [_tableViewDelegate setTableView:_tableView];
 //[_tableViewDelegate setDevices:_devices];
 [_tableViewDelegate setRoomID:room.name];
 [_tableViewDelegate.tableView reloadData];
 _tableView.hidden=NO;
 collections.hidden=YES;
 _tableView.userInteractionEnabled=YES;
 
 Device * dev = [[note userInfo] objectForKey:@"device"];
 //devices = [CoreDataManager getDevicesInRoom:headerName];
 for (int i=0; i < [_devices count]; i++){
 Device* mydev = [_devices objectAtIndex:i];
 
 if ([mydev.name isEqualToString:dev.name]){
 [_devices replaceObjectAtIndex:i withObject:dev];
 }
 }
 [_tableViewDelegate setDevices:_devices ];
 [_tableViewDelegate.tableView reloadData];
 
 
 }
 
 }
 -(void)syncCompleteLoader:(NSNotification *)note{
 [_loaderDialog hide];
 
 
 if ([_devices count]==0){
 
 rooms = [CoreDataManager readRooms];
 if (!rooms || [rooms count]==0) {
 [self dismissViewControllerAnimated:YES completion:nil];
 return;
 }
 if ([rooms count]==1){
 Room * room = [rooms objectAtIndex:0];
 
 self.currentRoomID = 0;
 
 self.roomName = room.name;
 self.roomLabel = room.label;
 
 
 //_devices = [[NSMutableArray alloc] initWithArray:[CoreDataManager getDevicesInRoom:room.name]];
 //[tableView reloadData];
 _tableViewDelegate = [[DevicesTableViewDelegate alloc] init];
 [_tableViewDelegate setView:self.view];
 [_tableView setDelegate:_tableViewDelegate];
 [_tableView setDataSource:_tableViewDelegate];
 [_tableViewDelegate setTableView:_tableView];
 //[_tableViewDelegate setDevices:_devices];
 [_tableViewDelegate setRoomID:room.name];
 [_tableViewDelegate.tableView reloadData];
 _tableView.hidden=NO;
 collections.hidden=YES;
 _tableView.userInteractionEnabled=YES;
 }
 
 Room* room = (Room*)[_fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
 _currentRoomID = 0;
 
 //_devices = [[NSMutableArray alloc] initWithArray:[CoreDataManager getDevicesInRoom:room.name]];
 //[_tableViewDelegate setDevices:_devices ];
 [_tableViewDelegate setRoomID:room.name];
 [_tableViewDelegate.tableView reloadData];
 
 isLoading=NO;
 }
 
 }
 
 -(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
 {
 
 if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
 
 [self.tableViewDelegate handleLongPress:gestureRecognizer];
 
 CGPoint p = [gestureRecognizer locationInView:self.tableView];
 
 NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:p];
 Device * device = [self.devices objectAtIndex:indexPath.row];
 
 NSLog(@"long press fired for %@", device.name);
 if (device.deviceState.locked!=nil)
 if([device.deviceState.locked boolValue]==YES) return;
 if ([device.actionsInfo valueForKey:@"red"]!=nil && [device.actionsInfo valueForKey:@"green"]!=nil && [device.actionsInfo valueForKey:@"blue"]!=nil && [device.actionsInfo valueForKey:@"demo"]!=nil && [device.actionsInfo valueForKey:@"brightness"]!=nil){
 if ([self.view viewWithTag:60]==nil){
 _rgb = [[RGBViewController alloc] initWithNibName:@"RGBViewController" bundle:nil device:device];
 _rgb.view.tag = 60;
 
 [self.view addSubview:_rgb.view];
 }
 }else{
 if ([self.view viewWithTag:50]==nil){
 if (device.secondaryActions!=nil && [device.secondaryActions count]>0 && ![device.secondaryActions containsObject:@"requested temperature"] && ![device.secondaryActions containsObject:@"correction"]){
 _actions = [[DeviceActionsViewController alloc] initWithNibName:@"DeviceActionsViewController" bundle:nil ];
 [_actions setDevice:device];
 _actions.view.tag = 50;
 [self.view addSubview:_actions.view];
 }else if ( device.deviceState.temperature!=nil && device.deviceState.openWindow!=nil && device.deviceState.openValve!=nil && device.deviceState.battery !=nil && device.deviceState.requestedTemperature !=nil && device.deviceState.openWindowSensitivity!=nil && device.deviceState.openWindowOffTime !=nil){
 if ([self.view viewWithTag:40]==nil){
 
 _thermView = [[ThermostatViewController alloc] initWithNibName:@"ThermostatViewController" bundle:nil device:device];
 _thermView.view.tag = 40;
 [self.view addSubview:_thermView.view];
 }
 }else if ( device.deviceState.correction !=nil && device.deviceState.mode !=nil && device.deviceState.temperature!=nil){
 if ([self.view viewWithTag:40]==nil){
 _heatView =[[HeatCoolAreaViewController alloc] initWithNibName:@"HeatCoolAreaViewController" bundle:nil device:device];
 
 _heatView.view.tag = 40;
 [self.view addSubview:_heatView.view];
 }
 
 }
 }
 }
 }
 }
 
 */
-(void)loadingStarted:(NSNotification *)note{
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    isLoading=YES;
}

-(IBAction)settingsPressed:(id)sender{
    [self performSegueWithIdentifier:@"settings" sender:self];
    
}

-(IBAction)viewScenesPressed:(id)sender{
    [self performSegueWithIdentifier:@"viewScenes" sender:self];
    
}

-(IBAction)viewFavouritesPressed:(id)sender{
    [self performSegueWithIdentifier:@"viewFavourites" sender:self];
    
}

-(IBAction)viewCamerasPressed:(id)sender{
    [self performSegueWithIdentifier:@"viewCameras" sender:self];
    
}
- (IBAction)helpPressed:(id)sender {
    [self performSegueWithIdentifier:@"showManual" sender:self];
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}




-(void)complete:(NSMutableDictionary *)Dict{
    
    //    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self userInfo:Dict];
    
}
-(void)startLoading:(NSDictionary*)object{
    //  [[NSNotificationCenter defaultCenter] postNotificationName:@"loadingStarted" object:self userInfo:object];
    
}

- (void)cleaningDB:(NSNotification*)notification {
    //self.rooms = @[];
    [self.tableView reloadData];
    [self.collections reloadData];
}


@end
