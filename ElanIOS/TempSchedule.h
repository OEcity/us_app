//
//  TempSchedule.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 1/21/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TempScheduleMode.h"
#import "TempScheduleDay.h"

@interface TempSchedule : NSObject

@property (nonatomic, retain) NSMutableArray * modes;
@property (nonatomic, retain) NSMutableArray * days;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, retain) NSNumber* hysteresis;
@property (nonatomic, copy) NSString* serverId;
- (TempScheduleDay*) getDayByPosition:(int) position;
- (void) setDayOnPosition:(TempScheduleDay*)day position:(int)position;
@end
