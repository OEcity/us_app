//
//  OneWeekScheduleViewController.m
//  iHC-MIRF
//
//  Created by admin on 24.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//


#import "TempScheduleDay.h"
#import "TempSchedule.h"
#import "OneWeekScheduleViewController.h"
#import "HUDWrapper.h"
#import "SYAPIManager.h"



@interface OneWeekScheduleViewController ()
//Frames for showing temperature maps
@property (weak, nonatomic) IBOutlet UIView *mondayView;
@property (weak, nonatomic) IBOutlet UIView *tuesdayView;
@property (weak, nonatomic) IBOutlet UIView *wednesdayView;
@property (weak, nonatomic) IBOutlet UIView *thursdayView;
@property (weak, nonatomic) IBOutlet UIView *fridayView;
@property (weak, nonatomic) IBOutlet UIView *saturdayView;
@property (weak, nonatomic) IBOutlet UIView *sundayView;

//Place for editable temperature map and copyTo buttons
@property (weak, nonatomic) IBOutlet UIView *setterView;
//Editable temperature map view
@property (weak, nonatomic) IBOutlet OneDayHeatTimeAdjustView *setterScheduleView;
@property (weak, nonatomic) IBOutlet UILabel *setterViewDayLabel;
@property (weak, nonatomic) IBOutlet UIView *saveButtonView;

//model variables
@property (nonatomic, retain) TempSchedule * tempSchedule;
@property (nonatomic, retain) TempScheduleDay * mDay;

@property (nonatomic, retain) HUDWrapper * loaderDialog;

//local variables
@property (strong, nonatomic) NSArray * arrayDaysView;
@property (strong, nonatomic) NSArray * buttonsCopyToArray;
@property (strong, nonatomic) NSArray * modesTemperatures;

@property (weak, nonatomic) IBOutlet UIButton *mondayCopyTo;
@property (weak, nonatomic) IBOutlet UIButton *tuesdayCopyTO;
@property (weak, nonatomic) IBOutlet UIButton *wednesDayCopyTo;
@property (weak, nonatomic) IBOutlet UIButton *thursdayCopyTO;
@property (weak, nonatomic) IBOutlet UIButton *fridayCopyTo;
@property (weak, nonatomic) IBOutlet UIButton *saturdayCopyTo;
@property (weak, nonatomic) IBOutlet UIButton *sundayCopyTo;


@property NSInteger testModePos;
@property NSInteger testdayInWeek;
@property (strong, nonatomic) TempDayMode *testTempMode;
@end

@implementation OneWeekScheduleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //NSLog(@"%@",[UIFont fontNamesForFamilyName:@"Roboto"]);
    NSLog(@"%@",_schedule);
    
    
    _tempSchedule = [[TempSchedule alloc] init];
    if ([_schedule objectForKey:@"schedule"]!=nil)
        _tempSchedule.days =  [self getDays:_schedule];
    if ([_schedule objectForKey:@"modes"]!=nil)
        _tempSchedule.modes = [self getModes:_schedule];
    [_tempSchedule setServerId:[_schedule objectForKey:@"id"]];
    [_tempSchedule setHysteresis:[_schedule objectForKey:@"hysteresis"]];
    [_tempSchedule setName:[_schedule objectForKey:@"label"]];
    
    
    _arrayDaysView = [NSArray arrayWithObjects:_mondayView, _tuesdayView, _wednesdayView, _thursdayView, _fridayView, _saturdayView, _sundayView, nil];
    
    _buttonsCopyToArray = [NSArray arrayWithObjects:_mondayCopyTo, _tuesdayCopyTO, _wednesDayCopyTo, _thursdayCopyTO, _fridayCopyTo,_saturdayCopyTo,_sundayCopyTo, nil];
    
    _modesTemperatures = [NSArray arrayWithObjects:
                          [[[_schedule objectForKey:@"modes"] objectForKey:@"1"] objectForKey:@"min"],
                          [[[_schedule objectForKey:@"modes"] objectForKey:@"2"] objectForKey:@"min"],
                          [[[_schedule objectForKey:@"modes"] objectForKey:@"3"] objectForKey:@"min"],
                          [[[_schedule objectForKey:@"modes"] objectForKey:@"4"] objectForKey:@"min"],
                          nil];
    _setterScheduleView.dataSource = self;
    _setterScheduleView.delegate = self;
    
    
    [self initDaysView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





-(void)initDaysView{
    int i = 0;
    
    for (OneDayHeatTimeSheduleView *view in _arrayDaysView){
        _mDay = [_tempSchedule getDayByPosition:i];
        [view repaintDay:_mDay];
        i++;
    }
    
    [_setterView setHidden:YES];
    [_saveButtonView setHidden:YES];
}

- (IBAction)adjustButtonPressed:(id)sender {
    
    [self writeData];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"addHeatingMode"]){
        if ([[segue.destinationViewController class]hash] ==  [[AddHeatingModeViewController class] hash]){
        
            AddHeatingModeViewController *vc =  segue.destinationViewController;
            vc.modePos = -1;
            vc.delegate = self;
            
            NSMutableArray *temperaturesString = [[NSMutableArray alloc] init];
            
            for(NSNumber *temperature in _modesTemperatures){
                NSString *stringValue =  [temperature.stringValue stringByAppendingString:@" °F"];
                [temperaturesString addObject:stringValue];
            }
            
            vc.temperatures = [temperaturesString copy];
             
        }
    }
    else if ([segue.identifier  isEqual: @"editHeatingMode"]){
        
        EditHeatingModeViewController *vc =  segue.destinationViewController;
        vc.day = _testdayInWeek;
        vc.modePos = _testModePos;
        vc.tempDayMode = _testTempMode;
        
        NSMutableArray *temperaturesString = [[NSMutableArray alloc] init];
        
        for(NSNumber *temperature in _modesTemperatures){
            NSString *stringValue =  [temperature.stringValue stringByAppendingString:@" °F"];
            [temperaturesString addObject:stringValue];
        }
        vc.temperatures = [temperaturesString copy];

        
        vc.delegate = self;
        
    }
    
}

- (IBAction)saveButtonPressed:(id)sender {
    [_setterView setHidden:YES];
    [_saveButtonView setHidden:YES];
    
    int i = 0;
    for(UIButton *copyTo in _buttonsCopyToArray){
        if(copyTo.state == UIControlStateDisabled){
           
            [_tempSchedule setDayOnPosition: _setterScheduleView.mDay position:i];
        }
        else if(copyTo.state == UIControlStateSelected){
            TempScheduleDay *anotherDay = [_setterScheduleView.mDay copy];
            switch (i) {
                case 0:
                    anotherDay.id = @"monday";
                    break;
                case 1:
                    anotherDay.id = @"tuesday";
                    break;
                case 2:
                    anotherDay.id = @"wednesday";
                    break;
                case 3:
                    anotherDay.id = @"thursday";
                    break;
                case 4:
                    anotherDay.id = @"friday";
                    break;
                case 5:
                    anotherDay.id = @"saturday";
                    break;
                case 6:
                    anotherDay.id = @"sunday";
                    break;
                default: break;
            }
            [_tempSchedule setDayOnPosition:anotherDay position:i];
        }
        i++;
    }

    
    
    i = 0;
    
    
    for (OneDayHeatTimeSheduleView *view in _arrayDaysView){
        _mDay = [_tempSchedule getDayByPosition:i];
        [view repaintDay:_mDay];
        i++;
    }

    
    [self enableAllCopyToButtons];
    [self setUnselectedAllCopyToButton];

    
}

-(void) writeData{
    NSMutableDictionary * root = [[NSMutableDictionary alloc] init];
    
    if (_tempSchedule.serverId != nil)
        [root setObject:_tempSchedule.serverId forKey:@"id"];
    
    [root setObject:_tempSchedule.hysteresis forKey:@"hysteresis"];
    [root setObject:_tempSchedule.name forKey:@"label"];
    
    NSMutableDictionary * modes = [[NSMutableDictionary alloc] init];
    for (TempScheduleMode *mode in _tempSchedule.modes) {
        NSDictionary * minMax = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 mode.min,@"min",
                                 mode.max,@"max",nil];
        [modes setObject:minMax forKey:[mode.id stringValue]];
    }
    
    [root setObject:modes forKey:@"modes"];
    
    NSMutableArray* modesA;
    NSMutableDictionary* temp = [[NSMutableDictionary alloc] init];
    for (TempScheduleDay* day in _tempSchedule.days) {
        modesA = [[NSMutableArray alloc] init];
        
        for (TempDayMode *mode in day.modes) {
            NSDictionary * modeObj = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithInt:[mode.endTime intValue]-[mode.startTime intValue]], @"duration",
                                      mode.mode , @"mode", nil];
            [modesA addObject:modeObj];
            
        }
        [temp setObject:modesA forKey:day.id];
        
    }
    [root setObject:temp forKey:@"schedule"];
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:root
                                                       options:(NSJSONWritingOptions)  (NSJSONWritingPrettyPrinted)
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        
    } else {
        NSLog(@"%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    }
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    [_loaderDialog showWithLabel:NSLocalizedString(@"temp_schedule_dialog_wait_tile", nil)];
    
    [[SYAPIManager sharedInstance] postScheduleWithDictionary:root success:^(AFHTTPRequestOperation * operation , id response){
        
        [_loaderDialog hide];
        NSLog(@"updated succesfully");
       //UPRAVIT HERE
        }
                                                      failure:^(AFHTTPRequestOperation * operation , NSError * error){
                                                          [_loaderDialog hide];
                                                          UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                                                                            message:NSLocalizedString(@"temp_schedule_failed_save", nil)
                                                                                                           delegate:nil
                                                                                                  cancelButtonTitle:@"OK"
                                                                                                  otherButtonTitles:nil];
                                                          [message show];
                                                          
                                                          NSLog(@"update err");
                                                      }];

}

-(NSMutableArray*)getDays:(NSDictionary*)dictionary{
    TempScheduleDay * day;
    TempDayMode * dayMode;
    NSMutableArray * days = [[NSMutableArray alloc] init];
    for (NSString * dayName in [dictionary objectForKey:@"schedule"]) {
        
        day = [[TempScheduleDay alloc] init];
        day.id = dayName;
        
        day.modes = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dayObject in [[dictionary objectForKey:@"schedule"] objectForKey:dayName]) {
            
            dayMode = [[TempDayMode alloc] init];
            dayMode.duration = [dayObject valueForKey:@"duration"];
            dayMode.mode = [dayObject valueForKey:@"mode"];
            
            [day.modes addObject:dayMode];
        }
        
        [day setupModes];
        [days addObject:day];
    }
    return days;
}

-(NSMutableArray*)getModes:(NSDictionary*)dictionary{
    
    TempScheduleMode* mode;
    NSMutableArray * modes = [[NSMutableArray alloc] init];
    NSDictionary * modesDictionary = [dictionary objectForKey:@"modes"];
    for (NSString* fieldName in modesDictionary) {
        
        NSDictionary * object = [modesDictionary objectForKey:fieldName];
        mode = [[TempScheduleMode alloc] init];
        mode.max = [[NSNumber alloc] initWithInt:35];
        mode.min =  [object valueForKey:@"min"];
        
        mode.id = [[NSNumber alloc] initWithInt:[fieldName intValue]] ;
        [modes addObject:mode];
    }
    return modes;
}
- (IBAction)mondayRiseUp:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_tempSchedule getDayByPosition:0];
        TempScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:0];
        [_setterViewDayLabel setText:@"MON"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        _setterScheduleView.mModePosition = -1;
        [self disableOneCopyToButton:0];
        //_setterScheduleView.mModePosition = 0;
        
    }
}
- (IBAction)tuesdayRiseUP:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_tempSchedule getDayByPosition:1];
        TempScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:1];
        [_setterViewDayLabel setText:@"TUE"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:1];
        //_setterScheduleView.mModePosition = 0;
    }

}
- (IBAction)wednesdayRiseUp:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_tempSchedule getDayByPosition:2];
        TempScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:2];
        [_setterViewDayLabel setText:@"WED"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:2];
        //_setterScheduleView.mModePosition = 0;
    }

}
- (IBAction)thursdayRiseUp:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_tempSchedule getDayByPosition:3];
        TempScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:3];
        [_setterViewDayLabel setText:@"THU"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:3];
        //_setterScheduleView.mModePosition = 0;
    }

}
- (IBAction)fridayRiseUp:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_tempSchedule getDayByPosition:4];
        TempScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:4];
        [_setterViewDayLabel setText:@"FRI"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:4];
        //_setterScheduleView.mModePosition = 0;
    }

}
- (IBAction)saturdayRiseUp:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_tempSchedule getDayByPosition:5];
        TempScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:5];
        [_setterViewDayLabel setText:@"SAT"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:5];
        //_setterScheduleView.mModePosition = 0;
    }

}
- (IBAction)sundayRiseUp:(id)sender {
    if([_setterView isHidden]){
        _mDay = [_tempSchedule getDayByPosition:6];
        TempScheduleDay *anotherDay = [_mDay copy];
        [_setterScheduleView paintDateFirstTime:anotherDay dayInWeek:6];
        [_setterViewDayLabel setText:@"SUN"];
        [_setterView setHidden:NO];
        [_saveButtonView setHidden:NO];
        [self disableOneCopyToButton:6];
        //_setterScheduleView.mModePosition = 0;
    }

}
- (IBAction)dismissSetterView:(id)sender {
    [_setterView setHidden:YES];
    [_saveButtonView setHidden:YES];
    
    int i = 0;
    for (OneDayHeatTimeSheduleView *view in _arrayDaysView){
        _mDay = [_tempSchedule getDayByPosition:i];
        [view repaintDay:_mDay];
        i++;
    }
    [self enableAllCopyToButtons];
    [self setUnselectedAllCopyToButton];

    //NOW ALL ENABLED, and ZERO STATE and ARRAY ZERO state
}

-(void)enableAllCopyToButtons{
    for (UIButton *button in _buttonsCopyToArray){
        [button setEnabled:YES];
    }
}
-(void)disableOneCopyToButton:(NSUInteger)index{
    UIButton *button = [_buttonsCopyToArray objectAtIndex:index];
    //[button setSelected:YES];
    [button setEnabled:NO];
    
    
}
-(void)setUnselectedAllCopyToButton{
    for (UIButton *button in _buttonsCopyToArray){
        [button setSelected:NO];
    }
}



- (IBAction)copyTo:(UIButton *)sender {
    sender.selected = ![sender isSelected];
}




#pragma mark temperature data source methods

-(NSNumber *)days:(NSUInteger)index{
    return [_modesTemperatures objectAtIndex:index];
    
}

#pragma mark add heating mode delegate methods

-(void)addHeatingModeToday:(NSInteger)day comesFromLTouch:(BOOL)edit tempDayMode:(TempDayMode *)tempdayMode position:(NSInteger)position erase:(BOOL)erase{
    if(edit){
        if (!erase){
            [_setterScheduleView.mDay editMode:tempdayMode modePosition:(int)position];
            
        }
        else{
            [_setterScheduleView.mDay deleteMode:(int)position];
        }
        [_setterScheduleView repaintContent];
        
    }
    else{
        if (!erase){
            if(day>=0 && day<7){
                TempScheduleDay* tempScheduleDay = [_tempSchedule getDayByPosition:(int)day];
                [tempScheduleDay editMode:tempdayMode modePosition:(int)position];
            }
        }
        else{
            if(day>=0 && day<7){
                TempScheduleDay* tempScheduleDay = [_tempSchedule getDayByPosition:(int)day];
                [tempScheduleDay deleteMode:position];
            
            }
        
        }
        int i = 0;
        for (OneDayHeatTimeSheduleView *view in _arrayDaysView){
            _mDay = [_tempSchedule getDayByPosition:i];
            [view repaintDay:_mDay];
            i++;
        }
    }
    
}

#pragma mark editing mode delegate methods

-(void)putEditingOnScreen:(TempDayMode *)mode modePos:(int)modePos dayInWeek:(int)day{

    if (self.isViewLoaded && self.view.window) {
        _testModePos = modePos;
        _testdayInWeek = day;
        _testTempMode = mode;
        [self performSegueWithIdentifier:@"editHeatingMode" sender:nil];
        
        


    }
    
}

@end
