//
//  AddDeviceModeViewController.m
//  iHC-MIRF
//
//  Created by admin on 15.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "AddDeviceModeViewController.h"

@implementation AddDeviceModeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_dayInWeekPickerView setHidden:YES];
    [_timePickerView setHidden:YES];
    
    //initialize settings Bool
    _selectedDay = NO;
    _selectedFrom = NO;
    _selectedTill = NO;
    _selectedMode = NO;
    
    //initialize day picker
    _daysInWeekForPicker = @[@"Monday", @"Tuesday", @"Wednesday", @"Thursday", @"Friday", @"Saturday", @"Sunday"];
    _dayInWeekPicker.dataSource = self;
    _dayInWeekPicker.delegate = self;
    
    //initialize time picker
    _timePicker.dataSource = self;
    _timePicker.delegate = self;
    
    _odCausedOpen = NO;
    
    _formatter = [[NSNumberFormatter alloc] init];
    
    [_formatter setNumberStyle:NSNumberFormatterNoStyle];
    [_formatter setMaximumIntegerDigits:2];
    [_formatter setMinimumIntegerDigits:2];
    
    _tableView.delegate = self;
    _tableView.dataSource = self;
    if (_shouldShowZero){
        [_heightTableView setConstant:(_temperatures.count * 40.0f) + 2.0f];
    }
    else {
        [_heightTableView setConstant:((_temperatures.count -1) * 40.0f) + 2.0f];
    }
    
    
}

- (IBAction)saveButtonPressed:(id)sender {
    if(_selectedDay && _selectedMode && _selectedTill && _selectedFrom){
        if (_timeTo > _timeFrom){
            [self prepareAndSendDataToDelegate];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"To time is bigger than from time." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
    }
    else{
        //warning terrible English
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Values was not set properly." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    
    
    
    
}
-(void)prepareAndSendDataToDelegate{
    
    TempDayMode *newMode = [[TempDayMode alloc] init];
    
    newMode.mode = [[NSNumber alloc] initWithInteger:_mode];
    newMode.startTime = [[NSNumber alloc] initWithInteger:_timeFrom];
    newMode.endTime = [[NSNumber alloc] initWithInteger:_timeTo];
    newMode.duration = [[NSNumber alloc] initWithInteger:_timeTo-_timeFrom];
    if (_delegate!=nil)
        [_delegate addDeviceModeToday:_day comesFromLTouch:NO tempDayMode:newMode position:(int)_modePos erase:NO];
    
}

- (IBAction)odButtonTouched:(id)sender {
    [_timePickerView setHidden:NO];
    [_timePicker selectRow:_timeFrom/60 inComponent:0 animated:NO];
    [_timePicker selectRow:_timeFrom % 60 inComponent:1 animated:NO];
    _odCausedOpen = YES;
    
}
- (IBAction)doButtonTouched:(id)sender {
    [_timePickerView setHidden:NO];
    [_timePicker selectRow:_timeTo/60 inComponent:0 animated:NO];
    [_timePicker selectRow:_timeTo % 60 inComponent:1 animated:NO];
    _odCausedOpen = NO;
    
}
- (IBAction)dayInWeekChoosed:(id)sender {
    [_dayInWeekPickerView setHidden:YES];
    _day = [_dayInWeekPicker selectedRowInComponent:0];
    [_dayLabel setText:[_daysInWeekForPicker objectAtIndex:_day]];
    _selectedDay = YES;
    
}
- (IBAction)timeChoosed:(id)sender {
    [_timePickerView setHidden:YES];
    
    if (_odCausedOpen){
        NSNumber *help = [[NSNumber alloc] initWithInteger:[_timePicker selectedRowInComponent:0]];
        NSMutableString *stringToShow = [[NSMutableString alloc] initWithString:@"From "];
        [stringToShow appendString:[_formatter stringFromNumber:help]];
        [stringToShow appendString:@":"];
        NSNumber *help2 = [[NSNumber alloc] initWithInteger:[_timePicker selectedRowInComponent:1]];
        [stringToShow appendString:[_formatter stringFromNumber:help2]];
        
        [_odLabel setText: stringToShow];
        _timeFrom = ([_timePicker selectedRowInComponent:0]*60) + [_timePicker selectedRowInComponent:1];
        _selectedFrom = YES;
        
    }
    else{
        
        NSNumber *help = [[NSNumber alloc] initWithInteger:[_timePicker selectedRowInComponent:0]];
        NSMutableString *stringToShow = [[NSMutableString alloc] initWithString:@"Till "];
        [stringToShow appendString:[_formatter stringFromNumber:help]];
        [stringToShow appendString:@":"];
        NSNumber *help2 = [[NSNumber alloc] initWithInteger:[_timePicker selectedRowInComponent:1]];
        [stringToShow appendString:[_formatter stringFromNumber:help2]];
        
        [_doLabel setText: stringToShow];
        _timeTo = ([_timePicker selectedRowInComponent:0]*60) + [_timePicker selectedRowInComponent:1];
        if (_timeTo == 0){
            _timeTo = 1440;
        }
        _selectedTill = YES;
        
        
    }
    _odCausedOpen = NO;
    
}
- (IBAction)denButtonPressed:(id)sender {
    [_dayInWeekPickerView setHidden:NO];
}




#pragma mark day in week picker data source and delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (pickerView == _timePicker){
        return 2;
    }
    else return 1;
}

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView == _timePicker){
        if (component == 1){
            return 60;
        }
        else return 24;
    }
    else return _daysInWeekForPicker.count;
    
    
}


-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    if (pickerView == _timePicker){
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width/2-25, 44)];
        label.backgroundColor = [UIColor blackColor];
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont fontWithName:@"Roboto-Light" size:18];
        if (component == 0){
            label.textAlignment = NSTextAlignmentRight;
            label.text = [NSString stringWithFormat:@"%li",(long)(row)];
            
        }
        else {
            label.textAlignment = NSTextAlignmentLeft;
            label.text = [_formatter stringFromNumber:[NSNumber numberWithInteger:row]];
        }
        
        return label;
        
    }
    else{
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 44)];
        label.backgroundColor = [UIColor blackColor];
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont fontWithName:@"Roboto-Light" size:18];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = _daysInWeekForPicker[row];
        return label;
    }
}

#pragma mark table view delegate, data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_shouldShowZero){
        return [_temperatures count];
    }
    else{
        return [_temperatures count] - 1;
    }
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_shouldShowColor){
        AddModeColorDetailTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"colorDetail" forIndexPath:indexPath];
        if (_shouldShowZero){
            [cell.label setText:[_temperatures objectAtIndex:indexPath.row]];
            [cell.colorView setBackgroundColor:[_colors objectAtIndex:indexPath.row]];
        }
        else{
            [cell.label setText:[_temperatures objectAtIndex:indexPath.row + 1]];
            [cell.colorView setBackgroundColor:[_colors objectAtIndex:indexPath.row + 1]];
        }
        
        
        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        [cell setSelectedBackgroundView:bgColorView];
        
        return cell;

    }
    else{
        AddModeNoDetailTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"noDetail" forIndexPath:indexPath];
        if (_shouldShowZero){
            [cell.label setText:[_temperatures objectAtIndex:indexPath.row]];
        }
        else{
            [cell.label setText:[_temperatures objectAtIndex:indexPath.row + 1]];
        }

        UIView *bgColorView = [[UIView alloc] init];
        bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
        [cell setSelectedBackgroundView:bgColorView];
        
        return cell;

        
    }
    /*AddModeTemperatureDetailTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"temperatureDetail" forIndexPath:indexPath];
    switch (indexPath.row){
        case 0:
            [cell.label setText:@"Minimum"];
            [cell.temperatureLabel setTextColor:[UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f]];
            [cell.temperatureLabel setHighlightedTextColor:[UIColor whiteColor]];
            break;
        case 1:
            [cell.label setText:@"Attenutation"];
            [cell.temperatureLabel setTextColor:[UIColor colorWithRed:147.0f/255.0f green:162.0f/255.0f blue:108.0f/255.0f alpha:1.0f]];
            break;
        case 2:
            [cell.label setText:@"Normal"];
            [cell.temperatureLabel setTextColor:[UIColor colorWithRed:245.0f/255.0f green:134.0f/255.0f blue:60.0f/255.0f alpha:1.0f]];
            break;
        case 3:
            [cell.label setText:@"Comfort"];
            [cell.temperatureLabel setTextColor:[UIColor colorWithRed:248.0f/255.0f green:45.0f/255.0f blue:38.0f/255.0f alpha:1.0f]];
            break;
    }
    
    [cell.temperatureLabel setText:[_temperatures objectAtIndex:indexPath.row]];*/
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    _selectedMode = true;
    if (_shouldShowZero){
        _mode = indexPath.row;
    }
    else{
        _mode = indexPath.row + 1;
    }
    
    
}


@end
