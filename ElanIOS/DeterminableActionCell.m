//
//  DeterminableActionCell.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 19/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "DeterminableActionCell.h"
#import <AudioToolbox/AudioToolbox.h>
//#import "Brightness.h"
#import "ChangeTime.h"
#import "TouchDownGestureRecognizer.h"
#import "Util.h"
#import "SceneAction.h"
#import "Action.h"
@interface DeterminableActionCell ()
{
    NSTimer *_timer;
    NSDictionary* translationNames;
}
- (void)_timerFired:(NSTimer*)info;

@end
@implementation DeterminableActionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}


-(id) initRGB:(NSArray*)sceneActions WithOwner:(id) owner{
    _expanded = NO;
    _expandedHeight = 0.0f;
    
    CGFloat expandedHeightTemp;
    Type typeTemp = kRgb;
    expandedHeightTemp = 162.0f;
    
    NSString * cellName =[self imageTypeEnumToString:typeTemp];
    self = (DeterminableActionCell*)[[[NSBundle mainBundle] loadNibNamed:cellName owner:owner options:nil] objectAtIndex:0];
    _sceneActionsViewController = (SYSceneActionsViewController*)owner;
    // Initialize variables
    _type = typeTemp;
    _expandedHeight = expandedHeightTemp;
    
    translationNames = [[Util generateNames] copy];
    [self.name setText: NSLocalizedString(@"rgb", @"")];
    //        [self.name setText:@"RGB"];
    
    _itemName = @"RGB";
    self.height = 72.0f;
    
    self.showsReorderControl=YES;
    UILongPressGestureRecognizer *lpgr;
    lpgr = [[UILongPressGestureRecognizer alloc]
            initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.5; //seconds
    lpgr.delegate = self;
    [self.view addGestureRecognizer:lpgr];
    
    TouchDownGestureRecognizer *touchDown = [[TouchDownGestureRecognizer alloc] initWithTarget:self action:@selector(handleTouchDown:)];
    [self.colorPicker addGestureRecognizer:touchDown];
    NSNumber*red= 0;
    NSNumber*green= 0;
    NSNumber*blue= 0;
    for (SceneAction* ac in sceneActions){
        if ([ac.action.name isEqualToString:@"red"] && ac.value != nil){
            red= ac.value;
        } else if ([ac.action.name isEqualToString:@"green"] && ac.value != nil){
            green= ac.value;
        } else if ([ac.action.name isEqualToString:@"blue"] && ac.value != nil){
            blue= ac.value;
        }
    }
    UIColor * color = [UIColor colorWithRed:red.intValue/255.0 green:green.intValue/255.0 blue:blue.intValue/255.0 alpha:1.0f];
    [self findPixel:color];
    _color =color;
    [self makeTimeInvisibleWithAlert:NO];
    return self;
}


-(id)initWithAction:(SceneAction*)sceneAction owner:(id)owner{
    _expanded = NO;
    Action* action = sceneAction.action;
    _expandedHeight = 0.0f;
    
    CGFloat expandedHeightTemp;
    Type typeTemp = kPlain;
    if (action.type == nil){
        typeTemp = kPlain;
        expandedHeightTemp=72.0f;
    }else if ([action.type isEqualToString:@"int"]){
        typeTemp = kTime;
        expandedHeightTemp = 213.0f;
        if ([action.name isEqualToString:@"brightness"]){
            typeTemp = kBrightness;
            expandedHeightTemp = 120.0f;
        }
    }else {
        typeTemp = kOn;
        expandedHeightTemp = 120.0f;
    }
    
    NSString * cellName =[self imageTypeEnumToString:typeTemp];
    self = (DeterminableActionCell*)[[[NSBundle mainBundle] loadNibNamed:cellName owner:owner options:nil] objectAtIndex:0];
    _sceneActionsViewController = (SYSceneActionsViewController*)owner;
    // Initialize variables
    _type = typeTemp;
    _expandedHeight = expandedHeightTemp;
    
    translationNames = [[Util generateNames] copy];
    NSString* value = [translationNames objectForKey:action.name];
    
    if(value)
        [self.name setText: NSLocalizedString(value, @"")];
    else
        [self.name setText:action.name];
    
    _itemName =action.name;
    self.height = 72.0f;
    
    self.showsReorderControl=YES;
    UILongPressGestureRecognizer *lpgr;
    lpgr = [[UILongPressGestureRecognizer alloc]
            initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.5; //seconds
    lpgr.delegate = self;
    [self.view addGestureRecognizer:lpgr];
    switch (_type) {
        case kBrightness:{
            float max = [action.max  floatValue];
            [_slider setMaximumValue:max];
            float min = [action.min floatValue];
            [_slider setMinimumValue:min];
            _stepValue = [action.step floatValue];
            _slider.value = (max-min)/2 + min;
            float newStep = roundf((_slider.value) / self.stepValue);
            _slider.value = newStep * _stepValue;
            if (sceneAction.value!=nil && ![sceneAction.value isKindOfClass:[NSNull class]]){
                NSNumber * value = sceneAction.value;
                _slider.value = value.floatValue;
            }
            _lSliderValue.text = [NSString stringWithFormat:@"%.0f%%",(_slider.value/_slider.maximumValue)*100];
            UIImage *sliderLeftTrackImage = [UIImage imageNamed: @"action_slider_on.png"] ;
            UIImage *sliderRightTrackImage = [UIImage imageNamed: @"action_slider_off.png"];
            UIImage *sliderThumb = [UIImage imageNamed: @"action_thumb.png"];
            sliderLeftTrackImage = [sliderLeftTrackImage resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
            sliderRightTrackImage = [sliderRightTrackImage resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
            
            [_slider setMinimumTrackImage: sliderLeftTrackImage forState: UIControlStateNormal];
            [_slider setMaximumTrackImage: sliderRightTrackImage forState: UIControlStateNormal];
            [_slider setThumbImage:sliderThumb forState: UIControlStateNormal];
            [self makeTimeInvisibleWithAlert:NO];
            break;
        }
        case kTime:{
            NSInteger min = [action.min integerValue];
            NSInteger max = [action.max integerValue];
            _timerStep = [action.step integerValue];
            
            for (int i=1; i < 7; i++){
                UIButton * but = (UIButton*)[self.view viewWithTag:i];
                if (i%2!=0)[but setBackgroundImage:[UIImage imageNamed:@"action_arrow_top_on"] forState:UIControlStateHighlighted];
                else [but setBackgroundImage:[UIImage imageNamed:@"action_arrow_bot_on"] forState:UIControlStateHighlighted];
                lpgr = [[UILongPressGestureRecognizer alloc]
                        initWithTarget:self action:@selector(handleLongPress:)];
                lpgr.minimumPressDuration = 0.5; //seconds
                lpgr.delegate = self;
                [but addGestureRecognizer:lpgr];
                
            }
            
            if (min> max){
                min=0;
                max=0;
                _timerStep = 0;
                break;
            }
            if (min<60){
                [_secondsLabel setText:[NSString stringWithFormat:@"%02ld", (long)min]];
            }else if (min< 3600){
                [_minutesLabel setText:[NSString stringWithFormat:@"%02ld", (long)min]];
            }else{
                [_hoursLabel setText:[NSString stringWithFormat:@"%02ld", (long)min]];
            }
            _timerMax = max;
            _timerMin = min;
            _time =[_hoursLabel.text intValue]*3600+[_minutesLabel.text intValue]*60+[_secondsLabel.text intValue];
            
            if (sceneAction.value!=nil && ![sceneAction.value isKindOfClass:[NSNull class]]){
                NSNumber * timeDelayTime = sceneAction.value;
                if (timeDelayTime!=nil){
                    _time = timeDelayTime.intValue;
                    if (_time > _timerMin && _time < _timerMax){
                        [_secondsLabel setText:[NSString stringWithFormat:@"%02d", (int)_time%60]];
                        [_minutesLabel setText:[NSString stringWithFormat:@"%02d", (int)(_time/60)%3600]];
                        [_hoursLabel setText:[NSString stringWithFormat:@"%02d", (int)_time/3600]];
                    }
                }
            }
            if (max < 3600){
                [_hoursLabel setEnabled:NO];
                [_hoursLabel setText:@"--"];
                
            }
            if (max<60){
                [_minutesLabel setEnabled:NO];
                [_minutesLabel setText:@"--"];
            }
            [self makeTimeInvisibleWithAlert:NO];
            break;
        }
        case kOn:
            [_offLabel setText:NSLocalizedString(@"sceneOff", nil)];
            [_onLabel setText:NSLocalizedString(@"sceneOn", nil)];
            if (sceneAction.value!=nil && ![sceneAction.value isKindOfClass:[NSNull class]]){
                NSNumber * res = sceneAction.value;
                if (res.boolValue==YES){
                    [self OnButtonPressed:self];
                }else{
                    [self OffButtonPressed:self];
                }
            }
            [self makeTimeInvisibleWithAlert:NO];
            break;
        default:
            break;
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}


-(NSString*) imageTypeEnumToString:(Type)enumVal
{
    NSArray *imageTypeArray = [[NSArray alloc] initWithObjects:kTypeArray];
    return [imageTypeArray objectAtIndex:enumVal];
}

#pragma mark RGB cell settings

-(void)handleTouchDown:(TouchDownGestureRecognizer *)sender{
    NSLog(@"Down");
    CGPoint coords = [sender locationInView:_colorView.superview];
    float pickerHeight = _colorPicker.frame.size.height/2 -10;
    float pickerWidth = _colorPicker.frame.size.width/2 -10;
    if (coords.y >= _colorView.frame.origin.y + pickerHeight && coords.y < _colorView.frame.origin.y - pickerHeight + _colorView.frame.size.height-5 && coords.x >=  pickerWidth && coords.x <  _colorView.frame.size.width - pickerWidth ){
        [_colorPicker setCenter:CGPointMake(coords.x, _colorView.center.y-5)];
    }else{
        if (coords.x >=  pickerWidth && coords.x <  _colorView.frame.size.width - pickerWidth ){
            [_colorPicker setCenter:CGPointMake(coords.x, _colorView.center.y-5)];
        }else{
            [_colorPicker setCenter:CGPointMake(_colorPicker.center.x, _colorView.center.y-5)];
        }
    }
    if (sender.state == UIGestureRecognizerStateEnded){
        NSLog(@"ended");
        [self saveColorPoint];
    }
}

-(void) saveColorPoint{
    CGPoint colorCoords = CGPointMake(_colorPicker.center.x, _colorPicker.center.y - _colorView.frame.origin.y);
    _color = [self colorOfPoint:colorCoords];
}

-(IBAction)setColorButton:(id)sender{
    UIButton * button = (UIButton*) sender;
    NSArray * arr = [[NSArray alloc] initWithObjects: [[NSNumber alloc] initWithInt:0xFF00A2] , [[NSNumber alloc] initWithInt:0x9500FF], [[NSNumber alloc] initWithInt:0x0132FE], [[NSNumber alloc] initWithInt:0x01FAFF],[[NSNumber alloc] initWithInt:0x00FF00],[[NSNumber alloc] initWithInt:0xFFFF00],[[NSNumber alloc] initWithInt:0xFE3600], nil];
    
    NSNumber *obj = [arr objectAtIndex:button.tag];
    
    
    _color = UIColorFromRGB([obj intValue]);
    
    
    
    [self findPixel:_color];
}

- (UIColor *) colorOfPoint:(CGPoint)point
{
    unsigned char pixel[4] = {0};
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pixel, 1, 1, 8, 4, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    
    CGContextTranslateCTM(context, -point.x, -point.y);
    
    [_colorView.layer renderInContext:context];
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    //NSLog(@"pixel: %d %d %d %d", pixel[0], pixel[1], pixel[2], pixel[3]);
    
    UIColor *color = [UIColor colorWithRed:pixel[0]/255.0 green:pixel[1]/255.0 blue:pixel[2]/255.0 alpha:pixel[3]/255.0];
    
    return color;
}

-(void) findPixel:(UIColor*)pixelColor{
    dispatch_queue_t taskQ = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    __block CGPoint resultPoint;
    
    dispatch_async(taskQ , ^{
        
        
        _data = [self getdataFromImage];
        for (int x=0; x< self.w; x++){
            for (int y=0; y< self.h/2; y++){
                CGPoint invesigatedPoint= CGPointMake(x,y);
                
                UIColor * invPixelColor = [self getPixelColorAtLocation:invesigatedPoint];
                
                if ([self compareColors:pixelColor second:invPixelColor])
                    resultPoint = CGPointMake(invesigatedPoint.x/480*320, invesigatedPoint.y/70*_colorView.frame.size.height+_colorView.frame.origin.y);
            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            if (round(resultPoint.x)>0&& round(resultPoint.y)>0) [_colorPicker setCenter:CGPointMake(resultPoint.x, _colorView.center.y-5)];
            if (round(resultPoint.x) - _colorPicker.frame.size.width/2 < 0 ) {
                [_colorPicker setCenter:CGPointMake(_colorPicker.frame.size.width/2-5, _colorView.center.y-5)];
            }
            if (round(resultPoint.x) + _colorPicker.frame.size.width/2 > _colorView.frame.size.width ) {
                [_colorPicker setCenter:CGPointMake(_colorView.frame.size.width - _colorPicker.frame.size.width/2+5, _colorView.center.y-5)];
            }
        });
    });
    
}
-(BOOL)compareColors:(UIColor*)firstColor second:(UIColor*)secondColor{
    CGColorRef color = [firstColor CGColor];
    int red1 = 0, red2 = 0, green1, green2, blue1, blue2;
    CGFloat alpha1=0, alpha2=0;
    NSInteger numComponents = CGColorGetNumberOfComponents([firstColor CGColor]);
    const CGFloat *components1 = CGColorGetComponents([firstColor CGColor]);
    const CGFloat *components2 = CGColorGetComponents([secondColor CGColor]);
    
    if (numComponents == 4)
    {
        red1 = components1[0]*255.0f;
        green1 = components1[1]*255.0f;
        blue1 = components1[2]*255.0f;
        alpha1 = components1[3];
    }
    
    color = [secondColor CGColor];
    
    numComponents = CGColorGetNumberOfComponents([secondColor CGColor]);
    if (numComponents == 4)
    {
        
        red2 = components2[0]*255;
        green2 = components2[1]*255;
        blue2 = components2[2]*255;
        alpha2 = components2[3];
    }
    
    if (red1 > red2-10 && red1 < red2 +10)
        if ( green1 >  green2-10 && green1< green2+10)
            if (blue1 >blue2-10 && blue1< blue2+10)
                return YES;
    return NO;
    
}


- (UIColor*) getPixelColorAtLocation:(CGPoint)point
{
    
    UIColor* color = nil;
    
    if (_data != NULL) {
        //offset locates the pixel in the data from x,y.
        //4 for 4 bytes of data per pixel, w is width of one row of data.
        int offset = 4*((self.w*round(point.y))+round(point.x));
        int alpha =255;//  data[offset];
        int red = _data[offset+1];
        int green = _data[offset+2];
        int blue = _data[offset+3];
        
        color = [UIColor colorWithRed:(red/255.0f) green:(green/255.0f) blue:(blue/255.0f) alpha:(alpha/255.0f)];
    }
    
    // When finished, release the context
    //CGContextRelease(cgctx);
    // Free image data memory for the context
    
    
    return color;
}
- (CGContextRef) createARGBBitmapContextFromImage:(CGImageRef)inImage
{
    CGContextRef    context = NULL;
    CGColorSpaceRef colorSpace;
    void *          bitmapData;
    NSInteger             bitmapByteCount;
    NSInteger             bitmapBytesPerRow;
    
    // Get image width, height. We'll use the entire image.
    size_t pixelsWide = CGImageGetWidth(inImage);
    size_t pixelsHigh = CGImageGetHeight(inImage);
    
    // Declare the number of bytes per row. Each pixel in the bitmap in this
    // example is represented by 4 bytes; 8 bits each of red, green, blue, and
    // alpha.
    bitmapBytesPerRow   = (pixelsWide * 4);
    bitmapByteCount     = (bitmapBytesPerRow * pixelsHigh);
    
    // Use the generic RGB color space.
    colorSpace = CGColorSpaceCreateDeviceRGB();
    
    if (colorSpace == NULL)
    {
        fprintf(stderr, "Error allocating color space\n");
        return NULL;
    }
    
    // Allocate memory for image data. This is the destination in memory
    // where any drawing to the bitmap context will be rendered.
    bitmapData = malloc( bitmapByteCount );
    if (bitmapData == NULL)
    {
        fprintf (stderr, "Memory not allocated!");
        CGColorSpaceRelease( colorSpace );
        return NULL;
    }
    
    // Create the bitmap context. We want pre-multiplied ARGB, 8-bits
    // per component. Regardless of what the source image format is
    // (CMYK, Grayscale, and so on) it will be converted over to the format
    // specified here by CGBitmapContextCreate.
    context = CGBitmapContextCreate (bitmapData,
                                     pixelsWide,
                                     pixelsHigh,
                                     8,      // bits per component
                                     bitmapBytesPerRow,
                                     colorSpace,
                                     (CGBitmapInfo)kCGImageAlphaPremultipliedFirst);
    if (context == NULL)
    {
        free (bitmapData);
        fprintf (stderr, "Context not created!");
    }
    
    // Make sure and release colorspace before returning
    CGColorSpaceRelease( colorSpace );
    
    return context;
}

-(unsigned char*) getdataFromImage{
    
    CGImageRef inImage;
    
    inImage = [_colorView.image CGImage];
    
    
    // Create off screen bitmap context to draw the image into. Format ARGB is 4 bytes for each pixel: Alpa, Red, Green, Blue
    CGContextRef cgctx = [self createARGBBitmapContextFromImage:inImage];
    if (cgctx == NULL) { return nil; /* error */ }
    
    self.w = CGImageGetWidth(inImage);
    self.h = CGImageGetHeight(inImage);
    CGRect rect = {{0,0},{self.w,self.h}};
    
    
    // Draw the image to the bitmap context. Once we draw, the memory
    // allocated for the context for rendering will then contain the
    // raw image data in the specified color space.
    CGContextDrawImage(cgctx, rect, inImage);
    // Now we can get a pointer to the image data associated with the bitmap
    // context.
    unsigned char* data_ = CGBitmapContextGetData (cgctx);
    return data_;
}

#pragma mark Brightness cell settings

-(IBAction)valueChanged:(id)sender {
    // This determines which "step" the slider should be on. Here we're taking
    //   the current position of the slider and dividing by the `self.stepValue`
    //   to determine approximately which step we are on. Then we round to get to
    //   find which step we are closest to.
    float newStep = roundf((_slider.value) / self.stepValue);
    
    // Convert "steps" back to the context of the sliders values.
    float newValue =  newStep * self.stepValue;
    _slider.value = newValue;
    _lSliderValue.text = [NSString stringWithFormat:@"%.0f%%",(newValue/_slider.maximumValue)*100];
}


#pragma mark On cell settings
- (IBAction)OnButtonPressed:(id)sender {
    
    [_onButton setImage:[UIImage imageNamed:@"fajka-56"] forState:UIControlStateNormal];
    [_offButton setImage:nil forState:UIControlStateNormal];
    _on = [[NSNumber alloc] initWithBool:YES];
}

- (IBAction)OffButtonPressed:(id)sender {
    [_offButton setImage:[UIImage imageNamed:@"fajka-56"] forState:UIControlStateNormal];
    [_onButton setImage:nil forState:UIControlStateNormal];
    _on = [[NSNumber alloc] initWithBool:NO];
}

#pragma mark Timer cell settings
- (IBAction)changeTime:(id)sender {
    UIButton * button = (UIButton *)sender;
    NSInteger secondsValue = [_secondsLabel.text intValue];
    NSInteger minutesValue = [_minutesLabel.text intValue];
    NSInteger hoursValue = [_hoursLabel.text intValue];
    switch (button.tag) {
        case 1:
            //seconds up
            
            if (secondsValue + _timerStep+ hoursValue*3600 + minutesValue*60 <= _timerMax){
                secondsValue+=_timerStep;
                if ((int)(secondsValue/60) == 1) {
                    secondsValue = secondsValue % 60;
                    minutesValue+=1;
                }
            }
            
            break;
        case 2:
            // senconds down
            
            if (secondsValue - _timerStep + hoursValue*3600 + minutesValue*60 >= _timerMin){
                secondsValue-=_timerStep;
                if (secondsValue<0){
                    
                    if (_timerMax<60) {
                        secondsValue = _timerMax;
                    }else{
                        secondsValue = 59;
                    }
                    if (minutesValue>0) minutesValue-=1;
                    
                }
                
            }
            break;
        case 3:
            // minutes up
            
            if ((minutesValue*60 + _timerStep*60)+secondsValue + hoursValue*3600 <= _timerMax){
                minutesValue+=_timerStep;
                if (secondsValue==_timerMin){
                    secondsValue = 0;
                }
                if ((int)(minutesValue/60) == 1){
                    minutesValue = minutesValue % 60;
                    hoursValue+=1;
                }
                
            }
            break;
        case 4:
            // minutes down
            
            if ((minutesValue*60 - _timerStep*60) + secondsValue + hoursValue*3600 >= _timerMin){
                minutesValue-=_timerStep;
                if (hoursValue*3600+minutesValue*60+secondsValue<_timerMin){
                    secondsValue = _timerMin % 60;
                }
                if (minutesValue<0){
                    if (_timerMax<3600) {
                        minutesValue = _timerMax/60;
                    }else{
                        minutesValue = 59;
                    }
                    if (hoursValue>0) hoursValue-=1;
                }
                
            }else if ((minutesValue*60 - _timerStep*60) + secondsValue + hoursValue*3600 >= 0 && (minutesValue*60 - _timerStep*60) + secondsValue + hoursValue*3600 < _timerMin){
                if (_timerMin < 60){
                    minutesValue-=_timerStep;
                    secondsValue = _timerMin;
                }
            }
            
            break;
        case 5:
            // hours up
            if ((hoursValue*3600 + _timerStep*3600)+minutesValue*60+secondsValue <= _timerMax){
                hoursValue+=_timerStep;
                
                
                if ((int)(hoursValue/25) == 1){
                    hoursValue = hoursValue % 24;
                }
                
            }
            
            
            
            break;
        case 6:
            //hours down;
            if (hoursValue==0) break;
            if ((hoursValue*3600 - _timerStep*3600)+minutesValue*60+secondsValue >= _timerMin){
                hoursValue-=_timerStep;
            }
            if (hoursValue<0){
                if (_timerMax<86400 ) {
                    hoursValue = _timerMax/3600;
                }else{
                    hoursValue = 24;
                }
                
            }
            
            break;
        default:
            break;
    }
    
    [_secondsLabel setText:[NSString stringWithFormat:@"%02ld", (long)secondsValue]];
    [_minutesLabel setText:[NSString stringWithFormat:@"%02ld", (long)minutesValue]];
    [_hoursLabel setText:[NSString stringWithFormat:@"%02ld", (long)hoursValue]];
    if (_timerMax <= 3600){
        [_hoursLabel setEnabled:NO];
        [_hoursLabel setText:@"--"];
    }
    if (_timerMax<=60){
        [_minutesLabel setEnabled:NO];
        [_minutesLabel setText:@"--"];
    }
    _time =hoursValue*3600+minutesValue*60+secondsValue;
    
    AudioServicesPlaySystemSound (1104);
}
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer{
    if (gestureRecognizer.view.tag>0 && gestureRecognizer.view.tag < 7){
        if ( gestureRecognizer.state == UIGestureRecognizerStateBegan ) {
            _timer = [NSTimer scheduledTimerWithTimeInterval:0.2f
                                                      target:self
                                                    selector:@selector(_timerFired:)
                                                    userInfo:[[NSMutableDictionary alloc] initWithObjectsAndKeys:gestureRecognizer.view, @"tag", nil]
                                                     repeats:YES];
            UIButton * but = (UIButton*)gestureRecognizer.view;
            if (but.tag%2!=0)[but setBackgroundImage:[UIImage imageNamed:@"action_arrow_top_on"] forState:UIControlStateNormal];
            else [but setBackgroundImage:[UIImage imageNamed:@"action_arrow_bot_on"] forState:UIControlStateNormal];
            
            
        }else if (gestureRecognizer.state == UIGestureRecognizerStateEnded ){
            UIButton * but = (UIButton*)gestureRecognizer.view;
            if (but.tag%2!=0)[but setBackgroundImage:[UIImage imageNamed:@"action_arrow_top"] forState:UIControlStateNormal];
            else [but setBackgroundImage:[UIImage imageNamed:@"action_arrow_bot"] forState:UIControlStateNormal];
            
            if (_timer != nil)
            {
                [_timer invalidate];
                _timer = nil;
            }
        }
    }else{
        [_sceneActionsViewController longPressEdit:gestureRecognizer];
    }
    
}
- (void)_timerFired:(NSTimer*)info
{
    UIView * view = [[info userInfo] objectForKey:@"tag"];
    [self changeTime:view];
}


-(NSDictionary*)jsonRepresentation{
    NSMutableDictionary * dict = [NSMutableDictionary new];
    switch (_type) {
        case kPlain:
            [dict setObject:[NSNull null] forKey:_itemName];
            //            [[@"\"" stringByAppendingString:_itemName] stringByAppendingString:@"\":null"];
            break;
        case kBrightness:
            [dict setObject:[NSNumber numberWithInt:(int)roundf(_slider.value)] forKey:_itemName];
            //            return [[@"\"" stringByAppendingString:_itemName] stringByAppendingString:[NSString stringWithFormat:@"\":%d",(int)roundf(_slider.value) ]];
            break;
        case kTime:
            [dict setObject:[NSNumber numberWithLong:(long)_time] forKey:_itemName];
            //            return [[@"\"" stringByAppendingString:_itemName] stringByAppendingString:[NSString stringWithFormat:@"\":%ld",(long)_time ]];
            break;
        case kRgb:{
            const CGFloat *components = CGColorGetComponents([_color CGColor]);
            int red=0, green=0, blue=0;
            NSInteger numComponents = CGColorGetNumberOfComponents([_color CGColor]);
            
            if (numComponents == 4)
            {
                red = components[0]*255.0f;
                green = components[1]*255.0f;
                blue = components[2]*255.0f;
            }
            [dict setValue:[NSNumber numberWithInt:red] forKey:@"red"];
            [dict setValue:[NSNumber numberWithInt:green] forKey:@"green"];
            [dict setValue:[NSNumber numberWithInt:blue] forKey:@"blue"];
            //            return [NSString stringWithFormat:@"\"red\":%d, \"green\":%d, \"blue\":%d",red,green,blue];
            break;
        }
        case kOn:
            [dict setObject:[NSNumber  numberWithBool:_on.boolValue?YES:NO] forKey:_itemName];
            //            if (_on.boolValue==YES)
            //                return [[@"\"" stringByAppendingString:_itemName] stringByAppendingString:@"\":true"];
            //            else
            //                            [dict setValue:[NSNumber numberWithInt:red] forKey:@"red"];
            //                return [[@"\"" stringByAppendingString:_itemName] stringByAppendingString:@"\":false"];
            break;
        default:
            break;
    }
    return dict;
}
-(void)makeTimeVisible{
    [[self.view viewWithTag:15] setHidden:NO];
    _expanded = YES;
    //AudioServicesPlaySystemSound (1104);
}
-(void)makeTimeInvisibleWithAlert:(BOOL)show {
    [[self.view viewWithTag:15] setHidden:YES];
    _expanded = NO;
    
    if (show) {
        AudioServicesPlaySystemSound (1104);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"warning", nil)
                                                        message:NSLocalizedString(@"delay_timer_set", nil)
                                                       delegate:self
                                              cancelButtonTitle:nil
                                              otherButtonTitles:nil];
        [alert show];
        [self performSelector:@selector(hide:) withObject:alert afterDelay:2];
    }
}


-(void)hide:(UIAlertView*)sender{
    [sender dismissWithClickedButtonIndex:-1 animated:YES];
}

- (IBAction)setTimer:(id)sender {
    AudioServicesPlaySystemSound (1104);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"warning", nil)
                                                    message:NSLocalizedString(@"delay_timer_set", nil)
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil];
    [alert show];
    [self performSelector:@selector(hide:) withObject:alert afterDelay:2];

    [self makeTimeInvisibleWithAlert:NO];
}

- (BOOL)isEqual:(id)other
{
    if (other == self) {
        return YES;
    } else {
        if ([other isKindOfClass:[DeterminableActionCell class]]){
            DeterminableActionCell * otherCell = (DeterminableActionCell*) other;
            return [_itemName isEqualToString:otherCell.itemName];
        } else {
            return NO;
        }
    }
}

@end
