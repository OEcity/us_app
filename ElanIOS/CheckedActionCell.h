//
//  CheckedActionCell.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 18/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SceneAction.h"
@interface CheckedActionCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel * nameLabel;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) IBOutlet UIImageView * checker;
@property (weak, nonatomic) IBOutlet UIView *vOverlay;
@property (nonatomic, retain) IBOutlet Action * action;
@end
