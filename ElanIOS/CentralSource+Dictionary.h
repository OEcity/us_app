//
//  CentralSource+Dictionary.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 16.11.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "CentralSource+CoreDataClass.h"
#import "SYSerializeProtocol.h"

@interface CentralSource (Dictionary) <SYSerializeProtocol>

@end
