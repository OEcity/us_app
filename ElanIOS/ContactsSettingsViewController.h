//
//  ContactsSettingsViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 10.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IntercomContact+CoreDataClass.h"

@interface ContactsSettingsViewController : UIViewController<NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) NSFetchedResultsController* contacts;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
