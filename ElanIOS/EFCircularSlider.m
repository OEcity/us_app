//
//  EFCircularSlider.m
//  Awake
//
//  Created by Eliot Fowler on 12/3/13.
//  Copyright (c) 2013 Eliot Fowler. All rights reserved.
//
//  Modified by Mikoláš Stuchlík on 6/21/16
//

#import "EFCircularSlider.h"
#import <QuartzCore/QuartzCore.h>
#import "EFCircularTrig.h"


@interface EFCircularSlider ()

@property (nonatomic) CGFloat radius;
@property (nonatomic, strong) NSMutableDictionary *labelsWithPercents;

@property (nonatomic, readonly) CGFloat handleWidth;
@property (nonatomic, readonly) CGFloat innerLabelRadialDistanceFromCircumference;
@property (nonatomic, readonly) CGPoint centerPoint;

@property (nonatomic, readonly) CGFloat radiusForDoubleCircleOuterCircle;
@property (nonatomic, readonly) CGFloat lineWidthForDoubleCircleOuterCircle;
@property (nonatomic, readonly) CGFloat radiusForDoubleCircleInnerCircle;
@property (nonatomic, readonly) CGFloat lineWidthForDoubleCircleInnerCircle;

@end

static const CGFloat kFitFrameRadius = -1.0;

@implementation EFCircularSlider

@synthesize radius = _radius;

#pragma mark - Initialisation
- (id)init
{
    return [self initWithRadius:kFitFrameRadius];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initDefaultValuesWithRadius:kFitFrameRadius];
    }
    return self;
}

- (id)initWithRadius:(CGFloat)radius
{
    self = [super init];
    if (self)
    {
        [self setClearsContextBeforeDrawing:YES];
        [self initDefaultValuesWithRadius:radius];
    }
    return self;
}

-(void) initDefaultValuesWithRadius:(CGFloat)radius
{
    _autoredrawingPaused = YES;
    
    _imageBackgroundFilledLine = NO;
    _imageBackgroundUnfilledLine = NO;
    
    _radius        = radius;
    _maximumValue  = 100.0f;
    _minimumValue  = 0.0f;
    _lineWidth     = 5;
    _unfilledColor = [UIColor blackColor];
    _filledColor   = [UIColor redColor];
    _labelFont     = [UIFont systemFontOfSize:10.0f];
    _snapToLabels  = NO;
    _handleType    = CircularSliderHandleTypeCircleCustom;
    _labelColor    = [UIColor redColor];
    _labelDisplacement = 0;
    _arcStartAngle = 0;
    _arcAngleLength = 360;
    
    _unfilledLineWidthDifference = 0;
    _filledLineWidthDifference = 0;
    
    _filledLineDash[0] = 1;
    _unfilledLineDash[0] = 1;
    
    _filledLineDashCount = 0;
    _unfilledLineDashCount = 0;
    
    _handleColor = [UIColor redColor];
    _handleBorderColor = [UIColor whiteColor];
    _handleBorderSize = 1.0;
    _handleRadius = 7.0;
    
    _filledLineStrokeBorderColor = [UIColor redColor];
    _filledLineStrokeBorderWidth = 3.0;
    _filledLineStrokeBorderRadius = 10.0;
    _filledLineInnerImage = nil;
    _filledLineInnerBGColor = [UIColor whiteColor];
    
    _unfilledLineStrokeBorderColor = [UIColor redColor];
    _unfilledLineStrokeBorderWidth = 3.0;
    _unfilledLineStrokeBorderRadius = 10.0;
    _unfilledLineInnerImage = nil;
    _unfilledLineInnerBGColor = [UIColor whiteColor];
    
    _angleFromNorth = 0;
    
    _autoredrawingPaused = NO;
    
    self.backgroundColor = [UIColor clearColor];
}

#pragma mark - Public setter overrides
-(CGFloat *)filledLineDash{
    return _filledLineDash;
}

-(CGFloat *)unfilledLineDash{
    return _unfilledLineDash;
}

-(void) setLineWidth:(CGFloat)lineWidth
{
    _lineWidth = lineWidth;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void) setHandleType:(CircularSliderHandleType)handleType
{
    _handleType = handleType;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setHandleRadius:(CGFloat)handleRadius{
    _handleRadius = handleRadius;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setHandleBorderColor:(UIColor *)handleBorderColor{
    _handleBorderColor = handleBorderColor;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setHandleBorderSize:(CGFloat)handleBorderSize{
    _handleBorderSize = handleBorderSize;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void) setFilledColor:(UIColor*)filledColor
{
    _filledColor = filledColor;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void) setUnfilledColor:(UIColor*)unfilledColor
{
    _unfilledColor = unfilledColor;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void) setHandlerColor:(UIColor *)handleColor
{
    _handleColor = handleColor;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void) setLabelFont:(UIFont*)labelFont
{
    _labelFont = labelFont;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void) setLabelColor:(UIColor*)labelColor
{
    _labelColor = labelColor;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setInnerMarkingLabels:(NSArray*)innerMarkingLabels
{
    _innerMarkingLabels = innerMarkingLabels;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setMinimumValue:(CGFloat)minimumValue
{
    _minimumValue = minimumValue;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setMaximumValue:(CGFloat)maximumValue
{
    _maximumValue = maximumValue;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setImageBackgroundUnfilledLine:(BOOL)imageBackgroundUnfilledLine{
    _imageBackgroundUnfilledLine = imageBackgroundUnfilledLine;
    if(!_autoredrawingPaused){[self redraw]; }
    
}

-(void)setImageBackgroundFilledLine:(BOOL)imageBackgroundFilledLine{
    _imageBackgroundFilledLine = imageBackgroundFilledLine;
    if(!_autoredrawingPaused){[self redraw]; }
    
}

-(void)setUnfilledLineInnerBGColor:(UIColor *)unfilledLineInnerBGColor{
    _unfilledLineInnerBGColor = unfilledLineInnerBGColor;
    if(!_autoredrawingPaused){[self redraw]; }
    
}

-(void)setFilledLineInnerBGColor:(UIColor *)filledLineInnerBGColor{
    _filledLineInnerBGColor = filledLineInnerBGColor;
    if(!_autoredrawingPaused){[self redraw]; }
    
}

-(void)setArcStartAngle:(CGFloat)arcStartAngle
{
    if (_arcStartAngle > 360.0) {
        _arcStartAngle = 360.0;
    }
    else if (_arcStartAngle < 0.0) {
        _arcStartAngle = 0.0;
    }
    else {
        _arcStartAngle = arcStartAngle;
    }
    if(!_autoredrawingPaused){[self redraw]; } // Need to redraw with updated value range
}

-(void)setArcAngleLength:(CGFloat)arcAngleLength
{
    if (arcAngleLength > 360.0) {
        _arcAngleLength = 360.0;
    }
    else if (arcAngleLength < 0.0) {
        _arcAngleLength = 0.0;
    }
    else {
        _arcAngleLength = arcAngleLength;
    }
    if(!_autoredrawingPaused){[self redraw]; } // Need to redraw with updated value range
}

-(void)setFilledLineWidthDifference:(CGFloat)filledLineWidthDifference{
    _filledLineWidthDifference = filledLineWidthDifference;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setUnfilledLineWidthDifference:(CGFloat)unfilledLineWidthDifference{
    _unfilledLineWidthDifference = unfilledLineWidthDifference;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setFilledLineDash:(CGFloat *)filledLineDash andCount:(int)filledLineDashCount{
    for(int i = 0; i<filledLineDashCount; i++){
        _filledLineDash[i] = filledLineDash[i];
    }
    _filledLineDashCount = filledLineDashCount;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setUnfilledLineDash:(CGFloat *)unfilledLineDash andCount:(int)unfilledLineDashCount{
    for(int i = 0; i<unfilledLineDashCount; i++){
        _unfilledLineDash[i] = unfilledLineDash[i];
    }
    _unfilledLineDashCount = unfilledLineDashCount;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setFilledLineStrokeBorderColor:(UIColor *)filledLineStrokeBorderColor{
    _filledLineStrokeBorderColor = filledLineStrokeBorderColor;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setFilledLineStrokeBorderWidth:(CGFloat)filledLineStrokeBorderWidth{
    _filledLineStrokeBorderWidth = filledLineStrokeBorderWidth;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setFilledLineStrokeBorderRadius:(CGFloat)filledLineStrokeBorderRadius{
    _filledLineStrokeBorderRadius = filledLineStrokeBorderRadius;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setFilledLineInnerImage:(UIImage *)filledLineInnerImage{
    _filledLineInnerImage = [self resizeImage:filledLineInnerImage];
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setUnfilledLineStrokeBorderColor:(UIColor *)unfilledLineStrokeBorderColor{
    _unfilledLineStrokeBorderColor = unfilledLineStrokeBorderColor;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setUnfilledLineStrokeBorderWidth:(CGFloat)unfilledLineStrokeBorderWidth{
    _unfilledLineStrokeBorderWidth = unfilledLineStrokeBorderWidth;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setUnfilledLineStrokeBorderRadius:(CGFloat)unfilledLineStrokeBorderRadius{
    _unfilledLineStrokeBorderRadius = unfilledLineStrokeBorderRadius;
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setUnfilledLineInnerImage:(UIImage *)unfilledLineInnerImage{
    _unfilledLineInnerImage = [self resizeImage:unfilledLineInnerImage];
    if(!_autoredrawingPaused){[self redraw]; }
}

-(void)setFrame:(CGRect)frame{
    [super setFrame:frame];
    
    if (_unfilledLineInnerImage) {
        _unfilledLineInnerImage = [self resizeImage:_unfilledLineInnerImage];
    }
    
    if (_filledLineInnerImage) {
        _filledLineInnerImage = [self resizeImage:_filledLineInnerImage];
    }
}

/**
 *  There is no local variable currentValue - it is always calculated based on angleFromNorth
 *
 *  @param currentValue Value used to update angleFromNorth between minimumValue & maximumValue
 */
-(void) setCurrentValue:(CGFloat)currentValue
{
    NSAssert(currentValue <= self.maximumValue && currentValue >= self.minimumValue,
             @"currentValue (%.2f) must be between self.minimuValue (%.2f) and self.maximumValue (%.2f)",
             currentValue, self.minimumValue, self.maximumValue);
    
    // Update the angleFromNorth to match this newly set value
    self.angleFromNorth = (currentValue * 360)/(self.maximumValue - self.minimumValue);
    [self redraw];
}

-(void) setCurrentArcValue:(CGFloat)currentValue
{
    // Update the angleFromNorth to match this newly set value
    self.angleFromNorth = (currentValue * _arcAngleLength)/(self.maximumValue - self.minimumValue) + _arcStartAngle;
    [self redraw];
}

-(void)setCurrentArcValue:(CGFloat)value forStartAnglePadding:(CGFloat)stArcPd endAnglePadding:(CGFloat)enArcPd{
    if (value <= _minimumValue) {
        if ([self getCurrentArcValueForStartAnglePadding:stArcPd endAnglePadding:enArcPd] == _minimumValue) {
            return;
        }
        [self setCurrentArcValue:_minimumValue];
    }else if (value >= _maximumValue){
        if ([self getCurrentArcValueForStartAnglePadding:stArcPd endAnglePadding:enArcPd] == _maximumValue) {
            return;
        }
        [self setCurrentArcValue:_maximumValue];
    }else{
        self.angleFromNorth = (value * ( _arcAngleLength - stArcPd - enArcPd))/(self.maximumValue - self.minimumValue) + (_arcStartAngle + stArcPd);
    }
    [self redraw];
}

-(void)setAngleFromNorth:(CGFloat)angleFromNorth
{
    _angleFromNorth = angleFromNorth;
    [self redraw];
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

-(void) setRadius:(CGFloat)radius
{
    _radius = radius;
    if(!_autoredrawingPaused){[self redraw]; } // Need to redraw with new radius
}

-(void) setDeviceForHandler:(Device *)deviceForHandler
{
    _deviceForHandler = deviceForHandler;
}
#pragma mark - Public getter overrides

/**
 *  There is no local variable currentValue - it is always calculated based on angleFromNorth
 *
 *  @return currentValue Value between minimumValue & maximumValue derived from angleFromNorth
 */
-(CGFloat) currentValue
{
    return self.minimumValue + (self.angleFromNorth * (self.maximumValue - (CGFloat)2.0 * self.minimumValue))/(CGFloat)360.0f;
}

-(CGFloat) currentArcValue
{
    return self.minimumValue + ([self angleFromStartPoint] * (self.maximumValue - (CGFloat)2.0 * self.minimumValue))/_arcAngleLength;
}

-(CGFloat)getCurrentArcValueForStartAnglePadding:(CGFloat)stArcPd endAnglePadding:(CGFloat)enArcPd{
    if ([self angleFromStartPoint]-stArcPd <= 0) {
        return _minimumValue;
    }else if ([self angleFromStartPoint]+enArcPd >= _arcAngleLength){
        return _maximumValue;
    }else{
        return self.minimumValue + (([self angleFromStartPoint] - stArcPd) * (self.maximumValue - (CGFloat)2.0 * self.minimumValue))/(_arcAngleLength-stArcPd-enArcPd);
    }
    
    return 0.0;
}

-(CGFloat) radius
{
    if (_radius == kFitFrameRadius)
    {
        // Slider is being used in frames - calculate the max radius based on the frame
        //  (constrained by smallest dimension so it fits within view)
        CGFloat minimumDimension = MIN(self.bounds.size.height, self.bounds.size.width);
        
        CGFloat unfilledSize, filledSize;
        
        if (_imageBackgroundFilledLine) {
            filledSize = _lineWidth + _filledLineWidthDifference + _filledLineStrokeBorderWidth*2;
        }else{
            filledSize = _lineWidth + _filledLineWidthDifference;
        }
        
        if (_imageBackgroundUnfilledLine) {
            unfilledSize = _lineWidth + _unfilledLineWidthDifference + _unfilledLineStrokeBorderWidth*2;
        }else{
            unfilledSize = _lineWidth + _unfilledLineWidthDifference;
        }
        
        CGFloat halfLineWidth = MAX(unfilledSize, filledSize)/2;
        CGFloat halfHandleWidth;
        if (_handleType == CircularSliderHandleTypeCircleCustom) {
             halfHandleWidth = (self.handleWidth + _handleBorderSize);
        }else{
            halfHandleWidth = self.handleWidth;
        }
        
        return minimumDimension * 0.5 - MAX(halfHandleWidth, halfLineWidth);
    }
    return _radius;
}

-(UIColor*)handleColor
{
    UIColor *newHandleColor = _handleColor;
    switch (self.handleType) {
        case CircularSliderHandleTypeCircleCustom:{
            return _handleColor;
            break;
        }
        case CircularSliderHandleTypeSemiTransparentWhiteCircle:
        {
            newHandleColor = [UIColor colorWithWhite:1.0 alpha:0.7];
            break;
        }
        case CircularSliderHandleTypeSemiTransparentBlackCircle:
        {
            newHandleColor = [UIColor colorWithWhite:0.0 alpha:0.7];
            break;
        }
        case CircularSliderHandleTypeDoubleCircleWithClosedCenter:
        case CircularSliderHandleTypeDoubleCircleWithOpenCenter:
        case CircularSliderHandleTypeBigCircle:
        {
            if (!newHandleColor)
            {
                // handleColor public property hasn't been set - use filledColor
                newHandleColor = self.filledColor;
            }
            break;
        }
    }
    
    return newHandleColor;
}

#pragma mark - Private getter overrides

-(CGFloat) handleWidth
{
    switch (self.handleType) {
        case CircularSliderHandleTypeCircleCustom:{
            return _handleRadius;
        }
        case CircularSliderHandleTypeSemiTransparentWhiteCircle:
        case CircularSliderHandleTypeSemiTransparentBlackCircle:
        {
            return self.lineWidth;
        }
        case CircularSliderHandleTypeBigCircle:
        {
            return self.lineWidth + 15; // 5 points bigger than standard handles
        }
        case CircularSliderHandleTypeDoubleCircleWithClosedCenter:
        case CircularSliderHandleTypeDoubleCircleWithOpenCenter:
        {
            return 2 * [EFCircularTrig outerRadiuOfUnfilledArcWithRadius:self.radiusForDoubleCircleOuterCircle
                                                               lineWidth:self.lineWidthForDoubleCircleOuterCircle];
        }
    }
    return 0;
}

-(CGFloat)radiusForDoubleCircleOuterCircle
{
    return 0.5 * self.lineWidth + 5;
}
-(CGFloat)lineWidthForDoubleCircleOuterCircle
{
    return 4.0;
}

-(CGFloat)radiusForDoubleCircleInnerCircle
{
    return 0.5 * self.lineWidth;
}
-(CGFloat)lineWidthForDoubleCircleInnerCircle
{
    return 2.0;
}

-(CGFloat)innerLabelRadialDistanceFromCircumference
{
    // Labels should be moved far enough to clear the line itself plus a fixed offset (relative to radius).
    int distanceToMoveInwards  = 0.1 * -(self.radius) - 0.5 * self.lineWidth;
        distanceToMoveInwards -= 0.5 * self.labelFont.pointSize; // Also account for variable font size.
    return distanceToMoveInwards;
}

-(CGPoint)centerPoint
{
    return CGPointMake(self.bounds.size.width * 0.5, self.bounds.size.height * 0.5);
}

#pragma mark - Method overrides
-(CGSize)intrinsicContentSize
{
    // Total width is: diameter + (2 * MAX(halfLineWidth, halfHandleWidth))
    int diameter = self.radius * 2;
    int halfLineWidth = ceilf(self.lineWidth / 2.0);
    int halfHandleWidth = ceilf(self.handleWidth / 2.0);
    
    int widthWithHandle = diameter + (2 *  MAX(halfHandleWidth, halfLineWidth));
    
    return CGSizeMake(widthWithHandle, widthWithHandle);
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    // Draw the circular lines that slider handle moves along
    [self drawLine:ctx];
    
    // Draw the draggable 'handle'
    [self drawHandle:ctx];
    
    // Add the labels
    [self drawInnerLabels:ctx];
}


- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    if ([self pointInsideHandle:point withEvent:event])
    {
        return YES; // Point is indeed within handle bounds
    }
    else
    {
        return [self pointInsideCircle:point withEvent:event]; // Return YES if point is inside slider's circle
    }
}

- (BOOL)pointInsideCircle:(CGPoint)point withEvent:(UIEvent *)event {
    CGPoint p1 = [self centerPoint];
    CGPoint p2 = point;
    CGFloat xDist = (p2.x - p1.x);
    CGFloat yDist = (p2.y - p1.y);
    double distance = sqrt((xDist * xDist) + (yDist * yDist));
    return distance < self.radius + self.lineWidth * 0.5;
}

- (BOOL)pointInsideHandle:(CGPoint)point withEvent:(UIEvent *)event {
    CGPoint handleCenter = [self pointOnCircleAtAngleFromNorth:self.angleFromNorth];
    CGFloat handleRadius = MAX(self.handleWidth, 44.0) * 0.5;
    // Adhere to apple's design guidelines - avoid making touch targets smaller than 44 points
    
    // Treat handle as a box around it's center
    BOOL pointInsideHorzontalHandleBounds = (point.x >= handleCenter.x - handleRadius
                                             && point.x <= handleCenter.x + handleRadius);
    BOOL pointInsideVerticalHandleBounds  = (point.y >= handleCenter.y - handleRadius
                                             && point.y <= handleCenter.y + handleRadius);
    return pointInsideHorzontalHandleBounds && pointInsideVerticalHandleBounds;
}

#pragma mark - Drawing methods

-(void) drawLine:(CGContextRef)ctx
{
    // Draw an unfilled circle (this shows what can be filled)
    if (_imageBackgroundUnfilledLine) {
        [EFCircularTrig drawUnfilledImagedArcInContext:ctx
                                                center:self.centerPoint
                                                radius:self.radius
                                             lineWidth:self.lineWidth + _unfilledLineWidthDifference
                                           borderWidth:_unfilledLineStrokeBorderWidth
                                          bordarRadius:_unfilledLineStrokeBorderRadius
                                           borderColor:_unfilledLineStrokeBorderColor
                                           insideColor:_unfilledLineInnerBGColor
                                           insideImage:_unfilledLineInnerImage
                                    fromAngleFromNorth:_arcStartAngle
                                      toAngleFromNorth:[self getEndArcDegreeFromNowth]];
    }else{
        [self.unfilledColor set];
        [EFCircularTrig drawUnfilledDashedArcInContext:ctx
                                            center:self.centerPoint
                                            radius:self.radius
                                         lineWidth:self.lineWidth + _unfilledLineWidthDifference
                                            dashes:_unfilledLineDash
                                       dashesCount:_unfilledLineDashCount
                                fromAngleFromNorth:_arcStartAngle
                                  toAngleFromNorth:[self getEndArcDegreeFromNowth]];
    }
    
    if (_imageBackgroundFilledLine) {
        [EFCircularTrig drawUnfilledImagedArcInContext:ctx
                                                center:self.centerPoint
                                                radius:self.radius
                                             lineWidth:self.lineWidth + _filledLineWidthDifference
                                           borderWidth:_filledLineStrokeBorderWidth
                                          bordarRadius:_filledLineStrokeBorderRadius
                                           borderColor:_filledLineStrokeBorderColor
                                           insideColor:_filledLineInnerBGColor
                                           insideImage:_filledLineInnerImage
                                    fromAngleFromNorth:_arcStartAngle
                                      toAngleFromNorth:[self getEndArcDegreeFromNowth]];
    }else{
        // Draw an unfilled arc up to the currently filled point
        [self.filledColor set];
        [EFCircularTrig drawUnfilledDashedArcInContext:ctx
                                            center:self.centerPoint
                                            radius:self.radius
                                         lineWidth:self.lineWidth + _filledLineWidthDifference
                                            dashes:_filledLineDash
                                       dashesCount:_filledLineDashCount
                                fromAngleFromNorth:_arcStartAngle
                                  toAngleFromNorth:self.angleFromNorth];
    }
}

-(void) drawHandle:(CGContextRef)ctx{
    CGContextSaveGState(ctx);
    CGPoint handleCenter = [self pointOnCircleAtAngleFromNorth:self.angleFromNorth];
    
    // Ensure that handle is drawn in the correct color
    [self.handleColor set];
    
    switch (self.handleType) {
        case CircularSliderHandleTypeCircleCustom:{
            [EFCircularTrig drawFilledCircleInContext:ctx
                                               center:handleCenter
                                               radius:_handleRadius
                                                color:_handleColor
                                          strokeColor:_handleBorderColor
                                          strokeWidth:_handleBorderSize];
            break;
        }
        case CircularSliderHandleTypeSemiTransparentWhiteCircle:
        case CircularSliderHandleTypeSemiTransparentBlackCircle:
        case CircularSliderHandleTypeBigCircle:
        {
            [EFCircularTrig drawFilledCircleInContext:ctx
                                     center:handleCenter
                                     radius:0.5 * self.handleWidth];
            break;
        }
        case CircularSliderHandleTypeDoubleCircleWithClosedCenter:
        case CircularSliderHandleTypeDoubleCircleWithOpenCenter:
        {
            [self drawUnfilledLineBehindDoubleCircleHandle:ctx];
            
            // Draw unfilled outer circle
            [EFCircularTrig drawUnfilledCircleInContext:ctx
                                       center:CGPointMake(handleCenter.x,
                                                          handleCenter.y)
                                       radius:self.radiusForDoubleCircleOuterCircle
                                    lineWidth:self.lineWidthForDoubleCircleOuterCircle];
            
            if (self.handleType == CircularSliderHandleTypeDoubleCircleWithClosedCenter)
            {
                // Draw filled inner circle
                [EFCircularTrig drawFilledCircleInContext:ctx
                                                   center:handleCenter
                                                   radius:[EFCircularTrig outerRadiuOfUnfilledArcWithRadius:self.radiusForDoubleCircleInnerCircle
                                                                                                  lineWidth:self.lineWidthForDoubleCircleInnerCircle]];
            }
            else if (self.handleType == CircularSliderHandleTypeDoubleCircleWithOpenCenter)
            {
                // Draw unfilled inner circle
                [EFCircularTrig drawUnfilledCircleInContext:ctx
                                                     center:CGPointMake(handleCenter.x,
                                                                        handleCenter.y)
                                                     radius:self.radiusForDoubleCircleInnerCircle
                                                  lineWidth:self.lineWidthForDoubleCircleInnerCircle];
            }
            
            break;
        }
    }
    
    CGContextRestoreGState(ctx);
}

/**
 *  Draw unfilled line from left edge of handle to right edge of handle
 *  This is to ensure that the filled portion of the line doesn't show inside the double circle
 *  @param ctx Graphics Context within which to draw unfilled line behind handle
 */
-(void) drawUnfilledLineBehindDoubleCircleHandle:(CGContextRef)ctx
{
    CGFloat degreesToHandleCenter   = self.angleFromNorth;
    // To determine where handle intersects the filledCircle, make approximation that arcLength ~ radius of handle outer circle.
    // This is a fine approximation whenever self.radius is sufficiently large (which it must be for this control to be usable)
    CGFloat degreesDifference = [EFCircularTrig degreesForArcLength:self.radiusForDoubleCircleOuterCircle
                                                 onCircleWithRadius:self.radius];
    CGFloat degreesToHandleLeftEdge  = degreesToHandleCenter - degreesDifference;
    CGFloat degreesToHandleRightEdge = degreesToHandleCenter + degreesDifference;
    
    CGContextSaveGState(ctx);
    [self.unfilledColor set];
    [EFCircularTrig drawUnfilledArcInContext:ctx
                                      center:self.centerPoint
                                      radius:self.radius
                                   lineWidth:self.lineWidth
                          fromAngleFromNorth:degreesToHandleLeftEdge
                            toAngleFromNorth:degreesToHandleRightEdge];
    CGContextRestoreGState(ctx);
}

-(void) drawInnerLabels:(CGContextRef)ctx
{
    // Only draw labels if they have been set
    NSInteger labelsCount = self.innerMarkingLabels.count;
    if(labelsCount)
    {
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
        NSDictionary *attributes = @{ NSFontAttributeName: self.labelFont,
                                      NSForegroundColorAttributeName: self.labelColor};
#endif
        for (int i = 0; i < labelsCount; i++)
        {
            // Enumerate through labels clockwise
            NSString* label = self.innerMarkingLabels[i];
            
            CGRect labelFrame = [self contextCoordinatesForLabelAtIndex:i];
            
#if __IPHONE_OS_VERSION_MIN_REQUIRED >= __IPHONE_7_0
            [label drawInRect:labelFrame withAttributes:attributes];
#else
            [self.labelColor setFill];
            [label drawInRect:labelFrame withFont:self.labelFont];
#endif
        }
    }
}

-(CGRect)contextCoordinatesForLabelAtIndex:(NSInteger)index
{
    NSString *label = self.innerMarkingLabels[index];

    // Determine how many degrees around the full circle this label should go
    CGFloat percentageAlongCircle    = (index + 1) / (CGFloat)self.innerMarkingLabels.count;
    CGFloat degreesFromNorthForLabel = percentageAlongCircle * 360;
    CGPoint pointOnCircle = [self pointOnCircleAtAngleFromNorth:degreesFromNorthForLabel];
    
    CGSize  labelSize        = [self sizeOfString:label withFont:self.labelFont];
    CGPoint offsetFromCircle = [self offsetFromCircleForLabelAtIndex:index withSize:labelSize];

    return CGRectMake(pointOnCircle.x + offsetFromCircle.x, pointOnCircle.y + offsetFromCircle.y, labelSize.width, labelSize.height);
}

-(CGPoint) offsetFromCircleForLabelAtIndex:(NSInteger)index withSize:(CGSize)labelSize
{
    // Determine how many degrees around the full circle this label should go
    CGFloat percentageAlongCircle    = (index + 1) / (CGFloat)self.innerMarkingLabels.count;
    CGFloat degreesFromNorthForLabel = percentageAlongCircle * 360;
    
    CGFloat radialDistance = self.innerLabelRadialDistanceFromCircumference + self.labelDisplacement;
    CGPoint inwardOffset   = [EFCircularTrig pointOnRadius:radialDistance
                                            atAngleFromNorth:degreesFromNorthForLabel];
    
    return CGPointMake(-labelSize.width * 0.5 + inwardOffset.x, -labelSize.height * 0.5 + inwardOffset.y);
}

#pragma mark - UIControl functions

-(BOOL) continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [super continueTrackingWithTouch:touch withEvent:event];
    
    CGPoint lastPoint = [touch locationInView:self];
    [self moveHandle:lastPoint];
    
    return YES;
}

-(void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event
{
    [super endTrackingWithTouch:touch withEvent:event];
    [self sendActionsForControlEvents:UIControlEventEditingDidEnd];
    if(self.snapToLabels && self.innerMarkingLabels != nil)
    {
        CGPoint bestGuessPoint = CGPointZero;
        CGFloat minDist = 360;
        NSUInteger labelsCount = self.innerMarkingLabels.count;
        
        for (int i = 0; i < labelsCount; i++)
        {
            CGFloat percentageAlongCircle = i/(CGFloat)labelsCount;
            CGFloat degreesForLabel       = percentageAlongCircle * 360;
            if(fabs(self.angleFromNorth - degreesForLabel) < minDist)
            {
                minDist = fabs(self.angleFromNorth - degreesForLabel);
                bestGuessPoint = [self pointOnCircleAtAngleFromNorth:degreesForLabel];
            }
        }
        self.angleFromNorth = floor([EFCircularTrig angleRelativeToNorthFromPoint:self.centerPoint
                                                                             toPoint:bestGuessPoint]);
        [self setNeedsDisplay];
        [self sendActionsForControlEvents:UIControlEventEditingDidEnd];
    }
}

-(void)moveHandle:(CGPoint)point
{
    CGFloat consideredAngle = floor([EFCircularTrig angleRelativeToNorthFromPoint:self.centerPoint
                                                                        toPoint:point]);
    if ([self degreeInRange:consideredAngle]) {
        self.angleFromNorth = consideredAngle;
    }
    
    [self setNeedsDisplay];
}

#pragma mark - Helper functions
- (BOOL) isDoubleCircleHandle
{
    return self.handleType == CircularSliderHandleTypeDoubleCircleWithClosedCenter || self.handleType == CircularSliderHandleTypeDoubleCircleWithOpenCenter;
}

- (CGSize) sizeOfString:(NSString *)string withFont:(UIFont*)font
{
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[NSAttributedString alloc] initWithString:string attributes:attributes].size;
}

-(CGPoint)pointOnCircleAtAngleFromNorth:(int)angleFromNorth
{
    CGPoint offset = [EFCircularTrig  pointOnRadius:self.radius atAngleFromNorth:angleFromNorth];
    return CGPointMake(self.centerPoint.x + offset.x, self.centerPoint.y + offset.y);
}

#pragma mark - Strunchlík supporting methods
-(BOOL)degreeInRange:(CGFloat)degree{
    CGFloat endDegree = [self getEndArcDegreeFromNowth];
    
    if (endDegree > _arcStartAngle){
        if (degree <= endDegree && degree >= _arcStartAngle) {
            return YES;
        }else{
            return NO;
        }
    }else if (endDegree < _arcStartAngle){
        if ((degree >= _arcStartAngle && degree <= 360) || (degree >= 0 && degree <= endDegree)) {
            return YES;
        }else{
            return NO;
        }
    }
    
    return YES;
}

-(CGFloat)getEndArcDegreeFromNowth{
    CGFloat returnAngle = 0;
    returnAngle = _arcStartAngle + _arcAngleLength;
    if (returnAngle > 360) {
        returnAngle -= 360;
    }
    
    return returnAngle;
}

-(CGFloat)angleFromStartPoint{
    if (_angleFromNorth >= _arcStartAngle){
        return _angleFromNorth - _arcStartAngle;
    }else{
        return (_angleFromNorth + 360) - _arcStartAngle;
    }
    
    return 0;
}

-(CGFloat)distanceFromstartAngle:(CGFloat)angle{
    if (angle < _arcStartAngle) {
        angle += 360;
    }
    
    return angle - _arcStartAngle;
}

-(UIImage *)resizeImage:(UIImage *)image{
    UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(BOOL)isAngleValidArcAngle:(CGFloat)angle{
    if (angle < 0 || angle >= 360) {
        return NO;
    }
    
    if ([self distanceFromstartAngle:angle] > _arcAngleLength) {
        return NO;
    }
    
    return YES;
}

//REDRAWING OPTIMIZATION
-(void)pauseAutoredrawing{
    _autoredrawingPaused = YES;
}

-(void)redraw{
    [self setNeedsUpdateConstraints]; [self invalidateIntrinsicContentSize]; [self setNeedsDisplay];
}

-(void)activateAutoredrawing{
    [self redraw];
    _autoredrawingPaused = NO;
}
@end
