//
//  SYFavouritesViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 7/28/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYBaseViewController.h"
#import "SYCoreDataManager.h"
@interface SYFavouritesViewController : SYBaseViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate >


@property (nonatomic, retain) IBOutlet UITableView* tableView;

@property (weak, nonatomic) IBOutlet UILabel *favouritesLabel;
@property (weak, nonatomic) IBOutlet UIButton *bottomRightMenu;
@property (weak, nonatomic) IBOutlet UIButton *bottomLeftMenu;

-(IBAction)nextStep:(id)sender;
@end
