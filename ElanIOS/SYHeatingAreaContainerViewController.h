//
//  SYHeatingAreaContainerViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/8/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HUDWrapper.h"
#import "SYHeatContainerViewController.h"
#import "SYCoreDataManager.h"
@interface SYHeatingAreaContainerViewController : UIViewController <NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) HUDWrapper * loaderDialog;
@property (nonatomic, retain) SYHeatContainerViewController * container;
@property (nonatomic, retain) UILabel * lNoContent;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;

@end
