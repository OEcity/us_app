//
//  TrimmerViewController.h
//  ElanIOS
//
//  Created by Vratislav Zima on 6/2/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//



@interface TrimmerViewController : UIViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil value:(int)value maxValue:(int)maxValue_;
@property (nonatomic, retain) IBOutlet UILabel* number;
@end
