//
//  Action+CoreDataProperties.h
//  
//
//  Created by Tom Odler on 01.09.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Action.h"

NS_ASSUME_NONNULL_BEGIN

@interface Action (CoreDataProperties)

@property (nullable, nonatomic, retain) NSNumber *max;
@property (nullable, nonatomic, retain) NSNumber *min;
@property (nullable, nonatomic, retain) NSString *name;
@property (nullable, nonatomic, retain) NSNumber *step;
@property (nullable, nonatomic, retain) NSString *type;
@property (nullable, nonatomic, retain) Device *device;
@property (nullable, nonatomic, retain) NSSet<SceneAction *> *inScenes;
@property (nullable, nonatomic, retain) Device *primaryOnDevice;
@property (nullable, nonatomic, retain) NSSet<SecondaryAction *> *secondaryOnDevice;
@property (nullable, nonatomic, retain) IRDevice *irDevice;

@end

@interface Action (CoreDataGeneratedAccessors)

- (void)addInScenesObject:(SceneAction *)value;
- (void)removeInScenesObject:(SceneAction *)value;
- (void)addInScenes:(NSSet<SceneAction *> *)values;
- (void)removeInScenes:(NSSet<SceneAction *> *)values;

- (void)addSecondaryOnDeviceObject:(SecondaryAction *)value;
- (void)removeSecondaryOnDeviceObject:(SecondaryAction *)value;
- (void)addSecondaryOnDevice:(NSSet<SecondaryAction *> *)values;
- (void)removeSecondaryOnDevice:(NSSet<SecondaryAction *> *)values;

@end

NS_ASSUME_NONNULL_END
