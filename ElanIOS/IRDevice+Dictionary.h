//
//  IRDevice+Dictionary.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 12.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "IRDevice.h"
#import "SYSerializeProtocol.h"

@interface IRDevice (Dictionary) <SYSerializeProtocol>

@end
