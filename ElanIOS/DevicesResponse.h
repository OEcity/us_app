//
//  DevicesResponse.h
//  ElanIOS
//
//  Created by Vratislav Zima on 5/27/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"

@interface DevicesResponse : NSObject <Response>

-(NSObject*) parseResponse:(id)inputData identifier:(NSString *)identifier;
@end
