//
//  TouchDownGestureRecognizer.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 7/13/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TouchDownGestureRecognizer : UIGestureRecognizer

@end