//
//  HeatCoolAreaDetailViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 25.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "HeatCoolAreaDetailViewController.h"
#import "SYCoreDataManager.h"
#import "Constants.h"
#import "AppDelegate.h"

@interface HeatCoolAreaDetailViewController (){
    BOOL sensorsSelected;
    BOOL plansSelected;
    BOOL holidayPlansSelected;
    BOOL heatingSelected;
    
    NSString *hcaID;
    NSString *hcaName;
    NSString *scheduleID;
    NSString *holidayScheduleID;
    
    NSString *stringForSensor;
    
    Device *tempSensor;
    
    UITableView *sensorsTBV;
    UITableView *schedulesTBV;
    UITableView *holidaySchedulesTBV;
    UITableView *heatingTBV;
}
@property (nonatomic, retain) NSArray * temperatureSensors;
@property (nonatomic, retain) NSArray * schedules;
@property (nonatomic, retain) NSMutableDictionary * selectedSettings;


@end

@implementation HeatCoolAreaDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    sensorsSelected = plansSelected = holidayPlansSelected = heatingSelected = NO;
    
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    
    _pickerViewContainer.hidden = YES;
    
    _selectedSettings = [[NSMutableDictionary alloc] init];
    _heatersSelected = [[NSMutableArray alloc] init];
    
    if(_heatCoolArea){
        hcaID = _heatCoolArea.deviceID;
        hcaName = _heatCoolArea.label;
        scheduleID = _heatCoolArea.schedule;
        holidayScheduleID = _heatCoolArea.holidaySchedule;
        tempSensor = _heatCoolArea.temperatureSensor;
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.elan.mac == %@", tempSensor.elan.mac];
        
        _schedules = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"HeatTimeSchedule" withPredicate:predicate];
        
        if (_heatCoolArea.temperatureSensor)[_selectedSettings setObject:@"temperature" forKey:_heatCoolArea.temperatureSensor.deviceID];
    }

    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SUBQUERY(self.states,$state,($state.name == 'temperature' OR $state.name == 'temperature IN' OR $state.name == 'temperature OUT') AND $state.value != nil).@count > 0 AND SUBQUERY(self.states,$state,$state.name == 'mode').@count=0"];
    
    _temperatureSensors = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Device" withPredicate:predicate];
    
    NSPredicate * predicateHCA = [NSPredicate predicateWithFormat:@"(SUBQUERY(self.actions,$action,$action.name MATCHES 'delayed off: set time' OR $action.name MATCHES 'requested temperature' OR $action.name MATCHES 'safe on').@count >0 AND (self.deviceCooling.deviceID == nil OR self.deviceCooling.deviceID == %@) AND (self.deviceHeating.deviceID == nil OR self.deviceHeating.deviceID == %@))", _heatCoolArea.deviceID,  _heatCoolArea.deviceID];
    
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    
    
    _availableHeaters = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Device"
                                                                  withPredicate:predicateHCA
                                                                 sortDescriptor:sortDescriptor
                                                                      inContext:[[SYCoreDataManager sharedInstance] managedObjectContext]];
    
    [self setupHeatingDevices];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark textField Delegate
-(void)textFieldDidEndEditing:(UITextField *)textField{
    hcaName = textField.text;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return NO;
}

#pragma mark - UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView == sensorsTBV && sensorsSelected)return _temperatureSensors.count+1;
    if(tableView == schedulesTBV && plansSelected)return _schedules.count+1;
    if(tableView == holidaySchedulesTBV && holidayPlansSelected)return _schedules.count+1;
    if(tableView == heatingTBV && heatingSelected)return _availableHeaters.count+2;
    if(tableView.tag == 1)return 5;
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1 && sensorsSelected && indexPath.row == 1) return _temperatureSensors.count*50 +50;
    if(tableView.tag == 1 && plansSelected && indexPath.row == 2) return _schedules.count*50 +50;
    if(tableView.tag == 1 && holidayPlansSelected && indexPath.row == 3) return _schedules.count*50 +50;
    if(tableView.tag == 1 && heatingSelected && indexPath.row == 4) return _availableHeaters.count*50+100;
//    if(tableView.tag == 1 && indexPath.row == 3 && modesSelected) return 4*50+50;
    return 50;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        switch (indexPath.row) {
            case 0:{
                NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"enterName", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField *tf = (UITextField*)[[[cell contentView] subviews] firstObject];
                tf.delegate = self;
                tf.attributedPlaceholder = namePlaceHolder;
                if(hcaName != nil){
                    tf.text = hcaName;
                }
                return cell;
            }
                break;
            case 1:{
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 2;
                sensorsTBV = tbv;
                tbv.delegate = self;
                tbv.dataSource = self;
                return cell;
            }
                break;
            case 2: {
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 3;
                schedulesTBV = tbv;
                tbv.delegate = self;
                tbv.dataSource = self;
                return cell;
            }
                break;
            case 3:{
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 4;
                holidaySchedulesTBV = tbv;
                tbv.delegate = self;
                tbv.dataSource = self;
                return cell;
            }
                break;
            case 4:{
                UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 5;
                heatingTBV = tbv;
                tbv.delegate = self;
                tbv.dataSource = self;
                return cell;
            }
                break;
                
            default:{
                UITableViewCell * cell= [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
                return cell;
            }
                break;
        }
    }else if(tableView == sensorsTBV){
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        
        img.hidden = NO;
        
        if(sensorsSelected){
            line.hidden = YES;
            
            if(indexPath.row>0){
                if([tempSensor.deviceID isEqualToString:((Device*)[_temperatureSensors objectAtIndex:indexPath.row-1]).deviceID]){
                    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
                label.text = ((Device*)[_temperatureSensors objectAtIndex:indexPath.row-1]).label;
                img.hidden = YES;
            } else {
                label.text = NSLocalizedString(@"heat_cool_area_sensor_picker_title", nil);
                
                img.hidden = NO;
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            }
        } else {
            label.text = NSLocalizedString(@"heat_cool_area_sensor_picker_title", nil);
            if(tempSensor){
                label.text = tempSensor.label;
            }
            line.hidden = NO;
            img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
        }
        if(!cell){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        return cell;
    } else if(tableView == schedulesTBV){
        if(indexPath.row == 0){
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
            
            
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
            
            UILabel* label = [[cell contentView] viewWithTag:1];
            UIImageView *img = [[cell contentView ] viewWithTag:2];
            UIView *line = [[cell contentView ] viewWithTag:3];

            img.hidden = NO;
            
            HeatTimeSchedule *schedule = [[SYCoreDataManager sharedInstance] getScheduleWithID:scheduleID];
            if(plansSelected){
                line.hidden = YES;
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            label.text = NSLocalizedString(@"heat_cool_area_schedule_picker_title", nil);
            } else {
                line.hidden = NO;
                img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
                label.text = NSLocalizedString(@"heat_cool_area_schedule_picker_title", nil);
                if(schedule){
                    label.text = schedule.label;
                }
            }
            return cell;
        } else {
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
            
            UILabel* label = [[cell contentView] viewWithTag:1];
            UIImageView *img = [[cell contentView ] viewWithTag:2];
            UIView *line = [[cell contentView ] viewWithTag:3];
            
            img.hidden = YES;
            line.hidden = YES;
            
            HeatTimeSchedule *schedule = [_schedules objectAtIndex:indexPath.row-1];
            label.text = schedule.label;
            
            if(scheduleID && [scheduleID isEqualToString:schedule.heattimescheduleID]){
                [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            
            return cell;
        
        }
    } else if(tableView == holidaySchedulesTBV){
        if(indexPath.row == 0){
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
            
            
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
            
            UILabel* label = [[cell contentView] viewWithTag:1];
            UIImageView *img = [[cell contentView ] viewWithTag:2];
            UIView *line = [[cell contentView ] viewWithTag:3];
            
            
            
            img.hidden = NO;
            HeatTimeSchedule *schedule = [[SYCoreDataManager sharedInstance] getScheduleWithID:holidayScheduleID];
            if(holidayPlansSelected){
                line.hidden = YES;
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
                label.text = NSLocalizedString(@"add_holiday_schedule", nil);
            } else {
                line.hidden = NO;
                img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
                label.text = NSLocalizedString(@"add_holiday_schedule", nil);
                if(schedule){
                    label.text = schedule.label;
                }
            }
            return cell;
        } else {
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
            
            UILabel* label = [[cell contentView] viewWithTag:1];
            UIImageView *img = [[cell contentView ] viewWithTag:2];
            UIView *line = [[cell contentView ] viewWithTag:3];
            
            img.hidden = YES;
            line.hidden = YES;
            
            HeatTimeSchedule *schedule = [_schedules objectAtIndex:indexPath.row-1];
            label.text = schedule.label;
            
            if([holidayScheduleID isEqualToString:schedule.heattimescheduleID]){
                [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
            }
            
            return cell;
        }

    } else if(tableView == heatingTBV){
        if(indexPath.row == 0){
            UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
            
            UIView *bgColorView = [[UIView alloc] init];
            [cell setSelectedBackgroundView:bgColorView];
            
            UILabel* label = [[cell contentView] viewWithTag:1];
            UIImageView *img = [[cell contentView ] viewWithTag:2];
            UIView *line = [[cell contentView ] viewWithTag:3];
            
            label.text = @"Central source";
            if(heatingSelected){
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
                line.hidden = YES;
            } else {
                img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
                line.hidden = NO;
            }
            return cell;
        }
        
        UITableViewCell*cell = [tableView dequeueReusableCellWithIdentifier:@"heatingCell"];
        UIButton*leftButton = [[cell contentView] viewWithTag:5];
        UIButton*rightButton = [[cell contentView] viewWithTag:1005];
        
        UILabel*leftLabel = [[cell contentView] viewWithTag:10];
        UILabel*rightLabel = [[cell contentView] viewWithTag:11];
        UILabel*nameLabel = [[cell contentView] viewWithTag:1];
        
        UIView*line = [[cell contentView] viewWithTag:3];
        
        leftLabel.hidden = rightLabel.hidden = YES;
        leftButton.hidden = rightButton.hidden = NO;
        line.hidden = YES;
        
        
        if(indexPath.row == 1){
            leftButton.hidden = rightButton.hidden = YES;
            leftLabel.hidden = rightLabel.hidden = NO;
            line.hidden = NO;
            nameLabel.text = NSLocalizedString(@"heat_source_name", nil);
        } else {
            leftButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
            rightButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
            
            leftButton.tag = rightButton.tag = indexPath.row-2;
            nameLabel.text = ((Device*)[_availableHeaters objectAtIndex:indexPath.row-2]).label;
            switch ([[_heatersSelected objectAtIndex:indexPath.row-2] intValue]) {
                case 0:
                    [leftButton setImage:[UIImage imageNamed:@"vyber_sede_kolecko.png"] forState:UIControlStateNormal];
                    [rightButton setImage:[UIImage imageNamed:@"vyber_sede_kolecko.png"] forState:UIControlStateNormal];
                    break;
                case 1:
                    [leftButton setImage:[UIImage imageNamed:@"vyber_modre_kolecko.png"] forState:UIControlStateNormal];
                    [rightButton setImage:[UIImage imageNamed:@"vyber_sede_kolecko.png"] forState:UIControlStateNormal];
                    break;
                case 2:
                    [leftButton setImage:[UIImage imageNamed:@"vyber_modre_kolecko.png"] forState:UIControlStateNormal];
                    [rightButton setImage:[UIImage imageNamed:@"vyber_modre_kolecko.png"] forState:UIControlStateNormal];
                    break;
                default:
                    break;
            }
        }
        
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        return cell;
    } else {
        UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        return cell;
    }
    UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == sensorsTBV && indexPath.row == 0 && !sensorsSelected){
        sensorsSelected = YES;
        plansSelected = NO;
        holidayPlansSelected = NO;
        heatingSelected = NO;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
    } else if (tableView == sensorsTBV && indexPath.row == 0 && sensorsSelected){
        sensorsSelected = NO;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
    } if (tableView == sensorsTBV && indexPath.row > 0){
        Device *device = [_temperatureSensors objectAtIndex:indexPath.row-1];
        tempSensor = device;
        
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.elan.mac == %@", device.elan.mac];
        
        _schedules = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"HeatTimeSchedule" withPredicate:predicate];
        
        [_selectedSettings setObject:@"temperature" forKey:tempSensor.deviceID];
        
        if([device hasStateName:@"temperature OUT"]){
            self.pickerViewContainer.hidden = NO;
        }
        
    } else if(tableView == schedulesTBV && indexPath.row == 0 && !plansSelected){
        sensorsSelected = NO;
        plansSelected = YES;
        holidayPlansSelected = NO;
        heatingSelected = NO;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
        
    } else if (tableView == schedulesTBV && indexPath.row == 0 && plansSelected){
        plansSelected = NO;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
    } else if(tableView == holidaySchedulesTBV && indexPath.row == 0 && !holidayPlansSelected){
        sensorsSelected = NO;
        plansSelected = NO;
        holidayPlansSelected = YES;
        heatingSelected = NO;

        [_tableView beginUpdates];
        [_tableView endUpdates];
        
        
    } else if (tableView == holidaySchedulesTBV && indexPath.row == 0 && holidayPlansSelected){
        holidayPlansSelected = NO;

        [_tableView beginUpdates];
        [_tableView endUpdates];
    } else if((tableView == schedulesTBV || tableView == holidaySchedulesTBV) && indexPath.row >0){
        HeatTimeSchedule*schedule = [_schedules objectAtIndex:indexPath.row-1];
        if(plansSelected){
            scheduleID = schedule.heattimescheduleID;
        } else {
            holidayScheduleID = schedule.heattimescheduleID;
        }
    } else if(tableView == heatingTBV && indexPath.row == 0 && !heatingSelected){
        sensorsSelected = NO;
        plansSelected = NO;
        holidayPlansSelected = NO;
        heatingSelected = YES;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
    } else if(tableView == heatingTBV && indexPath.row == 0 && heatingSelected){
        heatingSelected = NO;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
    }

    if(indexPath.row == 0)
        [tableView reloadData];
    
    [sensorsTBV reloadData];
    [schedulesTBV reloadData];
    [holidaySchedulesTBV reloadData];
    [heatingTBV reloadData];
    
    [self setTableViewBorders];
    
}

-(void)setTableViewBorders{
    sensorsTBV.layer.borderColor = schedulesTBV.layer.borderColor = holidaySchedulesTBV.layer.borderColor = heatingTBV.layer.borderColor = nil;
    sensorsTBV.layer.borderWidth = schedulesTBV.layer.borderWidth = holidaySchedulesTBV.layer.borderWidth = heatingTBV.layer.borderWidth = 0;
    
    if(sensorsSelected){
        sensorsTBV.layer.borderColor = USBlueColor.CGColor;
        sensorsTBV.layer.borderWidth = 1.0f;
    }
    
    if(plansSelected){
        schedulesTBV.layer.borderColor = USBlueColor.CGColor;
        schedulesTBV.layer.borderWidth = 1.0f;
    }
    
    if(holidayPlansSelected){
        holidaySchedulesTBV.layer.borderColor = USBlueColor.CGColor;
        holidaySchedulesTBV.layer.borderWidth = 1.0f;
    }
    
    if(heatingSelected){
        heatingTBV.layer.borderColor = USBlueColor.CGColor;
        heatingTBV.layer.borderWidth = 1.0f;
    }
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return 2;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width-25, 44)];
    label.backgroundColor = [UIColor blackColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Roboto-Light" size:18];
    
    switch (row) {
        case 0:
            label.text = NSLocalizedString(@"heat_cool_outdoor_temperature_sensor", nil);
            break;
        case 1:
            label.text = NSLocalizedString(@"heat_cool_indoor_temperature_sensor", nil);
            break;
        case 2:
            label.text = NSLocalizedString(@"heat_cool_kombi_sensor",nil);
            break;
        default:
            break;
    }
    return label;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 60.f;
}

- (IBAction)doneButtonTapped:(id)sender {
    _pickerViewContainer.hidden = YES;
    NSString * value = nil;
    NSString * settingsValue = nil;
    int row = (int)[_pickerView selectedRowInComponent:0];
    
    [_selectedSettings removeAllObjects];
    if (row==0){
        value =NSLocalizedString(@"heat_cool_outdoor_temperature_sensor", nil);
        settingsValue = @"temperature OUT";
        [_selectedSettings setObject:settingsValue forKey:tempSensor.deviceID];
    }else if (row==1){
        value =NSLocalizedString(@"heat_cool_indoor_temperature_sensor", nil);
        settingsValue = @"temperature IN";
        [_selectedSettings setObject:settingsValue forKey:tempSensor.deviceID];
    
    }else if (row==2){
        value=NSLocalizedString(@"heat_cool_kombi_sensor", nil);
        settingsValue = @"combi";
        [_selectedSettings setObject:@"combi" forKey:tempSensor.deviceID];
    }
    stringForSensor =  [NSString stringWithFormat:@"%@ - %@", tempSensor.label, value];
_pickerView.hidden=YES;
    
    [sensorsTBV reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

-(void)setupHeatingDevices{

    int i=0;
    while ( i < [_availableHeaters count]){
        Device* device = [_availableHeaters objectAtIndex:i];
        [_heatersSelected addObject:[[NSNumber alloc] initWithInt:0]];
        if (_heatCoolArea !=nil){
            if ([_heatCoolArea.heatingDevices containsObject:device] || [_heatCoolArea.coolingDevices containsObject:device]){
                [_heatersSelected replaceObjectAtIndex:i withObject:[[NSNumber alloc] initWithInt:1]];
            }
            if ([_heatCoolArea.centralSources containsObject:device]){
                [_heatersSelected replaceObjectAtIndex:i withObject:[[NSNumber alloc] initWithInt:2]];
            }
        }
        i++;
    }
}
- (IBAction)addButtonTapped:(id)sender {
    UIButton*button=sender;
    NSLog(@"addButtonTag: %ld", (long)button.tag);
    if ([[_heatersSelected objectAtIndex:((UIButton*)sender).tag] intValue]==1){
        [_heatersSelected replaceObjectAtIndex:((UIButton*)sender).tag withObject:[[NSNumber alloc] initWithInt:0]];
    }else if ([[_heatersSelected objectAtIndex:((UIButton*)sender).tag] intValue]==0){
        [_heatersSelected replaceObjectAtIndex:((UIButton*)sender).tag withObject:[[NSNumber alloc] initWithInt:1]];
    }else{
        [_heatersSelected replaceObjectAtIndex:((UIButton*)sender).tag withObject:[[NSNumber alloc] initWithInt:0]];
    }
    
    button.tag = 5;
    [heatingTBV reloadData];
}
- (IBAction)centralButtonTapped:(id)sender {
    UIButton*button=sender;
    NSLog(@"rightButtonTag: %ld", (long)button.tag);
    if ([[_heatersSelected objectAtIndex:((UIButton*)sender).tag] intValue]==0) return;
    
    if ([[_heatersSelected objectAtIndex:((UIButton*)sender).tag] intValue]==1){
        [_heatersSelected replaceObjectAtIndex:((UIButton*)sender).tag withObject:[[NSNumber alloc] initWithInt:2]];
    }else{
        [_heatersSelected replaceObjectAtIndex:((UIButton*)sender).tag withObject:[[NSNumber alloc] initWithInt:1]];
    }
    
    button.tag = 1005;
    [heatingTBV reloadData];
}


-(IBAction)Save:(id)sender{
    NSArray * heatingDevices = [self selectHeatingDevices];
    
    if (!tempSensor){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"heat_cool_area_empty_sensor_error", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }else if (scheduleID == nil){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"heat_cool_area_empty_schedule_error", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }else if ([heatingDevices count]==0){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"heat_cool_area_empty_cooling_heating_devices", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else
        if ([hcaName isEqualToString:@""] || !hcaName) {
            
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"errorName", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
            
        }
/*    AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (delegate.limitsSettings!=nil){
        NSDictionary * heatingDevicesDict =[[delegate.limitsSettings objectForKey:@"heatcoolarea"] objectForKey:@"heating devices"];
        if ([heatingDevices count] > [[heatingDevicesDict objectForKey:@"max count"] intValue]){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:[NSString stringWithFormat:NSLocalizedString(@"Number of heating devices must be less or equal than %d", nil),[[heatingDevicesDict objectForKey:@"max count"] intValue] ]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
            
        }
        
    }*/
    
    
    
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    NSMutableDictionary * heatCoolSettings = [[NSMutableDictionary alloc] init];
    NSDictionary * deviceInfo = [[NSDictionary alloc] initWithObjectsAndKeys:hcaName, @"label",
                                 @"temperature regulation area", @"type",
                                 @"HeatCoolArea", @"product type", nil];
    
    [heatCoolSettings setObject:deviceInfo forKey:@"device info"];
    [heatCoolSettings setObject:scheduleID forKey:@"schedule"];
    
    if(holidayScheduleID != nil){
        [heatCoolSettings setObject:holidayScheduleID forKey:@"schedule2"];
    } else {
        [heatCoolSettings setObject:[[NSNumber alloc] initWithInt:0] forKey:@"schedule2"];
    }
    
    
    [heatCoolSettings setObject:heatingDevices forKey:@"heating devices"];
    
    NSMutableArray * coolingDevices = [[NSMutableArray alloc] init];
    for(Device *myDevice in _heatCoolArea.coolingDevices){
        [coolingDevices addObject:myDevice.deviceID];
    }
    
    [heatCoolSettings setObject:coolingDevices forKey:@"cooling devices"];
    if (hcaID!=nil){
        [heatCoolSettings setObject:hcaID forKey:@"id"];
    }
    
    [heatCoolSettings setObject:_selectedSettings forKey:@"temperature sensor"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:heatCoolSettings
                                                       options:(NSJSONWritingOptions)  (NSJSONWritingPrettyPrinted)
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        
    } else {
        NSLog(@"%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    }
    
    [[SYAPIManager sharedInstance] postDataWithHeatCoolAreaToElan:tempSensor.elan dict:heatCoolSettings success:^(AFHTTPRequestOperation * operation, id response){
        
        [_loaderDialog hide];
        [[self navigationController] popViewControllerAnimated:YES];
        
        
    } failure:^(AFHTTPRequestOperation * operation, NSError * error){
        [_loaderDialog hide];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"heat_cool_area_failed_add", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        
    }];
    
}

-(NSArray*)selectHeatingDevices{
    NSMutableArray * resultArray = [[NSMutableArray alloc] init];
    for (int i=0;i< [_heatersSelected count];i++){
        NSNumber * number = [_heatersSelected objectAtIndex:i];
        if ([number intValue]!=0){
            NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
            Device * device = [_availableHeaters objectAtIndex:i];
            [dict setObject:device.deviceID forKey:@"id"];
            if ([number intValue]==2){
                [dict setObject:[[NSNumber alloc] initWithBool:YES] forKey:@"central source"];
            }else{
                [dict setObject:[[NSNumber alloc] initWithBool:NO] forKey:@"central source"];
            }
            [resultArray addObject:dict];
        }
        
        
    }
    return resultArray;
    
}

@end
