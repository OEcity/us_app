//
//  CentralSourceDetalViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 12/5/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "SYCentralSourceDetalViewController.h"
#import "SYCoreDataManager.h"
#import "ResourceTiles.h"
#import "SYAPIManager.h"
@interface SYCentralSourceDetalViewController ()

@end

@implementation SYCentralSourceDetalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _HeatCoolArea = [[SYCoreDataManager sharedInstance] getAllHeatCoolAreas];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SUBQUERY(self.actions,$action,($action.name MATCHES 'delayed off: set time' OR $action.name MATCHES 'safe on') ).@count>0 "];
    
    _centralSourceElements = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Device" withPredicate:predicate];

    [_pickerView reloadAllComponents];
    [self generateBody];
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    _pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
    _pickerViewController.delegate = self;
    if (_centralSourceItem){
        [_tfName setText:[_centralSourceItem valueForKey:@"label"]];

        Device * device = [self seekDeviceWithName:[_centralSourceItem valueForKey:@"device"]];
        [_lElement setText:device.label];
        _centralSourceElement = device;
    }
    
    // set translations
    [_lElementTitle setText:NSLocalizedString(@"heat_source_device", nil)];
    [_lNameTitle setText:NSLocalizedString(@"heat_source_name", nil)];
    [_lHeatingArea setText:NSLocalizedString(@"heat_source_heat_cool_areas", nil)];
    [_btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_btnSave setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];

    
}

-(HeatCoolArea*)seekDeviceWithName:(NSString *)name{
    
    for (HeatCoolArea * device in _centralSourceElements){
        if ([device.deviceID isEqualToString:name]){
            return device;
        }
    }
    return nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)generateBody{
    int start=_lHeatingArea.frame.origin.y+_lHeatingArea.frame.size.height;
    _heatersSelected = [[NSMutableArray alloc] init];
    for (int i =0; i < [_HeatCoolArea count]; i++){
        [_heatersSelected addObject:[[NSNumber alloc] initWithInt:0]];
        UIView * holder = [[UIView alloc] initWithFrame:CGRectMake(0, start+i*50, 320, 50)];
        holder.tag=99;
        [holder setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3f]];
        
        UIImageView * iVSeparator3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 49, 320, 1)];
        [iVSeparator3 setBackgroundColor:[UIColor colorWithRed:87 green:87 blue:87 alpha:0.8]];
        [holder addSubview:iVSeparator3];
        
        Device* device = [_HeatCoolArea objectAtIndex:i];
        
        UILabel * name = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, 93, 50)];
        [name setText:device.label];
        
        //[name setTextColor:[UIColor whiteColor]];
        
        [name setTextAlignment:NSTextAlignmentCenter];
        
        name.adjustsFontSizeToFitWidth=YES;
        name.minimumScaleFactor=0.5;
        
        
        UIButton * centralSource = [[UIButton alloc] initWithFrame:CGRectMake(262, 5, 40, 40)];
        [centralSource setBackgroundImage:[UIImage imageNamed:@"krizek"] forState:UIControlStateNormal];
        [centralSource addTarget:self action:@selector(centralButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        centralSource.tag=i;
        centralSource.userInteractionEnabled=YES;
        [holder addSubview:name];
        if (_centralSourceItem && [_centralSourceItem objectForKey:@"areas"]){
            if ([[_centralSourceItem objectForKey:@"areas"] indexOfObject:device.deviceID] != NSNotFound){
                [_heatersSelected replaceObjectAtIndex:[_heatersSelected count]-1 withObject:[[NSNumber alloc] initWithInt:1]];
                [centralSource setImage:[UIImage imageNamed:@"fajfka"] forState:UIControlStateNormal];
                

            }
            

            
        }
        [holder addSubview:centralSource];
        [_scrollView addSubview:holder];

        
    }
}

-(NSArray*)listAddedAreas{
    NSMutableArray * added =[[NSMutableArray alloc] init];
    for (int i=0; i <  [_HeatCoolArea count]; i++){
        if ( [[_heatersSelected objectAtIndex:i] integerValue]==1){
            
            Device * device = [_HeatCoolArea objectAtIndex:i];
            [added addObject:device.deviceID];
            
        }
        
    }
    return added;
    
}

- (void)centralButtonTapped:(id)sender
{
    
    UIButton * button = (UIButton*)sender;
    if ([[_heatersSelected objectAtIndex:((UIButton*)sender).tag] intValue]==0){
        [_heatersSelected replaceObjectAtIndex:((UIButton*)sender).tag withObject:[[NSNumber alloc] initWithInt:1]];
        
        [button setImage:[UIImage imageNamed:@"fajfka"] forState:UIControlStateNormal];
    }else{
        [_heatersSelected replaceObjectAtIndex:((UIButton*)sender).tag withObject:[[NSNumber alloc] initWithInt:0]];
        [button setImage:nil forState:UIControlStateNormal];
        
    }
}

#pragma TextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}

- (IBAction)resignAll:(id)sender {

    [_tfName resignFirstResponder];
//    _pickerView.hidden= YES;
}

#pragma mark -
#pragma Picker View Data Source methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    //Return the number of components
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    //Return the number of rows in the component
    return [_centralSourceElements count];
}

#pragma mark -
#pragma Picker View Delegate methods
/*
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    Device * element = [_centralSourceElements objectAtIndex:row];
    return element.label;
}
*/

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    Device * device = [_centralSourceElements objectAtIndex:row];
    
    
    Tile * iconTile = [ResourceTiles generateDeviceTileForName:device.type];
    if (iconTile.tile_off==nil){
        iconTile = [ResourceTiles generateDeviceTileForName:@"all"];
    }
    UIImageView* icon = [[UIImageView alloc]initWithImage:[iconTile tile_on]];
    [icon setFrame:CGRectMake(0, 0, 50,50)];
    icon.contentMode = UIViewContentModeScaleAspectFit;
    UIView * overallView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
    [overallView addSubview:icon];
    UILabel * label = [[UILabel alloc] initWithFrame:CGRectMake(50, 0, 260, 50)];
    [label setText:device.label];
    [overallView addSubview:label];
    return overallView;
    
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50.f;
}

-(void)DoneButtonTappedForRow:(NSInteger)row inComponent:(NSInteger)component{
    _centralSourceElement = [_centralSourceElements objectAtIndex:row];
    [_lElement setText:_centralSourceElement.label];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _pickerViewController.row = row;
    _pickerViewController.component= component;

    /*_centralSourceElement = [_centralSourceElements objectAtIndex:row];
    [_lElement setText:_centralSourceElement.label];
    _pickerView.hidden=YES;
     */
    
}


- (IBAction)showElements:(id)sender {
//    _pickerView.hidden= NO;
    [[[[self.view superview] superview] superview] addSubview:_pickerViewController.view ];
}

- (IBAction)backButtonTap:(id)sender {
    [_container setViewController:@"centralSource"];

}
- (IBAction)saveButtonTap:(id)sender {
    if ([_tfName.text isEqualToString:@""] ) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"heat_source_failed_no_name", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];

        return;
        
    }
    if ([_lElement.text isEqualToString:@""]){
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"heat_source_failed_no_device", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        return;
        
    }
    if ([[self listAddedAreas] count]==0){
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"heat_source_failed_no_area", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        return;

    }
    
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    
    NSMutableDictionary * deviceInfo = [[NSMutableDictionary alloc] initWithObjectsAndKeys:_tfName.text, @"label",
                                 @"heating", @"source type",
                                 _centralSourceElement.deviceID, @"device",
                                 [self listAddedAreas], @"areas", nil];
    if (_centralSourceItem){
        [deviceInfo setObject:[_centralSourceItem objectForKey:@"id"] forKey:@"id"];
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:deviceInfo
                                                       options:(NSJSONWritingOptions)  (NSJSONWritingPrettyPrinted)
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        
    } else {
        NSLog(@"%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    }

    [[SYAPIManager sharedInstance] postCentralSourceWithDictionary:deviceInfo success:^(AFHTTPRequestOperation * operation, id response){
        
        [_loaderDialog hide];
        [_container setViewController:@"centralSource"];
        
        
    } failure:^(AFHTTPRequestOperation * operation, NSError * error){
        [_loaderDialog hide];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"heat_source_failed_add", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        
    }];

}

- (IBAction)editBegin:(id)sender {
    UITextField * tf = sender;
    [tf setBackground:[UIImage imageNamed:@"edit_text_bcg_on.png"]];
    [tf setTextColor:[UIColor whiteColor]];
    
}
- (IBAction)editEnd:(id)sender {
    UITextField * tf = sender;
    [tf setBackground:[UIImage imageNamed:@"edit_text_bcg.png"]];
    [tf setTextColor:[UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1]];
}


@end
