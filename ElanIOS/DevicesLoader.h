//
//  DevicesLoader.h
//  ElanIOS
//
//  Created by Vratislav Zima on 5/30/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "URLConnectionListener.h"
#import "URLConnector.h"
#import "Device.h"

@interface DevicesLoader : NSObject <URLConnectionListener>

-(void) LoadDevices:(id)sender;
- (void)requestDeviceInfo:(NSArray*)devicesArray sender:(id)sender;
-(void) LoadOnlyDevices:(id)sender;



@property (nonatomic) BOOL isLoading;
@property (nonatomic, retain) NSMutableArray * rooms;
@property (nonatomic, retain) NSMutableArray * devices;
@end
