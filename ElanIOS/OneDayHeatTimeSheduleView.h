//
//  OneDayHeatTimeSheduleView.h
//  iHC-MIRF
//
//  Created by admin on 24.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TempScheduleDay.h"
#import "TempDayMode.h"



@interface OneDayHeatTimeSheduleView : UIView

@property (nonatomic, retain) TempScheduleDay * mDay;

-(void) repaintDay:(TempScheduleDay*) day;


@end
