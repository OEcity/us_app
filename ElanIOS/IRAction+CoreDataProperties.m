//
//  IRAction+CoreDataProperties.m
//  
//
//  Created by Tom Odler on 12.09.16.
//
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "IRAction+CoreDataProperties.h"

@implementation IRAction (CoreDataProperties)

@dynamic id;
@dynamic irCodes;
@dynamic irLed;
@dynamic device;

@end
