//
//  PageViewController.h
//  SAM_11_NewDesign
//
//  Created by admin on 06.07.16.
//  Copyright © 2016 admin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageViewController : UIViewController
/*
 *  @property   currentOrderedContentArray  holds all view controlles for page view controller
 */
@property (readonly, strong, nonatomic) NSMutableArray <UIViewController *>*currentOrderedContentArray;

/*
 *  @property   futureOrderedContentArray   use this propery to customise pages in page view, changes must be applyed using applyFutureStructureWithListedPageIndex:
 */
@property (strong, nonatomic) NSMutableArray <UIViewController *> *futureOrderedContentArray;

/*
 *  @property   looped  if you want to reach first page scrolling left from last page and last page scrolling right from first page set this property to YES, othervise NO, NO by default
 */
@property (nonatomic, getter=isLooped) BOOL looped;

/*
 *  @property   pageControl contains reference to page controll of page view controller
 */
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

/*
 *  this method changes content of page view controller and move page view controller to view controller on index parameter
 *
 *  @parm   index   index of page listed after content change, scrolled to 0 oage if index is out of range
 */
-(void)applyFutureStructureWithListedPageIndex:(NSUInteger)index;

/*
 *  drop all items in futureOrderedContentArray and fills this array with currentOrderedContentArray
 */
-(void)dropStructureAndFillWithCurrent;

@end
