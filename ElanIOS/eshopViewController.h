//
//  eshopViewController.h
//  iHC-MIRF
//
//  Created by Tom Odler on 07.10.15.
//  Copyright © 2015 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface eshopViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *newsButton;
@property (weak, nonatomic) IBOutlet UIImageView *news_sipka;
@property (weak, nonatomic) IBOutlet UILabel *news_label;
@property (weak, nonatomic) IBOutlet UIImageView *news_image;


@end
