//
//  GuideScenesAddViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 04.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HUDWrapper.h"
#import "Device.h"
#import "Scene.h"

@interface GuideScenesAddViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, NSFetchedResultsControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) HUDWrapper *loaderDialog;
@property (nonatomic, retain) NSMutableDictionary * selectedDevices;
@property (nonatomic)NSMutableArray *actionsArray;
@property (nonatomic, strong) NSFetchedResultsController* fetchedResultsController;
@property (nonatomic, retain) Scene* scene;


@end
