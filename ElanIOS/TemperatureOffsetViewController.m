//
//  TemperatureOffsetViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/17/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "TemperatureOffsetViewController.h"

@interface TemperatureOffsetViewController ()
{
    NSTimer *_timer;
}

@end

@implementation TemperatureOffsetViewController

float maxValue=20;
float minValue=-20;

- (void)viewDidLoad {
    [super viewDidLoad];
    //    _realTemperature= 0;
    [self displayNumber];
    [_bCancel setTitle:NSLocalizedString(@"confirmation.cancel",nil) forState:UIControlStateNormal];
    [_bDone setTitle:NSLocalizedString(@"confirmation.OK",nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(id)initWithDelegate:(id<TemperatureSetterDelegate>)delegate min:(float)min max:(float)max data:(NSDictionary*)data{
    self = [[TemperatureOffsetViewController alloc] initWithNibName:@"TemperatureOffsetViewController" bundle:nil];
    
    if (self !=nil){
        _delegate = delegate;
        minValue = min;
        maxValue = max;
        _realTemperature = minValue;
        _data =data;
        if ([_data valueForKey:@"setTemperature"]!=nil){
            _realTemperature = [[_data valueForKey:@"setTemperature"] floatValue];
            [self displayNumber];
        }
    }
    return self;
}

- (IBAction)touchDown:(id)sender {
    NSLog(@"start");
    [self determineActionForSender:sender];
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.2f
                                              target:self
                                            selector:@selector(_timerFired:)
                                            userInfo:sender
                                             repeats:YES];
}


- (IBAction)touchUp:(id)sender {
    NSLog(@"stop");
    if (_timer != nil){
        [_timer invalidate];
        _timer = nil;
    }
}


- (void)_timerFired:(NSTimer*)info
{
    id sender = [info userInfo];
    [self determineActionForSender:sender];
}


- (void) determineActionForSender:(id)sender{
    if ([_bDigitsDown isEqual:sender]){
        [self downDigits:sender];
    } else if ([_bDigitsUp isEqual:sender]){
        [self upDigits:sender];
    } else if ([_bNumbersDown isEqual:sender]){
        [self downNumber:sender];
    } else if ([_bNumbersUp isEqual:sender]){
        [self upNumber:sender];
    }
}


- (IBAction)upDigits:(id)sender {
    if (_realTemperature<maxValue) _realTemperature +=0.5;
    [self displayNumber];
}

- (IBAction)downDigits:(id)sender {
    if (_realTemperature>minValue) _realTemperature -=0.5;
    [self displayNumber];
}

- (IBAction)upNumber:(id)sender {
    if (_realTemperature<maxValue) _realTemperature +=1.0;
    if (_realTemperature>maxValue)_realTemperature -=0.5;
    [self displayNumber];
}

- (IBAction)downNumber:(id)sender {
    
    if (_realTemperature>minValue) _realTemperature -=1.0;
    if (_realTemperature<minValue)_realTemperature +=0.5;
    [self displayNumber];
}

-(void)displayNumber{
    float digits = _realTemperature - (int)_realTemperature;
    [_lDigits setText:[NSString stringWithFormat:@"%0.2d", abs((int)(digits*100))]];
    
    int numbers = (int)_realTemperature;
    if (numbers==0 && _realTemperature<0)
        [_lNumbers setText:[NSString stringWithFormat:@"-%d", numbers]];
    else
        [_lNumbers setText:[NSString stringWithFormat:@"%d", numbers]];
}


- (IBAction)OKButtonPressed:(id)sender {
    [_delegate temperatureSet:_realTemperature data:_data];
    [self.view removeFromSuperview];
}

- (IBAction)CancelButtonPressed:(id)sender {
    [self.view removeFromSuperview];
}

-(void)setBoundsWithMax:(float)max min:(float)min{
    maxValue = max;
    minValue=min;
}
@end
