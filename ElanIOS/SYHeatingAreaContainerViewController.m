//
//  SYHeatingAreaContainerViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/8/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "SYHeatingAreaContainerViewController.h"
#import "SYCoreDataManager.h"
#import "IconWithLabelTableViewCell.h"
#import "SYAPIManager.h"
@interface SYHeatingAreaContainerViewController ()
@property (nonatomic, strong) NSFetchedResultsController* devices;

@end

@implementation SYHeatingAreaContainerViewController
BOOL shouldDisplayLoader;

- (void)viewDidLoad {
    [super viewDidLoad];
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
  //  [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncComplete)
                                                 name:@"dataSyncComplete"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(refreshStarted:)
                                                 name:@"loadingStarted"
                                               object:nil];
    
    
    
    
        //[_loaderDialog hide];
    [_btnBack setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_btnAdd setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];

 

    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self initFetchedResultsController];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!_devices || ![_devices.fetchedObjects count]){
        
        CGFloat screenHeight = self.view.frame.size.height;
        _lNoContent = [[UILabel alloc] initWithFrame:CGRectMake(0, screenHeight/2 - 10, 320, 20)];
        _lNoContent.textAlignment = NSTextAlignmentCenter;
        [_lNoContent setTextColor:[UIColor blackColor]];
        [_lNoContent setText:NSLocalizedString(@"heat_area_settings_empty_list", nil)];
        [self.view  addSubview:_lNoContent];
        
        
    }
    NSLog(@"before viwwillappear");

    if (shouldDisplayLoader){
        
        NSLog(@"displaying loader");
        [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    }
}

- (void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    _devices = nil;
}


#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"HeatCoolArea"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];

    
    _devices = [[NSFetchedResultsController alloc]
                                 initWithFetchRequest:fetchRequest
                                 managedObjectContext:context
                                 sectionNameKeyPath:nil
                                 cacheName:nil];
    _devices.delegate = self;
    NSError *error;
    
    if (![_devices performFetch:&error]) {
        NSLog(@"error fetching Device: %@",[error description]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods
//- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
//    [self.tableView beginUpdates];
//}
//
//
//- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
//       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
//      newIndexPath:(NSIndexPath *)newIndexPath {
//    
//    UITableView *tableView = self.tableView;
//    
//    switch(type) {
//            
//        case NSFetchedResultsChangeInsert:
//            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
//                             withRowAnimation:UITableViewRowAnimationFade];
//            break;
//            
//        case NSFetchedResultsChangeDelete:
//            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
//                             withRowAnimation:UITableViewRowAnimationFade];
////            self.emptyLabel.hidden = [_fetchedResultsController.fetchedObjects count] > 0;
//            break;
//            
//        case NSFetchedResultsChangeUpdate:
//            [self configureCell:[tableView cellForRowAtIndexPath:indexPath]
//                    atIndexPath:indexPath];
//            break;
//            
//        case NSFetchedResultsChangeMove:
//            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
//                             withRowAnimation:UITableViewRowAnimationFade];
//            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]
//                             withRowAnimation:UITableViewRowAnimationFade];
//            break;
//    }
//}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
//    [self.tableView endUpdates];
    self.lNoContent.hidden = [_devices.fetchedObjects count] > 0;
    [self.tableView reloadData];
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {


}

- (IBAction)dismissController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)addHeatingArea:(id)sender {
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    [[SYAPIManager sharedInstance] getTemperatureSchedulesWithSuccess:^(AFHTTPRequestOperation *request, id response){
        [_loaderDialog hide];
        int arraySize = (int)[((NSArray*)response) count];
        if (arraySize==0){
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                              message:NSLocalizedString(@"add_heat_cool_area_error", nil)
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];

        }else{
            _container.heatingDetailDevice=nil;
            [_container setViewController:@"heatingAreaDetail"];
        }
        
    }
                                                             failure:^(AFHTTPRequestOperation * request, NSError * error){
                                                                 
                                                                 [_loaderDialog hide];
                                                                 UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                                                                                   message:NSLocalizedString(@"deviceCommFailed", nil)
                                                                                                                  delegate:nil
                                                                                                         cancelButtonTitle:@"OK"
                                                                                                         otherButtonTitles:nil];
                                                                 [message show];
                                                             }];
    

    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_devices.fetchedObjects count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 72;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString* cellIdentifier = @"heatingAreaCell";
    IconWithLabelTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[IconWithLabelTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    Device* device = [_devices objectAtIndexPath:indexPath];
    [cell.lLabel setText:device.label];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
        [_container setHeatingDetailDevice:[_devices objectAtIndexPath:indexPath]];
        [_container setViewController:@"heatingAreaDetail"];
}



#pragma mark DELETE table rows section
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {

    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete && _devices) {
        if ([_devices.fetchedObjects count]<= indexPath.row) return;
        Device * device = [_devices objectAtIndexPath:indexPath];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_heatingArea", nil), device.label] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
        alert.tag = indexPath.row;
        [alert show];
    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Cancel Tapped.");
    }
    else if (buttonIndex == 1) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
        
        Device * device = [_devices objectAtIndexPath:indexPath];
        [[SYAPIManager sharedInstance] deleteDeviceWithId:device.deviceID success:^(AFHTTPRequestOperation * request, id object){
            
        } failure:^(AFHTTPRequestOperation * request, NSError * error){
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                              message:NSLocalizedString(@"heat_cool_area_failed_delete", nil)
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];
            
        }];
    }
}



-(void)refreshStarted:(NSNotification *)note{
    if ([[note.userInfo objectForKey:@"type"] isEqualToString:@"allDevices"]){

            NSLog(@"Loader appearing");

            [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
            shouldDisplayLoader = YES;


    }
}

-(void)syncComplete{
    shouldDisplayLoader = NO;
    [_loaderDialog hide];
}


@end
