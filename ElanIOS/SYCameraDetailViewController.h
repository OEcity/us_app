//
//  SYCameraDetailViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/14/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Camera.h"
#import "MotionJpegImageView.h"

@interface SYCameraDetailViewController : UIViewController{
    @private NSTimer * staticJPGTimer;
}


@property (nonatomic, retain) Camera * camera;
@property (nonatomic, retain) IBOutlet MotionJpegImageView * player;
@property (nonatomic, retain) IBOutlet UIButton * closeButton;
@property (nonatomic, retain) NSTimer * hideTimer;
@property (nonatomic, retain) NSOperationQueue * operationQueue;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *controllCollection;

-(IBAction)onTap:(id)sender;


@end
