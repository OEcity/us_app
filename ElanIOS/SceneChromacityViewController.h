//
//  SceneChromacityViewController.h
//  Click Smart
//
//  Created by Tom Odler on 08.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"
#import "SYAddSceneViewController.h"
#import "GuideScenesAddViewController.h"

@interface SceneChromacityViewController : UIViewController
@property Device *device;
@property (nonatomic, copy)  NSString * deviceName;
@property (nonatomic) SYAddSceneViewController *controller;
@property (nonatomic) GuideScenesAddViewController *guideController;
@end
