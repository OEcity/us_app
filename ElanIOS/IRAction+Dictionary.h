//
//  IRAction+Dictionary.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 13.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "IRAction.h"
#import "SYSerializeProtocol.h"

@interface IRAction (DictUpdate) <SYSerializeProtocol>

- (BOOL)updateWithDictionary:(NSDictionary *)dict andName:(NSString*)name;


@end
