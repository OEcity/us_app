//
//  EFGradientGenerator.m
//  RGBViewController
//
//  Created by admin on 21.06.16.
//  Copyright © 2016 admin. All rights reserved.
//

/**
 *  Macro for converting radian degrees from cartesian reference (0 radians is along X axis)
 *   to 'compass style' reference (0 radians is along Y axis (ie North on a compass)).
 *
 *  @param rad Radian degrees to convert from Cartesian reference
 *
 *  @return Radian Degrees in 'Compass' reference
 */
#define CartesianToCompass(rad) ( rad + M_PI/2 )
/**
 *  Macro for converting radian degrees from 'compass style' reference (0 radians is along Y axis (ie North on a compass))
 *   to cartesian reference (0 radians is along X axis).
 *
 *  @param rad Radian degrees to convert from 'Compass' reference
 *
 *  @return Radian Degrees in Cartesian reference
 */
#define CompassToCartesian(rad) ( rad - M_PI/2 )
#define ToRad(deg) 		( (M_PI * (deg)) / 180.0 )
#define ToDeg(rad)		( (180.0 * (rad)) / M_PI )
#define SQR(x)			( (x) * (x) )

#import "EFGradientGenerator.h"

static NSMutableDictionary <NSString *, EFGradientGenerator *> *staticInstances;

@implementation ColorContainer
@end


@implementation EFGradientGenerator

+(EFGradientGenerator *)getStaticInstanceForKey:(NSString *)key{
    [EFGradientGenerator initStaticInstanceDictionaryIfNotExists];
    
    return [staticInstances objectForKey:key];
}

+(EFGradientGenerator *)createStaticInstanceForKey:(NSString *)key withSize:(CGSize)size{
    [EFGradientGenerator initStaticInstanceDictionaryIfNotExists];

    if ([staticInstances objectForKey:key] == nil) {
        EFGradientGenerator *newGenerator = [[EFGradientGenerator alloc] initWithSize:size];
        [staticInstances setObject:newGenerator forKey:key];
        return newGenerator;
    }
    
    return nil;
}

+(BOOL)hasStaticInstanceForKey:(NSString *)key{
    [EFGradientGenerator initStaticInstanceDictionaryIfNotExists];

    if ([staticInstances objectForKey:key]) {
        return YES;
    }
    
    return NO;
}

+(NSArray <NSString *> * )allStaticInstanceKeys{
    [EFGradientGenerator initStaticInstanceDictionaryIfNotExists];
    
    return [staticInstances allKeys];
}

+(void)initStaticInstanceDictionaryIfNotExists{
    if (staticInstances == nil) {
        staticInstances = [[NSMutableDictionary alloc] init];
    }
}

+(CGFloat)rangesafeColorForColor:(CGFloat)color{
    if (color < 0) {
        return 0;
    }else if (color > 1){
        return 1;
    }
    
    return color;
}

-(id)initWithSize:(CGSize)size{
    self = [super init];
    if (self) {
        _size = size;
        _renderedImage = nil;
        _colors = [[NSMutableArray alloc] init];
        _backgroundColor = [UIColor clearColor];
        _sectors = 1;
        _radius = 0;
        _colorArray = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)setSectors:(NSUInteger)sectors{
    if (sectors == 0) {
        _sectors = 1;
    }else{
        _sectors = sectors;
    }
}

-(void)addNewColor:(UIColor *)color atSector:(NSUInteger)sector{
    if (sector > _sectors) {
        return;
    }
    
    ColorContainer *newColor = [[ColorContainer alloc] init];
    newColor.color = color;
    newColor.sector = sector;
    
    for (int i = 0; i < [_colors count]; i++){
        if ([(ColorContainer *)[_colors objectAtIndex:i] sector] > sector) {
            [_colors insertObject:newColor atIndex:i];
            return;
        }
    }
    
    [_colors addObject:newColor];
}

-(void)removeColorFromIndex:(NSUInteger)index{
    [_colors removeObjectAtIndex:index];
}

-(UIColor *)getColorFromSector:(NSUInteger)sector withStartingColor:(ColorContainer *)stC endingColor:(ColorContainer *)enC{

    CGFloat components1[4],components2[4];
    
    [stC.color getRed:&components1[0] green:&components1[1] blue:&components1[2] alpha:&components1[3]];
    [enC.color getRed:&components2[0] green:&components2[1] blue:&components2[2] alpha:&components2[3]];
    
    CGFloat cmp1Effect, cmp2Effect;
    
    if (enC.sector - stC.sector != 1) {
        cmp1Effect = (CGFloat)((CGFloat)enC.sector - (CGFloat)sector)/(CGFloat)((CGFloat)enC.sector - (CGFloat)stC.sector - (CGFloat)1);
        cmp2Effect = (CGFloat)((CGFloat)sector - (CGFloat)stC.sector)/ (CGFloat)((CGFloat)enC.sector - (CGFloat)stC.sector - (CGFloat)1);
    }else{
        cmp1Effect = cmp2Effect = 1;
    }
    
    CGFloat red = components1[0] * cmp1Effect + components2[0]  * cmp2Effect;
    CGFloat green = components1[1] * cmp1Effect + components2[1]  * cmp2Effect;
    CGFloat blue = components1[2] * cmp1Effect + components2[2]  * cmp2Effect;
    CGFloat alpha = components1[3] * cmp1Effect + components2[3]  * cmp2Effect;
    
    red = [EFGradientGenerator rangesafeColorForColor:red];
    green = [EFGradientGenerator rangesafeColorForColor:green];
    blue = [EFGradientGenerator rangesafeColorForColor:blue];
    alpha = [EFGradientGenerator rangesafeColorForColor:alpha];
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

-(NSUInteger)getSectorFromAngleFromNorth:(CGFloat)angle{
    CGFloat anglePerSect = 360/_sectors;
    
    angle -= 90;
    angle += 360;
    
    for (; angle >= 360; angle -= 360) {
    }
    
    NSUInteger output = angle/anglePerSect;
    
    return output;
}

-(UIColor *)getColorFromAngleFromNorth:(CGFloat)angle{
    
    if ([_colorArray objectAtIndex:[self getSectorFromAngleFromNorth:angle]]) {
        return [_colorArray objectAtIndex:[self getSectorFromAngleFromNorth:angle]];
    }
    
    return nil;
}

-(void)checkColorsArrayIntegrity{
    for (int i = 0; i < [_colors count]; i++){
        if ([(ColorContainer *)[_colors objectAtIndex:i] sector] > _sectors) {
            [_colors removeObjectAtIndex:i];
        }
    }
}

-(void)renderImage{
    CGRect generalRect = CGRectMake(0, 0, _size.width, _size.height);
    
    UIGraphicsBeginImageContextWithOptions(generalRect.size, YES, 0.0);
    [_backgroundColor setFill];
    UIRectFill(generalRect);
    
    int sectors = _sectors;
    float radius = _radius; //MIN(100, 100)/2;
    float angle = 2 * M_PI/sectors;
    UIBezierPath *bezierPath;
    
    ColorContainer *stColor = [[ColorContainer alloc] init];
    ColorContainer *encolor = [[ColorContainer alloc] init];
    
    encolor.sector = 0;
    encolor.color = [(ColorContainer *)[_colors lastObject] color];
    NSUInteger positionOfZero = [(ColorContainer *)[_colors lastObject] sector];
    stColor.sector = positionOfZero + [(ColorContainer *)[_colors objectAtIndex:0] sector];
    stColor.color = [(ColorContainer *)[_colors objectAtIndex:0] color];

    ColorContainer *startAndEndColor = [[ColorContainer alloc] init];
    startAndEndColor.color = [self getColorFromSector:positionOfZero withStartingColor:encolor endingColor:stColor];
    
    
    ColorContainer *aCol, *bCol;
    bCol = [_colors objectAtIndex:0];
    startAndEndColor.sector = 0;
    aCol = startAndEndColor;
    int bColArrayIndex = 0;
    for (int i = 0; i < sectors; i++)
    {
        CGPoint center = CGPointMake(generalRect.size.width/2, generalRect.size.height/2);
        bezierPath = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:i * angle endAngle:(i + 1) * angle clockwise:YES];
        [bezierPath addLineToPoint:center];
        [bezierPath closePath];
        
        if (i > bCol.sector) {
            aCol = bCol;
            if (bColArrayIndex+1 >= [_colors count]) {
                startAndEndColor.sector = _sectors;
                bCol = startAndEndColor;
            }else{
                bColArrayIndex++;
                bCol = [_colors objectAtIndex:bColArrayIndex];
            }
        }
        
        UIColor *color = [self getColorFromSector:i withStartingColor:aCol endingColor:bCol];
        [_colorArray addObject:color];
        [color setFill];
        [color setStroke];
        [bezierPath fill];
        [bezierPath stroke];
    }
    
    UIImage *output =  UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    _renderedImage = output;
}


-(CGFloat)getAngleFromNorthForSector:(NSUInteger)sector{
    CGFloat anglePerSect = 360/_sectors;
    CGFloat angle;
    
    angle = sector * anglePerSect;
    
    angle += 90;
    angle += 360;
    
    for (; angle >= 360; angle -= 360) {
    }
    
    return angle;
}

-(NSArray<NSNumber *> *)getSegmentForUIColor:(UIColor *)color{
    NSMutableArray *outputSet= [[NSMutableArray alloc] init];
    
    for (NSUInteger i = 0; i < [_colors count]; i++){
        ColorContainer *stContainer, *enContainer;
        if (i == 0) {
            stContainer = [_colors lastObject];
            enContainer = [_colors objectAtIndex:0];
        }else{
            stContainer = [_colors objectAtIndex:i-1];
            enContainer = [_colors objectAtIndex:i];
        }
        
        CGFloat componentsSt[4], componentsEn[4], componentsArg[4];
        
        [stContainer.color getRed:&componentsSt[0] green:&componentsSt[1] blue:&componentsSt[2] alpha:&componentsSt[3]];
        [enContainer.color getRed:&componentsEn[0] green:&componentsEn[1] blue:&componentsEn[2] alpha:&componentsEn[3]];
        [color getRed:&componentsArg[0] green:&componentsArg[1] blue:&componentsArg[2] alpha:&componentsArg[3]];
        
        if (
            ( ( componentsSt[0] <= componentsArg[0] && componentsEn[0] >= componentsArg[0] ) || ( componentsSt[0] >= componentsArg[0] && componentsEn[0] <= componentsArg[0] ) ) &&
            ( ( componentsSt[1] <= componentsArg[1] && componentsEn[1] >= componentsArg[1] ) || ( componentsSt[1] >= componentsArg[1] && componentsEn[1] <= componentsArg[1] ) ) &&
            ( ( componentsSt[2] <= componentsArg[2] && componentsEn[2] >= componentsArg[2] ) || ( componentsSt[2] >= componentsArg[2] && componentsEn[2] <= componentsArg[2] ) ) &&
            ( ( componentsSt[3] <= componentsArg[3] && componentsEn[3] >= componentsArg[3] ) || ( componentsSt[3] >= componentsArg[3] && componentsEn[3] <= componentsArg[3] ) )
            ) {
            
            NSLog(@"------->>> --->> ->%lu", i);
            
            CGFloat redStep, greenStep, blueStep, alphaStep;
            CGFloat sectors;
            
            if (stContainer.sector > enContainer.sector) {
                sectors = (enContainer.sector + _sectors) - stContainer.sector;
            }else{
                sectors = enContainer.sector - stContainer.sector;
            }
            
            redStep = fabs(componentsSt[0]-componentsEn[0])/sectors;
            greenStep = fabs(componentsSt[1]-componentsEn[1])/sectors;
            blueStep = fabs(componentsSt[2]-componentsEn[2])/sectors;
            alphaStep = fabs(componentsSt[3]-componentsEn[3])/sectors;
            
            CGFloat number, factor;
            number = factor = 0;
            
            if (componentsSt[0] == componentsEn[0]) {
                
            }else if(componentsSt[0] < componentsEn[0]){
                number += (componentsArg[0] - componentsSt[0])/redStep;
                factor++;
            }else{
                number += (componentsSt[0] - componentsArg[0])/redStep;
                factor++;
            }
            
            if (componentsSt[1] == componentsEn[1]) {
                
            }else if(componentsSt[1] < componentsEn[1]){
                number += (componentsArg[1] - componentsSt[1])/greenStep;
                factor++;
            }else{
                number += (componentsSt[1] - componentsArg[1])/greenStep;
                factor++;
            }
            
            if (componentsSt[2] == componentsEn[2]) {
                
            }else if(componentsSt[2] < componentsEn[2]){
                number += (componentsArg[2] - componentsSt[2])/blueStep;
                factor++;
            }else{
                number += (componentsSt[2] - componentsArg[2])/blueStep;
                factor++;
            }
            
            if (componentsSt[3] == componentsEn[3]) {
                
            }else if(componentsSt[3] < componentsEn[3]){
                number += (componentsArg[3] - componentsSt[3])/alphaStep;
                factor++;
            }else{
                number += (componentsSt[3] - componentsArg[3])/alphaStep;
                factor++;
            }
            
            if (factor == 0) {
                [outputSet addObject:[NSNumber numberWithUnsignedInteger:stContainer.sector]];
            }else{
                number = number/factor;
                [outputSet addObject:[NSNumber numberWithUnsignedInteger:stContainer.sector + (NSUInteger)number]];
            }
        }
    }
    return [outputSet copy];
}

@end
