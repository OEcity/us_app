//
//  StateResponse.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 05/11/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"

@interface StateResponse : NSObject <Response>

@end
