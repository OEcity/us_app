//
//  SYSettingsViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 6/26/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYSettingsViewController.h"
#import "SYDevicesViewController.h"
#import "SYCoreDataManager.h"
#import "SYDataLoader.h"
#import "SYWebSocket.h"

@interface SYSettingsViewController ()

@end

@implementation SYSettingsViewController
@synthesize bottomMenu;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [_configurebutton setTitle:NSLocalizedString(@"configuration", nil) forState:UIControlStateNormal];
    [_ipSettingsButton setTitle:NSLocalizedString(@"elan_settigns",nil) forState:UIControlStateNormal];
    [_favouritesButton setTitle:NSLocalizedString(@"favourite", nil) forState:UIControlStateNormal];
    [_heatingButton setTitle:NSLocalizedString(@"heating", nil) forState:UIControlStateNormal];
    
    [_scenesButton setTitle:NSLocalizedString(@"scenes", nil) forState:UIControlStateNormal];
    [_camerasButton setTitle:NSLocalizedString(@"cameras", nil) forState:UIControlStateNormal];
    [_bottomButtonLeft setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_bottomButtonRight setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    [_othersButton setTitle:NSLocalizedString(@"others",nil) forState:UIControlStateNormal];
    
    
    [[SYWebSocket sharedInstance] reconnect];
//    Elan *myElan = [[SYCoreDataManager sharedInstance]getCurrentElan];
//    NSLog(@"%@", myElan.type);
    
   
//   if([[SYCoreDataManager sharedInstance]getCurrentElan].update.boolValue == YES){
//        _updateBadge.hidden = false;
//   } else {
//            _updateBadge.hidden = true;
//   }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)backToMainMenu:(id)sender{

    if ([self.presentingViewController isMemberOfClass:[SYDevicesViewController class]]){
//        [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
        [self performSegueWithIdentifier:@"rooms" sender:self];
    } else{
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (IBAction)heatingButtonPressed:(id)sender {
    [self performSegueWithIdentifier:@"heating" sender:self];
}

-(IBAction)IPConfigureButtonPressed:(id)sender{
    [self performSegueWithIdentifier:@"ipconfig" sender:self];
    
}

- (IBAction)showManualTap:(id)sender {
    [self performSegueWithIdentifier:@"showManual" sender:self];
}

-(IBAction)favouritesButtonPressed:(id)sender{
    [self performSegueWithIdentifier:@"favourites" sender:self];
    
}
-(IBAction)scenesButtonPressed:(id)sender{
    [self performSegueWithIdentifier:@"scenes" sender:self];
    
}

-(IBAction)configureButtonPressed:(id)sender{
    [self performSegueWithIdentifier:@"configure" sender:self];
    
}

-(IBAction)cameraButtonPressed:(id)sender{
    [self performSegueWithIdentifier:@"cameras" sender:self];
    
}
- (IBAction)otherSettingsPressed:(id)sender {
    [self performSegueWithIdentifier:@"showOtherSettings" sender:self];
}

- (NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}

@end
