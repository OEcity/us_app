//
//  RoomsRootViewController.h
//  Click Smart
//
//  Created by Tom Odler on 21.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device.h"

@interface RoomsRootViewController : UIViewController<UIPageViewControllerDataSource, UIPageViewControllerDelegate>
@property (strong, nonatomic) UIPageViewController *pageViewController;
@property (nonatomic, retain) NSArray* rooms;
@property (nonatomic, retain) Device *segueDevice;
@property NSUInteger indexForOpen;

@end
