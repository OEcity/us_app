//
//  SceneActionsViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/2/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYSceneActionsViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "Device.h"
#import "Device+Action.h"
#import "Action.h"
#import "URLConnector.h"
#import "Util.h"
#import "AppDelegate.h"
#import "DeterminableActionCell.h"
#import "SceneLoader.h"
#import "SYAPIManager.h"

@interface SYSceneActionsViewController ()
@property (nonatomic, strong) NSFetchedResultsController* sceneActions;

@end
#warning TODO refactor Actions necessary!
@implementation SYSceneActionsViewController

@synthesize swipeGesture;
@synthesize carousel;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    //it's a good idea to set these to nil here to avoid
    //sending messages to a deallocated viewcontroller
    carousel.delegate = nil;
    carousel.dataSource = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.slider setMinimumTrackImage:[UIImage imageNamed:@"slider_off-08"] forState:UIControlStateNormal];
    [self.slider setMaximumTrackImage:[UIImage imageNamed:@"slider_on-08"] forState:UIControlStateNormal];
    [self.slider setThumbImage:[UIImage imageNamed:@"slider-07"] forState:UIControlStateNormal];
    
    /*[[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(sceneSyncComplete:)
     name:@"sceneLoaderComplete"
     object:nil];
     */
    _cellsForDevice = [[NSMutableDictionary alloc] init];
    [_addActionButton setBackgroundImage:[UIImage imageNamed:@"scen_add_function_on"] forState:UIControlStateHighlighted];
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    [self loadSceneActionsFromDB];
    carousel.delegate = self;
    [carousel reloadData];
    [carousel setType:iCarouselTypeLinear];
    [carousel setVertical:NO];
    [carousel setCenterItemWhenSelected:YES];
}

-(void)loadSceneActionsFromDB{
    _selectedActions = [[NSMutableDictionary alloc] init];
    for (Device * device in _selectedDevices){
        NSString * deviceName = device.deviceID;
        NSMutableArray * deviceArray = [_selectedActions objectForKey:deviceName];
        deviceArray = [[NSMutableArray alloc] init];
        if(!device)
            continue;
        
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.scene.sceneID == %@", _scene.sceneID];
        [deviceArray addObjectsFromArray:[[device.inSceneActions filteredSetUsingPredicate:predicate] allObjects]];
        [_selectedActions setObject:deviceArray forKey:deviceName];
    }
}
-(void)showSceneActions{
    for (Device * device in _selectedDevices){
        if (device==nil) return;
        NSString * deviceName = device.deviceID;
        NSMutableArray* deviceArray = [_selectedActions objectForKey:deviceName];
        NSMutableArray* cell = [[_cellsForDevice objectForKey:deviceName] mutableCopy];//[[NSMutableArray alloc] init];
        if (!cell){
            cell = [[NSMutableArray alloc] init];
        }
        BOOL hasRGB = NO;
        for (SceneAction * res in deviceArray){
            if ([res.action.name isEqualToString:@"red"] || [res.action.name isEqualToString:@"green"] || [res.action.name isEqualToString:@"blue"] ){
                if (!hasRGB){
                    hasRGB = YES;
                    NSArray* actions = [deviceArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"action.name == 'red' OR action.name == 'green' OR action.name == 'blue'"]];
                    DeterminableActionCell * determinableActionCell  = [[DeterminableActionCell alloc] initRGB:actions WithOwner:self];
                    if (![cell containsObject:determinableActionCell]){
                        [cell addObject:determinableActionCell];
                    }
                }
            } else {
                DeterminableActionCell * determinableActionCell  = [[DeterminableActionCell alloc] initWithAction:res owner:self];
                if (![cell containsObject:determinableActionCell]){
                    [cell addObject:determinableActionCell];
                }
            }
        }
        [_cellsForDevice setObject:cell forKey:deviceName];
    }
    _cells = [_cellsForDevice objectForKey:_selectedDevice.deviceID];
    [_tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //    [self returnViews];
    if (_predefinedActions!=nil){
        [self showSceneActions];
        _cells = [_cellsForDevice valueForKey:self.selectedDevice.deviceID];
    }
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [_actionSetLabel setText:NSLocalizedString(@"scenes", nil)];
    [_bottomRightButton setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
    [_bottomLeftButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_addFunctionButton setTitle:NSLocalizedString(@"addFunction", nil) forState:UIControlStateNormal] ;
    
    int index = self.selectedDevices.count/2;
    self.selectedDevice = [self.selectedDevices objectAtIndex:index];
    [carousel setCurrentItemIndex:index];
    [carousel scrollToItemAtIndex:index animated:NO];
    //    [self initFetchedResultsControllerForDevice:self.selectedDevice];
    //    if (self.selectedActions==nil) self.selectedActions = [[NSMutableDictionary alloc] init];
    
}
//
//#pragma mark - Init
//
//- (void)initFetchedResultsControllerForDevice:(Device*) device{
//
//    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
//
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"SceneAction"];
//    // Configure the request's entity, and optionally its predicate.
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"action" ascending:YES];
//    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
//    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"self.scene.sceneID == %@ AND self.device.deviceID == %@", _scene.sceneID, device.deviceID]];
//    [fetchRequest setSortDescriptors:sortDescriptors];
//
//    _sceneActions = [[NSFetchedResultsController alloc]
//              initWithFetchRequest:fetchRequest
//              managedObjectContext:context
//              sectionNameKeyPath:nil
//              cacheName:nil];
//    _sceneActions.delegate = self;
//    NSError *error;
//
//    if (![_sceneActions performFetch:&error]) {
//        NSLog(@"error fetching SceneActions: %@",[error description]);
//    }
//    [_tableView reloadData];
//}
//
#pragma mark - NSFetchedResultsControllerDelegate methods
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView reloadData];
}

- (void)configureCell:(DeterminableActionCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    
}
# pragma mark tableView methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    //NSArray *actions = [self.selectedActions objectForKey:self.selectedDevice.name];
    //    return [actions count];
    
    return [_cells count];
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    DeterminableActionCell * cell = [_cells objectAtIndex:indexPath.row];
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DeterminableActionCell *cell = [_cells objectAtIndex:indexPath.row];
    CGPoint origin ;
    if (cell.colorPicker!=nil){
        NSLog(@"poloha x:%f, y:%f", cell.colorPicker.center.x, cell.colorPicker.center.y);
        origin = cell.colorPicker.center;
    }
    
    [tableView beginUpdates];
    if (cell.height == 72.0f){
        if (cell.expandedHeight!=0.0f){
            cell.height = cell.expandedHeight;
            [cell makeTimeVisible];
            [cell.secondaryArrow setImage:[UIImage imageNamed:@"scene_arrow_expanded" ]];
        }
    }else{
        cell.height = 72.0f;
        [cell.secondaryArrow setImage:[UIImage imageNamed:@"scene_arrow" ]];
        [cell makeTimeInvisibleWithAlert:cell.type == kTime];
    }
    [tableView endUpdates];
    
    if (cell.colorPicker!=nil){
        NSLog(@"poloha x:%f, y:%f", cell.colorPicker.center.x, cell.colorPicker.center.y);
        [cell.colorPicker setCenter:origin];
    }
    if (_tableView.editing)
        _tableView.editing = NO;
    
    [cell setSelected:NO];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    DeterminableActionCell * cell =[_cells objectAtIndex:indexPath.row];
    return cell.height;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    DeterminableActionCell * cell = [_cells objectAtIndex:indexPath.row];
    
    if (_tableView.editing || cell.expanded){
        return UITableViewCellEditingStyleNone;
    }
    else{
        return UITableViewCellEditingStyleDelete;
    }
    
    //    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}


-(void)tableView:(UITableView*)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    UIView * view1 = [cell viewWithTag:2];
    
    [UIView animateWithDuration:0.2f animations:^{
        CGRect theFrame = view1.frame;
        theFrame.size.width -= 50;
        view1.frame = theFrame;
        [view1 layoutIfNeeded];
    }];
    [UIView commitAnimations];
    for (DeterminableActionCell * cell in _cells){
        [_tableView beginUpdates];
        cell.height = 72.0f;
        [cell makeTimeInvisibleWithAlert:NO];
        [_tableView endUpdates];
    }
    
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        DeterminableActionCell * cell = [_cells objectAtIndex:indexPath.row];
        NSMutableArray *actions = [_selectedActions objectForKey:_selectedDevice.deviceID];
        NSPredicate* pred =[cell.itemName isEqualToString:@"RGB"]?
        [NSPredicate predicateWithFormat:@"action.name != 'red' AND action.name != 'green' AND action.name != 'blue' AND scene.sceneID == %@", _scene.sceneID]:
        [NSPredicate predicateWithFormat:@"action.name != %@ AND scene.sceneID == %@", cell.itemName, _scene.sceneID];
        NSArray *filteredActions = [actions filteredArrayUsingPredicate:pred];
        [_selectedActions setObject:filteredActions forKey:_selectedDevice.deviceID];
        [_cells removeObjectAtIndex:indexPath.row];
        //        NSMutableArray *actions = [self.selectedActions objectForKey:self.selectedDevice.deviceID];
        //        [actions removeObjectAtIndex:indexPath.row];
        //        [_selectedActions setObject:actions forKey:_selectedDevice.deviceID];
        [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
        
    }
}


- (BOOL)tableView:(UITableView *)tableview canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    NSMutableArray* actions = (NSMutableArray*)[self.selectedActions objectForKey:self.selectedDevice.deviceID];
    id cell = [_cells objectAtIndex:sourceIndexPath.row];
    id thing = [actions objectAtIndex:sourceIndexPath.row];
    [actions removeObjectAtIndex:sourceIndexPath.row];
    [_cells removeObjectAtIndex:sourceIndexPath.row];
    [actions insertObject:thing atIndex:destinationIndexPath.row];
    [_cells insertObject:cell atIndex:destinationIndexPath.row];
    
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0)
    {
        //User clicked on No button
    }
    else if(buttonIndex == 1)
    {
        AudioServicesPlaySystemSound (1104);
        
        [_loaderDialog showWithLabel:NSLocalizedString(@"addSceneDialog", nil)];
        
        NSDictionary* json = [self storeToJSON];
        
        [[SYAPIManager sharedInstance] postSceneWithID:_scene.sceneID withDict:json success:^(AFHTTPRequestOperation *operation, id response) {
            [_loaderDialog hide];
            [self.sourceController dismissViewControllerAnimated:NO completion:nil];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [_loaderDialog hide];
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"cannotAddScene", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }];
    }
}

-(IBAction)save:(id)sender{
    
    //AudioServicesPlaySystemSound (1104);
    
    [_loaderDialog showWithLabel:NSLocalizedString(@"addSceneDialog", nil)];
    
    NSDictionary* json = [self storeToJSON];
    
    [[SYAPIManager sharedInstance] postSceneWithID:_scene.sceneID withDict:json success:^(AFHTTPRequestOperation *operation, id response) {
        [_loaderDialog hide];
        [self.sourceController dismissViewControllerAnimated:NO completion:nil];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [_loaderDialog hide];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"cannotAddScene", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }];

}

-(void)showNoActions{
    [_loaderDialog hide];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"", nil) message:NSLocalizedString(@"sceneActionNotSet", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


-(IBAction)back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

/*
 -(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
 //[tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 */


-(NSDictionary*) storeToJSON{
    NSMutableDictionary* data = [NSMutableDictionary new];
    NSMutableArray* actions = [NSMutableArray new];
    
    [data setObject:self.sceneLabel forKey:@"label"];
    BOOL failed= NO;
    for (Device* device in self.selectedDevices){
        NSArray *cells = [self.cellsForDevice objectForKey:device.deviceID];
        if ([cells count]==0) failed=YES;
        BOOL hasRGB=[device hasActionWithName:@"red"] && [device hasActionWithName:@"green"] && [device hasActionWithName:@"blue"];
        if (hasRGB){
            NSMutableDictionary *dict = [NSMutableDictionary new];
            for (DeterminableActionCell * cell in cells){
                [dict addEntriesFromDictionary:[cell jsonRepresentation]];
            }
            [actions addObject:[NSDictionary dictionaryWithObject:dict forKey:device.deviceID]];
        }else{
            for (DeterminableActionCell * cell in cells){
                [actions addObject:[NSDictionary dictionaryWithObject:[cell jsonRepresentation] forKey:device.deviceID]];
            }
        }
    }
    [data setObject:actions forKey:@"actions"];
    if (failed) return nil;
    if (self.scene == nil){
        return data;
    }
    [data setValue:_scene.sceneID forKey:@"id"];
    return data;
}

-(void) connectionResponseFailed:(int)code{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:[NSString stringWithFormat:@"cannotAddScene"]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
    
}

- (NSUInteger)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
}

- (IBAction)addFunction:(id)sender {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(actionSelectionDone:) name:@"actionsSelected"
                                               object:nil];
    NSSet * arr = _selectedDevice.actions;
    //    NSArray * selected = [[_selectedDevice.inSceneActions filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"scene.sceneID = %@", _scene.sceneID]] allObjects];
    
    //[_selectedActions objectForKey:_selectedDevice.deviceID];
    
    //    for (SceneAction * sel in selected){
    //        if ([sel.action.name isEqualToString:@"red"] || [sel.action.name isEqualToString:@"green"] || [sel.action.name isEqualToString:@"blue"]){
    //            if ([selected count]+2>=[[arr allObjects] count]) return;
    //        }
    //    }
    NSMutableArray * selected = [_selectedActions objectForKey:_selectedDevice.deviceID];
    //    for (NSString * key in _selectedActions){
    //        SceneAction * action = [_selectedActions objectForKey:key];
    //        if ([action.scene.sceneID isEqualToString:_scene.sceneID] && [action.device.deviceID isEqualToString:_selectedDevice.deviceID]){
    //            [selected addObject:action];
    //        }
    //    }
    
    if ([selected count]>=[[arr allObjects] count]) return;
    self.actionSelectViewController =  [[ActionSelectViewController alloc] initWithNibName:@"ActionSelectViewController" bundle:nil andArray:arr selected:selected withDevice:_selectedDevice withScene:_scene];
    //    [self.actionSelectViewController.view setTag:10];
    [self.view addSubview:self.actionSelectViewController.view];
    
}


-(void)actionSelectionDone:(NSNotification*)not{
    NSMutableArray* selected = [not.userInfo objectForKey:@"selected"];
    if (_selectedDevice == nil|| _selectedDevice.deviceID==nil){
        return;
    }
    [_selectedActions setObject:selected forKey:_selectedDevice.deviceID];
    [self showSceneActions];
    
    //    _cells = [[NSMutableArray alloc] init];
    //    for (SceneAction * res in selected){
    //        //        SceneAction * action = [[res allValues] objectAtIndex:0];
    ////        [_cells addObject:[[DeterminableActionCell alloc] initWithAction:res owner:self]];
    //        NSMutableArray * old =[[_selectedActions objectForKey:_selectedDevice.deviceID] mutableCopy];
    //        [old addObject:res];
    //    }
    //    [_cellsForDevice setObject:_cells forKey:_selectedDevice.deviceID];
    //    [_tableView reloadData];
}


-(void)refreshArray{
    _cells = [_cellsForDevice objectForKey:_selectedDevice.deviceID];
    [_tableView reloadData];
}




-(void)longPressEdit:(UILongPressGestureRecognizer *)gestureRecognizer{
    
    
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        
        if (_tableView.editing==NO){
            
            _tableView.editing = YES;
            /*
             for (DeterminableActionCell * cell in _cells){
             [_tableView beginUpdates];
             //[cell setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"pruh_off"]]];
             [_tableView endUpdates];
             }
             
             */
        }else{
            _tableView.editing = NO;
            for (DeterminableActionCell * cell in _cells){
                [_tableView beginUpdates];
                [cell setBackgroundColor:[UIColor clearColor]];
                [_tableView endUpdates];
            }
            
        }
    }else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        for (DeterminableActionCell * cell in _cells){
            [_tableView beginUpdates];
            cell.height = 72.0f;
            [cell makeTimeInvisibleWithAlert:NO];
            [_tableView endUpdates];
        }
    }
}

#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [self.selectedDevices count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    
    
    UIView *tile;
    //    CGFloat spaceSize = 10;
    //    CGFloat startPos = self.view.frame.size.width/2 - (([self.selectedDevices count]-1)* (self.deviceBackgroundTemplate.frame.size.width+spaceSize))/2;
    //    self.deviceViews = [[NSMutableArray alloc] init];
    //    int inkr=0;
    Device * device = [self.selectedDevices objectAtIndex:index];
    tile = [[UIView alloc] initWithFrame:self.deviceBackgroundTemplate.frame];
    [tile setBackgroundColor:[UIColor whiteColor]];
    UIImageView *backImage = [[UIImageView alloc] initWithFrame:self.deviceBackgroundTemplate.frame];
    backImage.contentMode = UIViewContentModeScaleToFill;
    [backImage setCenter:CGPointMake(backImage.frame.size.width/2, backImage.frame.size.height/2)];
    
    if (self.selectedDevice==device)
        [backImage setImage:[UIImage imageNamed:@"dlazdice_on.png"]];
    else
        [backImage setImage:[UIImage imageNamed:@"dlazdice_off.png"]];
    
    backImage.tag=1;
    UILabel * nameLabel = [[UILabel alloc] initWithFrame:self.deviceLabelTemplate.frame];
    [nameLabel setFont:[self.deviceLabelTemplate font]];
    [nameLabel setTextColor:[self.deviceLabelTemplate textColor]];
    [nameLabel setTextAlignment:NSTextAlignmentCenter];
    [nameLabel setAdjustsFontSizeToFitWidth:YES];
    [nameLabel setBackgroundColor:[UIColor clearColor]];
    [nameLabel setText:device.label];
    UIImageView * icon = [[UIImageView alloc] initWithFrame:self.deviceIconTemplate.frame];
    if (self.selectedDevice==device) {
        [icon setImage:[device imageOn]];
    }else{
        [icon setImage:[device imageOff]];
    }
    icon.contentMode = UIViewContentModeScaleAspectFit;
    //    icon.tag=2;
    //    [tile setCenter:CGPointMake(startPos+inkr*onespace, self.deviceBackgroundTemplate.center.y)];
    [tile addSubview:backImage];
    [tile addSubview:icon];
    [tile addSubview:nameLabel];
    [tile setBackgroundColor:[UIColor clearColor]];
    [tile setBounds:CGRectMake(-5, 0, tile.frame.size.width+5, tile.frame.size.height)];
    //        tile.tag = 100+inkr;
    //        inkr++;
    //        [[self.view viewWithTag:1] addSubview:tile];
    //        UITapGestureRecognizer *oneFingerTwoTaps =
    //        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapRecognize:)];
    
    // Set required taps and number of touches
    //        [oneFingerTwoTaps setNumberOfTapsRequired:1];
    //        [oneFingerTwoTaps setNumberOfTouchesRequired:1];
    
    // Add the gesture to the view
    //        [tile addGestureRecognizer:oneFingerTwoTaps];
    //        [self.deviceViews addObject:tile];
    return tile;
    //
    //    UILabel *label = nil;
    //
    //    //create new view if no view is available for recycling
    //    if (view == nil)
    //    {
    //        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
    //        ((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
    //        view.contentMode = UIViewContentModeCenter;
    //        label = [[UILabel alloc] initWithFrame:view.bounds];
    //        label.backgroundColor = [UIColor clearColor];
    //        label.textAlignment = NSTextAlignmentCenter;
    //        label.font = [label.font fontWithSize:50];
    //        label.tag = 1;
    //        [view addSubview:label];
    //    }
    //    else
    //    {
    //        //get a reference to the label in the recycled view
    //        label = (UILabel *)[view viewWithTag:1];
    //    }
    //
    //    //set item label
    //    //remember to always set any properties of your carousel item
    //    //views outside of the `if (view == nil) {...}` check otherwise
    //    //you'll get weird issues with carousel item content appearing
    //    //in the wrong place in the carousel
    //    label.text = [items[index] stringValue];
    //
    //    return view;
}

- (void) selectItemAtIndex:(NSInteger)index animated:(BOOL)animated{
    int oldIndex = [self.selectedDevices indexOfObject:self.selectedDevice];
    self.selectedDevice = [self.selectedDevices objectAtIndex:index];
    [self.carousel reloadItemAtIndex:oldIndex animated:animated];
    [self.carousel reloadItemAtIndex:index animated:animated];
    [self showSceneActions];
}

-(void) carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    [self selectItemAtIndex:index animated:NO];
}


- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
    [self selectItemAtIndex:self.carousel.currentItemIndex animated:NO];
}


@end
