//
//  NSDictionary+ParseStrings.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 02/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (ParseStrings)


-(id)objectForKeyNSNumber:(id)aKey;

@end
