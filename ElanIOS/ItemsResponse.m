//
//  ItemsResponse.m
//  O2archiv
//
//  Created by Vratislav Zima on 1/16/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "ItemsResponse.h"
#import "SBJson.h"

#import "Room.h"

@implementation ItemsResponse


-(NSObject*)parseResponse:(NSData *)inputData identifier:(NSString *)identifier{
    NSMutableArray * itemsArray = [[NSMutableArray alloc] init];
    
    SBJsonParser *parser2 = [[SBJsonParser alloc] init];
    NSString *inputString = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];

    NSDictionary* jsonObjects = [parser2 objectWithString:inputString];
    
    
    for(NSString* dict in jsonObjects)
    {
        

        
        Room *room = [[Room alloc] init];

        @try {
            room.roomID = dict;
            NSDictionary* newDict = [jsonObjects valueForKey:dict];
            NSString * url = [newDict valueForKey:@"url"];
            room.url = url;
            
        }
        @catch (NSException *exception) {
            //handle this exception !!

        }@finally {
            [itemsArray addObject:room];
            NSLog(@"room name %@", room.roomID);
        }

    }
    return itemsArray;

}
@end
