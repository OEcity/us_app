//
//  SYHeatingAreaContainerDetailViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/17/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "SYHeatingAreaContainerDetailViewController.h"
#import "SYCoreDataManager.h"
#import "Device.h"
#import "SYAPIManager.h"
#import "AppDelegate.h"
#import "Device+State.h"
@interface SYHeatingAreaContainerDetailViewController ()
@end
BOOL tableViewExpanded = NO;

@implementation SYHeatingAreaContainerDetailViewController
int pickerViewMode=1;
#define TEMPERATURE_SENSORS_MODE 1
#define TEMPERATURE_SCHEDULE_MODE 2
#define TEMPERATURE_SENSORS_OUT_MODE 3


- (void)viewDidLoad {
    [super viewDidLoad];
    tableViewExpanded = NO;
    _selectedSettings = [[NSMutableDictionary alloc] init];
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    
    
    //set translations for titles
    [_lNameTitle setText:NSLocalizedString(@"heat_cool_area_add_name", nil)];
    [_lAddTemperatureSensorTitle setText:NSLocalizedString(@"heat_cool_area_add_temp_sensor", nil)];
    
    [_lTemperatureOffsetTitle setText:NSLocalizedString(@"heat_cool_area_add_temp_offset", nil)];
    [_lTemperatureScheduleTitle setText:NSLocalizedString(@"heat_cool_area_add_temp_schedule", nil)];
    [_lAddHeatingTitle setText:NSLocalizedString(@"heat_cool_area_add_heating", nil)];
    
    [_btnHeatingArea setTitle:NSLocalizedString(@"heating_area_settings_title", nil) forState:UIControlStateNormal];
    [_btnSave setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    
    [[SYAPIManager sharedInstance] getTemperatureSchedulesWithSuccess:^(AFHTTPRequestOperation * operation, id response){
        _temperatureSchedules = [[NSMutableArray alloc] init];
        for (NSString * scheduleName in (NSArray *)response){
            [[SYAPIManager sharedInstance] getTemperatureScheduleDetailsWithName:scheduleName success:
             ^(AFHTTPRequestOperation *request, id response){
                 [_loaderDialog hide];
                 [_temperatureSchedules addObject:(NSDictionary*)response];
                 NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"label" ascending:YES selector:@selector(localizedCaseInsensitiveCompare:)];
                 _temperatureSchedules=[[NSMutableArray alloc] initWithArray:[_temperatureSchedules sortedArrayUsingDescriptors:@[sort]]];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [_pickerView reloadAllComponents];
                     if (_device!=nil)[_lTemperatureSchedule setText:[self resolveNameForSchedule:_device.schedule]];
                 });
                 
             }
                                                                         failure:^(AFHTTPRequestOperation * request, NSError * error){
                                                                             [_loaderDialog hide];
                                                                         }];}
    } failure:^(AFHTTPRequestOperation * operation, NSError * error){
        [_loaderDialog hide];
        
    }];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"SUBQUERY(self.states,$state,($state.name == 'temperature' OR $state.name == 'temperature IN' OR $state.name == 'temperature OUT') AND $state.value != nil).@count > 0 AND SUBQUERY(self.states,$state,$state.name == 'mode').@count=0"];
    
    _temperatureSensors = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Device" withPredicate:predicate];
    NSPredicate * predicateHCA = [NSPredicate predicateWithFormat:@"(SUBQUERY(self.actions,$action,$action.name MATCHES 'delayed off: set time' OR $action.name MATCHES 'requested temperature' OR $action.name MATCHES 'safe on').@count >0 AND (self.deviceCooling.deviceID == nil OR self.deviceCooling.deviceID == %@) AND (self.deviceHeating.deviceID == nil OR self.deviceHeating.deviceID == %@))", _device.deviceID,  _device.deviceID];
    
    NSSortDescriptor* sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    
    
    _availableHeaters = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Device"
                                                                  withPredicate:predicateHCA
                                                                 sortDescriptor:sortDescriptor
                                                                      inContext:[[SYCoreDataManager sharedInstance] managedObjectContext]];
    if (_device !=nil){
        [_tFName setText:_device.label];
        _deviceId  = _device.deviceID;
        _temperatureSchedule = _device.schedule;
        [_lTemperatureOfset setText:[NSString stringWithFormat:@"%0.1f",[_device.temperatureOffset floatValue]]];
        [_lTemperatureSensor setText:[self resolveNameForSensor:_device]];
        _temperatureSensor = _device.temperatureSensor;
        if (_device.temperatureSensor)[_selectedSettings setObject:@"temperature" forKey:_device.temperatureSensor.deviceID];
        [self setupHeatingDevices];
    }
    _pickerViewController = [[PickerViewController alloc] initWithNibName:@"PickerViewController" bundle:nil];
    _pickerViewController.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */





-(NSString*)resolveNameForSchedule:(NSString*)id{
    
    for (NSDictionary* schedule in _temperatureSchedules){
        if ([[schedule objectForKey:@"id"] isEqualToString:id]){
            return [schedule objectForKey:@"label"];
        }
    }
    return id;
    
}
-(NSString*)resolveNameForSensor:(HeatCoolArea*) device{
    if (device.temperatureSensor){
        return [NSString stringWithFormat:@"%@ - %@",device.temperatureSensor.label, NSLocalizedString(@"heat_cool_temperature_sensor", nil) ];
    }
    return  nil;
}


- (IBAction)resignAll:(id)sender {
    [_tFName resignFirstResponder];
    
}

#pragma mark Fields click events

- (IBAction)addTemperatureSensorClicked:(id)sender {
    
    pickerViewMode = TEMPERATURE_SENSORS_MODE;
    [_pickerView reloadAllComponents];
    if (tableViewExpanded){
        [self addHeatingButtonPressed:_addHeatingButton];
    }
    
    [[[[self.view superview] superview] superview] addSubview:_pickerViewController.view ];
}

- (IBAction)temperatureOffsetClicked:(id)sender {
    _temperatureOffset = [[TemperatureOffsetViewController alloc] initWithNibName:@"TemperatureOffsetViewController" bundle:nil];
    AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (delegate.limitsSettings!=nil){
        NSDictionary * offset =[[delegate.limitsSettings objectForKey:@"heatcoolarea"] objectForKey:@"temperature offset"];
        [_temperatureOffset setBoundsWithMax:[[offset objectForKey:@"max"] floatValue] min:[[offset objectForKey:@"min"] floatValue]];
        _temperatureOffset.realTemperature = [_lTemperatureOfset.text floatValue];
        [_temperatureOffset displayNumber];
    }
    
    _temperatureOffset.delegate = self;
    _temperatureOffset.view.tag= 100;
    UIView * superView = [[[self.view superview] superview] superview];
    [_temperatureOffset.view setFrame:superView.frame];
    [[_temperatureOffset.view viewWithTag:1] setCenter:_temperatureOffset.view.center];
    if ([superView viewWithTag:100]==nil){
        [UIView transitionWithView:superView
                          duration:0.3
                           options:UIViewAnimationOptionTransitionCrossDissolve //any animation
                        animations:^ { [superView addSubview:_temperatureOffset.view]; }
                        completion:nil];
        
    }
    
}

- (IBAction)addTemperatureScheduleClicked:(id)sender {
    
    pickerViewMode = TEMPERATURE_SCHEDULE_MODE;
    [_pickerView reloadAllComponents];
    [[[[self.view superview] superview] superview] addSubview:_pickerViewController.view ];
    
}
#pragma mark -

#pragma Picker View Data Source methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (pickerViewMode) {
        case TEMPERATURE_SENSORS_MODE:
            return [_temperatureSensors count];
            break;
        case TEMPERATURE_SCHEDULE_MODE:
            return [_temperatureSchedules count];
        case TEMPERATURE_SENSORS_OUT_MODE:
            return 2;
        default:
            
            break;
    }
    return 0;
}

#pragma mark -
#pragma Picker View Delegate methods

//Set the dimensions of the picker view.
/*
 - (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
 {
 return 40.0f;
 }
 */

/*
 - (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
 {
 return 200.f;
 }
 */

//The methods in this group are marked @optional. However, to use a picker view, you must implement either the pickerView:titleForRow:forComponent: or the pickerView:viewForRow:forComponent:reusingView: method to provide the content of component rows.
//If both pickerView:titleForRow:forComponent: and pickerView:attributedTitleForRow:forComponent: are implemented, attributed title is favored.

// Return a string representing the title for the given row.
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //return [dataSource objectAtIndex:row];
    switch (pickerViewMode) {
        case TEMPERATURE_SENSORS_MODE:{
            Device * device = [_temperatureSensors objectAtIndex:row];
            return device.label;
            break;
        }
        case TEMPERATURE_SCHEDULE_MODE:{
            return [[_temperatureSchedules objectAtIndex:row] objectForKey:@"label"];
            break;
        }
        case TEMPERATURE_SENSORS_OUT_MODE:{
            switch (row) {
                case 0:
                    return NSLocalizedString(@"heat_cool_outdoor_temperature_sensor", nil);
                    break;
                case 1:
                    return NSLocalizedString(@"heat_cool_indoor_temperature_sensor", nil);
                    break;
                default:
                    break;
            }
            break;
        }
            
        default:
            break;
    }
    
    return @"Nil";
}



- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    _pickerViewController.row = row;
    _pickerViewController.component= component;
    
    
}
-(void)DoneButtonTappedForRow:(NSInteger)row inComponent:(NSInteger)component{
    switch (pickerViewMode) {
        case TEMPERATURE_SENSORS_MODE:{
            Device * device = [_temperatureSensors objectAtIndex:row];
            [_lTemperatureSensor setText:device.label];
            [_selectedSettings removeAllObjects];
            [_selectedSettings setObject:@"temperature" forKey:device.deviceID];
            _temperatureSensor = device;
            
            if ([device getStateValueForStateName:@"temperature OUT"] !=nil){
                pickerViewMode = TEMPERATURE_SENSORS_OUT_MODE;
                [[[[self.view superview] superview] superview] addSubview:_pickerViewController.view ];
                
            }
            
            break;
        }
        case TEMPERATURE_SCHEDULE_MODE:{
            [_lTemperatureSchedule setText:[[_temperatureSchedules objectAtIndex:row] objectForKey:@"label"]];
            _temperatureSchedule =[[_temperatureSchedules objectAtIndex:row] objectForKey:@"id"];
            _pickerView.hidden=YES;
            
            break;
        }
        case TEMPERATURE_SENSORS_OUT_MODE:{
            NSString * value = nil;
            NSString * settingsValue = nil;
            if (row==0){
                value =NSLocalizedString(@"heat_cool_outdoor_temperature_sensor", nil);
                settingsValue = @"temperature OUT";
            }else if (row==1){
                value =NSLocalizedString(@"heat_cool_indoor_temperature_sensor", nil);
                settingsValue = @"temperature IN";
                
            }
            [_lTemperatureSensor setText:[NSString stringWithFormat:@"%@ - %@", _lTemperatureSensor.text, value]];
            NSString * key = [[_selectedSettings allKeys] objectAtIndex:0];
            [_selectedSettings setObject:settingsValue forKey:key];
            _pickerView.hidden=YES;
            
            break;
        }
        default:
            
            break;
    }
    
}


#pragma TextField delegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}



-(void)temperatureSet:(float)temperature data:(NSDictionary *)data{
    _realTemperature = temperature;
    [_lTemperatureOfset setText:[NSString stringWithFormat:@"%0.1f", _realTemperature]];
}


#pragma mark Add Heating section

- (IBAction)addHeatingButtonPressed:(id)sender {
    
    
    int size = (int)[_availableHeaters count] * 50 + 30;
    if (!tableViewExpanded){
        [self generateHeaderGUI];
        [self generateBody];
        _scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width , _scrollView.contentSize.height+size);
        tableViewExpanded = YES;
        CGRect frame = CGRectMake(0, _addHeatingButton.frame.origin.y, 320,_scrollView.frame.size.height);
        [self.scrollView scrollRectToVisible:frame animated:YES];
        
        
    }else{
        
        _scrollView.contentSize = CGSizeMake(_scrollView.contentSize.width , _scrollView.contentSize.height-size);
        
        tableViewExpanded = NO;
        for (id subview in _scrollView.subviews)
        {
            if ([(UIView*)subview tag]==99 ){
                [(UIView*)subview removeFromSuperview];
            }
            
        }
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_availableHeaters count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* cellIdentifier = @"HeatingCell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    cell.contentView.userInteractionEnabled= NO;
    UIImageView * iVSeparator1 = [[UIImageView alloc] initWithFrame:CGRectMake(107, 10, 1, 30)];
    [iVSeparator1 setBackgroundColor:[UIColor whiteColor]];
    [cell addSubview:iVSeparator1];
    
    UIImageView * iVSeparator2 = [[UIImageView alloc] initWithFrame:CGRectMake(214, 10, 1, 30)];
    [iVSeparator2 setBackgroundColor:[UIColor whiteColor]];
    [cell addSubview:iVSeparator2];
    
    
    UIImageView * iVSeparator3 = [[UIImageView alloc] initWithFrame:CGRectMake(10, 49, 300, 1)];
    [iVSeparator3 setBackgroundColor:[UIColor whiteColor]];
    [cell addSubview:iVSeparator3];
    
    Device* device = [_availableHeaters objectAtIndex:indexPath.row];
    
    UILabel * name = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, 93, 50)];
    [name setText:device.label];
    
    [name setTextColor:[UIColor blackColor]];
    
    [name setTextAlignment:NSTextAlignmentCenter];
    
    name.adjustsFontSizeToFitWidth=YES;
    name.minimumScaleFactor=0.5;
    
    UIButton * add = [[UIButton alloc] initWithFrame:CGRectMake(133, 5, 40, 40)];
    [add setBackgroundImage:[UIImage imageNamed:@"neoznaceny_ctverec"] forState:UIControlStateNormal];
    
    UIButton * centralSource = [[UIButton alloc] initWithFrame:CGRectMake(242, 5, 40, 40)];
    [centralSource setBackgroundImage:[UIImage imageNamed:@"neoznaceny_ctverec"] forState:UIControlStateNormal];
    [add addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    add.tag=1;
    add.userInteractionEnabled=YES;
    [centralSource addTarget:self action:@selector(centralButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    centralSource.tag=2;
    centralSource.userInteractionEnabled=YES;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    _tVHeatings.userInteractionEnabled=NO;
    [cell.contentView addSubview:name];
    [cell.contentView addSubview:add];
    [cell.contentView addSubview:centralSource];
    return cell;
}

- (void)addButtonTapped:(id)sender
{
    
    UIButton * button = (UIButton*)sender;
    if ([[_heatersSelected objectAtIndex:((UIButton*)sender).tag] intValue]==1){
        [button setImage:[UIImage imageNamed:@"krizek"] forState:UIControlStateNormal];
        [_heatersSelected replaceObjectAtIndex:((UIButton*)sender).tag withObject:[[NSNumber alloc] initWithInt:0]];
    }else if ([[_heatersSelected objectAtIndex:((UIButton*)sender).tag] intValue]==0){
        [button setImage:[UIImage imageNamed:@"fajfka"] forState:UIControlStateNormal];
        
        [_heatersSelected replaceObjectAtIndex:((UIButton*)sender).tag withObject:[[NSNumber alloc] initWithInt:1]];
    }else{
        for (id subview in button.superview.subviews)
        {
            if ([(UIButton*)subview tag]==button.tag && [subview isKindOfClass:[UIButton class]]){
                [(UIButton*)subview setImage:nil forState:UIControlStateNormal];
            }
            
        }
        [_heatersSelected replaceObjectAtIndex:((UIButton*)sender).tag withObject:[[NSNumber alloc] initWithInt:0]];
    }
    
}
- (void)centralButtonTapped:(id)sender
{
    
    if ([[_heatersSelected objectAtIndex:((UIButton*)sender).tag] intValue]==0) return;
    UIButton * button = (UIButton*)sender;
    if ([[_heatersSelected objectAtIndex:((UIButton*)sender).tag] intValue]==1){
        [_heatersSelected replaceObjectAtIndex:((UIButton*)sender).tag withObject:[[NSNumber alloc] initWithInt:2]];
        
        [button setImage:[UIImage imageNamed:@"fajfka"] forState:UIControlStateNormal];
    }else{
        [_heatersSelected replaceObjectAtIndex:((UIButton*)sender).tag withObject:[[NSNumber alloc] initWithInt:1]];
        
        [button setImage:nil forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"krizek"] forState:UIControlStateNormal];
    }
}
/*
 
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 
 }
 
 
 
 -(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
 {
 UIView *cell = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
 
 UILabel * name = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, 93, 30)];
 [name setText:NSLocalizedString(@"Name", nil)];
 UILabel * add = [[UILabel alloc] initWithFrame:CGRectMake(114, 0, 93, 30)];
 [add setText:NSLocalizedString(@"Add", nil)];
 UILabel * centralSource = [[UILabel alloc] initWithFrame:CGRectMake(221, 0, 93, 30)];
 [centralSource setText:NSLocalizedString(@"Central source", nil)];
 
 [name setTextColor:[UIColor whiteColor]];
 [add setTextColor:[UIColor whiteColor]];
 [centralSource setTextColor:[UIColor whiteColor]];
 
 [name setTextAlignment:NSTextAlignmentCenter];
 [add setTextAlignment:NSTextAlignmentCenter];
 [centralSource setTextAlignment:NSTextAlignmentCenter];
 
 name.adjustsFontSizeToFitWidth=YES;
 name.minimumScaleFactor=0.5;
 
 add.adjustsFontSizeToFitWidth=YES;
 add.minimumScaleFactor=0.5;
 
 centralSource.adjustsFontSizeToFitWidth=YES;
 centralSource.minimumScaleFactor=0.5;
 
 
 [cell addSubview:name];
 [cell addSubview:add];
 [cell addSubview:centralSource];
 
 
 UIImageView * iVSeparator1 = [[UIImageView alloc] initWithFrame:CGRectMake(107, 5, 1, 20)];
 [iVSeparator1 setBackgroundColor:[UIColor whiteColor]];
 [cell addSubview:iVSeparator1];
 
 UIImageView * iVSeparator2 = [[UIImageView alloc] initWithFrame:CGRectMake(214, 5, 1, 20)];
 [iVSeparator2 setBackgroundColor:[UIColor whiteColor]];
 [cell addSubview:iVSeparator2];
 
 
 UIImageView * iVSeparator3 = [[UIImageView alloc] initWithFrame:CGRectMake(10, 29, 300, 1)];
 [iVSeparator3 setBackgroundColor:[UIColor whiteColor]];
 [cell addSubview:iVSeparator3];
 [cell setBackgroundColor:[UIColor blackColor]];
 return cell;
 }
 
 -(float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
 
 return  30.0;
 }
 */
-(void)generateHeaderGUI{
    UIView *cell = [[UIView alloc] initWithFrame:CGRectMake(0, 403, 320, 30)];
    cell.tag = 99;
    UILabel * name = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, 93, 30)];
    [name setText:NSLocalizedString(@"heat_cool_area_add_child_header_name", nil)];
    UILabel * add = [[UILabel alloc] initWithFrame:CGRectMake(114, 0, 93, 30)];
    [add setText:NSLocalizedString(@"heat_cool_area_add_child_header_add", nil)];
    UILabel * centralSource = [[UILabel alloc] initWithFrame:CGRectMake(221, 0, 93, 30)];
    [centralSource setText:NSLocalizedString(@"heat_cool_area_add_child_header_central_source", nil)];
    
    [name setTextColor:[UIColor whiteColor]];
    [add setTextColor:[UIColor whiteColor]];
    [centralSource setTextColor:[UIColor whiteColor]];
    
    [name setTextAlignment:NSTextAlignmentCenter];
    [add setTextAlignment:NSTextAlignmentCenter];
    [centralSource setTextAlignment:NSTextAlignmentCenter];
    
    name.adjustsFontSizeToFitWidth=YES;
    name.minimumScaleFactor=0.5;
    
    add.adjustsFontSizeToFitWidth=YES;
    add.minimumScaleFactor=0.5;
    
    centralSource.adjustsFontSizeToFitWidth=YES;
    centralSource.minimumScaleFactor=0.5;
    
    
    [cell addSubview:name];
    [cell addSubview:add];
    [cell addSubview:centralSource];
    
    
    UIImageView * iVSeparator1 = [[UIImageView alloc] initWithFrame:CGRectMake(107, 5, 1, 20)];
    [iVSeparator1 setBackgroundColor:[UIColor whiteColor]];
    [cell addSubview:iVSeparator1];
    
    UIImageView * iVSeparator2 = [[UIImageView alloc] initWithFrame:CGRectMake(214, 5, 1, 20)];
    [iVSeparator2 setBackgroundColor:[UIColor whiteColor]];
    [cell addSubview:iVSeparator2];
    
    
    UIImageView * iVSeparator3 = [[UIImageView alloc] initWithFrame:CGRectMake(10, 29, 300, 1)];
    [iVSeparator3 setBackgroundColor:[UIColor whiteColor]];
    [cell addSubview:iVSeparator3];
    [cell setBackgroundColor:[UIColor blackColor]];
    
    [_scrollView addSubview:cell];
    
}

-(void)generateBody{
    int start=403+30;
    _heatersSelected = [[NSMutableArray alloc] init];
    int i=0;
    while ( i < [_availableHeaters count]){
        Device* device = [_availableHeaters objectAtIndex:i];
        UIView * holder = [[UIView alloc] initWithFrame:CGRectMake(0, start+i*50, 320, 50)];
        holder.tag=99;
        [holder setBackgroundColor:[UIColor blackColor]];
        UIImageView * iVSeparator1 = [[UIImageView alloc] initWithFrame:CGRectMake(107, 10, 1, 30)];
        [iVSeparator1 setBackgroundColor:[UIColor whiteColor]];
        [holder addSubview:iVSeparator1];
        
        UIImageView * iVSeparator2 = [[UIImageView alloc] initWithFrame:CGRectMake(214, 10, 1, 30)];
        [iVSeparator2 setBackgroundColor:[UIColor whiteColor]];
        [holder addSubview:iVSeparator2];
        
        
        UIImageView * iVSeparator3 = [[UIImageView alloc] initWithFrame:CGRectMake(10, 49, 300, 0.75)];
        [iVSeparator3 setBackgroundColor:[UIColor whiteColor]];
        if (i != [_availableHeaters count]-1)[holder addSubview:iVSeparator3];
        
        [_heatersSelected addObject:[[NSNumber alloc] initWithInt:0]];
        
        UILabel * name = [[UILabel alloc] initWithFrame:CGRectMake(7, 0, 93, 50)];
        [name setText:device.label];
        
        [name setTextColor:[UIColor whiteColor]];
        
        [name setTextAlignment:NSTextAlignmentCenter];
        
        name.adjustsFontSizeToFitWidth=YES;
        name.minimumScaleFactor=0.5;
        
        UIButton * add = [[UIButton alloc] initWithFrame:CGRectMake(133, 5, 40, 40)];
        [add setBackgroundImage:[UIImage imageNamed:@"krizek"] forState:UIControlStateNormal];
        
        UIButton * centralSource = [[UIButton alloc] initWithFrame:CGRectMake(242, 5, 40, 40)];
        [centralSource setBackgroundImage:[UIImage imageNamed:@"krizek"] forState:UIControlStateNormal];
        [add addTarget:self action:@selector(addButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        add.tag=i;
        add.userInteractionEnabled=YES;
        [centralSource addTarget:self action:@selector(centralButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        centralSource.tag=i;
        centralSource.userInteractionEnabled=YES;
        _tVHeatings.userInteractionEnabled=NO;
        [holder addSubview:name];
        [holder addSubview:add];
        [holder addSubview:centralSource];
        [_scrollView addSubview:holder];
        if (_device !=nil){
            if ([_device.heatingDevices containsObject:device] || [_device.coolingDevices containsObject:device]){
                [self addButtonTapped:add];
            }
            if ([_device.centralSources containsObject:device]){
                [self centralButtonTapped:centralSource];
            }
        }
        i++;
    }
}

-(void)setupHeatingDevices{
    _heatersSelected = [[NSMutableArray alloc] init];
    int i=0;
    while ( i < [_availableHeaters count]){
        Device* device = [_availableHeaters objectAtIndex:i];
        [_heatersSelected addObject:[[NSNumber alloc] initWithInt:0]];
        if (_device !=nil){
            if ([_device.heatingDevices containsObject:device] || [_device.coolingDevices containsObject:device]){
                [_heatersSelected replaceObjectAtIndex:i withObject:[[NSNumber alloc] initWithInt:1]];
            }
            if ([_device.centralSources containsObject:device]){
                [_heatersSelected replaceObjectAtIndex:i withObject:[[NSNumber alloc] initWithInt:2]];
            }
        }
        i++;
    }
}


-(IBAction)backToMenu:(id)sender{
    
    [_container setViewController:@"heatingArea"];
    
}

-(IBAction)Save:(id)sender{
    NSArray * heatingDevices = [self selectHeatingDevices];
    
    if ([_lTemperatureSensor.text isEqualToString:@""]){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"heat_cool_area_empty_sensor_error", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        return;
    }else if (_temperatureSchedule == nil){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"heat_cool_area_empty_schedule_error", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else if ([heatingDevices count]==0){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"heat_cool_area_empty_cooling_heating_devices", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }else
        if ([_tFName.text isEqualToString:@""] || [[_lTemperatureOfset text] isEqualToString:@""]) {
            
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"Please fill all arrays", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
            
        }
    AppDelegate * delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (delegate.limitsSettings!=nil){
        NSDictionary * heatingDevicesDict =[[delegate.limitsSettings objectForKey:@"heatcoolarea"] objectForKey:@"heating devices"];
        if ([heatingDevices count] > [[heatingDevicesDict objectForKey:@"max count"] intValue]){
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:[NSString stringWithFormat:NSLocalizedString(@"Number of heating devices must be less or equal than %d", nil),[[heatingDevicesDict objectForKey:@"max count"] intValue] ]  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return;
            
        }
        
    }
    
    
    
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    NSMutableDictionary * heatCoolSettings = [[NSMutableDictionary alloc] init];
    NSDictionary * deviceInfo = [[NSDictionary alloc] initWithObjectsAndKeys:_tFName.text, @"label",
                                 @"temperature regulation area", @"type",
                                 @"HeatCoolArea", @"product type", nil];
    
    [heatCoolSettings setObject:deviceInfo forKey:@"device info"];
    [heatCoolSettings setObject:_temperatureSchedule forKey:@"schedule"];
    NSString * offsetString =[_lTemperatureOfset text];
    [heatCoolSettings setObject:[[NSNumber alloc] initWithFloat:[offsetString floatValue]] forKey:@"temperature offset"];
    
    
    [heatCoolSettings setObject:heatingDevices forKey:@"heating devices"];
    NSArray * coolingDevices = [[NSArray alloc] init];
    [heatCoolSettings setObject:coolingDevices forKey:@"cooling devices"];
    if (_deviceId!=nil){
        [heatCoolSettings setObject:_deviceId forKey:@"id"];
    }
    
    [heatCoolSettings setObject:_selectedSettings forKey:@"temperature sensor"];
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:heatCoolSettings
                                                       options:(NSJSONWritingOptions)  (NSJSONWritingPrettyPrinted)
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
        
    } else {
        NSLog(@"%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
    }
    
    [[SYAPIManager sharedInstance] postDataWithHeatCoolArea:heatCoolSettings success:^(AFHTTPRequestOperation * operation, id response){
        
        [_loaderDialog hide];
        [_container setViewController:@"heatingArea"];
        
        
    } failure:^(AFHTTPRequestOperation * operation, NSError * error){
        [_loaderDialog hide];
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"heat_cool_area_failed_add", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        
    }];
    
}

-(NSArray*)selectHeatingDevices{
    NSMutableArray * resultArray = [[NSMutableArray alloc] init];
    for (int i=0;i< [_heatersSelected count];i++){
        NSNumber * number = [_heatersSelected objectAtIndex:i];
        if ([number intValue]!=0){
            NSMutableDictionary * dict = [[NSMutableDictionary alloc] init];
            Device * device = [_availableHeaters objectAtIndex:i];
            [dict setObject:device.deviceID forKey:@"id"];
            if ([number intValue]==2){
                [dict setObject:[[NSNumber alloc] initWithBool:YES] forKey:@"central source"];
            }else{
                [dict setObject:[[NSNumber alloc] initWithBool:NO] forKey:@"central source"];
            }
            [resultArray addObject:dict];
        }
        
        
    }
    return resultArray;
    
}

- (IBAction)editBegin:(id)sender {
    UITextField * tf = sender;
    [tf setBackground:[UIImage imageNamed:@"edit_text_bcg_on.png"]];
    [tf setTextColor:[UIColor whiteColor]];
    
}
- (IBAction)editEnd:(id)sender {
    UITextField * tf = sender;
    [tf setBackground:[UIImage imageNamed:@"edit_text_bcg.png"]];
    [tf setTextColor:[UIColor colorWithRed:87/255 green:87/255 blue:87/255 alpha:1]];
}


@end
