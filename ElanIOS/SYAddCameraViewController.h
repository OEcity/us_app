//
//  SYAddCameraViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/14/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Camera.h"

@interface SYAddCameraViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) Camera*camera;

- (IBAction)textFieldDidBeginEditing:(UITextField *)textField;
- (IBAction)textFieldDidEndEditing:(UITextField *)textField;

-(void) keyboardWillShow:(NSNotification *)note;


@end
