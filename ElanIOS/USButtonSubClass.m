//
//  USButtonSubClass.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 27.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "USButtonSubClass.h"
#import "Constants.h"

@implementation USButtonSubClass


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    self.layer.borderColor = USBlueColor.CGColor;
    self.layer.borderWidth = 1.0f;
 

    

    
    [self setNeedsDisplay];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self){
        [self setBackgroundImage:nil forState:UIControlStateNormal];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    }
    return self;
}

-(void)setHighlighted:(BOOL)highlighted{
    [super setHighlighted:highlighted];
    
    if(highlighted){
        self.backgroundColor = USBlueColor;
    } else {
        self.backgroundColor = [UIColor clearColor];
    }
}

@end
