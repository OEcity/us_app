//
//  NSManagedObject+NSString.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 21/02/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObject (NSString)
-(void)setStringValue:(id)value forKey:(NSString*)key;

@end
