//
//  SearchService.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 1/17/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncSocket.h"
#import "GCDAsyncUdpSocket.h"
#import "Elan.h"
@protocol SearchServiceDelegate

-(void)searchServiceFinishedWithResult:(Elan*)discoveredElan;
-(void)searchServiceTimedOut;

@end

@interface SearchService : NSObject <GCDAsyncUdpSocketDelegate, GCDAsyncSocketDelegate>

@property (nonatomic, retain) NSTimer * timer;
@property (nonatomic, retain) id <SearchServiceDelegate> delegate;

+(SearchService*) sharedSearchService;
- (void)sendUdpDiscoverySocketWithDelegate:(id)delegate;
- (void)stopUdpDiscovery;

@end
