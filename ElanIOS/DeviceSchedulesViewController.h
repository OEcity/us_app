//
//  DeviceSchedulesViewController.h
//  Click Smart
//
//  Created by Tom Odler on 17.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HUDWrapper.h"
#import "DeviceDetailScheduleViewController.h"

@interface DeviceSchedulesViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property(nonatomic, retain) NSMutableArray * schedules;
@property(nonatomic, retain) NSMutableArray * devicesInSchedules;
@property(nonatomic, retain) NSMutableArray * tempDevices;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) HUDWrapper * loaderDialog;
@property (nonatomic, retain) UILabel * lNoContent;
@property (nonatomic, retain) NSDictionary* timeScheduleConfig;
@property ( nonatomic ) DeviceDetailScheduleViewController *nextController;

@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;


@end
