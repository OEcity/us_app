//
//  SY_State_HCAView.h
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 18/06/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SY_State_View.h"

#define DEFAULT_MODE_IMAGE_WIDTH 15

@interface SY_State_HCAView : SY_State_View
@property (weak, nonatomic) IBOutlet UILabel *lHeatingMode;
@property (weak, nonatomic) IBOutlet UIImageView *iHeatingMode;
@property (weak, nonatomic) IBOutlet UILabel *lTemp;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *cModeImageWidth;

@end
