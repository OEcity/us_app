//
//  SYViewFavouritesViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/7/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYViewFavouritesViewController.h"
#import "Device.h"
#import "Room.h"
#import "SYCoreDataManager.h"
#import "RGBTrimmerViewController.h"
#import "TrimmerViewController.h"
#import "Util.h"
#import "URLConnector.h"
#import "RGBTrimmerViewController.h"
#import "AppDelegate.h"
#import "SYDataLoader.h"
@interface SYViewFavouritesViewController ()

@end

@implementation SYViewFavouritesViewController
@synthesize devices;
@synthesize tableView;

#pragma mark - View Management

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.minimumPressDuration = 0.5; //seconds
    lpgr.delegate = self;
    [self.tableView addGestureRecognizer:lpgr];

}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

    
    //self.devices = [[NSMutableArray alloc] initWithArray:[CoreDataManager getFavourites]];
    _tableViewDelegate = [[DevicesTableViewDelegate alloc] init];
    [_tableViewDelegate setDeviceListMode:DeviceListModeFavourite];
    [_tableViewDelegate setView:self.view];
    [tableView setDelegate:_tableViewDelegate];
    [tableView setDataSource:_tableViewDelegate];
    [_tableViewDelegate setTableView:tableView];
    [_tableViewDelegate.tableView reloadData];
    
    [[SYWebSocket sharedInstance] reconnect];
    
//    [self changeCurrentRoom:app.viewController.currentRoomID];

}

- (void)initLocalizableStrings {
    [_leftBottomButton setTitle:NSLocalizedString(@"help", nil) forState:UIControlStateNormal];
    [_rightBottomButton setTitle:NSLocalizedString(@"menu", nil) forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    [self.tableViewDelegate handleLongPress:gestureRecognizer];
}


-(IBAction)nextStep:(id)sender{
    [self performSegueWithIdentifier:@"settings" sender:self];

}
-(IBAction)help:(id)sender{
    [self performSegueWithIdentifier:@"showManual" sender:self];
}



- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{

    return UIInterfaceOrientationMaskPortrait;
}

@end
