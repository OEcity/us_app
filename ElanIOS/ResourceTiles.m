//
//  ResourceTiles.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 08/11/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "ResourceTiles.h"

@implementation ResourceTiles


+(Tile*)generateTileForName:(NSString*)name{

        Tile * tile = [[Tile alloc] init];
    NSDictionary *const imageDict = [self getDict];
    NSString *imagePRefix = [imageDict valueForKey:name];
    if (imagePRefix==nil) imagePRefix = @"misc";
    
    UIImage* miscImage = [UIImage imageNamed:@"jine_off"];
    UIImage* imageOff = [UIImage imageNamed:[imagePRefix stringByAppendingString:@"_off"]];
    UIImage* imageOn = [UIImage imageNamed:[imagePRefix stringByAppendingString:@"_on"]];
    UIImage* imageSeda = [UIImage imageNamed:[imagePRefix stringByAppendingString:@"_seda"]];
    
    
    [tile setTile_off:imageOff ? imageOff : miscImage];
    [tile setTile_on:imageOn ? imageOn : miscImage];
    [tile setTile_seda:imageSeda ? imageSeda : miscImage];

    return tile;

}

+(NSDictionary*)getDict{
    NSDictionary *const imageDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"jine",@"misc",
                                     @"sprcha",@"bathroom",
                                     @"tv",@"living room",
                                     @"bar",@"bar",
                                     @"garaz",@"garage",
                                     @"host",@"guestroom",
                                     @"studovna",@"study",
                                     @"dilna",@"workroom",
                                     @"detsky_pokoj",@"child room",
                                     @"loznice",@"bedroom",
                                     @"pradelna",@"laundry",
                                     @"hala",@"hall",
                                     @"kuchyn",@"kitchen",
                                     @"wc",@"toilet",
                                     @"garden",@"garden",
                                     @"cellar",@"cellar",
                                     @"jidelna",@"dining room",
                                     @"bazen",@"pool",
                                     @"prizemi", @"ground floor",
                                     @"poschodi", @"floor",
                                     nil];
   
    return imageDict;
}

+(NSDictionary*)getDevicesDict{
    NSDictionary *const imageDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"svetlo",@"light",
                                     @"lampicka",@"lamp",
                                     @"stmivani",@"dimmed light",
                                     @"teplomer",@"heating",
                                     @"teplomer",@"thermometer",
                                     @"zaluzie",@"blinds",
                                     @"vrata",@"gate",
                                     @"ventilace",@"ventilation",
                                     @"zasuvka",@"appliance",
                                     @"rgb",@"rgb light",
                                     @"zasuvka",@"all",
                                     @"klimatizace",@"climatization",
                                     @"detektor",@"detector",
                                     @"zavlazovani",@"irrigation",
                                     @"para",@"dehumidifier",
                                     @"tv",@"TV",
                                     @"kamera", @"camera",
                                     @"zamek", @"lock",
                                     @"ir_prvky_air_conditioning", @"air conditioning",
                                     @"ir_prvky_bluray", @"blue ray",
                                     @"ir_prvky_dvd", @"DVD",
                                     @"ir_prvky_home_cinema", @"home cinema",
                                     @"ir_prvky_projektor", @"projector",
                                     @"ir_prvky_reproduktor", @"amplifier",
                                     @"ir_prvky_rolety", @"shutters",
                                     @"ir_prvky_air_conditioning", @"air conditioning",
                                     nil];

    return imageDict;
}

+(Tile*)generateDeviceTileForName:(NSString*)name{
    Tile * tile = [[Tile alloc] init];
    NSDictionary *const imageDict = [self getDevicesDict];
    NSString *imagePRefix = [imageDict valueForKey:name];
    if (imagePRefix==nil) imagePRefix = @"all_on_off";
    [tile setTile_off:[UIImage imageNamed:[imagePRefix stringByAppendingString:@"_off"]]];
    [tile setTile_on:[UIImage imageNamed:[imagePRefix stringByAppendingString:@"_on"]]];
    [tile setTile_seda:[UIImage imageNamed:[imagePRefix stringByAppendingString:@"_seda"]]];

    return tile;
    
}


+(NSString*)getDevicesTranslationName:(NSString*)deviceName{
    NSDictionary *const namesDict = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"irrigation",@"irrigation",
                                     @"dimmedLight",@"dimmed light",
                                     @"light",@"light",
                                     @"appliance",@"appliance",
                                     @"heating",@"heating",
                                     @"rgbLight",@"rgb light",
                                     @"lamp",@"lamp",
                                     @"ventilation",@"ventilation",
                                     @"thermometer",@"thermometer",
                                     @"dehumidifier",@"dehumidifier",
                                     @"gate",@"gate",
                                     @"blinds",@"blinds",
                                     @"detector",@"detector",
                                     @"climatization",@"climatization",
                                     @"TV",@"TV",
                                     @"detector",@"detector",
                                     @"DVD", @"DVD",
                                     @"homeCinema", @"home cinema",
                                     @"blueRay", @"blue ray",
                                     @"projector", @"projector",
                                     @"amplifier", @"amplifier",
                                     @"airConditioning", @"air conditioning",
                                     @"blinds", @"shutters",
                                     @"kamera", @"camera",
                                     nil];
    return [namesDict objectForKey:deviceName];
}
@end
