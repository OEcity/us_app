//
//  CentralSourceDetailViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 25.10.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYAPIManager.h"

@interface CentralSourceDetailViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@end
