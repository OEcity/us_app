//
//  DeviceScheduleDay.m
//  iHC-MIRF
//
//  Created by Tom Odler on 16.03.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "DeviceScheduleDay.h"

@implementation DeviceScheduleDay

@synthesize id = _id;
@synthesize modes = _modes;


-(id)initWithNameAndType:(NSString *)name type:(NSString *)type{
    self = [super init];
    
    if (self){
        _id = name;
        _modes = [[NSMutableArray alloc] init];
        if([type isEqualToString:@"switch"]){
                    [_modes addObject:[[TempDayMode alloc] initWithDayMode:[[NSNumber alloc] initWithInt:0] duration:[[NSNumber alloc] initWithInt:1440] type:[[NSString alloc]init]]];
        } else if ([type isEqualToString:@"diming"]){
                                [_modes addObject:[[TempDayMode alloc] initWithDayMode:[[NSNumber alloc] initWithInt:1] duration:[[NSNumber alloc] initWithInt:1440] type:[[NSString alloc]init]]];
        } else if ([type isEqualToString:@"rgb"] || [type isEqualToString:@"white"]) {
                                [_modes addObject:[[TempDayMode alloc] initWithDayMode:[[NSNumber alloc] initWithInt:1] duration:[[NSNumber alloc] initWithInt:1440] type:[[NSString alloc]init]]];
        } else {
                                [_modes addObject:[[TempDayMode alloc] initWithDayMode:[[NSNumber alloc] initWithInt:2] duration:[[NSNumber alloc] initWithInt:1440] type:[[NSString alloc]init]]];
        }

    }
    
    return self;
}


-(void)setupModes{
    int startTime = 0;
    for (TempDayMode *mode in _modes) {
        mode.startTime = [[NSNumber alloc] initWithInt:startTime];
        mode.originalStartTime = [[NSNumber alloc] initWithInt:startTime];
        mode.endTime = mode.originalEndTime = [[NSNumber alloc] initWithInt:startTime + [mode.duration intValue]];
        
        startTime = [mode.endTime intValue];
    }
}


- (id)copyWithZone:(NSZone *)zone
{
    DeviceScheduleDay *another = [[DeviceScheduleDay allocWithZone: zone] init];
    another.type = [_type copyWithZone:zone];
    [another setId: [_id copyWithZone:zone]];
    
    NSMutableArray *modeArray = [[NSMutableArray alloc] init];
    for (id mode in _modes) {
        [modeArray addObject:[mode copyWithZone:zone]];
        
    }
    [another setModes: modeArray];
    
    return another;
}




-(int) joinAndDeleteModes:(int)modePosition {
    modePosition = [self joinModes:modePosition];
    
    modePosition = [self deleteModes:modePosition];
    
    modePosition = [self joinModes:modePosition];
    
    [self redefineModes];
    
    return modePosition;
}

//Move modes
-(void) recountModes:(BOOL)isLeftDirection modePosition:(int) mModePosition {
    
    TempDayMode* actualMode = [_modes objectAtIndex:mModePosition];
    TempDayMode* nextMode;
    TempDayMode* mode;
    
    if (isLeftDirection) {
        
        for (int i = mModePosition; i >= 1; i--) {
            mode = [_modes objectAtIndex:i];
            nextMode = [self findValidNextMode:i-1];
            
            if ([mode.endTime intValue] == [mode.startTime intValue]) {
                if ([nextMode.endTime intValue] <= [nextMode.originalEndTime intValue]) {
                    continue;
                } else {
                    mode.endTime =[[NSNumber alloc] initWithInt:[actualMode.startTime intValue]] ;
                }
            }
            
            nextMode.endTime = [[NSNumber alloc] initWithInt:[mode.startTime intValue]];
            if ([nextMode.endTime intValue] < [nextMode.startTime intValue])
                nextMode.endTime = [[NSNumber alloc] initWithInt:[nextMode.startTime intValue]];
            
        }
        
    } else {
        NSInteger length = [_modes count];
        for (int i = mModePosition; i < length - 1; i++) {
            mode = [_modes objectAtIndex:i];
            nextMode = [self findValidNextModeUp:i+1];
            
            if ([mode.endTime intValue]== [mode.startTime intValue]) {
                if ([nextMode.startTime intValue] >= [nextMode.originalStartTime intValue]) {
                    continue;
                } else {
                    mode.startTime = [[NSNumber alloc] initWithInt:[actualMode.endTime intValue]];
                }
            }
            
            nextMode.startTime = [[NSNumber alloc] initWithInt:[mode.endTime intValue]];
            if ([nextMode.startTime intValue] > [nextMode.endTime intValue])
                nextMode.startTime = [[NSNumber alloc] initWithInt:[nextMode.endTime intValue]];
            
        }
    }
}

-(void) redefineModes {
    for (TempDayMode* mode in _modes) {
        mode.originalStartTime = [[NSNumber alloc] initWithInt:[mode.startTime intValue]];
        mode.originalEndTime = [[NSNumber alloc] initWithInt:[mode.endTime intValue]];
    }
}

-(TempDayMode*) findValidNextModeUp:(int) position {
    TempDayMode *mode = nil;
    NSInteger length = [_modes count];
    for (int i = position; i < length; i++) {
        mode = [_modes objectAtIndex:i];
        
        if ([mode.startTime intValue] != [mode.endTime intValue])
            break;
    }
    
    return mode;
}

-(TempDayMode*) findValidNextMode:(int) position {
    TempDayMode *mode = nil;
    for (int i = position; i >= 0; i--) {
        mode = [_modes objectAtIndex:i];
        
        if ([mode.startTime intValue] != [mode.endTime intValue])
            break;
    }
    
    return mode;
}

-(int) deleteModes:(int) mModePosition {
    TempDayMode *mode = nil;
    NSInteger length = [_modes count];
    for (NSInteger i = length - 1; i >= 0; i--) {
        mode = [_modes objectAtIndex:i];
        
        if ([mode.startTime intValue] >= [mode.endTime intValue]) {
            [_modes removeObjectAtIndex:i];
            
            
            if (mModePosition >= i)
                mModePosition--;
        }
    }
    
    return mModePosition;
}

-(int) joinModes:(int) mModePosition {
    TempDayMode *mode = nil;
    TempDayMode* nextMode;
    NSInteger length = [_modes count];
    for (NSInteger i = length - 1; i >= 1; i--) {
        mode = [_modes objectAtIndex:i];
        nextMode = [_modes objectAtIndex:i-1];
        
        if ([mode.mode intValue] == [nextMode.mode intValue]) {
            nextMode.endTime = [[NSNumber alloc] initWithInt:[mode.endTime intValue]];
            [_modes removeObject:mode];
            
            if (mModePosition >= i)
                mModePosition--;
        }
    }
    
    return mModePosition;
}

-(int) deleteMode:(NSInteger) result type:(NSString *)type{
    int mModePosition = -1;
    
    NSInteger index = result;
    
    if (index == 0 && [_modes count] == 1)
        return mModePosition;
    
    NSNumber*modeNumber;
    if([type isEqualToString:@"switch"]){
        modeNumber = [[NSNumber alloc]initWithInt:0];
    } else if([type isEqualToString:@"shutters"]){
        modeNumber = [[NSNumber alloc]initWithInt:2];
    } else if([type isEqualToString:@"rgb"] || [type isEqualToString:@"white"]){
        modeNumber = [[NSNumber alloc] initWithInt:1];
    } else {
        modeNumber = [[NSNumber alloc]initWithInt:1];
    }
    
    TempDayMode* original = [_modes objectAtIndex:index];
    original.mode = modeNumber;
    
    return [self joinAndDeleteModes:mModePosition];
}

-(int) editMode:(TempDayMode*) edit modePosition:(int) modePosition {
    
    if (modePosition == -1) {
        return [self addNewMode:edit];
    } else {
        return [self addEditMode:edit modePosition:modePosition];
    }
    
}

-(int) addEditMode:(TempDayMode*) addEdit modePosition:(int) modePosition {
    TempDayMode *originalMode = [_modes objectAtIndex:modePosition];
    
    
    if ([originalMode.startTime intValue] != [addEdit.startTime intValue]) {
        
        TempDayMode *tempDayLeft = [self findValidNextMode:modePosition-1];
        if (tempDayLeft != nil) {
            tempDayLeft.endTime = [[NSNumber alloc] initWithInt:[addEdit.startTime intValue]];
        } else {
            tempDayLeft = [[TempDayMode alloc] init];
            tempDayLeft.mode = [[NSNumber alloc] initWithInt:1];
            tempDayLeft.startTime = [[NSNumber alloc] initWithInt:0];;
            tempDayLeft.endTime = [[NSNumber alloc] initWithInt:[addEdit.startTime intValue]];
            [_modes insertObject:tempDayLeft atIndex:0];
            modePosition++;
        }
        
    }
    
    if ([originalMode.endTime intValue] != [addEdit.endTime intValue]) {
        
        TempDayMode *tempDayRight = [self findValidNextModeUp:modePosition+1];
        if (tempDayRight != nil) {
            tempDayRight.startTime = [[NSNumber alloc] initWithInt:[addEdit.endTime intValue]];
        } else {
            tempDayRight = [[TempDayMode alloc] init];
            tempDayRight.mode = [[NSNumber alloc] initWithInt:1];
            tempDayRight.startTime = [[NSNumber alloc] initWithInt:[addEdit.endTime intValue]];
            tempDayRight.endTime = [[NSNumber alloc] initWithInt:1440];
            [_modes insertObject:tempDayRight atIndex:[_modes count]];
        }
        
    }
    
    // Copy edited mode values
    originalMode.startTime = [[NSNumber alloc] initWithInt:[addEdit.startTime intValue]];
    originalMode.endTime = [[NSNumber alloc] initWithInt:[addEdit.endTime intValue] ];
    originalMode.mode = [[NSNumber alloc] initWithInt:[addEdit.mode intValue]];
    
    for (TempDayMode *mode in _modes) {
        // Mark to delete
        if ([mode.startTime intValue] > [addEdit.startTime intValue] && [mode.endTime intValue] < [addEdit.endTime intValue])
            mode.startTime = [[NSNumber alloc] initWithInt:[mode.endTime intValue]];
    }
    
    modePosition = [self joinAndDeleteModes:modePosition];
    [self recountModes:YES modePosition:modePosition];
    [self recountModes:NO modePosition:modePosition];
    
    return [self joinAndDeleteModes:modePosition];
}

-(int) addNewMode:(TempDayMode*) addEdit {
    int mModePosition = -1;
    
    NSInteger position = 0;
    NSInteger addIndex = NSIntegerMax;
    NSInteger addSplitIndex = 0;
    TempDayMode *splitMode = nil;
    for (TempDayMode *mode in _modes) {
        
        if ([mode.endTime intValue] > [addEdit.startTime intValue]) {
            addIndex = position + 1;
            
            // Case when we need split one interval
            if ([mode.endTime intValue]> [addEdit.endTime intValue]) {
                addSplitIndex = addIndex + 1;
                splitMode = [[TempDayMode alloc] init];
                splitMode.startTime = [[NSNumber alloc] initWithInt:[addEdit.endTime intValue]];
                splitMode.endTime = [[NSNumber alloc] initWithInt:[mode.endTime intValue]];
                splitMode.mode = [[NSNumber alloc] initWithInt:[mode.mode intValue]];
                
                mode.endTime = [[NSNumber alloc] initWithInt:[addEdit.startTime intValue]];
                
                break;
            }
            
            mode.endTime = [[NSNumber alloc] initWithInt:[addEdit.startTime intValue]];
            
            //Mark for delete
            if ([mode.startTime intValue] > [addEdit.startTime intValue])
                mode.startTime = [[NSNumber alloc] initWithInt:[mode.endTime intValue]];
        }
        
        
        position++;
    }
    [_modes insertObject:addEdit atIndex:addIndex];
    //modes.add(addIndex, addEdit);
    
    if (splitMode != nil)
        [_modes insertObject:splitMode atIndex:addSplitIndex];
    
    
    return [self joinAndDeleteModes:mModePosition];
}



@end

