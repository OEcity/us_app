//
//  DevicesViewController.h
//  ElanIOS
//
//  Created by Vratislav Zima on 5/29/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "URLConnectionListener.h"
#import "IntensityConfigurationViewController.h"
#import "RGBViewController.h"
#import "DeviceActionsViewController.h"
#import "DevicesTableViewDelegate.h"
#import <HUDWrapper.h>
#import "URLConnector.h"
#import "UrlResponse.h"
#import <CoreData/CoreData.h>
#import "SYBaseViewController.h"
#import "DevicesCollectionViewDelegate.h"
@class VoIPViewController;

@interface SYDevicesViewController : SYBaseViewController <UIGestureRecognizerDelegate, NSFetchedResultsControllerDelegate>
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic,retain) IBOutlet UIActivityIndicatorView * activityInd;
@property (nonatomic, retain) NSMutableArray* devices;
@property (nonatomic, retain) NSArray* rooms;
@property (nonatomic) NSInteger currentRoomID;
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, retain) IBOutlet UILabel * header;
@property (nonatomic, copy)  NSString * headerName;
@property (nonatomic, copy)  NSString * headerLabel;
@property (nonatomic, retain)  IBOutlet UIImageView * imageController;
@property (nonatomic, retain)  IBOutlet UIImageView * moveRight;
@property (nonatomic, retain)  IBOutlet UIImageView * moveLeft;
@property (nonatomic, retain)  IBOutlet UIView * headerView;
@property (nonatomic, retain)  IBOutlet NSMutableArray * rgbControllers;
@property (retain) IntensityConfigurationViewController * intensity;
@property (retain) DeviceActionsViewController * actions;
@property (nonatomic, retain) DevicesTableViewDelegate * tableViewDelegate;
@property (nonatomic, retain) DevicesCollectionViewDelegate * collectionViewDelegate;
@property (retain) RGBViewController * rgb;
@property (nonatomic, retain) Device *segueDevice;

@property (nonatomic, retain) Room *room;
@property NSUInteger pageIndex;

@property (nonatomic, retain) HUDWrapper * loaderDialog;
@property (weak, nonatomic) IBOutlet UIButton *rightBottomButton;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIButton *leftBottomButton;



-(void)syncComplete:(NSNotification *)note;
-(IBAction)SettingsButtonPressed:(id)sender;

@end
