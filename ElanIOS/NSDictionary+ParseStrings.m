//
//  NSDictionary+ParseStrings.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 02/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "NSDictionary+ParseStrings.h"

@implementation NSDictionary (ParseStrings)


-(id)objectForKeyNSNumber:(id)aKey{

    id returnedObject = [self objectForKey:aKey];
    if ([returnedObject isKindOfClass:[NSString class]]){
        NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
        [f setDecimalSeparator:@","];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        returnedObject = [f numberFromString:returnedObject];
    }
    return returnedObject;
}
@end
