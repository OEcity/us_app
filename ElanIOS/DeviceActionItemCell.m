//
//  DeviceActionItemCell.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 07/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "DeviceActionItemCell.h"
#import "AppDelegate.h"
#import "Util.h"
#import "SYAPIManager.h"
#import "SYCoreDataManager.h"
#import <AudioToolbox/AudioToolbox.h>

@interface DeviceActionItemCell ()
{
    NSTimer *_timer;
    struct time_struct{
        int secondsValue;
        int minutesValue;
        int hoursValue;
    };
}
@property(nonatomic, strong) NSIndexPath *indexPath;
- (void)_timerFired:(NSTimer*)info;

@end
@implementation DeviceActionItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

-(NSString*) getActionName:(NSString*) key{
    NSString* value = [_nameConverter objectForKey:key];
    
    if(value)
        return NSLocalizedString(value, @"");
    else
        return key;
}

-(id)initWithType:(Type)type owner:(id)owner bundle:(NSDictionary*)bundle{
    
    self = (DeviceActionItemCell *)[[[NSBundle mainBundle] loadNibNamed:[self cellTypeEnumToString:type] owner:owner options:nil] objectAtIndex:0];
    _indexPath = [bundle objectForKey:@"indexPath"];
    _nameConverter = [NSDictionary dictionaryWithDictionary:[Util generateNames]];
    _actionName = [bundle objectForKey:@"label"];
    _deviceName = [bundle objectForKey:@"device"];
    _deviceActionsViewController = [bundle objectForKey:@"parent"];
    Device* device = [[SYCoreDataManager sharedInstance] getDeviceWithID:_deviceName];
    UILongPressGestureRecognizer *lpgr;
    
    _bundle = bundle;
    _normalHeight = 95.0f;
    int min;
    int max;
    switch (type) {
        case kPlain:
            [_nullButton setTitle:[self getActionName: [bundle objectForKey:@"label"]] forState:UIControlStateNormal];
            [_nullButton setBackgroundImage:[UIImage imageNamed:@"action_text_bcg_on"] forState:UIControlStateHighlighted];
            _nullButton.titleLabel.adjustsFontSizeToFitWidth = YES;
            _nullButton.titleLabel.minimumScaleFactor = .5;
            break;
        case kBrightness:{
            UIImage *sliderLeftTrackImage = [UIImage imageNamed: @"action_slider_on.png"] ;
            UIImage *sliderRightTrackImage = [UIImage imageNamed: @"action_slider_off.png"] ;
            UIImage *sliderThumb = [UIImage imageNamed: @"action_thumb.png"];
            sliderLeftTrackImage = [sliderLeftTrackImage resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
            sliderRightTrackImage = [sliderRightTrackImage resizableImageWithCapInsets:UIEdgeInsetsZero resizingMode:UIImageResizingModeStretch];
            [_slider setMinimumTrackImage: sliderLeftTrackImage forState: UIControlStateNormal];
            [_slider setMaximumTrackImage: sliderRightTrackImage forState: UIControlStateNormal];
            [_slider setThumbImage:sliderThumb forState: UIControlStateNormal];
            _brightnessSettings = (Brightness*)[ bundle objectForKey:@"brightnessSettings"];
            _brightnessState= (NSNumber*)[ bundle objectForKey:@"brightnessState"];
            NSNumber* min = _brightnessSettings.min;
            NSNumber* max = _brightnessSettings.max;
            [_slider setMaximumValue:max.floatValue];
            [_slider setMinimumValue:min.floatValue];
            _slider.value = _brightnessState.floatValue;
            int percent = (int)((_slider.value/(max.floatValue - min.floatValue))*100);
            NSString * sliderValue = [NSString stringWithFormat:@"%d", percent];
            [_label setText:[[[[self getActionName: [bundle objectForKey:@"label"]] stringByAppendingString:@" "] stringByAppendingString:sliderValue] stringByAppendingString:@"%"]];
            
            _stepValue = _brightnessSettings.step.intValue;
            
            //            [[NSNotificationCenter defaultCenter] addObserver:self
            //                                                     selector:@selector(syncComplete:)
            //                                                         name:@"dataSyncComplete"
            //                                                       object:[SYWebSocket sharedInstance]];
            break;
        }
        case kTimeAndButton:
            [_setTimeLabel setText:NSLocalizedString(@"set", nil)];
            _nullButton.titleLabel.adjustsFontSizeToFitWidth = YES;
            _nullButton.titleLabel.minimumScaleFactor = .5;
            _expandedHeight = 241.0f;
            [_nullButton setTitle:[self getActionName: [bundle objectForKey:@"label"]] forState:UIControlStateNormal];
            [_nullButton setBackgroundImage:[UIImage imageNamed:@"action_text_bcg_on"] forState:UIControlStateHighlighted];
            [_actionClock setBackgroundImage:[UIImage imageNamed:@"action_clock_on"] forState:UIControlStateHighlighted];
            [_actionClock setBackgroundImage:[UIImage imageNamed:@"action_clock"] forState:UIControlStateNormal];
            
            
            for (int i=1; i < 7; i++){
                UIButton * but = (UIButton*)[self.view viewWithTag:i];
                if (i%2!=0)[but setBackgroundImage:[UIImage imageNamed:@"action_arrow_top_on"] forState:UIControlStateHighlighted];
                else [but setBackgroundImage:[UIImage imageNamed:@"action_arrow_bot_on"] forState:UIControlStateHighlighted];
                lpgr = [[UILongPressGestureRecognizer alloc]
                        initWithTarget:self action:@selector(handleLongPress:)];
                lpgr.minimumPressDuration = 0.5; //seconds
                lpgr.delegate = self;
                [but addGestureRecognizer:lpgr];
                
            }
            _timeSettings = (ChangeTime*)[ bundle objectForKey:@"time"];
            _timeDelayTime = (NSNumber*)[ bundle objectForKey:@"delayedTime"];
            _timeLabel = (NSString*)[ bundle objectForKey:@"time_label"];
            _timerStep = _timeSettings.step.intValue;
            min = _timeSettings.min.intValue;
            max = _timeSettings.max.intValue;
            
            if (min> max){
                min=0;
                max=0;
                _timerStep = 0;
                break;
            }
            if (min<60){
                [_secondsLabel setText:[NSString stringWithFormat:@"%02d", min]];
            }else if (min< 3600){
                [_minutesLabel setText:[NSString stringWithFormat:@"%02d", min]];
            }else{
                [_hoursLabel setText:[NSString stringWithFormat:@"%02d", min]];
            }
            
            _timerMax = max;
            _timerMin = min;
            _resultTime =[_hoursLabel.text intValue]*3600+[_minutesLabel.text intValue]*60+[_secondsLabel.text intValue];
            if (_timeDelayTime!=nil){
                _resultTime = _timeDelayTime.intValue;
                if (_resultTime > _timerMin && _resultTime < _timerMax){
                    
                    [_secondsLabel setText:[NSString stringWithFormat:@"%02d", _resultTime%60]];
                    [_minutesLabel setText:[NSString stringWithFormat:@"%02d", (_resultTime/60)%3600]];
                    [_hoursLabel setText:[NSString stringWithFormat:@"%02d", _resultTime/3600]];
                }
            }
            if (max < 3600){
                [_hoursLabel setEnabled:NO];
                [_hoursLabel setText:@"--"];
                [(UIButton*)[self.view viewWithTag:5] setEnabled:NO];
                [(UIButton*)[self.view viewWithTag:6] setEnabled:NO];
            }
            if (max<60){
                [_minutesLabel setEnabled:NO];
                [_minutesLabel setText:@"--"];
                [(UIButton*)[self.view viewWithTag:3] setEnabled:NO];
                [(UIButton*)[self.view viewWithTag:4] setEnabled:NO];
                
            }
            [self makeTimeInvisible];
            break;
        case kTime:
            [_setTimeLabel setText:NSLocalizedString(@"set", nil)];
            _normalHeight = 167.0f;
            _expandedHeight = 167.0f;
            [_label setText:[self getActionName: [bundle objectForKey:@"label"]]];
            [_actionClock setBackgroundImage:[UIImage imageNamed:@"action_clock_on"] forState:UIControlStateHighlighted];
            [_actionClock setBackgroundImage:[UIImage imageNamed:@"action_clock"] forState:UIControlStateNormal];
            for (int i=1; i < 7; i++){
                UIButton * but = (UIButton*)[self.view viewWithTag:i];
                //                if (i%2!=0)[but setBackgroundImage:[UIImage imageNamed:@"action_arrow_top_on"] forState:UIControlStateHighlighted];
                //                else [but setBackgroundImage:[UIImage imageNamed:@"action_arrow_bot_on"] forState:UIControlStateHighlighted];
                lpgr = [[UILongPressGestureRecognizer alloc]
                        initWithTarget:self action:@selector(handleLongPress:)];
                lpgr.minimumPressDuration = 0.5; //seconds
                lpgr.delegate = self;
                [but addGestureRecognizer:lpgr];
            }
            _timeSettings = (ChangeTime*)[ bundle objectForKey:@"time"];
            _timeDelayTime = (NSNumber*)[device getStateValueForStateName:_actionName];
            _resultTime = [_timeDelayTime intValue];
            _timeLabel = (NSString*)[ bundle objectForKey:@"time_label"];
            _timerStep = _timeSettings.step.intValue;
            min = _timeSettings.min.intValue;
            max = _timeSettings.max.intValue;
            
            if (min> max){
                min=0;
                max=0;
                _timerStep = 0;
                break;
            }
            if (min<60){
                [_secondsLabel setText:[NSString stringWithFormat:@"%02d", min]];
            }else if (min< 3600){
                [_minutesLabel setText:[NSString stringWithFormat:@"%02d", min]];
            }else{
                [_hoursLabel setText:[NSString stringWithFormat:@"%02d", min]];
            }
            
            _timerMax = max;
            _timerMin = min;
            _resultTime =[_hoursLabel.text intValue]*3600+[_minutesLabel.text intValue]*60+[_secondsLabel.text intValue];
            if (_timeDelayTime!=nil){
                _resultTime = _timeDelayTime.intValue;
                if (_resultTime > _timerMin && _resultTime < _timerMax){
                    [_secondsLabel setText:[NSString stringWithFormat:@"%02d", _resultTime%60]];
                    [_minutesLabel setText:[NSString stringWithFormat:@"%02d", (_resultTime/60)%3600]];
                    [_hoursLabel setText:[NSString stringWithFormat:@"%02d", _resultTime/3600]];
                }
            }
            if (max <= 3600){
                [_hoursLabel setEnabled:NO];
                [_hoursLabel setText:@"--"];
            }
            if (max<=60){
                [_minutesLabel setEnabled:NO];
                [_minutesLabel setText:@"--"];
            }
            
            break;
        case kOn:
            //_timeDelayOn = (NSNumber*)[ bundle objectForKey:@"on"];
            break;
            
        default:
            break;
    }
    
    if (![_deviceActionsViewController.expandedeRows containsObject:[NSNumber numberWithInteger:_indexPath.row]] && _expandedHeight != _normalHeight){
        [self makeTimeInvisible];
    }else{
        [self makeTimeVisible];
    }
    return self;
}




- (IBAction)nullButtonClick:(id)sender {
    [self sendNull:sender];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
}



-(void)sendNull:(id)sender{
    NSDictionary * data = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSNull alloc] init], _actionName, nil];
    [self putDataAsychn:data];
    NSLog(@"%@", data);
}

-(void)putDataAsychn:(NSMutableDictionary*)dict
{
    [[SYAPIManager sharedInstance] putDeviceAction:dict device:[[SYCoreDataManager sharedInstance] getDeviceWithID:_deviceName]
                                           success:^(AFHTTPRequestOperation* operation, id response){
                                               NSLog(@"update complete: %@",response);
                                           }
                                           failure:^(AFHTTPRequestOperation* operation, NSError *error) {
                                               NSLog(@"update error: %@",[error description]);
                                           }];
}

-(NSString*) cellTypeEnumToString:(Type)enumVal
{
    NSArray *imageTypeArray = [[NSArray alloc] initWithObjects:kTypeArray];
    return [imageTypeArray objectAtIndex:enumVal];
}


-(BOOL) subtractSecondFromSeconds:(int*)seconds minutes:(int*)minutes hours:(int*)hours{
    *seconds-=_timerStep;
    if (*seconds<0){
        if ([self subtractMinuteFromSeconds:seconds minutes:minutes hours:hours]){
            *seconds = 59;
        } else {
            *seconds = 0;
            return false;
        }
    }
    return true;
}

-(BOOL) subtractMinuteFromSeconds:(int*)seconds minutes:(int*)minutes hours:(int*)hours{
    *minutes-=_timerStep;
    if (*minutes<0){
        if ([self subtractHourFromSeconds:seconds minutes:minutes hours:hours]){
            *minutes = 59;
        } else {
            *minutes = 0;
            return false;
        }
    }
    return true;
}

-(BOOL) subtractHourFromSeconds:(int*)seconds minutes:(int*)minutes hours:(int*)hours{
    *hours-=_timerStep;
    if (*hours<0){
        *hours = 0;
        return false;
    }
    return true;
}

-(void) addSecondFromSeconds:(int*)seconds minutes:(int*)minutes hours:(int*)hours{
    *seconds+=_timerStep;
    if ((int)(*seconds/60) == 1) {
        *seconds %= 60;
        [self addMinuteFromSeconds:seconds minutes:minutes hours:hours];
    }
}

-(void) addMinuteFromSeconds:(int*)seconds minutes:(int*)minutes hours:(int*)hours{
    *minutes+=_timerStep;
    if ((int)(*minutes/60) == 1) {
        *minutes %= 60;
        [self addHourFromSeconds:seconds minutes:minutes hours:hours];
    }
}

-(void) addHourFromSeconds:(int*)seconds minutes:(int*)minutes hours:(int*)hours{
    *hours+=_timerStep;
    if (*hours > 99) {
        *hours = 99;
    }
}


#pragma mark Timer cell settings
- (IBAction)changeTime:(id)sender {
    
    AudioServicesPlaySystemSound (1104);
    
    UIButton * button = (UIButton *)sender;
    int secondsValue = [_secondsLabel.text intValue];
    int minutesValue = [_minutesLabel.text intValue];
    int hoursValue = [_hoursLabel.text intValue];
    switch (button.tag) {
        case 1:
            //seconds up
            [self addSecondFromSeconds:&secondsValue minutes:&minutesValue hours:&hoursValue];
            break;
        case 2:
            // senconds down
            [self subtractSecondFromSeconds:&secondsValue minutes:&minutesValue hours:&hoursValue];
            break;
        case 3:
            // minutes up
            [self addMinuteFromSeconds:&secondsValue minutes:&minutesValue hours:&hoursValue];
            break;
        case 4:
            // minutes down
            [self subtractMinuteFromSeconds:&secondsValue minutes:&minutesValue hours:&hoursValue];
            break;
        case 5:
            // hours up
            [self addHourFromSeconds:&secondsValue minutes:&minutesValue hours:&hoursValue];
            break;
        case 6:
            //hours down;
            [self subtractHourFromSeconds:&secondsValue minutes:&minutesValue hours:&hoursValue];
            break;
        default:
            break;
    }
    
    _resultTime =hoursValue*3600+minutesValue*60+secondsValue;
    if (_resultTime> _timerMax){
        _resultTime = _timerMax;
        secondsValue = _resultTime % 60;
        minutesValue = (_resultTime / 60) % 60;
        hoursValue = _resultTime / 3600;
    }
    
    if (_resultTime < _timerMin){
        _resultTime = _timerMin;
        secondsValue = _resultTime % 60;
        minutesValue = (_resultTime / 60) % 60;
        hoursValue = _resultTime / 3600;
    }
    
    
    [_secondsLabel setText:[NSString stringWithFormat:@"%02d", secondsValue]];
    [_minutesLabel setText:[NSString stringWithFormat:@"%02d", minutesValue]];
    [_hoursLabel setText:[NSString stringWithFormat:@"%02d", hoursValue]];
    if (_timeSettings.max.intValue <3600){
        [_hoursLabel setEnabled:NO];
        [_hoursLabel setText:@"--"];
    }
    if (_timeSettings.max.intValue<60){
        [_minutesLabel setEnabled:NO];
        [_minutesLabel setText:@"--"];
    }
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer{
    
    if ( gestureRecognizer.state == UIGestureRecognizerStateBegan ) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.2f
                                                  target:self
                                                selector:@selector(_timerFired:)
                                                userInfo:[[NSMutableDictionary alloc] initWithObjectsAndKeys:gestureRecognizer.view, @"tag", nil]
                                                 repeats:YES];
        UIButton * but = (UIButton*)gestureRecognizer.view;
        if (but.tag%2!=0)[but setBackgroundImage:[UIImage imageNamed:@"action_arrow_top_on"] forState:UIControlStateNormal];
        else [but setBackgroundImage:[UIImage imageNamed:@"action_arrow_bot_on"] forState:UIControlStateNormal];
        
        
    }else if (gestureRecognizer.state == UIGestureRecognizerStateEnded ){
        if (_timer != nil)
        {
            UIButton * but = (UIButton*)gestureRecognizer.view;
            if (but.tag%2!=0)[but setBackgroundImage:[UIImage imageNamed:@"action_arrow_top"] forState:UIControlStateNormal];
            else [but setBackgroundImage:[UIImage imageNamed:@"action_arrow_bot"] forState:UIControlStateNormal];
            
            [_timer invalidate];
            _timer = nil;
        }
    }
    
}
- (void)_timerFired:(NSTimer*)info
{
    UIView * view = [[info userInfo] objectForKey:@"tag"];
    [self changeTime:view];
}

- (IBAction)setTime:(id)sender {
    
    AudioServicesPlaySystemSound (1104);
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"warning", nil)
                                                    message:NSLocalizedString(@"delay_timer_set", nil)
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:nil];
    [alert show];
    [self performSelector:@selector(hideAlert:) withObject:alert afterDelay:2];
    [self rollDown:nil];
    
    [self putDataAsychn:[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithInt:_resultTime],_timeLabel, nil]];
}

- (IBAction)rollDown:(id)sender {
    //    id view = [self superview];
    //    while ([view isKindOfClass:[UITableView class]] == NO) {
    //        view = [view superview];
    //    }
    if (![_deviceActionsViewController.expandedeRows containsObject:[NSNumber numberWithInteger:_indexPath.row]]){
        [_deviceActionsViewController.expandedeRows addObject:[NSNumber numberWithInteger:_indexPath.row]];
    }else{
        [_deviceActionsViewController.expandedeRows removeObject:[NSNumber numberWithInteger:_indexPath.row]];
    }
    [_deviceActionsViewController showExpandedItems];
}

-(void)hideAlert:(UIAlertView*)sender{
    [sender dismissWithClickedButtonIndex:-1 animated:YES];
}

#pragma mark Brightness cell settings

-(IBAction)valueChanged:(id)sender {
    // This determines which "step" the slider should be on. Here we're taking
    //   the current position of the slider and dividing by the `self.stepValue`
    //   to determine approximately which step we are on. Then we round to get to
    //   find which step we are closest to.
    float newStep = roundf((_slider.value) / self.stepValue);
    
    // Convert "steps" back to the context of the sliders values.
    _slider.value = newStep * self.stepValue;
    int percent = (int)((_slider.value/(_slider.maximumValue - _slider.minimumValue))*100);
    NSString * sliderValue = [NSString stringWithFormat:@"%d", percent];
    [_label setText:[[[[self getActionName: [_bundle objectForKey:@"label"]] stringByAppendingString:@" "] stringByAppendingString:sliderValue] stringByAppendingString:@"%"]];
    
}

-(IBAction)commitSliderData:(id)sender{
    [self putDataAsychn:[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithFloat:_slider.value],_actionName, nil]];
    
}
/*
 -(void)syncComplete:(NSNotification *)note{
 Device * dev = [[note userInfo] objectForKey:@"device"];
 if ([dev.deviceID isEqualToString:_deviceName]){
 [_slider setValue:dev.deviceState.brightness.floatValue animated:YES];
 int percent = (int)((_slider.value/(_slider.maximumValue - _slider.minimumValue))*100);
 NSString * sliderValue = [NSString stringWithFormat:@"%d", percent];
 
 [_label setText:[[[[self getActionName: [_bundle objectForKey:@"label"]] stringByAppendingString:@" "] stringByAppendingString:sliderValue] stringByAppendingString:@"%"]];
 
 }
 
 }
 */
-(void)makeTimeVisible{
    for (int i=1; i <17; i++){
        [[self.view viewWithTag:i] setHidden:NO];
    }
    self.height = _expandedHeight;
    [_expandArrow setImage:[UIImage imageNamed:@"scene_arrow_expanded"] forState:UIControlStateNormal];
}
-(void)makeTimeInvisible{
    for (int i=1; i <17; i++){
        [[self.view viewWithTag:i] setHidden:YES];
    }
    self.height = _normalHeight;
    [_expandArrow setImage:[UIImage imageNamed:@"scene_arrow"] forState:UIControlStateNormal];
}
@end
