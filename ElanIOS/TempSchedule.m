//
//  TempSchedule.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 1/21/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "TempSchedule.h"
@implementation TempSchedule

-(id) init{
    
    self= [super init];
    if (self){
        
        _modes = [[NSMutableArray alloc] initWithCapacity:4];
        _modes[0] = [[TempScheduleMode alloc] initWithId:[[NSNumber alloc] initWithInt:1] min:[[NSNumber alloc] initWithInt:15] max:[[NSNumber alloc] initWithInt:35]];
        _modes[1] = [[TempScheduleMode alloc] initWithId:[[NSNumber alloc] initWithInt:2] min:[[NSNumber alloc] initWithInt:20] max:[[NSNumber alloc] initWithInt:35]];
        _modes[2] = [[TempScheduleMode alloc] initWithId:[[NSNumber alloc] initWithInt:3] min:[[NSNumber alloc] initWithInt:24] max:[[NSNumber alloc] initWithInt:35]];

        _modes[3] = [[TempScheduleMode alloc] initWithId:[[NSNumber alloc] initWithInt:4] min:[[NSNumber alloc] initWithInt:28] max:[[NSNumber alloc] initWithInt:35]];
        
        _days = [[NSMutableArray alloc] initWithCapacity:7];
        _days[0] =[[TempScheduleDay alloc] initWithName:@"monday"];
        _days[1] =[[TempScheduleDay alloc] initWithName:@"tuesday"];
        _days[2] =[[TempScheduleDay alloc] initWithName:@"wednesday"];
        _days[3] =[[TempScheduleDay alloc] initWithName:@"thursday"];
        _days[4] =[[TempScheduleDay alloc] initWithName:@"friday"];
        _days[5] =[[TempScheduleDay alloc] initWithName:@"saturday"];
        _days[6] =[[TempScheduleDay alloc] initWithName:@"sunday"];

        
        for (TempScheduleDay *day in _days)
            [day setupModes];

        //TODO setup hysteresis from previous viewcontroller

    }
    
    return  self;

}
- (TempScheduleDay*) getDayByPosition:(int) position {
    if (_days == nil)
        return nil;
    
    NSString*key = nil;
    switch (position) {
        case 0:
        default:
            key = @"monday";
            break;
            
        case 1:
            key = @"tuesday";
            break;
            
        case 2:
            key = @"wednesday";
            break;
            
        case 3:
            key = @"thursday";
            break;
            
        case 4:
            key = @"friday";
            break;
            
        case 5:
            key = @"saturday";
            break;
            
        case 6:
            key = @"sunday";
            break;
    }
    
    for (TempScheduleDay *day in _days)
        if ([day.id isEqualToString:key])
            return day;
    
    return nil;
}

- (void) setDayOnPosition:(TempScheduleDay*)day position:(int)position{
#warning upravit key
    
    NSString*key = nil;
    switch (position) {
        case 0:
        default:
            key = @"monday";
            break;
            
        case 1:
            key = @"tuesday";
            break;
            
        case 2:
            key = @"wednesday";
            break;
            
        case 3:
            key = @"thursday";
            break;
            
        case 4:
            key = @"friday";
            break;
            
        case 5:
            key = @"saturday";
            break;
            
        case 6:
            key = @"sunday";
            break;
    }
    int i=0;
    int rec = 0;
    for (TempScheduleDay *den in _days){
        
        if ([den.id isEqualToString:key]){
            
            rec = i;
        }
        i++;
    }
    
    _days[rec] = day;
}

- (TempScheduleMode*) getModeById:(int) id {
    if (_modes == nil)
        return nil;
    
    for (TempScheduleMode *mode in _modes)
        if ([mode.id intValue] == id)
            return mode;
    
    return nil;
}




@end
