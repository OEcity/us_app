//
//  LedWhitePlanViewController.h
//  iHC-MIRF
//
//  Created by Tom Odler on 18.04.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TouchDownGestureRecognizer.h"
#import "CircleView.h"
#import "HUDWrapper.h"
#import <UIKit/UIKit.h>

@protocol whiteSetterDelegate
-(void)setWhite:(int)value red:(int)cRed green:(int)cGreen blue:(int)cBlue mode:(NSInteger)mode intensity:(int)intensity;
@end


@interface LedWhitePlanViewController : UIViewController
@property (nonatomic, retain) IBOutlet UIImageView * colorPicker;
@property (nonatomic, retain) IBOutlet UIImageView * colorView;
@property (nonatomic, retain) IBOutlet CircleView * colorTaste;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * activityIndicator;
@property (nonatomic, retain) CircleView * myCircle;
@property (nonatomic, retain) NSArray * colors;
@property (nonatomic, copy)  NSString * deviceName;
@property (nonatomic, retain) IBOutlet UIImageView* closeWindow;
@property (nonatomic, retain) HUDWrapper * loaderDialog;
@property size_t w;
@property size_t h;
@property CGPoint colorViewCenter;
@property NSInteger mode;
@property double cRed;
@property double cGreen;
@property double cBlue;
@property int intensity;
@property int whiteBalance;
@property int kelvin;
@property     UIView * circleView;
@property (weak, nonatomic) IBOutlet UILabel *onOffLabel;
@property (weak, nonatomic) IBOutlet UILabel *kelvinLabel;
@property (nonatomic, retain) id <whiteSetterDelegate> myDelegate;
-(id)initWithDelegate:(id<whiteSetterDelegate>)delegate value:(int)value mode:(NSInteger)mode red:(int)cRed green:(int)cGreen blue:(int)cBlue intensity:(int)intensity;

@end
