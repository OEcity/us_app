//
//  AddModeColorDetailTVCell.h
//  iHC-MIRF
//
//  Created by admin on 15.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddModeColorDetailTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIView *colorView;
@property (weak, nonatomic) IBOutlet UIView *whiteView;


@end
