//
//  IPTableViewCell.m
//  iHC-MIRF
//
//  Created by Vlastimil Venclik on 26.04.14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "IPTableViewCell.h"

@implementation IPTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
