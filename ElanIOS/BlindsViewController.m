//
//  BlindsViewController.m
//  US App
//
//  Created by Tom Odler on 16.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "BlindsViewController.h"
#import "SYAPIManager.h"
#import "SYCoreDataManager.h"
#import <QuartzCore/QuartzCore.h>
#import "Constants.h"
#import "UIViewController+RevealViewControllerAddon.h"

@interface BlindsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *deviceNameLabel;
@property (nonatomic) BOOL automat;
@property (nonatomic) UIButton* automatButton;

@end

@implementation BlindsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initFetchedResultsControllerWithHCAID:_device.deviceID];
    
    _automatButton = (UIButton*) [self.view viewWithTag:3];
    
    for(int i = 1; i<5; i++){
        UIButton *button = (UIButton*) [self.view viewWithTag:i];
        button.layer.borderColor = [USBlueColor CGColor];
        button.layer.borderWidth = 1.0f;
    }
    
    _deviceNameLabel.text = _device.label;
    // Do any additional setup after loading the view.
    
    //Configure navigation bar apperence
    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
    
    [self initAutomat];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    UIButton *stopButton = [self.view viewWithTag:1];
    [[stopButton layer] setBorderWidth:1.0f];
    [[stopButton layer] setBorderColor: USBlueColor.CGColor];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if([[_device getStateValueForStateName:@"roll up"] boolValue]){
        [_imgZaluzie setHidden:YES];
        [_imgZaluzieUp setHidden:NO];
    } else {
        [_imgZaluzie setHidden:NO];
        [_imgZaluzieUp setHidden:YES];
    }
}

-(void)initAutomat{
    _automatButton.selected = NO;
    _automat = NO;
    
    if([[_device getStateValueForStateName:@"automat"]boolValue]){
        _automatButton.selected = YES;
        _automat = YES;
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)sendAction:(id)sender {
    UIButton *myButton = (UIButton*)sender;
    NSMutableDictionary * data = [[NSMutableDictionary alloc] init];
    switch(myButton.tag){
        case 1:
            //STOP
            [data setObject:[[NSNull alloc] init] forKey:@"stop"];
            break;
        case 2:
            //UP
            [data setObject:[[NSNull alloc] init] forKey:@"roll up"];
            break;
        case 3:
            //AUTOMAT
            [data setObject:[NSNumber numberWithBool:!_automat] forKey:@"automat"];
            break;
        case 4:
            //DOWN
            [data setObject:[[NSNull alloc] init] forKey:@"roll down"];
            break;
    }
    if(data != nil)
    [self putAction:data];
}

-(void)putAction:(NSDictionary*)dict{
[[SYAPIManager sharedInstance] putDeviceAction:dict device: _device
                                       success:^(AFHTTPRequestOperation* operation, id response){
                                           NSLog(@"update complete: %@",response);
                                       }
                                       failure:^(AFHTTPRequestOperation* operation, NSError *error) {
                                           NSLog(@"update error: %@",[error description]);
                                       }];
}

- (void)initFetchedResultsControllerWithHCAID:(NSString*)hcaID {
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"State"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"device.label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.device.deviceID == %@", hcaID];
    [fetchRequest setPredicate:predicate];
    _devices = [[NSFetchedResultsController alloc]
                initWithFetchRequest:fetchRequest
                managedObjectContext:context
                sectionNameKeyPath:nil
                cacheName:nil];
    _devices.delegate = self;
    NSError *error;
    
    if (![_devices performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }else if ([[_devices fetchedObjects] count]>0){
        _device = (Device*)((State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if ([_devices.fetchedObjects count]>0){
        _device = (Device*)((State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
        
        [self syncComplete:_device];
    }
}

-(void)syncComplete:(Device *)device{
    [self initAutomat];
    if([[device getStateValueForStateName:@"roll up"] boolValue]){
        [_imgZaluzie setHidden:YES];
        [_imgZaluzieUp setHidden:NO];
    } else {
        [_imgZaluzie setHidden:NO];
        [_imgZaluzieUp setHidden:YES];
    }
}

@end
