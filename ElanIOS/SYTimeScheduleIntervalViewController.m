//
//  SYTimeScheduleIntervalViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 12/5/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "SYTimeScheduleIntervalViewController.h"
#import "TempScheduleMode.h"
#import "TempDayMode.h"
#import "SYAPIManager.h"
@interface SYTimeScheduleIntervalViewController ()

@end

@implementation SYTimeScheduleIntervalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    /*
    for (UIView * view in  _modeFrames){
      //  view.layer.borderColor = [UIColor grayColor].CGColor;
      //  view.layer.borderWidth = 1.5f;
    }
     */
    CGRect screenRect = [[UIScreen mainScreen] bounds];

    CGFloat swimlinesWidth = screenRect.size.height-140;

    
    _res = [[NSArray alloc] initWithObjects:NSLocalizedString(@"temp_schedule_monday", nil) , NSLocalizedString(@"temp_schedule_tuesday", nil), NSLocalizedString(@"temp_schedule_wednesday", nil), NSLocalizedString(@"temp_schedule_thursday", nil), NSLocalizedString(@"temp_schedule_friday", nil),NSLocalizedString(@"temp_schedule_saturday", nil),NSLocalizedString(@"temp_schedule_sunday", nil), nil];
    
    NSString * scheduleString = [[NSLocalizedString(@"temp_schedule_title", nil) stringByAppendingString:@" " ]  stringByAppendingString:[_schedule objectForKey:@"label"]];
    [_lTimeScheduleName setText:scheduleString];
    _tempSchedule = [[TempSchedule alloc] init];
    if ([_schedule objectForKey:@"schedule"]!=nil)
    _tempSchedule.days =  [self getDays:_schedule];
    if ([_schedule objectForKey:@"modes"]!=nil)
    _tempSchedule.modes = [self getModes:_schedule];
    [_tempSchedule setServerId:[_schedule objectForKey:@"id"]];
    [_tempSchedule setHysteresis:[_schedule objectForKey:@"hysteresis"]];
    [_tempSchedule setName:[_schedule objectForKey:@"label"]];
    _mDay = [_tempSchedule getDayByPosition:0];
    _currentDayPosition = 0;
    CGRect frame =[[_modeFrames objectAtIndex:0] frame];

    
    for (int i =0; i <16; i++){
        UIView * swimLine = [[UIView alloc] initWithFrame:CGRectMake(i%4==0?-frame.size.width:0, (_swimlinesArea.frame.size.height/16)*i -0.75f, i%4==0?swimlinesWidth+frame.size.width:swimlinesWidth, i%4==0?1.5f:0.75f)];
        [swimLine setBackgroundColor:[UIColor grayColor]];
        [_swimlinesArea addSubview:swimLine];
        
        
    }
    UIView * swimLine = [[UIView alloc] initWithFrame:CGRectMake(-frame.size.width, _swimlinesArea.frame.size.height-0.75f, swimlinesWidth+frame.size.width, 1.5f)];
    [swimLine setBackgroundColor:[UIColor grayColor]];
    [_swimlinesArea addSubview:swimLine];

    UIView * swimLine2 = [[UIView alloc] initWithFrame:CGRectMake(frame.origin.x-1, frame.origin.y, 1.5f, 4*frame.size.height-2)];
    [swimLine2 setBackgroundColor:[UIColor grayColor]];
    [self.view addSubview:swimLine2];


    UIView * swimLineVertical2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1.5f, _swimlinesArea.frame.size.height)];
    [swimLineVertical2 setBackgroundColor:[UIColor grayColor]];
    [_swimlinesArea addSubview:swimLineVertical2];
    
    
    [self.view addSubview:[self prepareTimeAxisWithFrame:CGRectMake(_swimlinesArea.frame.origin.x, _swimlinesArea.frame.origin.y + _swimlinesArea.frame.size.height + 10, swimlinesWidth, 20.0f)]];
    
    UIView * bottomLine = [[UIView alloc] initWithFrame:CGRectMake(0,  _swimlinesArea.frame.origin.y + _swimlinesArea.frame.size.height + 30, self.view.frame.size.height , 3.0)];
    [bottomLine setBackgroundColor:[UIColor grayColor]];
    [self.view addSubview:bottomLine];
    CGFloat screenHeight = screenRect.size.height;
    _timeScheduleView = [[HeatTimeScheduleView alloc] initWithFrameAndDay:CGRectMake(_swimlinesArea.frame.origin.x, 0, screenHeight-140, _swimlinesArea.frame.size.height +_swimlinesArea.frame.origin.y)  tempScheduleDay:_mDay topOffset:_swimlinesArea.frame.origin.y parentViewController:self];
    [self.view addSubview:_timeScheduleView];
    [self handleDayChange:0];

    [_lCopyTo setText:NSLocalizedString(@"temp_schedule_copy_to", nil)];
    [_lNewTitle setText:NSLocalizedString(@"temp_schedule_new", nil)];
    [_lTimeAxisTitle setText:NSLocalizedString(@"temp_schedule_time_axis", nil)];
    [_lSave setText:NSLocalizedString(@"temp_schedule_dialog_save", nil)];
    [_lAlltitle setTitle:NSLocalizedString(@"temp_schedule_day_all", nil) forState:UIControlStateNormal];
    [_lMonTitle setTitle:NSLocalizedString(@"temp_schedule_monday_short", nil) forState:UIControlStateNormal];
    [_lTueTitle setTitle:NSLocalizedString(@"temp_schedule_tuesday_short", nil) forState:UIControlStateNormal];
    [_lWedTitle setTitle:NSLocalizedString(@"temp_schedule_wednesday_short", nil) forState:UIControlStateNormal];
    [_lThurTtitle setTitle:NSLocalizedString(@"temp_schedule_thursday_short", nil) forState:UIControlStateNormal];
    [_lFrTitle setTitle:NSLocalizedString(@"temp_schedule_friday_short", nil) forState:UIControlStateNormal];
    [_lSatTitle setTitle:NSLocalizedString(@"temp_schedule_saturday_short", nil) forState:UIControlStateNormal];
    [_lSunTitle setTitle:NSLocalizedString(@"temp_schedule_sunday_short", nil) forState:UIControlStateNormal];
    
    [_lAttenuationTitle setText:NSLocalizedString(@"heat_mode_attenuation", nil)];
    [_lMinimumTitle setText:NSLocalizedString(@"heat_mode_minimum", nil)];
    [_lNormalTitle setText:NSLocalizedString(@"heat_mode_normal", nil)];
    [_lComfortTitle setText:NSLocalizedString(@"heat_mode_comfort", nil)];
    
    
    
    
    

    
    [_lMinimumTemperature setText:[NSString stringWithFormat:@"%.1f", [[[[_schedule objectForKey:@"modes"] objectForKey:@"1"] valueForKey:@"min"] floatValue] ]];
    
    [_lMinimumTemperature setText:[_lMinimumTemperature.text stringByAppendingString:@" °C"]];
    [_lAttenuationTemperature setText:[NSString stringWithFormat:@"%.1f", [[[[_schedule objectForKey:@"modes"] objectForKey:@"2"] valueForKey:@"min"] floatValue] ]];
    [_lAttenuationTemperature setText:[_lAttenuationTemperature.text stringByAppendingString:@" °C"]];
    [_lNormalTemperature setText:[NSString stringWithFormat:@"%.1f", [[[[_schedule objectForKey:@"modes"] objectForKey:@"3"] valueForKey:@"min"] floatValue] ]];
    [_lNormalTemperature setText:[_lNormalTemperature.text stringByAppendingString:@" °C"]];
    [_lComfortTemperature setText:[NSString stringWithFormat:@"%.1f", [[[[_schedule objectForKey:@"modes"] objectForKey:@"4"] valueForKey:@"min"] floatValue] ]];
    [_lComfortTemperature setText:[_lComfortTemperature.text stringByAppendingString:@" °C"]];

    [self.view bringSubviewToFront:[self.view viewWithTag:199]];
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(NSMutableArray*)getModes:(NSDictionary*)dictionary{
    
    TempScheduleMode* mode;
    NSMutableArray * modes = [[NSMutableArray alloc] init];
    NSDictionary * modesDictionary = [dictionary objectForKey:@"modes"];
    for (NSString* fieldName in modesDictionary) {
        
        NSDictionary * object = [modesDictionary objectForKey:fieldName];
        mode = [[TempScheduleMode alloc] init];
        mode.max = [[NSNumber alloc] initWithInt:35];
        mode.min =  [object valueForKey:@"min"];
        
        mode.id = [[NSNumber alloc] initWithInt:[fieldName intValue]] ;
        [modes addObject:mode];
    }
    return modes;
}


-(NSMutableArray*)getDays:(NSDictionary*)dictionary{
    TempScheduleDay * day;
    TempDayMode * dayMode;
    NSMutableArray * days = [[NSMutableArray alloc] init];
    for (NSString * dayName in [dictionary objectForKey:@"schedule"]) {
        
        day = [[TempScheduleDay alloc] init];
        day.id = dayName;
        
        day.modes = [[NSMutableArray alloc] init];
        
        for (NSDictionary *dayObject in [[dictionary objectForKey:@"schedule"] objectForKey:dayName]) {
            
            dayMode = [[TempDayMode alloc] init];
            dayMode.duration = [dayObject valueForKey:@"duration"];
            dayMode.mode = [dayObject valueForKey:@"mode"];
            
            [day.modes addObject:dayMode];
        }
        
        [day setupModes];
        [days addObject:day];
    }
    return days;

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(UIView*)prepareTimeAxisWithFrame:(CGRect)frame{

    UIView * containerView = [[UIView alloc] initWithFrame:frame];
    for (int i = 0; i < 24*4; i++){
        CGFloat positionX = ((frame.size.width-1.5f)/(24*4))*i;
        // draw the largest
        if (i%24==0){
            UIView * largeLine = [[UIView alloc] initWithFrame:CGRectMake(positionX, 0, 1.5f, 10.0f)];
            [largeLine setBackgroundColor:[UIColor blackColor]];
            [containerView addSubview:largeLine];
            
            UILabel *hoursLabel = [[UILabel alloc]initWithFrame:CGRectMake(positionX-10, 10.0f, 20.0f, 10.0f)];
            [hoursLabel setBackgroundColor:[UIColor clearColor]];
            [hoursLabel setTextColor:[UIColor blackColor]];
            [hoursLabel setFont:[UIFont systemFontOfSize:9.0f]];
            [hoursLabel setTextAlignment:NSTextAlignmentCenter];
            [hoursLabel setText:[NSString stringWithFormat:@"%d", i/4]];
            [containerView addSubview:hoursLabel];
        }
        if (i%4==0){
            UIView * mediumLine = [[UIView alloc] initWithFrame:CGRectMake(positionX, 0, 1.3f, 5.0f)];
            [mediumLine setBackgroundColor:[UIColor blackColor]];
            [containerView addSubview:mediumLine];
        
        }
        
        UIView * smallLine = [[UIView alloc] initWithFrame:CGRectMake(positionX, 0, 0.75f, 2.5f)];
        [smallLine setBackgroundColor:[UIColor blackColor]];
        [containerView addSubview:smallLine];
        
    }
    UIView * largeLine = [[UIView alloc] initWithFrame:CGRectMake(frame.size.width-1.5f, 0, 1.5f, 10.0f)];
    [largeLine setBackgroundColor:[UIColor blackColor]];
    [containerView addSubview:largeLine];
    
    UILabel *hoursLabel = [[UILabel alloc]initWithFrame:CGRectMake(frame.size.width-11.5f, 10.0f, 20.0f, 10.0f)];
    [hoursLabel setBackgroundColor:[UIColor clearColor]];
    [hoursLabel setTextColor:[UIColor blackColor]];
    [hoursLabel setFont:[UIFont systemFontOfSize:9.0f]];
    [hoursLabel setTextAlignment:NSTextAlignmentCenter];
    [hoursLabel setText:@"24"];
    [containerView addSubview:hoursLabel];
    
    return containerView;
}


- (IBAction)closeButtonTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


- (IBAction)newButtonTapped:(id)sender {
    _addHeatingInterval = [[AddHeatingIntervalViewController alloc] initWithNibName:@"AddHeatingIntervalViewController" bundle:nil];
    _addHeatingInterval.delegate = self;
    _addHeatingInterval.modePos = -1;
    [self presentViewController:_addHeatingInterval animated:YES completion:nil];
    
}


- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
    return UIInterfaceOrientationLandscapeRight;
}

-(BOOL)shouldAutorotate{
    return false;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight);
}

-(void)addHeatingModeToday:(BOOL)today tempDayMode:(TempDayMode*)tempdayMode position:(int)position erase:(BOOL)erase{
    if (!erase){
        if (today){
            [_timeScheduleView addMode:tempdayMode position:position];
            
        }else{
            
            NSInteger tempDayPosition = _currentDayPosition + 1;
            if(tempDayPosition == 7)
                tempDayPosition = 0;
            
            TempScheduleDay* tempScheduleDay = [_tempSchedule getDayByPosition:(int)tempDayPosition];
            [tempScheduleDay editMode:tempdayMode modePosition:position];
        }
    }else{
        [_timeScheduleView deleteMode:position];
        
    }
    
    
}

-(IBAction)saveButtonTapped:(id)sender{
    
    if (_lDay.hidden==NO){
        
        NSMutableDictionary * root = [[NSMutableDictionary alloc] init];
        
        if (_tempSchedule.serverId != nil)
            [root setObject:_tempSchedule.serverId forKey:@"id"];
        
        [root setObject:_tempSchedule.hysteresis forKey:@"hysteresis"];
        [root setObject:_tempSchedule.name forKey:@"label"];
        
        NSMutableDictionary * modes = [[NSMutableDictionary alloc] init];
        for (TempScheduleMode *mode in _tempSchedule.modes) {
            NSDictionary * minMax = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     mode.min,@"min",
                                     mode.max,@"max",nil];
            [modes setObject:minMax forKey:[mode.id stringValue]];
        }
        
        [root setObject:modes forKey:@"modes"];
        
        NSMutableArray* modesA;
        NSMutableDictionary* temp = [[NSMutableDictionary alloc] init];
        for (TempScheduleDay* day in _tempSchedule.days) {
            modesA = [[NSMutableArray alloc] init];
            
            for (TempDayMode *mode in day.modes) {
                NSDictionary * modeObj = [[NSDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithInt:[mode.endTime intValue]-[mode.startTime intValue]], @"duration",
                                          mode.mode , @"mode", nil];
                [modesA addObject:modeObj];
                
            }
            [temp setObject:modesA forKey:day.id];
            
        }
        [root setObject:temp forKey:@"schedule"];
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:root
                                                           options:(NSJSONWritingOptions)  (NSJSONWritingPrettyPrinted)
                                                             error:&error];
        
        if (! jsonData) {
            NSLog(@"bv_jsonStringWithPrettyPrint: error: %@", error.localizedDescription);
            
        } else {
            NSLog(@"%@",[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]);
        }
        _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
        [_loaderDialog showWithLabel:NSLocalizedString(@"temp_schedule_dialog_wait_tile", nil)];

        [[SYAPIManager sharedInstance] postScheduleWithDictionary:root success:^(AFHTTPRequestOperation * operation , id response){
            [_loaderDialog hide];
            NSLog(@"updated succesfully");
            [_container setViewController:@"timeSchedule"];

            [self dismissViewControllerAnimated:NO completion:nil];
        }
        failure:^(AFHTTPRequestOperation * operation , NSError * error){
            [_loaderDialog hide];
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                              message:NSLocalizedString(@"temp_schedule_failed_save", nil)
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];

            NSLog(@"update err");
        }];
    }else{
        
        [self copyDayToOthers];
        [_lCopyTo setText:NSLocalizedString(@"temp_schedule_copy_to", nil)];
        [_btnCopyTo setImage:[UIImage imageNamed:@"scene_add_img"] forState:UIControlStateNormal];
        [_lSave setText:NSLocalizedString(@"temp_schedule_dialog_save", nil) ];
        _lDay.hidden=NO;
        _leftArrowBtn.hidden=NO;
        _rightArrowBtn.hidden=NO;
        _lTimeScheduleName.hidden=NO;
        _DaysView.hidden=YES;
        [self handleDayChange:0];

        
    }
}





-(void) handleDayChange:(int) positionChange {
    _currentDayPosition = _currentDayPosition + positionChange;
    
    if (_currentDayPosition == 0) {
        _leftArrowBtn.hidden=YES;
    } else if (_currentDayPosition == [_tempSchedule.days count] - 1) {
        _rightArrowBtn.hidden=YES;
    } else {
        _leftArrowBtn.hidden=NO;
        _rightArrowBtn.hidden=NO;
    }
    
    [self refresActualDayFromView];
    
    _mDay = [_tempSchedule getDayByPosition:(int)_currentDayPosition];
    [_lDay setText:_res[_currentDayPosition]];

    [_timeScheduleView setDay:_mDay];
    
}

-(void) refresActualDayFromView{
    TempScheduleDay *day = [_timeScheduleView getDay];
    if(day != nil)
        _mDay.modes = day.modes;
}

- (IBAction)leftButtonTap:(id)sender {
    [self handleDayChange:-1];
}

- (IBAction)rightButtonTap:(id)sender {
        [self handleDayChange:1];
}

- (IBAction)copyToButtonTapped:(id)sender {
    
    if (_lDay.hidden==NO){
        [_lCopyTo setText: NSLocalizedString(@"back",nil)];
        [_btnCopyTo setImage:[UIImage imageNamed:@"sipka_zpet"] forState:UIControlStateNormal];
        _btnCopyTo.contentMode = UIViewContentModeRedraw;
        [_lSave setText:NSLocalizedString(@"temp_schedule_copy", nil) ];
        _lDay.hidden=YES;
        _leftArrowBtn.hidden=YES;
        _rightArrowBtn.hidden=YES;
        _lTimeScheduleName.hidden=YES;
        _DaysView.hidden=NO;
        _dayToCopy = [[NSMutableArray alloc] init];
        for (int i =0; i < 8; i++){
            [_dayToCopy addObject:[[NSNumber alloc] initWithBool:NO]];
        }
        
        for (UIView * view in [_DaysView subviews]){
            
            if (view.tag>=9){
                UIButton * button = (UIButton*)view;
                if (NO)
                    [button setImage:[UIImage imageNamed:@"checkmark_on"] forState:UIControlStateNormal];
                else{
                    [button setImage:[UIImage imageNamed:@"checkmark_off"] forState:UIControlStateNormal];
                    
                }
                [_dayToCopy replaceObjectAtIndex:view.tag-9 withObject:[[NSNumber alloc] initWithBool:NO]];
                
            }
        }

    }else{
        [_lCopyTo setText:NSLocalizedString(@"temp_schedule_copy_to", nil) ];
        [_btnCopyTo setImage:[UIImage imageNamed:@"scene_add_img"] forState:UIControlStateNormal];
        [_lSave setText:NSLocalizedString(@"temp_schedule_dialog_save", nil)];
        _lDay.hidden=NO;
        _leftArrowBtn.hidden=NO;
        _rightArrowBtn.hidden=NO;
        _lTimeScheduleName.hidden=NO;
        _DaysView.hidden=YES;
        [self handleDayChange:0];
    }
    
    
    
    
}

-(void) copyDayToOthers {

    TempScheduleDay *day;
    for (int i = 0; i < 8; i++) {
        
        if ([[_dayToCopy objectAtIndex:i] boolValue]==NO)
            continue;
        
        if (i != 0) {
            day = [_tempSchedule getDayByPosition:i-1];
            day.modes = [[NSMutableArray alloc] initWithArray:_mDay.modes];
        }
        
    }
}


- (IBAction)AllButtonTapped:(id)sender {
    BOOL valueToStore = NO;
    if ([[_dayToCopy objectAtIndex:0] boolValue]==NO){
        valueToStore =  YES;
    }
    
    if (valueToStore)
        [(UIButton*)sender setImage:[UIImage imageNamed:@"checkmark_on"] forState:UIControlStateNormal];
    else{
        [(UIButton*)sender setImage:[UIImage imageNamed:@"checkmark_off"] forState:UIControlStateNormal];
        
    }
    [_dayToCopy replaceObjectAtIndex:0 withObject:[[NSNumber alloc] initWithBool:valueToStore]];

    
    for (UIView * view in [[sender superview] subviews]){

        if (view.tag>=10){
            UIButton * button = (UIButton*)view;
            if (valueToStore)
                [button setImage:[UIImage imageNamed:@"checkmark_on"] forState:UIControlStateNormal];
            else{
                [button setImage:[UIImage imageNamed:@"checkmark_off"] forState:UIControlStateNormal];
                
            }
            [_dayToCopy replaceObjectAtIndex:view.tag-9 withObject:[[NSNumber alloc] initWithBool:valueToStore]];
            
        }
    }
    
}


- (IBAction)dayButtonTapped:(id)sender {
    UIButton * button = (UIButton*)sender;
    BOOL valueToStore = NO;
    if ([[_dayToCopy objectAtIndex:button.tag-9] boolValue]==NO){
        valueToStore =  YES;
    }
    
    if (valueToStore)
        [button setImage:[UIImage imageNamed:@"checkmark_on"] forState:UIControlStateNormal];
    else{
        [button setImage:[UIImage imageNamed:@"checkmark_off"] forState:UIControlStateNormal];        
    }
    [_dayToCopy replaceObjectAtIndex:button.tag-9 withObject:[[NSNumber alloc] initWithBool:valueToStore]];


}



@end
