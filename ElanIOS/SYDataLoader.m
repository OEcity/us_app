//
//  SYDataLoader.m
//  iHC-MIRF
//
//  Created by Marek Žehra on 16.04.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "SYDataLoader.h"
#import "SYAPIManager.h"
#import "Device+State.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "HCASchedule+Dictionary.h"
#import "DeviceTimeScheduleClass+Dictionary.h"
#import "MainMenuPVC.h"

@implementation SYDataLoader

+(SYDataLoader*) sharedInstance{
    
    static SYDataLoader *_sharedDataLoader = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        _sharedDataLoader = [[SYDataLoader alloc] init];
    });
    
    return _sharedDataLoader;
}

-(void)loadDataFromElan:(Elan*)elan{
    [[SYAPIManager sharedInstance] getConfigForElan:elan success:^(AFHTTPRequestOperation * operation, id response){
        
        Elan *dbElan = [[SYCoreDataManager sharedInstance] getELANwithMAC:elan.mac inContext: [[SYCoreDataManager sharedInstance] privateObjectContext]];
        
        NSDictionary * configDictionary = (NSDictionary*)response;
        NSNumber * remoteConfigurationChangesCounter = [configDictionary valueForKey:@"configuration changes counter"];
        NSNumber * localConfigurationChangesCounter = dbElan.configurationCounter;
        
        NSLog(@"load data from elan: type: %@, ws:%@",elan.type,elan.socketAddress);
        
        if (!localConfigurationChangesCounter || ![remoteConfigurationChangesCounter isEqualToNumber:localConfigurationChangesCounter]){
            [self loadConfigurationForElan:elan];
            [self loadDevicesForElan:elan];
            
            if(![elan.type containsString:@"IR"]){
                [self loadSHCASchedulesFromElan:elan];
                [self loadDeviceSchedulesFromElan:elan];
            }
            
        } else {
            if(![elan.type containsString:@"IR"]){
                [self updateDeviceStates];
            }
        }
        
    }failure:^(AFHTTPRequestOperation * operation, NSError * error){
        NSLog(@"Error loading configuration: %@",[error localizedDescription]);
    }];

}

#pragma mark - Configuration check

-(void)reloadDevicesForElan:(Elan*)elan {
//    [[SYAPIManager sharedInstance] reinit];
    
    if(!elan.selected.boolValue)return;
    [[SYAPIManager sharedInstance] getConfigForElan:elan success:^(AFHTTPRequestOperation * operation, id response){
        NSLog(@"Reload if needed triggered");
        NSDictionary * configDictionary = (NSDictionary*)response;
//        NSNumber * remoteConfigurationChangesCounter = [configDictionary valueForKey:@"configuration changes counter"];
//        NSNumber * localConfigurationChangesCounter = elan.configurationCounter;
        
        [self loadConfigurationForElan:elan];
        [self loadDevicesForElan:elan];
    }failure:^(AFHTTPRequestOperation * operation, NSError * error){
        NSLog(@"Error loading configuration: %@",[error localizedDescription]);
    }];
}

- (void)reloadIfNeeded {
    
    for(Elan*elan in [[SYCoreDataManager sharedInstance] getAllElans]){
        if(elan.selected.boolValue){
//            [[SYAPIManager sharedInstance] reinit];
            [[SYAPIManager sharedInstance] getConfigForElan:elan success:^(AFHTTPRequestOperation * operation, id response){
                
                NSLog(@"Got config from eLAN: %@", elan.label);
                
                NSDictionary * configDictionary = (NSDictionary*)response;
                NSNumber * remoteConfigurationChangesCounter = [configDictionary valueForKey:@"configuration changes counter"];
                NSNumber * localConfigurationChangesCounter = elan.configurationCounter;
                
                if (!localConfigurationChangesCounter || ![remoteConfigurationChangesCounter isEqualToNumber:localConfigurationChangesCounter]){
                    [self loadConfigurationForElan:elan];
                    [self loadDevicesForElan:elan];
                
                    if(![elan.type containsString:@"IR"]){
                        [self loadSHCASchedulesFromElan:elan];
                        [self loadDeviceSchedulesFromElan:elan];
                    }
                    
                } else {
                    if(![elan.type containsString:@"IR"])
                    [self updateDeviceStates];
                }
                
            }failure:^(AFHTTPRequestOperation * operation, NSError * error){
                NSLog(@"Error loading configuration: %@",[error localizedDescription]);
            }];
        }
    }
    
}

-(void)reloadFWVersion:(UIImageView *) myView{
    
      [[SYAPIManager sharedInstance]getDataFromURL:@"http://217.197.144.56:4434/fw/files/fw.info.json" success:^(AFHTTPRequestOperation *operation, id object) {
          
          
        Elan * myElan = [[[SYCoreDataManager sharedInstance] getCurrentElans]firstObject];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
          myElan.type = [defaults valueForKey:@"elanType"];

        NSDictionary * objectDict = (NSDictionary*)object;
        NSDictionary * info = [objectDict objectForKey:myElan.type];
        NSString * internetFWVersion = [info objectForKey:@"version"];
          
          
        NSLog(@"ReloadFWVersion: elanType: %@",myElan.type);
        NSLog(@"%@",internetFWVersion);
        
        
        NSArray * internetVersion = [internetFWVersion componentsSeparatedByString:@"."];
        NSArray *elanVersion = [myElan.firmware componentsSeparatedByString:@"."];
        
        myElan.update = [[NSNumber alloc] initWithBool:NO];
        myView.hidden = true;
        
        if([internetVersion count] == [elanVersion count]){
            for (int i = 0; i<[internetVersion count]; i++) {
                if([internetVersion[i] integerValue] > [elanVersion[i]integerValue]){
                    myElan.update = [[NSNumber alloc]initWithBool:YES];
                    myView.hidden = false;
                    break;
                } else if ([internetVersion[i] integerValue] < [elanVersion[i]integerValue]){
                    break;
                }
            }
        }

        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Bez internetu");
    }];
}

-(BOOL)isIRDevice:(NSDictionary*)dict{
    
    BOOL res = [[[dict objectForKey:@"device info"] objectForKey:@"product type"] isEqualToString:@"IR"];
 
    return res;
}

- (BOOL)isDictHCADevice:(NSDictionary*)dict {
    NSPredicate * pred = [NSPredicate predicateWithFormat:@"NOT('address' IN %@)AND('on' IN %@ OR 'power' IN %@) AND 'correction' IN %@",
                          [[dict objectForKey:@"device info"] allKeys],
                          [[dict objectForKey:@"actions info"] allKeys],
                          [[dict objectForKey:@"actions info"] allKeys],
                          [[dict objectForKey:@"actions info"] allKeys]];
    BOOL res = [pred evaluateWithObject:dict];
    return res;
}


#pragma mark - Loading data from API
- (void)loadDevicesForElan:(Elan*)elan {
    
    NSLog(@"Loading devices for eLAN: %@", elan.label);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_DEVICES object:self];
    // UPDATE or CREATE device
    [[SYAPIManager sharedInstance] getDevicesWithSuccessForElan:elan success:^(AFHTTPRequestOperation* operation, id object){
        NSLog(@"got devices with success");
        __block BOOL first = YES;
        NSString * predicateString = @"";
        __block int index =0;
        __block NSUInteger amout = [[object allKeys] count];
        if(amout == 0){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"pairingRooms" object:nil];
            return;
        }
        for (NSString* deviceID in (NSDictionary*)object){
            predicateString = [predicateString stringByAppendingString:[NSString stringWithFormat: first?@"deviceID != '%@'":@" AND deviceID != '%@'", deviceID]];
            first = NO;
            [[SYAPIManager sharedInstance] getDeviceWithID:deviceID fromElan:elan success:^(AFHTTPRequestOperation * operation, id object){
                index++;
                //check if label is null
                if (![[(NSDictionary*)object valueForKey:@"device info"] valueForKey:@"label"]){
                    return;
                }
                
                Device * device = [[SYCoreDataManager sharedInstance] getDeviceWithID:[object valueForKey:@"id"] inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
                if (device == nil){
                    if ([self isDictHCADevice:object]){
                        device = [[SYCoreDataManager sharedInstance] createEntityHeatCoolArea];
                    } else if( [self isIRDevice:object]){
                        device = [[SYCoreDataManager sharedInstance] createEntityIRDevice];
                    } else {
                        device = [[SYCoreDataManager sharedInstance] createEntityDevice];
                        
                    }
                }
                NSString *elanMac = elan.mac;
                
                Elan *myElan = [[SYCoreDataManager sharedInstance] getELANwithMAC:elanMac inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
                
                [device updateWithDictionary:(NSDictionary*)object];
                
                [device setElan:myElan];
                
                if (index==amout){
                    [[SYCoreDataManager sharedInstance] saveContext];
                    NSLog(@"elan type: %@, myelan %@", elan.type, myElan.type);
                    if([elan.type containsString:@"IR"]){
                        [self pairIRDevicesFromElan:myElan];
                    }else {
                        [self loadRoomsFromElan:elan];
                        [self updateDeviceStates];
                    }
                    [self loadScenesFromElan:elan];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self];
                    [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_DEVICES_ENDED object:self];
                }
                [[SYCoreDataManager sharedInstance]  saveContext];
            } failure:^(AFHTTPRequestOperation * operation, NSError * error){
                NSLog(@"Cant't load device: %@", deviceID);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self];
                [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_DEVICES_ENDED object:self];
            }];
            
        }
        if (amout == 0){
            [self loadRoomsFromElan:elan];
            [self loadScenesFromElan:elan];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_DEVICES_ENDED object:self];
        }
        
        // DELETE devices not included on server
        if (![predicateString isEqualToString:@""]){
            NSPredicate * objectsToDeletePredicate = [NSPredicate predicateWithFormat:predicateString];
            NSArray * toBeDeletedDevices = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Device"
                                                                                     withPredicate:objectsToDeletePredicate
                                                                                    sortDescriptor:nil
                                                                                         inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]                                           ];
            for (Device * device in toBeDeletedDevices){
                if([device.elan.mac isEqualToString:elan.mac]){
                    [[SYCoreDataManager sharedInstance] deleteManagedObject:device];
                }
            }
            [[SYCoreDataManager sharedInstance] saveContext];
        }
    } failure:^(AFHTTPRequestOperation * operation, NSError * error){
        NSLog(@"cant load devices");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_DEVICES_ENDED object:self];
    }];
}

- (void)loadSHCASchedulesFromElan:(Elan *)elan {
    [[SYAPIManager sharedInstance] getTemperatureSchedulesWithSuccessFromElan:elan success:^(AFHTTPRequestOperation *operation, id response) {
        __block BOOL first = YES;
        NSString * predicateString = @"";
        __block int index =0;
        __block NSUInteger amout = [[response allKeys] count];
        for (NSString* scheduleID in (NSDictionary*)response){
            predicateString = [predicateString stringByAppendingString:[NSString stringWithFormat: first?@"heattimescheduleID != '%@'":@" AND heattimescheduleID != '%@'", scheduleID]];
            first = NO;
            [[SYAPIManager sharedInstance] getScheduleWithID:scheduleID fromElan:elan success:^(AFHTTPRequestOperation * operation, id object){
                index++;
                //check if label is null
                if (![(NSDictionary*)object valueForKey:@"label"]){
                    return;
                }
                HeatTimeSchedule * schedule = [[SYCoreDataManager sharedInstance] getScheduleWithID:[object valueForKey:@"id"] inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
                if (schedule == nil){
                    schedule = [[SYCoreDataManager sharedInstance] createEntityHCASchedule];
                    
                    Elan *myElan = [[SYCoreDataManager sharedInstance] getELANwithMAC:elan.mac inContext:schedule.managedObjectContext];
                    schedule.elan = myElan;
                }
                [schedule updateWithDictionary:(NSDictionary*)object];
                
                // DELETE schedules not included on server
                if (![predicateString isEqualToString:@""]){
                    NSPredicate * objectsToDeletePredicate = [NSPredicate predicateWithFormat:predicateString];
                    NSArray * toBeDeletedDevices = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"HeatTimeSchedule"
                                                                                             withPredicate:objectsToDeletePredicate
                                                                                            sortDescriptor:nil
                                                                                                 inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]                                           ];
                    for (HeatTimeSchedule * hcaSchedule in toBeDeletedDevices){
                        if([hcaSchedule.elan.mac isEqualToString:elan.mac]){
                            [[SYCoreDataManager sharedInstance] deleteManagedObject:hcaSchedule];
                        }
                    }
                    [[SYCoreDataManager sharedInstance] saveContext];
                }
            } failure:^(AFHTTPRequestOperation * operation, NSError * error){
                NSLog(@"Cannot update dictionary for schedules");
            }];
            
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

-(void)loadDeviceSchedulesFromElan:(Elan *)elan {
    [[SYAPIManager sharedInstance] getDeviceSchedulesWithSuccessFromElan:(Elan*)elan success:^(AFHTTPRequestOperation *operation, id response) {
        __block BOOL first = YES;
        NSString * predicateString = @"";
        __block int index =0;
        NSMutableArray*myArray = [[NSMutableArray alloc] init];
        __block NSUInteger amout = [[response allKeys] count];
        for (NSString* scheduleID in (NSDictionary*)response){
            predicateString = [predicateString stringByAppendingString:[NSString stringWithFormat: first?@"devicetimescheduleclassID != '%@'":@" AND devicetimescheduleclassID != '%@'", scheduleID]];
            [myArray addObject:scheduleID];
            first = NO;
            [[SYAPIManager sharedInstance] getDeviceScheduleDetailsWithName:scheduleID fromElan:elan success:^(AFHTTPRequestOperation * operation, id object){
                index++;
                //check if label is null
                if (![(NSDictionary*)object valueForKey:@"label"]){
                    return;
                }
                DeviceTimeScheduleClass * schedule = [[SYCoreDataManager sharedInstance] getDeviceScheduleWithID:[object valueForKey:@"id"] inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
                if (schedule == nil){
                    schedule = [[SYCoreDataManager sharedInstance] createEntityDeviceSchedule];
                    
                    Elan *myElan = [[SYCoreDataManager sharedInstance] getELANwithMAC:elan.mac inContext:schedule.managedObjectContext];
                    schedule.elan = myElan;
                }
                [schedule updateWithDictionary:(NSDictionary*)object];
                
                // DELETE schedules not included on server
                if (![predicateString isEqualToString:@""]){
                    NSPredicate * objectsToDeletePredicate = [NSPredicate predicateWithFormat:predicateString];
                    NSArray * toBeDeletedDevices = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"DeviceTimeScheduleClass"
                                                                                             withPredicate:objectsToDeletePredicate
                                                                                            sortDescriptor:nil
                                                                                                 inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]                                           ];
                    for (DeviceTimeScheduleClass * schedule in toBeDeletedDevices){
                        if([myArray containsObject:schedule.devicetimescheduleclassID]){
                            continue;
                        }
                        if([schedule.elan.mac isEqualToString:elan.mac]){
                            [[SYCoreDataManager sharedInstance] deleteManagedObject:schedule];
                        }
                    }
                    [[SYCoreDataManager sharedInstance] saveContext];
                }
            } failure:^(AFHTTPRequestOperation * operation, NSError * error){
                NSLog(@"Cannot update dictionary for schedules");
            }];
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
    }];
}

- (void)loadRoomsFromElan:(Elan*)elan{
   
    // UPDATE or CREATE room
    [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_ROOMS object:self];
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.loadingDevices = YES;
    
    [[SYAPIManager sharedInstance] getRoomsWithSuccessFromElan:(Elan*)elan success:^(AFHTTPRequestOperation* operation, id object){
        __block BOOL first = YES;
        NSString * predicateString = @"";
        __block NSUInteger amout = [[object allKeys] count];
        __block int index =0;
        for (NSString* roomID in (NSDictionary*)object){
            predicateString = [predicateString stringByAppendingString:[NSString stringWithFormat: first?@"roomID != '%@'":@" AND roomID != '%@'", roomID]];
            first = NO;
            [[SYAPIManager sharedInstance] getRoomWithID:roomID fromElan:elan success:^(AFHTTPRequestOperation * operation, id object){
                index++;
                Room * room = [[SYCoreDataManager sharedInstance] getRoomWithID:[object valueForKey:@"id"] inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
                if (room == nil)
                    room = [[SYCoreDataManager sharedInstance] createEntityRoom];
                [room updateWithDictionary:(NSDictionary*)object];
                Elan*myElan = [[SYCoreDataManager sharedInstance] getELANwithMAC:elan.mac inContext: room.managedObjectContext];
                room.elan = myElan;
                
                [[SYCoreDataManager sharedInstance] saveContext];
                
                if (index==amout){
                    app.loadingDevices = NO;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadingRoomsFinished" object:self];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self];
                    [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_ROOMS_ENDED object:self];
                }
            } failure:^(AFHTTPRequestOperation * operation, NSError * error){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadingRoomsFailed" object:self];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self];
                [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_ROOMS_ENDED object:self];
                app.loadingDevices = NO;
            }];
        }
        // DELETE rooms not included on server
        if (![predicateString isEqualToString:@""]){
            NSPredicate * objectsToDeletePredicate = [NSPredicate predicateWithFormat:predicateString];
            NSArray * toBeDeletedRooms = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Room"
                                                                                   withPredicate:objectsToDeletePredicate
                                                                                  sortDescriptor:nil
                                                                                       inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
            for (Room * room in toBeDeletedRooms){
                if([room.elan.mac isEqualToString:elan.mac]){
                    [[SYCoreDataManager sharedInstance] deleteManagedObject:room];
                }
            }
            [[SYCoreDataManager sharedInstance] saveContext];
        }
        
        if (amout == 0){
            app.loadingDevices = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadingRoomsFinished" object:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_ROOMS_ENDED object:self];
        }
    } failure:^(AFHTTPRequestOperation * operation, NSError * error){
        app.loadingDevices = NO;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LoadingRoomsFailed" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"dataSyncComplete" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_ROOMS_ENDED object:self];
    }];
}

- (void)loadConfigurationForElan:(Elan*)elan {
    [[SYAPIManager sharedInstance] getConfigForElan:elan success:^(AFHTTPRequestOperation * operation, id response){
        [self updateConfigurationWithDictionary:(NSDictionary*)response elan:elan];
    }failure:^(AFHTTPRequestOperation * operation, NSError * error){
        NSLog(@"Error loading configuration: %@",[error localizedDescription]);
    }];
}

- (void)updateDeviceStates {
    [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_STATES object:self];
    _devices = [[SYCoreDataManager sharedInstance] getAllDevices];
    NSLog(@"Updating states for %lu devices", (unsigned long)[_devices count]);
    _statesToDownload = (int)[_devices count];
    for (Device* device in _devices){
        if (!device || !device.deviceID || [device.elan.type containsString:@"IR"]){
            NSLog(@"device not updated");
            [self loadingStatesEnded];
            continue;
        }
        [self updateStateForDeviceID:device.deviceID fromElan:device.elan];
    }
}

-(void) loadingStatesEnded{
    _statesToDownload--;
    if (_statesToDownload <0){
        [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_STATES_ENDED object:self];
    }
}

- (void)updateStateForDeviceID:(NSString*)deviceID fromElan:(Elan*)elan{

    [[SYAPIManager sharedInstance] getDeviceState:deviceID fromElan:elan success:^(AFHTTPRequestOperation * operation, id object){
        NSLog(@"reloading state device: %@",deviceID);
        BOOL takeNext = NO;
        NSString * deviceID;
        for (NSString * pathComponent in [operation.response.URL pathComponents]){
            if (takeNext){
                deviceID = pathComponent;
                break;
            }
            if ([pathComponent isEqualToString:@"devices"]){
                takeNext = YES;
            }
        }
        Device * fetchedDevice = [[SYCoreDataManager sharedInstance] getDeviceWithID:deviceID inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        for (NSString * key in (NSDictionary*)object){
            NSString * stateName = key;
            id stateValue = [object valueForKey:key];
            if ([stateName isEqualToString:@"locked"]){
                if (stateValue==[NSNumber numberWithBool:YES]){
                    [fetchedDevice setStateValue:stateValue forName:stateName];
                } else {
                    [fetchedDevice removeStateForName:stateName];
                }
            }else {
                [fetchedDevice setStateValue:stateValue forName:stateName];
            }
        }
        [[SYCoreDataManager sharedInstance] saveContext];
        [self loadingStatesEnded];
    } failure:^(AFHTTPRequestOperation * operation, NSError* error){
        NSLog(@"Failed loading state!");
        [self loadingStatesEnded];
    }];
}


- (void)loadScenesFromElan:(Elan*)elan {
    // UPDATE or CREATE scene
    //    [[SYCoreDataManager sharedInstance] deleteAllSceneActions];
    //    [[SYCoreDataManager sharedInstance] saveContext];
    [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_SCENES object:self];
    [[SYAPIManager sharedInstance] getScenesWithSuccessFromElan:(Elan*)elan success:^(AFHTTPRequestOperation *operation, id object) {
        
        __block BOOL first = YES;
        NSString * predicateString = @"";
        __block NSUInteger amout = [[object allKeys] count];
        __block int index =0;
        NSMutableArray * deleteSceneActionsString = [NSMutableArray new];
        for (NSString* sceneID in (NSDictionary*)object){
            predicateString = [predicateString stringByAppendingString:[NSString stringWithFormat: first?@"sceneID != '%@'":@" AND sceneID != '%@'", sceneID]];
            first = NO;
            //            __block NSUInteger amout = [[object allKeys] count];
            [[SYAPIManager sharedInstance] getSceneWithID:sceneID fromElan:(Elan*)elan success:^(AFHTTPRequestOperation * operation, id object){
                index++;
                NSDictionary *dictonary = (NSDictionary*)object;
                Scene *scene = [[SYCoreDataManager sharedInstance] getSceneWithID:[dictonary valueForKey:@"id"] inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
                if (scene == nil){
                    scene = [[SYCoreDataManager sharedInstance] createEntityScene];
                }
                Elan *sceneElan = [[SYCoreDataManager sharedInstance] getELANwithMAC:elan.mac inContext:scene.managedObjectContext];
                [scene setElan:sceneElan];
                [scene setLabel:[dictonary valueForKey:@"label"]];
                [scene setSceneID:[dictonary valueForKey:@"id"]];
                for (NSDictionary *action in [dictonary valueForKey:@"actions"]){
                    Device * device = [[SYCoreDataManager sharedInstance] getDeviceWithID:[[action allKeys] firstObject] inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
                    for (NSDictionary * a in [action allValues]){
                        for  (NSString *key in a){
                            NSString * actionName = key;
                            NSString * actionValue = [a valueForKey:key];
                            
                            Action *actionEntity = [[SYCoreDataManager sharedInstance]
                                                    getActionWithName:actionName
                                                    withDeviceID:[device deviceID]
                                                    inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
                            
                            SceneAction *sceneAction = [[SYCoreDataManager sharedInstance] getSceneActionWithSceneID:scene.sceneID withDeviceID:device.deviceID withActionName:actionName inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
                            
                            if (sceneAction == nil){
                                sceneAction = [[SYCoreDataManager sharedInstance] createEntitySceneAction];
                                [sceneAction setScene: scene];
                                [sceneAction setDevice: device];
                                [sceneAction setAction: actionEntity];
                            }
                            //                    [actionEntity setName:[(NSDictionary*)[action allValues][0] allKeys][0]];
                            [sceneAction setValue:actionValue];
                            [deleteSceneActionsString addObject:sceneAction];
                        }
                    }
                }
                [[SYCoreDataManager sharedInstance] saveContext];
                if (index == amout){
                    [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_SCENES_ENDED object:self];
                    
                }
            } failure:^(AFHTTPRequestOperation * operation, NSError * error){
                
                [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_SCENES_ENDED object:self];
            }];
        }
        //         DELETE scenes not included on server
        NSPredicate * objectsToDeletePredicate =nil;
        if (![predicateString isEqualToString:@""]){
            objectsToDeletePredicate = [NSPredicate predicateWithFormat:predicateString];
        }
        NSArray * toBeDeletedRooms = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Scene"
                                                                               withPredicate:objectsToDeletePredicate
                                                                              sortDescriptor:nil
                                                                                   inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        for (Scene * scene in toBeDeletedRooms){
            if([scene.elan.mac isEqualToString:elan.mac]){
                [[SYCoreDataManager sharedInstance] deleteManagedObject:scene];
            }
        }
        [[SYCoreDataManager sharedInstance] saveContext];
        
        
        for (SceneAction* sc in [[SYCoreDataManager sharedInstance] getAllObjectsForEntity:@"SceneAction"]){
            if (![deleteSceneActionsString containsObject:sc]){
                [[SYCoreDataManager sharedInstance] deleteManagedObject:sc];
            }
        }
        [[SYCoreDataManager sharedInstance] saveContext];
        if (amout == 0){
            
            [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_SCENES_ENDED object:self];
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:LOADING_SCENES_ENDED object:self];
    }];
}

#pragma mark - Parsing responses

- (void)updateConfigurationWithDictionary:(NSDictionary*)config elan:(Elan*)elan{
    
    NSLog(@"Updating config for eLAN: %@", elan.label);
    
    NSNumber * remoteConfigurationChangesCounter = [config valueForKey:@"configuration changes counter"];
    NSNumber * localConfigurationChangesCounter = elan.configurationCounter;
    
    if (!localConfigurationChangesCounter || [remoteConfigurationChangesCounter doubleValue] > [localConfigurationChangesCounter doubleValue]){
        NSString* limits = [config valueForKey:@"limits"];
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        //        [[SYAPIManager sharedInstance] getDataFromURL:elanLabel success:^(AFHTTPRequestOperation *operation, id object) {
        //            [[SYCoreDataManager sharedInstance] getCurrentElan].label=[(NSDictionary*)object valueForKey:@"ELAN label"];
        //            [[SYCoreDataManager sharedInstance] saveContext];
        //        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        //        }];
        
        
        if(limits){
            [[SYAPIManager sharedInstance] getDataFromURL:limits success:^(AFHTTPRequestOperation *operation, id object) {
                NSDictionary *dict = (NSDictionary*) object;
                [defaults setObject:dict forKey:@"limits"];
                [defaults synchronize];
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            }];
        }
        
        elan.configurationCounter = remoteConfigurationChangesCounter;
        [[SYCoreDataManager sharedInstance] saveContext];
    }
}

- (void)rebuildDatabase {
#warning TODO implement
}

- (void)loadWeatherFromElan:(Elan*)elan{
    [[SYAPIManager sharedInstance] getWeatherFromElan:elan success:^(AFHTTPRequestOperation *operation, id response) {
        AppDelegate *app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        MainMenuPVC *menu = app.mainMenuPVC;
        
        if([response objectForKey:@"source"] != nil){
            
            if([[response objectForKey:@"source"] containsString:@"openweather"]){
                [[NSUserDefaults standardUserDefaults] setObject:[response objectForKey:@"coord"] forKey:@"weatherCoords"];
                [[NSUserDefaults standardUserDefaults] setObject:elan.mac forKey:@"weatherElanMac"];
                app.hasOPWeather = YES;
                app.hasGIOMWeather = NO;
            } else {
                app.hasOPWeather = NO;
                app.hasGIOMWeather = YES;
            }
            
        } else {
            app.hasGIOMWeather = app.hasOPWeather = NO;
        }
        
        [menu refreshPVC:@"laode weahter from elan"];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"error loading weather: %@", [error localizedDescription]);
    }];
}

-(void)pairIRDevicesFromElan:(Elan*)elan{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pairingRooms" object:nil];
    for(IRDevice*device in [[SYCoreDataManager sharedInstance] getAllIRDevices]){
        if([device.elan.mac isEqualToString:elan.mac]){
            Room *room = [[SYCoreDataManager sharedInstance] getRoomWithID:[NSString stringWithFormat:@"roomTitle:%@", elan.mac]];
            if(room == nil){
                room = [[SYCoreDataManager sharedInstance] createEntityRoom];
                room.label = @"roomTitle";
                room.roomID = [NSString stringWithFormat:@"roomTitle:%@", elan.mac];
                room.type = @"living room";
                [[SYCoreDataManager sharedInstance] saveContext];
            }
            struct SYDevicePosition position;
                position.coordX = 0.0;
                position.coordY = 0.0;

            [[SYCoreDataManager sharedInstance] pairDevice:device andRoom:room withCoordinates:position];
        }
    }
}

@end
