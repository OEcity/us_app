//
//  Device+Action.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 4/20/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "Device+Action.h"

@implementation Device (Action)


- (BOOL) hasActionWithName:(NSString*)actionName{
    NSSet * filteredStatesSet = [self.actions filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"self.name MATCHES %@", actionName]];
    if ([filteredStatesSet count] > 0) {
        return YES;
    }
    return NO;
}

- (id)getActionForActionName:(NSString*)actionName{

    NSSet * filteredStatesSet = [self.actions filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"self.name MATCHES %@", actionName]];
    
    if ([filteredStatesSet count] == 1) {
        Action * action = [filteredStatesSet anyObject];
        
        return action;
    }
    
    return nil;
    
}

@end
