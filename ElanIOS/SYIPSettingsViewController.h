//
//  SYIPSettingsViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 6/26/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "HUDWrapper.h"
#import "Elan.h"
#import "SYServerInputViewController.h"


@interface SYIPSettingsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, SYServerInputViewControllerDelegate,UIAlertViewDelegate>
@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) Elan * currentElan;
-(IBAction)backToSettings:(id)sender;
-(IBAction)addServer:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *ipSettingsLabel;
@property (weak, nonatomic) IBOutlet UIButton *bottomRightMenu;
@property (weak, nonatomic) IBOutlet UIButton *bottomLeftMenu;
@property (nonatomic, retain) HUDWrapper * loaderDialog;


@end
