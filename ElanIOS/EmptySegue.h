//
//  EmptySegue.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/21/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
@interface EmptySegue : UIStoryboardSegue
@property (nonatomic, retain) AppDelegate* appDelegate;
@end
