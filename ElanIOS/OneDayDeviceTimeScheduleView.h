//
//  OneDayDeviceTimeScheduleView.h
//  iHC-MIRF
//
//  Created by admin on 10.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DeviceScheduleDay.h"

@protocol OneDayDeviceTimeScheduleViewDataSource <NSObject>
@optional
-(NSString *)getPercentOfColor:(NSInteger)index;
@optional
-(UIColor *)getColorForPosintion:(NSInteger)position;
@optional
-(BOOL)shouldShowImage;

@end


@interface OneDayDeviceTimeScheduleView : UIView

@property (nonatomic, retain) DeviceScheduleDay * mDay;

@property (nonatomic, weak) id <OneDayDeviceTimeScheduleViewDataSource> dataSource;


-(void) repaintDay:(DeviceScheduleDay*) day;


@end
