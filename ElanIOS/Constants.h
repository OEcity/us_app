//
//  Constants.h
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 19/05/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#ifndef iHC_MIRF_Constants_h
#define iHC_MIRF_Constants_h

#define PREF_SHOW_FAVOURITES_AFTER_BOOT "PREF_SHOW_FAVOURITES"
#define LOADING_DEVICES @"LOADING_DEVICES"
#define LOADING_DEVICES_ENDED @"LOADING_DEVICES_ENDED"
#define LOADING_ROOMS @"LOADING_ROOMS"
#define LOADING_ROOMS_ENDED @"LOADING_ROOMS_ENDED"
#define LOADING_SCENES @"LOADING_SCENES"
#define LOADING_SCENES_ENDED @"LOADING_SCENES_ENDED"
#define LOADING_STATES @"LOADING_STATES"
#define LOADING_STATES_ENDED @"LOADING_STATES_ENDED"
#define LOADING_DATA_ENDED @"LOADING_DATA_ENDED"
#define USBlueColor [UIColor colorWithRed:0/255.0f green:192/255.0f blue:243/255.0f alpha:1]
#define USGrayColor [UIColor colorWithRed:109/255.0f green:110/255.0f blue:113/255.0f alpha:1]
#define USUtlumColor [UIColor colorWithRed:147/255.0f green:162/255.0f blue:108/255.0f alpha:1]
#define USNormalColor [UIColor colorWithRed:245/255.0f green:134/255.0f blue:60/255.0f alpha:1]
#define USKomfrotColor [UIColor colorWithRed:238/255.0f green:45/255.0f blue:38/255.0f alpha:1]
#define INTERCOM_ENABLED @"intercomEnabled"

#endif

