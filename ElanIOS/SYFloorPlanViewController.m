//
//  FloorPlanViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/28/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYFloorPlanViewController.h"
#import "FloorPlanLoader.h"
#import "FloorPlanDetailResponse.h"
#import "SYFloorplanDetailViewController.h"
#import "SYConfigurationMainViewController.h"
#import "SYAPIManager.h"
#import <UIImageView+AFNetworking.h>
@interface SYFloorPlanViewController ()


@end

@implementation SYFloorPlanViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.receivedData = [[NSMutableData alloc] init];
    
    self.emptyLabel.text = NSLocalizedString(@"noFloorplans", nil);

    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    
    [[SYAPIManager sharedInstance] getFloorplansWithSuccess:^(AFHTTPRequestOperation * operation, id object)
    {
        NSDictionary * allDicts = (NSMutableDictionary*)object;
        _floorplans = [[NSMutableArray alloc] init];
        for (NSString* floorplan in allDicts){
            Floorplan * floorplanObject= [[Floorplan alloc] init];
            floorplanObject.name = floorplan;
            floorplanObject.url = [allDicts valueForKey: floorplan];;
            [_floorplans addObject:floorplanObject];
        }
        [_tableView reloadData];
        [_loaderDialog hide];
        if (!_floorplans || [_floorplans count]==0){
            self.emptyLabel.hidden = NO;
        } else {
            self.emptyLabel.hidden = YES;
        }


    } failure:^(AFHTTPRequestOperation * operation, NSError * error){
//        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString( @"errorFloorplan",nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
        self.emptyLabel.hidden = self.floorplans.count > 0;
        [_loaderDialog hide];

    }];
    
    self.loading = [[NSMutableArray alloc] init];
    self.actInds = [[NSMutableDictionary alloc] init];


}
-(void)viewWillAppear:(BOOL)animated{
    [_leftBottomButton setTitle:NSLocalizedString(@"back", nil) forState:UIControlStateNormal];
    [_rightBottomButton setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
    
    
}

-(void) viewWillDisappear:(BOOL)animated{
    if (self.tableView.editing){
        self.tableView.editing = NO;
    }
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.floorplans count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"MyCell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }

    Floorplan *floorplan = [_floorplans objectAtIndex:indexPath.row];
    UILabel * deviceName = (UILabel*)[cell viewWithTag:1];
    UIImageView * planImage = (UIImageView*)[cell viewWithTag:2];
    UIActivityIndicatorView * actiInd =(UIActivityIndicatorView*)[cell viewWithTag:5];
    if (floorplan.image==nil){

        BOOL dismiss = NO;
        for (NSString* loading in self.loading){
            if ([floorplan.url isEqualToString:loading]){
                dismiss=YES;
            }
        }
        if (dismiss==NO){
            
            [self.loading addObject:floorplan.url];
            NSMutableDictionary * dict = [[NSMutableDictionary alloc] initWithObjectsAndKeys:floorplan.url, @"url", actiInd, @"ind", nil];
            [self.actInds setValue:actiInd forKey:floorplan.url];
            
//            [planImage setImageWithURL:[[NSURL alloc] initWithString:floorplan.url]];
            actiInd.hidden=YES;
            
//            NSInvocationOperation *invocationOperation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(loadDataAsychn:) object:dict];
//            [self.operationQueue addOperation:invocationOperation];
            [self loadDataAsychn:dict];

//            [[SYAPIManager sharedInstance] getDataFromURL:floorplan.url success:^(AFHTTPRequestOperation *operation, id response) {
//                UIImage * image = (UIImage* )response;
//
//            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            }];
        }
    }else{
        [planImage setImage:floorplan.image];
    }
    deviceName.text = floorplan.name;

    [[cell viewWithTag:99] removeFromSuperview];

    // Setting different views for different type of devices

    return cell;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    Floorplan *floorplan = [self.floorplans objectAtIndex:indexPath.row];
    [self.containerViewController setFloorplan:floorplan];
    [self.containerViewController setViewController:@"addFloorplan"];

}


-(IBAction)nextStep:(id)sender{
    [self.containerViewController setFloorplan:nil];
    [self.containerViewController setViewController:@"addFloorplan"];
}
-(IBAction)back:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
    
}


-(IBAction)button1Tapped:(id)sender
{
    UIView *view = sender;
    while (![view isKindOfClass:[UITableViewCell class]]) {
        view = [view superview];
    }

    NSIndexPath* pathOfTheCell = [self.tableView indexPathForCell:(UITableViewCell*)view ];

    NSInteger rowOfTheCell = [pathOfTheCell row];

    UIImage * imageData = ((Floorplan*)[self.floorplans objectAtIndex:rowOfTheCell]).image;
//    [self.containerViewController setFloorPlanImageData:imageData];
   // [self.containerViewController setViewController:@"floorplanDetail"];

   

   SYConfigurationMainViewController* cfgMain = (SYConfigurationMainViewController*) [self.containerViewController parentViewController];
    if (imageData!=nil)
        [cfgMain setImage:imageData];
    else{
        [cfgMain setImage:nil];
        [cfgMain setFloorPlanURL:((Floorplan*)[self.floorplans objectAtIndex:rowOfTheCell]).url];

    }
    [cfgMain performSegueWithIdentifier:@"floorplanDetail" sender:self];


}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return YES if you want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Floorplan * floorplan= ((Floorplan*)[self.floorplans objectAtIndex:indexPath.row]);

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_floorplan", nil), floorplan.name] delegate:self cancelButtonTitle:NSLocalizedString(@"confirmation.no", nil) otherButtonTitles:NSLocalizedString(@"confirmation.yes", nil) ,nil];
        alert.tag = indexPath.row;
        [alert show];
    }
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 1) {
        NSIndexPath * indexPath = [NSIndexPath indexPathForRow:alertView.tag inSection:0];
        self.deletedIndex = indexPath;
        Floorplan * floorplan= ((Floorplan*)[self.floorplans objectAtIndex:indexPath.row]);
        [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
        [[SYAPIManager sharedInstance] deleteFloorplanWithPath:floorplan.name success:^(AFHTTPRequestOperation * operation, id object)
        {
            [self.floorplans removeObjectAtIndex:self.deletedIndex.row];
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:self.deletedIndex] withRowAnimation:UITableViewRowAnimationAutomatic];
            [_loaderDialog hide];
        }failure:^(AFHTTPRequestOperation * operation, NSError * error)
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"" message:NSLocalizedString(@"cannotDeleteFloorplan",nil)  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            [_loaderDialog hide];

        }];
    }
}

-(void) connectionResponseOK:(NSObject *)returnedObject response:(id<Response>)responseType{
    if ([responseType isKindOfClass:[FloorPlanDetailResponse class]]){
        Floorplan * floorplan = (Floorplan*) returnedObject;
        for (Floorplan* floor in self.floorplans){
            if ([floor.url isEqualToString:floorplan.url]){
                floor.image=floorplan.image;
                [self.tableView reloadData];
            }
        }
    }else{
    }

}



-(void)connection:(NSURLConnection *)connection didReceiveResponse:
(NSURLResponse *)response
{
    // Discard all previously received data.
    [self.receivedData setLength:0];
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    self.statusCode = [httpResponse statusCode];
    self.responseURL = [response.URL absoluteString];

}

-(void)connection:(NSURLConnection *)connection didReceiveData:
(NSData *)data
{
    // Append the new data to the receivedData.
    [self.receivedData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{

    FloorPlanDetailResponse * itemsResponse = [[FloorPlanDetailResponse alloc] init];
    NSObject * returnedObject = [itemsResponse parseResponse:self.receivedData identifier:self.responseURL];
    Floorplan * floorplan = (Floorplan*) returnedObject;
    for (Floorplan* floor in self.floorplans){
        if ([floor.url isEqualToString:floorplan.url]){
            floor.image=floorplan.image;
            [self.tableView reloadData];
        }
    }

    self.emptyLabel.hidden = self.floorplans.count == 0;
}


-(void)loadDataAsychn:(NSMutableDictionary*)dict
{
    NSString * imageURl = [dict objectForKey:@"url"];
    UIActivityIndicatorView * actInd = [dict objectForKey:@"ind"];

    NSData *imageData=[[NSData alloc] initWithContentsOfURL: [NSURL URLWithString:imageURl]];
    UIImage *image=[[UIImage alloc]initWithData:imageData];
    NSMutableDictionary* imgDict = [[NSMutableDictionary alloc] init];
    [imgDict setValue:image forKey:@"image"];
    [imgDict setValue:actInd forKey:@"ind"];
    [imgDict setValue:imageURl forKey:@"imageNo"];
    [self performSelectorOnMainThread:@selector(showImage:) withObject:imgDict waitUntilDone:NO];
}
-(void)showImage:(NSMutableDictionary *)Dict
{
    NSString *url = [Dict objectForKey:@"imageNo"];
    UIActivityIndicatorView * actInd = [self.actInds objectForKey:url];
    [actInd stopAnimating];
    actInd.hidden=YES;
    for (Floorplan* floor in self.floorplans){
        if ([floor.url isEqualToString:url]){
            floor.image=[Dict objectForKey:@"image"];
            [self.tableView reloadData];
        }
    }
}
- (NSUInteger)supportedInterfaceOrientations
{

    return UIInterfaceOrientationMaskPortrait;
}
@end
