//
//  ReleViewController.m
//  Click Smart
//
//  Created by Tom Odler on 09.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "ReleViewController.h"
#import "Constants.h"
#import "UIViewController+RevealViewControllerAddon.h"
#import "SYAPIManager.h"
#import "SYCoreDataManager.h"

@interface ReleViewController ()
@property (weak, nonatomic) IBOutlet UIView *timePickerView;

@property (weak, nonatomic) IBOutlet UIPickerView *powerOnPicker;

@property (weak, nonatomic) IBOutlet UIView *topPickerView;

@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *startButton;

@property (weak, nonatomic) IBOutlet UILabel *headerLabel1;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel2;

@property (strong, nonatomic) NSNumberFormatter *formatter;

@property (nonatomic) int seconds1;
@property (nonatomic) int seconds2;
@property (nonatomic) int minutes1;
@property (nonatomic) int minutes2;
@property (nonatomic) int resultTime1;
@property (nonatomic) int resultTime2;

@property (nonatomic) BOOL delayedONOpened;
@property (nonatomic) BOOL automat;

@property (weak, nonatomic) IBOutlet UIImageView *automatImage;
@end

@implementation ReleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:NO];
    
    //Configure navigation bar apperence
    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
    
    _powerOnPicker.delegate = self;
    _powerOnPicker.dataSource = self;
    
    _formatter = [[NSNumberFormatter alloc] init];
    [_formatter setNumberStyle:NSNumberFormatterNoStyle];
    [_formatter setMaximumIntegerDigits:2];
    [_formatter setMinimumIntegerDigits:2];

    _cancelButton.layer.borderWidth = 1.0f;
    _cancelButton.layer.borderColor = USBlueColor.CGColor;
    
    _startButton.layer.borderWidth = 1.0f;
    _startButton.layer.borderColor = USBlueColor.CGColor;
   
    _saveButton.layer.borderWidth = 1.0f;
    _saveButton.layer.borderColor = USBlueColor.CGColor;
    
    
    _topPickerView.layer.borderWidth = 1.0f;
    _topPickerView.layer.borderColor = USBlueColor.CGColor;
    
    _headerLabel1.text = NSLocalizedString(@"actionTimedOff", @"");
    _headerLabel2.text = NSLocalizedString(@"actionTimedOn", @"");
    
    [_saveButton setBackgroundImage:[self imageWithColor:USBlueColor] forState:UIControlStateHighlighted];
    [_cancelButton setBackgroundImage:[self imageWithColor:USBlueColor] forState:UIControlStateHighlighted];
    [_startButton setBackgroundImage:[self imageWithColor:USBlueColor] forState:UIControlStateHighlighted];
   
    if([[_device getStateValueForStateName:@"delayed on: set time"]intValue]){
        _resultTime1 = [[_device getStateValueForStateName:@"delayed on: set time"]intValue];
    }
    
    if([[_device getStateValueForStateName:@"delayed off: set time"]intValue]){
        _resultTime2 = [[_device getStateValueForStateName:@"delayed off: set time"]intValue];
    }

    [self getMinutesAndSeconds];
    [self initAutomat];

    [self initFetchedResultsControllerWithHCAID:_device.deviceID];
    // Do any additional setup after loading the view.
}

-(void)initAutomat{
    _automatImage.highlighted = NO;
    _automat = NO;
    
    if([[_device getStateValueForStateName:@"automat"]boolValue]){
        _automatImage.highlighted = YES;
        _automat = YES;
    }
}

-(void)getMinutesAndSeconds{
    _minutes1 = _resultTime1/60;
    _minutes2 = _resultTime2/60;
    
    _seconds1 = _resultTime1%60;
    _seconds2 = _resultTime2%60;
    
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    _timePickerView.hidden = YES;
}


- (IBAction)delayedTimeOnButton:(id)sender {
    _delayedONOpened = true;
    _timePickerView.hidden = NO;
    [_powerOnPicker selectRow:_minutes1 inComponent:0 animated:NO];
    [_powerOnPicker selectRow:_seconds1 inComponent:1 animated:NO];
}
- (IBAction)cancelButtonTapped:(id)sender {
    _timePickerView.hidden = YES;
}
- (IBAction)delayedTimeOffButton:(id)sender {
    _delayedONOpened = false;
    _timePickerView.hidden = NO;
    [_powerOnPicker selectRow:_minutes2 inComponent:0 animated:NO];
    [_powerOnPicker selectRow:_seconds2 inComponent:1 animated:NO];
}

- (IBAction)saveButtonTapped:(id)sender {

    NSString *name = @"delayed off: set time";
    int timeToSend = _resultTime2;
    
    if(_delayedONOpened){
        name = @"delayed on: set time";
        timeToSend = _resultTime1;
    }
    
    [self putDataAsychn:[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithInt:timeToSend],name, nil]];
}


-(void)putDataAsychn:(NSMutableDictionary*)dict
{
    NSLog(@"%@", dict);
    [[SYAPIManager sharedInstance] putDeviceAction:dict device:_device
                                           success:^(AFHTTPRequestOperation* operation, id response){
                                               NSLog(@"update complete: %@",response);
                                           }
                                           failure:^(AFHTTPRequestOperation* operation, NSError *error) {
                                               NSLog(@"update error: %@",[error description]);
                                           }];
}


- (IBAction)startButtonTapped:(id)sender {
    NSString *name = @"delayed off";
    
    if(_delayedONOpened)
        name = @"delayed on";
    
    [self putDataAsychn:[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSNull alloc]init],name, nil]];
    
    _timePickerView.hidden = YES;
}
#pragma mark - Navigation
/*
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

// The number of rows of data
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 60;

}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width/2-25, 44)];
    label.backgroundColor = [UIColor blackColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Roboto-Light" size:18];
    if (component == 0){
        label.textAlignment = NSTextAlignmentRight;
        label.text = [NSString stringWithFormat:@"%li",(long)(row)];
        
    }
    else {
        label.textAlignment = NSTextAlignmentLeft;
        label.text = [_formatter stringFromNumber:[NSNumber numberWithInteger:row]];
    }
    
    
    
    return label;
    }

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if(_delayedONOpened){
        
    if(component == 0){
        _minutes1 = (int)row;
    } else {
        _seconds1 = (int)row;
    }
    _resultTime1 = (_minutes1*60) + _seconds1;
        
    } else {
        
        if(component == 0){
            _minutes2 = (int)row;
        } else {
            _seconds2 = (int)row;
        }
        _resultTime2 = (_minutes2*60) + _seconds2;
        
    }
    NSLog(@"time1: %d, time2: %d", _resultTime1, _resultTime2);
}

-(void)hideAlert:(UIAlertView*)sender{
    [sender dismissWithClickedButtonIndex:-1 animated:YES];
}

- (void)initFetchedResultsControllerWithHCAID:(NSString*)hcaID {
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"State"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"device.label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"self.device.deviceID == %@", hcaID];
    [fetchRequest setPredicate:predicate];
    _devices = [[NSFetchedResultsController alloc]
                initWithFetchRequest:fetchRequest
                managedObjectContext:context
                sectionNameKeyPath:nil
                cacheName:nil];
    _devices.delegate = self;
    NSError *error;
    
    if (![_devices performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }else if ([[_devices fetchedObjects] count]>0){
        _device = (Device*)((State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    if ([_devices.fetchedObjects count]>0){
        _device = (Device*)((State*)[[_devices fetchedObjects] objectAtIndex:0]).device;
        
        [self syncComplete:_device];
    }
}

-(void)syncComplete:(Device *)device{
    if([[_device getStateValueForStateName:@"delayed on: set time"]intValue]){
        _resultTime1 = [[_device getStateValueForStateName:@"delayed on: set time"]intValue];
    }
    
    if([[_device getStateValueForStateName:@"delayed off: set time"]intValue]){
        _resultTime2 = [[_device getStateValueForStateName:@"delayed off: set time"]intValue];
    }
    
    [self getMinutesAndSeconds];
    [self initAutomat];
}
- (IBAction)automatTapped:(id)sender {
        [self putDataAsychn:[[NSMutableDictionary alloc] initWithObjectsAndKeys:[[NSNumber alloc] initWithBool:!_automat],@"automat", nil]];
}

@end
