//
//  eshopViewController.m
//  iHC-MIRF
//
//  Created by Tom Odler on 07.10.15.
//  Copyright © 2015 Vratislav Zima. All rights reserved.
//

#import "eshopViewController.h"
#import "UIViewController+RevealViewControllerAddon.h"

@interface eshopViewController ()

@end

@implementation eshopViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[self navigationController] setNavigationBarHidden:NO];
    
    [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (self.navigationController) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        
        [[[self navigationController] navigationBar] setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
        [[[self navigationController] navigationBar] setShadowImage:[UIImage new]];
    }
}

-(UIImage*)generateImage:(NSUInteger)index{
    
    NSArray *obrazky = @[@"ovladace",@"systemove_prvky", @"stmivace",@"spinace",@"regulace_teploty", @"osvetleni", @"monitorovaci_prvky", @"rf_sety",@"prislusenstvi"];
    UIImage *img = [UIImage imageNamed:[[obrazky objectAtIndex:index ]stringByAppendingString:@"_off"]];
    return img;
}

-(UIImage*)generateImageOn:(NSUInteger)index{
    NSArray *obrazky = @[@"ovladace",@"systemove_prvky", @"stmivace",@"spinace",@"regulace_teploty", @"osvetleni", @"monitorovaci_prvky", @"rf_sety",@"prislusenstvi"];
    UIImage *img = [UIImage imageNamed:[[obrazky objectAtIndex:index ]stringByAppendingString:@"_on"]];
    return img;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSArray *nazvy = @[NSLocalizedString(@"ldrivers.eshop", nil), NSLocalizedString(@"lsystem_features.eshop", nil), NSLocalizedString(@"ldimmer.eshop", nil), NSLocalizedString(@"lswitches.eshop", nil), NSLocalizedString(@"ltemperature_control.eshop", nil), NSLocalizedString(@"llighting.eshop", nil),NSLocalizedString(@"lmonitoring_features.eshop", nil), NSLocalizedString(@"RF_sets.eshop", nil), NSLocalizedString(@"laccessories.eshop", nil)];

    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"myCell" forIndexPath:indexPath];
    
    UIImageView *icon = (UIImageView *)[cell viewWithTag:2];
    UILabel *myLabel = (UILabel *)[cell viewWithTag:3];
    
    
    [myLabel setText: [nazvy objectAtIndex:indexPath.item]];
    icon.image = [self generateImage:indexPath.item];
    icon.highlightedImage = [self generateImageOn:indexPath.item];
    
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 9;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView{
    return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    
    UIImageView *cellBcg = (UIImageView *)[cell viewWithTag:1];
    UIImageView *icon = (UIImageView *)[cell viewWithTag:2];
    UILabel *myLabel = (UILabel *)[cell viewWithTag:3];
    UIImageView *sipka = (UIImageView *)[cell viewWithTag:4];
    

    
    
    switch(indexPath.item){
        case 0:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"drivers.eshop", nil)]];
            break;
        case 1:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"systemfeatures.eshop", nil)]];
            break;
        case 2:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"dimmer.eshop", nil)]];
            break;
        case 3:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"switches.eshop", nil)]];
            break;
        case 4:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"temperaturecontrol.eshop", nil)]];
            break;
        case 5:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"lighting.eshop", nil)]];
            break;
        case 6:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"monitoringfeatures.eshop", nil)]];
            break;
        case 7:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"rfsets.eshop", nil)]];
            break;
        case 8:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:NSLocalizedString(@"accessories.eshop", nil)]];
            break;
    }
    [icon setHighlighted:false];
    [cellBcg setHighlighted:false];
    [myLabel setHighlighted:false];
    [sipka setHighlighted:false];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(nonnull NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    UILabel *myLabel = (UILabel *)[cell viewWithTag:3];
    
    [myLabel setTextColor:[UIColor whiteColor]];
}

-(IBAction)backToMainMenu:(id)sender{
        [self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
   
    
    if ([[touch.view class] isSubclassOfClass:[UICollectionViewCell class]]) {
        
        UICollectionViewCell *cell =(UICollectionViewCell *) touch.view;
        UIImageView *cellBcg = (UIImageView *)[cell viewWithTag:1];
        UIImageView *icon = (UIImageView *)[cell viewWithTag:2];
        UILabel *myLabel = (UILabel *)[cell viewWithTag:3];
        UIImageView *sipka = (UIImageView *)[cell viewWithTag:4];
        
        [icon setHighlighted:true];
        [cellBcg setHighlighted:true];
        [myLabel setHighlighted:true];
        [sipka setHighlighted:true];
        
}
    
}

-(void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([[touch.view class] isSubclassOfClass:[UICollectionViewCell class]]) {
        UICollectionViewCell *cell =(UICollectionViewCell *) touch.view;
        UIImageView *cellBcg = (UIImageView *)[cell viewWithTag:1];
        UIImageView *icon = (UIImageView *)[cell viewWithTag:2];
        UILabel *myLabel = (UILabel *)[cell viewWithTag:3];
        UIImageView *sipka = (UIImageView *)[cell viewWithTag:4];
        
        [icon setHighlighted:false];
        [cellBcg setHighlighted:false];
        [myLabel setHighlighted:false];
        [sipka setHighlighted:false];
        
}
}

-(void) touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    
    if ([[touch.view class] isSubclassOfClass:[UICollectionViewCell class]]) {
        UICollectionViewCell *cell =(UICollectionViewCell *) touch.view;
        UIImageView *cellBcg = (UIImageView *)[cell viewWithTag:1];
        UIImageView *icon = (UIImageView *)[cell viewWithTag:2];
        UILabel *myLabel = (UILabel *)[cell viewWithTag:3];
        UIImageView *sipka = (UIImageView *)[cell viewWithTag:4];
        
        [icon setHighlighted:false];
        [cellBcg setHighlighted:false];
        [myLabel setHighlighted:false];
        [sipka setHighlighted:false];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
