//
//  RGBPlanViewController.m
//  iHC-MIRF
//
//  Created by Tom Odler on 07.04.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//
#import "EFCircularSlider.h"
#import "EFGradientGenerator.h"
#import "RGBPlanViewController.h"

@interface RGBPlanViewController ()
@property (weak, nonatomic) IBOutlet UIView *topSliderView;
@property (weak, nonatomic) IBOutlet UIView *bottomSliderView;

@property (weak, nonatomic) IBOutlet UILabel *topSlideViewValueIndicatorLabel;
@property (strong, nonatomic) EFGradientGenerator *gradientGen;

@property (strong, nonatomic) EFCircularSlider* circularSlider;
@property (strong, nonatomic) EFCircularSlider* bcircularSlider;


@property (nonatomic) Byte red;
@property (nonatomic) Byte green;
@property (nonatomic) Byte blue;
@property (nonatomic) Byte intensity;
@end

#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )
#define COLOR_PALETE_ROTATION_OFFSET 0

@implementation RGBPlanViewController
-(id)initWithDelegate:(id<rgbSetterDelegate>)delegate red:(int)cRed green:(int)cGreen blue:(int)cBlue mode:(NSInteger)mode intensity:(int)intensity{
    self = [[RGBPlanViewController alloc] initWithNibName:@"RGBPlanViewController" bundle:nil];
    
    if(self != nil){
        _red = cRed;
        _green = cGreen;
        _blue = cBlue;
        _mode = mode;
        _intensity = intensity;
        _myDelegate = delegate;
        NSLog(@"GOT RGB R:%d, G:%d, B:%d", _red,_green,_blue);
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //TOP SLIDER CONF
    CGRect sliderFrame = CGRectMake(0, 0, _topSliderView.frame.size.width, _topSliderView.frame.size.height);
    _circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    [_circularSlider addTarget:self action:@selector(topSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_topSliderView addSubview:_circularSlider];
    [_circularSlider pauseAutoredrawing];
    
    CGFloat dash[]={1,15};
    [_circularSlider setHandleRadius:10];
    [_circularSlider setUnfilledLineDash:dash andCount:2];
    [_circularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_circularSlider setUnfilledColor:[UIColor whiteColor]];
    [_circularSlider setFilledColor:[UIColor whiteColor]];
    [_circularSlider setHandleColor:[UIColor whiteColor]];
    [_circularSlider setLineWidth:1];
    [_circularSlider setArcStartAngle:150];
    [_circularSlider setArcAngleLength:240];
    
    
    [_circularSlider setCurrentArcValue:(CGFloat)_intensity forStartAnglePadding:2 endAnglePadding:2];
    
    //BOTTOM SLIDER CONF
    
    EFGradientGenerator *gradient = [[EFGradientGenerator alloc] initWithSize:_bottomSliderView.frame.size];
    [gradient setRadius:80];
    [gradient setSectors:360];
    
    
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0] atSector:36];  //WHITE OfR
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:37];  //WHITE
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:40];  //WHITE
    [gradient addNewColor:[UIColor colorWithRed:1 green:0 blue:1 alpha:1] atSector:90];  //PURPLE
    [gradient addNewColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:1] atSector:135]; //BLUE
    [gradient addNewColor:[UIColor colorWithRed:0 green:1 blue:1 alpha:1] atSector:180]; //AZURE
    [gradient addNewColor:[UIColor colorWithRed:0 green:1 blue:0 alpha:1] atSector:225]; //GREEN
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:0 alpha:1] atSector:260]; //YELLOW
    [gradient addNewColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1] atSector:318]; //RED
    [gradient addNewColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:1] atSector:323]; //RED
    [gradient addNewColor:[UIColor colorWithRed:1 green:0 blue:0 alpha:0] atSector:324]; //RED OfR
    
    [gradient renderImage];
    _gradientGen = gradient;
    
    CGRect bsliderFrame = CGRectMake(0, 0, _bottomSliderView.frame.size.width, _bottomSliderView.frame.size.height);
    _bcircularSlider = [[EFCircularSlider alloc] initWithFrame:bsliderFrame];
    [_bcircularSlider addTarget:self action:@selector(botSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    [_bottomSliderView addSubview:_bcircularSlider];
    [_bcircularSlider pauseAutoredrawing];
    
    [_bcircularSlider setImageBackgroundUnfilledLine:YES];
    [_bcircularSlider setLineWidth:8];
    [_bcircularSlider setFilledColor:[UIColor clearColor]];
    [_bcircularSlider setUnfilledLineStrokeBorderWidth:0];
    [_bcircularSlider setUnfilledLineStrokeBorderColor:[UIColor clearColor]];
    [_bcircularSlider setUnfilledLineInnerImage:[gradient renderedImage]];
    [_bcircularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_bcircularSlider setHandleColor:[UIColor whiteColor]];
    [_bcircularSlider setArcStartAngle:130];
    [_bcircularSlider setArcAngleLength:280];
    [_bcircularSlider setHandleRadius:10];
    [_bcircularSlider setHandleBorderSize:2];
    
    [self setColorSliderPosition];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeButton:(id)sender {
    NSLog(@"%hhu", _red);
    NSLog(@"%hhu", _green);
    NSLog(@"%hhu", _blue);
    NSLog(@"%zd", _mode);

    [_myDelegate setRGB:_red green:_green blue:_blue mode:_mode intensity:_intensity];
    [self.view removeFromSuperview];
}

-(void)setColorSliderPosition{
    if (!_gradientGen || !_bcircularSlider) {
        return;
    }
    
    NSArray *gradientColors = [_gradientGen getSegmentForUIColor:[UIColor colorWithRed:(CGFloat)_red/255.0 green:(CGFloat)_green/255.0 blue:(CGFloat)_blue/255.0 alpha:1.0] ];
    if ([gradientColors count] == 0) {
        [_bcircularSlider setCurrentArcValue:0.0];
    }else{
        NSNumber *number = [gradientColors objectAtIndex:0];
        CGFloat angle = [_gradientGen getAngleFromNorthForSector:[number unsignedIntegerValue]];
        NSLog(@"SettingColor: %lu %f", [number unsignedIntegerValue], angle);
        if ([_bcircularSlider isAngleValidArcAngle:angle]) {
            [_bcircularSlider setAngleFromNorth:angle];
            [self botSliderValueChanged:_bcircularSlider];
        }else{
            [_bcircularSlider setAngleFromNorth:[_bcircularSlider arcStartAngle]];
            [self botSliderValueChanged:_bcircularSlider];
        }
    }
    
}

-(void)topSliderValueChanged:(EFCircularSlider*)circularSlider {
    _topSlideViewValueIndicatorLabel.text = [NSString stringWithFormat:@"%.f", [circularSlider getCurrentArcValueForStartAnglePadding:2 endAnglePadding:2]];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:_topSlideViewValueIndicatorLabel.text];
    
    _intensity = [myNumber intValue];
    
}

-(void)botSliderValueChanged:(EFCircularSlider*)circularSlider {
    UIColor *myColor = [_gradientGen getColorFromAngleFromNorth:([circularSlider angleFromNorth])];
    
    [circularSlider setHandleColor:[_gradientGen getColorFromAngleFromNorth:([circularSlider angleFromNorth])]];
    CGColorRef color = [myColor CGColor];
    CGFloat *components = CGColorGetComponents(color);
    
    _red = (Byte)(components[0]*255);
    _green = (Byte)(components[1]*255);
    _blue = (Byte)(components[2]*255);
    
    
    NSLog(@"onChange: RED: %d GREEN: %d BLUE: %d", _red, _green, _blue);
}

@end
