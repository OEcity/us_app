//
//  URLConnector.h
//  O2archiv
//
//  Created by Vratislav Zima on 1/15/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"
#import "URLConnectionListener.h"
#import "Elan.h"

@interface URLConnector : NSObject <NSURLConnectionDelegate>
    

-(id)initWithAddressAndLoader:(NSString *)address_ activityInd:(UIActivityIndicatorView *)activityInd_;
-(void) setParser:(id <Response>) response_;
-(void)setConnectionListener:(id <URLConnectionListener> )listener_;
-(void)ConnectAndGetData;
-(void)ConnectAndPostData:(NSData*)requestData toElan:(Elan*)elan;
-(void)setPost;
-(void)deleteItem;

    @property(nonatomic, copy) NSString * address;
    @property(nonatomic, copy) NSString * name;
    @property(nonatomic, retain) UIActivityIndicatorView * activityInd;
    @property (nonatomic,retain) NSMutableURLRequest * request;
    @property BOOL isPost;
    @property (nonatomic) NSObject * bundle;
    @property (nonatomic, copy) NSString * ContentType;
    @property (nonatomic, retain) NSMutableData * receivedData;
    @property NSInteger statusCode;
    @property (retain) NSURLConnection *connection;

@end
