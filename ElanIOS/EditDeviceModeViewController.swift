//
//  EditDeviceModeViewController.swift
//  iHC-MIRF
//
//  Created by admin on 16.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

import UIKit

class EditDeviceModeViewController: AddDeviceModeViewController {

    var tempDayMode = TempDayMode()
    
    @IBOutlet weak var dayButton: UIButton!
    @IBOutlet weak var eraseButton: UIButton!
    @IBOutlet weak var odButton: UIButton!
    @IBOutlet weak var doButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //initiate selecting day
        switch day{
        case 0: dayLabel.text = "Monday"
        case 1: dayLabel.text = "Tuesday"
        case 2: dayLabel.text = "Wednesday"
        case 3: dayLabel.text = "Thursday"
        case 4: dayLabel.text = "Friday"
        case 5: dayLabel.text = "Saturday"
        case 6: dayLabel.text = "Sunday"
        default: dayLabel.text = "Day"
        }
        selectedDay = true
        dayButton.isEnabled = false
        
        
        //initiate class (because of superClass)
        timeFrom = tempDayMode.startTime.intValue
        timeTo = tempDayMode.endTime.intValue
        selectedFrom = true
        selectedTill = true
        selectedMode = true
        mode = tempDayMode.mode.intValue
        
        odLabel.text = "From \(formatter.string(from: NSNumber(value:tempDayMode.startTime.intValue/60))!):\(formatter.string(from: NSNumber(value:tempDayMode.startTime.intValue%60))!)"
        doLabel.text = "Till \(formatter.string(from: NSNumber(value:tempDayMode.endTime.intValue/60))!):\(formatter.string(from: NSNumber(value:tempDayMode.endTime.intValue%60))!)"
        
        //cover, else model makes mistakes
        if timeFrom == 0 && timeTo == 1440 {
            eraseButton.isHidden = true
            odButton.isEnabled = false
            doButton.isEnabled = false
        }
        
        //modeTap(tempDayMode.mode.integerValue)
        if shouldShowZero{
            tableView.selectRow(at: NSIndexPath.init(item: tempDayMode.mode.intValue, section: 0) as IndexPath, animated: false, scrollPosition: UITableViewScrollPosition.top)
        }
        else{
            tableView.selectRow(at: NSIndexPath.init(item: tempDayMode.mode.intValue - 1, section: 0) as IndexPath, animated: false, scrollPosition: UITableViewScrollPosition.top)
        }
        
        
        
    }
    
    @IBAction func eraseButtonTap() {
        self.delegate.addDeviceModeToday(day, comesFromLTouch: true, tempDayMode: tempDayMode, position: modePos, erase: true)
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func prepareAndSendDataToDelegate() {
        
        tempDayMode.mode = NSNumber(value:mode)
        tempDayMode.startTime = NSNumber(value:timeFrom)
        tempDayMode.endTime = NSNumber(value:timeTo)
        tempDayMode.duration = NSNumber(value:timeTo - timeFrom)
        
        
        self.delegate?.addDeviceModeToday(day, comesFromLTouch: true, tempDayMode: tempDayMode, position:modePos, erase: false)
    }
    
    
}
