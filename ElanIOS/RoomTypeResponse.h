//
//  RoomTypeResponse.h
//  iHC-MIRF
//
//  Created by Vlastimil Venclik on 21.11.13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"
@interface RoomTypeResponse : NSObject <Response>

@end
