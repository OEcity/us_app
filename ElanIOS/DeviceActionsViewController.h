//
//  DeviceActionsViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 07/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataObjects.h"



typedef enum _ActionType {
    Impulse,
    On,
    DelayedOn,
    TimeOfDelayedOn,
    DelayedOff,
    TimeOfDelayedOff
} ActionType;

@interface DeviceActionsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate>


@property (nonatomic , retain) Device* device;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *tableViewBackground;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property ActionType type;
@property (nonatomic, strong) NSMutableSet* expandedeRows;

-(void)showExpandedItems;

@end
