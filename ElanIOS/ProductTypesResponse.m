//
//  ProductTypesResponse.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/1/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "ProductTypesResponse.h"
#import "SBJson.h"
@implementation ProductTypesResponse


-(NSObject*)parseResponse:(NSData *)inputData identifier:(NSString *)identifier{
    NSMutableArray * itemsArray = [[NSMutableArray alloc] init];

    SBJsonParser *parser2 = [[SBJsonParser alloc] init];
    NSString *inputString = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];

    NSArray* jsonObjects = [parser2 objectWithString:inputString];
    for (NSString* object in jsonObjects){
        [itemsArray addObject:object];

    }

    return itemsArray;
    
}
@end
