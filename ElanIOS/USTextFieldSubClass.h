//
//  USTextFieldSubClass.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 27.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface USTextFieldSubClass : UITextField<UITextFieldDelegate>

@end
