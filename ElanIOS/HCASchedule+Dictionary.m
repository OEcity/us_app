//
//  HCASchedule+Dictionary.m
//  US App
//
//  Created by Tom Odler on 14.07.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "HCASchedule+Dictionary.h"
#import "SYCoreDataManager.h"
#import "CoreDataObjects.h"

@implementation HeatTimeSchedule (Dictionary)

- (BOOL)updateWithDictionary:(NSDictionary*)dict {
    BOOL updated = NO;
    
    NSString* parsedID = [dict objectForKey:@"id"];
    NSString* parsedLabel  = [dict objectForKey:@"label"];
    NSNumber* parsedHysteresis = [dict objectForKey:@"hysteresis"];
    
    
    
    if (![self.heattimescheduleID isEqualToString:parsedID]) {
        self.heattimescheduleID = parsedID;
        updated = YES;
    }
    
    if (![self.label isEqualToString:parsedLabel]) {
        self.label = parsedLabel;
        updated = YES;
    }
    
    if (self.hysteresis.doubleValue != parsedHysteresis.boolValue) {
        self.hysteresis = parsedHysteresis;
        updated = YES;
    }
    
    NSDictionary* modesDict = [dict objectForKey:@"modes"];
    
    if (![modesDict isKindOfClass: [NSNull class]]){
        NSDictionary *singleModeDict = [[NSDictionary alloc] init];
        
        singleModeDict = [modesDict objectForKey:@"1"];
        self.temp1 = [NSNumber numberWithDouble:[[singleModeDict objectForKey:@"min"] doubleValue]];
        
        
        singleModeDict = [modesDict objectForKey:@"2"];
        self.temp2 = [NSNumber numberWithDouble:[[singleModeDict objectForKey:@"min"] doubleValue]];
        
        
        singleModeDict = [modesDict objectForKey:@"3"];
        self.temp3 = [NSNumber numberWithDouble:[[singleModeDict objectForKey:@"min"] doubleValue]];
        
        
        singleModeDict = [modesDict objectForKey:@"4"];
        self.temp4 = [NSNumber numberWithDouble:[[singleModeDict objectForKey:@"min"] doubleValue]];
        
        updated = YES;
    }
    
    return updated;
}



@end
