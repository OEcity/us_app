//
//  FloorPlanLoader.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/21/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "URLConnector.h"
#import "Floorplan.h"
@interface FloorPlanLoader : NSObject <URLConnectionListener>


@property (nonatomic, retain) NSMutableArray * floorplans;
@property (nonatomic, retain) URLConnector * url;
-(void)LoadFloorplans;

@end
