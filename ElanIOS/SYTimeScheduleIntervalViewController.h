//
//  SYTimeScheduleIntervalViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 12/5/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYHeatContainerViewController.h"
#import "TempSchedule.h"
#import "TempScheduleDay.h"
#import "HeatTimeScheduleView.h"
#import "AddHeatingIntervalViewController.h"
#import "SYHeatContainerViewController.h"
#import "HUDWrapper.h"
@interface SYTimeScheduleIntervalViewController : UIViewController <AddHeatingDelegate>
@property (nonatomic, retain) SYHeatContainerViewController * container;
@property (nonatomic, retain) NSDictionary * schedule;
@property (weak, nonatomic) IBOutlet UIView *timeAxisView;
@property (weak, nonatomic) IBOutlet UILabel *lComfortTemperature;
@property (weak, nonatomic) IBOutlet UILabel *lNormalTemperature;
@property (weak, nonatomic) IBOutlet UILabel *lAttenuationTemperature;
@property (weak, nonatomic) IBOutlet UILabel *lMinimumTemperature;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *modeFrames;
@property (weak, nonatomic) IBOutlet UIView *swimlinesArea;
@property (nonatomic, retain) TempSchedule * tempSchedule;
@property (nonatomic, retain) TempScheduleDay * mDay;
@property (nonatomic, retain) HeatTimeScheduleView * timeScheduleView;
@property (nonatomic, retain) AddHeatingIntervalViewController * addHeatingInterval;
@property NSInteger currentDayPosition;
@property (weak, nonatomic) IBOutlet UILabel *lDay;
@property (weak, nonatomic) IBOutlet UILabel *lTimeScheduleName;
@property (weak, nonatomic) IBOutlet UIButton *rightArrowBtn;
@property (weak, nonatomic) IBOutlet UIButton *leftArrowBtn;
@property (nonatomic, retain) NSArray * res;
@property (nonatomic, retain) NSMutableArray * dayToCopy;
@property (nonatomic, retain) HUDWrapper * loaderDialog;

@property (weak, nonatomic) IBOutlet UIView *DaysView;
@property (weak, nonatomic) IBOutlet UILabel *lSave;
@property (weak, nonatomic) IBOutlet UILabel *lCopyTo;
@property (weak, nonatomic) IBOutlet UIButton *btnCopyTo;

@property (weak, nonatomic) IBOutlet UILabel *lNewTitle;

@property (weak, nonatomic) IBOutlet UILabel *lTimeAxisTitle;
@property (weak, nonatomic) IBOutlet UIButton *lAlltitle;
@property (weak, nonatomic) IBOutlet UIButton *lMonTitle;
@property (weak, nonatomic) IBOutlet UIButton *lTueTitle;
@property (weak, nonatomic) IBOutlet UIButton *lWedTitle;
@property (weak, nonatomic) IBOutlet UIButton *lThurTtitle;
@property (weak, nonatomic) IBOutlet UIButton *lFrTitle;
@property (weak, nonatomic) IBOutlet UIButton *lSatTitle;
@property (weak, nonatomic) IBOutlet UIButton *lSunTitle;
@property (weak, nonatomic) IBOutlet UILabel *lComfortTitle;
@property (weak, nonatomic) IBOutlet UILabel *lNormalTitle;
@property (weak, nonatomic) IBOutlet UILabel *lAttenuationTitle;
@property (weak, nonatomic) IBOutlet UILabel *lMinimumTitle;

@end
