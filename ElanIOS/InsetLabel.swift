//
//  InsetLabel.swift
//  iHC-MIRF
//
//  Created by Marek Žehra on 12.03.15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

import UIKit

@IBDesignable
class InsetLabel: UILabel {
    @IBInspectable var insetX: CGFloat = 0
    @IBInspectable var insetY: CGFloat = 0
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsetsMake(insetY,insetX,insetY,insetX)
        let insetsRect = UIEdgeInsetsInsetRect(rect,insets)
        
        super.drawText(in: insetsRect)
    }
}

