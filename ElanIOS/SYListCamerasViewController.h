//
//  SYListCamerasViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 9/14/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SYListCamerasViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDataSource>


@property (nonatomic, retain) NSArray * cameras;
@property (nonatomic, retain) NSMutableArray * loading;
@property (nonatomic, retain) IBOutlet UICollectionView * collectionView;
@property (nonatomic)  NSInteger currentCameraID;
@property (nonatomic, retain) NSOperationQueue *operationQueue;
@property (weak, nonatomic) IBOutlet UIButton *rightBottomButton;
@property (weak, nonatomic) IBOutlet UIButton *leftBottomButton;

@end
