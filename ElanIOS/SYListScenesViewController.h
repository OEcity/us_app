//
//  SYListScenesViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 7/30/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoaderViewController.h"
#import "URLConnector.h"
#import "Scene.h"
#import "HUDWrapper.h"
#import "SYBaseViewController.h"
@interface SYListScenesViewController : SYBaseViewController <UITabBarDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate>


@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) Scene * scene;
@property (nonatomic, retain) NSIndexPath * deletedIndex;
@property (nonatomic, retain) HUDWrapper * loaderDialog;

@property (weak, nonatomic) IBOutlet UILabel *scenesLabel;
@property (weak, nonatomic) IBOutlet UIButton *bottomLeftButton;
@property (weak, nonatomic) IBOutlet UIButton *bottomRightButton;


@end
