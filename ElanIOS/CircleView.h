//
//  CircleView.h
//  iHC-MIRF
//
//  Created by Tom Odler on 29.03.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CircleView : UIView
@property(nonatomic, retain) UIColor* circleColor;


@end
