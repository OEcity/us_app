//
//  OneWeekScheduleViewController.h
//  iHC-MIRF
//
//  Created by admin on 24.06.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "TemperatureOffsetViewController.h"
#import "OneDayHeatTimeAdjustView.h"
#import "OneDayHeatTimeSheduleView.h"

#import "AddHeatingModeViewController.h"

//změnit při přeimportu
#import "Click Smart-Swift.h"


@interface OneWeekScheduleViewController : UIViewController<OneDayHeatTimeModesTemperatuDataSource, AddHeatingDelegate, OneDayHeatTimeModesEditDelegate>

@property (nonatomic, retain) NSMutableDictionary * schedule;
//@property (nonatomic, retain) TemperatureOffsetViewController * temperatureOffset;



@end
