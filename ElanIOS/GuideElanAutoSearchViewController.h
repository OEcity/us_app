//
//  GuideElanAutoSearchViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 28.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchService.h"
#import "SYBaseViewController.h"

@interface GuideElanAutoSearchViewController : UIViewController<SearchServiceDelegate, UITabBarDelegate, UITableViewDataSource, NSFetchedResultsControllerDelegate, UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *loadingTopView;
@property (nonatomic, retain) UILabel * noElan;
@property (nonatomic, retain) NSMutableArray * elanArray;
@property (nonatomic, retain) NSArray * savedElans;
@property (nonatomic, retain) Elan * selectedElan;
//@property (nonatomic, retain) NSTimer * broadcastTimer;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loadingTopViewHeightConstrint;
@property (weak, nonatomic) IBOutlet UILabel *lSearchingElanTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lTitle;
@property (weak, nonatomic) IBOutlet UILabel *lnoElan;

@end
