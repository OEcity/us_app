//
//  LinphoneManager.h
//  iHC
//
//  Created by Pavel Gajdoš on 06.09.13.
//  Copyright (c) 2013 Pavel Gajdoš. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AVFoundation/AVAudioSession.h>
#import <AudioToolbox/AudioToolbox.h>
#import <CoreTelephony/CTCallCenter.h>
#import <MediaPlayer/MediaPlayer.h>

#include "linphone/linphonecore.h"


extern const char *const LINPHONERC_APPLICATION_KEY;

extern NSString *const kLinphoneCoreUpdate;
extern NSString *const kLinphoneDisplayStatusUpdate;
extern NSString *const kLinphoneTextReceived;
extern NSString *const kLinphoneCallUpdate;
extern NSString *const kLinphoneRegistrationUpdate;
extern NSString *const kLinphoneMainViewChange;
extern NSString *const kLinphoneAddressBookUpdate;
extern NSString *const kLinphoneLogsUpdate;
extern NSString *const kLinphoneSettingsUpdate;
extern NSString *const kLinphoneBluetoothAvailabilityUpdate;



@interface LinphoneManager : NSObject<AVAudioSessionDelegate> {

@private
    NSTimer * iterateTimer;
    UIBackgroundTaskIdentifier pausedCallBgTask;
	UIBackgroundTaskIdentifier incallBgTask;
    CTCallCenter* mCallCenter;

    
@public
    LinphoneCall * callBeforeGoingToBackground;

}

@property (readwrite) BOOL speakerEnabled;

+ (LinphoneManager *)instance;

+ (LinphoneCore *)getLinphoneCore;
+ (BOOL)isLinphoneCoreReady;

+ (NSString *)sipAddressWithUsername:(NSString *)username;
+ (NSString*)bundleFile:(NSString*)file;
+ (NSString*)documentFile:(NSString*)file;


- (void)startLibLinphone;
- (void)destroyLibLinphone;
- (BOOL)resignActive;
- (void)becomeActive;
- (BOOL)enterBackgroundMode;
- (void)acceptCallForCallId:(NSString*)callid;


- (void)iterate;

- (void)refreshRegisters;

- (void)authenticate;

- (void)acceptCall:(LinphoneCall*)call;
- (void)declineCall:(LinphoneCall*)call;
- (void)endCall:(LinphoneCall*)call;
- (void)call:(NSString *)address;

- (void)sendStringUsingDTMF:(NSString *)string;
+ (BOOL)checkUnlockCode:(id)object;


- (BOOL)isSpeakerEnabled;
- (void)enableSpeaker:(BOOL)enable;


@end
