//
//  URLConnectionListener.h
//  O2archiv
//
//  Created by Vratislav Zima on 1/15/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Response.h"

@protocol URLConnectionListener <NSObject>
-(void) connectionResponseOK:(NSObject*)returnedObject response:(id <Response> ) responseType;
-(void) connectionResponseFailed:(NSInteger)code;
@end
