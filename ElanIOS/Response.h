//
//  Response.h
//  O2archiv
//
//  Created by Vratislav Zima on 1/15/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol Response <NSObject>

@required
-(NSObject *)parseResponse:(NSData *)inputData identifier:(NSString*)identifier;

@end
