//
//  DeviceSchedule.m
//  iHC-MIRF
//
//  Created by Tom Odler on 16.03.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "DeviceSchedule.h"

@implementation DeviceSchedule
-(id) initWithType:(NSString*)type{
    
    self= [super init];
    if (self){
        _type = type;
        _modes = [[NSMutableArray alloc] initWithCapacity:4];
        _modes[0] = [[TempScheduleMode alloc] initWithId:[[NSNumber alloc] initWithInt:1] min:[[NSNumber alloc] initWithInt:15] max:[[NSNumber alloc] initWithInt:35]];
        _modes[1] = [[TempScheduleMode alloc] initWithId:[[NSNumber alloc] initWithInt:2] min:[[NSNumber alloc] initWithInt:20] max:[[NSNumber alloc] initWithInt:35]];
        _modes[2] = [[TempScheduleMode alloc] initWithId:[[NSNumber alloc] initWithInt:3] min:[[NSNumber alloc] initWithInt:24] max:[[NSNumber alloc] initWithInt:35]];
        
        _modes[3] = [[TempScheduleMode alloc] initWithId:[[NSNumber alloc] initWithInt:4] min:[[NSNumber alloc] initWithInt:28] max:[[NSNumber alloc] initWithInt:35]];
        
        NSLog(@"SCHEDULE INIT TYPE: %@", _type);
        
        _days = [[NSMutableArray alloc] initWithCapacity:7];
        _days[0] =[[DeviceScheduleDay alloc] initWithNameAndType:@"monday"type:_type];
        _days[1] =[[DeviceScheduleDay alloc] initWithNameAndType:@"tuesday"type:_type];
        _days[2] =[[DeviceScheduleDay alloc] initWithNameAndType:@"wednesday"type:_type];
        _days[3] =[[DeviceScheduleDay alloc] initWithNameAndType:@"thursday"type:_type];
        _days[4] =[[DeviceScheduleDay alloc] initWithNameAndType:@"friday"type:_type];
        _days[5] =[[DeviceScheduleDay alloc] initWithNameAndType:@"saturday" type:_type];
        _days[6] =[[DeviceScheduleDay alloc] initWithNameAndType:@"sunday" type:_type];
        
        
        for (DeviceScheduleDay *day in _days)
            [day setupModes];
        
        //TODO setup hysteresis from previous viewcontroller
        
    }
    
    return  self;
    
}
- (DeviceScheduleDay*) getDayByPosition:(int) position {
    if (_days == nil)
        return nil;
    
    NSString*key = nil;
    switch (position) {
        case 0:
        default:
            key = @"monday";
            break;
            
        case 1:
            key = @"tuesday";
            break;
            
        case 2:
            key = @"wednesday";
            break;
            
        case 3:
            key = @"thursday";
            break;
            
        case 4:
            key = @"friday";
            break;
            
        case 5:
            key = @"saturday";
            break;
            
        case 6:
            key = @"sunday";
            break;
    }
    
    for (DeviceScheduleDay *day in _days)
        if ([day.id isEqualToString:key])
            return day;
    
    return nil;
}


- (void) setDayOnPosition:(DeviceScheduleDay*)day position:(int)position{
    
    NSString*key = nil;
    switch (position) {
        case 0:
        default:
            key = @"monday";
            break;
            
        case 1:
            key = @"tuesday";
            break;
            
        case 2:
            key = @"wednesday";
            break;
            
        case 3:
            key = @"thursday";
            break;
            
        case 4:
            key = @"friday";
            break;
            
        case 5:
            key = @"saturday";
            break;
            
        case 6:
            key = @"sunday";
            break;
    }
    int i=0;
    int rec = 0;
    for (DeviceScheduleDay *den in _days){
        
        if ([den.id isEqualToString:key]){
            
            rec = i;
        }
        i++;
    }
    
    _days[rec] = day;
    
}

- (TempScheduleMode*) getModeById:(int) id {
    if (_modes == nil)
        return nil;
    
    for (TempScheduleMode *mode in _modes)
        if ([mode.id intValue] == id)
            return mode;
    
    return nil;
}

@end
