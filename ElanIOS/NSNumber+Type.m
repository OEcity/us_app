//
//  NSNumber+Type.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 15/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "NSNumber+Type.h"

@implementation NSNumber (Type)




/**
 *
    Return integer value by default
 */
-(id) initWithType:(NSString*)type value:(id)value{

    if ([type isEqualToString:@"int"]){
        return [self initWithInt:[value intValue]];
    }else if ([type isEqualToString:@"float"]){
        return [self initWithInt:[value floatValue]];
    }else if ([type isEqualToString:@"boolean"]){
        return [self initWithInt:[value boolValue]];
    }else{
        return [self initWithInt:[value intValue]];
    }
}
@end
