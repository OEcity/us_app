//
//  TempScheduleMode.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 1/21/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface TempScheduleMode : NSObject
@property (nonatomic, retain) NSNumber*  id;
@property (nonatomic, retain) NSNumber* min;
@property (nonatomic, retain) NSNumber* max;
-(id)initWithId:(NSNumber*)id min:(NSNumber*)min max:(NSNumber*)max;

@end
