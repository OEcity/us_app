//
//  EFCircularSlider.h
//  Awake
//
//  Created by Eliot Fowler on 12/3/13.
//  Copyright (c) 2013 Eliot Fowler. All rights reserved.
//
//  Modified by Mikoláš Stuchlík on 6/21/16
//

#import <UIKit/UIKit.h>
#import "Device+State.h"

/**
 *  Class used to define a circular control with a handle that can be moved around the circumference to represent a value
 */
@interface EFCircularSlider : UIControl{
    CGFloat _filledLineDash[32];
    CGFloat _unfilledLineDash[32];
}

typedef enum : NSUInteger {
    CircularSliderHandleTypeSemiTransparentWhiteCircle,
    CircularSliderHandleTypeSemiTransparentBlackCircle,
    CircularSliderHandleTypeDoubleCircleWithOpenCenter,
    CircularSliderHandleTypeDoubleCircleWithClosedCenter,
    CircularSliderHandleTypeBigCircle,
    CircularSliderHandleTypeCircleCustom
} CircularSliderHandleType;

#pragma mark - Default Autolayout initialiser
/**
 *  Initialise the class with a desired radius
 *  This initialiser should be used for autolayout - use initWithFrame otherwise
 *  Note: Intrinsice content size will be based on this parameter, lineWidth and handleType
 *
 *  @param radius Desired radius of circular slider
 *
 *  @return Allocated instance of this class
 */
-(id)initWithRadius:(CGFloat)radius;


#pragma mark - Values
/**
 *  @property Value at North/midnight (start)
 */
@property (nonatomic) CGFloat minimumValue;
/**
 *  @property Value at North/midnight (end)
 */
@property (nonatomic) CGFloat maximumValue;
/**
 * WARNING: beware using when circle is not finite (e.g. when using only arc), does NOT provide error response on invalid position
 *  @property Current value between North/midnight (start) and North/midnight (end) - clockwise direction
 */
@property (nonatomic) CGFloat currentValue;
/**
 *  @property arcStartAngle sets start of arc
 */
@property (nonatomic) CGFloat arcStartAngle;
/**
 *  @property arcAngleLength sets length of erc in degrees, clockwise
 */
@property (nonatomic) CGFloat arcAngleLength;
/**
 *  @property Current value between start and end angle - clockwise direction
 */
@property (nonatomic) CGFloat currentArcValue;
/**
 *  Return current value between start and end angle with angle padding - clockwise.
 *
 *  @param stArcPd  angle oriented clockwisely before which is returned value minimal
 *  @param enArcPd  angle oriented non-clockwisely after which is returned value maximal
 */
-(CGFloat)getCurrentArcValueForStartAnglePadding:(CGFloat)stArcPd endAnglePadding:(CGFloat)enArcPd;

/**
 *  Set current value between start and end angle with angle padding - clockwise.
 *
 *  @param value    target value
 *  @param stArcPd  angle oriented clockwisely before which is returned value minimal
 *  @param enArcPd  angle oriented non-clockwisely after which is returned value maximal
 */
-(void)setCurrentArcValue:(CGFloat)value forStartAnglePadding:(CGFloat)stArcPd endAnglePadding:(CGFloat)enArcPd;

#pragma mark - Labels
/**
 *  @property BOOL indicating whether values snap to nearest label
 */
@property (nonatomic) BOOL snapToLabels;
/**
 *  Note: The LAST label will appear at North/midnight
 *        The FIRST label will appear at the first interval after North/midnight
 *
 *  @property NSArray of strings used to render labels at regular intervals within the circle
 */
@property (nonatomic, strong) NSArray *innerMarkingLabels;


#pragma mark - Visual Customisation
/**
 *  @property describes line stroke border width
 */
@property (nonatomic) CGFloat filledLineStrokeBorderWidth;
/**
 *  @property describes line stroke border radius
 */
@property (nonatomic) CGFloat filledLineStrokeBorderRadius;
/**
 *  @property describes line stroke border color
 */
@property (strong, nonatomic) UIColor * filledLineStrokeBorderColor;
/**
 *  @property image that is displayed on background of slider, image should be equal or greater size then slider
 */
@property (strong, nonatomic) UIImage * filledLineInnerImage;
/**
 *  @property  color that is displayed on background of slider (if image is nil), image should be equal or greater size then slider
 */
@property (strong, nonatomic) UIColor * filledLineInnerBGColor;

/**
 *  @property describes line stroke border width
 */
@property (nonatomic) CGFloat unfilledLineStrokeBorderWidth;
/**
 *  @property describes line stroke border radius
 */
@property (nonatomic) CGFloat unfilledLineStrokeBorderRadius;
/**
 *  @property describes line stroke border color
 */
@property (strong, nonatomic) UIColor * unfilledLineStrokeBorderColor;
/**
 *  @property  image that is displayed on background of slider, image should be equal or greater size then slider
 */
@property (strong, nonatomic) UIImage * unfilledLineInnerImage;

/**
 *  @property  color that is displayed on background of slider (if image is nil), image should be equal or greater size then slider
 */
@property (strong, nonatomic) UIColor * unfilledLineInnerBGColor;

/**
 *  @property which defines what kind of line you want to draw, in fact there are two kinds of line NO = default only stroke line, YES = line with image stroke and defined border. Image stroke line also usem some of non image line's properties, this line has also curved edges
 */
@property (nonatomic) BOOL imageBackgroundUnfilledLine;

/**
 *  @property which defines what kind of line you want to draw, in fact there are two kinds of line NO = default only stroke line, YES = line with image stroke and defined border. Image stroke line also usem some of non image line's properties, this line has also curved edges
 */
@property (nonatomic) BOOL imageBackgroundFilledLine;

/**
 *  @property Width of the line to draw for slider
 */
@property (nonatomic) CGFloat lineWidth;

/**
 *  @property Width difference from @property lineWidth of the filled line to draw for slider
 */
@property (nonatomic) CGFloat filledLineWidthDifference;

/**
 *  @property Width difference from @property lineWidth of the unfilled line to draw for slider
 */
@property (nonatomic) CGFloat unfilledLineWidthDifference;

/**
 *  @property filled line dash parameters
 */
@property (readonly, nonatomic) CGFloat *filledLineDash;

/**
 *  @property unfilled line dash parameters
 */
@property (readonly, nonatomic) CGFloat *unfilledLineDash;

/**
 *  @property filled line dash parameters
 */
@property (readonly, nonatomic) int filledLineDashCount;

/**
 *  @property unfilled line dash parameters
 */
@property (readonly, nonatomic) int unfilledLineDashCount;

/**
 *  set up filled line dash style (solid style parse {1} and 0)
 */
-(void)setFilledLineDash:(CGFloat *)filledLineDash andCount:(int)filledLineDashCount;

/**
 *  set up unfilled line dash style (solid style parse {1} and 0)
 */
-(void)setUnfilledLineDash:(CGFloat *)unfilledLineDash andCount:(int)unfilledLineDashCount;

/**
 *  @property Color of filled portion of line (from North/midnight start to currentValue)
 */
@property (nonatomic, strong) UIColor* filledColor;
/**
 *  @property Color of unfilled portion of line (from currentValue to North/midnight end)
 */
@property (nonatomic, strong) UIColor* unfilledColor;
/**
 *  Note: If this property is not set, filledColor will be used.
 *        If handleType is semiTransparent*, specified color will override this property.
 *
 *  @property Color of the handle
 */
@property (nonatomic, strong) UIColor* handleColor;
/**
 *  @property color of the CircularSliderHandleTypeCircleCustom circle border
 */
@property (nonatomic, strong) UIColor* handleBorderColor;
/**
 *  @property size of the CircularSliderHandleTypeCircleCustom border
 */
@property (nonatomic) CGFloat handleBorderSize;
/**
 *  @property radius of the CircularSliderHandleTypeCircleCustom handler
 */
@property (nonatomic) CGFloat handleRadius;

/**
 *  @property Font of the inner marking labels within the circle
 */
@property (nonatomic, strong) UIFont*  labelFont;
/**
 *  @property Color of the inner marking labels within the circle
 */
@property (nonatomic, strong) UIColor* labelColor;
/**
 *  Note: A negative value will move the label closer to the center. A positive value will move the label closer to the circumference
 *  @property Value with which to displace all labels along radial line from center to slider circumference.
 */
@property (nonatomic) CGFloat labelDisplacement;
/**
 *  @property Type of the handle to display to represent draggable current value
 */
@property (nonatomic) CircularSliderHandleType handleType;

/*
 * Provides distance in degrees of handler from start angle
 */
-(CGFloat)angleFromStartPoint;

/*
 * @property determinate handler's angle from north
 */
@property (nonatomic) CGFloat     angleFromNorth;

/*
 *Had to set Device class because of handling slider action in DeviceCollectionDelegate - Sending commands to specific devices
 */
@property (nonatomic) Device *deviceForHandler;

/*
 *  PERFORMANE OPTIMIZATION: there is possible to stop rendering when parameters are changed due to high pefrormace requirements on start. Does NOT work for redrawing caused by handler events. If required implement UIControll state disabled instead.
 *  WARNING: Use carefuly, may cause application crash when used unproperly
 */

/*
 * @property that describes if autoredrawind was paused
 */
@property (readonly, nonatomic, getter=isAutoredrawingPaused) BOOL autoredrawingPaused;

/*
 * Pauses autoredrawing on non-slide property changes
 */
-(void)pauseAutoredrawing;

/*
 * Enables autoredrawing on non-slide property changes; Also instantly redraws
 */
-(void)activateAutoredrawing;

/*
 * Instantanstly redraws
 */
-(void)redraw;

/**
 *  If arg angle is inside of arc returning value is YES. Otherwise NO.
 *
 *  @param angle    tested angle from north
 */
-(BOOL)isAngleValidArcAngle:(CGFloat)angle;
@end
