//
//  GuideELANListViewController.h
//  iHC-MIIRF
//
//  Created by Tom Odler on 27.09.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Elan.h"
#import "HUDWrapper.h"

@interface GuideELANListViewController : UIViewController <NSFetchedResultsControllerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) Elan * currentElan;
@property (nonatomic, retain) HUDWrapper *loaderDialog;

@end
