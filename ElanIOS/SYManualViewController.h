//
//  SYManualViewController.h
//  iHC-MIRF
//
//  Created by Vlastimil Venclik on 21.11.13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SYManualViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *leftBottomButton;

@end
