//
//  DismissSegue.m
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 10/06/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "DismissSegue.h"

@implementation DismissSegue

- (void)perform {
    UIViewController *sourceViewController = self.sourceViewController;
    [sourceViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
