//
//  AddHeatingIntervalViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 1/26/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TempDayMode.h"

@protocol AddHeatingDelegate

-(void)addHeatingModeToday:(BOOL)today tempDayMode:(TempDayMode*)tempdayMode position:(int)position erase:(BOOL)erase;

@end

@interface AddHeatingIntervalViewController : UIViewController <UIGestureRecognizerDelegate>
@property (nonatomic, retain) TempDayMode * mMode;
@property (nonatomic, retain) id<AddHeatingDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UILabel *secondsLabel;
@property (retain, nonatomic) IBOutlet UILabel *minutesLabel;
@property (retain, nonatomic) IBOutlet UILabel *hoursLabel;

@property (retain, nonatomic) IBOutlet UILabel *secondsToLabel;
@property (retain, nonatomic) IBOutlet UILabel *minutesToLabel;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *heatModeCollection;
@property (weak, nonatomic) IBOutlet UILabel *lWindowStateTitle;
@property (weak, nonatomic) IBOutlet UIButton *eraseButton;
@property (weak, nonatomic) IBOutlet UILabel *lToTitle;
@property (weak, nonatomic) IBOutlet UILabel *lHeatModeTitle;
@property (weak, nonatomic) IBOutlet UILabel *lMinimumTitle;
@property (weak, nonatomic) IBOutlet UILabel *lAttenuationTitle;
@property (weak, nonatomic) IBOutlet UILabel *lNormalTitle;
@property (weak, nonatomic) IBOutlet UILabel *lComfortTitle;
@property (weak, nonatomic) IBOutlet UIButton *lBackTitle;
@property (weak, nonatomic) IBOutlet UIButton *lSaveTitle;
@property (weak, nonatomic) IBOutlet UIButton *lEraseTitle;

@property (weak, nonatomic) IBOutlet UIButton *bFromTimeHourUp;
@property (weak, nonatomic) IBOutlet UIButton *bFromTimeMinuteUp;
@property (weak, nonatomic) IBOutlet UIButton *bFromTimeHourDown;
@property (weak, nonatomic) IBOutlet UIButton *bFromTimeMinuteDown;

@property (weak, nonatomic) IBOutlet UIButton *bToTimeHourUp;
@property (weak, nonatomic) IBOutlet UIButton *bToTimeMinuteUp;
@property (weak, nonatomic) IBOutlet UIButton *bToTimeHourDown;
@property (weak, nonatomic) IBOutlet UIButton *bToTimeMinuteDown;


@property NSInteger timerStep;
@property NSInteger timerMin;
@property NSInteger timerMax;
@property NSInteger timeFrom;
@property NSInteger timeTo;
@property NSInteger mode;
@property NSInteger modePos;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *timeSetButtons;
@end
