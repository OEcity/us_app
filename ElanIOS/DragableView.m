//
//  DragableView.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 05/01/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import "DragableView.h"

@implementation DragableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


-(void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    _originalPosition = self.center;
    _touchOffset = CGPointMake(self.center.x-2,self.center.y-2);
    [self highlight];
}

-(void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    CGPoint position = [touch locationInView: self.superview];

    [UIView animateWithDuration:.001
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^ {

                         self.center = CGPointMake(position.x, position.y);
                     }
                     completion:^(BOOL finished) {}];
}

-(void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];

    CGPoint positionInView = [touch locationInView:self.superview];
    CGPoint newPosition;
    if (CGRectContainsPoint(CGRectMake(0, 0, self.superview.frame.size.width, self.superview.frame.size.height), positionInView)) {
        newPosition = positionInView;
        // _desiredView is view where the user can drag the view
    } else {
        newPosition = _originalPosition;
        // its outside the desired view so lets move the view back to start position
    }

    [UIView animateWithDuration:0.4
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^ {
                         self.center = newPosition;
                         // to
                     }
                     completion:^(BOOL finished) {}];
    
}


-(void)highlight{
    Device * device = [self.selectedDevices objectAtIndex:self.tag-100];


    //clear all
    for (int i=100; i < 100 + [self.selectedDevices count]; i++){
        UIImageView * iV1 = (UIImageView*)[[[self.superview.superview viewWithTag:1] viewWithTag:i] viewWithTag:1];
        [iV1 setImage:[UIImage imageNamed:@"dlazdice_off.png"]];
        UIImageView * iV2 = (UIImageView*)[[[self.superview.superview viewWithTag:1] viewWithTag:i] viewWithTag:2];
        [iV2 setImage:[UIImage imageNamed:[((Device*)[self.selectedDevices objectAtIndex:i-100]).iconPrefix stringByAppendingString:@"_off.png"] ]];

        UIImageView * iV3 = (UIImageView*)[self.superview viewWithTag:i];
        [iV3 setImage:[UIImage imageNamed:@"room_dlazdice_off.png"]];
        UIImageView * iV4 = (UIImageView*)[[self.superview viewWithTag:i] viewWithTag:1];
        [iV4 setImage:[UIImage imageNamed:[((Device*)[self.selectedDevices objectAtIndex:i-100]).iconPrefix stringByAppendingString:@"_off.png"] ]];

    }
        UIImageView * iV1 = (UIImageView*)[[[self.superview.superview viewWithTag:1] viewWithTag:self.tag] viewWithTag:1];
        [iV1 setImage:[UIImage imageNamed:@"dlazdice_on.png"]];
        UIImageView * iV2 = (UIImageView*)[[[self.superview.superview viewWithTag:1] viewWithTag:self.tag] viewWithTag:2];
        [iV2 setImage:[UIImage imageNamed:[device.iconPrefix stringByAppendingString:@"_on.png"]]];

        UIImageView *iv1 = (UIImageView*)[self.superview viewWithTag:self.tag] ;
        [iv1 setImage:[UIImage imageNamed:@"room_dlazdice_on.png"]];
        UIImageView *iv2 = (UIImageView*)[[self.superview viewWithTag:self.tag] viewWithTag:1] ;
        [iv2 setImage:[UIImage imageNamed:[device.iconPrefix stringByAppendingString:@"_on.png"] ]];

        UIView * obj =[[self.superview.superview viewWithTag:1] viewWithTag:self.tag];

        [self moveViewsRelative: CGPointMake(150 - obj.center.x, 130- obj.center.y)];

}
-(void) moveViewsRelative:(CGPoint) newLocation{

    for (UIView *v in self.deviceViews){
        if (_landscape){
            [v setCenter:CGPointMake(v.center.x  , v.center.y +  newLocation.y)];
        }else{
            [v setCenter:CGPointMake(v.center.x +  newLocation.x , v.center.y)];
        }

    }
}


@end
