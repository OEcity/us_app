//
//  HUDWrapper.h
//  KJ PDA
//
//  Created by Kaktus on 6/30/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MBProgressHUD.h"


@interface HUDWrapper : NSObject <MBProgressHUDDelegate> {
    MBProgressHUD *HUD;
    UIViewController  *rootController;
}

@property (nonatomic, retain) MBProgressHUD *HUD;
@property (nonatomic, retain) UIViewController *rootController;
@property BOOL *isCancelable;

-(HUDWrapper *) initWithRootController:(UIViewController *) controller;
-(HUDWrapper *) initWithRootController:(UIViewController *) controller cancelable:(BOOL)isCancelable;

-(void) show;
-(void) hide;
-(void) showWithLabel: (NSString *) text;
- (void)hudWasHidden:(MBProgressHUD *)hud;
- (void) setViewController: (UIViewController *) controller;

@end
