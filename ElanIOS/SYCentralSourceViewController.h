//
//  SYCentralSourceViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 11/10/14.
//  Copyright (c) 2014 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HUDWrapper.h"
#import "SYHeatContainerViewController.h"
@interface SYCentralSourceViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray * sources;
@property (nonatomic, retain) HUDWrapper * loaderDialog;
@property (nonatomic, retain) SYHeatContainerViewController * container;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UIButton *btnAdd;

@end
