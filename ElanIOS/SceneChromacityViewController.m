//
//  SceneChromacityViewController.m
//  Click Smart
//
//  Created by Tom Odler on 08.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "SceneChromacityViewController.h"
#import "EFCircularSlider.h"
#import "EFGradientGenerator.h"

@interface SceneChromacityViewController ()
@property (strong, nonatomic) EFCircularSlider* circularSlider;
@property (strong, nonatomic) EFCircularSlider* bcircularSlider;
@property (strong, nonatomic) EFGradientGenerator *gradientGen;
@property (weak, nonatomic) IBOutlet UILabel *lisghtDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *productLabel;
@property (weak, nonatomic) IBOutlet UIButton *whiteButton;
@property (weak, nonatomic) IBOutlet UIButton *switchButton;
@property (weak, nonatomic) IBOutlet UIView *topSliderView;
@property (weak, nonatomic) IBOutlet UIView *bottomSliderView;
@property (weak, nonatomic) IBOutlet UILabel *topSlideViewValueIndicatorLabel;

@property (nonatomic) int whiteBalance;
@property (nonatomic) int intensity;

@property (nonatomic) NSMutableDictionary *dict;

@end

@implementation SceneChromacityViewController
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _deviceName = _device.deviceID;
    
    _dict = [NSMutableDictionary new];
    
    _productLabel.text = _device.productType;
    _lisghtDescriptionLabel.text = _device.label;
    
    //CONF BUTTONS
    [[_whiteButton layer] setBorderColor:[[UIColor colorWithRed:0.0/255.0 green:192.0/255.0 blue:243.0/255.0 alpha:1.0] CGColor]];
    [[_switchButton layer] setBorderColor:[[UIColor colorWithRed:0.0/255.0 green:192.0/255.0 blue:243.0/255.0 alpha:1.0] CGColor]];
    
    
    //TODO: SET BOTTOM SLIDER VALUE
    
    //Configure navigation bar apperence
//    [self addButtonToNaviagationControllerWithImage:[UIImage imageNamed:@"tecky_nastaveni_off"]];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //TOP SLIDER CONF
    CGRect sliderFrame = CGRectMake(0, 0, _topSliderView.frame.size.width, _topSliderView.frame.size.height);
    _circularSlider = [[EFCircularSlider alloc] initWithFrame:sliderFrame];
    [_circularSlider addTarget:self action:@selector(topSliderValueChanged:) forControlEvents:UIControlEventValueChanged];

    [_topSliderView addSubview:_circularSlider];
    [_circularSlider pauseAutoredrawing];
    
    CGFloat dash[]={1,15};
    [_circularSlider setHandleRadius:10];
    [_circularSlider setUnfilledLineDash:dash andCount:2];
    [_circularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_circularSlider setUnfilledColor:[UIColor whiteColor]];
    [_circularSlider setFilledColor:[UIColor whiteColor]];
    [_circularSlider setHandleColor:[UIColor whiteColor]];
    [_circularSlider setLineWidth:1];
    [_circularSlider setArcStartAngle:150];
    [_circularSlider setArcAngleLength:240];
    
    
    [_circularSlider setCurrentArcValue:0 forStartAnglePadding:2 endAnglePadding:2];
    
    //BOTTOM SLIDER CONF
    
    EFGradientGenerator *gradient = [[EFGradientGenerator alloc] initWithSize:_bottomSliderView.frame.size];
    [gradient setRadius:80];
    [gradient setSectors:360];
    
    
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:1];
    [gradient addNewColor:[UIColor colorWithRed:208.0/255.0 green:85.0/255.0 blue:32.0/255.0 alpha:1] atSector:40];
    [gradient addNewColor:[UIColor colorWithRed:240.0/255.0 green:163.0/255.0 blue:52.0/255.0 alpha:1] atSector:90];
    [gradient addNewColor:[UIColor colorWithRed:231.0/255.0 green:208.0/255.0 blue:69.0/255.0 alpha:1] atSector:180];
    [gradient addNewColor:[UIColor colorWithRed:250.0/255.0 green:250.0/255.0 blue:210.0/255.0 alpha:1] atSector:270];
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:310];
    [gradient addNewColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] atSector:360];
    
    [gradient renderImage];
    _gradientGen = gradient;
    
    CGRect bsliderFrame = CGRectMake(0, 0, _bottomSliderView.frame.size.width, _bottomSliderView.frame.size.height);
    _bcircularSlider = [[EFCircularSlider alloc] initWithFrame:bsliderFrame];
    [_bcircularSlider addTarget:self action:@selector(botSliderValueChanged:) forControlEvents:UIControlEventValueChanged];

    [_bottomSliderView addSubview:_bcircularSlider];
    [_bcircularSlider pauseAutoredrawing];
    
    [_bcircularSlider setImageBackgroundUnfilledLine:YES];
    [_bcircularSlider setLineWidth:8];
    [_bcircularSlider setFilledColor:[UIColor clearColor]];
    [_bcircularSlider setUnfilledLineStrokeBorderWidth:0];
    [_bcircularSlider setUnfilledLineStrokeBorderColor:[UIColor clearColor]];
    [_bcircularSlider setUnfilledLineInnerImage:[gradient renderedImage]];
    [_bcircularSlider setHandleType:CircularSliderHandleTypeCircleCustom];
    [_bcircularSlider setHandleColor:[UIColor whiteColor]];
    [_bcircularSlider setArcStartAngle:130];
    [_bcircularSlider setArcAngleLength:280];
    [_bcircularSlider setHandleRadius:10];
    [_bcircularSlider setHandleBorderSize:2];
    [_bcircularSlider setCurrentArcValue:10 forStartAnglePadding:2 endAnglePadding:2];
    [self loadParametersFromArray];
}


-(void)loadParametersFromArray{
    NSArray *mySceneArray = nil;
    
    if(_controller != nil){
        mySceneArray = _controller.actionsArray;
    } else {
        mySceneArray = _guideController.actionsArray;
    }
    
    NSInteger index = 0;
    NSMutableIndexSet *indexForDelete = [[NSMutableIndexSet alloc]init];
    for(NSDictionary*dict in mySceneArray){
        NSString *deviceID =[[dict allKeys]firstObject];
        if([deviceID isEqualToString:_device.deviceID]){
            NSDictionary *myActionsDict = [dict objectForKey:deviceID];

                _intensity = [[myActionsDict objectForKey:@"brightness"]intValue]/2.55;
                _whiteBalance = [[myActionsDict objectForKey:@"white balance"]intValue]/2.45;
            
            if(_intensity > 255)
                _intensity = 255;
            
            if(_whiteBalance < 10)
                _whiteBalance = 10;
            
            if(_whiteBalance > 245)
                _whiteBalance = 245;
            
            [_circularSlider setCurrentArcValue:_intensity forStartAnglePadding:2 endAnglePadding:2];
            [_bcircularSlider setCurrentArcValue:(100 - _whiteBalance) forStartAnglePadding:2 endAnglePadding:2];
            
            [indexForDelete addIndex:index];
        }
        index++;
    }
    
    if ([indexForDelete count] > 0){
        if(_guideController)
            [_guideController.actionsArray removeObjectsAtIndexes:indexForDelete];
        else
            [_controller.actionsArray removeObjectsAtIndexes:indexForDelete];
    }
}


-(void)topSliderValueChanged:(EFCircularSlider*)circularSlider {
    _topSlideViewValueIndicatorLabel.text = [NSString stringWithFormat:@"%.f", [circularSlider getCurrentArcValueForStartAnglePadding:2 endAnglePadding:2]];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *myNumber = [f numberFromString:_topSlideViewValueIndicatorLabel.text];

    _intensity = [myNumber intValue];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    int myIntensity =_intensity *2.55;
    if (myIntensity > 255)
        myIntensity = 255;
    
    int myWhiteBalance = _whiteBalance *2.45;
    if(myWhiteBalance > 245)
        myWhiteBalance = 245;
    
    if(myWhiteBalance < 10)
        myWhiteBalance = 10;
        
    [_dict setObject:[NSNumber numberWithInt:(int)myIntensity] forKey:@"brightness"];
    [_dict setObject:[NSNumber numberWithInt:(int)myWhiteBalance] forKey:@"white balance"];
    
    NSMutableDictionary *deviceWithActionDictionary = [[NSMutableDictionary alloc]init];
    [deviceWithActionDictionary setObject:_dict forKey:_device.deviceID];
    
    if(_controller){
        [[_controller actionsArray] addObject:deviceWithActionDictionary];
        [[self.controller selectedDevices] setObject:_device forKey:_device.deviceID];
    }
    else{
        [[_guideController actionsArray] addObject:deviceWithActionDictionary];
        [[self.guideController selectedDevices] setObject:_device forKey:_device.deviceID];
    }

}

-(void)botSliderValueChanged:(EFCircularSlider*)circularSlider {
    [circularSlider setHandleColor:[_gradientGen getColorFromAngleFromNorth:([circularSlider angleFromNorth])]];
    _whiteBalance = (100 - [circularSlider getCurrentArcValueForStartAnglePadding:2 endAnglePadding:2]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
