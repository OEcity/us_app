//
//  SYServerInputViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 6/27/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYServerInputViewController.h"
#import "AppDelegate.h"
#import "Util.h"
#import "SYAPIManager.h"
#import "SYCoreDataManager.h"
#import "Constants.h"
@interface SYServerInputViewController (){

    BOOL isAdding;


}
@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *linkaCollection;

@end

@implementation SYServerInputViewController

@synthesize name;
@synthesize address;
@synthesize port;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    _bottomRightButton.layer.borderColor = [USBlueColor CGColor];
    _bottomRightButton.layer.borderWidth = 1.0f;
    
    NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"enterName", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
    NSAttributedString *addressPlaceholder = [[NSAttributedString alloc] initWithString:@"Address" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
    NSAttributedString *portTypePlaceHolder = [[NSAttributedString alloc] initWithString:@"Port" attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];

    
    name.attributedPlaceholder = namePlaceHolder;
    address.attributedPlaceholder = addressPlaceholder;
    port.attributedPlaceholder = portTypePlaceHolder;

}



- (void)initLocalizableStrings {
    [_lTitle setText:NSLocalizedString(@"elan_settigns", nil)];
    
    [_nameLabel setText:NSLocalizedString(@"enterName", nil)];
    [_ipAddressLabel setText:NSLocalizedString(@"enterIP", nil)];
    [_portLabel setText:NSLocalizedString(@"enterPort", nil)];
    [_lMacAddress setText:NSLocalizedString(@"elan_mac_address", nil)];
    [_lFirmware setText:NSLocalizedString(@"elan_fw_version", nil)];
    
    [_bottomRightButton setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
    
    [_bottomLeftButton setTitle:NSLocalizedString(@"list", nil) forState:UIControlStateNormal];


}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardChangedStatus:) name:UIKeyboardWillShowNotification object:nil];

    
    if(_elan){
        self.name.text = _elan.label;
        NSArray* tmp = [_elan.baseAddress componentsSeparatedByString:@":"];
        if ([tmp count]!=2){
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                              message:NSLocalizedString(@"elan_not_reacheable", nil)
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];
  //          [self dismissViewControllerAnimated:YES completion:nil];
            [self.navigationController popViewControllerAnimated:YES];
            return;
        }
        self.port.text = [tmp objectAtIndex:1];
        self.address.text = [tmp objectAtIndex:0];
        [_tfFirmware setText:_elan.firmware];
        [_tfMacAddress setText:_elan.mac];
        NSURL * baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/", _elan.baseAddress]];
        
        [[SYAPIManager sharedInstance] getAPIRootForURL:baseURL success:^(AFHTTPRequestOperation* request, id object){
            NSDictionary * objectDict = (NSDictionary*)object;
            NSDictionary * info = [objectDict objectForKey:@"info"];
            
            [_elan parseTypeAndVersion:info];
            [_tfFirmware setText:_elan.firmware];
            
        }failure:^(AFHTTPRequestOperation * operation, NSError * error){
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                              message:NSLocalizedString(@"elan_not_reacheable", nil)
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            
            
            
            [message show];
            
        }];

    } else {

        isAdding = YES;
        self.port.text = @"80";
        _lFirmware.hidden=YES;
        _lMacAddress.hidden=YES;
        _tfFirmware.hidden=YES;
        _tfMacAddress.hidden=YES;
        
        for (UIImageView* myImage in _linkaCollection ){
            myImage.hidden = YES;
        }
    }
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self hideKeyboard:textField];
    return NO;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)addServer:(id)sender{

    if ([name.text isEqualToString:@""]){
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"elan_name_dialog_title", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        return;
    } if([address.text isEqualToString:@""]) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"ipError", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        return;
    }if ([port.text isEqualToString:@""] ){
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                          message:NSLocalizedString(@"portError", nil)
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        [message show];
        return;

        
    }
    
    
    NSString* elanLabel = name.text;
    NSString* elanBaseAddress = [[address.text stringByAppendingString:@":"] stringByAppendingString:port.text];
    
    if(isAdding){
        
        NSPredicate * predicate = [NSPredicate predicateWithFormat:@"baseAddress == %@", elanBaseAddress];
        
        NSArray * foundElans = [[SYCoreDataManager sharedInstance] getObjectsForEntity:@"Elan" withPredicate:predicate];
        if (foundElans.count!=0){
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                              message:NSLocalizedString(@"elan_already_saved", nil)
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];
            return;
        }
        
        
        //Obtain additionally mac address and fw version
        NSURL * baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/", elanBaseAddress]];

        [[SYAPIManager sharedInstance] getAPIRootForURL:baseURL success:^(AFHTTPRequestOperation* request, id object){
            NSDictionary * objectDict = (NSDictionary*)object;
            NSDictionary * info = [objectDict objectForKey:@"info"];
            _elan = [[SYCoreDataManager sharedInstance] createEntityElanInContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
            _elan.mac = [info valueForKey:@"MAC address"];
            [_elan parseTypeAndVersion:info];
            _elan.baseAddress = elanBaseAddress;
            _elan.label = elanLabel;
            
            [[SYCoreDataManager sharedInstance] saveContext];
            
//            [self dismissViewControllerAnimated:YES completion:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }failure:^(AFHTTPRequestOperation * operation, NSError * error){
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@""
                                                              message:NSLocalizedString(@"elan_not_reacheable", nil)
                                                             delegate:nil
                                                    cancelButtonTitle:@"OK"
                                                    otherButtonTitles:nil];
            [message show];

        }];


    } else {
        Elan * elan = [[SYCoreDataManager sharedInstance] getELANwithMAC:_elan.mac inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
        _elan.label = elanLabel;
        _elan.baseAddress = elanBaseAddress;
        _elan.configurationCounter = [NSNumber numberWithInt:-1];
        
        elan.label = elanLabel;
        elan.baseAddress = elanBaseAddress;
        elan.configurationCounter = [NSNumber numberWithInt:-1];
        [[SYCoreDataManager sharedInstance] saveContext];

        if (_delegate) {
            [_delegate didSaveElan];
        }
        
 //      [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];

    }
    

   // [CoreDataManager setActiveServer:server];
}
-(IBAction)backToList:(id)sender{
//    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)hideKeyboard:(id)sender{
    [name resignFirstResponder];
    [address resignFirstResponder];
    [port resignFirstResponder];
    [_tfMacAddress resignFirstResponder];
    [_tfFirmware resignFirstResponder];
    _cTBottomOffset.constant=40;

}
- (NSUInteger)supportedInterfaceOrientations
{

    return UIInterfaceOrientationMaskPortrait;
}



#pragma mark - Get Keyboard size

- (void)keyboardChangedStatus:(NSNotification*)notification {
    //get the size!
    CGRect keyboardRect;
    [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardRect];
    int keyboardHeight = keyboardRect.size.height;
    //move your view to the top, to display the textfield..
    _cTBottomOffset.constant=keyboardHeight;

}





@end
