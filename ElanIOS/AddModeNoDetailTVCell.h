//
//  AddModeNoDetailTVCell.h
//  iHC-MIRF
//
//  Created by admin on 15.08.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddModeNoDetailTVCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;

@end
