//
//  SY_State_TemperatureView.h
//  iHC-MIRF
//
//  Created by Daniel Rutkovský on 18/06/15.
//  Copyright (c) 2015 Vratislav Zima. All rights reserved.
//

#import "SY_State_View.h"

@interface SY_State_TemperatureView : SY_State_View
@property (weak, nonatomic) IBOutlet UILabel *lTempOut;
@property (weak, nonatomic) IBOutlet UILabel *lTempIn;
@property (weak, nonatomic) IBOutlet UILabel *lTemp;

-(void) setModeTempINnOUT;
-(void) setModeTemp;

@end
