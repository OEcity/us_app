//
//  SYViewFavouritesViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 8/7/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RGBViewController.h"
#import "IntensityConfigurationViewController.h"
#import "DeviceActionsViewController.h"
#import "DevicesTableViewDelegate.h"
#import "SYBaseViewController.h"
@interface SYViewFavouritesViewController : SYBaseViewController <UIGestureRecognizerDelegate, NSFetchedResultsControllerDelegate>


@property (nonatomic, retain) IBOutlet UITableView * tableView;
@property (nonatomic, retain) NSMutableArray* devices;
@property (retain) IntensityConfigurationViewController * intensity;
@property (retain) RGBViewController * rgb;
@property (nonatomic, retain)  IBOutlet NSMutableArray * rgbControllers;
@property (retain) DeviceActionsViewController * actions;
@property (nonatomic, retain) DevicesTableViewDelegate * tableViewDelegate;
@property (weak, nonatomic) IBOutlet UIButton *rightBottomButton;
@property (weak, nonatomic) IBOutlet UIButton *leftBottomButton;


@end
