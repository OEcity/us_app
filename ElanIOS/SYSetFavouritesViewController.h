//
//  SYSetFavouritesViewController.h
//  iHC-MIRF
//
//  Created by Vratislav Zima on 7/29/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SYCoreDataManager.h"

@interface SYSetFavouritesViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>


@property (nonatomic, retain) NSArray * devices;
@property (nonatomic, retain) NSMutableSet * devicesToBeAdded;
@property (nonatomic, retain) NSMutableArray* devicesToBeChanged;
@property (nonatomic, retain) NSMutableDictionary* devicesToBeChangedStates;
@property (nonatomic, retain) NSMutableArray* originalArray;

@property (weak, nonatomic) IBOutlet UILabel *favouritesLabel;
@property (weak, nonatomic) IBOutlet UIButton *bottomRightButton;
@property (weak, nonatomic) IBOutlet UIButton *bottomLeftButton;

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@end
