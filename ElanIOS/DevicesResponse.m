//
//  DevicesResponse.m
//  ElanIOS
//
//  Created by Vratislav Zima on 5/27/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "DevicesResponse.h"
#import "SBJson.h"
#import "Device.h"

@implementation DevicesResponse


-(NSObject*) parseResponse:(id )inputData identifier:(NSString *)identifier{
    NSMutableArray * itemsArray = [[NSMutableArray alloc] init];
    
    SBJsonParser *parser2 = [[SBJsonParser alloc] init];
    NSDictionary* jsonObjects;
    if ([inputData isKindOfClass:[NSData class]]){
    NSString *inputString = [[NSString alloc] initWithData:inputData encoding:NSUTF8StringEncoding];
    
    jsonObjects = [parser2 objectWithString:inputString];
    }else{
        jsonObjects = (NSDictionary*)inputData;
    }
    
    for(NSString* dict in jsonObjects)
    {
                
        
        Device *device = [[Device alloc] init];
        
        @try {
            device.deviceID = dict;
            NSDictionary* newDict = [jsonObjects valueForKey:dict];
            NSString * url = [newDict valueForKey:@"url"];
            device.url = url;
            
        }
        @catch (NSException *exception) {
            NSLog(@"Chyba"); //handle this exception !!
            
        }@finally {
            [itemsArray addObject:device];
        }
        
    }
    return itemsArray;

}


@end
