//
//  SYIPSettingsViewController.m
//  iHC-MIRF
//
//  Created by Vratislav Zima on 6/26/13.
//  Copyright (c) 2013 Vratislav Zima. All rights reserved.
//

#import "SYIPSettingsViewController.h"
#import "SYCoreDataManager.h"
#import "AppDelegate.h"
#import "Util.h"
#import "IPTableViewCell.h"
#import "SYServerInputViewController.h"
#import "SYAPIManager.h"
#import "SYDataLoader.h"
#import "Elan+Type.h"
#import "Constants.h"
#import "MainMenuPVC.h"


@interface SYIPSettingsViewController (){
    NSInteger selectedRow;
    BOOL changed;
    NSIndexPath* indexOfElan;

}
@property (nonatomic, strong) NSFetchedResultsController* elans;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;

@end

@implementation SYIPSettingsViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.bottomRightMenu.layer.borderColor = [USBlueColor CGColor];
    self.bottomRightMenu.layer.borderWidth = 1.0f;
    
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    
    self.searchButton.layer.borderColor = [USBlueColor CGColor];
    self.searchButton.layer.borderWidth = 1.0f;
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [_ipSettingsLabel setText:NSLocalizedString(@"elan_settigns", nil)];
    [_bottomLeftMenu setTitle:NSLocalizedString(@"menu", nil) forState:UIControlStateNormal];
    [_bottomRightMenu setTitle:NSLocalizedString(@"add", nil) forState:UIControlStateNormal];
    [self initFetchedResultsController];
    [_tableView reloadData];
    
//    _currentElan =[[SYCoreDataManager sharedInstance] getCurrentElan];
    changed = NO;
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    _elans = nil;
    if (self.tableView.editing){
        self.tableView.editing = NO;
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Init

- (void)initFetchedResultsController {
    
    NSManagedObjectContext *context = [[SYCoreDataManager sharedInstance] managedObjectContext];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Elan"];
    // Configure the request's entity, and optionally its predicate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"label" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    _elans = [[NSFetchedResultsController alloc]
              initWithFetchRequest:fetchRequest
              managedObjectContext:context
              sectionNameKeyPath:nil
              cacheName:nil];
    _elans.delegate = self;
    NSError *error;
    
    if (![_elans performFetch:&error]) {
        NSLog(@"error fetching Rooms: %@",[error description]);
    }
}

#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    //    [self.tableView endUpdates];
    [self.tableView reloadData];
}

- (void)configureCell:(IPTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    UILabel * nameLabel = (UILabel*)[cell viewWithTag:1];
    UILabel * addressLabel = (UILabel*)[cell viewWithTag:2];
    UIImageView * isActive = (UIImageView*)[cell viewWithTag:3];
    UIImageView * iVCircle = (UIImageView*)[cell viewWithTag:5];
    Elan* elan =(Elan*)[_elans objectAtIndexPath:indexPath];
    nameLabel.text = elan.label;
    addressLabel.text = elan.baseAddress;

    if(elan.selected.boolValue == true){
        [isActive setImage:[UIImage imageNamed:@"vyber_modre_kolecko.png"]];
    }else{
        [isActive setImage:[UIImage imageNamed:@"vyber_sede_kolecko.png"]];
    }
    [isActive setUserInteractionEnabled:NO];
    if (elan.selected.boolValue){
        iVCircle.image = [UIImage imageNamed:@"elan_on.png"];
        iVCircle.highlightedImage = [UIImage imageNamed:@"elan_bila.png"];
    }else{
        [iVCircle setImage:[UIImage imageNamed:@"elan_off.png"]];
        iVCircle.image= [UIImage imageNamed:@"elan_off.png"];
        iVCircle.highlightedImage = [UIImage imageNamed:@"elan_bila.png"];
    }
    cell.btn.userInteractionEnabled = YES;
    cell.btn.tag = indexPath.row;
    [cell.btn addTarget:self action:@selector(switchTapped:) forControlEvents:UIControlEventTouchUpInside];
    
}

-(void)setupElan{
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    [[SYAPIManager sharedInstance] getAPIRootWithSuccessForElan:_currentElan success:^(AFHTTPRequestOperation *request, NSDictionary* response){
        NSString*inf = [response valueForKey:@"info"];
        NSString*myType = nil;
        
        for (NSString* elanType in ELAN_TYPES) {
            NSString* version = [inf valueForKey:[elanType stringByAppendingString:@" version"]];
            if(version != nil){
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setValue:elanType forKey:@"elanType"];
                [defaults synchronize];
                myType = elanType;
                NSLog(@"Elan type: %@", elanType);
            }
        }
        
        NSString* ws = [response valueForKey:@"notifications"];
        
        [[SYCoreDataManager sharedInstance] setElanTypeAndWSAddress:_currentElan type:myType wsAddress:ws];
        
        [[SYDataLoader sharedInstance] loadDataFromElan:_currentElan];
        
//        [[SYWebSocket sharedInstance] initialize];
        
        
    }failure:^(AFHTTPRequestOperation * request, NSError * error){
        [_loaderDialog hide];
        UIAlertController*alert = [UIAlertController alertControllerWithTitle:@"" message:NSLocalizedString(@"deviceCommFailed", nil) preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction*ok = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [alert dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }];
}

-(IBAction)backToSettings:(id)sender{
    //Server * currentServer = [CoreDataManager getActiveServer];
    if (!changed){
        [[self navigationController] popViewControllerAnimated:YES];
        return;
    }
    
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.loadingDevices=NO;
    app.showRooms=YES;
    
    [[SYDataLoader sharedInstance] reloadIfNeeded];
    [self setupNewElan];
    //[self dismissViewControllerAnimated:YES completion:nil];
}

-(void)setupNewElan{
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    [_loaderDialog showWithLabel:NSLocalizedString(@"waitPls", nil)];
    AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    app.loadingDevices = YES;
    

}-(IBAction)addServer:(id)sender{
    selectedRow = -1;
    [self performSegueWithIdentifier:@"input" sender:self];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.destinationViewController isKindOfClass:[SYServerInputViewController class]]){
        if(selectedRow != -1) {
            Elan* object = [_elans objectAtIndexPath:[NSIndexPath indexPathForRow:selectedRow inSection:0]];
            [(SYServerInputViewController*)segue.destinationViewController setElan:object];
            [(SYServerInputViewController*)segue.destinationViewController setDelegate:self];
        }
    }
}

#pragma mark - UITableView datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    // If you're serving data from an array, return the length of the array:
    return [_elans.fetchedObjects count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IPTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"serverCell"];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)tableView:(UITableView *)_tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObject* object = [_elans objectAtIndexPath:indexPath];
        
        UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"confirm_delete_elan", nil),[object valueForKey:@"label"]] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"confirmation.yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [controller dismissViewControllerAnimated:YES completion:nil];
            [self deleteElanWithMac:[[_elans objectAtIndexPath:indexPath] mac]];
        }];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"confirmation.no", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [controller dismissViewControllerAnimated:YES completion:nil];
        }];
        
        [controller addAction:okAction];
        [controller addAction:cancelAction];
        [self presentViewController:controller animated:YES completion:nil];
        
    }
}

-(void)loadingRooms{
    [_loaderDialog hide];
//    [[SYWebSocket sharedInstance] initialize];
    [_tableView reloadData];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"pairingRooms" object:nil];
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void) deleteElanWithMac:(NSString*)macAddress{
    [[SYAPIManager sharedInstance] cancelAllOperations];

    Elan * elan = [[SYCoreDataManager sharedInstance] getELANwithMAC:macAddress inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
    [[SYCoreDataManager sharedInstance] deleteManagedObject:elan inContext:[[SYCoreDataManager sharedInstance] privateObjectContext]];
    [[SYCoreDataManager sharedInstance] saveContext];
    
    for(Elan*elan in [[SYCoreDataManager sharedInstance] getCurrentElans]){
        if ([[elan mac] isEqualToString:macAddress]){
            [[SYWebSocket sharedInstance] disconnect];
            //        [[SYCoreDataManager sharedInstance] setCurrentElan:nil];
            [[SYCoreDataManager sharedInstance] saveContext];
            [[SYAPIManager sharedInstance] reinit];
        }
    }
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedRow = indexPath.row;
    UITableViewCell * cell = [_tableView cellForRowAtIndexPath:indexPath];
    cell.selected=NO;
    [self performSegueWithIdentifier:@"input" sender:self];
}

-(void) switchTapped:(UIButton*) sender{
    _currentElan = [_elans objectAtIndexPath:[NSIndexPath indexPathForRow:sender.tag inSection:0]];
    if(_currentElan.selected.boolValue){
        
        [[SYCoreDataManager sharedInstance] setUnselectedElan:_currentElan];
        [[SYCoreDataManager sharedInstance]deleteObjectsForElan:_currentElan];
        [_tableView reloadData];
        
        
        AppDelegate * app = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        MainMenuPVC*menu = app.mainMenuPVC;
        
        [menu refreshPVC:@"ip config view controller - switch tapped"];
        return;
    }
    
    UIAlertController *controller = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:NSLocalizedString(@"downloading_data", nil) ] preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"confirmation.yes", nil) style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [controller dismissViewControllerAnimated:YES completion:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(loadingRooms)
                                                     name:@"pairingRooms"
                                                   object:nil];
        
        [[SYAPIManager sharedInstance] cancelAllOperations];
        [SYWebSocket sharedInstance].connected=YES;
        [[SYWebSocket sharedInstance] disconnect];
        
        [[SYCoreDataManager sharedInstance] setSelectedElan:_currentElan];
        [self setupElan];
        
        [_tableView reloadData];
        changed=YES;
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"confirmation.no", nil) style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [controller dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [controller addAction:okAction];
    [controller addAction:cancelAction];
    [self presentViewController:controller animated:YES completion:nil];

}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    
    return UIInterfaceOrientationMaskPortrait;
    
}


- (void)didSaveElan{
    changed = YES;
}

@end
