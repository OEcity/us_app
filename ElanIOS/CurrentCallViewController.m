//
//  CurrentCallViewController.m
//  iHC
//
//  Created by Pavel Gajdoš on 07.09.13.
//  Copyright (c) 2013 Pavel Gajdoš. All rights reserved.
//

#import "CurrentCallViewController.h"
#import "SYCoreDataManager.h"
#import "Constants.h"

@interface CurrentCallViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *noStream;

@end

@implementation CurrentCallViewController

@synthesize call;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    cameraView.layer.borderColor = USBlueColor.CGColor;
    cameraView.layer.borderWidth = 1.0f;
    cameraView.layer.cornerRadius = 0.0f;
    
    speakerButton.selected = [[LinphoneManager instance] isSpeakerEnabled];
    [callTimeLabel setText:@"00:00"];
    [callerNameLabel setText:@"?"];

    [unlockButton setTitle:NSLocalizedString(@"unlock", nil) forState:UIControlStateNormal];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(volumeChanged:)
     name:@"AVSystemController_SystemVolumeDidChangeNotification"
     object:nil];
    
    
    
    //
    
    
/*    [cameraView setURL:@"http://10.10.5.103/mjpg/video.mjpg" user:@"root" password:@"ksikmx"];
    [cameraView setHidden:NO];
    [cameraView start];*/
}



- (void)volumeChanged:(NSNotification *)notif
{
    float volume =
    [[[notif userInfo]
      objectForKey:@"AVSystemController_AudioVolumeNotificationParameter"]
     floatValue];
    
    if (volume > 0.0) {
        speakerButton.selected = YES;
        [[LinphoneManager instance] enableSpeaker:YES];
    }
    else {
        speakerButton.selected = NO;
        [[LinphoneManager instance] enableSpeaker:NO];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    callDurationTimer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(updateCallDuration) userInfo:nil repeats:YES];
    [self update];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [callDurationTimer invalidate];
    [cameraView stop];
}

- (void)updateCallDuration
{
    if (!call) {
        return;
    }
    
    int duration = linphone_call_get_duration(call);
    [callTimeLabel setText:[NSString stringWithFormat:@"%02i:%02i", (duration/60), duration - 60 * (duration / 60), nil]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)setCall:(LinphoneCall *)acall
{
    call = acall;
    
    [self update];
}

- (void)update
{
    if(call == nil)
        return;
    const LinphoneAddress *addr = linphone_call_get_remote_address(call);
    
    NSString* username = nil;
    
    if (addr != NULL) {
        
        const char* username_char = linphone_address_get_username(addr);
        
        username = [NSString stringWithCString:username_char encoding:NSUTF8StringEncoding];
    }
    else {
        username = @"Unknown";
    }

    //
    
    NSString * name;
    
    IntercomContact * contact = [[SYCoreDataManager sharedInstance] getIntercomContact:username]; //[[VoIPContactsModel sharedModel] findOneByUsername:username];
    
    if (contact) {
        name = contact.label;
        
        switchCode = contact.switchCode;
        [unlockButton setHidden:![LinphoneManager checkUnlockCode:switchCode]];
        [unlockButton setHidden:YES];
        
        
        [cameraView setUrl:[NSURL URLWithString:[self streamAddressUsingContact:contact]]];
        
        [cameraView setHidden:NO];
        [cameraView startForIntercom];
        //[cameraView playWith:contact.username And:contact.password And:@""];
    }
    else {
        name = username;
        [unlockButton setHidden:YES];
    }
    
    [callerNameLabel setText:name];
}

-(NSString*)streamAddressUsingContact:(IntercomContact*)contact{
    NSString*path = [NSString stringWithFormat:@"http://%@%@",contact.ipAddress,@"/api/camera/snapshot?width=640&height=480&source=internal"];
    return path;
}

#pragma mark - Button handlers

- (void)endCallButtonPressed:(id)sender
{
    if (call) {
        [[LinphoneManager instance] endCall:[self call]];
    }
    
//    [[self presentingViewController] dismissViewControllerAnimated:self completion:^{}];
}

- (void)speakerButtonPressed:(UIButton *)sender
{
    [[LinphoneManager instance] enableSpeaker:![speakerButton isSelected]];
    
    speakerButton.selected = !speakerButton.isSelected;//[[LinphoneManager instance] isSpeakerEnabled];
}

//- (void)fullscreenButtonPressed:(id)sender
//{
//    originalCameraViewFrame = cameraView.frame;
//    
//    cameraView.frame = [UIScreen mainScreen].bounds;
//    
//    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leaveFullscreen:)];
//    [cameraView addGestureRecognizer:tap];
//    [cameraView setUserInteractionEnabled:YES];
//}
//
//- (void)leaveFullscreen:(id)sender
//{
//    cameraView.frame = originalCameraViewFrame;
//    [cameraView setUserInteractionEnabled:NO];
//}

- (void)unlockButtonPressed:(id)sender
{    
    [[LinphoneManager instance] sendStringUsingDTMF:switchCode];
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

@end
