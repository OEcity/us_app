//
//  DeviceScheduleDetailViewController.m
//  iHC-MIIRF
//
//  Created by Tom Odler on 12.11.16.
//  Copyright © 2016 Vratislav Zima. All rights reserved.
//

#import "DeviceScheduleDetailViewController.h"
#import "SYCoreDataManager.h"
#import "Constants.h"
#import "AppDelegate.h"

@interface DeviceScheduleDetailViewController (){
    
    NSString *myScheduleName;
    double hysteresis;
    Elan*selectedElan;
    
    BOOL eLANsSelected;
    BOOL modesSelected;
    BOOL hysteresisSelected;
    
    double myTemp1;
    double myTemp2;
    double myTemp3;
    double myTemp4;
    
    UITableView *modesTableview;
    UITableView *elansTableView;
    
    NSMutableDictionary *scheduleDict;
}
@property (strong, nonatomic) NSNumberFormatter *formatter;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tbvHeight;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@end

@implementation DeviceScheduleDetailViewController
- (void)viewDidLoad
{
    [super viewDidLoad];
    _doneButton.tag = 0;
    
    scheduleDict = [[NSMutableDictionary alloc] init];
    
    _loaderDialog = [[HUDWrapper alloc] initWithRootController:self];
    
    eLANsSelected = hysteresisSelected = NO;
    
    _formatter = [[NSNumberFormatter alloc] init];
    [_formatter setNumberStyle:NSNumberFormatterNoStyle];
    [_formatter setMaximumIntegerDigits:2];
    [_formatter setMinimumIntegerDigits:2];
    
    
    _pickerView.delegate =self;
    _pickerView.dataSource = self;
    _pickerViewContainer.hidden = YES;
    
    myTemp1 = 15;
    myTemp2 = 20;
    myTemp3 = 24;
    myTemp4 = 28;
    
    hysteresis = 0;
    
    NSLog(@"elans count: %lu", (unsigned long)[[SYCoreDataManager sharedInstance]getAllRFElans].count);
    
    if(_schedule != nil){
        myScheduleName = _schedule.label;
        hysteresis = [_schedule.hysteresis  doubleValue];
        selectedElan = _schedule.elan;
        
        myTemp1 = _schedule.temp1.doubleValue;
        myTemp2 = _schedule.temp2.doubleValue;
        myTemp3 = _schedule.temp3.doubleValue;
        myTemp4 = _schedule.temp4.doubleValue;
        
        [_loaderDialog showWithLabel: NSLocalizedString(@"waitPls", nil)];
        [[SYAPIManager sharedInstance] getScheduleWithID:_schedule.heattimescheduleID fromElan:_schedule.elan success:^(AFHTTPRequestOperation *operation, id response) {
            scheduleDict = [response mutableCopy];
            NSLog(@"Schedule loaded");
            [_loaderDialog hide];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [_loaderDialog hide];
            NSLog(@"Error: %@", [error localizedDescription]);
        }];
    }
    
    eLANsSelected = modesSelected = NO;
}
#pragma mark textField Delegate
-(void)textFieldDidEndEditing:(UITextField *)textField{
    myScheduleName = textField.text;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    
    return NO;
}

#pragma mark - UITableViewDelegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSLog(@"Tableview tag:%ld", (long)tableView.tag);
    if(tableView == elansTableView && eLANsSelected)return [[SYCoreDataManager sharedInstance] getAllRFElans].count +1;
    if(tableView == modesTableview && modesSelected)return 5;
    if(tableView.tag == 1)return 4;
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1 && indexPath.row == 1 && eLANsSelected) return [[SYCoreDataManager sharedInstance] getAllRFElans].count*50+50;
    if(tableView.tag == 1 && indexPath.row == 3 && modesSelected) return 4*50+50;
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView.tag == 1){
        UITableViewCell * cell = nil;
        
        switch (indexPath.row) {
            case 0:{
                NSAttributedString *namePlaceHolder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"enterName", nil) attributes:@{ NSForegroundColorAttributeName : [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] }];
                cell = [tableView dequeueReusableCellWithIdentifier:@"nameCell" forIndexPath:indexPath];
                UITextField *tf = (UITextField*)[[[cell contentView] subviews] firstObject];
                tf.delegate = self;
                tf.attributedPlaceholder = namePlaceHolder;
                if(myScheduleName != nil){
                    tf.text = myScheduleName;
                }
            }
                break;
            case 1:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 2;
                elansTableView = tbv;
                tbv.delegate = self;
                tbv.dataSource = self;
            }
                break;
            case 2: {
                
                for(UIView*view in [[cell contentView] subviews]){
                    if(view.tag == 50){
                        [view removeFromSuperview];
                    }
                }
                
                cell = [tableView dequeueReusableCellWithIdentifier:@"hysteresisCell" forIndexPath:indexPath];
                UILabel *tf = (UILabel*)[[cell contentView] viewWithTag:1];
                
                tf.text = [NSString stringWithFormat:@"%@: %.1f", NSLocalizedString(@"heat_temp_schedule_add_hysteresis", nil), hysteresis ];
            }
                break;
            case 3:{
                cell = [tableView dequeueReusableCellWithIdentifier:@"elansCell"forIndexPath:indexPath];
                UITableView *tbv = (UITableView*)[[[cell contentView] subviews] firstObject];
                tbv.tag = 3;
                modesTableview = tbv;
                tbv.delegate = self;
                tbv.dataSource = self;
            }
                break;
                
            default:
                break;
        }
        if(!cell){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        return cell;
    }else if(tableView == elansTableView){
        
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
        
        if(indexPath.row == 0){
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
        } else {
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
        }
        
        
        UILabel* label = [[cell contentView] viewWithTag:1];
        UIImageView *img = [[cell contentView ] viewWithTag:2];
        UIView *line = [[cell contentView ] viewWithTag:3];
        
        if(eLANsSelected){
            line.hidden = YES;
            
            if(indexPath.row>0){
                img.hidden = YES;
                label.text = ((Elan*)[[[SYCoreDataManager sharedInstance] getAllRFElans] objectAtIndex:indexPath.row-1]).label;
                if([label.text isEqualToString:selectedElan.label]){
                    [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                }
            } else {
                label.text = @"eLAN";
                
                img.hidden = NO;
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            }
        } else {
            label.text = @"eLAN";
            if(selectedElan){
                label.text = selectedElan.label;
            }
            line.hidden = NO;
            img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
        }
        if(!cell){
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        }
        return cell;
    } else if(tableView == modesTableview){
        if(indexPath.row == 0){
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"elanLabel" forIndexPath:indexPath];
            
            
            UIView *bgColorView = [[UIView alloc] init];
            cell.selectedBackgroundView = bgColorView;
            
            UILabel* label = [[cell contentView] viewWithTag:1];
            UIImageView *img = [[cell contentView ] viewWithTag:2];
            UIView *line = [[cell contentView ] viewWithTag:3];
            
            label.text = NSLocalizedString(@"heat_temp_schedule_add_sub_header", nil);
            
            if(modesSelected){
                line.hidden = YES;
                img.image = [UIImage imageNamed:@"sipka_rozbalena.png"];
            } else {
                line.hidden = NO;
                img.image = [UIImage imageNamed:@"sipka_zabalena.png"];
            }
            return cell;
        } else {
            UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
            
            
            UIView *bgColorView = [[UIView alloc] init];
            bgColorView.backgroundColor = [UIColor colorWithRed:0.0f green:192.0f/255.0f blue:243.0f/255.0f alpha:1.0f];
            [cell setSelectedBackgroundView:bgColorView];
            
            UILabel *label = [[cell contentView] viewWithTag:1];
            UIButton *button = [[cell contentView] viewWithTag:2];
            switch(indexPath.row){
                case 1:
                    label.text = NSLocalizedString(@"heat_mode_minimum", nil);
                    [button setTitleColor:USBlueColor forState:UIControlStateNormal];
                    [button setTitle:[NSString stringWithFormat:@"%.1f", myTemp1] forState:UIControlStateNormal];
                    
                    button.layer.borderColor = USBlueColor.CGColor;
                    button.layer.borderWidth = 1.0f;
                    break;
                case 2:
                    label.text = NSLocalizedString(@"heat_mode_attenuation", nil);
                    [button setTitleColor:USUtlumColor forState:UIControlStateNormal];
                    [button setTitle:[NSString stringWithFormat:@"%.1f", myTemp2] forState:UIControlStateNormal];
                    
                    button.layer.borderColor = USUtlumColor.CGColor;
                    button.layer.borderWidth = 1.0f;
                    break;
                case 3:
                    label.text = NSLocalizedString(@"heat_mode_normal", nil);
                    [button setTitleColor:USNormalColor forState:UIControlStateNormal];
                    [button setTitle:[NSString stringWithFormat:@"%.1f", myTemp3] forState:UIControlStateNormal];
                    
                    button.layer.borderColor = USNormalColor.CGColor;
                    button.layer.borderWidth = 1.0f;
                    break;
                case 4:
                    label.text = NSLocalizedString(@"heat_mode_comfort", nil);
                    [button setTitleColor:USKomfrotColor forState:UIControlStateNormal];
                    [button setTitle:[NSString stringWithFormat:@"%.1f", myTemp4] forState:UIControlStateNormal];
                    
                    button.layer.borderColor = USKomfrotColor.CGColor;
                    button.layer.borderWidth = 1.0f;
                    break;
            }
            
            return cell;
        }
    } else {
        UITableViewCell * cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        return cell;
    }
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"tableviw tag selected: %ld", (long)tableView.tag);
    if(tableView.tag == 2 && indexPath.row == 0 && !eLANsSelected){
        eLANsSelected = YES;
        
        tableView.layer.borderColor = USBlueColor.CGColor;
        tableView.layer.borderWidth = 1.0f;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
        
    } else if (tableView.tag == 2 && indexPath.row == 0 && eLANsSelected){
        eLANsSelected = NO;
        
        tableView.layer.borderColor = nil;
        tableView.layer.borderWidth = 0.0f;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
    } else if(tableView.tag == 3 && indexPath.row == 0 && !modesSelected){
        modesSelected = YES;
        eLANsSelected = NO;
        
        tableView.layer.borderColor = USBlueColor.CGColor;
        tableView.layer.borderWidth = 1.0f;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
        
    } else if (tableView.tag == 3 && indexPath.row == 0 && modesSelected){
        modesSelected = NO;
        eLANsSelected = NO;
        
        tableView.layer.borderColor = nil;
        tableView.layer.borderWidth = 0.0f;
        
        [_tableView beginUpdates];
        [_tableView endUpdates];
        
    } else if(tableView.tag == 2 && indexPath.row > 0){
        selectedElan = [[[SYCoreDataManager sharedInstance] getAllRFElans] objectAtIndex:indexPath.row-1];
    } else if(tableView.tag == 3 && indexPath.row>0){
        switch(indexPath.row){
            case 1:
                [self showPickerWithNumber:myTemp1];
                break;
            case 2:
                [self showPickerWithNumber:myTemp2];
                break;
            case 3:
                [self showPickerWithNumber:myTemp3];
                break;
            case 4:
                [self showPickerWithNumber:myTemp4];
                break;
        }
        _doneButton.tag = indexPath.row;
    }
    
    if(indexPath.row == 0)
        [tableView reloadData];
    
    if(eLANsSelected){
        _tbvHeight.constant = (4*50) + ([[SYCoreDataManager sharedInstance] getAllRFElans].count*50);
    } else if(modesSelected){
        _tbvHeight.constant = (5*50) + (5*50);
    } else {
        _tbvHeight.constant = 4*50;
    }
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        if(hysteresisSelected){
            return 6;
        } else {
            return 27;
        }
    } else {
        return 2;
    }
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width/2-25, 44)];
    label.backgroundColor = [UIColor blackColor];
    label.textColor = [UIColor whiteColor];
    label.font = [UIFont fontWithName:@"Roboto-Light" size:18];
    if (component == 0){
        label.textAlignment = NSTextAlignmentRight;
        
        if(hysteresisSelected){
            label.text = [NSString stringWithFormat:@"%li",(long)(row)];
        } else {
            label.text = [NSString stringWithFormat:@"%li",(long)(row+5)];
        }
    }
    else {
        label.textAlignment = NSTextAlignmentLeft;
        if(row == 0){
            label.text = [_formatter stringFromNumber:[NSNumber numberWithInteger:0]];
        } else {
            label.text = [_formatter stringFromNumber:[NSNumber numberWithInteger:50]];
        }
        
    }
    return label;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 60.f;
}

-(void)showPickerWithNumber:(double)number{
    hysteresisSelected = NO;
    
    [_pickerView reloadAllComponents];
    [_pickerView selectRow:(int)number-5 inComponent:0 animated:NO];
    
    if(fmod(number, 1) != 0){
        [_pickerView selectRow:1 inComponent:1 animated:NO];
    } else {
        [_pickerView selectRow:0 inComponent:1 animated:NO];
    }
    
    _pickerViewContainer.hidden = NO;
}

- (IBAction)showPicker:(id)sender {
    hysteresisSelected = YES;
    _doneButton.tag =0;
    
    [_pickerView selectRow:(int)hysteresis inComponent:0 animated:NO];
    [_pickerView reloadAllComponents];
    
    if(fmod(hysteresis, 1) != 0){
        [_pickerView selectRow:1 inComponent:1 animated:NO];
    } else {
        [_pickerView selectRow:0 inComponent:1 animated:NO];
    }
    
    _pickerViewContainer.hidden = NO;
}

- (IBAction)doneButtonTapped:(id)sender {
    _pickerViewContainer.hidden = YES;
    if(hysteresisSelected){
        hysteresisSelected = NO;
        hysteresis = [_pickerView selectedRowInComponent:0];
        if([_pickerView selectedRowInComponent:1] > 0){
            hysteresis += 0.5;
        }
        NSLog(@"Hysteresis = :%.1f", hysteresis);
        NSIndexPath*ip = [NSIndexPath indexPathForRow:2 inSection:0];
        NSArray*ips = [[NSArray alloc] initWithObjects:ip, nil];
        [_tableView reloadRowsAtIndexPaths:ips withRowAnimation:UITableViewRowAnimationNone];
    } else {
        switch (_doneButton.tag) {
            case 1:
                myTemp1 = [_pickerView selectedRowInComponent:0]+5;
                if([_pickerView selectedRowInComponent:1] > 0){
                    myTemp1 += 0.5;
                }
                NSLog(@"MyTemp1: %.1f", myTemp1);
                break;
            case 2:
                myTemp2 = [_pickerView selectedRowInComponent:0]+5;
                if([_pickerView selectedRowInComponent:1] > 0){
                    myTemp2 += 0.5;
                }
                NSLog(@"MyTemp2: %.1f", myTemp2);
                break;
            case 3:
                myTemp3 = [_pickerView selectedRowInComponent:0]+5;
                if([_pickerView selectedRowInComponent:1] > 0){
                    myTemp3 += 0.5;
                }
                NSLog(@"MyTemp3: %.1f", myTemp3);
                break;
            case 4:
                myTemp4 = [_pickerView selectedRowInComponent:0]+5;
                if([_pickerView selectedRowInComponent:1] > 0){
                    myTemp4 += 0.5;
                }
                NSLog(@"MyTemp4: %.1f", myTemp4);
                break;
                
            default:
                break;
        }
        [modesTableview reloadData];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    [scheduleDict setObject:myScheduleName forKey:@"label"];
    
    NSMutableDictionary * modes = [[NSMutableDictionary alloc] initWithDictionary:[scheduleDict objectForKey:@"modes"]];
    
    NSMutableDictionary* mode1= [[NSMutableDictionary alloc] initWithDictionary:[modes objectForKey:@"1"]] ;
    [mode1 setObject:[NSNumber numberWithDouble:myTemp1] forKey:@"min"];
    [modes setObject:mode1 forKey:@"1"];
    
    NSMutableDictionary* mode2= [[NSMutableDictionary alloc] initWithDictionary:[modes objectForKey:@"2"]];
    [mode2 setObject:[NSNumber numberWithDouble:myTemp2] forKey:@"min"];
    [modes setObject:mode2 forKey:@"2"];
    
    NSMutableDictionary* mode3= [[NSMutableDictionary alloc] initWithDictionary:[modes objectForKey:@"3"]];
    [mode3 setObject:[NSNumber numberWithDouble:myTemp3] forKey:@"min"];
    [modes setObject:mode3 forKey:@"3"];
    
    NSMutableDictionary* mode4= [[NSMutableDictionary alloc] initWithDictionary:[modes objectForKey:@"4"]];
    [mode4 setObject:[NSNumber numberWithDouble:myTemp4] forKey:@"min"];
    [modes setObject:mode4 forKey:@"4"];
    
    [scheduleDict setObject:modes forKey:@"modes"];
    
    [scheduleDict setObject:[NSNumber numberWithDouble:hysteresis] forKey:@"hysteresis"];
    
}

- (IBAction)saveTapped:(id)sender {
    if(!myScheduleName || [myScheduleName isEqualToString:@""]){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"nameError", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    if(selectedElan == nil){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:NSLocalizedString(@"selectElan", nil)  delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
        
    }
    
    [self performSegueWithIdentifier:@"scheduleInterval" sender:nil];
}
@end
